﻿<%@ Page Title=" Linked Birth Records View Details" Language="C#" MasterPageFile="~/loginMaster.master" AutoEventWireup="true" CodeFile="LinkedBirthRecordsView.aspx.cs" Inherits="LinkedBirthRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #primary-icon, #divGreen, #divRed, div.logo, img.bell-icon {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <% if (Session["username"] != null)
        { %>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Literal Text="" ID="ltrTableName" runat="server" />
            </h1>
        </div>
    </div>

    <div class="row row-no-padding">
        <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
            <div class="tableFixHead">
                <table class="table table-bordered table-striped table-fixed" visible="true" rules="all" id="gvDynamic" style="border-collapse: collapse;" border="1" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="data-element">Data Element</th>
                            <th class="birth-record">Birth Record</th>
                    </thead>
                    <tbody>

                        <% if (dt != null)
                            {
                                for (int i = 1; i < dt.Rows.Count; i++)
                                { %>
                        <tr>
                            <% int k = 0;
                                for (int J = 0; J < dt.Columns.Count; J++)
                                {
                                    k = k + 1;
                                    if (J == 1 && dt.Rows[0][J].ToString() == "Birth Record")
                                    { %>
                            <td class="bold-text white-color"><%= dt.Rows[i][J] %></td>
                            <% }
                                else
                                { %>
                            <td <%= J == 0 ? "class=\"grey-color\"" : (J == 1 ? "class=\"blue-color\"" : (J == 2 ? "class=\"white-color\"" : (J == 3 ? "class=\"white-color\"" : (J == 4 ? "class=\"white-color\"" : "")))) %>><%= dt.Rows[i][J] %></td>
                            <% }
                                }
                                if (k != 1)
                                {
                                    for (int J = k; J < 1; J++)
                                    { %>
                            <td>&nbsp;</td>
                            <% }
                                } %>
                        </tr>
                        <% }
                            } %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <% } %>

    <script>
        setTimeout(function () {
            window.close();
        }, 600000);
    </script>

</asp:Content>

