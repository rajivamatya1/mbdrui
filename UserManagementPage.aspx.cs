﻿using DynamicGridView.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//User Management Page

public partial class UserManagementPage : System.Web.UI.Page
{
    dynamic permissions = null;
    bool userCredential = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            string env = ConfigurationManager.AppSettings["environment"];
            hdEnviroment.Value = env;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    divpassword.Visible = true;
                }
                else
                {
                    divpassword.Visible = false;
                }
            }
            else
            {
                divpassword.Visible = false;
            }

            if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string miMiLogin = string.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();

            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string userId = !string.IsNullOrEmpty(Request.QueryString["userId"]) ? Convert.ToString(Request.QueryString["userId"]) : "";

            if (!IsPostBack)
            {
                string timeout = !string.IsNullOrEmpty(Request.QueryString["timeout"]) ? Convert.ToString(Request.QueryString["timeout"]) : "";

                if (timeout == "Yes")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "timeout", "noRecFound( 'The timeout period elapsed prior to completion of the operation or the server is not responding.');", true);
                }

                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                txtFirstName.EnableViewState = false;
                txtLastName.EnableViewState = false;

                txtEmailId.EnableViewState = false;
                txtConfirmMail.Text = string.Empty;
                txtUserPass.EnableViewState = false;
                txtConfirmPassword.Text = string.Empty;

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');
                    BindColumnsDropDown(schema, tableName);
                    DataTable dt = new DataTable();                   

                    if (permissions.Count > 0)
                    {

                        if (!string.IsNullOrEmpty(userId))
                        {
                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                            {
                                string where = "where isHidden IS NULL AND userId=" + userId + "";

                                string query = string.Format("SELECT userId,firstName,lastName,emailAddress,HashedPassword,roleid FROM {0}.{1} {2}", schema, tableName, where);

                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.CommandTimeout = 0;

                                conn.Open();
                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                conn.Close();
                                da.Fill(dt);
                            }
                            if (dt.Rows.Count > 0)
                            {
                                txtFirstName.Text = dt.Rows[0]["firstName"].ToString();
                                hdExtFN.Value = dt.Rows[0]["firstName"].ToString();

                                txtLastName.Text = dt.Rows[0]["lastName"].ToString();
                                hdExtLN.Value = dt.Rows[0]["lastName"].ToString();

                                txtEmailId.Text = dt.Rows[0]["emailAddress"].ToString();
                                hdExtEmail.Value = dt.Rows[0]["emailAddress"].ToString();

                                txtConfirmMail.Text = dt.Rows[0]["emailAddress"].ToString();
                                hdExtConEmail.Value = dt.Rows[0]["emailAddress"].ToString();

                                ddlRoleType.SelectedValue = dt.Rows[0]["roleid"].ToString();
                                hdExtRoles.Value = dt.Rows[0]["roleid"].ToString();

                                // Retrieve the hash password
                                string hashedPassword = dt.Rows[0]["HashedPassword"].ToString();

                                // Calculate the length of the hashed password
                                int passwordLength = hashedPassword.Length;

                                // Replace each character of the hashed password with asterisks
                                string maskedPassword = new string('*', passwordLength);

                                hidMaskedPassword.Value = maskedPassword;
                                hdExtPass.Value = maskedPassword;
                                hdExtConPass.Value = maskedPassword;

                                txtUserPass.Attributes["value"] = hidMaskedPassword.Value;
                                txtConfirmPassword.Attributes["value"] = hidMaskedPassword.Value;

                                btnCreate.Text = "Update";
                                btnCreate.CssClass = "cursor-default";
                                lblHeader.Text = "Update Existing Account";

                                btnNewAc.Enabled = true;
                                btnNewAc.CssClass = "cursor-pointer";

                                btnClear.Enabled = false;
                                btnClear.Visible = false;
                            }
                        }
                        else
                        {
                            lblHeader.Text = "Create New Account";
                        }
                    }
                    else
                    {
                        createUserAccess.Visible = false;
                        btnUserRole.Visible = false;
                    }
                }
            }

            BindGrid(schema, tableName, permissions);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_Page_Load");
        }
    }

    public dynamic CheckRole()
    {
        dynamic storepermissions = null;

        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

            if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
            {
                storepermissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "USER_WRITE")
                    .Select(permission => new
                    {
                        userId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_CheckRole");
        }
        return storepermissions;
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable dtEntityList = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string queryList = string.Format("select roleid, name, details from Reference.Roles order by details asc");
                SqlCommand cmd = new SqlCommand(queryList, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtEntityList);
                ddlRoleType.DataSource = dtEntityList;
                ddlRoleType.DataTextField = "details";
                ddlRoleType.DataValueField = "roleid";
                ddlRoleType.DataBind();
                ddlRoleType.Items.Insert(0, "Select Role");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_BindColumnsDropDown");
        }
    }

    public void BindGrid(string schema, string tableName, dynamic permissions)
    {
        try
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Convert.ToString(item[0]) == "userId")
                        {
                            sortExp = "userId desc";
                            break;
                        }
                    }
                }
            }
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string OrderByCRec = sortExp;
                if (string.IsNullOrEmpty(OrderByCRec))
                {
                    OrderByCRec = "userId desc";
                }

                query = string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,MP.userId 'userId',MP.firstName 'firstName', MP.lastName 'lastName', MP.displayName 'displayName', MP.emailAddress 'emailAddress',MP.HashedPassword 'HashedPassword', MP.created 'created',MP.lastLogin 'lastLogin', R.details 'UserPreferences', R.roleid 'roleid' FROM MBDR_System.Users MP Left OUTER JOIN Reference.roles R ON MP.roleId = R.roleId  WHERE  MP.isHidden IS NULL) AS RESULT WHERE ROW between({0}) and ({1})", ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, OrderByCRec);

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);

                gvDynamic.Columns.Clear();
                List<string> lstColumns = includeColumns.Split(',').ToList();
                foreach (var col in lstColumns)
                {
                    if (gvDynamic.Columns.Count < 9)
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            foreach (DataColumn item in dt.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                                if (colName != "IsPermittedToLogon" && colName != "LogonName" && colName != "aliasFirstName" && colName != "aliasMiddleName")
                                {
                                    if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                        BoundField field = new BoundField();
                                        field.HeaderText = columnInfo;
                                        field.DataField = item.ColumnName;
                                        field.SortExpression = item.ColumnName;
                                        if (readonlyColumns.Contains(colName))
                                        {
                                            field.ReadOnly = true;
                                        }
                                        gvDynamic.Columns.Add(field);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                if (permissions.Count > 0)
                {
                    CommandField commandField = new CommandField();
                    commandField.ButtonType = ButtonType.Button;
                    commandField.HeaderText = "Action";
                    commandField.ShowEditButton = true;
                    commandField.ShowCancelButton = true;
                    commandField.Visible = true;
                    gvDynamic.Columns.Add(commandField);

                    CommandField commandField1 = new CommandField();
                    commandField1.ButtonType = ButtonType.Button;
                    commandField1.ShowEditButton = true;
                    commandField1.ShowCancelButton = false;
                    commandField1.Visible = true;
                    gvDynamic.Columns.Add(commandField1);
                }

                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                if (gvDynamic.VirtualItemCount < 1)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                }

                gvDynamic.DataSource = dt;
                gvDynamic.DataBind();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                Response.Redirect("/UserManagementPage.aspx?schema=MBDR_System&table=Users&timeout=Yes", false);
            }
        }

        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";

                query = string.Format("select count({0}) from {1}.{2} WHERE isHidden IS NULL ", datakeyName, schema, tableName);

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_GetTotalRecords");
        }
        return totalRecords;
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                string UserPreferencesValue = ddlRoleType.SelectedItem.Text;
                string roleId = ddlRoleType.SelectedItem.Value;

                string query = string.Empty;
                string message = string.Empty;
                string buttontext = (sender as Button).Text;

                int totalRecords = 0;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query1 = "";
                    string where = "where emailAddress ='" + txtEmailId.Text.Trim() + "'";
                    query1 = string.Format("select count({0}) from {1}.{2} {3} ", "userId", "MBDR_System", "Users", where);

                    SqlCommand cmd = new SqlCommand(query1, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }

                string env = ConfigurationManager.AppSettings["environment"];

                hdEnviroment.Value = env;

                /************ Edit a existing user *****************/

                if (buttontext == "Update")
                {
                    if (totalRecords > 0 || totalRecords == 0)
                    {
                        string userId = !string.IsNullOrEmpty(Request.QueryString["userId"]) ? Convert.ToString(Request.QueryString["userId"]) : "";

                        if (!string.IsNullOrEmpty(roleId) && roleId == "Select Role" || ddlRoleType.SelectedIndex == -1)
                        {
                            txtUserPass.Attributes["value"] = hidMaskedPassword.Value;
                            txtConfirmPassword.Attributes["value"] = hidMaskedPassword.Value;
                            ClientScript.RegisterStartupScript(this.GetType(), "Select Role Type", "SelectRoleType(' Select Role Type.');", true);
                        }
                        else
                        {
                            // check if email address already been used or not 

                            int emailAlreadyUsed = 0;

                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                            {
                                string query1 = "";
                                string where = "where emailAddress ='" + txtEmailId.Text.Trim() + "' and userId != " + userId + "";
                                query1 = string.Format("select count({0}) from {1}.{2} {3} ", "userId", "MBDR_System", "Users", where);

                                SqlCommand cmd = new SqlCommand(query1, conn);
                                cmd.CommandTimeout = 0;

                                DataTable dt = new DataTable();
                                conn.Open();
                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                conn.Close();
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    emailAlreadyUsed = Convert.ToInt32(dt.Rows[0][0]);
                                }
                            }

                            if (emailAlreadyUsed > 0) 
                            {
                                message = "A user with this email address already exists.";
                            }
                            else
                            {
                                int totalPassMatchcount = 0;

                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    if (txtUserPass.Text.Contains("***"))
                                    {
                                        totalPassMatchcount = 1;
                                    }
                                    else
                                    {
                                        string query1 = "";
                                        string where = "where HashedPassword = HASHBYTES('SHA2_512', '" + txtUserPass.Text.Trim() + "') and userId = " + userId + "";
                                        query1 = string.Format("select count({0}) from {1}.{2} {3} ", "userId", "MBDR_System", "Users", where);

                                        SqlCommand cmd = new SqlCommand(query1, conn);
                                        cmd.CommandTimeout = 0;

                                        DataTable dt = new DataTable();
                                        conn.Open();
                                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                                        conn.Close();
                                        da.Fill(dt);
                                        if (dt.Rows.Count > 0)
                                        {
                                            totalPassMatchcount = Convert.ToInt32(dt.Rows[0][0]);
                                        }
                                    }
                                }

                                if (totalPassMatchcount > 0)
                                {
                                    query = "Update [MBDR_System].[Users] set firstName='" + txtFirstName.Text + "', lastName='" + txtLastName.Text + "' , emailAddress='" + txtEmailId.Text.Trim() + "' , roleid='" + roleId + "', modifierId='" + Convert.ToString(Session["userid"]) + "' where userId='" + userId + "'";
                                }
                                else
                                {
                                    query = "Update [MBDR_System].[Users] set firstName='" + txtFirstName.Text + "', lastName='" + txtLastName.Text + "', HashedPassword=HASHBYTES('SHA2_512', '" + txtUserPass.Text + "') , emailAddress='" + txtEmailId.Text.Trim() + "' , roleid='" + roleId + "', modifierId='" + Convert.ToString(Session["userid"]) + "' where userId='" + userId + "'";
                                }
                                message = "The user account has been successfully updated.";

                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    int rowsAffected = cmd.ExecuteNonQuery();
                                    conn.Close();
                                    btnCreate.Text = "Create";
                                    lblHeader.Text = "Create New Account";
                                    divpassword.Visible = true;
                                    txtFirstName.Text = string.Empty;
                                    txtLastName.Text = string.Empty;
                                    txtEmailId.Text = string.Empty;
                                    txtConfirmMail.Text = string.Empty;
                                    txtUserPass.Text = string.Empty;
                                    txtConfirmPassword.Text = string.Empty;
                                    ddlRoleType.SelectedIndex = 0;
                                }
                            }
                        }
                    }
                    BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);

                    if (!string.IsNullOrEmpty(message) && message == "A user with this email address already exists.")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "dialogBox('" + message + "', 'Error');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "SelectRoleType", "SelectRoleType('" + message + "')", true);
                    }
                }

                /************ Create a new user *****************/

                if (buttontext == "Create")
                {
                    if (totalRecords == 0)
                    {
                        Guid guid = Guid.NewGuid();

                        //DateTime dt = DateTime.Now;
                        
                        //string guid = dt.ToLocalTime().ToUniversalTime().Subtract(dt).TotalMilliseconds.ToString();

                        string pass = string.Empty;

                        if (!string.IsNullOrEmpty(env))
                        {
                            if (env == "dev" || env == "qa")
                            {
                                pass = txtUserPass.Text;
                            }
                            else
                            {
                                pass = guid.ToString();
                            }
                        }
                        else
                        {
                            pass = guid.ToString();
                        }

                        if (!string.IsNullOrEmpty(roleId) && roleId == "Select Role" || ddlRoleType.SelectedIndex == -1)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Select Role Type", "SelectRoleType(' Select Role Type.');", true);
                        }
                        else
                        {
                            query = "insert into MBDR_System.Users (firstName,lastName,emailAddress,isHidden,roleid,HashedPassword,created,modifierId) values ('" + txtFirstName.Text.Trim() + "','" + txtLastName.Text.Trim() + "','" + txtEmailId.Text.Trim() + "',null," + roleId + ",HASHBYTES('SHA2_512', '" + pass + "'),GETDATE()," + Convert.ToString(Session["userid"]) + ")";

                            message = "The user account has been successfully created.";

                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                            {
                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.CommandTimeout = 0;

                                conn.Open();
                                int rowsAffected = cmd.ExecuteNonQuery();
                                conn.Close();
                                btnCreate.Text = "Create";
                                lblHeader.Text = "Create New Account";
                                divpassword.Visible = true;
                                txtFirstName.Text = string.Empty;
                                txtLastName.Text = string.Empty;
                                txtEmailId.Text = string.Empty;
                                txtConfirmMail.Text = string.Empty;
                                txtUserPass.Text = string.Empty;
                                txtConfirmPassword.Text = string.Empty;
                                ddlRoleType.SelectedIndex = 0;
                            }
                        }
                    }
                    else
                    {
                       message = "A user with this email address already exists.";
                    }
                     
                    BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);

                    if (!string.IsNullOrEmpty(message) && message == "A user with this email address already exists.")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "dialogBox('" + message + "', 'Error');", true);
                    }
                    else 
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "SelectRoleType", "SelectRoleType('" + message + "')", true);
                    }
                }

                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        divpassword.Visible = true;
                    }
                    else
                    {
                        divpassword.Visible = false;
                    }
                }
                else
                {
                    divpassword.Visible = false;
                }

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.ErrorLogCapture(ex, path, "UserManagementPage_btnCreate_Click");
            }
        }
    }

    private void ClearFormFields()
    {
        // Find all input controls on the web form.
        foreach (Control control in Page.Controls)
        {
            if (control is TextBox)
            {
                ((TextBox)control).Text = "";
            }
            else if (control is DropDownList)
            {
                ((DropDownList)control).SelectedIndex = 0;
            }
            else if (control is CheckBox)
            {
                ((CheckBox)control).Checked = false;
            }
        }
    }

    protected void btnUserRole_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementRoles.aspx?Schema=Reference&Table=Roles");
    }

    protected void btnPermissions_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementRolesPermission.aspx?schema=Reference&table=Permission");
    }

    protected void gvDynamic_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("/UserManagementPage.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&userId=" + id + "", false);
            }
            else if (e.CommandName == "Disable")
            {
                int userID = Convert.ToInt32(e.CommandArgument.ToString());
                hdnUserID.Value = Convert.ToString(userID);
                ScriptManager.RegisterStartupScript(this, GetType(), "ConfirmationScript", "deactiveAlert('" + userID + "')", true);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_gvDynamic_RowDataBound");
        }
    }

    private void UpdateAccess(int userID)
    {
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string updateQuery = "Update MBDR_System.Users set RoleID = null, modifierId = @modifierId  where userId = @userID";
            SqlCommand updateCommand = new SqlCommand(updateQuery, connection);
            updateCommand.Parameters.AddWithValue("@modifierId", Convert.ToString(Session["userid"]));
            updateCommand.Parameters.AddWithValue("@userID", userID);
            updateCommand.CommandTimeout = 0;

            connection.Open();
            updateCommand.ExecuteNonQuery();
            connection.Close();
        }
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int cellCount = e.Row.Cells.Count;

                int sessionUser = Convert.ToInt32(Session["userid"]);

                if (cellCount >= 9) // Check if the row has at least 8 cells
                {
                    Button field = e.Row.Cells[7].Controls[0] as Button;
                    field.Text = "Edit";
                    field.CommandName = "Edit";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    e.Row.Cells[7].Controls.Add(field);

                    field.OnClientClick = "ShowProgress();";

                    Button cancelButton = e.Row.Cells[8].Controls[0] as Button;
                    cancelButton.Text = "Disable";
                    cancelButton.CommandName = "Disable";
                    cancelButton.ToolTip = string.Format("Cancel Cancel for Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    cancelButton.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);

                    cancelButton.OnClientClick = "ShowProgress();";

                    string txtRole = Convert.ToString(e.Row.Cells[3].Text);

                    // check if the current row index's data key value matches session user

                    int rowUserId = Convert.ToInt32(gvDynamic.DataKeys[e.Row.RowIndex].Value);

                    if (!string.IsNullOrEmpty(txtRole) && txtRole != "&nbsp;" && rowUserId != sessionUser)
                    {
                        cancelButton.Enabled = true;
                    }
                    else
                    {
                        cancelButton.Enabled = false;
                    }

                    e.Row.Cells[7].Controls.Add(cancelButton);

                    TableCell currentCell = e.Row.Cells[8];
                    TableCell previousCell = e.Row.Cells[7];
                    previousCell.ColumnSpan = 2;
                    previousCell.CssClass = "merged-cell";

                    if (string.IsNullOrEmpty(currentCell.Text))
                    {
                        currentCell.Visible = false; // Hide the current cell if it is empty
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementPage_gvDynamic_RowDataBound");
        }
    }

    protected void gvDynamic_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_Sorting");
        }
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC";
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_GetSortDirection");
        }
        return nextDir;
    }

    protected void gvDynamic_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagement_gvDynamic_PageIndexChanging");
        }
    }

    protected void gvDynamic_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {
        gvDynamic.EditIndex = e.NewEditIndex;
        // Rebind the GridView to enter edit mode
        BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
    }

    protected void gvDynamic_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {

    }

    protected void gvDynamic_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        gvDynamic.EditIndex = -1;
        // Rebind the GridView to enter edit mode
        BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
    }

    protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDynamic.EditIndex = -1;
        // Rebind the GridView to enter edit mode
        BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
    }

    protected void userTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // args.IsValid = userTypeCheckBoxList.SelectedIndex != -1;
    }
        
    protected void btnHidden_Click(object sender, EventArgs e)
    {
        RemoveUserAccess(Convert.ToInt32(hdnUserID.Value));

        if (!userCredential)
        {
            BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);
            string message = "The user has been disabled.";
            ScriptManager.RegisterStartupScript(this, GetType(), "SelectRoleType", "SelectRoleType('" + message + "')", true);
        }
    }

    private void RemoveUserAccess(int userID)
    {
        try
        {
            string json = "{\"cmd\":\"DISABLE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"target\":" + userID + "}";

            string response = SocketConn(json);
            string getMsgs = $"RemoveUserAccess|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    userCredential = false;
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                    return;
                }
                else
                {       
                    // Possible Error codes: NO_USER, NOT_VALID
                    string message = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support.";

                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "dialogBox('" + message + "', 'Error Message');", true);
                }
            }
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
            // This exception is expected when using Response.Redirect and can be safely ignored.
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagement_RemoveUserAccess");
        }

        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void cvRoleType_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = ddlRoleType.SelectedValue != "Select Role";
    }

    protected void gvDynamic_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[7].Text = "Action";
            e.Row.Cells[7].ColumnSpan = 2;
            e.Row.Cells.RemoveAt(8);
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Center;
        }
    }

    protected void btnNewAc_Click(object sender, EventArgs e)
    {
        lblHeader.Text = "Create New Account";
        btnCreate.Text = "Create";

        btnNewAc.Enabled = false;
        btnNewAc.CssClass = "cursor-default";

        btnClear.Enabled = true;
        btnClear.Visible = true;
        btnClear.CssClass = "btnCommonBlue";

        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        ReqValEmail.Enabled = false; // Disable email validation
        ReqValConfirmEmail.Enabled = false; // Disable email validation
        txtEmailId.Text = string.Empty;
        txtEmailId.Attributes["value"] = string.Empty; // Clear the autofilled value
        txtConfirmMail.Text = string.Empty;
        txtConfirmMail.Attributes["value"] = string.Empty; // Clear the autofilled value

        CompValConfirmEmail.IsValid = true;
        txtUserPass.Text = string.Empty;
        txtConfirmPassword.Text = string.Empty;
        divpassword.Visible = true;
        ddlRoleType.SelectedIndex = 0;
        cvRoleType.Enabled = false;

        BindGrid(hdfSchema.Value, hdfTableName.Value, permissions);

        string env = ConfigurationManager.AppSettings["environment"];

        hdEnviroment.Value = env;

        if (!string.IsNullOrEmpty(env))
        {
            if (env == "dev" || env == "qa")
            {
                divpassword.Visible = true;
            }
            else
            {
                divpassword.Visible = false;
            }
        }
        else
        {
            divpassword.Visible = false;
        }
    }

    public string SocketConn(string JsonRecord)
    {
        string responseFromServer = string.Empty;
        string socketIp = ConfigurationManager.AppSettings["ipAddress"];
        int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
        try
        {
            using (var socket = new TcpClient(socketIp, socketPort))
            {
                string json = JsonRecord;
                byte[] body = Encoding.UTF8.GetBytes(json);
                int bodyLength = Encoding.UTF8.GetByteCount(json);
                var bl = (byte)(bodyLength);
                var bh = (byte)(bodyLength >> 8);
                using (var stream = socket.GetStream())
                {
                    stream.WriteByte(bh);
                    stream.WriteByte(bl);
                    stream.Write(body, 0, bodyLength);

                    short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                    byte[] buffer = new byte[size];

                    int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                    if (sizestream == size)
                    {
                        responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                    }
                    else
                    {
                        responseFromServer = "SocketConnectionError";
                    }
                }
            }
        }
        catch (Exception)
        {
            responseFromServer = "SocketConnectionError";
        }
        return responseFromServer;
    }

    public static void ErrorLog(string values)
    {
        bool shot = true;
        if (shot)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.Snap(values, path, "UserManagementPage.aspx");
        }
    }
}

// Custom class for the button templates

public class ButtonTemplate : ITemplate
{
    private Button _button;

    public ButtonTemplate(Button button)
    {
        _button = button;
    }

    public void InstantiateIn(Control container)
    {
        container.Controls.Add(_button);
    }
}