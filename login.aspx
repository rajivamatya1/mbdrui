﻿<%@ Page Title="MBDR UI Home" Language="C#" MasterPageFile="~/loginMaster.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" Runat="server">
        <div class="MBDRUIFORM-container">
            <div class="rowMBDRUIFORM"> 
                <div class="MBDRUIFORM-item">
                    <%--<p class="MBDRUIFORMError"><asp:Label ID="lblErrorMessage" runat="server" Text="Unable to authenticate user." ForeColor="Red"></asp:Label></p>--%>
                    <asp:Label ID="lblUserDetail" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblUserDetail1" runat="server" Text=""></asp:Label>
                </div>
                <div class="MBDRUIFORM-item">
                    <div class="MBDRUIFORM-card">
                        <h1>Please login below to access the application.</h1>
                        <label for="Content_txtUserName">Email</label>
                        <asp:TextBox ID="txtUserName" runat="server" aria-describedby="help-email" required="required" ToolTip="Enter email" Width="100%"></asp:TextBox>
                       <%-- <span id="help-email">Please enter your email address. Note: email is case-senstive.</span>--%>
                        <span id="help-email">Please enter your email address.</span>
                        <asp:RequiredFieldValidator Width="100%" CssClass="form-text" ID="RequiredFieldValidator1" ControlToValidate="txtUserName" runat="server" ErrorMessage="This is a required field."></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator  Width="100%" CssClass="form-text" ID="RegularExpressionValidator1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtUserName" runat="server" ErrorMessage="Please enter valid email address"></asp:RegularExpressionValidator>
                        
                        <label for="Content_txtPassword">Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" aria-describedby="help-pass" required="required" ToolTip="Enter Password" Width="100%"></asp:TextBox>
                        <span id="help-pass">Please enter your password.</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="form-text" ControlToValidate="txtPassword" runat="server" ErrorMessage="This is a required field."></asp:RequiredFieldValidator>
                        <br />
                        <asp:button class="btn btn-primary" runat="server" onclick="MBDRpass" text="Login" />
                        <%--<p>Or</p>
                        <asp:button ID="btnMiLogin" class="btn btn-primary lnkButton" CausesValidation="false" UseSubmitBehavior="false" runat="server" onclick="btnMiLogin_Click" Text="Continue with MILogin" />--%>
                    </div>     
                </div>
                  <center>
                    <p class="MBDRUIFORMError">
                        <h3>
                            <asp:Label ID="lblErrorMessage" runat="server" Text="Unable to authenticate user." ForeColor="Red"></asp:Label>
                        </h3>
                    </p>
                </center>
            </div>
        </div>
</asp:Content>