﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


//User Management Roles

public partial class UserManagementRoles : System.Web.UI.Page
{
    dynamic permissions = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }


        permissions = CheckRole();
        if (permissions.Count > 0)
        {
            btnAddrole.Enabled = true;
        }
        else
        {
            btnAddrole.Enabled = false;
        }
        if (!IsPostBack)
        {
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string roleId = !string.IsNullOrEmpty(Request.QueryString["roleId"]) ? Convert.ToString(Request.QueryString["roleId"]) : "";
            hdfSchema.Value = schema;
            hdfTableName.Value = tableName;
            hdfDataKeyName.Value = "roleId";
            DataTable dt = new DataTable();

            if(!string.IsNullOrEmpty(roleId))
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string where = "where roleId=" + roleId + "";
                    string query = string.Format("SELECT * FROM {0}.{1} {2}", schema, tableName, where);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                }
                if (dt.Rows.Count > 0)
                {
                    txtRole.Text = dt.Rows[0]["name"].ToString();
                    txtRoleDetails.Text = dt.Rows[0]["details"].ToString();
                    btnAddrole.Text = "Update";
                }

            }

            BindGrid(schema, tableName);

        }
    }
    
    public dynamic CheckRole()
    {
        dynamic permissions = null;
        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

             permissions = dtAccessPermission.AsEnumerable()
                .Where(permission => permission.Field<string>("PermissionName") == "USER_WRITE")
                .Select(permission => new
                {
                    UserId = permission.Field<string>("userId"),
                    RoleId = permission.Field<string>("RoleId"),
                    RoleName = permission.Field<string>("RoleName"),
                    RoleDetails = permission.Field<string>("RoleDetails"),
                    PermissionId = permission.Field<string>("PermissionId"),
                    PermissionName = permission.Field<string>("PermissionName")
                })
                .ToList();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRoles_CheckRole");
        }
        return permissions;
    }
    
    protected void btnAddrole_Click(object sender, EventArgs e)
    {
        try
        {
            string query = string.Empty;
            string buttontext = (sender as Button).Text;

            if (buttontext == "Update")
            {
                string roleId = !string.IsNullOrEmpty(Request.QueryString["roleId"]) ? Convert.ToString(Request.QueryString["roleId"]) : "";

                query = "Update [Reference].[Roles] set name='" + txtRole.Text + "', details='" + txtRoleDetails.Text + "' where roleId='" + roleId + "'";

            }
            else
            {
                query = "insert into [Reference].[Roles] (name,details) values('" + txtRole.Text + "','" + txtRoleDetails.Text + "')";
            }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                int rowsAffected = cmd.ExecuteNonQuery();
                conn.Close();
                btnAddrole.Text = "Create";
                txtRole.Text = string.Empty;
                txtRoleDetails.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRoles_btnAddrole_Click");
        }
        BindGrid(hdfSchema.Value, hdfTableName.Value);
    }

    public void BindGrid(string schema, string tableName)
    {
        try
        {
            permissions = CheckRole();
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Convert.ToString(item[0]) == "roleId")
                        {
                            sortExp = " roleId asc";
                            break;
                        }
                    }
                }
            }

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            hdfDataKeyName.Value = dataKeyname;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                string OrderBySID = sortExp;
                if (string.IsNullOrEmpty(OrderBySID))
                {
                    OrderBySID = "roleid desc";
                }
                int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
               
                    query = string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS Row,* FROM {0}.{1} {5}) AS RESULT WHERE ROW between({3}) and ({4}) ORDER BY {6}", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);


                gvDynamic.Columns.Clear();

                    List<string> lstColumns = includeColumns.Split(',').ToList();

                    foreach (var col in lstColumns)
                    {
                        if (gvDynamic.Columns.Count < 2)
                        {
                            if (!string.IsNullOrEmpty(col))
                            {
                                foreach (DataColumn item in dt.Columns)
                                {
                                    string colName = item.ColumnName;
                                    string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                            BoundField field = new BoundField();
                                            field.HeaderText = columnInfo;
                                            field.DataField = item.ColumnName;
                                            field.SortExpression = item.ColumnName;
                                            if (readonlyColumns.Contains(colName))
                                            {
                                                field.ReadOnly = true;
                                            }
                                            gvDynamic.Columns.Add(field);
                                            break;
                                        }
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                if (permissions.Count > 0)
                {
                    CommandField commandField = new CommandField();
                    commandField.ButtonType = ButtonType.Button;
                    commandField.HeaderText = "Operation";
                    commandField.ShowEditButton = true;
                    commandField.ShowCancelButton = true;
                    gvDynamic.Columns.Add(commandField);
                }

                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                    gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                    if (gvDynamic.VirtualItemCount < 1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                    }
                    gvDynamic.DataSource = dt;
                    gvDynamic.DataBind();
                }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRole_BindGrid");
        }
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                int pageIndex = 0;
                query = string.Format("select count({0}) from {1}.{2} ", datakeyName, schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
               
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRole_GetTotalRecords");
        }
        return totalRecords;
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRole_gvDynamic_PageIndexChanging");
        }
    }

    protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvDynamic.EditIndex = -1;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRole_gvDynamic_RowCancelingEdit");
        }
    }

    protected void btnUserManage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementPage.aspx?schema=MBDR_System&table=Users");
    }
       
    protected void gvDynamic_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var row = (DataRowView)e.Row.DataItem;
                if (e.Row.Cells.Count >= 3) // Check if the row has at least 3 cells
                {
                    Button field = e.Row.Cells[2].Controls[0] as Button;
                    field.Text = "Edit";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                }
                else
                {

                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRoles_gvDynamic_RowDataBound");
        }
    }

    protected void gvDynamic_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                Response.Redirect("UserManagementRoles.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&roleId=" + id + "", false);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRoles_gvDynamic_RowDataBound");
        }
    }

    protected void gvDynamic_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {

    }

    protected void gvDynamic_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_Sorting");
        }
    }
    
    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC";
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_GetSortDirection");
        }
        return nextDir;
    }

    protected void gvDynamic_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {

    }

    protected void btnPermissions_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementRolesPermission.aspx?schema=Reference&table=Permission");
    }

    protected void gvDynamic_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[2].Text = "Action";
            //e.Row.Cells[7].ColumnSpan = 2;
            //e.Row.Cells.RemoveAt(8);
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Center;
        }
    }
}
