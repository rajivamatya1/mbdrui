﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class DefaultTotintView : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
                string facility = !string.IsNullOrEmpty(Request.QueryString["facility"]) ? Convert.ToString(Request.QueryString["facility"]) : "";

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');
                    if (header.Length > 0)
                    {
                        lblHeader.Text = header[1];
                        lblHeader.Visible = true;
                    }
                    hdfName.Value = headerLabel;
                    lblSchema.Text = schema;
                    lblTable.Text = tableName;
                    BindColumnsDropDown(schema, tableName);
                    txtDateFrom.Text = fromDate;
                    txtDateTo.Text = toDate;
                    txtSearch.Text = search;
                    ddlColumn.SelectedValue = colName;
                    ddlTotintTables.SelectedValue = facility;
                    BindGrid(schema, tableName);

                }
            }
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            DataTable dt = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            List<DropDownModal> lstTotintTables = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                // Totint Dropdown Code
                var totintTables = lstFilter.Where(s => s.Schema == "UI_MBDR_TOTINT").ToList();
                foreach (var item in totintTables)
                {
                    string[] totIntTable = item.Table.Split(':');
                    if (totIntTable.Length > 1)
                    {
                        lstTotintTables.Add(new DropDownModal() { Name = totIntTable[1], OrigColName = string.Format("{0}.{1}", item.Schema, totIntTable[0]) });
                    }
                    else
                    {
                        lstTotintTables.Add(new DropDownModal() { Name = totIntTable[0], OrigColName = string.Format("{0}.{1}", item.Schema, totIntTable[0]) });
                    }
                }

                ddlTotintTables.DataSource = lstTotintTables;
                ddlTotintTables.DataTextField = "Name";
                ddlTotintTables.DataValueField = "OrigColName";
                ddlTotintTables.DataBind();
                ddlTotintTables.SelectedValue = string.Format("{0}.{1}", schema, tableName);
                // Totint Dropdown Code end here
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                }

                foreach (DataColumn item in dt.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }

                    }

                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }


        public void BindGrid(string schema, string tableName)
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                sortExp = " loaded_datetime desc";
            }
            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
            }
            hdfDataKeyName.Value = dataKeyname;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                int pageIndex = 0;
                if (!string.IsNullOrEmpty(txtDateFrom.Text))
                {
                    where += " and loaded_datetime >='" + txtDateFrom.Text + " 00:00:00' ";
                }
                if (!string.IsNullOrEmpty(txtDateTo.Text))
                {
                    where += " and loaded_datetime <='" + txtDateTo.Text + " 23:59:59' ";
                }
                if (!string.IsNullOrEmpty(sortExp))
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text);
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' {8} ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1} {6}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }

                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text);
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%' {7}) as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }



            gvDynamic.Columns.Clear();

            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            commandField.ShowEditButton = true;
            gvDynamic.Columns.Add(commandField);
            List<string> lstColumns = includeColumns.Split(',').ToList();

            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 10)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        foreach (DataColumn item in dt.Columns)
                        {
                            string colName = item.ColumnName;
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                            {

                                string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                //if (columnInfo == "Child First Name" || columnInfo == "Baby First Name" || columnInfo == "Child Last Name" || columnInfo == "Baby Last Name")
                                //{
                                //    CommandField cf = new CommandField();
                                //    cf.ButtonType = ButtonType.Link;
                                //    cf.HeaderText = columnInfo;
                                //    //cf.DeleteText = item.Name;
                                //    cf.ShowEditButton = true;
                                //    gvDynamic.Columns.Add(cf);
                                //    break;
                                //}
                                //else
                                //{
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = item.ColumnName;
                                    field.SortExpression = item.ColumnName;
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                //}
                                //BoundField field = new BoundField();
                                //field.HeaderText = columnInfo;
                                //field.DataField = item.ColumnName;
                                //field.SortExpression = item.ColumnName;
                                //if (readonlyColumns.Contains(colName))
                                //{
                                //    field.ReadOnly = true;
                                //}
                                //gvDynamic.Columns.Add(field);
                                //break;
                            }
                        }
                    }
                }
            }

            //CommandField cf = new CommandField();
            //cf.ButtonType = ButtonType.Link;
            //cf.DeleteText = "View";
            //cf.ShowDeleteButton = true;
            //gvDynamic.Columns.Add(cf);

            //if (schema != "MBDR_UI_Archive")
            //{
            //    CommandField edit = new CommandField();
            //    edit.ButtonType = ButtonType.Link;
            //    edit.EditText = "Edit";
            //    edit.ShowEditButton = true;
            //    gvDynamic.Columns.Add(edit);
            //}


            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim()
    };
            gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
            gvDynamic.DataSource = dt;
            gvDynamic.DataBind();
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";

                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    query = string.Format("select count({0}) from {1}.{2} where {3} like '%{4}%'", datakeyName, schema, tableName, Convert.ToString(ddlColumn.SelectedValue), Common.Common.ReplaceSQLChar(txtSearch.Text));
                }
                else
                {
                    query = string.Format("select count({0}) from {1}.{2}", datakeyName, schema, tableName);
                }

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                ViewState["PageIndex"] = gvDynamic.PageIndex;
            }
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDynamic.EditIndex = -1;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();

            string dataKeyname = string.Empty;
            if (model != null)
            {
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
            }

            Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value);

        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDynamic.EditIndex = e.NewEditIndex;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;

                if (model != null)
                {
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";").ToLower();
                    dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";");
                }
                string query = " set ";
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    if (cell.ContainingField is BoundField)
                    {
                        string colName = ((BoundField)cell.ContainingField).DataField;
                        if (!readonlyColumns.Contains(colName) && colName != "id")
                        {
                            query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                        }
                    }
                }
                gvDynamic.EditIndex = -1;
                conn.Open();
                string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                SqlCommand cmd = new SqlCommand(finalQuery, conn);
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
                conn.Close();
                BindGrid(lblSchema.Text, lblTable.Text);
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC"; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid(lblSchema.Text, lblTable.Text);
            //gvDynamic.Focus();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            txtDateFrom.Text = string.Empty;
            txtDateTo.Text = string.Empty;
            ddlColumn.SelectedIndex = 0;
            ViewState["SortExpression"] = null;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void ddlTotintTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = Convert.ToString(ddlTotintTables.SelectedValue);

            if (!string.IsNullOrEmpty(value))
            {
                string[] tableInfo = value.Split('.');
                string schema = tableInfo[0];
                string tableName = tableInfo[1];
                string name = ddlTotintTables.SelectedItem.Text;
                Response.Redirect("DefaultTotintView.aspx?schema=" + schema + "&table=" + tableName + "&name=" + name + "");
            }

        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var row = (DataRowView)e.Row.DataItem;
                int index = -1;
                LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                field.Text = "View";
                field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                //foreach (DataControlField cell in gvDynamic.Columns)
                //{
                //    index = -1;
                //    if (cell.HeaderText.ToLower() == "baby first name" || cell.HeaderText.ToLower() == "child first name")
                //    {
                //        index = gvDynamic.Columns.IndexOf(cell);
                //        LinkButton field = e.Row.Cells[index].Controls[0] as LinkButton;
                //        if (cell.HeaderText.ToLower() == "baby first name")
                //        {
                //            field.Text = Convert.ToString(row["baby_first_name"]);
                //        }
                //        else
                //        {
                //            field.Text = Convert.ToString(row["child_first_name"]);
                //        }
                //        //field.Text = row.BabyFirstName;
                //        field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value); //row[e.Row.RowIndex].ToString();

                //    }
                //    else if (cell.HeaderText.ToLower() == "baby last name" || cell.HeaderText.ToLower() == "child last name")
                //    {
                //        index = gvDynamic.Columns.IndexOf(cell);
                //        LinkButton field = e.Row.Cells[index].Controls[0] as LinkButton;
                //        if (cell.HeaderText.ToLower() == "baby last name")
                //        {
                //            field.Text = Convert.ToString(row["baby_last_name"]);
                //        }
                //        else
                //        {
                //            field.Text = Convert.ToString(row["child_last_name"]);
                //        }
                //        //field.Text = row.BabyFirstName;
                //        field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                //    }
                //}
            }
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text);
                string columnName = ddlColumn.SelectedValue;
                Response.Redirect("DefaultDataViewMatching.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + "&searchText=" + searchText + "&colName=" + columnName + "&fromDate=" + txtDateFrom.Text + "&toDate=" + txtDateTo.Text + "&facility=" + ddlTotintTables.SelectedValue + "");
            }
        }
    }
}