﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Net.Sockets;
using System.Text;
using System.Web.UI;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Web;
using Newtonsoft.Json;
using System.Collections;

public partial class MatchingViewDetails : System.Web.UI.Page
{
    protected DataTable dt;
    protected DataTable dtAllRecords;
    protected string getMsgs;
    protected string assignedUserName;
    protected string isOwnerID;
    protected string paramValue;
    private string[] fetchRequireIDs;
    public string iFrameSrc { get; set; }
    dynamic permissions = null;
    private object convert;


    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        permissions = CheckRole();

        try
        {
            if (!Page.IsPostBack)
            {
                string refreshOne = !string.IsNullOrEmpty(Request.QueryString["refresh"]) ? Convert.ToString(Request.QueryString["refresh"]) : "";
                string flagSetRP = !string.IsNullOrEmpty(Request.QueryString["returnpage"]) ? Convert.ToString(Request.QueryString["returnpage"]) : "";
                string newSidFirst = !string.IsNullOrEmpty(Request.QueryString["newSidFirst"]) ? Convert.ToString(Request.QueryString["newSidFirst"]) : "";
                if (Session["paramValue"] == null && refreshOne == "true")
                {
                    RefreshCommand();
                }
                else if (newSidFirst == "true")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Success", "successDiagBox();", true);
                }

                int scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"])) : 0;
                hdfScrutinizerID.Value = Convert.ToString(scrutinizerId);


                if (Convert.ToInt32(Session["paramValue"]) == scrutinizerId)
                {
                    int archiveId = Convert.ToInt32(Convert.ToString(Request.QueryString["archiveId"]));
                    string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                    string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                    string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                    string compareTableName = !string.IsNullOrEmpty(Request.QueryString["CompTable"]) ? Convert.ToString(Request.QueryString["CompTable"]) : "CaseReports";
                    string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

                    string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                    string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

                    string fromDateDOB = !string.IsNullOrEmpty(Request.QueryString["fromDateDOB"]) ? Convert.ToString(Request.QueryString["fromDateDOB"]) : "";
                    string toDateDOB = !string.IsNullOrEmpty(Request.QueryString["toDateDOB"]) ? Convert.ToString(Request.QueryString["toDateDOB"]) : "";


                    hdfMultipleTableName.Value = tableName;
                    hdfSchema.Value = schema;
                    hdfTableName.Value = tableName;
                    hdfDataKeyName.Value = keyName;
                    hdfTableKeyName.Value = "Case Reports";
                    hdfFromDate.Value = fromDate;
                    hdfToDate.Value = toDate;

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                    {
                        scrutinizerId = Convert.ToInt32(Session["paramValue"]);
                    }

                    DataSet dsRows = new DataSet();
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string query = "";
                        string where = "where scrutinizerId=" + scrutinizerId + "";
                        query = string.Format("select top 1 * from {0}.{1} {2}", schema, tableName, where);
                        SqlCommand cmd = new SqlCommand(query, conn);
                        getMsgs = $"M&LDetails|Page_Load-1|{query}";
                        ErrorLog(getMsgs);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dsRows);
                        foreach (DataRow dr in dsRows.Tables[0].Rows)
                        {
                            hdfArchiveID.Value = Convert.ToString(dr["archiveId"]);
                            hdfLastName.Value = Convert.ToString(dr["childLastName"]);
                            hdfFirstName.Value = Convert.ToString(dr["childFirstName"]);
                            hdfDOB.Value = Convert.ToString(dr["birthDate"]);
                            hdfMomLastName.Value = Convert.ToString(dr["momLastName"]);
                            hdfMomFirstName.Value = Convert.ToString(dr["momFirstName"]);
                            hdfAddress1.Value = Convert.ToString(dr["address1"]);
                        }
                    }
                    hdfRowID.Value = scrutinizerId.ToString();
                    hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());

                    var _list = new List<MatchingViewModel>();

                    _list = ListRecords(dsRows, false);
                    string includeColumns = string.Empty;
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;
                    var row = _list.Where(s => s.ScrutinizerID == scrutinizerId).FirstOrDefault();


                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        DataTable dataTable = new DataTable();

                        string query = "";
                        string reqColName = "scrutinizerId";

                        string where = getWhereClause();

                        query = string.Format("SELECT {4} FROM (SELECT ROW_NUMBER() OVER (PARTITION BY scrutinizerId ORDER BY {3}) AS Row,* FROM {0}.{1} {2}) AS RESULT WHERE ROW = 1 ORDER BY {3}", schema, tableName, where, "scrutinizerId desc", reqColName);
                        SqlCommand cmd = new SqlCommand(query, conn);
                        getMsgs = $"M&LDetails|Page_Load-2|{query}";
                        ErrorLog(getMsgs);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dataTable);

                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            if (hdfRowID.Value == dataTable.Rows[i].ItemArray[0].ToString())
                            {

                                if (i != 0 && !string.IsNullOrEmpty(dataTable.Rows[i - 1].ItemArray[0].ToString()))
                                {
                                    hdfPreviousRowID.Value = dataTable.Rows[i - 1].ItemArray[0].ToString();

                                    btnPrev.Enabled = true;
                                    btnPrev.CssClass = "btn btn-success";
                                }
                                else
                                {
                                    btnPrev.Enabled = false;
                                    btnPrev.CssClass = "btn btn-disabled";
                                }
                                if (dataTable.Rows.Count > i + 1 && !string.IsNullOrEmpty(dataTable.Rows[i + 1].ItemArray[0].ToString()))
                                {
                                    hdfNextRowID.Value = dataTable.Rows[i + 1].ItemArray[0].ToString();
                                    btnNext.Enabled = true;
                                    btnNext.CssClass = "btn btn-success";
                                }
                                else
                                {
                                    btnNext.Enabled = false;
                                    btnNext.CssClass = "btn btn-disabled";
                                }

                                break;
                            }
                        }
                    }


                    List<ColumnList> lstRecord = new List<ColumnList>();

                    if (row != null)
                    {
                        var filter = Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                        if (filter != null)
                        {
                            includeColumns = filter.IncludedColumns;
                            readonlyColumns = filter.ReadonlyColumns;
                            dataKeyname = filter.DataKeyName;
                        }

                        List<string> lstColumns = includeColumns.Split(',').ToList();

                        List<MatchingViewModel> lstMatches = new List<MatchingViewModel>();
                        List<MatchingViewModel> lstMatchesRecords = new List<MatchingViewModel>();

                        dt = new DataTable();
                        ltrName.Text = string.Format("{0} {1}", row.ChildFirstName, row.ChildLastName);
                        ltrSimilarName.Text = string.Format("{0} {1}", row.ChildFirstName, row.ChildLastName);
                        string query = string.Empty;
                        string where = " where SC.scrutinizerId = " + scrutinizerId + "";
                        if (hdfTableKeyName.Value == "Birth Records")
                        {
                            string className = btnBirthRecords.CssClass;
                            btnBirthRecords.CssClass = className + " active";
                            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                            hdfTableKeyName.Value = "Birth Records";
                            hdfTableName.Value = "BirthRecords";
                            hdfSchema.Value = "Reference";
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC {0} ", where);
                            getMsgs = $"M&LDetails|Page_Load-3|{query}";
                            ErrorLog(getMsgs);
                        }
                        else if (hdfTableKeyName.Value == "Death Records")
                        {
                            string className = btnDeathRecords.CssClass;
                            btnDeathRecords.CssClass = className + " active";
                            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                            hdfTableKeyName.Value = "Death Records";
                            hdfTableName.Value = "DeathRecords";
                            hdfSchema.Value = "Reference";
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC {0}", where);
                            getMsgs = $"M&LDetails|Page_Load-4|{query}";
                            ErrorLog(getMsgs);
                        }
                        else
                        {
                            string className = btnCaseReports.CssClass;
                            btnCaseReports.CssClass = className + " active";
                            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                            hdfTableKeyName.Value = "Case Reports";
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC {0}", where);
                            getMsgs = $"M&LDetails|Page_Load-5|{query}";
                            ErrorLog(getMsgs);
                        }


                        lstMatches = _list; //matching link view
                        lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);

                        if (lstMatchesRecords.Count == 0)
                        {
                            hdfTableKeyName.Value = "Birth Records";

                            string className = btnBirthRecords.CssClass;
                            btnBirthRecords.CssClass = className + " active";
                            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                            hdfTableKeyName.Value = "Birth Records";
                            hdfTableName.Value = "BirthRecords";
                            hdfSchema.Value = "Reference";
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC {0} ", where);
                            getMsgs = $"M&LDetails|Page_Load-6|{query}";
                            ErrorLog(getMsgs);
                            lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);


                            if (lstMatchesRecords.Count == 0)
                            {
                                hdfTableKeyName.Value = "Death Records";
                                className = btnDeathRecords.CssClass;
                                btnDeathRecords.CssClass = className + " active";
                                btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                                btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                                hdfTableKeyName.Value = "Death Records";
                                hdfTableName.Value = "DeathRecords";
                                hdfSchema.Value = "Reference";
                                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC {0}", where);
                                getMsgs = $"M&LDetails|Page_Load-7|{query}";
                                ErrorLog(getMsgs);
                                lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);
                                if (lstMatchesRecords.Count > 0)
                                {
                                    hdfIsTabClicked.Value = "true";
                                    hdfIsTabClickedDR.Value = "true";
                                }
                            }
                            else
                            {
                                hdfIsTabClicked.Value = "true";
                                hdfIsTabClickedBR.Value = "true";
                            }
                        }
                        else
                        {
                            hdfIsTabClicked.Value = "true";
                            hdfIsTabClickedCR.Value = "true";
                        }


                        //ScrutinizerClearViewCaseReports /ScrutinizerClearView_DeathRecords /ScrutinizerClearView_BirthRecords
                        lstMatches.AddRange(lstMatchesRecords);
                        PropertyInfo[] properties = typeof(MatchingViewModel).GetProperties();

                        for (int i = 0; i < lstMatches.Count + 1; i++)
                        {
                            dt.Columns.Add();
                        }
                        // Binding data

                        for (int i = 0; i < properties.Length; i++)
                        {
                            DataRow dr = dt.NewRow();
                            var colName = lstColumns.Find(s => s.StartsWith(properties[i].Name));
                            
                            string displayColname = string.Empty;

                            if (!string.IsNullOrEmpty(colName))
                            {
                                string columnNames = colName.Split(':')[1];
                                dr[0] = columnNames;
                                displayColname = columnNames;
                            }
                            else
                            {
                                displayColname = properties[i].Name;
                                foreach (var col in lstColumns)
                                {
                                    string columnNames = col.Contains("->") ? col.Split('>')[1] : col;
                                    if (string.Equals(columnNames.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        dr[0] = columnNames.Split(':')[1];
                                    }
                                }
                            }

                            dr[1] = Convert.ToString(properties[i].GetValue(row, null));
                            for (int j = 0; j < lstMatches.Count; j++)
                            {
                                dr[j + 1] = Convert.ToString(properties[i].GetValue(lstMatches[j], null));
                            }

                            dt.Rows.Add(dr);
                        }
                    }
                    if (dt != null)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = "IsMatchRecord";
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                {
                                    dr1[i] = "Check for Linking Process";
                                }
                            }
                        }
                        dt.Rows.Add(dr1);
                        if (dt.Columns.Count > 2)
                        {
                            for (int i = 2; i < dt.Columns.Count; i++)
                            {
                                if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                                {
                                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                }
                            }
                        }
                    }


                    Session["datarecords"] = dt;

                    //Check if the data is avaiable or not from 
                    DataTable dtSc = new DataTable();
                    Session["scrutinizerdatarecords"] = null;

                    dtSc = CheckscrutinizerIdValue();
                    Session["scrutinizerdatarecords"] = dtSc;

                    foreach (DataRow dr in dtSc.Rows)
                    {
                        if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                                {
                                    if (hdfTableKeyName.Value != "Birth Records")
                                    {
                                        string className = btnBirthRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnBirthRecords.CssClass = className + " activeGreen";
                                        btnBirthRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnBirthRecords.CssClass;

                                    if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnBirthRecords.CssClass = className + " btn-disabled";
                                        btnBirthRecords.Enabled = false;
                                    }
                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                                {
                                    if (hdfTableKeyName.Value != "Death Records")
                                    {
                                        string className = btnDeathRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnDeathRecords.CssClass = className + " activeGreen";
                                        btnDeathRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnDeathRecords.CssClass;
                                    if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnDeathRecords.CssClass = className + " btn-disabled";
                                        btnDeathRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                        else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                                {
                                    if (hdfTableKeyName.Value != "Case Reports")
                                    {
                                        string className = btnCaseReports.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnCaseReports.CssClass = className + " activeGreen";
                                        btnCaseReports.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnCaseReports.CssClass;
                                    if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnCaseReports.CssClass = className + " btn-disabled";
                                        btnCaseReports.Enabled = false;
                                    }
                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                                {
                                    if (hdfTableKeyName.Value != "Death Records")
                                    {
                                        string className = btnDeathRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnDeathRecords.CssClass = className + " activeGreen";
                                        btnDeathRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnDeathRecords.CssClass;
                                    if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnDeathRecords.CssClass = className + " btn-disabled";
                                        btnDeathRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                        else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                                {
                                    if (hdfTableKeyName.Value != "Case Reports")
                                    {
                                        string className = btnCaseReports.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnCaseReports.CssClass = className + " activeGreen";
                                        btnCaseReports.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnCaseReports.CssClass;
                                    if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnCaseReports.CssClass = className + " btn-disabled";
                                        btnCaseReports.Enabled = false;
                                    }

                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                                {
                                    if (hdfTableKeyName.Value != "Birth Records")
                                    {
                                        string className = btnBirthRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnBirthRecords.CssClass = className + " activeGreen";
                                        btnBirthRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnBirthRecords.CssClass;
                                    if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnBirthRecords.CssClass = className + " btn-disabled";
                                        btnBirthRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                    }

                    //Similar data green check enable

                    DataTable dtSimilar = new DataTable();
                    Session["scrutinizersimilardatarecords"] = null;
                    
                    dtSimilar = CheckSimilarCandidates();
                    Session["scrutinizersimilardatarecords"] = dtSimilar;

                    if (dtSimilar != null && dtSimilar.Rows.Count > 0)
                    {
                        string className = btnSimilar.CssClass;
                        if (className.Contains("btn-disabled"))
                        {
                            className = className.Replace("btn-disabled", "");
                        }
                        btnSimilar.CssClass = className + " activeGreen";
                        btnSimilar.Enabled = true;
                        if (hdfIsTabClickedSM.Value != "true")
                        {
                            hdfIsTabClicked.Value = "false";
                        }
                    }
                    else
                    {
                        string className = btnSimilar.CssClass;
                        if (!btnSimilar.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                        {
                            btnSimilar.CssClass = className + " btn-disabled";
                            btnSimilar.Enabled = false;
                        }
                    }
                }
                else
                {
                    dt = (DataTable)Session["datarecords"];
                }

                isOwnerID = IsOwnerShip();

                string env = ConfigurationManager.AppSettings["environment"];

                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                if (!isOwnerID.Equals(Session["userid"]))
                {
                    // if Session["ErrorOccurred"] == null, it is giving null, so converting to false.

                    if (Session["ErrorOccurred"] == null)
                    {
                        Session["ErrorOccurred"] = false;
                    }

                    if ((bool)Session["ErrorOccurred"] == false)
                    {
                        if (!string.IsNullOrEmpty(displayName) && Convert.ToString(displayName) != "UnAssignedOwner" && (env != "qa" && env != "sqa"))
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Alert User", "cAlertMsgs('Warning: You do not own this candidate and cannot perform matching and linking.', 'Not Owner');", true);
                        }
                    }

                    btnRelease.Enabled = false;
                    btnLink.Enabled = false;
                    btnAssign.Enabled = false;
                    btnPreviousMatch.Enabled = false;
                    btnNextMatch.Enabled = false;

                    btnRelease.CssClass = "enableGrey";
                    btnLink.CssClass = "enableGrey";
                    btnAssign.CssClass = "enableGrey";
                    btnPreviousMatch.CssClass = "enableGrey";
                    btnNextMatch.CssClass = "enableGrey";
                    hdfOwner.Value = "false";

                    if (permissions.Count > 0)
                    {
                        string miMiLogin = string.Empty;
                        if (!string.IsNullOrEmpty(env))
                        {
                            if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                            {
                                if (string.IsNullOrEmpty(isOwnerID))
                                {
                                    btnOwn.Enabled = true;
                                    btnOwn.CssClass = "disableGrey";
                                }
                                else
                                {
                                    btnOwn.Enabled = false;
                                    btnOwn.CssClass = "enableGrey";
                                }
                            }
                            else if (env == "prod")
                            {
                                btnOwn.Enabled = false;
                                btnOwn.CssClass = "enableGrey";
                            }
                            else
                            {
                                btnOwn.Enabled = true;
                                btnOwn.CssClass = "disableGrey";
                            }
                        }
                    }
                    else
                    {
                        btnOwn.Enabled = false;
                        btnOwn.CssClass = "enableGrey";
                    }
                }
                else
                {
                    btnOwn.Enabled = false;
                    btnRelease.Enabled = true;
                    btnOwn.OnClientClick = null;
                    btnLink.Enabled = true;
                    btnAssign.Enabled = true;
                    btnPreviousMatch.Enabled = false;  // will need to change true both prev and next 
                    btnNextMatch.Enabled = false;

                    btnOwn.CssClass = "enableGrey";
                    btnRelease.CssClass = "disableGrey";
                    btnLink.CssClass = "disableGrey";
                    btnAssign.CssClass = "disableGrey";
                    btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
                    btnNextMatch.CssClass = "enableGrey";
                    hdfOwner.Value = "true";
                }
                OwnerShip();

                if (Session["ErrorOccurred"] != null && (bool)Session["ErrorOccurred"] == true)
                {
                    // Clear the error flag to allow normal processing on future requests

                    Session["Erroroccurred"] = false;

                    return;
                }
            }
            else
            {
                dt = (DataTable)Session["datarecords"];
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_Page_load");
        }
        // check if dt is null value and security check returning false.
        if (dt == null && hdfSecurityCheck.Value != "sv")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Data table is null", "dtNullError();", true);
        }

        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    public dynamic CheckRole()
    {
        dynamic permissions = null;

        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

            if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
            {
                permissions = dtAccessPermission.AsEnumerable()
                .Where(permission => permission.Field<string>("PermissionName") == "ML_WRITE")
                .Select(permission => new
                {
                    UserId = permission.Field<string>("userId"),
                    RoleId = permission.Field<string>("RoleId"),
                    RoleName = permission.Field<string>("RoleName"),
                    RoleDetails = permission.Field<string>("RoleDetails"),
                    PermissionId = permission.Field<string>("PermissionId"),
                    PermissionName = permission.Field<string>("PermissionName")
                })
                .ToList();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CheckRole");
        }
        return permissions;
    }

    protected void RefreshCommand()
    {
        try
        {
            string json = string.Empty;
            int scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"])) : 0;
            json = "{\"cmd\":\"REFRESH\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + scrutinizerId + "}";

            string response = SocketConn(json);
            getMsgs = $"RefreshCommand|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);

                Session["paramValue"] = null;
                Session["paramValue"] = scrutinizerId;

                Session["ErrorOccurred"] = true;
            }
            else
            {
                dynamic data = JObject.Parse(response);

                if (data["name"] != null && data["name"].ToString() == "END_OF_WORK")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "End Of Work", "endOfWOrk();", true);

                    string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                    string whereAssign = getQueryString();
                    string sortExpre = "";

                    if (string.IsNullOrEmpty(sortExp))
                    {
                        sortExpre = "scrutinizerId desc";
                    }
                    else
                    {
                        sortExpre = sortExp;
                    }

                    string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                    string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=endOfWork&sortExp=" + sortExpre + "&where=" + whereAssign;

                    if (!string.IsNullOrEmpty(recordId))
                    {
                        redirectUrl += "&scrutinizerId=" + recordId;
                    }
                    Session["paramValue"] = null;
                    Session.Remove("paramValue");

                    Response.Redirect(redirectUrl, false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else if (data["name"] != null && data["name"].ToString() == "SECURITY")
                {
                    hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true

                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);

                    return;
                }
                else if (data["name"] != null && data["name"].ToString() == "SUCCESS")
                {
                    if (data["param"] != null)
                    {
                        paramValue = Uri.EscapeDataString(Convert.ToString(data["param"]));
                        Session["paramValue"] = null;
                        Session["paramValue"] = paramValue;

                        string redirectUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                        UriBuilder uribuilder = new UriBuilder(redirectUrl);
                        var query = HttpUtility.ParseQueryString(uribuilder.Query);
                        query["scrutinizerId"] = paramValue;
                        query["refresh"] = "false";
                        uribuilder.Query = query.ToString();
                        string modifyUrl = uribuilder.Uri.ToString();

                        if (Convert.ToInt32(paramValue) != scrutinizerId)
                        {
                            modifyUrl += "&newSidFirst=true";
                            Response.Redirect(modifyUrl, false);
                        }
                        else
                        {
                            Session["paramValue"] = null;
                            Session.Remove("paramValue");
                            Session["paramValue"] = scrutinizerId;
                            string message = ResponseMessage(Convert.ToString(data.name));
                            lblSockerResponse.Text = null;
                            lblSockerResponse.Text = message;
                        }
                    }
                    else
                    {
                        Session["paramValue"] = null;
                        Session.Remove("paramValue");
                        Session["paramValue"] = scrutinizerId;
                        string message = ResponseMessage(Convert.ToString(data.name));
                        lblSockerResponse.Text = null;
                        lblSockerResponse.Text = message;
                    }
                }
                else
                {
                    Session["paramValue"] = null;
                    Session.Remove("paramValue");
                    Session["paramValue"] = scrutinizerId;
                    string message = ResponseMessage(Convert.ToString(data.name));
                    lblSockerResponse.Text = null;
                    lblSockerResponse.Text = message;

                    if (data["name"] != null && (Convert.ToString(data["name"]) == "EXCEPTION" || data["status"].ToString() == "13"))
                    {
                        // Set an error flag in the session 

                        Session["ErrorOccurred"] = true;

                        string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + scrutinizerId;

                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Refresh Server Error Message');", true);

                        return;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_RefreshCommand");
        }
    }

    private List<MatchingViewModel> GetAllDetails(string schema, string tableName, int? scrutinizerId, string query)
    {
        List<GridViewModel> lstFilter = Common.GetFilterList();
        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        if (model != null)
        {
            includeColumns = model.IncludedColumns;
            readonlyColumns = model.ReadonlyColumns;
            dataKeyname = model.DataKeyName;
        }
        DataSet dsRows = new DataSet();
        var lstMatches = new List<MatchingViewModel>();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            getMsgs = $"M&LDetails|GetAllDetails|{query}";
            ErrorLog(getMsgs);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dsRows);
            string tableRecName = string.Empty;
            lstMatches = ListRecords(dsRows, true);
        }
        return lstMatches;
    }

    protected void btnAdvanceResult_Click(object sender, EventArgs e)
    {
        try
        {
            dt = null;
            dt = (DataTable)Session["datarecords"];
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));
            // condition if unassigned cr by new owner.

            Response.Redirect("MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching and Linking" + hdfDataKeyName.Value + "&scrutinizerId=" + recordId + "&sortExp=" + sortExpre + "&where=" + where + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingLinkingViewDetails_btnAdvanceResult_Click");
        }
    }

    protected void btnAll_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching and Linking", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnAll_Click");
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }
            int recordId = Convert.ToInt32(Uri.EscapeDataString(hdfPreviousRowID.Value));
            hdfTableName.Value = "Matching_Linking";
            hdfSchema.Value = "MBDR_System";
            Session["paramValue"] = null;
            Session["paramValue"] = recordId;

            Response.Redirect("MatchingViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&scrutinizerId=" + recordId + "&sortExp=" + sortExpre + "&where=" + where + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnPrev_Click");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
            string where = getQueryString();

            string sortExpre = "";
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(hdfNextRowID.Value));

            hdfTableName.Value = "Matching_Linking";
            hdfSchema.Value = "MBDR_System";

            Session["paramValue"] = null;
            
            Response.Redirect("MatchingViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&scrutinizerId=" + recordId + "&sortExp=" + sortExpre + "&where=" + where + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnNext_Click");
        }
    }

    protected void btnLink_Click(object sender, EventArgs e)
    {
        try
        {
            isOwnerID = IsOwnerShip();

            if (!isOwnerID.Equals(Session["userid"]))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate.', 'Error Message');", true);
            }
            else
            {
                string details = string.Empty;
                bool clearView = true;
                bool isMatch = true;
                string query = string.Empty;
                string detailId = string.Empty;
                string reportId = string.Empty;
                string birthId = string.Empty;
                string deathId = string.Empty;
                DataTable dt = new DataTable();
                detailId = hdfDetailsID.Value;  // Request.Form["chkname"];
                DataTable dtScruClearView = new DataTable();

                // checking DID for Case report, Birth Record, Death Record

                if (hdfTableKeyName.Value == "Birth Records")
                {
                    BirthRecordLinkMatchingIds();

                    string btnCaseReportCSS = btnCaseReports.CssClass;
                    if (!btnCaseReportCSS.Contains("btn-disabled"))
                    {
                        CaseReportLinkMatchingIds();
                    }
                    string btnDeathRecordCSS = btnDeathRecords.CssClass;
                    if (!btnDeathRecordCSS.Contains("btn-disabled"))
                    {
                        DeathRecordLinkMatchingIds();
                    }
                    detailId = string.Empty;
                    detailId = hdfDetailsID.Value;
                }

                else if (hdfTableKeyName.Value == "Death Records")
                {
                    DeathRecordLinkMatchingIds();

                    string btnCaseReportCSS = btnCaseReports.CssClass;
                    if (!btnCaseReportCSS.Contains("btn-disabled"))
                    {
                        CaseReportLinkMatchingIds();
                    }
                    string btnBirthRecordCSS = btnBirthRecords.CssClass;
                    if (!btnBirthRecordCSS.Contains("btn-disabled"))
                    {
                        BirthRecordLinkMatchingIds();
                    }
                    detailId = string.Empty;
                    detailId = hdfDetailsID.Value;
                }

                else
                {
                    if (!hdfDetailsID.Value.Contains(","))
                    {
                        CaseReportLinkMatchingIds();
                    }
                    string btnBirthRecordCSS = btnBirthRecords.CssClass;
                    if (!btnBirthRecordCSS.Contains("btn-disabled"))
                    {
                        BirthRecordLinkMatchingIds();
                    }
                    string btnDeathRecordCSS = btnDeathRecords.CssClass;
                    if (!btnDeathRecordCSS.Contains("btn-disabled"))
                    {
                        DeathRecordLinkMatchingIds();
                    }

                    detailId = string.Empty;
                    detailId = hdfDetailsID.Value;
                }

                if (!string.IsNullOrEmpty(detailId) && isMatch)
                {
                    string json = string.Empty;

                    if (!detailId.Contains(hdfisFirstCheckedDetailsID.Value))
                    {
                        detailId = detailId + "," + hdfisFirstCheckedDetailsID.Value;
                    }

                    // remove null value from details ID's
                    detailId = detailId.Replace("null,", "");

                    int firstCommaIndex = detailId.IndexOf(",");

                    // remove empty comma value from details ID's  - detailId = " , value1, value2"

                    if (firstCommaIndex == 0)
                    {
                        detailId = detailId.Remove(firstCommaIndex, 1).TrimStart();
                    }

                    //ignore duplicate DIDs if exist

                    var uniqueDID = detailId.Split(',').Select(int.Parse).Distinct().ToArray();

                    detailId = string.Join(",", uniqueDID);

                    string comment = txtFixTool.Text;
                    comment = JsonConvert.SerializeObject(comment);

                    json = "{\"cmd\":\"SELECT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailId + "],\"similar\":\"" + hdfSimilarCheckUnCheckSIDs.Value + "\",\"clearView\":\"" + clearView + "\",\"comment\":" + comment + "}";

                    if (string.IsNullOrEmpty(json))
                    {
                        lblSockerResponse.Text = string.Empty;
                        OwnerShip();
                        return;
                    }
                    else
                    {
                        string response = SocketConn(json);
                        getMsgs = $"btnLink_Click|Request|{json}|Response|{response}";
                        ErrorLog(getMsgs);
                        hdfSimilarCheckUnCheckSIDs.Value = string.Empty;

                        if (response == "SocketConnectionError")
                        {
                            Session["ErrorOccurred"] = true;

                            ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                            details = "Server unavailable";
                            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                            string sortExpre = "";
                            if (string.IsNullOrEmpty(sortExp))
                            {
                                sortExpre = "scrutinizerId desc";
                            }
                            else
                            {
                                sortExpre = sortExp;
                            }
                            string where = getQueryString();
                            string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                            string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=linkerror&sortExp=" + sortExpre + "&where=" + where;

                            if (!string.IsNullOrEmpty(recordId))
                            {
                                redirectUrl += "&scrutinizerId=" + recordId;
                            }

                            Response.Redirect(redirectUrl, false);

                        }

                        else
                        {
                            dynamic data = JObject.Parse(response);
                            query = string.Empty;
                            if (hdfTableKeyName.Value == "Birth Records")
                            {
                                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                            }
                            else if (hdfTableKeyName.Value == "Death Records")
                            {
                                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                            }
                            else
                            {
                                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
                            }

                            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                            string sortExpre = "";

                            if (string.IsNullOrEmpty(sortExp))
                            {
                                sortExpre = "scrutinizerId desc";
                            }
                            else
                            {
                                sortExpre = sortExp;
                            }
                            string where = getQueryString();

                            int scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"])) : 0;

                            if (data.name == "SUCCESS")
                            {
                                string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                                string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=link&sortExp=" + sortExpre + "&where=" + where;

                                if (!string.IsNullOrEmpty(recordId))
                                {
                                    redirectUrl += "&scrutinizerId=" + recordId;
                                }

                                Response.Redirect(redirectUrl, false);
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                                details = data.details;
                            }
                            else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                            {
                                hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                                ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                                return;
                            }
                            else if (data["name"] != null && (Convert.ToString(data["name"]) == "EXCEPTION" || data["status"].ToString() == "13"))
                            {
                                // Set an error flag in the session 

                                Session["ErrorOccurred"] = true;

                                string RefSid = string.Empty;

                                if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                                {
                                    RefSid = Convert.ToString(Session["paramValue"]);
                                }
                                else
                                {
                                    RefSid = Uri.EscapeDataString(hdfScrutinizerID.Value);
                                }

                                string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + RefSid;

                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Link Server Error Message');", true);

                                OwnerShip();

                                hdfOwner.Value = "true";

                                return;
                            }
                            else
                            {
                                string message = ResponseMessage(Convert.ToString(data.name));
                                json = "{\"cmd\":\"VALIDATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailId + "],\"clearView\":" + clearView + "}";
                                response = SocketConn(json);
                                getMsgs = $"btnLink_Click|Validate|Request|{json}|Response|{response}";
                                ErrorLog(getMsgs);
                                if (response == "SocketConnectionError")
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                                    details = "Server unavailable";
                                    string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                                    string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=link&sortExp=" + sortExpre + "&where=" + where;

                                    if (!string.IsNullOrEmpty(recordId))
                                    {
                                        redirectUrl += "&scrutinizerId=" + recordId;
                                    }

                                    Response.Redirect(redirectUrl, false);
                                }
                                else
                                {
                                    data = JObject.Parse(response);
                                    lblSockerResponse.Text = null;
                                    lblSockerResponse.Text = message;

                                    if (data.name != "SUCCESS")
                                    {
                                        details = data.details;

                                        // Set an error flag in the session 

                                        Session["ErrorOccurred"] = true;

                                        string RefSid = string.Empty;

                                        if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                                        {
                                            RefSid = Convert.ToString(Session["paramValue"]);
                                        }
                                        else
                                        {
                                            RefSid = Uri.EscapeDataString(hdfScrutinizerID.Value);
                                        }

                                        string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + RefSid;

                                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Link Server Error Message');", true);

                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                else if (!string.IsNullOrEmpty(detailId) && isMatch == false)
                {
                    query = string.Empty;
                    string caseDId = hdfCaseDetailsID.Value;

                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                    }
                    else
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
                    }
                    dt = new DataTable();
                    dt = CommonMethod(query);
                    DataRow dr = dt.NewRow();
                    dr[0] = "IsMatchRecord";

                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (string.IsNullOrEmpty(hdfDetailsID.Value))
                            {
                                dr[i] = "Check for Linking Process";
                            }
                            else if (string.IsNullOrEmpty(reportId) && string.IsNullOrEmpty(birthId) && string.IsNullOrEmpty(deathId))
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(reportId) && (dt.Rows[j].ItemArray[i].ToString() == reportId) && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(reportId) && dt.Rows[j].ItemArray[i].ToString() == reportId)
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId)
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId)
                            {
                                dr[i] = "Matched";
                                break;
                            }

                            // Flagged from case reports

                            else if (hdfFirstTable.Value == "BirthRecords")
                            {
                                if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value))
                                {
                                    if (hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == reportId)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if ((!string.IsNullOrEmpty(hdfBirthReportID.Value)) && (!string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfBirthReportID.Value))
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if ((!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfBirthRecordBirthRecNo.Value))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))
                                // death
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedBR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                                // report 
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedBR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }
                            else if (hdfFirstTable.Value == "DeathRecords")
                            {
                                if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value))
                                {
                                    if (hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == reportId)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if ((!string.IsNullOrEmpty(hdfDeathReportID.Value)) && (!string.IsNullOrEmpty(reportId) && (Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfDeathReportID.Value)))
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if ((!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfDeathRecordDeathRecNo.Value)))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))  // report note: Case Rec ID can't be null if report ID exist and vice versa.
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedDR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))  // birth 
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedDR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }

                            // Flagged from birth records

                            else if (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking")
                            {
                                if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value))
                                {
                                    if (hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(birthId) && Convert.ToString(dt.Rows[71].ItemArray[i]) == birthId)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if ((!string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value)) && (!string.IsNullOrEmpty(birthId) && (Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfCaseReportBirthRecNo.Value)))
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if ((!string.IsNullOrEmpty(hdfCaseReportID.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i])) && (Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfCaseReportID.Value)))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))) // birth
                                                                                                                                                                                                                                                                                            // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedCR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))
                                // death 
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedCR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }
                            else if (hdfFirstTable.Value == "DeathRecords")
                            {
                                if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value))
                                {
                                    if (hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || (!string.IsNullOrEmpty(birthId) && Convert.ToString(dt.Rows[71].ItemArray[i]) == birthId))   // check Case ID match or internal ID match 
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if (!string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(birthId) && (Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfDeathRecordBirthRecNo.Value))  // check birth cert number from death source in birth source
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfDeathRecordDeathRecNo.Value))  // check death number from death source in birth source
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                                // birth
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedDR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                                // report 
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedDR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }

                            // Flagged from death records

                            else if (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking")
                            {
                                if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value))
                                {
                                    if (hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[72].ItemArray[i]) == deathId)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfCaseReportDeathRecNo.Value)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportID.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i])) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfCaseReportID.Value)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))  // birth
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedCR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))  // death
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedCR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }
                            else if (hdfFirstTable.Value == "BirthRecords")
                            {
                                if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value))
                                {
                                    if (hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[72].ItemArray[i]) == deathId)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if (!string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfBirthRecordDeathRecNo.Value)
                                    {
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else
                                    {
                                        dr[i] = "Unmatched";
                                        break;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfBirthRecordBirthRecNo.Value)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                                // report
                                // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedBR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))  // death
                                {
                                    dr[i] = "Nonpareil";
                                    string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                    if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                    {
                                        if (hdfisCheckedBR.Value == "true")
                                        {
                                            hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                        }
                                    }
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                }
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                            }
                        }
                    }
                    dt.Rows.Add(dr);

                    Session["datarecords"] = null;
                    Session["datarecords"] = dt;


                    lblSockerResponse.Text = null;

                    isOwnerID = IsOwnerShip();

                    if (!isOwnerID.Equals(Session["userid"]))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate.', 'Error Message');", true);

                        btnRelease.Enabled = false;
                        btnLink.Enabled = false;
                        btnAssign.Enabled = false;
                        btnPreviousMatch.Enabled = false;
                        btnNextMatch.Enabled = false;


                        btnRelease.CssClass = "enableGrey";
                        btnLink.CssClass = "enableGrey";
                        btnAssign.CssClass = "enableGrey";
                        btnPreviousMatch.CssClass = "enableGrey";
                        btnNextMatch.CssClass = "enableGrey";
                        hdfOwner.Value = "false";


                        if (permissions.Count > 0)
                        {
                            string env = ConfigurationManager.AppSettings["environment"];
                            string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                            string miMiLogin = string.Empty;
                            if (!string.IsNullOrEmpty(env))
                            {
                                if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                                {
                                    if (string.IsNullOrEmpty(isOwnerID))
                                    {
                                        btnOwn.Enabled = true;
                                        btnOwn.CssClass = "disableGrey";
                                    }
                                    else
                                    {
                                        btnOwn.Enabled = false;
                                        btnOwn.CssClass = "enableGrey";
                                    }
                                }
                                else if (env == "prod")
                                {
                                    btnOwn.Enabled = false;
                                    btnOwn.CssClass = "enableGrey";
                                }
                                else
                                {
                                    btnOwn.Enabled = true;
                                    btnOwn.CssClass = "disableGrey";
                                }
                            }
                        }
                    }
                    else
                    {
                        btnOwn.Enabled = false;
                        btnRelease.Enabled = true;
                        btnOwn.OnClientClick = null;
                        btnLink.Enabled = true;
                        btnAssign.Enabled = true;
                        btnPreviousMatch.Enabled = false; // will need to change true both prev and next 
                        btnNextMatch.Enabled = false;

                        btnOwn.CssClass = "enableGrey";
                        btnRelease.CssClass = "disableGrey";
                        btnLink.CssClass = "disableGrey";
                        btnAssign.CssClass = "disableGrey";
                        btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
                        btnNextMatch.CssClass = "enableGrey";
                        hdfOwner.Value = "true";
                    }

                    OwnerShip();
                }

                else
                {
                    Session["ErrorOccurred"] = true;

                    string RefSid = string.Empty;

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                    {
                        RefSid = Convert.ToString(Session["paramValue"]);
                    }
                    else
                    {
                        RefSid = Uri.EscapeDataString(hdfScrutinizerID.Value);
                    }

                    string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + RefSid;

                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Link Server Error Message');", true);

                    return;
                }
                
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
            // This exception is expected when using Response.Redirect and can be safely ignored.
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnLink_Click");
        }
    }

    protected void btnAssign_Click(object sender, EventArgs e)
    {
        try
        {
            isOwnerID = IsOwnerShip();

            if (!isOwnerID.Equals(Session["userid"]))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate.', 'Error Message');", true);
            }

            string details = string.Empty;
            bool clearView = true;
            string query = string.Empty;
            string detailids = string.Empty;
            DataTable dtSC = new DataTable();

            string where = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
            {
                where = " where CV.scrutinizerId = " + Convert.ToInt32(Session["paramValue"]) + "";
            }
            else
            {
                string scrutinizerId = Uri.EscapeDataString(hdfScrutinizerID.Value);
                where = " where CV.scrutinizerId = " + scrutinizerId + "";
            }

            query = "SELECT ROW_NUMBER() OVER(ORDER BY CV.detailId) AS Row,* from [MBDR_System].[ScrutinizerClearView] CV ";
            query = string.Format(query + "{0}", where);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LDetails|btnAssign_Click|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtSC);

                if (dtSC != null && dtSC.Rows.Count > 0)
                {
                    clearView = true;
                    foreach (DataRow dr in dtSC.Rows)
                    {
                        if (string.IsNullOrEmpty(dr["scrutinizerKind"].ToString()))
                        {
                            detailids = dr["detailId"].ToString();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(hdfDetailsID.Value))
                {
                    detailids = detailids + "," + hdfDetailsID.Value.Replace("null,", "");
                }

                int firstCommaIndex = detailids.IndexOf(",");

                // remove empty comma value from details ID's  - detailId = " , value1, value2"

                if (firstCommaIndex == 0)
                {
                    detailids = detailids.Remove(firstCommaIndex, 1).TrimStart();
                }

                //ignore duplicate DIDs if exist

                var uniqueDID = detailids.Split(',').Select(int.Parse).Distinct().ToArray();

                detailids = string.Join(",", uniqueDID);

            }
            string json = string.Empty;

            string comment = txtFixTool.Text;
            comment = JsonConvert.SerializeObject(comment);

            json = "{\"cmd\":\"SELECT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailids + "],\"similar\":\"" + hdfSimilarCheckUnCheckSIDs.Value + "\",\"clearView\":\"" + clearView + "\",\"comment\":" + comment + "}";

            if (string.IsNullOrEmpty(json))
            {
                lblSockerResponse.Text = string.Empty;
                OwnerShip();
                return;
            }
            else
            {
                string response = SocketConn(json);
                string getMsgs = $"btnAssign_Click|Request|{json}|Response|{response}";
                ErrorLog(getMsgs);

                hdfSimilarCheckUnCheckSIDs.Value = string.Empty;

                if (response == "SocketConnectionError")
                {

                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                    details = "Server unavailable";

                    string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                    string whereAssign = getQueryString();
                    string sortExpre = "";

                    if (string.IsNullOrEmpty(sortExp))
                    {
                        sortExpre = "scrutinizerId desc";
                    }
                    else
                    {
                        sortExpre = sortExp;
                    }

                    string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                    string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=linkerror&sortExp=" + sortExpre + "&where=" + whereAssign;

                    if (!string.IsNullOrEmpty(recordId))
                    {
                        redirectUrl += "&scrutinizerId=" + recordId;
                    }

                    Response.Redirect(redirectUrl, false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }

                else
                {
                    dynamic data = JObject.Parse(response);
                    query = string.Empty;
                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                    }
                    else
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
                    }

                    if (data.name == "SUCCESS")
                    {
                        string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                        string whereAssign = getQueryString();
                        string sortExpre = "";

                        if (string.IsNullOrEmpty(sortExp))
                        {
                            sortExpre = "scrutinizerId desc";
                        }
                        else
                        {
                            sortExpre = sortExp;
                        }

                        string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                        string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=assign&sortExp=" + sortExpre + "&where=" + whereAssign;

                        if (!string.IsNullOrEmpty(recordId))
                        {
                            redirectUrl += "&scrutinizerId=" + recordId;
                        }

                        Response.Redirect(redirectUrl, false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        details = data.details;

                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                    {
                        hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "EXCEPTION" || data["status"].ToString() == "13"))
                    {
                        // Set an error flag in the session 

                        Session["ErrorOccurred"] = true;

                        string RefSid = string.Empty;

                        if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                        {
                            RefSid = Convert.ToString(Session["paramValue"]);
                        }
                        else
                        {
                            RefSid = Uri.EscapeDataString(hdfScrutinizerID.Value);
                        }

                        string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + RefSid;

                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Create Server Error Message');", true);

                        OwnerShip();

                        hdfOwner.Value = "true";

                        return;
                    }
                    else
                    {
                        string message = ResponseMessage(Convert.ToString(data.name));
                        json = "{\"cmd\":\"VALIDATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailids + "],\"clearView\":" + clearView + "}";
                        response = SocketConn(json);
                        getMsgs = $"btnAssign_Click|Validate|Request|{json}|Response|{response}";
                        ErrorLog(getMsgs);
                        if (response == "SocketConnectionError")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);

                            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                            string whereAssign = getQueryString();
                            string sortExpre = "";

                            if (string.IsNullOrEmpty(sortExp))
                            {
                                sortExpre = "scrutinizerId desc";
                            }
                            else
                            {
                                sortExpre = sortExp;
                            }

                            string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                            string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=linkerror&sortExp=" + sortExpre + "&where=" + whereAssign;

                            if (!string.IsNullOrEmpty(recordId))
                            {
                                redirectUrl += "&scrutinizerId=" + recordId;
                            }

                            Response.Redirect(redirectUrl, false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }
                        else
                        {
                            data = JObject.Parse(response);
                            details = data.details;
                            lblSockerResponse.Text = null;
                            lblSockerResponse.Text = message;

                            if (data.name != "SUCCESS")
                            {
                                // Set an error flag in the session 

                                Session["ErrorOccurred"] = true;

                                string RefSid = string.Empty;

                                if (!string.IsNullOrEmpty(Convert.ToString(Session["paramValue"])))
                                {
                                    RefSid = Convert.ToString(Session["paramValue"]);
                                }
                                else
                                {
                                    RefSid = Uri.EscapeDataString(hdfScrutinizerID.Value);
                                }

                                string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + RefSid;

                                ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Create Server Error Message');", true);

                                return;
                            }
                        }
                    }
                }
            }
            
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
            // This exception is expected when using Response.Redirect and can be safely ignored.
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnAssign_Click");
        }
    }

    protected void btnOwn_Click(object sender, EventArgs e)
    {
        try
        {
            string details = string.Empty;
            string json = "{\"cmd\":\"TAKE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + "}";
            string response = SocketConn(json);
            getMsgs = $"btnOwn_Click|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            int paramValue = 0;
            if (response == "SocketConnectionError")
            {
                Session["ErrorOccurred"] = true;

                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                details = "Server unavailable";
            }
            else
            {
                dynamic data = JObject.Parse(response);

                if (data.name == "END_OF_WORK")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "End Of Work", "endOfWOrk();", true);

                    string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                    string whereAssign = getQueryString();
                    string sortExpre = "";

                    if (string.IsNullOrEmpty(sortExp))
                    {
                        sortExpre = "scrutinizerId desc";
                    }
                    else
                    {
                        sortExpre = sortExp;
                    }

                    string recordId = Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value));
                    string redirectUrl = "MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=endOfWork&sortExp=" + sortExpre + "&where=" + whereAssign;

                    if (!string.IsNullOrEmpty(recordId))
                    {
                        redirectUrl += "&scrutinizerId=" + recordId;
                    }

                    Response.Redirect(redirectUrl, false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SUCCESS" || data["name"].ToString() == "STOLEN"))
                {

                    if (data["param"] != null)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Success", "successDiagBox();", true);

                        paramValue = Convert.ToInt32(data["param"].Value);
                        Session["paramValue"] = null;
                        Session.Remove("paramValue");
                        Session["paramValue"] = paramValue;
                        string queryParam = string.Empty;
                        btnOwn.Enabled = false;
                        btnRelease.Enabled = true;
                        btnLink.Enabled = true;
                        btnAssign.Enabled = true;
                        btnPreviousMatch.Enabled = false;
                        btnNextMatch.Enabled = false;

                        btnOwn.CssClass = "enableGrey";
                        btnRelease.CssClass = "disableGrey";
                        btnLink.CssClass = "disableGrey";
                        btnAssign.CssClass = "disableGrey";
                        btnPreviousMatch.CssClass = "enableGrey";
                        btnNextMatch.CssClass = "enableGrey";

                        //OwnerShip for unassigned owner

                        if (hdfTableKeyName.Value == "Birth Records")
                        {
                            queryParam = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                        }
                        else if (hdfTableKeyName.Value == "Death Records")
                        {
                            queryParam = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                        }
                        else
                        {
                            queryParam = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
                        }
                        dt = new DataTable();
                        DataTable dt2 = new DataTable();
                        dt2 = CommonMethod(queryParam, paramValue);
                        dt = AddMatched(dt2);
                        NewOwnerShip(Convert.ToString(paramValue));
                        hdfOwner.Value = "true";
                        details = data.details;


                        string redirectUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                        UriBuilder uribuilder = new UriBuilder(redirectUrl);
                        var query1 = HttpUtility.ParseQueryString(uribuilder.Query);
                        query1["scrutinizerId"] = Convert.ToString(paramValue);
                        query1["refresh"] = "true";

                        uribuilder.Query = query1.ToString();
                        string modifyUrl = uribuilder.Uri.ToString();
                        int scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"])) : 0;
                        string newSidFirst = !string.IsNullOrEmpty(Request.QueryString["newSidFirst"]) ? Convert.ToString(Request.QueryString["newSidFirst"]) : "";

                        if (Convert.ToInt32(paramValue) != scrutinizerId)
                        {
                            modifyUrl += "&newSidFirst=true";
                            Response.Redirect(modifyUrl, false);
                        }
                        else
                        {
                            Response.Redirect(modifyUrl, false);
                        }
                    }
                    else
                    {
                        string query = string.Empty;
                        btnOwn.Enabled = false;
                        btnRelease.Enabled = true;
                        btnLink.Enabled = true;
                        btnAssign.Enabled = true;
                        btnPreviousMatch.Enabled = false;  // will need to change true both prev and next 
                        btnNextMatch.Enabled = false;

                        btnOwn.CssClass = "enableGrey";
                        btnRelease.CssClass = "disableGrey";
                        btnLink.CssClass = "disableGrey";
                        btnAssign.CssClass = "disableGrey";
                        btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
                        btnNextMatch.CssClass = "enableGrey";

                        if (hdfTableKeyName.Value == "Birth Records")
                        {
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                        }
                        else if (hdfTableKeyName.Value == "Death Records")
                        {
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                        }
                        else
                        {
                            query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
                        }
                        dt = new DataTable();
                        DataTable dt2 = new DataTable();
                        dt2 = CommonMethod(query);
                        dt = AddMatched(dt2);
                        OwnerShip();
                        hdfOwner.Value = "true";
                        details = data.details;
                    }
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "EXCEPTION" || data["status"].ToString() == "13"))
                {
                    // Set an error flag in the session 

                    Session["ErrorOccurred"] = true;

                    string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference SID # " + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value));

                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Refresh Server Error Message');", true);

                    OwnerShip();

                    hdfOwner.Value = "true";
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "BAD_STATE" && data["status"].ToString() == "11"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "NOT_OWNER" && data["status"].ToString() == "9"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error", "cAlertMsgs('This candidate is owned by another user. Please click OK to continue to view this candidate case report.', 'Not Owner');", true);
                    OwnerShip();
                    hdfOwner.Value = "true";
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                {
                    hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                    return;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                }
            }
           
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnOwn_Click");
        }
    }

    protected DataTable AddMatched(DataTable dt, int paramValue = 0)
    {
        try
        {
            string caseDId = hdfCaseDetailsID.Value;
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;
            DataTable dtScruClearView = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string whereM = string.Empty;
                string queryM = string.Empty;
                if (paramValue != 0)
                {
                    whereM = "where scrutinizerId=" + paramValue + "";
                    queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerClearView", whereM);
                }
                else
                {
                    whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
                    queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerClearView", whereM);
                }
                SqlCommand cmd = new SqlCommand(queryM, conn);
                getMsgs = $"M&LDetails|AddMatched|{queryM}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtScruClearView);
            }

            foreach (DataRow dr1 in dtScruClearView.Rows)
            {
                if (caseDId == Convert.ToString(dr1["detailId"]))
                {
                    reportId = Convert.ToString(dr1["reportId"]);
                    birthId = Convert.ToString(dr1["birthId"]);
                    deathId = Convert.ToString(dr1["deathId"]);
                    break;
                }
            }

            //Match Record
            foreach (DataRow dr2 in dtScruClearView.Rows)
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"])) || (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"])) || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"])))
                {
                    string matchDid = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfCaseDetailsID.Value).Contains(matchDid))
                    {
                        hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + matchDid;
                    }
                }
            }

            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (!string.IsNullOrEmpty(reportId))
                    {
                        dr[i] = "Unmatched";
                    }
                    if (!string.IsNullOrEmpty(reportId) && (dt.Rows[j].ItemArray[i].ToString() == reportId))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                }
            }
            dt.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_AddMatched");
        }
        return dt;
    }

    protected void btnNextMatch_Click(object sender, EventArgs e)
    {
        try
        {
            isOwnerID = IsOwnerShip();

            if (!isOwnerID.Equals(Session["userid"]))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate.', 'Error Message');", true);
            }

            int kindValue = 0;
            string query = string.Empty;
            if (hdfTableKeyName.Value == "Birth Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                kindValue = 1;
            }
            else if (hdfTableKeyName.Value == "Death Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                kindValue = 2;
            }
            else
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
            }
            string json = "{\"cmd\":\"NEXT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + ",\"kind\":" + kindValue + "}";
            string response = SocketConn(json);
            getMsgs = $"btnNextMatch_Click|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    if (dt != null)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = "IsMatchRecord";
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                {
                                    dr1[i] = "Check for Linking Process";
                                }
                            }
                        }
                        dt.Rows.Add(dr1);
                        if (dt.Columns.Count > 2)
                        {
                            for (int i = 2; i < dt.Columns.Count; i++)
                            {
                                if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                                {
                                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                }
                            }
                        }
                    }

                    Session["datarecords"] = null;
                    Session["datarecords"] = dt;
                    lblSockerResponse.Text = "<b><span class=\"red-italic\">Next possible records.</span></b>";
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                {
                    hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                    return;
                }
                else
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    if (dt != null)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = "IsMatchRecord";
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                {
                                    dr1[i] = "Check for Linking Process";
                                }
                            }
                        }
                        dt.Rows.Add(dr1);
                        if (dt.Columns.Count > 2)
                        {
                            for (int i = 2; i < dt.Columns.Count; i++)
                            {
                                if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                                {
                                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                }
                            }
                        }
                    }

                    Session["datarecords"] = null;
                    Session["datarecords"] = dt;
                    string message = ResponseMessage(Convert.ToString(data.name));
                    lblSockerResponse.Text = message;
                    if (message.Contains("There are no other matches available."))
                    {
                        btnNextMatch.Enabled = false;
                        btnNextMatch.CssClass = "enableGrey";
                        btnPreviousMatch.Enabled = true;
                        btnPreviousMatch.CssClass = "disableGrey";
                    }
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnNextMatch_Click");
        }
    }

    protected void btnPreviousMatch_Click(object sender, EventArgs e)
    {
        try
        {
            isOwnerID = IsOwnerShip();

            if (!isOwnerID.Equals(Session["userid"]))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate.', 'Error Message');", true);
            }

            int kindValue = 0;
            string query = string.Empty;

            if (hdfTableKeyName.Value == "Birth Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
                kindValue = 1;
            }
            else if (hdfTableKeyName.Value == "Death Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
                kindValue = 2;
            }
            else
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
            }
            string json = "{\"cmd\":\"PREVIOUS\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + ",\"kind\":" + kindValue + "}";
            string response = SocketConn(json);
            getMsgs = $"btnPreviousMatch_Click|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    if (dt != null)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = "IsMatchRecord";
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                {
                                    dr1[i] = "Check for Linking Process";
                                }
                            }
                        }
                        dt.Rows.Add(dr1);
                        if (dt.Columns.Count > 2)
                        {
                            for (int i = 2; i < dt.Columns.Count; i++)
                            {
                                if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                                {
                                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                }
                            }
                        }
                    }

                    Session["datarecords"] = null;
                    Session["datarecords"] = dt;
                    lblSockerResponse.Text = "<b><span class=\"red-italic\">Previous possible records.</span></b>";
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                {
                    hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                    return;
                }
                else
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    if (dt != null)
                    {
                        DataRow dr1 = dt.NewRow();
                        dr1[0] = "IsMatchRecord";
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                {
                                    dr1[i] = "Check for Linking Process";
                                }
                            }
                        }
                        dt.Rows.Add(dr1);
                        if (dt.Columns.Count > 2)
                        {
                            for (int i = 2; i < dt.Columns.Count; i++)
                            {
                                if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                                {
                                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                }
                            }
                        }
                    }

                    Session["datarecords"] = null;
                    Session["datarecords"] = dt;

                    string message = ResponseMessage(Convert.ToString(data.name));
                    lblSockerResponse.Text = message;
                    if (message.Contains("There are no other matches available."))
                    {
                        btnPreviousMatch.Enabled = false;
                        btnPreviousMatch.CssClass = "enableGrey";
                        btnNextMatch.Enabled = true;
                        btnNextMatch.CssClass = "disableGrey";
                    }
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnPreviousMatch_Click");
        }
    }

    public string SocketConn(string JsonRecord)
    {
        string responseFromServer = string.Empty;
        string socketIp = ConfigurationManager.AppSettings["ipAddress"];
        int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
        try
        {
            using (var socket = new TcpClient(socketIp, socketPort))
            {
                string json = JsonRecord;
                byte[] body = Encoding.UTF8.GetBytes(json);
                int bodyLength = Encoding.UTF8.GetByteCount(json);
                var bl = (byte)(bodyLength);
                var bh = (byte)(bodyLength >> 8);
                using (var stream = socket.GetStream())
                {
                    stream.WriteByte(bh);
                    stream.WriteByte(bl);
                    stream.Write(body, 0, bodyLength);

                    short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                    byte[] buffer = new byte[size];

                    int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                    if (sizestream == size)
                    {
                        responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                    }
                    else
                    {
                        responseFromServer = "SocketConnectionError";
                    }
                }
            }
        }
        catch (Exception)
        {
            responseFromServer = "SocketConnectionError";
        }
        return responseFromServer;
    }

    private string[] Get4RequireIDs(string query, string detailId)
    {
        string caserecordid = string.Empty;
        string caseRepNo = string.Empty;
        string birthRecNo = string.Empty;
        string deathRecNo = string.Empty;

        string[] recoupRequiredIDs = new string[4];

        DataTable dtCase = new DataTable();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string whereM = query + " where SC.detailId=" + detailId + "";
            string queryM = string.Format(whereM);
            SqlCommand cmd = new SqlCommand(queryM, conn);
            getMsgs = $"M&LDetails|Get4RequireIDs|{queryM}";
            ErrorLog(getMsgs);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dtCase);
            if (dtCase.Rows.Count > 0)
            {
                recoupRequiredIDs[0] = Convert.ToString(dtCase.Rows[0]["reportId"]);
                recoupRequiredIDs[1] = Convert.ToString(dtCase.Rows[0]["masterRecordNumber"]);
                recoupRequiredIDs[2] = Convert.ToString(dtCase.Rows[0]["deathNumber"]);
                recoupRequiredIDs[3] = Convert.ToString(dtCase.Rows[0]["caseRecId"]);
            }
        }
        return recoupRequiredIDs;
    }

    private void CaseReportLinkMatchingIds() //if there is active tab - then check match and if match take DID for Link Process
    {
        string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");
        DataTable dtCR = new DataTable();
        dtCR = CommonMethodOnlyForFecthRecord(query, "CaseReports");

        hdfCaseRecIDCurrentPageValue.Value = null;

        if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
        {
            hdfDetailsID.Value = string.Empty;
        }
        for (int i = 2; i < dtCR.Columns.Count; i++)
        {
            if ((dtCR.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
            {
                string queryC = string.Empty;
                string caseRecordId = string.Empty;
                string caseRepID = string.Empty;
                string birthRecNo = string.Empty;
                string deathRecNo = string.Empty;
                string firstCheckDid = string.Empty;

                firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                if (!string.IsNullOrEmpty(firstCheckDid))
                {
                    if (hdfFirstTable.Value == "BirthRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfBirthReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfBirthRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfBirthRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfBirthRecordCaseRecordId.Value = caseRecordId;
                    }
                    else if (hdfFirstTable.Value == "DeathRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfDeathReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfDeathRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfDeathRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfDeathRecordCaseRecordID.Value = caseRecordId;
                    }
                    else
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfCaseReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfCaseReportBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfCaseReportDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfCaseReportCaseRecordId.Value = caseRecordId;
                    }

                    if (caseRecordId == Convert.ToString(dtCR.Rows[4][i]))
                    {
                        hdfCaseReportID.Value = Convert.ToString(dtCR.Rows[3][i]);              // Case Report ID from C. Report Source
                        hdfCaseReportCaseRecordId.Value = Convert.ToString(dtCR.Rows[4][i]);     // Case Record ID from C. Report Source
                        hdfCaseReportBirthRecNo.Value = Convert.ToString(dtCR.Rows[5][i]);        // Birth Rec. No from C. Report Source
                        hdfCaseReportDeathRecNo.Value = Convert.ToString(dtCR.Rows[6][i]);        // Death Rec. No from C. Report Source
                        break;
                    }
                    else
                    {
                        hdfCaseReportID.Value = Convert.ToString(dtCR.Rows[3][i]);              // Case Report ID from C. Report Source
                        hdfCaseReportCaseRecordId.Value = Convert.ToString(dtCR.Rows[4][i]);     // Case Record ID from C. Report Source
                        hdfCaseReportBirthRecNo.Value = Convert.ToString(dtCR.Rows[5][i]);        // Birth Rec. No from C. Report Source
                        hdfCaseReportDeathRecNo.Value = Convert.ToString(dtCR.Rows[6][i]);        // Death Rec. No from C. Report Source
                    }
                }
                else
                {
                    hdfCaseReportID.Value = Convert.ToString(dtCR.Rows[3][i]);              // Case Report ID from C. Report Source
                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dtCR.Rows[4][i]);     // Case Record ID from C. Report Source
                    hdfCaseReportBirthRecNo.Value = Convert.ToString(dtCR.Rows[5][i]);        // Birth Rec. No from C. Report Source
                    hdfCaseReportDeathRecNo.Value = Convert.ToString(dtCR.Rows[6][i]);        // Death Rec. No from C. Report Source
                }
            }
            else
            {
                hdfCaseReportID.Value = Convert.ToString(dtCR.Rows[3][i]);              // Case Report ID from C. Report Source
                hdfCaseReportCaseRecordId.Value = Convert.ToString(dtCR.Rows[4][i]);     // Case Record ID from C. Report Source
                hdfCaseReportBirthRecNo.Value = Convert.ToString(dtCR.Rows[5][i]);        // Birth Rec. No from C. Report Source
                hdfCaseReportDeathRecNo.Value = Convert.ToString(dtCR.Rows[6][i]);        // Death Rec. No from C. Report Source
            }
        }

        string caseDId = hdfDetailsID.Value;

        DataTable dtScruClearView = new DataTable();
        string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

        //Match Record
        var multipleReturnValue = CommonMatchWithDID(hdfisFirstCheckedDetailsID.Value, dtCR);

    }

    private void CaseReport()
    {
        try
        {
            if (string.IsNullOrEmpty(hdfFirstTable.Value) && !string.IsNullOrEmpty(hdfFirstCheckedCRDid.Value))
            {
                hdfFirstTable.Value = "Matching_Linking";
                hdfisFirstCheckedDetailsID.Value = hdfFirstCheckedCRDid.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedCRDid.Value;
            }
            else if (string.IsNullOrEmpty(hdfFirstTable.Value) && string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && !string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
            {
                hdfFirstTable.Value = "BirthRecords";
                hdfisFirstCheckedDetailsID.Value = hdfBirthRecordDidOnly.Value;
                hdfFirstCheckedBRDid.Value = hdfBirthRecordDidOnly.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedBRDid.Value;
            }
            else if (string.IsNullOrEmpty(hdfFirstTable.Value) && string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
            {
                hdfFirstTable.Value = "DeathRecords";
                hdfisFirstCheckedDetailsID.Value = hdfDeathRecordDidOnly.Value;
                hdfFirstCheckedDRDid.Value = hdfDeathRecordDidOnly.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedDRDid.Value;
            }

            hdfCaseRecIDCurrentPageValue.Value = null;
            hdftieit.Value = null;

            if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
            {
                hdfDetailsID.Value = string.Empty;
            }

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("Green", "");

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("activeGreen", "");


            string className = btnCaseReports.CssClass;
            className = className.Replace("active", "");
            className = className.Replace("btn-disabled", "");

            btnCaseReports.CssClass = className + " active";
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("active", "");

            hdfTableKeyName.Value = "Case Reports";
            IsCaseDetailID.Value = "casereport";
            lblSockerResponse.Text = null;
            hdfTableName.Value = "Matching_Linking";
            hdfSchema.Value = "MBDR_System";
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC ");

            isOwnerID = IsOwnerShip();

            string env = ConfigurationManager.AppSettings["environment"];

            if (!isOwnerID.Equals(Session["userid"]))
            {
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                btnRelease.Enabled = false;
                btnLink.Enabled = false;
                btnAssign.Enabled = false;
                btnPreviousMatch.Enabled = false;
                btnNextMatch.Enabled = false;


                btnRelease.CssClass = "enableGrey";
                btnLink.CssClass = "enableGrey";
                btnAssign.CssClass = "enableGrey";
                btnPreviousMatch.CssClass = "enableGrey";
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "false";

                if (permissions.Count > 0)
                {
                    displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                    string miMiLogin = string.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                        {
                            if (string.IsNullOrEmpty(isOwnerID))
                            {
                                btnOwn.Enabled = true;
                                btnOwn.CssClass = "disableGrey";
                            }
                            else
                            {
                                btnOwn.Enabled = false;
                                btnOwn.CssClass = "enableGrey";
                            }
                        }
                        else if (env == "prod")
                        {
                            btnOwn.Enabled = false;
                            btnOwn.CssClass = "enableGrey";
                        }
                        else
                        {
                            btnOwn.Enabled = true;
                            btnOwn.CssClass = "disableGrey";
                        }
                    }
                }
                else
                {
                    btnOwn.Enabled = false;
                    btnOwn.CssClass = "enableGrey";
                }
            }
            else
            {
                btnOwn.Enabled = false;
                btnRelease.Enabled = true;
                btnOwn.OnClientClick = null;
                btnLink.Enabled = true;
                btnAssign.Enabled = true;
                btnPreviousMatch.Enabled = false;  // will need to change true both prev and next 
                btnNextMatch.Enabled = false;

                btnOwn.CssClass = "enableGrey";
                btnRelease.CssClass = "disableGrey";
                btnLink.CssClass = "disableGrey";
                btnAssign.CssClass = "disableGrey";
                btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "true";
            }

            OwnerShip();
            dt = new DataTable();
            dt = CommonMethod(query);

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                if ((dt.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
                {
                    string queryC = string.Empty;
                    string caseRecordId = string.Empty;
                    string caseRepID = string.Empty;
                    string birthRecNo = string.Empty;
                    string deathRecNo = string.Empty;
                    string firstCheckDid = string.Empty;

                    firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                    if (!string.IsNullOrEmpty(firstCheckDid))
                    {
                        if (hdfFirstTable.Value == "BirthRecords")
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfBirthReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfBirthRecordBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfBirthRecordDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfBirthRecordCaseRecordId.Value = caseRecordId;
                        }
                        else if (hdfFirstTable.Value == "DeathRecords")
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfDeathReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfDeathRecordBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfDeathRecordDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfDeathRecordCaseRecordID.Value = caseRecordId;
                        }
                        else
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfCaseReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfCaseReportBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfCaseReportDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfCaseReportCaseRecordId.Value = caseRecordId;
                        }

                        if (caseRecordId == Convert.ToString(dt.Rows[5][i]))
                        {
                            hdfCaseReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from C. Report Source
                            hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from C. Report Source
                            hdfCaseReportBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from C. Report Source
                            hdfCaseReportDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from C. Report Source
                            hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                            break;
                        }
                        else
                        {
                            hdfCaseReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from C. Report Source
                            hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from C. Report Source
                            hdfCaseReportBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from C. Report Source
                            hdfCaseReportDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from C. Report Source
                            hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                        }
                    }
                    else
                    {
                        hdfCaseReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from C. Report Source
                        hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from C. Report Source
                        hdfCaseReportBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from C. Report Source
                        hdfCaseReportDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);
                        hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                    }
                }
                else
                {
                    hdfCaseReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from C. Report Source
                    hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from C. Report Source
                    hdfCaseReportBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from C. Report Source
                    hdfCaseReportDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from C. Report Source
                    hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                }
            }

            string caseDId = hdfisFirstCheckedDetailsID.Value;

            if (!string.IsNullOrEmpty(caseDId))
            {
                DataTable dtScruClearView = new DataTable();
                string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

                //Match Record
                var multipleReturnValue = CommonMatchDID(caseDId, dt);
                dtScruClearView = multipleReturnValue.Item1 as DataTable;
                reportId = multipleReturnValue.Item2 as string;
                birthId = multipleReturnValue.Item3 as string;
                deathId = multipleReturnValue.Item4 as string;

                DataRow dr = dt.NewRow();
                dr[0] = "IsMatchRecord";


                for (int i = 2; i < dt.Columns.Count; i++)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (string.IsNullOrEmpty(hdfDetailsID.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5][i])))
                        {
                            dr[i] = "Check for Linking Process";
                            break;
                        }
                        else if (string.IsNullOrEmpty(reportId) && string.IsNullOrEmpty(birthId) && string.IsNullOrEmpty(deathId))
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(reportId) && (dt.Rows[4].ItemArray[i].ToString() == reportId) && caseDId == Convert.ToString(dt.Rows[5].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(reportId) && dt.Rows[4].ItemArray[i].ToString() == reportId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1].ItemArray[i]))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (hdfFirstTable.Value == "BirthRecords")
                        {
                            if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value))
                            {
                                if (hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == reportId)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if ((!string.IsNullOrEmpty(hdfBirthReportID.Value)) && (!string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfBirthReportID.Value))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                            }
                            else if ((!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfBirthRecordBirthRecNo.Value))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))
                            // death                                                                                                
                            // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedBR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                            // report 
                            // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedBR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            // condition to do reverse link process when  birth source do not contain case record ID and data do not conflict

                            else if ((string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))
                            {
                                // check if the death rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) != Convert.ToString(hdfBirthRecordDeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (Convert.ToString(dt.Rows[6].ItemArray[i]) != Convert.ToString(hdfBirthRecordBirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            // condition to do reverse link process when  death source do not contain case record ID and data do not conflict

                            else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7][i])))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (Convert.ToString(dt.Rows[6].ItemArray[i]) != Convert.ToString(hdfDeathRecordBirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }
                        else if (hdfFirstTable.Value == "DeathRecords")
                        {
                            if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value))
                            {
                                if (hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(reportId) && Convert.ToString(dt.Rows[4].ItemArray[i]) == reportId)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if ((!string.IsNullOrEmpty(hdfDeathReportID.Value)) && (!string.IsNullOrEmpty(reportId) && (Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfDeathReportID.Value)))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                            }
                            else if ((!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfDeathRecordDeathRecNo.Value)))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))  // report note: Case Rec ID can't be null if report ID exist and vice versa.
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedDR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))  // birth 
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedDR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }

                            // condition to do reverse link process when  death source do not contain case record ID and data do not conflict

                            else if ((string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7][i]))))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[6].ItemArray[i]) != hdfDeathRecordBirthRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && !string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfJSBirth_BirthRecNo.Value)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && !string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[6].ItemArray[i]) != hdfJSBirth_BirthRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            // condition to do reverse link process when  birth source do not contain case record ID and data do not conflict

                            else if ((string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))
                            {
                                // check if the death rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) != !string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            else
                            {
                                dr[i] = "Unmatched";
                            }
                        }
                        else if (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking")  // return to same first table
                        {
                            if (string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))  // birth
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))  // death
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }

                            //check if death source is checked and case ID exist or not 

                            else if ((string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7][i]))))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) != !string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) != !string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            //check if birth source is checked and case ID exist or not 

                            else if ((string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfJSBirth_DeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) != !string.IsNullOrEmpty(hdfJSBirth_DeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) != !string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }
                        else
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                    }
                }
                dt.Rows.Add(dr);

                if (dtScruClearView.Rows.Count == 0)
                {
                    dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
                }
                foreach (DataRow drSC in dtScruClearView.Rows)
                {
                    if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string className1 = btnBirthRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = className1 + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnBirthRecords.CssClass;
                                btnBirthRecords.Attributes.Remove("onclick");
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = className1 + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string className1 = btnDeathRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = className1 + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = className1 + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string className1 = btnCaseReports.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = className1 + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnCaseReports.CssClass = className1 + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string className1 = btnDeathRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = className1 + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = className1 + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string className1 = btnCaseReports.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = className1 + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnCaseReports.CssClass = className1 + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string className1 = btnBirthRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = className1 + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnBirthRecords.Enabled = false;
                                    btnBirthRecords.CssClass = className1 + " btn-disabled";
                                }
                            }
                        }
                    }

                }

            }
            else
            {
                if (dt != null)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1[0] = "IsMatchRecord";
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (string.IsNullOrEmpty(hdfDetailsID.Value))
                            {
                                dr1[i] = "Check for Linking Process";
                            }

                            // tieit conditional handle 
                            if (hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                            {
                                dr1[i] = "tieit";
                                hdftieit.Value = "tieit";
                            }
                        }
                    }
                    dt.Rows.Add(dr1);
                    if (dt.Columns.Count > 2)
                    {
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                            {
                                hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                                break;
                            }
                        }
                    }
                }
                //scrutinizerdatarecords pull from ScrutinizerClearView Table
                DataTable dtSc = new DataTable();
                Session["scrutinizerdatarecords"] = null;

                dtSc = CheckscrutinizerIdValue();
                Session["scrutinizerdatarecords"] = dtSc;

                foreach (DataRow dr in dtSc.Rows)
                {
                    if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string classNameblank = btnBirthRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                //if (!btnBirthRecords.CssClass.Contains("btn-disabled"))
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string classNameblank = btnDeathRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string classNameblank = btnCaseReports.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = classNameblank + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string classNameblank = btnDeathRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string classNameblank = btnCaseReports.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = classNameblank + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }

                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string classNameblank = btnBirthRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                        }
                    }
                }
            }

            //Similar data green check enable

            DataTable dtSimilar = new DataTable();
            Session["scrutinizersimilardatarecords"] = null;
            
            dtSimilar = CheckSimilarCandidates();
            Session["scrutinizersimilardatarecords"] = dtSimilar;

            if (dtSimilar != null && dtSimilar.Rows.Count > 0)
            {
                string classSimilarName = btnSimilar.CssClass;
                if (classSimilarName.Contains("btn-disabled"))
                {
                    classSimilarName = classSimilarName.Replace("btn-disabled", "");
                }
                btnSimilar.CssClass = classSimilarName + " activeGreen";
                btnSimilar.Enabled = true;
            }
            else
            {
                string classSimilarName = btnSimilar.CssClass;
                if (!btnSimilar.CssClass.Contains("btn-disabled") && !classSimilarName.Contains("active"))
                {
                    btnSimilar.CssClass = classSimilarName + " btn-disabled";
                    btnSimilar.Enabled = false;
                }
            }

            if (Session["dtMaintain"] != null)
            {
                Session["dtMaintain"] = null;
            }
            Session["dtMaintain"] = dt;
            Session["datarecords"] = null;
            Session["datarecords"] = dt;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CaseReports");
        }
    }
    
    protected Tuple<DataTable, string, string, string> CommonMatchWithDID(string caseDId, DataTable dt)
    {
        string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;
        DataTable dtScruClearView = new DataTable();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
            string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerClearView", whereM);
            SqlCommand cmd = new SqlCommand(queryM, conn);
            getMsgs = $"M&LDetails|CommonMatchWithDID|{queryM}";
            ErrorLog(getMsgs);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dtScruClearView);
        }

        foreach (DataRow dr1 in dtScruClearView.Rows)          // current clearview table 
        {
            if (caseDId == Convert.ToString(dr1["detailId"]))
            {
                reportId = Convert.ToString(dr1["reportId"]);  // 09/29 in birth and death there will not be report ID;s 
                birthId = Convert.ToString(dr1["birthId"]);
                deathId = Convert.ToString(dr1["deathId"]);
                break;
            }
        }
        foreach (DataRow dr2 in dtScruClearView.Rows)   // combining DID's from all available source
        {
            // First Table = Birth Records

            if (hdfFirstTable.Value == "BirthRecords")
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0") || (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0"))
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                            }
                        }
                    }
                }
                // why is it satisfying this condition SID 194
                else if ((!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2") ||
                    (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2"))
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId != Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                            }
                            else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                            {
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                                {
                                    if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                                    }
                                }
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId != Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                            }
                        }
                    }
                }


            }

            // First Table = Death Records

            else if (hdfFirstTable.Value == "DeathRecords")
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0") || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0"))
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                        {
                            hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + caseDId;
                        }
                    }
                }
                else if ((!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1") || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1"))
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                        {
                            hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + birthDId;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                        {
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                            {
                                hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + deathDId;
                            }
                        }
                    }
                }

                else if (!string.IsNullOrEmpty(deathId) && deathId != Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                            }
                        }
                    }
                }

                else if (!string.IsNullOrEmpty(deathId) && deathId != Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                            }
                        }
                    }
                }
            }
            // First Table = Case Reports 
            else
            {
                if (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                            {
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                                {
                                    hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + caseDId;
                                }
                            }
                        }
                    }
                }
                else if ((!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1") || (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1"))
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                            {
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                                {
                                    hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + birthDId;
                                }
                            }
                        }
                    }
                }
                else if ((!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2") || (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2"))
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                        {
                            if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                            {
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                                {
                                    hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + deathDId;
                                }
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(reportId) && reportId != Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i])) // error here 
                        if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i])) // error here 
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                            }
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(reportId) && reportId != Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                            }
                        }
                        //else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                            }
                        }
                    }
                }
            }
        }
        return Tuple.Create(dtScruClearView, reportId, birthId, deathId);
    }
    
    protected Tuple<DataTable, string, string, string> CommonMatchDID(string caseDId, DataTable dt)
    {
        string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;
        DataTable dtScruClearView = new DataTable();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
            string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerClearView", whereM);
            SqlCommand cmd = new SqlCommand(queryM, conn);
            getMsgs = $"M&LDetails|CommonMatchDID|{queryM}";
            ErrorLog(getMsgs);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dtScruClearView);
        }

        foreach (DataRow dr1 in dtScruClearView.Rows)          // current clearview table 
        {
            if (caseDId == Convert.ToString(dr1["detailId"]))
            {
                reportId = Convert.ToString(dr1["reportId"]);  // 09/29 in birth and death there will not be report ID;s 
                birthId = Convert.ToString(dr1["birthId"]);
                deathId = Convert.ToString(dr1["deathId"]);
                break;
            }
        }
        foreach (DataRow dr2 in dtScruClearView.Rows)   // combining DID's from all available source
        {
            // First Table = Birth Records

            if (hdfFirstTable.Value == "BirthRecords")
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0") || (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0"))
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                }

                else if ((!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2") ||
                    (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2"))
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId != Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId != Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && hdfBirthRecordBirthRecNo.Value == Convert.ToString(dt.Rows[6][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }
            }

            // First Table = Death Records

            else if (hdfFirstTable.Value == "DeathRecords")
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0") || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0"))
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                }
                else if ((!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1") || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1"))
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                }
                else if (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                }

                else if (!string.IsNullOrEmpty(deathId) && deathId != Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]))
                        {
                            caseDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }

                else if (!string.IsNullOrEmpty(deathId) && deathId != Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && hdfDeathRecordDeathRecNo.Value == Convert.ToString(dt.Rows[7][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }
            }
            // First Table = Case Reports 
            else
            {
                if (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                }
                else if ((!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1") || (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1"))
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                }
                else if ((!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2") || (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2"))
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                }
                else if (!string.IsNullOrEmpty(reportId) && reportId != Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        //if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i])) // error here 
                        if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i])) // error here 
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]))
                        {
                            birthDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(reportId) && reportId != Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                        }
                        //else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]) && hdfRecordFrom.Value != Convert.ToString(dt.Rows[0][i]))
                        else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && hdfCaseReportDeathRecNo.Value == Convert.ToString(dt.Rows[4][i]))
                        {
                            deathDId = Convert.ToString(dt.Rows[1][i]);
                        }
                    }
                }
            }
        }
        return Tuple.Create(dtScruClearView, reportId, birthId, deathId);
    }

    protected void btnCaseReports_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedCR.Value = "true";

            if (hdfIsTabClickedBR.Value == "true")
            {
                string btnDR = btnDeathRecords.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedDR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedSM.Value == "true")
                {
                    if (hdfIsTabClickedDR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnDR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnDR.Contains("active") && !btnSM.Contains("active"))
                { hdfIsTabClicked.Value = "true"; }
            }
            else if (hdfIsTabClickedDR.Value == "true")
            {

                string btnBR = btnBirthRecords.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedBR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedSM.Value == "true")
                {
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnBR.Contains("active") && !btnSM.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }

            }
            else
            {
                string btnBR = btnBirthRecords.CssClass;
                string btnDR = btnDeathRecords.CssClass;
                if (hdfIsTabClickedDR.Value == "true")
                {
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedBR.Value == "true")
                {
                    if (hdfIsTabClickedDR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnDR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnDR.Contains("active") && !btnBR.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }

            CaseReport();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "CheckBoxFunction();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnCaseReports_Click");
        }
    }

    private void BirthRecordLinkMatchingIds()  // if there is active tab - then check match and if match take DID for Link Process
    {
        string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
        DataTable dtBR = new DataTable();
        dtBR = CommonMethodOnlyForFecthRecord(query, "BirthRecords");

        hdfBirthIDCurrentPageValue.Value = null;
        if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
        {
            hdfDetailsID.Value = string.Empty;
        }

        for (int i = 2; i < dtBR.Columns.Count; i++)
        {
            if ((dtBR.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
            {
                string queryC = string.Empty;
                string caseRecordId = string.Empty;
                string caseRepID = string.Empty;
                string birthRecNo = string.Empty;
                string deathRecNo = string.Empty;
                string firstCheckDid = string.Empty;

                firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                if (!string.IsNullOrEmpty(firstCheckDid))
                {
                    if (hdfFirstTable.Value == "BirthRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfBirthReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfBirthRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfBirthRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfBirthRecordCaseRecordId.Value = caseRecordId;
                    }
                    else if (hdfFirstTable.Value == "DeathRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfDeathReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfDeathRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfDeathRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfDeathRecordCaseRecordID.Value = caseRecordId;
                    }
                    else
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfCaseReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfCaseReportBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfCaseReportDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfCaseReportCaseRecordId.Value = caseRecordId;
                    }

                    //Matching match
                    if (caseRecordId == Convert.ToString(dtBR.Rows[4][i]))
                    {
                        hdfBirthReportID.Value = Convert.ToString(dtBR.Rows[3][i]);              // Case Report ID from Birth Source
                        hdfBirthRecordCaseRecordId.Value = Convert.ToString(dtBR.Rows[4][i]);     // Case Record ID from Birth Source
                        hdfBirthRecordBirthRecNo.Value = Convert.ToString(dtBR.Rows[5][i]);        // Birth Rec. No from Birth Source
                        hdfBirthRecordDeathRecNo.Value = Convert.ToString(dtBR.Rows[6][i]);        // Death Rec. No from Birt Source

                        break;
                    }
                    else
                    {
                        hdfBirthReportID.Value = Convert.ToString(dtBR.Rows[3][i]);              // Case Report ID from Birth Source
                        hdfBirthRecordCaseRecordId.Value = Convert.ToString(dtBR.Rows[4][i]);     // Case Record ID from Birth Source
                        hdfBirthRecordBirthRecNo.Value = Convert.ToString(dtBR.Rows[5][i]);        // Birth Rec. No from Birth Source
                        hdfBirthRecordDeathRecNo.Value = Convert.ToString(dtBR.Rows[6][i]);        // Death Rec. No from Birt Source
                    }
                }
                else
                {
                    hdfBirthReportID.Value = Convert.ToString(dtBR.Rows[3][i]);              // Case Report ID from Birth Source
                    hdfBirthRecordCaseRecordId.Value = Convert.ToString(dtBR.Rows[4][i]);     // Case Record ID from Birth Source
                    hdfBirthRecordBirthRecNo.Value = Convert.ToString(dtBR.Rows[5][i]);        // Birth Rec. No from Birth Source
                    hdfBirthRecordDeathRecNo.Value = Convert.ToString(dtBR.Rows[6][i]);        // Death Rec. No from Birt Source
                }
            }
            else
            {
                hdfBirthReportID.Value = Convert.ToString(dtBR.Rows[3][i]);              // Case Report ID from Birth Source
                hdfBirthRecordCaseRecordId.Value = Convert.ToString(dtBR.Rows[4][i]);     // Case Record ID from Birth Source
                hdfBirthRecordBirthRecNo.Value = Convert.ToString(dtBR.Rows[5][i]);        // Birth Rec. No from Birth Source
                hdfBirthRecordDeathRecNo.Value = Convert.ToString(dtBR.Rows[6][i]);        // Death Rec. No from Birt Source
            }
        }

        string caseDId = hdfDetailsID.Value;

        DataTable dtScruClearView = new DataTable();
        string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

        //Match Record
        var multipleReturnValue = CommonMatchWithDID(hdfisFirstCheckedDetailsID.Value, dtBR);
    }

    private void BirthRecord()
    {
        try
        {
        if (string.IsNullOrEmpty(hdfFirstTable.Value) && !string.IsNullOrEmpty(hdfFirstCheckedBRDid.Value))
        {
            hdfFirstTable.Value = "BirthRecords";
            hdfisFirstCheckedDetailsID.Value = hdfFirstCheckedBRDid.Value;
            hdfCaseDetailsID.Value = hdfFirstCheckedBRDid.Value;
        }
        else if (string.IsNullOrEmpty(hdfFirstTable.Value) && string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
        {
            hdfFirstTable.Value = "DeathRecords";
            hdfisFirstCheckedDetailsID.Value = hdfDeathRecordDidOnly.Value;
            hdfFirstCheckedDRDid.Value = hdfDeathRecordDidOnly.Value;
            hdfCaseDetailsID.Value = hdfFirstCheckedDRDid.Value;
        }
        else if (string.IsNullOrEmpty(hdfFirstTable.Value) && !string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
        {
            hdfFirstTable.Value = "Matching_Linking";
            hdfisFirstCheckedDetailsID.Value = hdfCaseReportDidOnly.Value;
            hdfFirstCheckedCRDid.Value = hdfCaseReportDidOnly.Value;
            hdfCaseDetailsID.Value = hdfFirstCheckedCRDid.Value;
        }

        hdftieit.Value = null;

        hdfBirthIDCurrentPageValue.Value = null;

        if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
        {
            hdfDetailsID.Value = string.Empty;
        }

        /* active green - current screen, green - data indicator, btn-disabled - grey out */

        btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
        btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
        btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
        btnSimilar.CssClass = btnSimilar.CssClass.Replace("activeGreen", "");


        btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
        btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
        btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");
        btnSimilar.CssClass = btnSimilar.CssClass.Replace("Green", "");

        string className = btnBirthRecords.CssClass;
        className = className.Replace("active", "");
        className = className.Replace("btn-disabled", "");

        btnBirthRecords.CssClass = className + " active";
        btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
        btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
        btnSimilar.CssClass = btnSimilar.CssClass.Replace("active", "");

        hdfTableKeyName.Value = "Birth Records";
        IsCaseDetailID.Value = "birthrecord";
        lblSockerResponse.Text = null;
        string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ");
        hdfSchema.Value = "Reference";
        hdfTableName.Value = "BirthRecords";
        if (!hdfMultipleTableName.Value.Contains("BirthRecords"))
        {
            hdfMultipleTableName.Value = hdfMultipleTableName.Value + "," + hdfTableName.Value;
        }

        isOwnerID = IsOwnerShip();

        string env = ConfigurationManager.AppSettings["environment"];

        if (!isOwnerID.Equals(Session["userid"]))
        {
            string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

            btnRelease.Enabled = false;
            btnLink.Enabled = false;
            btnAssign.Enabled = false;
            btnPreviousMatch.Enabled = false;
            btnNextMatch.Enabled = false;


            btnRelease.CssClass = "enableGrey";
            btnLink.CssClass = "enableGrey";
            btnAssign.CssClass = "enableGrey";
            btnPreviousMatch.CssClass = "enableGrey";
            btnNextMatch.CssClass = "enableGrey";
            hdfOwner.Value = "false";

            if (permissions.Count > 0)
            {
                displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                string miMiLogin = string.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                    {
                        if (string.IsNullOrEmpty(isOwnerID))
                        {
                            btnOwn.Enabled = true;
                            btnOwn.CssClass = "disableGrey";
                        }
                        else
                        {
                            btnOwn.Enabled = false;
                            btnOwn.CssClass = "enableGrey";
                        }
                    }
                    else if (env == "prod")
                    {
                        btnOwn.Enabled = false;
                        btnOwn.CssClass = "enableGrey";
                    }
                    else
                    {
                        btnOwn.Enabled = true;
                        btnOwn.CssClass = "disableGrey";
                    }
                }
            }
            else
            {
                btnOwn.Enabled = false;
                btnOwn.CssClass = "enableGrey";
            }
        }
        else
        {
            btnOwn.Enabled = false;
            btnRelease.Enabled = true;
            btnOwn.OnClientClick = null;
            btnLink.Enabled = true;
            btnAssign.Enabled = true;
            btnPreviousMatch.Enabled = false; // will need to change true both prev and next 
            btnNextMatch.Enabled = false;

            btnOwn.CssClass = "enableGrey";
            btnRelease.CssClass = "disableGrey";
            btnLink.CssClass = "disableGrey";
            btnAssign.CssClass = "disableGrey";
            btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
            btnNextMatch.CssClass = "enableGrey";
            hdfOwner.Value = "true";
        }

        OwnerShip();
        dt = new DataTable();
        dt = CommonMethod(query);
        List<string> collectCaseReportDid = null;
        List<string> collectBirthRecordDid = null;
        List<string> collectDeathRecordDid = null;
        for (int i = 2; i < dt.Columns.Count; i++)
        {
            if ((dt.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
            {
                string queryC = string.Empty;
                string caseRecordId = string.Empty;
                string caseRepID = string.Empty;
                string birthRecNo = string.Empty;
                string deathRecNo = string.Empty;
                string firstCheckDid = string.Empty;

                firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                if (!string.IsNullOrEmpty(firstCheckDid))
                {
                    if (hdfFirstTable.Value == "BirthRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfBirthReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfBirthRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfBirthRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfBirthRecordCaseRecordId.Value = caseRecordId;
                    }
                    else if (hdfFirstTable.Value == "DeathRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfDeathReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfDeathRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfDeathRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfDeathRecordCaseRecordID.Value = caseRecordId;
                    }
                    else
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfCaseReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfCaseReportBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfCaseReportDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfCaseReportCaseRecordId.Value = caseRecordId;
                    }

                    //Matching match
                    if (!string.IsNullOrEmpty(caseRecordId) && caseRecordId == Convert.ToString(dt.Rows[5][i]))
                    {
                        hdfBirthReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Birth Source
                        hdfBirthRecordCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Birth Source
                        hdfBirthRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Birth Source
                        hdfBirthRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Birt Source
                        hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                        break;
                    }
                    else
                    {
                        hdfBirthReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Birth Source
                        hdfBirthRecordCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Birth Source
                        hdfBirthRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Birth Source
                        hdfBirthRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Birt Source
                        hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                    }
                }
                else
                {
                    hdfBirthReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Birth Source
                    hdfBirthRecordCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Birth Source
                    hdfBirthRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Birth Source
                    hdfBirthRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Birt Source
                    hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                }
            }
            else
            {
                hdfBirthReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Birth Source
                hdfBirthRecordCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Birth Source
                hdfBirthRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Birth Source
                hdfBirthRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Birt Source
                hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
            }
        }


        string caseDId = hdfisFirstCheckedDetailsID.Value;

        if (!string.IsNullOrEmpty(caseDId))
        {
            //Match Record

            DataTable dtScruClearView = new DataTable();
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

            //Match Record
            var multipleReturnValue = CommonMatchDID(caseDId, dt);
            dtScruClearView = multipleReturnValue.Item1 as DataTable;
            reportId = multipleReturnValue.Item2 as string;
            birthId = multipleReturnValue.Item3 as string;
            deathId = multipleReturnValue.Item4 as string;
            string didlist = hdfDetailsID.Value;
            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)       // compare report ID/ birth ID / Death ID hidden from UI screen 
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (string.IsNullOrEmpty(hdfDetailsID.Value) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[5][i]))))
                    {
                        dr[i] = "Check for Linking Process";
                        break;
                    }
                    else if (string.IsNullOrEmpty(reportId) && string.IsNullOrEmpty(birthId) && string.IsNullOrEmpty(deathId))
                    {
                        dr[i] = "Unmatched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(reportId) && (dt.Rows[4].ItemArray[i].ToString() == reportId) && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(reportId) && dt.Rows[4].ItemArray[i].ToString() == reportId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking")
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value))
                            {
                                if (hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(birthId) && Convert.ToString(dt.Rows[71].ItemArray[i]) == birthId)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if ((!string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value)) && (!string.IsNullOrEmpty(birthId) && (Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfCaseReportBirthRecNo.Value)))
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                                {   // new conditions when three sources are available with certain conditions
                                    //dr[i] = "Unmatched";

                                    if (!string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value) && !string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && hdfJSDeath_BirthRecNo.Value == Convert.ToString(dt.Rows[6].ItemArray[i]))
                                    {
                                        //hdfBirthIDCurrentPageValue.Value = "awol";
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if (!string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))) && Convert.ToString(hdfJSCase_DeathRecNo.Value) == Convert.ToString(dt.Rows[7].ItemArray[i]))
                                    {
                                        //hdfBirthIDCurrentPageValue.Value = "awol";
                                        dr[i] = "Matched";
                                        break;
                                    }
                                    else if (string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))) && !string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)))
                                    {
                                        if (string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value)) // check this condition 
                                        {
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfBirthIDCurrentPageValue.Value = "awol";
                                            dr[i] = "Unmatched";
                                            break;
                                        }
                                        else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) &&
                                            !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value) &&
                                            string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value) &&
                                            !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) &&
                                            Convert.ToString(hdfJSDeath_DeathRecNo.Value) == Convert.ToString(dt.Rows[7].ItemArray[i]) &&
                                           !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))
                                        {
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfBirthIDCurrentPageValue.Value = "awol";
                                            dr[i] = "Unmatched";
                                            break;
                                        }
                                        else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && !string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && hdfJSDeath_BirthRecNo.Value == Convert.ToString(dt.Rows[6].ItemArray[i])) // from DS - Death ID, Birth ID - now BS - only Birth ID and do not match 
                                        {
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfBirthIDCurrentPageValue.Value = "awol";
                                            dr[i] = "Unmatched";
                                            break;
                                        }
                                        else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))
                                        {
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            dr[i] = "Nonpareil";
                                            hdfBirthIDCurrentPageValue.Value = "Nonpareil";
                                            break;
                                        }
                                        else if (string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
                                        {

                                            dr[i] = "tieit";
                                            break;
                                        }
                                        else
                                        {
                                            dr[i] = "Unmatched";

                                            break;
                                        }

                                    }
                                    else if (!string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)))
                                    {
                                        dr[i] = "Unmatched";
                                        hdfBirthIDCurrentPageValue.Value = "awol";
                                        collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                        break;
                                    }

                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                            }
                            else if ((!string.IsNullOrEmpty(hdfCaseReportID.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i])) && (Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfCaseReportID.Value)))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                            // birth
                            // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))   // death
                                                                                                                                                                                                                                                                                          // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }

                            // condition to do reverse link process when  birth source do not contain case record ID and data do not conflict From REPORT Source

                            else if ((!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value)) && (string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) != !string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }
                            // condition to do reverse link process when  birth source do not contain case record ID and data do not conflict From Death Source
                            else if ((!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && (string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i]))))
                            {
                                // check if the death rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) != !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }

                    else if (hdfFirstTable.Value == "DeathRecords")
                    {
                        if (!string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value))
                        {
                            if (hdfDeathRecordCaseRecordID.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || (!string.IsNullOrEmpty(birthId) && Convert.ToString(dt.Rows[71].ItemArray[i]) == birthId))   // check Case ID match or internal ID match 
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(birthId) && (Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfDeathRecordBirthRecNo.Value))  // check birth cert number from death source in birth source
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                            {
                                dr[i] = "Unmatched"; // for link process if case ID exsit but birth/death do not exist. Birth/DeathID's avaiable in another source without Case ID
                                hdfBirthIDCurrentPageValue.Value = "awol";
                                break;
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }

                        else if (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfDeathRecordDeathRecNo.Value))  // check death number from death source in birth source
                        {
                            dr[i] = "Matched";
                            string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                            if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                            {
                                hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                            }
                            break;
                        }
                        // birth
                        // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                        else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && Convert.ToString(dt.Rows[7].ItemArray[i]) != hdfDeathRecordDeathRecNo.Value)
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && !string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && Convert.ToString(dt.Rows[6].ItemArray[i]) != hdfDeathRecordBirthRecNo.Value)
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                            else
                            {
                                dr[i] = "Nonpareil";
                                break;
                            }
                        }
                        // report 
                        // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                        else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                        {
                            dr[i] = "Nonpareil";
                            string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);

                            break;
                        }

                        // when source do not have case but landing page have case

                        else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && !string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value) && string.IsNullOrEmpty(hdfDeathRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6][i])))
                        {
                            // check if the death rec no exist or not 

                            if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && (Convert.ToString(dt.Rows[7].ItemArray[i]) != Convert.ToString(hdfJSDeath_DeathRecNo.Value)))
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && hdfCaseReportCaseRecordId.Value != Convert.ToString(dt.Rows[5].ItemArray[i]))
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]))
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfJSDeath_DeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))
                            {
                                dr[i] = "tieit";
                                hdftieit.Value = "tieit";
                                break;
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }

                        else
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                    }

                    else if (hdfFirstTable.Value == "BirthRecords")  // return to same first table
                    {
                        if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))  // report
                        {
                            //dr[i] = "Nonpareil";
                            if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && Convert.ToString(dt.Rows[7].ItemArray[i]) != hdfDeathRecordDeathRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Nonpareil";
                                    break;
                                }
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }

                            //break;
                        }
                        else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5][i])) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))  // death
                        {
                            dr[i] = "Nonpareil";
                            string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);

                            break;
                        }
                        else
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                    }
                    else
                    {
                        dr[i] = "Unmatched";
                        break;
                    }
                }
            }
            dt.Rows.Add(dr);

            string value = null;
            if (collectCaseReportDid != null && collectCaseReportDid.Count > 0)
            {
                foreach (var item in collectCaseReportDid)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        value = item;
                    }
                    else
                    {
                        value = value + item + ",";
                    }
                }
                hdfCaseReportDidMatch.Value = value;
            }

            if (dtScruClearView.Rows.Count == 0)
            {
                dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
            }
            foreach (DataRow drSC in dtScruClearView.Rows)
            {
                if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            btnBirthRecords.Attributes.Remove("onclick");
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.Enabled = false;
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                            }
                        }
                    }
                }
            }

        }
        else
        {
            if (dt != null)
            {
                DataRow dr1 = dt.NewRow();
                dr1[0] = "IsMatchRecord";
                for (int i = 2; i < dt.Columns.Count; i++)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (string.IsNullOrEmpty(hdfDetailsID.Value))
                        {
                            dr1[i] = "Check for Linking Process";
                        }
                    }
                }
                dt.Rows.Add(dr1);
                if (dt.Columns.Count > 2)
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                        {
                            hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                        }
                    }
                }
            }
            //scrutinizerdatarecords pull from ScrutinizerClearView Table
            DataTable dtSc = new DataTable();
            Session["scrutinizerdatarecords"] = null;

            dtSc = CheckscrutinizerIdValue();
            Session["scrutinizerdatarecords"] = dtSc;

            foreach (DataRow dr in dtSc.Rows)
            {
                if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnBirthRecords.CssClass;
                            //if (!btnBirthRecords.CssClass.Contains("btn-disabled"))
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = classNameblank + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = classNameblank + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                    }
                }
            }
        }

        //Similar data green check enable

        DataTable dtSimilar = new DataTable();
        Session["scrutinizersimilardatarecords"] = null;
       
        dtSimilar = CheckSimilarCandidates();
        Session["scrutinizersimilardatarecords"] = dtSimilar;

        if (dtSimilar != null && dtSimilar.Rows.Count > 0)
        {
            string classSimilarName = btnSimilar.CssClass;
            if (classSimilarName.Contains("btn-disabled"))
            {
                classSimilarName = classSimilarName.Replace("btn-disabled", "");
            }
            btnSimilar.CssClass = classSimilarName + " activeGreen";
            btnSimilar.Enabled = true;
        }
        else
        {
            string classSimilarName = btnSimilar.CssClass;
            if (!btnSimilar.CssClass.Contains("btn-disabled") && !classSimilarName.Contains("active"))
            {
                btnSimilar.CssClass = classSimilarName + " btn-disabled";
                btnSimilar.Enabled = false;
            }
        }

        if (Session["dtMaintain"] != null)
        {
            Session["dtMaintain"] = null;
        }
        Session["dtMaintain"] = dt;
        Session["datarecords"] = null;
        Session["datarecords"] = dt;


        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_BirthRecord");
        }
    }

    protected void btnBirthRecords_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedBR.Value = "true";

            if (hdfIsTabClickedDR.Value == "true")
            {
                string btnCR = btnCaseReports.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }

                }
                else if (hdfIsTabClickedSM.Value == "true")
                {
                    if (hdfIsTabClickedCR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnCR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnCR.Contains("active") && !btnSM.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }
            else if (hdfIsTabClickedCR.Value == "true")
            {

                string btnDR = btnDeathRecords.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedDR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnDR.Contains("active") && !btnSM.Contains("active"))
                { hdfIsTabClicked.Value = "true"; }
            }
            else
            {
                string btnDR = btnDeathRecords.CssClass;
                string btnCR = btnCaseReports.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    if (hdfIsTabClickedDR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnDR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedDR.Value == "true")
                {
                    if (hdfIsTabClickedCR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnCR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnCR.Contains("active") && !btnDR.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }

            BirthRecord();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnBirthRecords_Click");
        }
    }

    private void DeathRecordLinkMatchingIds()      //if there is active tab - then check match and if match take DID for Link Process
    {
        string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");
        DataTable dtDeath = new DataTable();
        dtDeath = CommonMethodOnlyForFecthRecord(query, "DeathRecords");

        hdfDeathIDCurrentPageValue.Value = null;

        if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
        {
            hdfDetailsID.Value = string.Empty;
        }

        for (int i = 2; i < dtDeath.Columns.Count; i++)
        {
            if ((dtDeath.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
            {
                string queryC = string.Empty;
                string caseRecordId = string.Empty;
                string caseRepID = string.Empty;
                string birthRecNo = string.Empty;
                string deathRecNo = string.Empty;

                string firstCheckDid = string.Empty;

                firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                if (!string.IsNullOrEmpty(firstCheckDid))
                {

                    if (hdfFirstTable.Value == "BirthRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfBirthReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfBirthRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfBirthRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfBirthRecordCaseRecordId.Value = caseRecordId;
                    }
                    else if (hdfFirstTable.Value == "DeathRecords")
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfDeathReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfDeathRecordBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfDeathRecordDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfDeathRecordCaseRecordID.Value = caseRecordId;
                    }
                    else
                    {
                        queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                        fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                        caseRepID = fetchRequireIDs[0];
                        hdfCaseReportID.Value = caseRepID;

                        birthRecNo = fetchRequireIDs[1];
                        hdfCaseReportBirthRecNo.Value = birthRecNo;

                        deathRecNo = fetchRequireIDs[2];
                        hdfCaseReportDeathRecNo.Value = deathRecNo;

                        caseRecordId = fetchRequireIDs[3];
                        hdfCaseReportCaseRecordId.Value = caseRecordId;
                    }

                    if (caseRecordId == Convert.ToString(dtDeath.Rows[4][i]))
                    {
                        hdfDeathReportID.Value = Convert.ToString(dtDeath.Rows[3][i]);              // Case Report ID from Death Source
                        hdfDeathRecordCaseRecordID.Value = Convert.ToString(dtDeath.Rows[4][i]);     // Case Record ID from Death Source
                        hdfDeathRecordBirthRecNo.Value = Convert.ToString(dtDeath.Rows[5][i]);        // Birth Rec. No from Death Source
                        hdfDeathRecordDeathRecNo.Value = Convert.ToString(dtDeath.Rows[6][i]);        // Death Rec. No from Death Source
                        break;
                    }
                    else
                    {
                        hdfDeathReportID.Value = Convert.ToString(dtDeath.Rows[3][i]);              // Case Report ID from Death Source
                        hdfDeathRecordCaseRecordID.Value = Convert.ToString(dtDeath.Rows[4][i]);     // Case Record ID from Death Source
                        hdfDeathRecordBirthRecNo.Value = Convert.ToString(dtDeath.Rows[5][i]);        // Birth Rec. No from Death Source
                        hdfDeathRecordDeathRecNo.Value = Convert.ToString(dtDeath.Rows[6][i]);        // Death Rec. No from Death Source
                    }
                }
                else
                {
                    hdfDeathReportID.Value = Convert.ToString(dtDeath.Rows[3][i]);              // Case Report ID from Death Source
                    hdfDeathRecordCaseRecordID.Value = Convert.ToString(dtDeath.Rows[4][i]);     // Case Record ID from Death Source
                    hdfDeathRecordBirthRecNo.Value = Convert.ToString(dtDeath.Rows[5][i]);        // Birth Rec. No from Death Source
                    hdfDeathRecordDeathRecNo.Value = Convert.ToString(dtDeath.Rows[6][i]);        // Death Rec. No from Death Source
                }
            }
            else
            {
                hdfDeathReportID.Value = Convert.ToString(dtDeath.Rows[3][i]);              // Case Report ID from Death Source
                hdfDeathRecordCaseRecordID.Value = Convert.ToString(dtDeath.Rows[4][i]);     // Case Record ID from Death Source
                hdfDeathRecordBirthRecNo.Value = Convert.ToString(dtDeath.Rows[5][i]);        // Birth Rec. No from Death Source
                hdfDeathRecordDeathRecNo.Value = Convert.ToString(dtDeath.Rows[6][i]);        // Death Rec. No from Death Source
            }
        }

        string caseDId = hdfDetailsID.Value;

        DataTable dtScruClearView = new DataTable();
        string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

        //Match Record
        var multipleReturnValue = CommonMatchWithDID(hdfisFirstCheckedDetailsID.Value, dtDeath);
    }

    private List<string> AddRemoveItem(List<string> arr, string additem, string removeitem)
    {
        List<string> newArr = new List<string>();
        if (arr == null)
        {
            arr = new List<string>();
        }
        if (!string.IsNullOrEmpty(additem))
        {
            if (arr.Count == 0)
            {
                arr.Add(additem);
                newArr = arr;
            }
            else
            {
                newArr = arr;
                if (!newArr.Exists(s => s.Contains(additem)))
                {
                    foreach (string item in arr.ToList())
                    {
                        if (item != additem)
                        {
                            newArr.Add(additem);
                        }
                    }
                }
            }
            arr = newArr;
        }
        return arr;
    }

    private void DeathRecord()
    {
        try
        {
            if (string.IsNullOrEmpty(hdfFirstTable.Value) && !string.IsNullOrEmpty(hdfFirstCheckedDRDid.Value))
            {
                hdfFirstTable.Value = "DeathRecords";
                hdfisFirstCheckedDetailsID.Value = hdfFirstCheckedDRDid.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedDRDid.Value;
                hdfDetailsID.Value = string.IsNullOrEmpty(hdfDetailsID.Value) ? hdfFirstCheckedDRDid.Value : hdfDetailsID.Value;
            }
            else if (string.IsNullOrEmpty(hdfFirstTable.Value) && string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && !string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
            {
                hdfFirstTable.Value = "BirthRecords";
                hdfisFirstCheckedDetailsID.Value = hdfBirthRecordDidOnly.Value;
                hdfFirstCheckedBRDid.Value = hdfBirthRecordDidOnly.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedBRDid.Value;

                hdfDetailsID.Value = string.IsNullOrEmpty(hdfDetailsID.Value) ? hdfFirstCheckedBRDid.Value : hdfDetailsID.Value;
            }
            else if (string.IsNullOrEmpty(hdfFirstTable.Value) && !string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
            {
                hdfFirstTable.Value = "Matching_Linking";
                hdfisFirstCheckedDetailsID.Value = hdfCaseReportDidOnly.Value;
                hdfFirstCheckedCRDid.Value = hdfCaseReportDidOnly.Value;
                hdfCaseDetailsID.Value = hdfFirstCheckedCRDid.Value;

                hdfDetailsID.Value = string.IsNullOrEmpty(hdfDetailsID.Value) ? hdfFirstCheckedCRDid.Value : hdfDetailsID.Value;
            }

            hdftieit.Value = null;
            hdfDeathIDCurrentPageValue.Value = null;

            if (hdfDetailsID.Value == "null" || string.IsNullOrEmpty(hdfDetailsID.Value))
            {
                hdfDetailsID.Value = string.Empty;
            }

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("activeGreen", "");

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("Green", "");

            string className = btnDeathRecords.CssClass;
            className = className.Replace("active", "");
            className = className.Replace("btn-disabled", "");
            btnDeathRecords.CssClass = className + "active";

            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("active", "");

            hdfTableKeyName.Value = "Death Records";
            IsCaseDetailID.Value = "deathrecord";
            lblSockerResponse.Text = null;
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC ");

            hdfSchema.Value = "Reference";
            hdfTableName.Value = "DeathRecords";
            if (!hdfMultipleTableName.Value.Contains("DeathRecords"))
            {
                hdfMultipleTableName.Value = hdfMultipleTableName.Value + "," + hdfTableName.Value;
            }

            isOwnerID = IsOwnerShip();

            string env = ConfigurationManager.AppSettings["environment"];

            if (!isOwnerID.Equals(Session["userid"]))
            {
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                btnRelease.Enabled = false;
                btnLink.Enabled = false;
                btnAssign.Enabled = false;
                btnPreviousMatch.Enabled = false;
                btnNextMatch.Enabled = false;


                btnRelease.CssClass = "enableGrey";
                btnLink.CssClass = "enableGrey";
                btnAssign.CssClass = "enableGrey";
                btnPreviousMatch.CssClass = "enableGrey";
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "false";

                if (permissions.Count > 0)
                {
                    displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                    string miMiLogin = string.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                        {
                            if (string.IsNullOrEmpty(isOwnerID))
                            {
                                btnOwn.Enabled = true;
                                btnOwn.CssClass = "disableGrey";
                            }
                            else
                            {
                                btnOwn.Enabled = false;
                                btnOwn.CssClass = "enableGrey";
                            }
                        }
                        else if (env == "prod")
                        {
                            btnOwn.Enabled = false;
                            btnOwn.CssClass = "enableGrey";
                        }
                        else
                        {
                            btnOwn.Enabled = true;
                            btnOwn.CssClass = "disableGrey";
                        }
                    }
                }
                else
                {
                    btnOwn.Enabled = false;
                    btnOwn.CssClass = "enableGrey";
                }
            }
            else
            {
                btnOwn.Enabled = false;
                btnRelease.Enabled = true;
                btnOwn.OnClientClick = null;
                btnLink.Enabled = true;
                btnAssign.Enabled = true;
                btnPreviousMatch.Enabled = false; // will need to change true both prev and next 
                btnNextMatch.Enabled = false;

                btnOwn.CssClass = "enableGrey";
                btnRelease.CssClass = "disableGrey";
                btnLink.CssClass = "disableGrey";
                btnAssign.CssClass = "disableGrey";
                btnPreviousMatch.CssClass = "enableGrey"; // will need to change true both prev and next 
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "true";
            }

            OwnerShip();
            dt = new DataTable();
            dt = CommonMethod(query);
            List<string> collectCaseReportDid = null;
            List<string> collectBirthRecordDid = null;
            List<string> collectDeathRecordDid = null;
            for (int i = 2; i < dt.Columns.Count; i++)
            {
                if ((dt.Columns.Count > 2) && (!string.IsNullOrEmpty(hdfDetailsID.Value)))
                {
                    string queryC = string.Empty;
                    string caseRecordId = string.Empty;
                    string caseRepID = string.Empty;
                    string birthRecNo = string.Empty;
                    string deathRecNo = string.Empty;

                    string firstCheckDid = string.Empty;
                    firstCheckDid = hdfisFirstCheckedDetailsID.Value;

                    if (!string.IsNullOrEmpty(firstCheckDid))
                    {
                        if (hdfFirstTable.Value == "BirthRecords")
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_BirthRecords SC ";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfBirthReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfBirthRecordBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfBirthRecordDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfBirthRecordCaseRecordId.Value = caseRecordId;
                        }
                        else if (hdfFirstTable.Value == "DeathRecords")
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearView_DeathRecords SC";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfDeathReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfDeathRecordBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfDeathRecordDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfDeathRecordCaseRecordID.Value = caseRecordId;
                        }
                        else
                        {
                            queryC = "SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerClearViewCaseReports SC";

                            fetchRequireIDs = Get4RequireIDs(queryC, firstCheckDid);

                            caseRepID = fetchRequireIDs[0];
                            hdfCaseReportID.Value = caseRepID;

                            birthRecNo = fetchRequireIDs[1];
                            hdfCaseReportBirthRecNo.Value = birthRecNo;

                            deathRecNo = fetchRequireIDs[2];
                            hdfCaseReportDeathRecNo.Value = deathRecNo;

                            caseRecordId = fetchRequireIDs[3];
                            hdfCaseReportCaseRecordId.Value = caseRecordId;
                        }

                        if (caseRecordId == Convert.ToString(dt.Rows[5][i]))
                        {
                            hdfDeathReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Death Source
                            hdfDeathRecordCaseRecordID.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Death Source
                            hdfDeathRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Death Source
                            hdfDeathRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Death Source
                            hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                            break;
                        }
                        else
                        {
                            hdfDeathReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Death Source
                            hdfDeathRecordCaseRecordID.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Death Source
                            hdfDeathRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Death Source
                            hdfDeathRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Death Source
                            hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                        }
                    }
                    else
                    {
                        hdfDeathReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Death Source
                        hdfDeathRecordCaseRecordID.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Death Source
                        hdfDeathRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Death Source
                        hdfDeathRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Death Source
                        hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                    }
                }
                else
                {
                    hdfDeathReportID.Value = Convert.ToString(dt.Rows[4][i]);              // Case Report ID from Death Source
                    hdfDeathRecordCaseRecordID.Value = Convert.ToString(dt.Rows[5][i]);     // Case Record ID from Death Source
                    hdfDeathRecordBirthRecNo.Value = Convert.ToString(dt.Rows[6][i]);        // Birth Rec. No from Death Source
                    hdfDeathRecordDeathRecNo.Value = Convert.ToString(dt.Rows[7][i]);        // Death Rec. No from Death Source
                    hdfRecordFrom.Value = Convert.ToString(dt.Rows[0][i]);
                }
            }

            string caseDId = hdfisFirstCheckedDetailsID.Value;

            if (!string.IsNullOrEmpty(caseDId))
            {
                DataTable dtScruClearView = new DataTable();
                string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty;

                //Match Record
                var multipleReturnValue = CommonMatchDID(caseDId, dt);
                dtScruClearView = multipleReturnValue.Item1 as DataTable;
                reportId = multipleReturnValue.Item2 as string;
                birthId = multipleReturnValue.Item3 as string;
                deathId = multipleReturnValue.Item4 as string;

                DataRow dr = dt.NewRow();
                dr[0] = "IsMatchRecord";

                for (int i = 2; i < dt.Columns.Count; i++)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (string.IsNullOrEmpty(hdfDetailsID.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[5][i])))
                        {
                            dr[i] = "Check for Linking Process";
                            break;
                        }
                        else if (string.IsNullOrEmpty(reportId) && string.IsNullOrEmpty(birthId) && string.IsNullOrEmpty(deathId))
                        {
                            dr[i] = "Unmatched";
                            break;
                        }

                        else if (!string.IsNullOrEmpty(reportId) && (dt.Rows[4].ItemArray[i].ToString() == reportId) && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(reportId) && dt.Rows[4].ItemArray[i].ToString() == reportId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(birthId) && dt.Rows[6].ItemArray[i].ToString() == birthId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(deathId) && dt.Rows[7].ItemArray[i].ToString() == deathId)
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking")
                        {
                            if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value))
                            {
                                if (hdfCaseReportCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[72].ItemArray[i]) == deathId) // change to deathID 77 is PhantomHit
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfCaseReportDeathRecNo.Value)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))
                                {
                                    if (!string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && !string.IsNullOrEmpty(hdfJSBirth_DeathRecNo.Value) && hdfJSBirth_DeathRecNo.Value == Convert.ToString(dt.Rows[7].ItemArray[i]))
                                    {
                                        dr[i] = "Matched";
                                        collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                        hdfDeathIDCurrentPageValue.Value = "awol";
                                        break;
                                    }
                                    else if (!string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))) && Convert.ToString(hdfJSCase_BirthRecNo.Value) == Convert.ToString(dt.Rows[6].ItemArray[i]))
                                    {
                                        dr[i] = "Matched";
                                        collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                        hdfDeathIDCurrentPageValue.Value = "awol";
                                        break;
                                    }
                                    else if (string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))) && !string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)))
                                    {
                                        if (!string.IsNullOrEmpty(hdfCaseReportDidMatch.Value) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
                                        {
                                            dr[i] = "Unmatched";
                                            hdfCaseReportDidMatch.Value = string.Empty;
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfDeathIDCurrentPageValue.Value = "awol";
                                            break;

                                        }
                                        else if (string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && string.IsNullOrEmpty(hdfJSDeath_BirthRecNo.Value))
                                        {
                                            if (hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1].ItemArray[i])))
                                            {
                                                hdfDetailsID.Value = hdfDetailsID.Value.Replace(Convert.ToString(dt.Rows[1].ItemArray[i]), "").TrimEnd(',').TrimStart(',');
                                            }
                                            dr[i] = "Unmatched";
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfDeathIDCurrentPageValue.Value = "awol";
                                            break;
                                        }
                                        else if (!string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && string.IsNullOrEmpty(hdfJSBirth_DeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))
                                        {
                                            dr[i] = "Unmatched";
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfDeathIDCurrentPageValue.Value = "awol";
                                            break;
                                        }
                                        else if (!string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && string.IsNullOrEmpty(hdfJSBirth_DeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (Convert.ToString(hdfJSBirth_BirthRecNo.Value) == Convert.ToString(dt.Rows[6].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))
                                        {
                                            dr[i] = "Unmatched";
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfDeathIDCurrentPageValue.Value = "awol";
                                            break;
                                        }
                                        else
                                        {
                                            dr[i] = "Unmatched";
                                            break;
                                        }

                                    }
                                    else if (!string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])))
                                    {

                                        // Birth Condition
                                        if (!string.IsNullOrEmpty(hdfJSCase_BirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])))
                                        {
                                            if (hdfJSCase_BirthRecNo.Value == Convert.ToString(dt.Rows[6].ItemArray[i]))
                                            {
                                                dr[i] = "Matched";
                                                collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                                hdfDeathIDCurrentPageValue.Value = "awol";
                                                break;
                                            }
                                            else
                                            {
                                                dr[i] = "Unmatched";
                                                break;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(hdfJSCase_DeathRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))
                                        {
                                            //Death check
                                            dr[i] = "Unmatched";
                                            collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                            hdfDeathIDCurrentPageValue.Value = "awol";
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportID.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i])) && Convert.ToString(dt.Rows[4].ItemArray[i]) == hdfCaseReportID.Value)
                            {
                                dr[i] = "Matched";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i]))))  // birth
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else if (!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))  // death
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedCR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            // condition to do reverse link process when  death source do not contain case record ID and data do not conflict From REPORT Source

                            else if ((!string.IsNullOrEmpty(hdfCaseReportCaseRecordId.Value) && string.IsNullOrEmpty(hdfCaseReportDeathRecNo.Value)) && (string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7][i]))))
                            {
                                // check if the birth rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) != !string.IsNullOrEmpty(hdfCaseReportBirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }

                            // condition to do reverse link process when  death source do not contain case record ID and data do not conflict From BIRTH Source

                            else if ((!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && (string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[7][i]))))
                            {
                                // check if the death rec no exists and do not match 

                                if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) != !string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)))
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "tieit";
                                    hdftieit.Value = "tieit";
                                    break;
                                }
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }

                        else if (hdfFirstTable.Value == "BirthRecords")
                        {
                            if (!string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value))
                            {
                                if (hdfBirthRecordCaseRecordId.Value == Convert.ToString(dt.Rows[5].ItemArray[i]) || !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[72].ItemArray[i]) == deathId)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && !string.IsNullOrEmpty(deathId) && Convert.ToString(dt.Rows[7].ItemArray[i]) == hdfBirthRecordDeathRecNo.Value)
                                {
                                    dr[i] = "Matched";
                                    break;
                                }
                                else if (string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))
                                {
                                    collectCaseReportDid = AddRemoveItem(collectCaseReportDid, Convert.ToString(dt.Rows[1].ItemArray[i]), null);
                                    dr[i] = "Unmatched"; // for link process if case ID exsit but birth/death do not exist. Birth/DeathID's avaiable in another source without Case ID
                                    hdfDeathIDCurrentPageValue.Value = "awol";
                                }
                                else
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                            }
                            else if (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && Convert.ToString(dt.Rows[6].ItemArray[i]) == hdfBirthRecordBirthRecNo.Value)
                            {
                                dr[i] = "Matched";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                }
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4].ItemArray[i]))))
                            // report
                            // condition no case record ID on both cases and checking the required relevent ID's for tables i.e Birth Rec. no in Birth source and Death Rec. no in death source
                            {
                                dr[i] = "Nonpareil";
                                break;
                            }
                            else if (string.IsNullOrEmpty(hdfBirthRecordCaseRecordId.Value) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))  // death
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && !string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && Convert.ToString(dt.Rows[7].ItemArray[i]) != hdfBirthRecordDeathRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[6].ItemArray[i])) && !string.IsNullOrEmpty(hdfJSBirth_BirthRecNo.Value) && Convert.ToString(dt.Rows[6].ItemArray[i]) != hdfJSBirth_BirthRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Nonpareil";
                                    break;
                                }
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }
                        else if (hdfFirstTable.Value == "DeathRecords")  // return to same first table
                        {
                            if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(hdfCaseReportID.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i]))))  // report
                            {
                                dr[i] = "Nonpareil";


                                if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])) && !string.IsNullOrEmpty(hdfBirthRecordDeathRecNo.Value) && Convert.ToString(dt.Rows[7].ItemArray[i]) != hdfBirthRecordDeathRecNo.Value)
                                {
                                    dr[i] = "Unmatched";
                                    break;
                                }
                                else
                                {
                                    dr[i] = "Nonpareil";
                                    break;
                                }
                            }
                            else if (string.IsNullOrEmpty(hdfDeathRecordCaseRecordID.Value) && (!string.IsNullOrEmpty(hdfDeathRecordDeathRecNo.Value)) && string.IsNullOrEmpty(Convert.ToString(dt.Rows[5].ItemArray[i])) && (!string.IsNullOrEmpty(hdfBirthRecordBirthRecNo.Value)) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[7].ItemArray[i])))) // death
                            {
                                dr[i] = "Nonpareil";
                                string didPickupfromResTable = Convert.ToString(dt.Rows[1].ItemArray[i]);
                                if (!Convert.ToString(hdfDetailsID.Value).Contains(didPickupfromResTable))
                                {
                                    if (hdfisCheckedDR.Value == "true")
                                    {
                                        hdfDetailsID.Value = hdfDetailsID.Value + "," + didPickupfromResTable;
                                    }
                                }
                                break;
                            }
                            else
                            {
                                dr[i] = "Unmatched";
                                break;
                            }
                        }
                        else
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                    }

                }

                string value = null;
                if (collectCaseReportDid != null && collectCaseReportDid.Count > 0)
                {
                    foreach (var item in collectCaseReportDid)
                    {
                        if (string.IsNullOrEmpty(value))
                        {
                            value = item;
                        }
                        else
                        {
                            value = value + "," + item;
                        }


                    }
                    hdfCaseReportDidMatch.Value = value;
                }
                dt.Rows.Add(dr);
                if (dtScruClearView.Rows.Count == 0)
                {
                    dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
                }

                foreach (DataRow drSC in dtScruClearView.Rows)
                {
                    if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string className1 = btnBirthRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = className1 + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnBirthRecords.CssClass;
                                btnBirthRecords.Attributes.Remove("onclick");
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = className1 + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string className1 = btnDeathRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = className1 + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = className1 + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string className1 = btnCaseReports.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = className1 + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnCaseReports.CssClass = className1 + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }

                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string className1 = btnDeathRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = className1 + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = className1 + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                    {
                        foreach (DataRow dr2 in dtScruClearView.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string className1 = btnCaseReports.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = className1 + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnCaseReports.CssClass = className1 + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }

                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string className1 = btnBirthRecords.CssClass;
                                    if (className1.Contains("btn-disabled"))
                                    {
                                        className1 = className1.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = className1 + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                                {
                                    btnBirthRecords.Enabled = false;
                                    btnBirthRecords.CssClass = className1 + " btn-disabled";
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (dt != null)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1[0] = "IsMatchRecord";
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (string.IsNullOrEmpty(hdfDetailsID.Value))
                            {
                                dr1[i] = "Check for Linking Process";
                            }
                        }
                    }
                    dt.Rows.Add(dr1);
                    if (dt.Columns.Count > 2)
                    {
                        for (int i = 2; i < dt.Columns.Count; i++)
                        {
                            if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                            {
                                hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                            }
                        }
                    }
                }
                //scrutinizerdatarecords pull from ScrutinizerClearView Table
                DataTable dtSc = new DataTable();
                Session["scrutinizerdatarecords"] = null;

                dtSc = CheckscrutinizerIdValue();
                Session["scrutinizerdatarecords"] = dtSc;

                foreach (DataRow dr in dtSc.Rows)
                {
                    if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string classNameblank = btnBirthRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnBirthRecords.CssClass;

                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string classNameblank = btnDeathRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string classNameblank = btnCaseReports.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = classNameblank + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                            {
                                if (hdfTableKeyName.Value != "Death Records")
                                {
                                    string classNameblank = btnDeathRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                    btnDeathRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                    btnDeathRecords.Enabled = false;
                                }
                            }
                        }
                    }
                    else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                    {
                        foreach (DataRow dr2 in dtSc.Rows)
                        {
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                            {
                                if (hdfTableKeyName.Value != "Case Reports")
                                {
                                    string classNameblank = btnCaseReports.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnCaseReports.CssClass = classNameblank + " activeGreen";
                                    btnCaseReports.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                    btnCaseReports.Enabled = false;
                                }
                            }
                            if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                            {
                                if (hdfTableKeyName.Value != "Birth Records")
                                {
                                    string classNameblank = btnBirthRecords.CssClass;
                                    if (classNameblank.Contains("btn-disabled"))
                                    {
                                        classNameblank = classNameblank.Replace("btn-disabled", "");
                                    }
                                    btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                    btnBirthRecords.Enabled = true;
                                    break;
                                }
                            }
                            else
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                                {
                                    btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                    btnBirthRecords.Enabled = false;
                                }
                            }
                        }
                    }
                }
            }

            //Similar data green check enable

            DataTable dtSimilar = new DataTable();
            Session["scrutinizersimilardatarecords"] = null;

            dtSimilar = CheckSimilarCandidates();
            Session["scrutinizersimilardatarecords"] = dtSimilar;

            if (dtSimilar != null && dtSimilar.Rows.Count > 0)
            {
                string classSimilarName = btnSimilar.CssClass;
                if (classSimilarName.Contains("btn-disabled"))
                {
                    classSimilarName = classSimilarName.Replace("btn-disabled", "");
                }
                btnSimilar.CssClass = classSimilarName + " activeGreen";
                btnSimilar.Enabled = true;
            }
            else
            {
                string classSimilarName = btnSimilar.CssClass;
                if (!btnSimilar.CssClass.Contains("btn-disabled") && !classSimilarName.Contains("active"))
                {
                    btnSimilar.CssClass = classSimilarName + " btn-disabled";
                    btnSimilar.Enabled = false;
                }
            }

            if (Session["dtMaintain"] != null)
            {
                Session["dtMaintain"] = null;
            }
            Session["dtMaintain"] = dt;
            Session["datarecords"] = null;
            Session["datarecords"] = dt;

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_DeathRecord");
        }
    }

    protected void btnDeathRecords_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedDR.Value = "true";

            if (hdfIsTabClickedBR.Value == "true")
            {
                string btnCR = btnCaseReports.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedSM.Value == "true")
                {
                    if (hdfIsTabClickedCR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnCR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnCR.Contains("active") && !btnSM.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }
            else if (hdfIsTabClickedCR.Value == "true")
            {
                string btnBR = btnBirthRecords.CssClass;
                string btnSM = btnSimilar.CssClass;
                if (hdfIsTabClickedBR.Value == "true")
                {
                    if (hdfIsTabClickedSM.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnSM.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedSM.Value == "true")
                {
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnBR.Contains("active") && !btnSM.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }
            else
            {
                string btnBR = btnBirthRecords.CssClass;
                string btnCR = btnCaseReports.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (hdfIsTabClickedBR.Value == "true")
                {
                    if (hdfIsTabClickedCR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnCR.Contains("active"))
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                }
                else if (!btnCR.Contains("active") && !btnBR.Contains("active"))
                {
                    hdfIsTabClicked.Value = "true";
                }
            }

            DeathRecord();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnDeathRecords_Click");
        }
    }

    public static string ResponseMessage(string ErrorCode)
    {
        string retMessage = string.Empty;
        switch (ErrorCode)
        {
            case "END_OF_WORK":
                retMessage = "There are no other matches available.";
                break;
            case "UNAVAILABLE":
                retMessage = "Server unavailable.";
                break;
            case "NO_COMMAND":
                retMessage = "No command was provided.";
                break;
            case "BAD_COMMAND":
                retMessage = "No such command available.";
                break;
            case "NO_USER":
                retMessage = "No user was provided.";
                break;
            case "UNKNOWN_USER":
                retMessage = "Provide user details.";
                break;
            case "NO_DETAILS":
                retMessage = "No details provided.";
                break;
            case "NOT_ASSIGNED":
                retMessage = "Operation not allowed. Click Own to take the ownership.";
                break;
            case "NOT_OWNER":
                retMessage = "Operation not allowed. Click Own to take the ownership.";
                break;
            case "NOT_VALID":
                retMessage = "Resources not valid.";
                break;
            case "BAD_STATE":
                retMessage = "Resource state is invalid.";
                break;
            case "NO_SCRUTINIZER":
                retMessage = "Resource does not exist.";
                break;
            case "EXCEPTION":
                retMessage = "Unhandled exception encountered.";
                break;
            case "BAD_GRAMMAR":
                retMessage = "Bad JSON syntax.";
                break;
            case "STOLEN":
                retMessage = "Ownership changed.";
                break;
            case "BAD_DETAIL":
                retMessage = "Invalid detail context.";
                break;
            case "TETRIS_FAIL":
                retMessage = "TETRIS Error. Invalid Selection ";
                break;
            case "INCOMPLETE":
                retMessage = "Work unit is incomplete or invalid.";
                break;
        }
        retMessage = $"<b><span class=\"red-italic\">{retMessage}</span></b>";
        return retMessage;
    }
    
    protected void OwnerShip()
    {
        try
        {
            Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string pplTbl = "Users";
                string ScrutinizersTbl = "Scrutinizers";
                string query = "";
                query = string.Format(@"SELECT ppl.firstName, ppl.displayName, scrut.assignedUserId FROM {0}.{1} ppl
                                        INNER JOIN {0}.{2} scrut ON ppl.userId = scrut.assignedUserId
                                        WHERE scrut.scrutinizerId = {3}", pplSchema, pplTbl, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                getMsgs = $"M&LDetails|M-OwnerShip|{query}";
                ErrorLog(getMsgs);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(pplRow);
            }
            string ClaimedRow = " is currently working on this candidate case report.";
            string unClaimedRow = "Please click Own to start working on this candidate case report.";
            string preOwner = string.Empty;
            string env = ConfigurationManager.AppSettings["environment"];
            if (env == "prod")
            {
                preOwner = " is currently working on this candidate case report. Please select another report to work.";
            }
            else
            {
                preOwner = " is currently working on this candidate case report. Please click Own to start working on the report.";
            }

            if (pplRow.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) == Convert.ToString(Session["userid"]))
                {
                    assignedUserName = (pplRow.Tables[0].Rows[0][1].ToString() + Convert.ToString(ClaimedRow));
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) != Convert.ToString(Session["userid"]))
                {
                    assignedUserName = pplRow.Tables[0].Rows[0][1].ToString() + preOwner;
                }
                else
                {
                    assignedUserName = unClaimedRow;
                }
            }
            else
            {
                assignedUserName = unClaimedRow;
            }
            lblSockerResponse.Text = null;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_OwnerShip");
        }
    }

    protected string IsOwnerShip()
    {
        string assignedUserID = string.Empty;
        try
        {
            Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string pplTbl = "Users";
                string ScrutinizersTbl = "Scrutinizers";
                string query = "";
                query = string.Format(@"SELECT ppl.firstName, ppl.displayName,scrut.assignedUserId FROM {0}.{1} ppl
                                        INNER JOIN {0}.{2} scrut ON ppl.userId = scrut.assignedUserId
                                        WHERE scrut.scrutinizerId = {3}", pplSchema, pplTbl, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LDetails|IsOwnerShip|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(pplRow);
            }
            if (pplRow.Tables[0].Rows.Count > 0)
            {
                assignedUserID = Convert.ToString(pplRow.Tables[0].Rows[0][2]);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_IsOwnerShip");
        }
        return assignedUserID;
    }

    protected void NewOwnerShip(string paramValue)
    {
        try
        {
            string scrutinizerId = paramValue;
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string pplTbl = "Users";
                string ScrutinizersTbl = "Scrutinizers";
                string query = "";
                query = string.Format(@"SELECT ppl.firstName, ppl.displayName, scrut.assignedUserId FROM {0}.{1} ppl
                                        INNER JOIN {0}.{2} scrut ON ppl.userId = scrut.assignedUserId
                                        WHERE scrut.scrutinizerId = {3}", pplSchema, pplTbl, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LDetails|NewOwnerShip|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(pplRow);
            }
            string ClaimedRow = " is currently working on this candidate case report.";
            string unClaimedRow = "Please click Own to start working on this candidate case report.";
            string preOwner = " is currently working on this candidate case report. Please click Own to start working on the report.";

            if (pplRow.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) == Convert.ToString(Session["userid"]))
                {
                    assignedUserName = (pplRow.Tables[0].Rows[0][1].ToString() + Convert.ToString(ClaimedRow));
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) != Convert.ToString(Session["userid"]))
                {
                    assignedUserName = pplRow.Tables[0].Rows[0][1].ToString() + preOwner;
                }
                else
                {
                    assignedUserName = unClaimedRow;
                }
            }
            else
            {
                assignedUserName = unClaimedRow;
            }
            lblSockerResponse.Text = null;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_NewOwnerShip");
        }
    }
    
    protected DataTable CheckSimilarCandidates()
    {
        DataTable dt = new DataTable();
        Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
        try
        {
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string ScrutinizersTbl = "Matching_Linking";
                string query = "";
                query = string.Format(@"SELECT * FROM {0}.{1} scrut WHERE scrut.scrutinizerId = {2} and scrut.similarSIDs is not null", pplSchema, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LDetails|CheckSimilarCandidates|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CheckSimilarCandidates");
        }
        return dt;
    }
    
    protected DataTable CheckscrutinizerIdValue()
    {
        DataTable dt = new DataTable();
        Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
        try
        {
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string ScrutinizersTbl = "ScrutinizerClearView";
                string query = "";
                query = string.Format(@"SELECT * FROM {0}.{1} scrut WHERE scrut.scrutinizerId = {2}", pplSchema, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LDetails|CheckscrutinizerIdValue|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CheckscrutinizerIdValue");
        }
        return dt;
    }
    
    protected List<MatchingViewModel> ListRecords(DataSet dsRows, bool isDetailId)
    {
        List<MatchingViewModel> lstMatches = new List<MatchingViewModel>();
        string tableName = Convert.ToString(hdfTableName.Value);
        string tableRecName = string.Empty;
        foreach (DataRow dr in dsRows.Tables[0].Rows)
        {
            var item = new MatchingViewModel();

            if (tableName == "DeathRecords")
            {
                tableRecName = "Death Records";
                hdfDeathRecordCaseRecordID.Value = Convert.ToString(dr["caseRecId"]);
            }
            else if (tableName == "BirthRecords")
            {
                tableRecName = "Birth Records";
                hdfBirthRecordCaseRecordId.Value = Convert.ToString(dr["caseRecId"]);
            }
            else if (tableName == "SimilarCandidatesView")
            {
                tableRecName = "Similar Records";
            }
            else
            {
                tableRecName = "Case Reports";
                hdfCaseReportCaseRecordId.Value = Convert.ToString(dr["caseRecId"]);
            }

            if (tableRecName != "Similar Records")
            {
                item.RecordFrom = tableRecName;
                item.ScrutinizerID = Convert.ToInt32(dr["scrutinizerId"]);
                item.ReportID = Convert.ToString(dr["reportId"]);
                item.MasterRecordNumber = Convert.ToString(dr["masterRecordNumber"]);
                item.DeathNumber = Convert.ToString(dr["deathNumber"]);
                item.CaseRecID = Convert.ToString(dr["caseRecId"]);
                item.BirthCertNumber = Convert.ToString(dr["birthCertNumber"]);
                item.Confidence = Convert.ToString(dr["confidence"]);
                item.MatchLevel = Convert.ToString(dr["matchLevel"]);
                item.Trips = Convert.ToString(dr["trips"]);
                item.ChildLastName = Convert.ToString(dr["childLastName"]);
                item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
                item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
                item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
                item.AliasFirstName = Convert.ToString(dr["aliasFirstName"]);
                item.AliasLastName = Convert.ToString(dr["aliasLastName"]);
                item.AliasMiddleName = Convert.ToString(dr["aliasMiddleName"]);
                item.Address1 = Convert.ToString(dr["address1"]);
                item.Address2 = Convert.ToString(dr["address2"]);
                item.City = Convert.ToString(dr["city"]);
                item.County = Convert.ToString(dr["county"]);
                item.State = Convert.ToString(dr["state"]);
                item.Country = Convert.ToString(dr["country"]);
                item.ZipCode = Convert.ToString(dr["zipcode"]);
                item.ChildSSN = Convert.ToString(dr["childSSN"]);
                item.ChildMRN = Convert.ToString(dr["childMRN"]);
                item.Gender = Convert.ToString(dr["gender"]);
                item.Plurality = Convert.ToString(dr["plurality"]);
                item.BirthOrder = Convert.ToString(dr["birthOrder"]);
                item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
                item.BirthWeight = Convert.ToString(dr["birthWeight"]);
                item.ChildMedicaidNumber = Convert.ToString(dr["childMedicaidNumber"]);
                item.BirthDate = Convert.ToString(dr["birthDate"]);
                item.BirthHospital = Convert.ToString(dr["birthHospital"]);
                item.MomSSN = Convert.ToString(dr["momSSN"]);
                item.MomFirstName = Convert.ToString(dr["momFirstName"]);
                item.MomLastName = Convert.ToString(dr["momLastName"]);
                item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
                item.MomSuffix = Convert.ToString(dr["momSuffix"]);
                item.AdmittingEntityCode = Convert.ToString(dr["admittingEntityCode"]);
                item.PatientType = Convert.ToString(dr["patientType"]);
                item.AdmissionSource = Convert.ToString(dr["admissionSource"]);
                item.AdmissionDate = Convert.ToString(dr["admissionDate"]);
                item.DischargeDate = Convert.ToString(dr["dischargeDate"]);
                item.ICD9SyndromeCode = Convert.ToString(dr["ICD9SyndromeCode"]);
                item.ICD10SyndromeCode = Convert.ToString(dr["ICD10SyndromeCode"]);
                item.CytogeneticsTesting = Convert.ToString(dr["cytogeneticsTesting"]);
                item.ICD9CytogeneticsDiagnosisCodes = Convert.ToString(dr["ICD9CytogeneticsDiagnosisCodes"]);
                item.ICD10CytogeneticsDiagnosisCodes = Convert.ToString(dr["ICD10CytogeneticsDiagnosisCodes"]);
                item.LabCode = Convert.ToString(dr["labCode"]);
                item.HeadCircumference = Convert.ToString(dr["headCircumference"]);
                item.DeliveryLength = Convert.ToString(dr["deliveryLength"]);
                item.InfantPostnatalEcho = Convert.ToString(dr["infantPostnatalEcho"]);
                item.AgeFirstPostnatalEcho = Convert.ToString(dr["ageFirstPostnatalEcho"]);
                item.DateFirstPostnatalEcho = Convert.ToString(dr["dateFirstPostnatalEcho"]);
                item.InfantAdmitted = Convert.ToString(dr["infantAdmitted"]);
                item.AgeInfantAdmitted = Convert.ToString(dr["ageInfantAdmitted"]);
                item.AgeInfantDischarged = Convert.ToString(dr["ageInfantDischarged"]);
                item.AgeInfantAdmittedICU = Convert.ToString(dr["ageInfantAdmittedICU"]);
                item.DateInfantAdmittedICU = Convert.ToString(dr["dateInfantAdmittedICU"]);
                item.ReceivedTimestamp = Convert.ToString(dr["receivedTimestamp"]);
                item.ProcessedTimestamp = Convert.ToString(dr["processedTimestamp"]);
                item.AssimilatedTimestamp = Convert.ToString(dr["assimilatedTimestamp"]);
                item.SearchContext = Convert.ToString(dr["searchContext"]);
                item.MessageState = Convert.ToString(dr["messageState"]);
                item.HistoricId = Convert.ToString(dr["HistoricId"]);
                item.ArchiveID = Convert.ToString(dr["archiveId"]);
                item.IsBirthId = Convert.ToString(dr["birthId"]);
                item.IsDeathId = Convert.ToString(dr["deathId"]);
                item.PhantomHit = Convert.ToString(dr["phantomHit"]);

                if (isDetailId)
                {
                    item.DetailID = Convert.ToString(dr["detailId"]);
                }
            }
            else
            {
                item.RecordFrom = tableRecName;
                item.ScrutinizerID = Convert.ToInt32(dr["scrutinizerId"]);
                item.ChildLastName = Convert.ToString(dr["childLastName"]);
                item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
                item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
                item.AliasLastName = Convert.ToString(dr["aliasLastName"]);
                item.AliasFirstName = Convert.ToString(dr["aliasFirstName"]);
                item.AliasMiddleName = Convert.ToString(dr["aliasMiddleName"]);
                item.ChildSSN = Convert.ToString(dr["childSSN"]);
                item.ChildMRN = Convert.ToString(dr["childMRN"]);
                item.AdmittingEntityCode = Convert.ToString(dr["admittingEntityCode"]);
                item.ChildMedicaidNumber = Convert.ToString(dr["childMedicaidNumber"]);
                item.Address1 = Convert.ToString(dr["address1"]);
                item.City = Convert.ToString(dr["city"]);
                item.County = Convert.ToString(dr["county"]);
                item.State = Convert.ToString(dr["state"]);
                item.ZipCode = Convert.ToString(dr["zipcode"]);
                item.Gender = Convert.ToString(dr["gender"]);
                item.Plurality = Convert.ToString(dr["plurality"]);
                item.BirthOrder = Convert.ToString(dr["birthOrder"]);
                item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
                item.BirthWeight = Convert.ToString(dr["birthWeight"]);
                item.BirthHospital = Convert.ToString(dr["birthHospital"]);
                item.MomLastName = Convert.ToString(dr["momLastName"]);
                item.MomFirstName = Convert.ToString(dr["momFirstName"]);
                item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
            }
            lstMatches.Add(item);
        }
        return lstMatches;
    }

    protected DataTable CommonMethod(string query, int paramValue = 0)
    {
        try
        {
            string includeColumns = "";
            string readonlyColumns = "";
            string dataKeyname = "";
            int? scrutinizerId = Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value));
            string orderM = "order by l.similarSIDs desc";

            DataSet dsRows = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string queryM = string.Empty;
                string whereM = string.Empty;

                if (paramValue != 0)
                {
                    whereM = "where scrutinizerId=" + paramValue + "";
                    queryM = string.Format("select top 1 * from {0}.{1} {2} {3}", "MBDR_System", "Matching_Linking l", whereM, orderM);
                }
                else
                {
                    whereM = "where scrutinizerId=" + scrutinizerId + "";
                    queryM = string.Format("select top 1 * from {0}.{1} {2} {3}", "MBDR_System", "Matching_Linking l", whereM, orderM);
                }

                SqlCommand cmd = new SqlCommand(queryM, conn);
                getMsgs = $"M&LDetails|CommonMethod|{queryM}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dsRows);
                foreach (DataRow dr in dsRows.Tables[0].Rows)
                {
                    hdfArchiveID.Value = Convert.ToString(dr["archiveId"]);
                    hdfLastName.Value = Convert.ToString(dr["childLastName"]);
                    hdfFirstName.Value = Convert.ToString(dr["childFirstName"]);
                    hdfDOB.Value = Convert.ToString(dr["birthDate"]);
                    hdfMomLastName.Value = Convert.ToString(dr["momLastName"]);
                    hdfMomFirstName.Value = Convert.ToString(dr["momFirstName"]);
                    hdfAddress1.Value = Convert.ToString(dr["address1"]);
                    hdfSimilarIDs.Value = Convert.ToString(dr["similarSIDs"]);
                }
            }

            hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());

            if (paramValue != 0)
            {
                hdfScrutinizerID.Value = Convert.ToString(paramValue);
            }
            else
            {
                hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());
            }

            List<MatchingViewModel> lstMatches = ListRecords(dsRows, false);

            string where = string.Empty;

            if (paramValue != 0)
            {
                where = "where scrutinizerId=" + paramValue + "";
                query = string.Format(query + "{0}", where);
                scrutinizerId = Convert.ToInt32(paramValue);
            }
            else
            {
                if (hdfTableKeyName.Value == "Similar Records")
                {
                    where = "where scrutinizerId in (" + hdfSimilarIDs.Value + ")";
                    query = string.Format(query + "{0}", where);
                }
                else
                {
                    where = "where scrutinizerId=" + scrutinizerId + "";
                    query = string.Format(query + "{0}", where);
                }
            }

            List<MatchingViewModel> lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);
            lstMatches.AddRange(lstMatchesRecords);

            var row = lstMatches.Where(s => s.ScrutinizerID == scrutinizerId).FirstOrDefault();
            PropertyInfo[] properties = typeof(MatchingViewModel).GetProperties();
            dt = new DataTable();
            if (row != null)
            {
                for (int i = 0; i < lstMatches.Count + 1; i++)
                {
                    dt.Columns.Add();
                }
                for (int i = 0; i < properties.Length; i++)
                {
                    DataRow dr = dt.NewRow();
                    var filter = Common.GetFilterList().Where(s => s.Schema == hdfSchema.Value && s.Table.StartsWith(hdfTableName.Value)).FirstOrDefault();
                    if (filter != null)
                    {
                        includeColumns = filter.IncludedColumns;
                        readonlyColumns = filter.ReadonlyColumns;
                        dataKeyname = filter.DataKeyName;
                    }

                    List<string> lstColumns = includeColumns.Split(',').ToList();
                    var colName = lstColumns.FirstOrDefault(s => s.StartsWith(properties[i].Name));
                    string displayColname = string.Empty;
                    List<ColumnList> lstRecord = new List<ColumnList>();
                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnName = colName.Split(':')[1];
                        dr[0] = columnName;
                        displayColname = columnName;
                    }
                    else
                    {
                        displayColname = properties[i].Name;

                        foreach (var col in lstColumns)
                        {
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                            {
                                dr[0] = columnName.Split(':')[1];
                            }
                        }
                    }
                    if (displayColname != "RecordFrom" && displayColname != "IsMatchRecord")
                    {
                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(properties[i].GetValue(row, null)) });
                    }
                    dr[1] = Convert.ToString(properties[i].GetValue(row, null));
                    for (int j = 0; j < lstMatches.Count; j++)
                    {
                        dr[j + 1] = Convert.ToString(properties[i].GetValue(lstMatches[j], null));
                    }
                    if (dr[0] != null)
                    {
                        dt.Rows.Add(dr);
                    }
                }
                Session["datarecords"] = dt;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CommonMethod");
        }
        return dt;
    }

    protected DataTable CommonMethodOnlyForFecthRecord(string query, string tablename)
    {
        DataTable dtFR = new DataTable();
        try
        {
            string includeColumns = "";
            string readonlyColumns = "";
            string dataKeyname = "";
            int? scrutinizerId = Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value));

            DataSet dsRows = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {

                string whereM = "where scrutinizerId=" + scrutinizerId + "";
                string queryM = string.Format("select top 1 * from {0}.{1} {2}", "MBDR_System", "Matching_Linking", whereM);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                getMsgs = $"M&LDetails|CommonMethodOnlyForFecthRecord|{queryM}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dsRows);
                foreach (DataRow dr in dsRows.Tables[0].Rows)
                {
                    hdfArchiveID.Value = Convert.ToString(dr["archiveId"]);
                    hdfLastName.Value = Convert.ToString(dr["childLastName"]);
                    hdfFirstName.Value = Convert.ToString(dr["childFirstName"]);
                    hdfDOB.Value = Convert.ToString(dr["birthDate"]);
                    hdfMomLastName.Value = Convert.ToString(dr["momLastName"]);
                    hdfMomFirstName.Value = Convert.ToString(dr["momFirstName"]);
                    hdfAddress1.Value = Convert.ToString(dr["address1"]);
                }
            }

            hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());
            List<MatchingViewModel> lstMatches = ListRecords(dsRows, false);

            string where = " where SC.scrutinizerId = " + scrutinizerId + "";
            query = string.Format(query + "{0}", where);

            List<MatchingViewModel> lstMatchesRecords = GetAllDetails(hdfSchema.Value, tablename, scrutinizerId, query);
            lstMatches.AddRange(lstMatchesRecords);

            var row = lstMatches.Where(s => s.ScrutinizerID == scrutinizerId).FirstOrDefault();
            PropertyInfo[] properties = typeof(MatchingViewModel).GetProperties();
            dtFR = new DataTable();
            if (row != null)
            {
                for (int i = 0; i < lstMatches.Count + 1; i++)
                {
                    dtFR.Columns.Add();
                }
                for (int i = 0; i < properties.Length; i++)
                {
                    DataRow dr = dtFR.NewRow();
                    var filter = Common.GetFilterList().Where(s => s.Schema == hdfSchema.Value && s.Table.StartsWith(hdfTableName.Value)).FirstOrDefault();
                    if (filter != null)
                    {
                        includeColumns = filter.IncludedColumns;
                        readonlyColumns = filter.ReadonlyColumns;
                        dataKeyname = filter.DataKeyName;
                    }

                    List<string> lstColumns = includeColumns.Split(',').ToList();
                    var colName = lstColumns.FirstOrDefault(s => s.StartsWith(properties[i].Name));
                    string displayColname = string.Empty;
                    List<ColumnList> lstRecord = new List<ColumnList>();
                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnName = colName.Split(':')[1];
                        dr[0] = columnName;
                        displayColname = columnName;
                    }
                    else
                    {
                        displayColname = properties[i].Name;

                        foreach (var col in lstColumns)
                        {
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                            {
                                dr[0] = columnName.Split(':')[1];
                            }
                        }
                    }
                    if (displayColname != "RecordFrom" && displayColname != "IsMatchRecord")
                    {
                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(properties[i].GetValue(row, null)) });
                    }
                    dr[1] = Convert.ToString(properties[i].GetValue(row, null));
                    for (int j = 0; j < lstMatches.Count; j++)
                    {
                        dr[j + 1] = Convert.ToString(properties[i].GetValue(lstMatches[j], null));
                    }
                    if (dr[0] != null)
                    {
                        dtFR.Rows.Add(dr);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CommonMethodOnlyForFecthRecord");
        }
        return dtFR;
    }

    protected void lnkCaseReport_Click(object sender, EventArgs e) // RV this
    {
        try
        {
            string url = "DeepStats.aspx?reportType=" + hdfTableName.Value + "&archiveId=" + Uri.EscapeDataString(hdfArchiveID.Value);
            OwnerShip();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('" + url + "','_target','width=600,height=300');", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_lnkCaseReport_Click");
        }
    }

    protected void btnRelease_Click(object sender, EventArgs e)
    {
        try
        {
            isOwnerID = IsOwnerShip();

            if (!isOwnerID.Equals(Session["userid"]))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error.", "errorMsgs(' Another user has taken ownership of this SID. You are no longer able to perform matching and linking for this Candidate. ', 'Error Message');", true);
            }

            string json = "{\"cmd\":\"RELEASE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Session["paramValue"]) + "}";

            string response = SocketConn(json);
            getMsgs = $"btnRelease_Click|Request|{json}|Response|{response}";
            ErrorLog(getMsgs);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    string script = @"                    
                                        ShowProgress();
                                        __doPostBack('" + btnAdvanceResult.UniqueID + @"', '');";
                    ClientScript.RegisterStartupScript(this.GetType(), "PostBackScript", script, true);
                }
                else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                {
                    hdfSecurityCheck.Value = "sv";  // sv - assigng security violation is true
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                    return;
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnRelease_Click");
        }
    }

    protected void btnLinkCaseReports_Click(object sender, EventArgs e)  // this is linked Case Reports from ML Possible Matches
    {
        try
        {
            string lnkCaseReportId = hdflinkCaseReport.Value.ToString();

            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

            Response.Redirect("MatchingLinkingCaseReportsViewLinkDetails.aspx?Schema=MBDR_System&Table=CaseReportsView&keyName=caseRecId&name=Case Report&rowId=" + lnkCaseReportId + "&scrutinizerId=" + recordId + "&renderTable=" + hdfTableName.Value + "&where=" + where + "&sortExp=" + sortExp + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnLinkCaseReports_Click");
        }
    }

    protected void btnFullViewDetails_Click(object sender, EventArgs e)  // Button for All Matches Screen 
    {
        try
        {
            string fullViewCaseId = hdfFullViewDetails.Value.ToString();
            string tableName = string.Empty;
            if (hdfTableKeyName.Value == "Birth Records")
            {
                tableName = "ScrutinizerFullViewBirthRecords";
            }
            else if (hdfTableKeyName.Value == "Death Records")
            {
                tableName = "ScrutinizerFullViewDeathRecords";
            }
            else
            {
                tableName = "ScrutinizerFullViewCaseReports";
            }

            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

            Response.Redirect("MatchingFullViewDetails.aspx?pagecall=fullview&Schema=MBDR_System&Table=" + tableName + "&keyName=caseRecId&name=MBDR_System&scrutinizerId=" + recordId + "&caseRecId=" + fullViewCaseId + "&sortExp=" + sortExp + "&where=" + where + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_btnFullViewDetails_Click");
        }
    }

    private string getWhereClause()
    {
        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";

        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
        string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
        string scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToString(Request.QueryString["scrutinizerId"]) : "";
        string archiveId = !string.IsNullOrEmpty(Request.QueryString["archiveId"]) ? Convert.ToString(Request.QueryString["archiveId"]) : "";
        string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

        string fromDateDOB = !string.IsNullOrEmpty(Request.QueryString["fromDateDOB"]) ? Convert.ToString(Request.QueryString["fromDateDOB"]) : "";
        string toDateDOB = !string.IsNullOrEmpty(Request.QueryString["toDateDOB"]) ? Convert.ToString(Request.QueryString["toDateDOB"]) : "";

        string whereSep = "where 1=1 ";

        /*** Loaded datetime - From Date to Date ***/

        if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // both from/to date
        {
            whereSep += " and loaded_datetime > ='" + fromDate + " 00:00:00'  and loaded_datetime < ='" + toDate + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate)) // from date only 
        {
            whereSep += " and loaded_datetime > ='" + toDate + " 00:00:00' ";
        }
        else if (string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // to date only 
        {
            whereSep += " and loaded_datetime < ='" + toDate + " 23:59:59' ";
        }

        /*** DOB - From Date To Date ***/

        if (string.IsNullOrEmpty(toDateDOB) && !string.IsNullOrEmpty(fromDateDOB)) // only selection from date
        {
            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
            whereSep += " and birthDate > ='" + fromDateDOB + " 00:00:00'  and birthDate < ='" + currentDate + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(toDateDOB) && string.IsNullOrEmpty(fromDateDOB)) // only selection TO date
        {
            string defaultFromDate = "1/1/1900";
            whereSep += " and birthDate > ='" + defaultFromDate + " 00:00:00'  and birthDate < ='" + toDateDOB + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(fromDateDOB) && !string.IsNullOrEmpty(toDateDOB))
        {
            whereSep += " and birthDate > ='" + fromDateDOB + " 00:00:00'  and birthDate < ='" + toDateDOB + " 23:59:59'";
        }

        if (!string.IsNullOrEmpty(displayName) && displayName != "Select Owner")
        {
            if (displayName == "UnAssignedOwner")
            {
                whereSep += " and displayName Is null";
            }
            else
            {
                whereSep += " and displayName ='" + displayName + "'";
            }
        }

        if (!string.IsNullOrEmpty(matchLevel) && matchLevel != "Select Match Level")   // matchLevel = Dropdown condition i.e matchlevel=0,1,2,3
        {
            if (matchLevel == "0")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 0 ";
            }
            else if (matchLevel == "1")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 1 ";
            }
            else if (matchLevel == "2")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 2 ";
            }
            else
            {
                whereSep += " and matchlevel != 0 and scrutinizerKind is not null";
            }
        }

        if (!string.IsNullOrEmpty(scrutinizerId) && isSCIDBlank == "true")
        {
            whereSep += " and scrutinizerId = '" + scrutinizerId.Replace("'", "''").Trim() + "'";
        }
        if (!string.IsNullOrEmpty(archiveId) && isSCIDBlank == "true")
        {
            whereSep += " and archiveId = '" + archiveId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childLastName) && !childLastName.Contains("*"))
        {
            whereSep += " and childLastName ='" + childLastName.Replace("'", "''").Trim() + "'"; ;
        }
        else if (childLastName.Contains("*"))
        {
            whereSep += " and childLastName like '" + childLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childFirstName) && !childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName ='" + childFirstName.Replace("'", "''").Trim() + "'";
        }
        else if (childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName like '" + childFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childMiddleName) && !childMiddleName.Contains("*"))
        {
            whereSep += " and childMiddleName ='" + childMiddleName.Replace("'", "''").Trim() + "'";
        }
        else if (childMiddleName.Contains("*"))
        {
            whereSep += " and childMiddleName like '" + childMiddleName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childSSN))
        {
            whereSep += " and childSSN ='" + childSSN.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momLastName) && !momLastName.Contains("*"))
        {
            whereSep += " and momLastName ='" + momLastName.Replace("'", "''").Trim() + "'";
        }
        else if (momLastName.Contains("*"))
        {
            whereSep += " and momLastName like '" + momLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momFirstName) && !momFirstName.Contains("*"))
        {
            whereSep += " and momFirstName ='" + momFirstName.Trim().Replace("'", "''") + "'";
        }
        else if (momFirstName.Contains("*"))
        {
            whereSep += " and momFirstName like '" + momFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(address1) && !address1.Contains("*"))
        {
            whereSep += " and address1 = '" + address1.Replace("'", "''").Trim() + "'";
        }
        else if (address1.Contains("*"))
        {
            whereSep += " and address1 like '" + address1.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }


        if (!string.IsNullOrEmpty(city) && !city.Contains("*"))
        {
            whereSep += " and city = '" + city.Replace("'", "''").Trim() + "'";
        }
        else if (city.Contains("*"))
        {
            whereSep += " and city like '" + city.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        return whereSep;
    }

    private string getQueryString()
    {
        string archiveId = !string.IsNullOrEmpty(Request.QueryString["archiveId"]) ? Convert.ToString(Request.QueryString["archiveId"]) : "";
        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
        string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
        string returnpage = !string.IsNullOrEmpty(Request.QueryString["returnpage"]) ? Convert.ToString(Request.QueryString["returnpage"]) : "";
        string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

        string fromDateDOB = !string.IsNullOrEmpty(Request.QueryString["fromDateDOB"]) ? Convert.ToString(Request.QueryString["fromDateDOB"]) : "";
        string toDateDOB = !string.IsNullOrEmpty(Request.QueryString["toDateDOB"]) ? Convert.ToString(Request.QueryString["toDateDOB"]) : "";

        string whereSep = "1";
        whereSep += "&isSCIDBlank=" + isSCIDBlank + "";

        if (!string.IsNullOrEmpty(displayName))
        {
            whereSep += "&displayName=" + displayName + "";
        }
        if (!string.IsNullOrEmpty(returnpage))
        {
            whereSep += "&returnpage=" + returnpage + "";
        }
        if (!string.IsNullOrEmpty(matchLevel))
        {
            whereSep += "&matchLevel=" + matchLevel + "";
        }
        if (!string.IsNullOrEmpty(archiveId))
        {
            whereSep += "&archiveId=" + archiveId.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(childLastName))
        {
            whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childFirstName))
        {
            whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childMiddleName))
        {
            whereSep += "&childMiddleName=" + childMiddleName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(birthDate))
        {
            whereSep += "&birthDate=" + birthDate + "";
        }
        if (!string.IsNullOrEmpty(childSSN))
        {
            whereSep += "&childSSN=" + childSSN.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(momLastName))
        {
            whereSep += "&momLastName=" + momLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(momFirstName))
        {
            whereSep += "&momFirstName=" + momFirstName.Trim().Replace("'", "''") + "";
        }

        if (!string.IsNullOrEmpty(address1))
        {
            whereSep += "&address1=" + address1.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(city))
        {
            whereSep += "&city=" + city.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(fromDate))
        {
            whereSep += "&fromDate=" + fromDate + "";
        }
        if (!string.IsNullOrEmpty(toDate))
        {
            whereSep += "&toDate=" + toDate + "";
        }

        if (!string.IsNullOrEmpty(fromDateDOB))
        {
            whereSep += "&fromDateDOB=" + fromDateDOB + "";
        }
        if (!string.IsNullOrEmpty(toDateDOB))
        {
            whereSep += "&toDateDOB=" + toDateDOB + "";
        }
        return whereSep;
    }

    protected void btnSimilar_Click(object sender, EventArgs e)
    {
        hdfIsTabClickedSM.Value = "true";

        if (hdfIsTabClickedBR.Value == "true")
        {
            string btnDR = btnDeathRecords.CssClass;
            if (hdfIsTabClickedDR.Value == "true")
            {
                hdfIsTabClicked.Value = "true";
            }
            else if (!btnDR.Contains("active"))
            { hdfIsTabClicked.Value = "true"; }
        }
        else if (hdfIsTabClickedDR.Value == "true")
        {

            string btnBR = btnBirthRecords.CssClass;
            if (hdfIsTabClickedBR.Value == "true")
            {
                hdfIsTabClicked.Value = "true";
            }
            else if (!btnBR.Contains("active"))
            { hdfIsTabClicked.Value = "true"; }

        }
        else
        {
            string btnSM = btnCaseReports.CssClass;
            if (hdfIsTabClickedSM.Value == "true")
            {
                hdfIsTabClicked.Value = "true";
            }
            else if (!btnSM.Contains("active"))
            { hdfIsTabClicked.Value = "true"; }
        }

        SimilarRecord();
    }

    private void SimilarRecord()
    {
        try
        {
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("activeGreen", "");

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");
            btnSimilar.CssClass = btnSimilar.CssClass.Replace("Green", "");

            string className = btnSimilar.CssClass;
            className = className.Replace("active", "");
            className = className.Replace("btn-disabled", "");


            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
            btnSimilar.CssClass = className + "active";

            hdfTableName.Value = "SimilarCandidatesView";
            hdfSchema.Value = "MBDR_System";
            hdfTableKeyName.Value = "Similar Records";
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.SimilarCandidatesView SC ");

            isOwnerID = IsOwnerShip();

            string env = ConfigurationManager.AppSettings["environment"];


            if (!isOwnerID.Equals(Session["userid"]))
            {
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                btnRelease.Enabled = false;
                btnLink.Enabled = false;
                btnAssign.Enabled = false;
                btnPreviousMatch.Enabled = false;
                btnNextMatch.Enabled = false;


                btnRelease.CssClass = "enableGrey";
                btnLink.CssClass = "enableGrey";
                btnAssign.CssClass = "enableGrey";
                btnPreviousMatch.CssClass = "enableGrey";
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "false";

                if (permissions.Count > 0)
                {
                    displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                    string miMiLogin = string.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if ((env == "prod") && (displayName == "UnAssignedOwner" || string.IsNullOrEmpty(displayName)))
                        {
                            if (string.IsNullOrEmpty(isOwnerID))
                            {
                                btnOwn.Enabled = true;
                                btnOwn.CssClass = "disableGrey";
                            }
                            else
                            {
                                btnOwn.Enabled = false;
                                btnOwn.CssClass = "enableGrey";
                            }
                        }
                        else if (env == "prod")
                        {
                            btnOwn.Enabled = false;
                            btnOwn.CssClass = "enableGrey";
                        }
                        else
                        {
                            btnOwn.Enabled = true;
                            btnOwn.CssClass = "disableGrey";
                        }
                    }
                }
                else
                {
                    btnOwn.Enabled = false;
                    btnOwn.CssClass = "enableGrey";
                }
            }
            else
            {
                btnOwn.Enabled = false;
                btnRelease.Enabled = true;
                btnOwn.OnClientClick = null;
                btnLink.Enabled = true;
                btnAssign.Enabled = true;
                btnPreviousMatch.Enabled = false;  // will need to change true both prev and next 
                btnNextMatch.Enabled = false;

                btnOwn.CssClass = "enableGrey";
                btnRelease.CssClass = "disableGrey";
                btnLink.CssClass = "disableGrey";
                btnAssign.CssClass = "disableGrey";
                btnPreviousMatch.CssClass = "enableGrey";   // will need to change true both prev and next 
                btnNextMatch.CssClass = "enableGrey";
                hdfOwner.Value = "true";
            }

            OwnerShip();
            dt = new DataTable();


            dt = CommonMethod(query);

            // checkbox maintain.
            if (dt != null)
            {
                DataRow dr1 = dt.NewRow();
                dr1[0] = "IsMatchRecord";
                for (int i = 2; i < dt.Columns.Count; i++)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (string.IsNullOrEmpty(hdfDetailsID.Value))
                        {
                            dr1[i] = "Check for Linking Process";
                        }
                    }
                }
                dt.Rows.Add(dr1);
                if (dt.Columns.Count > 2)
                {
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {
                        if (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i]))
                        {
                            hdfCaseReportCaseRecordId.Value = Convert.ToString(dt.Rows[5][i]);
                        }
                    }
                }
            }
            //scrutinizerdatarecords pull from ScrutinizerClearView Table
            DataTable dtSc = new DataTable();
            Session["scrutinizerdatarecords"] = null;

            dtSc = CheckscrutinizerIdValue();
            Session["scrutinizerdatarecords"] = dtSc;

            foreach (DataRow dr in dtSc.Rows)
            {
                if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnBirthRecords.CssClass;

                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = classNameblank + " activeGreen";
                                btnCaseReports.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = classNameblank + " activeGreen";
                                btnCaseReports.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnBirthRecords.CssClass;

                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtSc.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string classNameblank = btnCaseReports.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = classNameblank + " activeGreen";
                                btnCaseReports.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnCaseReports.CssClass = classNameblank + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string classNameblank = btnBirthRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = classNameblank + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnBirthRecords.CssClass = classNameblank + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string classNameblank = btnDeathRecords.CssClass;
                                if (classNameblank.Contains("btn-disabled"))
                                {
                                    classNameblank = classNameblank.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = classNameblank + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                //break;
                            }
                        }
                        else
                        {
                            string classNameblank = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !classNameblank.Contains("active"))
                            {
                                btnDeathRecords.CssClass = classNameblank + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
            }

            if (Session["dtMaintain"] != null)
            {
                Session["dtMaintain"] = null;
            }
            Session["dtMaintain"] = dt;
            Session["datarecords"] = null;
            Session["datarecords"] = dt;

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_SimilarRecord");
        }
    }

    public static void ErrorLog(string values)
    {
        bool shot = true;
        if (shot)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.Snap(values, path, "MatchingViewDetails.aspx");
        }
    }
}
