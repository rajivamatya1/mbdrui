﻿<%@ Page Title="User Roles and Permissions" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="UserManagementRoles.aspx.cs" Inherits="UserManagementRoles" EnableEventValidation="false" %>


<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">User Roles</h1>
    <!DOCTYPE html>


    <div class="col col-lg-12 col-md-12 col-sm-12">
        <div class="program">
            <div class="program-header">
                <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
                <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
                <asp:HiddenField runat="server" ID="hdfTableName" Value="" />
                <div class="program-name">
                    Add New Role
                </div>
            </div>
            <div class="program-description">
                <div class="row">
                    <div class="col col-lg-4 col-sm-12">

                        <div class="form-item-userManagement">
                            <label for="copy-permissions">Add New Role:</label>
                            <asp:TextBox ID="txtRole" runat="server" ValidationGroup="search" placeholder="Type New Role"></asp:TextBox>
                            <asp:TextBox ID="txtRoleDetails" runat="server" ValidationGroup="search" placeholder="Type Role Details"></asp:TextBox>
                            <div class="form-item">
                                <asp:Button ID="btnAddrole" runat="server" Text="Create" OnClick="btnAddrole_Click" />
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <span class="program-title-usermgmt">Existing User Roles </span>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button ID="btnUserManage" runat="server" Text="User Management" OnClick="btnUserManage_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                    <asp:Button ID="btnPermissions" runat="server" Text="Permissions" OnClick="btnPermissions_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-view-matching">
            <div class="col col-lg-12 col-sm-12">
                <div class="tableFixHead">
                    <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                        AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true" OnRowCommand="gvDynamic_RowCommand"
                        EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                        OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowEditing="gvDynamic_RowEditing" OnRowCreated="gvDynamic_RowCreated"
                        OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                        PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>




    <% } %>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
       <br />
    </div>

    <script>
        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ShowProgress(para) {

            var modal = $('<div />');
            modal.addClass("modalspin");
            $('body').append(modal);
            var loading = $(".loadingspin");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ "position": "center", top: top, left: left });

            return true;
        }
    </script>
</asp:Content>
