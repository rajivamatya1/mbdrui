﻿<%@ Page Title="Facility Report Card" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FacilityDashboard.aspx.cs" Inherits="DynamicGridView.FacilityDashboard" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
            padding: 20px;
        }

        .container-fac-das {
            background-color: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }

        .section-fac-das {
            background-color: #e3f4ff;  
            padding: 15px;
            border-radius: 6px;
            margin-bottom: 20px;
        }

        .section-fac-das-title {
            font-size: 18px;
            font-weight: bold;
            margin-bottom: 10px;
            text-align: center;
        }

        .stats-container-fac-das {
            display: flex;
            gap: 15px;
            flex-wrap: wrap;
        }

        .stat-box-fac-das {
            flex: 1;
            min-width: 200px;
            background-color: white;
            padding: 15px;
            border-radius: 6px;
            text-align: center;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 1.1);
        }

        .stat-title-fac-das {
            font-weight: bold;
            margin-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>

    <div>
        <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
        <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
        <asp:HiddenField runat="server" ID="hdfName" Value="" />
        <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
        <asp:HiddenField runat="server" ID="isResetClick" Value="" />
    </div>

    <div>
        <h1 class="page-title">
            <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
            FACILITY DASHBOARD
            <asp:Button ID="btnFRC" runat="server" Text="Facility Report Card" class="rightAlignment marginRight30" OnClick="btnFRC_Click" />
            <asp:Button ID="btnAQR" runat="server" Text="Annual Quality Report" class="rightAlignment" OnClick="btnAQR_Click" Visible="false" />        
        </h1>
    </div>

    <div class="row margin10px">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Submitter/Organization</label>
                <asp:DropDownList runat="server" ID="ddlOrg" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged" AutoPostBack="true" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ValidatorOrg" runat="server" ControlToValidate="ddlOrg" InitialValue="Select Submitter/Organization" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Organization is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>
            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Facility</label>
                <asp:DropDownList runat="server" ID="ddlFacility" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlFacility_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ValidatorFacility" runat="server" ControlToValidate="ddlFacility" InitialValue="Select Facility" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Facility is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

        </div>

        <div class="col col-lg-12 col-sm-12 pt-1">
            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Year</label>
                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ValidatorYear" runat="server" ControlToValidate="ddlYear" InitialValue="Select Year" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Year is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Month</label>
                <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ValidatorMonth" runat="server" ControlToValidate="ddlMonth" InitialValue="Select Month" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Month is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item">
                <label class="fontDDSize"></label>
                <asp:Button Text="Submit" runat="server" ID="btnSubmit" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSubmit_Click" CssClass="btn btn-danger" />
            </div>

            <div class="form-item">
                <label class="fontDDSize"></label>
                <asp:Button Text="Reset " runat="server" ID="btnReset" CausesValidation="false" OnClick="btnReset_Click" CssClass="btn btn-danger" />
            </div>
        </div>
    </div>
    <br /><br />

    <div class="marginLeft30" id="searchreport" runat="server" visible="false">
         <% if (dtFRCDashBoard != null && dtFRCDashBoard.Rows.Count > 0)
             { %>
  
        <h4>
            The “Birth Defects Statistics” are representative of the month/year and the organization/facility selected by the user.
        <br /> <br />
            A Facility Report Card should be used to generate a month-by-month comparison of reported birth defects data for a specific organization/facility. 
            <br /> <br />
        </h4>
        <div class="section-fac-das">
            <div class="section-fac-das-title">BIRTH DEFECTS DATA STATISTICS</div>
            <div class="stats-container-fac-das">
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Reporting Frequency</div>
                    <div><%=dtFRCDashBoard.Rows[0]["frequency"] %></div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Case Report Count</div>
                    <div><%=dtFRCDashBoard.Rows[0]["reportsCount"] %></div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Duplicate Case Reports</div>
                    <div><%=dtFRCDashBoard.Rows[0]["duplicateReports"] %></div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">File Load Errors</div>
                    <div><%=dtFRCDashBoard.Rows[0]["fileErrors"] %></div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Submitted Case Report Errors</div>
                    <div><%=dtFRCDashBoard.Rows[0]["reportErrors"] %></div>
                </div>
            </div>
        </div>
        <br />
        <h4>The “MBDR Case Volume Totals” are representative of the current aggregate totals, by specific application function noted and the organization/facility selected by the user.</h4>

        <div class="section-fac-das">
            <div class="section-fac-das-title">MBDR CASE VOLUME TOTALS</div>
            <div class="stats-container-fac-das">
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Matching and Linking</div>
                    <div><%=dtFRCDashBoard.Rows[0]["MLCandidates"] %> Primary Candidate Reports</div>
                    <div><%=dtFRCDashBoard.Rows[0]["MLSimilars"] %> Similars</div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Case Verification</div>
                    <div><%=dtFRCDashBoard.Rows[0]["CVComplete"] %> Complete</div>
                    <div><%=dtFRCDashBoard.Rows[0]["CVIncomplete"] %> Incomplete</div>
                    <div><%=dtFRCDashBoard.Rows[0]["CVCHDCases"] %> CHD/CCHD Cases</div>
                </div>
                <div class="stat-box-fac-das">
                    <div class="stat-title-fac-das">Fix It</div>
                    <div><%=dtFRCDashBoard.Rows[0]["FITCases"] %> Unresolved Case Records</div>
                </div>
            </div>
        </div>
        <%
            }%>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div id="commonMsg">
        <asp:HiddenField ID="hdCheckIds" runat="server" />
    </div>

    <script type="text/javascript">

        function hideReport() {
            var $searchReportElement = $("#<%=searchreport.ClientID%>");

           if ($searchReportElement.length) {
               $searchReportElement.hide();
           }
        }

        function clearBGD() {
            
            var tableContents = document.getElementById("searchreport");

            if (tableContents) {
                tableContents.style.display = "none";
            }
            sessionStorage.clear();
        }

       $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSubmit.ClientID%>").click();
                    }
                });

        /*** will trigger search button by enter ***/

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

        function ShowProgress() {
            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }


    </script>
    <% } %>
</asp:Content>



