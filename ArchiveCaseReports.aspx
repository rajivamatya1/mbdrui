﻿<%@ Page Title="MBDR Default Data View" Language="C#" MasterPageFile="~/index.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="ArchiveCaseReports.aspx.cs" Inherits="DynamicGridView.ArchiveCaseReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">


    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="isResetClick" Value="" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>
    <div class="row">
        <asp:Label Text="" runat="server" Visible="false" ID="lblMessage" />
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="row">
                <div class="form-item">
                    <label for="Content_txtchdLN">Child Last Name </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtchdLN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtchdLN" ID="RegularExpressionValidator2" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtchdFN">Child First Name </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtchdFN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtchdFN" ID="RegularExpressionValidator3" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtchdALN">Child Alias Last Name </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtchdALN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtchdALN" ID="RegularExpressionValidator11" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtchdAFN">Child Alias First Name </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtchdAFN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtchdAFN" ID="RegularExpressionValidator12" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>


                <div class="form-item">

                    <label for="Content_txtChildDOB">DOB </label>

                    <asp:TextBox
                        EnableViewState="true"
                        onblur="ValidateDate(this);"
                        runat="server"
                        CssClass="cursor-pointerWhiteNewField customDateField"
                        TextMode="Date"
                        ID="txtChildDOB"
                        ValidationGroup="search" />

                    <asp:CustomValidator
                        runat="server"
                        ID="CustomValidator3"
                        ClientValidationFunction="ValidateDate"
                        ValidationGroup="search"
                        ControlToValidate="txtChildDOB"
                        EnableClientScript="true"
                        ErrorMessage="Invalid DOB."
                        Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true"
                        onkeypress="return false;"
                        onpaste="return false;" />
                </div>


                <div class="form-item">
                    <label for="Content_txtChildSSN">Child SSN </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtChildSSN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtChildSSN" ValidationGroup="search" ValidationExpression="^[0-9]{9,9}$" runat="server" ErrorMessage="Enter 9-digit SSN." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtMomSSN">Mother SSN </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtMomSSN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txtMomSSN" ValidationGroup="search" ValidationExpression="^[0-9]{9,9}$" runat="server" ErrorMessage="Enter 9-digit SSN." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtMomLN">Mother Last Name </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtMomLN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtMomLN" ID="RegularExpressionValidator9" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item">
                    <label for="Content_txtMomFN">Mother First Name</label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtMomFN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtMomFN" ID="RegularExpressionValidator10" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-item">
                    <label for="Content_txtadd1">Address 1 </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtadd1" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ControlToValidate="txtadd1" ID="RegularExpressionValidator4" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>
                <div class="form-item" runat="server" id="dvFromDate">
                    <label for="Content_txtDateFrom">From Date</label>
                    <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateFrom" ValidationGroup="search" onblur="ValidateFromDate(this);" />
                    <asp:CustomValidator  EnableClientScript="true"  onkeypress="return false;" onpaste="return false;" runat="server" ValidationGroup="search" ID="CustomValidator1" ClientValidationFunction="ValidateFromDate" ControlToValidate="txtDateFrom" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                </div>
                <div class="form-item" runat="server" id="dvToDate">
                    <label for="Content_txtDateTo">To Date</label>
                    <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateTo" ValidationGroup="search" onblur="ValidateToDate(this);" />
                    <asp:CustomValidator  EnableClientScript="true" onkeypress="return false;" onpaste="return false;" 
                        runat="server" ValidationGroup="search" ID="CustomValidator2" ClientValidationFunction="ValidateToDate"
                        ControlToValidate="txtDateTo" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                </div>
                <div class="form-item">
                    <asp:Button Text="Search" runat="server" ID="btnSearch" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" ValidationGroup="search" CssClass="btn btn-success" />
                </div>
                <div class="form-item">
                    <asp:Button Text="Reset" runat="server" ID="btnReset" OnClientClick="resetValidation()" CssClass="btn btn-danger" />
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label for="Content_ddlArchiveTables">Filter by Facility</label>
                <asp:DropDownList runat="server" ID="ddlArchiveTables" AutoPostBack="true" onchange="hideReport()" OnSelectedIndexChanged="ddlArchiveTables_SelectedIndexChanged" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>


    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
                    }
                });

        /*** will trigger search button by enter ***/

        function resetValidation() {

            document.getElementById("<%=txtchdFN.ClientID %>").value = "";
            document.getElementById("<%=txtchdLN.ClientID %>").value = "";
            document.getElementById("<%=txtchdALN.ClientID %>").value = "";
            document.getElementById("<%=txtchdAFN.ClientID %>").value = "";
            document.getElementById("<%=txtChildDOB.ClientID %>").value = "";
            document.getElementById("<%=txtChildSSN.ClientID %>").value = "";
            document.getElementById("<%=txtMomSSN.ClientID %>").value = "";
            document.getElementById("<%=txtMomLN.ClientID %>").value = "";
            document.getElementById("<%=txtMomFN.ClientID %>").value = "";
            document.getElementById("<%=txtadd1.ClientID %>").value = "";

            document.getElementById("<%=txtDateFrom.ClientID %>").value = "";
            document.getElementById("<%=txtDateTo.ClientID %>").value = "";

            document.getElementById("<%=ddlArchiveTables.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

            var gridView = document.getElementById("<%= gvDynamic.ClientID %>");
            var pageIndex = 0;
            gridView.PageIndex = pageIndex;
        }

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

        function hideReport()
        {
            var $searchReportElement = $("#<%=gvDynamic.ClientID%>");

            if ($searchReportElement.length)
            {
                $searchReportElement.hide();
            }
        }

        function ShowProgress()
        {
            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {
                
                $("#btnAdvanceSearch").prop("disabled", true);
            }
            else
            {
                $("#btnAdvanceSearch").prop("disabled", false);


                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });
                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ValidateToDate(input) {

            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID %>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");
            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
            var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

            if (txtTo.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtTo === "" || txtTo === 'yyyy-mm-dd') {

                    fromDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                }
                else if (!dateRegex.test(txtTo)) {

                    toDateValidator.style.display = "block";
                    toDateValidator.innerHTML = "Invalid Date";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                }
                else {

                    var minDate = new Date("1900-01-01");
                    var maxDate = new Date();

                    // parse the date

                    var checkDateIfisValid = new Date(txtTo);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/1900

                    if (checkDateIfisValid < minDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    if (txtTo !== null)
                    {
                        if (txtFrom.length === 0)
                        {
                            toDateValidator.style.display = "none";
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else if (txtTo < txtFrom)
                        {
                            if (!dateRegex.test(txtFrom))
                            {
                                fromDateValidator.style.display = "block";
                                fromDateValidator.innerHTML = "Invalid Date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                            }
                            else
                            {
                                fromDateValidator.style.display = "none";

                                toDateValidator.style.display = "block";
                                toDateValidator.innerHTML = "Must be before from date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                                return false;
                            }
                        }
                        else {
                            if (enteredChildDOB !== "")
                            {
                                if (!dateRegex.test(enteredChildDOB))
                                {
                                    validatorChildDOB.style.display = "block";
                                    fromDateValidator.style.display = "none";       // clear the from/to validator message
                                    toDateValidator.style.display = "none";
                                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                    return false;
                                }
                            }
                            else
                            {
                                if (!dateRegex.test(txtFrom))
                                {
                                    fromDateValidator.style.display = "block";
                                    fromDateValidator.innerHTML = "Invalid Date";

                                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                }
                                else
                                {
                                    toDateValidator.style.display = "none";
                                    fromDateValidator.style.display = "none";

                                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                                    return true;
                                }
                            }
                        }
                    }
                    else {

                        if (enteredChildDOB !== "")
                        {
                            if (!dateRegex.test(enteredChildDOB))
                            {
                                validatorChildDOB.style.display = "block";
                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                return false;
                            }
                        }
                        else
                        {
                            toDateValidator.style.display = "none";
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                    }
                }
            } else {
                document.getElementById("<%=txtDateTo.ClientID%>").value = "";
            }
        }

        function ValidateFromDate(input) {

            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID%>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");
            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
            var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

            if (txtFrom.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtFrom === "" || txtFrom === 'yyyy-mm-dd') {

                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                }
                else if (!dateRegex.test(txtFrom)) {

                    fromDateValidator.style.display = "block";
                    fromDateValidator.innerHTML = "Invalid Date";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                }
                else {


                    var minDate = new Date("1900-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtFrom);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/1900

                    if (checkDateIfisValid < minDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    var checkDateIfisValidToDate = new Date(txtTo);

                    if (checkDateIfisValidToDate > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         return false;
                     }

                    if (txtTo.length > 0) {

                        if (txtFrom > txtTo) {

                            toDateValidator.style.display = "none";

                            fromDateValidator.style.display = "block";
                            fromDateValidator.innerHTML = "Must be before to date";


                            $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                            return false;

                        }
                        else if (!dateRegex.test(txtTo)) {

                            toDateValidator.style.display = "block";
                            toDateValidator.innerHTML = "Invalid Date";
                            $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                        }
                        else
                        {
                            if (enteredChildDOB !== "")
                            {
                                if (!dateRegex.test(enteredChildDOB)) {
                                    validatorChildDOB.style.display = "block";
                                    fromDateValidator.style.display = "none";       // clear the from/to validator message
                                    toDateValidator.style.display = "none";
                                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                    return false;
                                }
                                else
                                {   
                                    var emptyDOB = document.getElementById("<%=txtChildDOB.ClientID %>").value;

                                    if (emptyDOB && emptyDOB.length === 10) {
                                        ValidateDate();
                                    }
                                }
                            }
                            else
                            {
                                fromDateValidator.style.display = "none";
                                toDateValidator.style.display = "none";
                                $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                                return true;
                            }
                        }
                    }
                    else if (txtFrom <= txtTo) {

                        toDateValidator.style.display = "none";

                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Must be before to date.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                        return false;

                    }
                    else {

                        if (enteredChildDOB !== "")
                        {
                            if (!dateRegex.test(enteredChildDOB))
                            {
                                validatorChildDOB.style.display = "block";
                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                return false;
                            }
                            else
                            {
                                var emptyDOB = document.getElementById("<%=txtChildDOB.ClientID %>").value;

                                if (emptyDOB && emptyDOB.length === 10) {
                                    ValidateDate();
                                }
                            }
                        }
                        else
                        {
                            fromDateValidator.style.display = "none";
                            toDateValidator.style.display = "none";
                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                    }
                }
            }
            else
            {
                document.getElementById("<%=txtDateFrom.ClientID%>").value = "";

                if (txtTo) {

                    fromDateValidator.style.display = "none";
                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                    return true;
                }
            }
        }

        function ValidateDate(input) {

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;

            var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

            if (enteredChildDOB.length === 10) {
                if (enteredChildDOB === "undefined" || enteredChildDOB === null || enteredChildDOB === "") {
                    enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
                }

                var currentYear = new Date().getFullYear();

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (enteredChildDOB === "" || enteredChildDOB === 'yyyy-mm-dd') {
                    validatorChildDOB.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);
                }
                else if (!dateRegex.test(enteredChildDOB)) {
                    validatorChildDOB.style.display = "block";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                    return false;
                }
                else {

                    var minDate = new Date("1900-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(enteredChildDOB);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/1900

                    if (checkDateIfisValid < minDate) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Date must be 01/01/1900 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    validatorChildDOB.style.display = "none";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                     var emptytxtFrom = document.getElementById("<%=txtDateFrom.ClientID %>").value;

                     if (emptytxtFrom && emptytxtFrom.length === 10) {
                         ValidateFromDate();
                     }

                     var emptytxtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

                    if (emptytxtTo && emptytxtTo.length === 10) {
                        ValidateToDate();
                     }
                }
            }
            else {
                document.getElementById("<%=txtChildDOB.ClientID%>").value = "";
            }
        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

    </script>
</asp:Content>
