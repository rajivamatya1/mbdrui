﻿using DynamicGridView.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class VerificationDetails : System.Web.UI.Page
    {
        dynamic permissions = null;
        protected DataTable dt;
        protected string assignedUserName;

        protected bool isChecked = false;
        protected bool isSelected = false;
        private object previousRowData = null;
        private GridViewRow previousRow = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }

                permissions = CheckRole();

                string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";

                if (!IsPostBack)
                {
                    ltrTableName.Text = "VERIFICATION DETAILS FOR CASE REPORT  " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + reportId + "</span></b>";

                    BindCVPersonDetails("MBDR_System", "getCaseVerificationPersonDetails", reportId);

                    BindCVCHDDetails("MBDR_System", "getCaseVerificationCHDDetails", reportId);

                    BindCVDiagnosisDetails("MBDR_System", "getCaseVerificationDiagnosisDetails", reportId);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_Page_Load");
            }
        }

        /******************************************************************************************************/
        /*                     CASE REPORT with Patient Info -- First section in the screen                    */
        /******************************************************************************************************/

        protected void BindCVPersonDetails(string schema, string tableName, string reportId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    //string query = "SELECT * FROM {0}.{1}(@reportId)";

                    //query = string.Format(query, schema, tableName);

                    string query = string.Format("SELECT * FROM {0}.{1}(@reportId)", schema, tableName);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    cmd.Parameters.AddWithValue("@reportId", reportId);                   

                    dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                   
                    conn.Open();
                    da.Fill(dt);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_BindCVPersonDetails");
            }
        }

        /******************************************************************************************************/
        /*                      CHD VerificationDetails -- Second section in the screen                    */
        /******************************************************************************************************/

        protected void BindCVCHDDetails(string schema, string tableName, string reportId)
        {
            try
            {
                DataTable dtCCHD = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "childLastName")
                            {
                                sortExp = "childLastName asc, childMRN asc, admissionDate asc";
                                break;
                            }
                        }
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";

                    string query = "SELECT vs.reportId, vs.chdDiagnoses, vs.cchdDiagnoses, CONVERT(DATE,vs.occurrence,101) AS occurrence, vs.statusId, vs.verificationDate, vs.[user] AS submitterName, vs.savedInOtherReport FROM {0}.{1}(@reportId) vs ORDER BY vs.verificationDate DESC";

                    query = string.Format(query, schema, tableName);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    cmd.Parameters.AddWithValue("@reportId", reportId);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    
                    conn.Open();
                    da.Fill(dtCCHD);
                    conn.Close();

                    DataTable dtCCHDEmpty = dtCCHD.Clone();

                    if (dtCCHD.Rows.Count == 0)
                    {
                        dtCCHDEmpty.Rows.Add("0");
                    }

                    for (int i = 0; i < dtCCHD.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            DataRow dr = dtCCHDEmpty.NewRow();
                            dr["reportId"] = dtCCHD.Rows[i][0];
                            dr["chdDiagnoses"] = dtCCHD.Rows[i][1];
                            dr["cchdDiagnoses"] = dtCCHD.Rows[i][2];
                            dr["occurrence"] = DBNull.Value;
                            dr["statusId"] = DBNull.Value;
                            dr["verificationDate"] = DBNull.Value;
                            dr["submitterName"] = DBNull.Value;
                            dr["savedInOtherReport"] = DBNull.Value;
                            dtCCHDEmpty.Rows.Add(dr);

                            if (!string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][4])))
                            {
                                dr = dtCCHDEmpty.NewRow();
                                dr["reportId"] = dtCCHD.Rows[i][0];
                                dr["chdDiagnoses"] = dtCCHD.Rows[i][1];
                                dr["cchdDiagnoses"] = dtCCHD.Rows[i][2];
                                dr["occurrence"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][3])) ? dtCCHD.Rows[i][3] : DBNull.Value;
                                dr["statusId"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][4])) ? dtCCHD.Rows[i][4] : DBNull.Value;
                                dr["verificationDate"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][5])) ? dtCCHD.Rows[i][5] : DBNull.Value;
                                dr["submitterName"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][6])) ? dtCCHD.Rows[i][6] : DBNull.Value;
                                dr["savedInOtherReport"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][7])) ? dtCCHD.Rows[i][7] : DBNull.Value;
                                dtCCHDEmpty.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][4])))
                            {
                                DataRow dr = dtCCHDEmpty.NewRow();

                                dr["reportId"] = dtCCHD.Rows[i][0];
                                dr["chdDiagnoses"] = dtCCHD.Rows[i][1];
                                dr["cchdDiagnoses"] = dtCCHD.Rows[i][2];
                                dr["occurrence"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][3])) ? dtCCHD.Rows[i][3] : DBNull.Value;
                                dr["statusId"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][4])) ? dtCCHD.Rows[i][4] : DBNull.Value;
                                dr["verificationDate"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][5])) ? dtCCHD.Rows[i][5] : DBNull.Value;
                                dr["submitterName"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][6])) ? dtCCHD.Rows[i][6] : DBNull.Value;
                                dr["savedInOtherReport"] = !string.IsNullOrEmpty(Convert.ToString(dtCCHD.Rows[i][7])) ? dtCCHD.Rows[i][7] : DBNull.Value;
                                dtCCHDEmpty.Rows.Add(dr);
                            }
                        }
                    }
                    gvCCHDCaseVerification.DataSource = dtCCHDEmpty;
                    gvCCHDCaseVerification.DataBind();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_BindCVCHDDetails");
            }
        }
                
        /******************************************************************************************************/
        /*                  ICD-10 Diagnosis Code (Qcodes) -- Third section in the screen                     */
        /******************************************************************************************************/

        public void BindCVDiagnosisDetails(string schema, string tableNameVeri, string reportId)
        {
            try
            {
                DataTable vdt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableNameVeri);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "childLastName")
                            {
                                sortExp = "childLastName asc, childMRN asc, admissionDate asc";
                                break;
                            }
                        }
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string qOrder = sortExp;
                    DataTable dataTable = new DataTable();
                    qOrder = "diagnosisId ASC, verificationDate DESC";

                    int pageIndex = 0;

                    //where = GetWhereClause();
                    where = "(@reportId)";

                    string querySingle = string.Format("WITH CTE AS (SELECT diagnosis,diagnosisId,verifyId,0 AS statusId,comment,methodIds,condition,null AS verificationDate,null AS [user],ROW_NUMBER() OVER(Partition BY diagnosis ORDER BY diagnosisId) AS rn FROM {0}.{1} {5}) SELECT diagnosis, diagnosisId, verifyId,statusId,comment,methodIds, condition, null AS verificationDate, null AS submitterName FROM CTE WHERE rn=1 ", schema, tableNameVeri, "reportId", ((gvVerificationStatus.PageIndex * gvVerificationStatus.PageSize) + 1), (gvVerificationStatus.PageIndex + 1) * gvVerificationStatus.PageSize, where, qOrder);

                    /****** Only to get the empty ICD-10 diagnois code (Could be multiple)*******/

                    SqlCommand cmdSingle = new SqlCommand(querySingle, conn);
                    cmdSingle.CommandTimeout = 0;

                    cmdSingle.Parameters.AddWithValue("@reportId", reportId);
                    conn.Open();

                    SqlDataAdapter daSingle = new SqlDataAdapter(cmdSingle);

                    conn.Close();
                    daSingle.Fill(dataTable);

                    query = string.Format("WITH CTE AS (SELECT diagnosis,diagnosisId,verifyId,statusId,comment,methodIds,condition,verificationDate,[user],ROW_NUMBER() OVER(Partition BY diagnosis ORDER BY diagnosisId ASC) AS rn FROM {0}.{1} {5}) SELECT diagnosis, diagnosisId, verifyId,statusId,comment,methodIds, condition, verificationDate, [user] AS submitterName FROM CTE ORDER BY {6} ", schema, tableNameVeri, "reportId", ((gvVerificationStatus.PageIndex * gvVerificationStatus.PageSize) + 1), (gvVerificationStatus.PageIndex + 1) * gvVerificationStatus.PageSize, where, qOrder);

                    /****** History recorded ICD - 10 diagnois code(Could be multiple)*******/

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@reportId", reportId);
                    conn.Close();
                    da.Fill(vdt);

                    //  Both table merge on the basis of ICD-10 diagnois code - null value and 

                    DataTable dtClone = dataTable.Clone();
                    dtClone.Columns["verificationDate"].DataType = typeof(string);  // clone verificationDate and submitName to change the data type. 
                    dtClone.Columns["submitterName"].DataType = typeof(string);

                    foreach (DataRow row in dataTable.Rows)
                    {
                        dtClone.ImportRow(row);
                    }

                    if (vdt.Rows.Count > 0)
                    {
                        foreach (DataRow row in vdt.Rows)
                        {
                            if (row["verifyId"] != DBNull.Value)
                            {
                                dtClone.ImportRow(row);  // input rows into clone table
                            }
                        }
                    }

                    dtClone.DefaultView.Sort = "diagnosisId ASC ";   
                    DataTable sortedTable = dtClone.DefaultView.ToTable();

                    gvVerificationStatus.DataSource = sortedTable;
                    gvVerificationStatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_BindQCodesVerification");
            }
        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "CASE_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_CheckRole");
            }
            return permissions;
        }

        private string GetWhereClause()
        {
            string where = " where 1 = 1";

            try
            {
                string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";

                if (!string.IsNullOrEmpty(reportId))
                {
                    where += " AND reportId ='" + reportId + "'";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_GetWhereClause");
            }

            return where;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where 1=1 ";

                    where = GetWhereClause();

                    query = string.Format("select count({0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                   
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_GetTotalRecords");
            }
            return totalRecords;
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_GetSortDirection");
            }
            return nextDir;
        }

        HashSet<string> encounteredDiagnosis = new HashSet<string>();

        protected void gvVerificationStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                /******************** Verification Status DropDown ***************************/

                DataTable dtStatus = new DataTable();
                DataTable dtStatusList = new DataTable();
                DataTable dtMethodList = new DataTable();
                List<DropDownModal> lstStatusColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstStatusTables = new List<DropDownModal>();
                string statusId = !string.IsNullOrEmpty(Request.QueryString["statusId"]) ? Convert.ToString(Request.QueryString["statusId"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string queryList = "";
                    queryList = string.Format("select distinct vs.details, vs.statusId from [MBDR_System].[VerificationStatuses] vs order by details asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtStatusList);
                }

                /******************** Verification Methods choices ***************************/

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string queryList = "";
                    queryList = string.Format("Select * from [MBDR_System].[VerificationMethods] order by methodId asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtMethodList);
                }

                /******************** User Name  ***************************/

                DataSet pplRow = new DataSet();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string pplSchema = "MBDR_System";
                    string pplTbl = "Users";
                    int userName = Convert.ToInt32(Session["userid"]);
                    string query = "";
                    query = string.Format(@"SELECT ppl.firstName FROM {0}.{1} ppl WHERE userId = {2}", pplSchema, pplTbl, userName);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(pplRow);
                    if (pplRow.Tables[0].Rows.Count > 0)
                    {
                        assignedUserName = pplRow.Tables[0].Rows[0][0].ToString();
                        hdfUserName.Value = assignedUserName;
                    }
                }


                /******************** DataControlRowType ***************************/

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var currentRowData = e.Row.DataItem;
                    
                    var id = DataBinder.Eval(e.Row.DataItem, "diagnosis");

                    e.Row.Attributes.Add("data-id", Convert.ToString(id));

                    if (previousRowData != null)
                    {
                        string currentFieldValue = Convert.ToString(((System.Data.DataRowView)currentRowData).Row.ItemArray[0]);
                       
                        string previousFieldValue = Convert.ToString(((System.Data.DataRowView)previousRowData).Row.ItemArray[0]);// DataBinder.Eval(previousRowData, "");

                        if (currentFieldValue.Equals(previousFieldValue))
                        {
                            e.Row.Style.Add("display", "none");
                            
                            /* Below if there is a history, then first one with Zero status Id will add Plus Sign and visible true */

                            string statusIId = Convert.ToString(((System.Data.DataRowView)previousRowData).Row.ItemArray[3]);
                            if (statusIId == "0")
                            {
                                Label showPlus =(Label)previousRow.FindControl("plusSign");
                                showPlus.Visible = true;
                            }

                            DropDownList ddlVerifyStatus = (DropDownList)e.Row.FindControl("ddlVerifyStatus");
                            ddlVerifyStatus.DataSource = dtStatusList;
                            ddlVerifyStatus.DataTextField = "details";
                            ddlVerifyStatus.DataValueField = "statusId";
                            ddlVerifyStatus.DataBind();
                            ddlVerifyStatus.Items.Insert(0, "SELECT STATUS");

                            var dataItem = (DataRowView)e.Row.DataItem;

                            string statusSaveId = dataItem["statusId"].ToString();

                            if (!string.IsNullOrEmpty(statusSaveId))
                            {
                                ListItem selectedItem = ddlVerifyStatus.Items.FindByValue(statusSaveId);
                                if (selectedItem != null)
                                {
                                    selectedItem.Selected = true;
                                }
                            }

                            ddlVerifyStatus.Enabled = false;

                            CheckBoxList cblList = (CheckBoxList)e.Row.FindControl("idVerfiyMethod");
                            cblList.DataSource = dtMethodList;
                            cblList.DataTextField = "details";
                            cblList.DataValueField = "methodId";
                            cblList.DataBind();
                            cblList.CssClass = "checbox-list-disabled";
                            cblList.Enabled = false;

                            string methodIds = DataBinder.Eval(e.Row.DataItem, "methodIds").ToString();

                            string[] methodArray = methodIds.Split(',');


                            foreach (var checkmethodId in methodIds)
                            {
                                for (int i = 0; i < cblList.Items.Count; i++)
                                {
                                    if (Convert.ToString(cblList.Items[i].Value) == Convert.ToString(checkmethodId))
                                    {
                                        cblList.Items[i].Selected = true;
                                        cblList.Items[i].Enabled = false;
                                    }
                                    else
                                    {
                                        cblList.Items[i].Enabled = false;
                                    }
                                }
                            }

                            // Retrieve the verifyId value for the current row

                            string comment = dataItem["comment"].ToString();
                            Label lblComment = (Label)e.Row.FindControl("lblComment");
                            lblComment.Text = comment;
                            lblComment.Style["display"] = "block";


                            Button BtnSave = (Button)e.Row.FindControl("btnSave");
                            BtnSave.Style["display"] = "none";

                            /* making history greyout */

                            e.Row.BackColor = ColorTranslator.FromHtml("#F0F8FF");
                          
                            if(e.Row.BackColor == ColorTranslator.FromHtml("#F0F8FF"))
                            {
                                e.Row.Cells[0].CssClass = "indent";
                            }
                        }
                        else
                        {
                            DropDownList ddlVerifyStatus = (DropDownList)e.Row.FindControl("ddlVerifyStatus");
                            ddlVerifyStatus.DataSource = dtStatusList;
                            ddlVerifyStatus.DataTextField = "details";
                            ddlVerifyStatus.DataValueField = "statusId";
                            ddlVerifyStatus.DataBind();
                            ddlVerifyStatus.Items.Insert(0, "SELECT STATUS");

                            if (!string.IsNullOrEmpty(statusId))
                            {
                                ListItem selectedItem = ddlVerifyStatus.Items.FindByValue(statusId);
                                if (selectedItem != null)
                                {
                                    selectedItem.Selected = true;
                                }
                            }

                            CheckBoxList cblList = (CheckBoxList)e.Row.FindControl("idVerfiyMethod");
                            cblList.DataSource = dtMethodList;
                            cblList.DataTextField = "details";
                            cblList.DataValueField = "methodId";
                            cblList.DataBind();


                            // Retrieve the verifyId value for the current row
                            var dataItem = (DataRowView)e.Row.DataItem;

                            TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
                            txtComment.Style["display"] = "block";
                        }
                    }
                    else
                    {
                        DropDownList ddlVerifyStatus = (DropDownList)e.Row.FindControl("ddlVerifyStatus");
                        ddlVerifyStatus.DataSource = dtStatusList;
                        ddlVerifyStatus.DataTextField = "details";
                        ddlVerifyStatus.DataValueField = "statusId";
                        ddlVerifyStatus.DataBind();
                        ddlVerifyStatus.Items.Insert(0, "SELECT STATUS");

                        if (!string.IsNullOrEmpty(statusId))
                        {
                            ListItem selectedItem = ddlVerifyStatus.Items.FindByValue(statusId);
                            if (selectedItem != null)
                            {
                                selectedItem.Selected = true;
                            }
                        }                      

                        CheckBoxList cblList = (CheckBoxList)e.Row.FindControl("idVerfiyMethod");
                        cblList.DataSource = dtMethodList;
                        cblList.DataTextField = "details";
                        cblList.DataValueField = "methodId";
                        cblList.DataBind();

                        TextBox txtComment = (TextBox)e.Row.FindControl("txtComment");
                        txtComment.Style["display"] = "block";
                    }

                    previousRowData = currentRowData;
                    previousRow = e.Row;
                }

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_gvVerificationStatus_RowDataBound");
            }
        }

        protected void gvVerificationStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "SaveRow")
                {
                    string statusId = string.Empty;

                    int rowIndex = Convert.ToInt32(e.CommandArgument);
                    GridViewRow row = gvVerificationStatus.Rows[rowIndex];


                    /*** ICD 10 Diagnosis Code **/

                    string ICD10Diagnosis = gvVerificationStatus.DataKeys[rowIndex].Value.ToString();

                    HiddenField hf = (HiddenField)row.FindControl("diagnosisId");
                    string diagnosisIds = hf.Value;

                    /*** choosen dropdown status **/

                    DropDownList ddlStatus = (DropDownList)row.FindControl("ddlVerifyStatus");
                    foreach (ListItem item in ddlStatus.Items)
                    {
                        statusId = ddlStatus.SelectedValue;
                    }

                    /*** choosen dropdown methods **/

                    CheckBoxList cblMethod = (CheckBoxList)row.FindControl("idVerfiyMethod");
                    List<int> selectedValues = new List<int>();
                    foreach (ListItem item in cblMethod.Items)
                    {
                        if (item.Selected)
                        {
                            selectedValues.Add(int.Parse(item.Value));
                        }
                    }
                    string selectedMethodIds = JsonConvert.SerializeObject(selectedValues);

                    /*** User comments **/

                    TextBox userInput = (TextBox)row.FindControl("txtComment");
                    string comment = "" + userInput.Text + "";

                    comment = JsonConvert.SerializeObject(comment);

                    /*** report ID **/

                    ddlStatus.Focus();
                    cblMethod.Focus();
                    userInput.Focus();

                    string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";

                    string json = string.Empty;

                    json = "{\"cmd\":\"VERIFIED\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"comment\":" + comment + ",\"report\":" + reportId + "," +
                        "\"status\":" + statusId + ",\"diagnosis\":" + diagnosisIds + ",\"methods\":" + selectedMethodIds + "}";

                    string response = SocketConn(json);
                    string getMsgs = $"gvVerificationStatus_RowCommand|Request|{json}|Response|{response}";
                    ErrorLog(getMsgs);
                    if (response == "SocketConnectionError")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);

                        BindAllFunctions();
                    }
                    else
                    {
                        dynamic data = JObject.Parse(response);

                        if (data.name == "SUCCESS")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Save Success .", "ComDial( 'Verification data has been saved.');", true);

                            BindAllFunctions();
                        }
                        else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                            return;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Save Success .", "ComDial( 'Your request encountered an error and cannot be processed. Please report this message to Altarum Support.');", true);

                            BindAllFunctions();
                        }
                    }
                }             
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_gvVerificationStatus_RowCommand");
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Remove Show Progress", "removeProgress(); ", true);
        }

        public string SocketConn(string JsonRecord)
        {
            string responseFromServer = string.Empty;
            string socketIp = ConfigurationManager.AppSettings["ipAddress"];
            int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
            try
            {
                using (var socket = new TcpClient(socketIp, socketPort))
                {
                    string json = JsonRecord;
                    byte[] body = Encoding.UTF8.GetBytes(json);
                    int bodyLength = Encoding.UTF8.GetByteCount(json);
                    var bl = (byte)(bodyLength);
                    var bh = (byte)(bodyLength >> 8);
                    using (var stream = socket.GetStream())
                    {
                        stream.WriteByte(bh);
                        stream.WriteByte(bl);
                        stream.Write(body, 0, bodyLength);

                        short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                        byte[] buffer = new byte[size];

                        int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                        if (sizestream == size)
                        {
                            responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                        }
                        else
                        {
                            responseFromServer = "SocketConnectionError";
                        }
                    }
                }
            }
            catch (Exception)
            {
                responseFromServer = "SocketConnectionError";
            }
            return responseFromServer;
        }

        protected void btnAll_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Verification.aspx?schema=MBDR_System&table=getCaseVerificationList", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetails_btnAll_Click");
            }
        }

        protected void btnAdvanceResult_Click(object sender, EventArgs e)
        {
            try
            {
                dt = null;
                dt = (DataTable)Session["datarecords"];

                string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

                string where = getQueryString();

                string sortExpre = "";

                if (string.IsNullOrEmpty(sortExp))
                {
                    sortExpre = "childLastName asc, childMRN asc, admissionDate asc";
                }
                else
                {
                    sortExpre = sortExp;
                }

                Response.Redirect("Verification.aspx?schema=MBDR_System&table=getCaseVerificationList" + "&sortExp=" + sortExpre + "&where=" + where + "", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationsDetails_btnAdvanceResult_Click");
            }
        }

        private string getQueryString()
        {
            string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";
            string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
            string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
            string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
            string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
            string childMRN = !string.IsNullOrEmpty(Request.QueryString["childMRN"]) ? Convert.ToString(Request.QueryString["childMRN"]) : "";
            string admissionDate = !string.IsNullOrEmpty(Request.QueryString["admissionDate"]) ? Convert.ToString(Request.QueryString["admissionDate"]) : "";
            string dischargeDate = !string.IsNullOrEmpty(Request.QueryString["dischargeDate"]) ? Convert.ToString(Request.QueryString["dischargeDate"]) : "";
            string chkDiagCodes = !string.IsNullOrEmpty(Request.QueryString["chkDiagCodes"]) ? Convert.ToString(Request.QueryString["chkDiagCodes"]) : "";

            string reportStatus = !string.IsNullOrEmpty(Request.QueryString["reportStatus"]) ? Convert.ToString(Request.QueryString["reportStatus"]) : "";
            string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

            string whereSep = "1";

            if (!string.IsNullOrEmpty(chkDiagCodes))
            {
                whereSep += "&chkDiagCodes=" + chkDiagCodes.Trim() + "";
            }

            if (!string.IsNullOrEmpty(reportId))
            {
                whereSep += "&reportId=" + reportId.Trim() + "";
            }

            if (!string.IsNullOrEmpty(childLastName))
            {

                whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childFirstName))
            {

                whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childMiddleName))
            {

                whereSep += "&childMiddleName=" + childMiddleName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(birthDate))
            {

                whereSep += "&birthDate=" + birthDate + "";
            }
            if (!string.IsNullOrEmpty(childMRN))
            {

                whereSep += "&childMRN=" + childMRN.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(admissionDate))
            {
                whereSep += "&admissionDate=" + admissionDate + "";
            }
            if (!string.IsNullOrEmpty(dischargeDate))
            {
                whereSep += "&dischargeDate=" + dischargeDate + "";
            }

            if (!string.IsNullOrEmpty(admittingEntity))
            {
                whereSep += "&admittingEntity=" + admittingEntity + "";
            }

            if (!string.IsNullOrEmpty(reportStatus))
            {
                whereSep += "&reportStatus=" + reportStatus + "";
            }

            return whereSep;
        }

        /***********  CHD/CCHD Case Verification   ********************/
        protected void gvCCHDCaseVerification_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if(e.Row.RowType == DataControlRowType.Header) {
            //    e.Row.Cells[0].ColumnSpan = 2;
            //    e.Row.Cells.RemoveAt(1);
            //}
        }

        protected void gvCCHDCaseVerification_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRow row = ((DataRowView)e.Row.DataItem).Row;

                    if (row != null && Convert.ToInt64(row["reportId"]) != 0)
                    {
                        if (e.Row.RowIndex == 0)
                        {
                            DropDownList ddlPrenatalCCHDDiagnosis = (DropDownList)e.Row.FindControl("ddlPrenatalCCHDDiagnosis");
                            //This will make your ddl readonly 
                            ddlPrenatalCCHDDiagnosis.Enabled = true;
                            ddlPrenatalCCHDDiagnosis.CssClass = "cursor-pointerWhite";
                          
                            string statusId = Convert.ToString(row["statusId"]);
                            string occurrence = Convert.ToString(row["occurrence"]);
                            TextBox txtoccurrence = (TextBox)e.Row.FindControl("dateCCHDIndic");
                            txtoccurrence.Attributes["min"] = DateTime.Today.ToString("2017-01-01"); // will need to make it 01/01/2017 and validation error should be handled.
                            txtoccurrence.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                            ListItem selectedItem = ddlPrenatalCCHDDiagnosis.Items.FindByValue(statusId);
                            if (selectedItem != null)
                            {
                                Button btnSave = (Button)e.Row.FindControl("btnSaveCCHD");
                                btnSave.OnClientClick = "return ShowProgress(event);";
                                btnSave.Enabled = true;
                                btnSave.CssClass = "cursor-pointer";
                            }
                        }
                        else
                        {
                            DropDownList ddlPrenatalCCHDDiagnosis = (DropDownList)e.Row.FindControl("ddlPrenatalCCHDDiagnosis");
                            //This will make your ddl readonly 
                            ddlPrenatalCCHDDiagnosis.Enabled = false;
                            ddlPrenatalCCHDDiagnosis.CssClass = "cursor-default";
                         
                            string statusId = Convert.ToString(row["statusId"]);
                            string occurrence = Convert.ToString(row["occurrence"]);
                            ListItem selectedItem = ddlPrenatalCCHDDiagnosis.Items.FindByValue(statusId);

                            if (selectedItem != null)
                            {
                                selectedItem.Selected = true;
                                TextBox txtoccurrence = (TextBox)e.Row.FindControl("dateCCHDIndic");

                                txtoccurrence.Enabled = false;
                                txtoccurrence.CssClass = "cursor-defaultDateGO";
                                DateTime occurrenceDate;

                                if (DateTime.TryParse(occurrence, out occurrenceDate))
                                {
                                    txtoccurrence.Text = occurrenceDate.ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    txtoccurrence.Attributes["PlaceHolder"] = "--/--/----";
                                    txtoccurrence.TextMode = TextBoxMode.SingleLine;
                                }

                                Button btnSave = (Button)e.Row.FindControl("btnSaveCCHD");
                                btnSave.Enabled = false;
                                btnSave.Visible = false;
                                btnSave.CssClass = "cursor-default";

                                /******** for caseverificaiton date on action column *********/

                                Label cchdVdateNuser = (Label)e.Row.FindControl("cchdVdateNuser");
                                cchdVdateNuser.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        DropDownList ddlPrenatalCCHDDiagnosis = (DropDownList)e.Row.FindControl("ddlPrenatalCCHDDiagnosis");
                        //This will make your ddl readonly 
                        ddlPrenatalCCHDDiagnosis.Enabled = false;
                        ddlPrenatalCCHDDiagnosis.CssClass = "cursor-default";

                        TextBox txtoccurrence = (TextBox)e.Row.FindControl("dateCCHDIndic");
                        txtoccurrence.Enabled = false;
                        txtoccurrence.CssClass = "cursor-defaultDateGO";

                        Button btnSave = (Button)e.Row.FindControl("btnSaveCCHD");
                        btnSave.Enabled = false;
                        btnSave.CssClass = "cursor-default";
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationsDetails_gvCCHDCaseVerification_RowDataBound");
            }            
        }

        protected void gvCCHDCaseVerification_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";
                string json = string.Empty;
                string statusId = hdfCCHDDiagnosis.Value;
                string comment = hdfCCHDDiagnosisDate.Value;
                comment = JsonConvert.SerializeObject(comment);

                json = "{\"cmd\":\"VERIFIED\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"report\":" + reportId + "," +
                "\"status\":" + statusId + ",\"comment\":" + comment + "}";

                string response = SocketConn(json);
                string getMsgs = $"gvCCHDCaseVerification_RowCommand|Request|{json}|Response|{response}";
                ErrorLog(getMsgs);
                hdfCCHDDiagnosis.Value = string.Empty;
                hdfCCHDDiagnosisDate.Value = string.Empty;

                if (response == "SocketConnectionError")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);

                    BindAllFunctions();
                }
                else
                {
                    dynamic data = JObject.Parse(response);
                    if (data.name == "SUCCESS")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Save Success .", "ComDial( 'Verification data has been saved.');", true);

                        BindAllFunctions();
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Save Success .", "ComDial( 'Your request encountered an error and cannot be processed. Please report this message to Altarum Support.');", true);

                        BindAllFunctions();
                    }
                }
            }
            catch (Exception ex )
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationsDetails_gvCCHDCaseVerification_RowCommand");
            } 

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Remove Show Progress", "removeProgress(); ", true);
        }

        protected void BindAllFunctions()
        {
            string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";

            BindCVPersonDetails("MBDR_System", "getCaseVerificationPersonDetails", reportId);

            BindCVCHDDetails("MBDR_System", "getCaseVerificationCHDDetails", reportId);

            BindCVDiagnosisDetails("MBDR_System", "getCaseVerificationDiagnosisDetails", reportId);
        }

        public static void ErrorLog(string values)
        {
            bool shot = true;
            if (shot)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.Snap(values, path, "VerificationDetails.aspx");
            }
        }
    }
}