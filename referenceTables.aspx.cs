﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

public partial class referenceTables : System.Web.UI.Page
{
    protected List<BDRTablesList> BDRTables;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        BDRTables = new List<BDRTablesList>
        {
            new BDRTablesList() { Name = "Countries", DBName = "Master_Country", SCHName = "Reference" },
            new BDRTablesList() { Name = "Counties", DBName = "Master_County", SCHName = "Reference" },
            new BDRTablesList() { Name = "Hospitals", DBName = "Entities", SCHName = "Reference" },
            new BDRTablesList() { Name = "Submitters", DBName = "Memberships", SCHName = "Reference" },
            new BDRTablesList() { Name = "Cities", DBName = "Master_Cities", SCHName = "Reference" },
            new BDRTablesList() { Name = "Streets", DBName = "Master_Streets", SCHName = "Reference" },
            new BDRTablesList() { Name = "Zip Data", DBName = "Master_ZipDataB", SCHName = "Reference" },
            new BDRTablesList() { Name = "City Aliases", DBName = "Master_Relabel", SCHName = "Reference" },
            new BDRTablesList() { Name = "Hearing Codes", DBName = "Hearing_Codes", SCHName = "Reference" },
            new BDRTablesList() { Name = "Total ICD9-10 Codes", DBName = "Reference_TotalICD9_10", SCHName = "Reference" },
            new BDRTablesList() { Name = "Total ICD9-10-Snomed", DBName = "Reference_TotalICD9_10_Snomed", SCHName = "Reference" },
        };
    }

    public class BDRTablesList
    {
        public string Name { get; set; }
        public string DBName { get; set; }
        public string SCHName { get; set; }
    }
}