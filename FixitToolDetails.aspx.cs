﻿using DynamicGridView.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class FixitToolDetails : System.Web.UI.Page
    {
        dynamic permissions = null;
        protected DataTable dtFixItCases = null;
        protected DataTable dtFixItReportDataCases = null;
        protected string getMsgs;
        protected DataTable dtFixList;

        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();

            try
            {
                string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
                string fixer = !string.IsNullOrEmpty(Request.QueryString["fixer"]) ? Convert.ToString(Request.QueryString["fixer"]) : "";

                if (!IsPostBack)
                {
                    hdfFixitId.Value = fixItId;

                    GetFlaggedInfo(fixItId);

                    if (string.IsNullOrEmpty(fixer) || fixer == "Select Owner")
                    {
                        FTAKE(fixItId);
                    }

                    if (Session["ErrorOccurred"] != null && (bool)Session["ErrorOccurred"] == true)
                    {
                        // Clear the error flag to allow normal processing on future requests

                        mainScreen.Attributes["style"] = "display:none;";

                        Session["Erroroccurred"] = false;

                        return;
                    }

                    BindGridData(fixItId);
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_Page_Load");
            }
        }

        private void BindGridData(string fixItId)
        {
            try
            {
                //1. Release Button Disable
                string schema = "MBDR_System";
                string tableName = "FixItDataDetails";
                string strCountTargets = "select count(*) 'RowCount' from MBDR_System.FixItTargets where fixItId = " + fixItId + "";
                string caseID = string.Empty;
               
                DataTable dtFixItTargets = new DataTable();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(strCountTargets, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtFixItTargets);

                    if (Convert.ToString(dtFixItTargets.Rows[0][0]) != "0")   // Release button disable if any previous action made 
                    {
                        btnRelease.Enabled = true;
                        btnRelease.CssClass = "disableGrey"; // Currently enable all the time - MBDRMO #1023
                    }
                    else
                    {
                        btnRelease.Enabled = true;
                        btnRelease.CssClass = "disableGrey";
                    }

                    LitActions.Text = Convert.ToString(dtFixItTargets.Rows[0][0]);
                }


                //
                List<FixItViewModel> fixItCaseDataViewModels = new List<FixItViewModel>();
                FixItViewModel fixItReportDataViewModels = new FixItViewModel();

                //3. Get List of 

                string strQueryCaseID = "select * from MBDR_System.FixItCaseData where fixItId = " + fixItId + " order by caseId asc";
                DataTable dtFixItCaseData = new DataTable();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(strQueryCaseID, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtFixItCaseData);
                    // 4. Get CaseID + Records from FixItCaseData
                    foreach (DataRow dr in dtFixItCaseData.Rows)
                    {
                        fixItReportDataViewModels = ListRecords(dr, "Case Record");
                        fixItCaseDataViewModels.Add(fixItReportDataViewModels);

                        string strQueryCaseReportID = "select * from MBDR_System.FixItReportData where fixItId = " + fixItId + " and caseId = " + Convert.ToString(dr["caseId"]) + " order by admissionDate asc, reportId asc";
                        DataTable dtFixItCaseReportData = new DataTable();
                        SqlCommand cmdReport = new SqlCommand(strQueryCaseReportID, conn);
                        cmdReport.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daReport = new SqlDataAdapter(cmdReport);
                        conn.Close();
                        daReport.Fill(dtFixItCaseReportData);

                        // 5. Get Records from FixItCaseReportData
                        foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                        {
                            fixItReportDataViewModels = new FixItViewModel();
                            fixItReportDataViewModels = ListRecords(dataRow, "Case Report");
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }
                }

                // Data fill 

                //2. Get List of column 
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;

                PropertyInfo[] properties = typeof(FixItViewModel).GetProperties();
                var filter = Common.Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();

                if (filter != null)
                {
                    includeColumns = filter.IncludedColumns;
                    readonlyColumns = filter.ReadonlyColumns;
                    dataKeyname = filter.DataKeyName;
                }

                List<string> lstColumns = includeColumns.Split(',').ToList();


                dtFixItReportDataCases = new DataTable();

                foreach (PropertyInfo property in properties)
                {
                    var colName = lstColumns.FirstOrDefault(s => s.StartsWith(property.Name, StringComparison.OrdinalIgnoreCase));
                    var displayColname = "";
                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnName = colName.Split(':')[1];

                        displayColname = columnName;
                    }
                    else
                    {
                        string colDName = property.Name;

                        foreach (var col in lstColumns)
                        {
                            string columnName = col.Split(':')[0];

                            if (colDName.Contains(columnName))
                            {
                                displayColname = col.Split(':')[1];
                                break;
                            }
                        }
                    }

                    dtFixItReportDataCases.Columns.Add(displayColname);
                }

                foreach (FixItViewModel item in fixItCaseDataViewModels)
                {
                    DataRow dr = dtFixItReportDataCases.NewRow();

                    foreach (PropertyInfo property in properties)
                    {

                        var colName = lstColumns.FirstOrDefault(s => s.StartsWith(property.Name, StringComparison.OrdinalIgnoreCase));
                        var displayColname = "";
                        if (!string.IsNullOrEmpty(colName))
                        {
                            string columnName = colName.Split(':')[1];

                            displayColname = columnName;
                        }
                        else
                        {
                            string colDName = property.Name;

                            foreach (var col in lstColumns)
                            {
                                string columnName = col.Split(':')[0];

                                if (colDName.Contains(columnName))
                                {
                                    displayColname = col.Split(':')[1];
                                    break;
                                }
                            }
                        }

                        dr[displayColname] = Convert.ToString(property.GetValue(item, null));
                    }

                    dtFixItReportDataCases.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixtitToolDetails_BindGridData");
            }
        }

        protected FixItViewModel ListRecords(DataRow dr, string tableName)
        {
            FixItViewModel item = new FixItViewModel();

            item.CaseRecordTable = tableName;


            if (tableName == "Case Record")
            {
                item.ScrutinizerId = "";
                item.CaseRecId = Convert.ToString(dr["caseRecId"]);
                item.ReportId = "";
            }
            else
            {
                item.ScrutinizerId = Convert.ToString(dr["scrutinizerId"]);
                item.CaseRecId = "";
                item.ReportId = Convert.ToString(dr["reportId"]);
            }
            item.BirthCertNumber = Convert.ToString(dr["birthCertNumber"]);
            item.DeathNumber = Convert.ToString(dr["deathNumber"]);
            item.ChildLastName = Convert.ToString(dr["childLastName"]);
            item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
            item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
            item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
            item.AliasLastName = Convert.ToString(dr["aliasLastName"]);
            item.AliasFirstName = Convert.ToString(dr["aliasFirstName"]);
            item.AliasMiddleName = Convert.ToString(dr["aliasMiddleName"]);
            item.BirthDate = Convert.ToString(dr["birthDate"]);
            item.ChildSSN = Convert.ToString(dr["childSSN"]);
            item.ChildMRN = Convert.ToString(dr["childMRN"]);
            item.ChildMedicaidNumber = Convert.ToString(dr["childMedicaidNumber"]);
            item.Address1 = Convert.ToString(dr["address1"]);
            item.Address2 = Convert.ToString(dr["address2"]);
            item.City = Convert.ToString(dr["city"]);
            item.County = Convert.ToString(dr["county"]);
            item.State = Convert.ToString(dr["state"]);
            item.ZipCode = Convert.ToString(dr["zipcode"]);
            item.Gender = Convert.ToString(dr["gender"]);
            item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
            item.BirthHospital = Convert.ToString(dr["birthHospital"]);
            item.Plurality = Convert.ToString(dr["plurality"]);
            item.BirthOrder = Convert.ToString(dr["birthOrder"]);
            item.HeadCircumference = Convert.ToString(dr["headCircumference"]);
            item.DeliveryLength = Convert.ToString(dr["deliveryLength"]);
            item.MomSSN = Convert.ToString(dr["momSSN"]);
            item.MomLastName = Convert.ToString(dr["momLastName"]);
            item.MomFirstName = Convert.ToString(dr["momFirstName"]);
            item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
            item.MomSuffix = Convert.ToString(dr["momSuffix"]);
            item.EntityName = Convert.ToString(dr["entityName"]);
            item.AdmissionDate = Convert.ToString(dr["admissionDate"]);
            item.DischargeDate = Convert.ToString(dr["dischargeDate"]);
            item.HistoricId = Convert.ToString(dr["historicId"]);
            item.CaseId = Convert.ToString(dr["caseId"]);
            return item;
        }

        protected void GetFlaggedInfo(string fixItId)
        {
            try
            {
                string schema = "MBDR_System";
                string tableName = "FixItListing";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = $"SELECT reporter, flagged, comment FROM {schema}.{tableName} WHERE fixItId = @fixItId";

                    query = string.Format(query, schema, tableName);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    cmd.Parameters.AddWithValue("@fixItId", fixItId);

                    dtFixList = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    conn.Open();
                    da.Fill(dtFixList);
                    conn.Close();

                    if (dtFixList.Rows.Count > 0)
                    {
                        DataRow row = dtFixList.Rows[0];

                        litFid.Text = fixItId;
                        // Set values to Literal controls
                        litReporter.Text = row["reporter"].ToString();
                        LitDateFlagged.Text = Convert.ToDateTime(row["flagged"]).ToString("MM/dd/yyyy");
                        string comments = row["comment"].ToString();
                        if (comments.Length > 100)
                        {
                            LitComments.Text = $"<span title='{comments}'>{comments.Substring(0, 100)}...</span>";
                        }
                        else
                        {
                            LitComments.Text = $"<span title='{comments}'>{comments}</span>";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixtitToolDetails_GetFlaggedInfo");
            }
        }
        
        protected void FTAKE(string fixItId)
        {
            try
            {
                string json = string.Empty;

                json = "{\"cmd\":\"FTAKE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + fixItId + "}";

                string response = SocketConn(json);
                getMsgs = $"FTAKE|Request|{json}|Response|{response}";
                ErrorLog(getMsgs);

                if (response == "SocketConnectionError")
                {
                    Session["ConFail"] = true;

                    Response.Redirect("FixitTool.aspx", true);

                    Context.ApplicationInstance.CompleteRequest();

                    return;
                }
                else
                {
                    dynamic data = JObject.Parse(response);

                    if (data["name"] != null && (Convert.ToString(data["name"]) == "EXCEPTION" || data["status"].ToString() == "13"))
                    {
                        // Set an error flag in the session 

                        Session["ErrorOccurred"] = true;

                        string errMsg = "Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen. Reference FixId # " + Convert.ToInt32(Uri.EscapeDataString(fixItId));

                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs( '" + errMsg + "', 'Fixer Server Error Message');", true);
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "BAD_STATE" && data["status"].ToString() == "11"))
                    {
                        Session["ErrorOccurred"] = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "NOT_OWNER" && data["status"].ToString() == "9"))
                    {
                        Session["ErrorOccurred"] = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "Ownership Error", "errorMsgs('This Fixable is owned by another user. Please find another Fixable to work.', 'Not Owner');", true);
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixtitToolDetails_FTAKE");
            }
        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "FIXIT_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_CheckRole");
            }
            return permissions;
        }

        private string getQueryString()
        {
            string Status = !string.IsNullOrEmpty(Request.QueryString["ddlFilterByStatus"]) ? Convert.ToString(Request.QueryString["ddlFilterByStatus"]) : "";
            string Owner = !string.IsNullOrEmpty(Request.QueryString["ddlFilterByOwner"]) ? Convert.ToString(Request.QueryString["ddlFilterByOwner"]) : "";
            string FilterByFlagged = !string.IsNullOrEmpty(Request.QueryString["ddlFilterByFlagged"]) ? Convert.ToString(Request.QueryString["ddlFilterByFlagged"]) : "";
            string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
            string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";

            string whereSep = "1";

            if (!string.IsNullOrEmpty(Status))
            {
                whereSep += "&resolved=" + Status + "";
            }
            if (!string.IsNullOrEmpty(Owner))
            {
                whereSep += "&fixer=" + Owner + "";
            }
            if (!string.IsNullOrEmpty(FilterByFlagged))
            {
                whereSep += "&reporter=" + FilterByFlagged + "";
            }

            if (!string.IsNullOrEmpty(childLastName))
            {

                whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(birthDate))
            {

                whereSep += "&birthDate=" + birthDate + "";
            }

            return whereSep;
        }

        protected void btnReturnResult_Click(object sender, EventArgs e)
        {
            try
            {
                string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

                string where = getQueryString();

                string sortExpre = "";

                if (string.IsNullOrEmpty(sortExp))
                {
                    sortExpre = "flagged desc";
                }
                else
                {
                    sortExpre = sortExp;
                }

                BindGridData(hdfFixitId.Value);

                int rowId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfFixitId.Value)));

                Response.Redirect("FixitTool.aspx?schema=MBDR_System&table=FixItListing&rowId=" + rowId + "&where=" + where + "&sortExp=" + sortExpre + ",", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_btnReturnResult_Click");
            }
        }

        protected void btnRelease_Click(object sender, EventArgs e)
        {
            try
            {
                string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
                string json = "{\"cmd\":\"FRELEASE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + hdfFixitId.Value + "}";

                string response = SocketConn(json);
                getMsgs = $"btnRelease|Request|{json}|Response|{response}";
                ErrorLog(getMsgs);

                if (response == "SocketConnectionError")
                {
                    BindGridData(fixItId);
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                }
                else
                {
                    dynamic data = JObject.Parse(response);

                    if (data.name == "SUCCESS")
                    {
                        Response.Redirect("FixitTool.aspx", false);
                        return;
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "btnRelease_Click", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_btnRelease_Click");
            }

            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnResolved_Click(object sender, EventArgs e)
        {
            try
            {
                string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
                string json = "{\"cmd\":\"FRESOLVED\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + hdfFixitId.Value + "}";

                string response = SocketConn(json);
                getMsgs = $"btnResolved_Click|Request|{json}|Response|{response}";
                ErrorLog(getMsgs);
                if (response == "SocketConnectionError")
                {
                    BindGridData(fixItId);
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                }
                else
                {
                    dynamic data = JObject.Parse(response);

                    if (data.name == "SUCCESS")
                    {
                        Response.Redirect("FixitTool.aspx", false);
                        return;
                    }
                    else if (data["name"] != null && (Convert.ToString(data["name"]) == "SECURITY" && data["status"].ToString() == "22"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "btnResolved_Click", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                    }
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_btnResolved_Click");
            }

            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnMove_Click(object sender, EventArgs e)
        {
            try
            {
                string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
                string targetCaseRecId = hdfCaseRecordId.Value;
                string movedcaseReportIds = hdfCaseReportId.Value;

                string comment = txtFixTool.Text;

                string fixitDetailsComment = JsonConvert.SerializeObject(comment);

                string json = "{\"cmd\":\"FMOVE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + hdfFixitId.Value + ",\"target\":" + targetCaseRecId + ",\"reports\":[" + movedcaseReportIds + "],\"comment\":" + fixitDetailsComment + "}";

                string response = SocketConn(json);
                            
                getMsgs = $"btnMove_Click|UID|{Convert.ToInt32(Session["userid"])}|response:{response}";
                ErrorLog(getMsgs);

                if (response == "SocketConnectionError")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                }
                else
                {
                    FiXItRoot data = JsonConvert.DeserializeObject<FiXItRoot>(response);

                    if (data.name == "CHECK_BOTH" || data.name == "CHECK_BIRTH" || data.name == "CHECK_DEATH")
                    {
                        hdfPrimary.Value = Convert.ToString(data.primary);
                        string deathIds = "";
                        string birthIds = "";

                        for (int i = 0; i < data.deaths.Count; i++)
                        {
                            if (i == 0)
                            {
                                deathIds = data.deaths[i];
                            }
                            else
                            {
                                deathIds = deathIds + "," + data.deaths[i];
                            }
                        }

                        hdfDeath.Value = deathIds;


                        for (int i = 0; i < data.births.Count; i++)
                        {
                            if (i == 0)
                            {
                                birthIds = data.births[i];
                            }
                            else
                            {
                                birthIds = birthIds + "," + data.births[i];
                            }
                        }

                        hdfCheckName.Value = data.name;
                        hdfBirths.Value = birthIds;
                        hdfComments.Value = fixitDetailsComment;

                        hdfBirthSource.Value = Convert.ToString(data.sourceBirth);
                        hdfBirthTarget.Value = Convert.ToString(data.targetBirth);
                        hdfDeathSource.Value = Convert.ToString(data.sourceDeath);
                        hdfDeathTarget.Value = Convert.ToString(data.targetDeath);

                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "BDConflictDialog('move');", true);

                    }
                    else if (data.name != null && data.name == "SECURITY" && Convert.ToString(data.status) == "22")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else if (data.name != "SUCCESS")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "btnMove_Click", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                    }
                    else
                    {
                        Response.Redirect(Request.RawUrl, false);
                    }                  
                    BindGridData(fixItId);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_btnMove_Click");
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";

                string reportids = hdfCaseReportId.Value;

                var uniqueID = reportids.Split(',').Select(int.Parse).Distinct().ToArray();

                reportids = string.Join(",", uniqueID);

                string json = string.Empty;

                string comment = txtFixTool.Text;

                string fixitDetailsComment = JsonConvert.SerializeObject(comment);

                json = "{\"cmd\":\"FCREATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + hdfFixitId.Value + ",\"reports\":[" + reportids + "],\"comment\":" + fixitDetailsComment + "}";

                string response = SocketConn(json);
               
                getMsgs = $"btnCreate_Click|UID|{Convert.ToInt32(Session["userid"])}|response:{response}";
                ErrorLog(getMsgs);

                if (response == "SocketConnectionError")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                }
                else
                {
                    FiXItRoot data = JsonConvert.DeserializeObject<FiXItRoot>(response);

                    if (data.name == "CHECK_BOTH" || data.name == "CHECK_BIRTH" || data.name == "CHECK_DEATH")
                    {
                        hdfPrimary.Value = Convert.ToString(data.primary);
                        string deathIds = "";
                        string birthIds = "";

                        for (int i = 0; i < data.deaths.Count; i++)
                        {
                            if (i == 0)
                            {
                                deathIds = data.deaths[i];
                            }
                            else
                            {
                                deathIds = deathIds + "," + data.deaths[i];
                            }
                        }

                        hdfDeath.Value = deathIds;


                        for (int i = 0; i < data.births.Count; i++)
                        {
                            if (i == 0)
                            {
                                birthIds = data.births[i];
                            }
                            else
                            {
                                birthIds = birthIds + "," + data.births[i];
                            }
                        }

                        hdfCheckName.Value = data.name;
                        hdfBirths.Value = birthIds;
                        hdfComments.Value = fixitDetailsComment;

                        hdfBirthSource.Value = Convert.ToString(data.sourceBirth);
                        hdfBirthTarget.Value = Convert.ToString(data.targetBirth);
                        hdfDeathSource.Value = Convert.ToString(data.sourceDeath);
                        hdfDeathTarget.Value = Convert.ToString(data.targetDeath);

                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "BDConflictDialog('create');", true);

                    }
                    else if (data.name != null && data.name == "SECURITY" && Convert.ToString(data.status) == "22")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "clearBGD(); sViolation('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);
                        return;
                    }
                    else
                    {
                        if (data.name != "SUCCESS")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "btnCreate_Click", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                        }
                        else
                        {
                            Response.Redirect(Request.RawUrl, false);
                        }
                    }

                    //txtFixTool.Text = string.Empty;
                    BindGridData(fixItId);
                }
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolDetails_btnCreate_Click");
            }
        }

        public string SocketConn(string JsonRecord)
        {
            string responseFromServer = string.Empty;
            string socketIp = ConfigurationManager.AppSettings["ipAddress"];
            int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
            try
            {
                using (var socket = new TcpClient(socketIp, socketPort))
                {
                    string json = JsonRecord;
                    byte[] body = Encoding.UTF8.GetBytes(json);
                    int bodyLength = Encoding.UTF8.GetByteCount(json);
                    var bl = (byte)(bodyLength);
                    var bh = (byte)(bodyLength >> 8);
                    using (var stream = socket.GetStream())
                    {
                        stream.WriteByte(bh);
                        stream.WriteByte(bl);
                        stream.Write(body, 0, bodyLength);

                        short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                        byte[] buffer = new byte[size];

                        int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                        if (sizestream == size)
                        {
                            responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                        }
                        else
                        {
                            responseFromServer = "SocketConnectionError";
                        }
                    }
                }
            }
            catch (Exception)
            {
                responseFromServer = "SocketConnectionError";
            }
            return responseFromServer;
        }
        
        public static void ErrorLog(string values)
        {
            bool shot = true;
            if (shot)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.Snap(values, path, "FixitToolDetails.aspx");
            }
        }

    }
}