﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MLLinkModelView.aspx.cs" Inherits="MLModelView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
    <link href="Content/css/AltarumWebApp.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <% if (Session["username"] != null)
        { %>
    <%--<h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        Matching and Linking: Side-By-Side Comparison</h1>--%>
      <asp:HiddenField ID="hdfReportType" runat="server" />

     <div id="dialogHyperLink">
        <p id="messageHyperLink"></p>
       <table id="mcbdTable" style="width: 100%" border="1" cellspacing="0">
    <thead>
        <tr>
            <th class="data-element"></th>
            <th class="candidate">Candidate</th>
            <th class="case-report">Case Report</th>
            <th class="birth-record">Birth Record</th>
            <th class="death-record">Death Record</th>
        </tr>
    </thead>
    <tbody>
        <% if (dtAllRecords != null)
            {
                for (int i = 1; i < dtAllRecords.Rows.Count; i++)
                { %>
        <tr>
            <% int k = 0;
                for (int J = 0; J < dtAllRecords.Columns.Count; J++)
                {
                    k = k + 1;
                    if (!string.IsNullOrEmpty(hdfReportType.Value))
                    {
                        if (hdfReportType.Value == "Case Reports" && J == 2 && dtAllRecords.Rows[0][J].ToString() == "Case Report")
                        { %>
                            <td class="white-color"><%= dtAllRecords.Rows[i][J] %></td>
                        <% }
                        else if (hdfReportType.Value == "Birth Records" && J == 3 && dtAllRecords.Rows[0][J].ToString() == "Birth Record")
                        { %>
                            <td class="white-color"><%= dtAllRecords.Rows[i][J] %></td>
                        <% }
                        else if (hdfReportType.Value == "Death Records" && J == 4 && dtAllRecords.Rows[0][J].ToString() == "Death Record")
                        { %>
                            <td class="white-color"><%= dtAllRecords.Rows[i][J] %></td>
                        <% }
                        else
                        { %>
                            <td <%= J == 0 ? "class=\"grey-color\"" : (J == 1 ? "class=\"blue-color\"" : (J == 2 ? "class=\"white-color\"" : (J == 3 ? "class=\"white-color\"" : (J == 4 ? "class=\"white-color\"" : "")))) %>><%= dtAllRecords.Rows[i][J] %></td>
                        <% }
                    }
                    else
                    { %>
                        <td <%= J == 0 ? "class=\"grey-color\"" : (J == 1 ? "class=\"blue-color\"" : (J == 2 ? "class=\"white-color\"" : (J == 3 ? "class=\"white-color\"" : (J == 4 ? "class=\"white-color\"" : "")))) %>><%= dtAllRecords.Rows[i][J] %></td>
                    <% }
                }
                if (k != 5)
                {
                    for (int J = k; J < 5; J++)
                    { %>
                        <td>&nbsp;</td>
                    <% }
                } %>
        </tr>
        <% }
            } %>
    </tbody>
</table>

    </div>

    <% } %>
        </div>
    </form>
</body>
</html>
