﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FacilityReportCardPrint.aspx.cs" Inherits="FacilityReportCardPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%--Report--%></title>
    <link href="Content/css/AltarumWebApp.css" rel="stylesheet" />
    <style>
        @media print {
            header, footer {
                display: none;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="max-height: 100%" class="printFRCs">
            <table cellpadding="0" cellspacing="0" id="tableFRCDetails" border="1" class="facilityTable secondtablebackgroud">
                <tr id="trprint">
                    <td colspan="2"><a href="#" onclick="printpage();">
                        <img src="Content/css/images/print.png" class="printlogo" /></a> </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Content/css/images/frcsReportLogo.png" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <p>
                            <b><%=DateTime.Now.ToString("G") %>
                                <br />
                                <br />
                                This report provides data on your reporting facility’s performance measures regarding birth defects reports.
                     This report card provides this quarter’s data quality measures in timeliness, accuracy, and completeness.
                                <br />
                                <br />
                                Michigan Birth Defects reporting is required monthly, between the 10th and the 15th of each month, for the previous month’s data.
                     Monthly submissions prior to the 10th day of the month may require re-submission.
                     Monthly submissions submitted after the 15th of each month is a breach of MDHHS/MBDR compliance and may result in disciplinary actions.
                            </b>
                        </p>
                    </td>
                </tr>
                <tr class="secondtablefirsttr">
                    <td colspan="2">Facility Report Card For: </td>
                </tr>
                <tr class="secondtablefirsttr">
                    <td colspan="2">
                        <% if (!string.IsNullOrEmpty(selectedOrg))
                            {%>

                        <%=dtFRCStats.Rows[0]["submitter"] %>
                        <%}
                            else
                            { %>
                        <%=selectedFacilityText%>
                        <%}%>
                    </td>
                </tr>

                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">Year</td>
                    <td><%=selectedYear %></td>
                </tr>
                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">Month</td>
                    <td><%= System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(dtFRCStats.Rows[0]["month"])) %> </td>
                </tr>
                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">File Name</td>
                    <td><%=dtFRCStats.Rows[0]["fileName"] %></td>
                </tr>
                <tr style="background-color: white">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>File Created Date</td>
                    <td><%=dtFRCStats.Rows[0]["fileCreatedDate"] %></td>
                </tr>
                <tr>
                    <td>Days Early/Late Of Submission</td>
                    <td><%=dtFRCStats.Rows[0]["daysEarlyOrLateOfSubmission"] %></td>
                </tr>
                <tr>
                    <td>Load Date</td>
                    <td><%=dtFRCStats.Rows[0]["loadDate"] %></td>
                </tr>
                <tr>
                    <td>Date Of Last File (If Delinquent)</td>
                    <td><%=dtFRCStats.Rows[0]["dateOfLastFileIfDelinquent"] %></td>
                </tr>
                <tr>
                    <td>Days Since Last File (If Delinquent)</td>
                    <td><%=dtFRCStats.Rows[0]["daysSinceLastFileIfDelinquent"] %></td>
                </tr>
                <tr>
                    <td>Reports Count</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCount"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Prior Month</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountPriorMonth"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Monthly Avg</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountMonthlyAvg"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Percent Change Vs Monthly Avg</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountPercentChangeVsMonthlyAvg"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Invalid</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountInvalid"] %></td>
                </tr>
                <tr>
                    <td>Reports Invalid Percent
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsInvalidPercent"] %></td>
                </tr>
                <tr>
                    <td>Data Errors Count
                    </td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsCount"] %></td>
                </tr>
                <tr>
                    <td>Data Errors Percent
                    </td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsPercent"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Duplicates
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountDuplicates"] %></td>
                </tr>
                <tr>
                    <td>Reports Count VERA
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountVERA"] %></td>
                </tr>
                <tr>
                    <td>File Load Failure</td>
                    <td><%=dtFRCStats.Rows[0]["fileLoadFailure"] %></td>
                </tr>
                <tr>
                    <td>Keep up the great work!</td>
                    <td><b><%=DateTime.Now.ToString("G") %></b><br />
                        <%=Session["DisplayName"] %></td>
                </tr>
                <tr>
                    <td colspan="2"><b>Comments</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" id="tblFrcComment">
                            <%  if (dtFRCStatsComment != null)
                                {
                                    foreach (System.Data.DataRow dr in dtFRCStatsComment.Rows)
                                    {%>
                                        <tr>
                                            <td class="propertyFRCCommenttable"><%=dr["comment"] %></td>
                                            <td class="propertyFRCCommenttable"><%=dr["created"] %>
                                                <br />
                                                <%=dr["displayName"] %></td>
                                        </tr>
                                    <%}
                                }%>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <div class="marginLeft50 pt-1">
                <b>If you have any questions or concerns, please email us at <a href="mailto:MDHHS-MBDR@Michigan.gov" class="blueColor" target="_blank">MDHHS-MBDR@Michigan.gov.</a><br />
                    <br />

                    Visit our website <a href="http://www.michigan.gov/MBDR" class="blueColor" target="_blank">http://www.michigan.gov/MBDR </a>for additional information.
                </b>
                <br />
                <br />
            </div>

        </div>
    </form>
    <script>
        function printpage() {
            document.getElementById("trprint").style.display = "none";
            window.print();
            document.getElementById("trprint").style.display = "block";
        }
    </script>
</body>
</html>
