﻿<%@ Page Title="MBDR Error Logs" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="errorLogs.aspx.cs" Inherits="errorLogs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <h1 class="page-title"><img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> Activity and Error Logs </h1>
        <% foreach (BDRTablesList Names in BDRTables) { %>
            <div class="col col-lg-6 col-md-6 col-sm-9">
                <div class="program">
                    <% if (Names.SCHName == "MBDR_Soft_Edit" && (Names.DBName == "vw_DMC_Soft_Edits_Listing")) {  %>
                        <a href="/DefaultErrorView.aspx?schema=<%= Names.SCHName %>&table=<%= Names.DBName %>&name=<%= Names.Name %>">
                    <% } else if (Names.SCHName == "MBDR_System" && (Names.DBName == "FileLoadInformation")) { %>
                        <a href="/FileInfoDataView.aspx?schema=<%= Names.SCHName %>&table=<%= Names.DBName %>&name=<%= Names.Name %>">
                     <% } else if (Names.SCHName == "MBDR_System" && (Names.DBName == "FileLoadInfo")) { %>
                        <a href="/FileSubmissionLog.aspx?schema=<%= Names.SCHName %>&table=<%= Names.DBName %>&name=<%= Names.Name %>" onclick="return ShowProgress();">
                     <% } else if (Names.SCHName == "MBDR_System" && (Names.DBName == "ValidationFailures")) { %>
                        <a href="/FileSubmissionLog.aspx?schema=<%= Names.SCHName %>&table=<%= Names.DBName %>&name=<%= Names.Name %>" onclick="return ShowProgress();" >
                     <% }else { %>   
                    <% } %>
                        <div class="program-header">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <div class="program-name">
                                <%= Names.Name %>
                            </div>
                        </div>
                    <% if (Names.SCHName == "MBDR_Soft_Edit" && Names.DBName == "vw_DMC_Soft_Edits_Listing") {  %>
                        </a>
                    <% } else if (Names.SCHName == "MBDR_System" && Names.DBName == "FileLoadInformation") { %>
                        </a>
                    <% } else { %>
                    <% } %>

                    <div class="program-description">
                        <%= Names.Desc %>
                    </div>
                </div>	
            </div>
        <% } %>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
      <br />
    </div>

    <script>
        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ShowProgress(para) {

            var modal = $('<div />');
            modal.addClass("modalspin");
            $('body').append(modal);
            var loading = $(".loadingspin");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ "position": "center", top: top, left: left });

            return true;
        }
    </script>
</asp:Content>


