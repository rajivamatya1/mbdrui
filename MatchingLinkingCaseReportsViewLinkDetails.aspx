﻿<%@ Page Title="Linked Case Reports Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="MatchingLinkingCaseReportsViewLinkDetails.aspx.cs" Inherits="DynamicGridView.MatchingLinkingCaseReportsViewLinkDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
    #primary-icon, #divGreen, #divRed, div.logo, img.bell-icon, #cVer
    {
     display:none;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">


    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataRowId" Value="" />
            <asp:HiddenField ID="hdfSchema" runat="server" />
            <asp:HiddenField ID="hdfTableName" runat="server" />
            <asp:HiddenField ID="hdfKeyName" runat="server" />
            <asp:HiddenField ID="hdfRowID" runat="server" />
            <asp:HiddenField ID="hdfSearch" runat="server" />
            <asp:HiddenField ID="hdfColumnName" runat="server" />
            <asp:HiddenField ID="hdfScrutinizerId" runat="server" />
            <asp:HiddenField ID="hdfCaseRecId" runat="server" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>

    <br />
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
