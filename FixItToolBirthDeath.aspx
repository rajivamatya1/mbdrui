﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FixItToolBirthDeath.aspx.cs" Inherits="FixItToolBirthDeath" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FixitToolBirthDeath</title>
     <!-- CSS -->
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
    <link href="Content/css/MLTable.css" rel="stylesheet" />
    <link href="Content/css/AltarumWebApp.css" rel="stylesheet" />

    <!-- JS files -->
    <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    
</head>
<body id="mlsSecurityBody">
    <form id="form1" runat="server">
        <div>
            <% if (Session["username"] != null)
                { %>
            <div class="row" id="mlsSecurityBtns">
                <div class="col col-lg-6 col-md-6 col-sm-6 mlsourceMargin">
                    <asp:HiddenField ID="hdType" runat="server" />
                    <asp:HiddenField ID="hdfBirthId" runat="server" />
                    <asp:HiddenField ID="hdfDeathId" runat="server" />
                    <asp:HiddenField ID="hdfProceedChkBirth" runat="server" />
                    <asp:HiddenField ID="hdfProceedChkDeath" runat="server" />

                    <div class="col col-lg-12 col-md-12 col-sm-12">
                        <div class="comparison-buttons-container">
                            <div class="form-item-ml">
                                <strong>Compare with Source: </strong>
                                <asp:Button Text="Birth Records" runat="server" ID="btnBirthRecords" OnClick="btnBirthRecords_Click" CssClass="btn btn-primary" />
                                <asp:Button Text="Death Records" runat="server" ID="btnDeathRecords" OnClick="btnDeathRecords_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-6">
                    <div class="comparison-buttons-container">
                                               
                        <asp:Button Text="Proceed" runat="server" ID="btnProceed" OnClientClick="return ShowProgress(this);" OnClick="btnProceed_Click" CssClass="btn btn-primary grey-color" />
                        <button id="btnCancel" onclick="closewindow();" class="btn btn-primary">Cancel</button>

                    </div>
                </div>

            </div>
            <br />
            <div class="wrapperFixIt">
                <%--horizontal scroll main class--%>

                <div class="scroll-wrapper" id="top-scroll"></div>
                <%--horizontal top - scroll --%>
                <div class="table-container-fix">
                    <%-- add class for horizontal scroll bars --%>
                    <table id="fixitTable" class="fititTable" border="1" cellspacing="0">
                        <% if (dtFixITBirthData != null && dtFixITBirthData.Rows.Count > 0)
                            {%>
                        <%-- Header Name Display Start--%>
                        <thead>
                            <tr>
                                <th class="colorRowColLabel sticky-fixitFirstColumn"></th>
                                <% for (int i = 0; i < dtFixITBirthData.Rows.Count; i++)
                                    {
                                        if (Convert.ToString(dtFixITBirthData.Rows[i][0]) == "Most Recent Report")
                                        {%>
                                <th class="caseReports colorRowReports sticky-fixitsecondColumn case-<%=dtFixITBirthData.Rows[i][29]%>"><%=dtFixITBirthData.Rows[i][0]%></th>
                                <%}
                                    else
                                    {
                                %>
                                <th class="colorRowReports caseReports case-<%=dtFixITBirthData.Rows[i][29]%>" style="left: 0px; text-align: left; position: static;"><%=dtFixITBirthData.Rows[i][0] %></th>
                                <%  }
                                }%>
                            </tr>
                        </thead>
                        <%-- Header Name Display End--%>
                        <%-- Check Box Display Start--%>
                        <tbody id="table-body">
                            <tr style="background-color: #eceef1;">
                                <td class="colorRowColLabel sticky-fixitFirstColumn">


                                    <div style="display:flex; align-items: flex-end;">
                                    <%
                                        string isProceedChecked = string.Empty;
                                        if (hdType.Value == "birth")
                                        {
                                            isProceedChecked = hdfProceedChkBirth.Value;
                                            if (string.IsNullOrEmpty(isProceedChecked))
                                            {
                                                isProceedChecked = "false";
                                            }
                                    %>
                                    <input type="checkbox" id="chkProceedBirth" name="chkProceed" onclick="handleProceedCheck(this,'birth');" <%= isProceedChecked == "true" ? "checked" : "" %> />
                                   <label for="chkProceedBirth" style="margin-left: 8px;">Exclude Birth Record</label> 
                                    <%
                                        }
                                        else
                                        {
                                            isProceedChecked = hdfProceedChkDeath.Value;
                                            if (string.IsNullOrEmpty(isProceedChecked))
                                            {
                                                isProceedChecked = "false";
                                            }
                                    %>
                                    <input type="checkbox" id="chkProceedDeath" name="chkProceed" onclick="handleProceedCheck(this,'death');" <%= isProceedChecked == "true" ? "checked" : "" %> />
                                    <label for="chkProceedDeath" style="margin-left: 8px;">Exclude Death Record</label>
                                    <%} %></div>


                                </td>

                                <%
                                    int j = 0;                                    
                                    string elementCheckId = string.Empty;
                                    bool isBirthDeathChecked = false;

                                    for (int i = 0; i < dtFixITBirthData.Rows.Count; i++)
                                    {
                                        string checkBoxType = Convert.ToString(dtFixITBirthData.Rows[i][0]);
                                        string checkboxId = "chkId" + i;
                                        string onClickFunction;
                                        string checkboxValue;
                                        string cssclass = "";                                        
                                        string caseReportId = "";
                                        string isBirthDeathDisabled = string.Empty;
                                        string type = string.Empty;
                                        bool isCheckedBirthandDeathIdLevel = false;
                                        if (checkBoxType == "Most Recent Report")
                                        {
                                            j = i;
                                            %>
                                            <td class="colorColCaseRecords sticky-fixitsecondColumn">&nbsp;
                                            </td>
                                            <%}
                                        else
                                        {
                                            if (checkBoxType.Contains("Birth"))
                                            {
                                                type = "birth";
                                                onClickFunction = "handleReportClick(this,'birth')";
                                                if (!string.IsNullOrEmpty(hdfBirthId.Value) && !string.IsNullOrEmpty(Convert.ToString(dtFixITBirthData.Rows[i][29])))
                                                {
                                                    if (Convert.ToString(dtFixITBirthData.Rows[i][29]) == Convert.ToString(hdfBirthId.Value))
                                                    {
                                                        isCheckedBirthandDeathIdLevel = true;
                                                        isBirthDeathChecked = true;
                                                        elementCheckId = checkboxId;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                type = "death";
                                                onClickFunction = "handleReportClick(this,'death')";
                                                if (!string.IsNullOrEmpty(hdfDeathId.Value) && !string.IsNullOrEmpty(Convert.ToString(dtFixITBirthData.Rows[i][29])))
                                                {
                                                    if (Convert.ToString(dtFixITBirthData.Rows[i][29]) == Convert.ToString(hdfDeathId.Value))
                                                    {
                                                        isCheckedBirthandDeathIdLevel = true;
                                                        isBirthDeathChecked = true;
                                                        elementCheckId = checkboxId;
                                                    }
                                                }
                                            }

                                            checkboxValue = Convert.ToString("report_" + dtFixITBirthData.Rows[i][29]);
                                            caseReportId = Convert.ToString(dtFixITBirthData.Rows[i][3]);
                                            if (!string.IsNullOrEmpty(Convert.ToString(dtFixITBirthData.Rows[i][29])))
                                            {
                                            %>

                                            <td class="caseReports case-<%=dtFixITBirthData.Rows[i][29]%>">
                                                <input type="checkbox" id="<%=checkboxId %>" data-case-id="<%=dtFixITBirthData.Rows[i][29] %>"
                                                    data-case-reportid="<%=caseReportId %>" class="caseReport chkHeader  <%=cssclass %>"
                                                    name="chkname" onclick="<%= onClickFunction %>" <%= isCheckedBirthandDeathIdLevel == true ? "checked" : "" %> value="<%=checkboxValue %>" />
                                            </td>
                                            <%}
                                            else
                                            {  %>
                                            <td></td>
                                            <%}

                                                        if (isBirthDeathChecked && i == (dtFixITBirthData.Rows.Count - 1))
                                                        {
                                                            string elementId = elementCheckId;
                                                            string script = $"handleCheckBox(document.getElementById('{elementId}'));";
                                                            Page.ClientScript.RegisterStartupScript(this.GetType(), "handle Record Click", script, true);
                                                        }
                                                    }
                                                }%>
                            </tr>
                            <%-- Check Box Display End--%>
                            <%-- Details Display Start--%>

                            <% 
                                for (int i = 1; i < dtFixITBirthData.Columns.Count; i++)
                                {
                                    if (i < 28)
                                    {%>
                            <tr>

                                <td class="colorRowColLabel sticky-fixitFirstColumn"><%=dtFixITBirthData.Columns[i].ColumnName %></td>
                                <% 
                                    var index = 0;

                                    foreach (System.Data.DataRow item in dtFixITBirthData.Rows)
                                    {

                                        if (Convert.ToString(item[0]) == "Most Recent Report" && index == 0)
                                        {
                                            if (dtFixITBirthData.Columns[i].ColumnName != "Scrutinizer ID"
                                               && dtFixITBirthData.Columns[i].ColumnName != "Case ID" && dtFixITBirthData.Columns[i].ColumnName != "Birth Death ID")
                                            {%>
                                <td class="caseReports colorColCaseRecords sticky-fixitsecondColumn case-<%=item[29]%>"><%=item[i] %></td>
                                <%}
                                    else
                                    {%>
                                <td class="caseReports colorColCaseRecords sticky-fixitsecondColumn case-<%=item[29]%>"></td>
                                <%}
                                    }
                                    else if ((Convert.ToString(item[0]) == "Potential Birth" || Convert.ToString(item[0]) == "Potential Death") && dtFixITBirthData.Columns[i].ColumnName == "Case Record ID")
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(item[i])))
                                        { %>
                                <td class="caseReports  classPinkCell"><%=item[i] %></td>
                                <%
                                    }
                                    else
                                    {%>
                                <td class="caseReports">&nbsp;</td>
                                <%}
                                    }
                                    else if (dtFixITBirthData.Columns[i].ColumnName != "Scrutinizer ID"
                                            && dtFixITBirthData.Columns[i].ColumnName != "Case Report ID"
                                            && dtFixITBirthData.Columns[i].ColumnName != "Case ID" && dtFixITBirthData.Columns[i].ColumnName != "Birth Death ID"
                                    )
                                    {%>
                                <td class="caseReports case-<%=item[29]%>"><%=item[i] %></td>

                                <%}
                                    else
                                    {%>
                                <td class="caseReports case-<%=item[29]%>"></td>

                                <%}
                                        index++;
                                    }%>
                            </tr>
                            <%
                                    }
                                }%>
                        </tbody>
                        <%} %>
                    </table>
                </div>
                <div id="bottom-scroll" class="scroll-wrapper" style="display: none"></div>
                <%-- bottm top - scroll --%>
            </div>
        </div>

        <div class="loadingspin" align="center">
            <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
            <br />
            Loading ... Please wait ...
                    <br />
        </div>

        <div id="errorMsgs">
            <p id="errorMsgsForAll"></p>
        </div>
        <div id="socketConnError" class="socketConnErrorModal">
            <p id="connErrorMsg"></p>
        </div>

        <div id="socketConn">
            <p id="connSuccessMsg"></p>
        </div>

           <div id="securityMsg">
</div>

        <% } %>

        <script type="text/javascript">   

            function closewindow() {

                if (window.parent && typeof window.parent.closeModelPopUp == "function") {
                    window.parent.closeModelPopUp();
                }
            }

            function loadtimeproceeddisable() {
                const btnProcess = document.getElementById("<%= btnProceed.ClientID %>");
                btnProcess.disabled = "true";
            }

            /*start js function for scroll bar horizontally top and bottom*/

            $(document).ready(function () {
                const $topScroll = $('#top-scroll');
                const $bottomScroll = $('#bottom-scroll');
                const $tableContainer = $('.table-container-fix');
                const $mainTable = $('#fixitTable');


                $topScroll.html('<div style="height:1px;"></div>');
                $bottomScroll.html('<div style="height:1px;"></div>');
                const $dummyScrollBarTop = $topScroll.find('div');
                const $dummyScrollBarBottom = $bottomScroll.find('div');

                function updateScrollBarWidths() {
                    const tableWidth = $mainTable.outerWidth();
                    $dummyScrollBarTop.css('width', `${tableWidth}px`);
                    $dummyScrollBarBottom.css('width', `${tableWidth}px`);
                }

                updateScrollBarWidths();
                $(window).on('resize', updateScrollBarWidths);

                $topScroll.on('scroll', function () {
                    const scrollLeft = $topScroll.scrollLeft();
                    $tableContainer.scrollLeft(scrollLeft);
                    $bottomScroll.scrollLeft(scrollLeft);
                })

                $bottomScroll.on('scroll', function () {
                    const scrollLeft = $bottomScroll.scrollLeft();
                    $tableContainer.scrollLeft(scrollLeft);
                    $topScroll.scrollLeft(scrollLeft);
                })

                $tableContainer.on('scroll', function () {
                    const scrollLeft = $tableContainer.scrollLeft();
                    $bottomScroll.scrollLeft(scrollLeft);
                    $topScroll.scrollLeft(scrollLeft);
                })
            });

            /*End js function for scroll bar horizontally top and bottom*/

            function closewindowandRedirect() {

                var urlParam = new URLSearchParams(window.location.search);
                var fixItId = urlParam.get("fixItId");
                if (window.parent && typeof window.parent.closeModelPopUp == "function") {
                    window.parent.closeModelPopUpRefresh(fixItId);
                }
            }

            function ShowProgress() {
                
                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });
            }

            function removeProgress() {
                var modal = $('div.modalspin');
                modal.removeClass("modalspin");
                var loading = $(".loadingspin");
                loading.hide();
            }

            function socketConnError() {

                $("#socketConnError").dialog({

                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    title: "Connection Error",

                    open: function () {

                        var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                        var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                        $(this).append(imageConnError, connErrorMsg);
                    },

                    buttons: [
                        {
                            text: "DISMISS",
                            open: function () {
                                $(this).addClass('okcls')
                            },
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ],

                    position: {
                        my: "center center",
                        at: "center center"
                    }
                }).prev(".ui-dialog-titlebar").css("background", "darkred");;
            }

            function handleReportClick(reportCheckbox, type) {

                const caseId = reportCheckbox.dataset.caseId;
                var ischecked = false;
                var checkboxes = document.querySelectorAll("input[type=checkbox]");
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i] == reportCheckbox) {
                        if (checkboxes[i].checked) {
                            ischecked = true;
                            break;
                        }
                        else {
                            ischecked = false;
                        }
                    }
                }


                if (type == "birth" && ischecked) {
                    document.getElementById("<%=hdfBirthId.ClientID%>").value = caseId;
                }
                else if (!ischecked && type == "birth") {
                    document.getElementById("<%=hdfBirthId.ClientID%>").value = null;
                }
                else if (!ischecked && type == "death") {
                    document.getElementById("<%=hdfDeathId.ClientID%>").value = null;
                }
                else {
                    document.getElementById("<%=hdfDeathId.ClientID%>").value = caseId;
                }

                // CheckBox Status Maintain

                for (var i = 0; i < checkboxes.length; i++) {
                    if (ischecked) {
                        if (!checkboxes[i].checked) {
                            checkboxes[i].disabled = true;
                        }
                    }
                    else {
                        var hdfBID = document.getElementById("<%=hdfBirthId.ClientID%>").value;
                        var hdfDID = document.getElementById("<%=hdfDeathId.ClientID%>").value;

                        if ((hdfBID || hdfDID) && checkboxes[i].id == "chkProceed") {
                            checkboxes[i].disabled = true;


                        }
                        else {
                            checkboxes[i].disabled = false;
                            
                        }
                    }
                }

                // Proceed Button Status

                btnProcessEnableDisable();

            }

            function btnProcessEnableDisable() {
                const btnProcess = document.getElementById("<%= btnProceed.ClientID %>");
                var urlParam = new URLSearchParams(window.location.search);
                var checkname = urlParam.get("checkname");
                var hdfBID = document.getElementById("<%=hdfBirthId.ClientID%>").value;
                var hdfDID = document.getElementById("<%=hdfDeathId.ClientID%>").value;
                var hdfProccedBChk = document.getElementById("<%=hdfProceedChkBirth.ClientID%>").value;
                var hdfProccedDChk = document.getElementById("<%=hdfProceedChkDeath.ClientID%>").value;
                
                if (checkname === "CHECK_BOTH") {
                    if ((hdfBID && hdfDID) || (hdfProccedBChk == "true" && hdfProccedDChk == "true") || (hdfBID && hdfProccedDChk == "true") || (hdfDID && hdfProccedBChk == "true")) {
                        btnProcess.disabled = false;
                        btnProcess.className = btnProcess.className.replace("grey-color", "").trim();
                    }
                    else {
                        btnProcess.disabled = true;
                        if (!btnProcess.className.includes("grey-color")) {
                            btnProcess.className += " grey-color";
                        }
                    }
                }
                else if (checkname === "CHECK_BIRTH")
                {
                    if (hdfBID || hdfProccedBChk == "true") {
                        btnProcess.disabled = false;
                        btnProcess.className = btnProcess.className.replace("grey-color", "").trim();
                       
                    }
                    else {
                        btnProcess.disabled = true;

                        if (!btnProcess.className.includes("grey-color")) {
                            btnProcess.className += " grey-color";
                        }
                    }
                }
                else {
                    if (hdfDID || hdfProccedBChk == "false") {
                        btnProcess.disabled = false;
                        btnProcess.className = btnProcess.className.replace("grey-color", "").trim();
                    }
                    else {
                        btnProcess.disabled = true;

                        if (!btnProcess.className.includes("grey-color")) {
                            btnProcess.className += " grey-color";
                        }
                    }
                }
            }

            function handleCheckBox(reportCheckbox) {
                const caseId = reportCheckbox.dataset.caseId;
                var ischecked = false;

                var checkboxes = document.querySelectorAll("input[type=checkbox]");
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i] == reportCheckbox) {
                        if (checkboxes[i].checked) {
                            ischecked = true;
                            break;
                        }
                        else {
                            ischecked = false;
                        }
                    }
                }

                for (var i = 0; i < checkboxes.length; i++) {
                    if (ischecked) {
                        if (!checkboxes[i].checked) {
                            checkboxes[i].disabled = true;
                        }
                    }
                    else {
                        checkboxes[i].disabled = false;
                    }
                }

                btnProcessEnableDisable();
            }

            function handleProceedCheck(proceedCheckbox,type) {
                
                const allRecords = document.querySelectorAll('.caseReport');

                if (proceedCheckbox.checked) {
                    if (type == "birth") {
                        document.getElementById("<%=hdfProceedChkBirth.ClientID %>").value = "true";
                    }
                    else {
                        document.getElementById("<%=hdfProceedChkDeath.ClientID %>").value = "true";
                    }
                    allRecords.forEach(record => {
                        record.disabled = true;
                    });

                }
                else {
                    if (type == "birth") {
                        document.getElementById("<%=hdfProceedChkBirth.ClientID %>").value = "false";
                    }
                    else {
                        document.getElementById("<%=hdfProceedChkDeath.ClientID %>").value = "false";
                    }
                    allRecords.forEach(record => {
                        record.disabled = false;
                    });


                }
                btnProcessEnableDisable();
            }

            function DisableProcessed(type) {

                if (type == "birth") {
                    var birthProcecedCheckbox = document.getElementById("chkProceedBirth");
                    if (!birthProcecedCheckbox.checked) {
                        document.getElementById("chkProceedBirth").disabled = true;
                    }
                }
                else {
                    var deathProcecedCheckbox = document.getElementById("chkProceedDeath");
                    if (!deathProcecedCheckbox.checked) {
                        document.getElementById("chkProceedDeath").disabled = true;
                    }
                }


            }

            function errorMsgs(message, titleFor) {
                
                $("#errorMsgs").dialog({

                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    title: "Search Result",

                    open: function () {

                        var connMsg = message;
                        $(this).append(connMsg);
                    },

                    buttons: [
                        {
                            text: "Ok",
                            open: function () {
                                $(this).addClass('okcls')
                            },
                            click: function () {
                                $(this).dialog("close");
                                closewindowandRedirect();
                            }
                        }
                    ],
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    closeOnEscape: false
                }).prev(".ui-dialog-titlebar").css("background", "#00607F");


            }

            function sViolationDC(message, titleFor) {

                var headerContent = document.getElementById("mlsSecurityBtns");

                if (headerContent) {
                    headerContent.style.display = "none";
                }

                $("#securityMsg").dialog({

                    width: 450,
                    modal: true,
                    dialogClass: "no-close",

                    open: function () {
                        var connMsg = message;
                        $(this).append(connMsg);
                    },

                    buttons: [
                        {
                            text: "Ok",
                            open: function () {
                                $(this).addClass('okcls')
                            },
                            click: function () {
                                redirectDefault();
                            }
                        }
                    ],
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    closeOnEscape: false
                }).prev(".ui-dialog-titlebar").css("background", "#00607F");

                if (titleFor) {
                    $(".ui-dialog-title").text(titleFor);
                }

                $(".ui-dialog-titlebar-close").on("click", function () {
                    redirectDefault();
                });

                $(document).on("keydown", function (e) {
                    if (e.which === 27) {
                        redirectDefault();
                    }
                });
            }

            function redirectDefault() {
               
                $(".ui-dialog-content").dialog("destroy");
                $(".ui-dialog").remove();

                setTimeout(function () {
                    if (window.top !== window.self) {
                        window.top.location.href = "/Default.aspx";
                    } else {
                        window.open('', '_self', '');
                        window.close();

                        setTimeout(function () {
                            window.location.href = "/Default.aspx";
                        }, 400);
                    }
                }, 300);
            }
        </script>
    </form>
</body>
</html>
