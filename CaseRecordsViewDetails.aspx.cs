﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class CaseRecordsViewDetails : System.Web.UI.Page
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
            string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
            string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";
            string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
            string birthRecId = !string.IsNullOrEmpty(Request.QueryString["masterRecordNumber"]) ? Convert.ToString(Request.QueryString["masterRecordNumber"]) : "";
            string deathRecId = !string.IsNullOrEmpty(Request.QueryString["deathNumber"]) ? Convert.ToString(Request.QueryString["deathNumber"]) : "";
            string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
            string searchText = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
            string columnName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
            string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
            string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
            string facility = !string.IsNullOrEmpty(Request.QueryString["facility"]) ? Convert.ToString(Request.QueryString["facility"]) : "";
            string searchType = !string.IsNullOrEmpty(Request.QueryString["SearchType"]) ? Convert.ToString(Request.QueryString["SearchType"]) : "";
            string whereClause = !string.IsNullOrEmpty(Request.QueryString["where"]) ? Convert.ToString(Request.QueryString["where"]) : "";
            if (!IsPostBack)
            {
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                hdfSearch.Value = searchText;
                hdfColumnName.Value = columnName;
                hdfName.Value = name;
                hdfKeyName.Value = keyName;
                hdfRowID.Value = caseId;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                hdfFromDate.Value = fromDate;
                hdfToDate.Value = toDate;
                hdfChangeFacility.Value = facility;
                hdfBirthId.Value = birthRecId;
                hdfDeathId.Value = deathRecId;
                hdfCaseID.Value = caseId;
                hdfCaseRecId.Value = caseRecId;

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    BindColumnsDropDown(schema, tableName);
                    BindGrid(tableName, schema, keyName, caseId);
                }
            }

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_Page_Load");
        }

        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dtDropDown = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtDropDown);
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                }

                foreach (DataColumn item in dtDropDown.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }
                    }
                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_BindColumnsDropDown");
        }
    }

    public void BindGrid(string tableName, string schema, string keyName, string rowID)
    {
        try
        {
            string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            List<ColumnList> lstRecord = new List<ColumnList>();
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
                List<string> lstColumns = includeColumns.Split(',').ToList();
                DataTable dtActualRow = new DataTable();
                DataSet dsRows = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    DataTable dataTable = new DataTable();
                    int row = Convert.ToInt32(rowID);
                    string query = "";

                    string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

                    string whereSep = getWhereClause();
                    
                    query = string.Format("select caseid, purgeId, caseRecId from {0}.{1} {2} order by {3} ", schema, tableName, whereSep, sortExp);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dataTable);

                    /* Checking if Case Record Id is Archieved or not */

                    if(dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow checkRowPurge in dataTable.Rows)
                        {
                            if (checkRowPurge["purgeId"] != DBNull.Value && !string.IsNullOrEmpty(checkRowPurge["purgeId"].ToString()))
                            {
                                ltrTableName.Text = string.Empty;
                                hdfCaseRecId.Value = Convert.ToString(checkRowPurge["caseRecId"]);
                                ltrTableName.Text = "Case Record Details for Case " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + hdfCaseRecId.Value + " (Archived)" + "</span></b>";

                                btnCaseReport.Enabled = false;

                                btnCaseReport.CssClass = btnCaseReport.CssClass.Replace("disableGreyCaseReport", "").Trim();
                                btnCaseReport.CssClass += " enableGreyCaseReport";

                            }
                            else
                            {
                                ltrTableName.Text = string.Empty;
                                hdfCaseRecId.Value = Convert.ToString(checkRowPurge["caseRecId"]);
                                ltrTableName.Text = "Case Record Details for Case " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + hdfCaseRecId.Value + "</span></b>";

                                btnCaseReport.Enabled = true;

                                btnCaseReport.CssClass = btnCaseReport.CssClass.Replace("enableGreyCaseReport", "").Trim();
                                btnCaseReport.CssClass += " disableGreyCaseReport";
                            }
                        }

                    }
                }
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    int row = Convert.ToInt32(rowID);
                    string query = "";
                    query = string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, rowID);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row + 1);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row - 1);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dsRows);
                }

                dtActualRow = dsRows.Tables[0];
                string compareQuery = "";
                DataSet dsTables = new DataSet();
                dt = new DataTable();
                dt.Columns.Add("Columns");
                dt.Columns.Add(model.Table.Split(':')[1]);
                List<string> lstNavigation = new List<string>();
                if (model.ComparisonTables.Count > 0)
                {
                    if (!model.ComparisonTables.Any(s => s.TableName == ""))
                    {
                        foreach (var item in model.ComparisonTables)
                        {
                            if (!string.IsNullOrEmpty(item.TableName))
                            {
                                string[] lstTableNames = item.TableName.Split(':');
                                string key = item.FKey.Split(':')[0];
                                string key1 = item.FKey.Split(':')[1];
                                if (!string.IsNullOrEmpty(lstTableNames[1]))
                                {
                                    lstNavigation.Add(lstTableNames[1]);
                                    dt.Columns.Add(lstTableNames[1]);
                                }
                                else
                                {
                                    lstNavigation.Add(lstTableNames[0]);
                                    dt.Columns.Add(lstTableNames[0]);
                                }
                                compareQuery = compareQuery + " select * from " + item.Schema + "." + lstTableNames[0] + " where " + key + " = '" + dtActualRow.Rows[0][key1] + "'";
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(compareQuery))
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand(compareQuery, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        for (int i = 0; i < lstNavigation.Count; i++)
                        {
                            if (i == 0)
                            {
                                da.TableMappings.Add("Table", lstNavigation[i]);
                            }
                            else
                            {
                                da.TableMappings.Add("Table" + (i) + "", lstNavigation[i]);
                            }
                        }
                        da.Fill(dsTables);
                    }
                }
                if (dtActualRow.Rows.Count > 0)
                {
                    foreach (var column in lstColumns)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {

                            foreach (DataColumn item in dtActualRow.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = column.Contains("->") ? column.Split('>')[1] : column;

                                DataRow dr = dt.NewRow();
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                    string displayColname = string.Empty;
                                    if (!string.IsNullOrEmpty(colName))
                                    {
                                        dr[0] = columnInfo;
                                        displayColname = columnInfo;
                                    }
                                    else
                                    {
                                        dr[0] = item.ColumnName;
                                        displayColname = item.ColumnName;
                                    }
                                    if (displayColname != "RecordFrom")
                                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]) });
                                    dr[model.Table.Split(':')[1]] = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]);
                                    for (int j = 0; j < lstNavigation.Count; j++)
                                    {
                                        try
                                        {
                                            string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(lstNavigation[j])).FirstOrDefault().ColumnMapping;
                                            if (colMapping.Contains(item.ColumnName))
                                            {
                                                string strName = Common.GetBetween(colMapping, item.ColumnName, ",");
                                                dr[lstNavigation[j]] = Convert.ToString(dsTables.Tables[lstNavigation[j]].Rows[0][strName.Split(':')[1]]);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dr[lstNavigation[j]] = "";
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(hdfSearchText.Value))
                                    {
                                        if (item.ColumnName == hdfSearchText.Value)
                                        {
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                    else
                                    {
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            hdfCaseRecId.Value = Convert.ToString(dt.Rows[0][1]);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                btnReset_Click(this, EventArgs.Empty);
            }
        }
    }

    private string getWhereClause()
    {        
        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string zipcode = !string.IsNullOrEmpty(Request.QueryString["zipcode"]) ? Convert.ToString(Request.QueryString["zipcode"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
        string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
        string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
        string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";
        string birthRecId = !string.IsNullOrEmpty(Request.QueryString["masterRecordNumber"]) ? Convert.ToString(Request.QueryString["masterRecordNumber"]) : "";
        string deathRecId = !string.IsNullOrEmpty(Request.QueryString["deathNumber"]) ? Convert.ToString(Request.QueryString["deathNumber"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

        string whereSep = "where 1=1 ";

       if (!string.IsNullOrEmpty(fromDate))
        {
            if (!string.IsNullOrEmpty(toDate))
            {
                whereSep += " and createdTimestamp > ='" + fromDate + " 00:00:00'  and createdTimestamp < ='" + toDate + " 23:59:59'";
            }
            else
            {
                whereSep += " and createdTimestamp > ='" + fromDate + " 00:00:00' ";
            }
        }

        if (!string.IsNullOrEmpty(caseRecId))
        {
            whereSep += " and caseRecId ='" + caseRecId.Replace("'", "''").Trim() + "'";
        }
        if (!string.IsNullOrEmpty(caseId))
        {
            whereSep += " and caseId ='" + caseId.Replace("'", "''").Trim() + "'";
        }
        if (!string.IsNullOrEmpty(birthRecId))
        {

            whereSep += " and masterRecordNumber ='" + birthRecId.Replace("'", "''").Trim() + "'";
        }
        if (!string.IsNullOrEmpty(deathRecId))
        {

            whereSep += " and deathNumber ='" + deathRecId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childLastName) && !childLastName.Contains("*"))
        {
            whereSep += " and childLastName ='" + childLastName.Replace("'", "''").Trim() + "'"; ;
        }
        else if (childLastName.Contains("*"))
        {
            whereSep += " and childLastName like '" + childLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }
               
        if (!string.IsNullOrEmpty(childFirstName) && !childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName ='" + childFirstName.Replace("'", "''").Trim() + "'";
        }
        else if (childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName like '" + childFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(birthDate))
        {

            whereSep += " and birthDate > ='" + birthDate + " 00:00:00'  and birthDate < ='" + birthDate + " 23:59:59'";
        }

        if (!string.IsNullOrEmpty(address1) && !address1.Contains("*"))
        {

            whereSep += " and address1 = '" + address1.Replace("'", "''").Trim() + "'";
        }
        else if (address1.Contains("*"))
        {
            whereSep += " and address1 like '" + address1.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(city) && !city.Contains("*"))
        {
            whereSep += " and city = '" + city.Replace("'", "''").Trim() + "'";
        }
        else if (city.Contains("*"))
        {
            whereSep += " and city like '" + city.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(zipcode))
        {

            whereSep += " and zipcode ='" + zipcode.Replace("'", "''").Trim() + "'";
        }
        if (!string.IsNullOrEmpty(childSSN))
        {

            whereSep += " and childSSN ='" + childSSN.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momLastName) && !momLastName.Contains("*"))
        {

            whereSep += " and momLastName ='" + momLastName.Replace("'", "''").Trim() + "'";
        }
        else if (momLastName.Contains("*"))
        {
            whereSep += " and momLastName like '" + momLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momFirstName) && !momFirstName.Contains("*"))
        {

            whereSep += " and momFirstName ='" + momFirstName.Trim().Replace("'", "''") + "'";
        }
        else if (momFirstName.Contains("*"))
        {
            whereSep += " and momFirstName like '" + momFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momSSN))
        {

            whereSep += " and momSSN ='" + momSSN.Replace("'", "''").Trim() + "'";
        }
        return whereSep;
    }
    
    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#e3f4ff");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_gvDynamic_RowDataBound");
        }
    }
    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hdfSearchText.Value = Convert.ToString(ddlColumn.SelectedValue);
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnSearch_Click");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ddlColumn.SelectedIndex = 0;
            hdfSearchText.Value = "";
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnReset_Click");
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
          
            string whereSep = getQueryString();
            string sortExpre = "";
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "caseRecId desc";
            }
            else
            {
                sortExpre = sortExp;
            }
            int row = Convert.ToInt32(hdfPreviousRowID.Value);
            Response.Redirect("CaseRecordsViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&caseId=" + row + "&name=" + hdfName.Value + "&sortExp=" + sortExpre + "&where=" + whereSep + "&rowId=" + row + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnPrev_Click");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
            
            string whereSep = getQueryString();
            string sortExpre = "";
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "caseRecId desc";
            }
            else
            {
                sortExpre = sortExp;
            }           

            int row = Convert.ToInt32(hdfNextRowID.Value);
            Response.Redirect("CaseRecordsViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&caseId=" + row + "&name=" + hdfName.Value + "&sortExp=" + sortExpre + "&where=" + whereSep + "&rowId=" + row + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnNext_Click");
        }
    }

    protected void btnAll_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("CaseRecordsView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnAll_Click");
        }
    }
  
    protected void btnCaseReport_Click(object sender, EventArgs e) // Linked Case Reports
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
            string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";

            string whereSep = getQueryString();

            string sortExpre = "";
            
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "caseRecId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            Response.Redirect("CaseRecordsViewLinkDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + hdfRowID.Value + "&name=" + hdfName.Value + "&where=" + whereSep + "&sortExp=" + sortExpre + "&caseId=" + caseId + "",false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnCaseReport_Click");
        }
    }

    protected void btnAdvancedSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
            string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";

            string whereSep = getQueryString();
            string sortExpre = "";
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "caseRecId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            Response.Redirect("CaseRecordsView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&searchfrom=advance&colName=" + hdfColumnName.Value + "&where=" + whereSep + "&sortExp=" + sortExpre + "&caseId=" + caseId + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnAdvancedSearch_Click");
        }
    }

    protected void btnAdvancedAll_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("CaseRecordsView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&searchfrom=advance&colName=" + hdfColumnName.Value + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsViewDetails_btnAdvancedAll_Click");
        }
    }

    private string getQueryString()
    {
        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string zipcode = !string.IsNullOrEmpty(Request.QueryString["zipcode"]) ? Convert.ToString(Request.QueryString["zipcode"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
        string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
        string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
        string birthRecId = !string.IsNullOrEmpty(Request.QueryString["masterRecordNumber"]) ? Convert.ToString(Request.QueryString["masterRecordNumber"]) : "";
        string deathRecId = !string.IsNullOrEmpty(Request.QueryString["deathNumber"]) ? Convert.ToString(Request.QueryString["deathNumber"]) : "";
        
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

        string whereSep = "1";      
     
        if (!string.IsNullOrEmpty(caseRecId))
        {
            whereSep += "&caseRecId=" + caseRecId.Trim() + "";
        }
        if (!string.IsNullOrEmpty(birthRecId))
        {
            whereSep += "&masterRecordNumber=" + birthRecId.Trim() + "";
        }
        if (!string.IsNullOrEmpty(deathRecId))
        {
            whereSep += "&deathRecId=" + deathRecId.Trim() + "";
        }
        if (!string.IsNullOrEmpty(childLastName))
        {

            whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childFirstName))
        {

            whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(birthDate))
        {

            whereSep += "&birthDate=" + birthDate + "";
        }
        if (!string.IsNullOrEmpty(address1))
        {

            whereSep += "&address1=" + address1.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(city))
        {

            whereSep += "&city=" + city.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(zipcode))
        {

            whereSep += "&zipcode=" + zipcode.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childSSN))
        {

            whereSep += "&childSSN=" + childSSN.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(momLastName))
        {

            whereSep += "&momLastName=" + momLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(momFirstName))
        {

            whereSep += "&momFirstName=" + momFirstName.Trim().Replace("'", "''") + "";
        }
        if (!string.IsNullOrEmpty(momSSN))
        {

            whereSep += "&momSSN=" + momSSN.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(fromDate))
        {
            whereSep += "&fromDate=" + fromDate + "";
        }
        if (!string.IsNullOrEmpty(toDate))
        {
            whereSep += "&toDate=" + toDate + "";
        }

        return whereSep;
    }

}