﻿<%@ Page Title="Case Verification" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="VerificationDetails.aspx.cs" Inherits="DynamicGridView.VerificationDetails"   EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <%--<script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>

    <style>

        .gvCCHD{
            width: auto ;
            margin: auto !important;         
            table-layout: fixed !important;
            border-collapse: collapse !important;
        }

              .gvCCHD th{
                    padding: 4px !important;
                    text-align: center !important;
                    border: 1px solid #ddd !important;
                }

             .gvCCHD td{
                    text-align: center !important;
                }

             .gvCCHD tr td:nth-child(5){
                    text-align: left !important;
                }

             .gvCCHD tr:nth-child(2) td:nth-child(5){
                    text-align: center !important;
                }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title" id="mlsSecuritylabel">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        <%--VERIFICATION Details --%>
        <asp:Literal Text="" ID="ltrTableName" runat="server" />
        
                                 <label style="text-align: left; margin-left: 25px; font-weight:600; margin-top: 4px;">
                                <%if (dt != null && dt.Rows.Count > 0 )
                                {  %>
                                Facility :  <%=dt.Rows[0][1] %>
                                <%} %>
                                     </label>
    </h1>

    <asp:HiddenField ID="hdfDataKeyName" runat="server" />
    <asp:HiddenField ID="hdfDiagnosisId" runat="server" />
    <asp:HiddenField ID="hdfStatusId" runat="server" />
    <asp:HiddenField ID="hdfUserName" runat="server" />
    <asp:HiddenField ID="hdfCCHDDiagnosis" runat="server" />
    <asp:HiddenField ID="hdfCCHDDiagnosisDate" runat="server" />
   <%-- <asp:Button ID="btnHiddenCHDSave" runat="server" OnClick="btnHiddenCHDSave_Click" Visible="false" />--%>

    <div class="row" id="mlsSecurityBody">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                               
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="action-buttons-container">
                                 <div class="form-item">
                                    <asp:Button Text="Return to Search Results" OnClick="btnAdvanceResult_Click" runat="server" ID="btnAdvanceResult" CssClass="btn btn-primary" OnClientClick="return ShowProgress();" />
                                    <asp:Button Text="New Search" OnClick="btnAll_Click" runat="server" ID="btnAll" CssClass="btn btn-primary" OnClientClick="return ShowProgress(); mySessionClear()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row row-no-padding">
                    <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                        <div class="tableFixHead" style="padding-left: 14px; padding-right: 14px;">
                            <table class="table table-bordered table-striped table-fixed" visible="true" rules="all" id="gvDynamic" style="border-collapse: collapse;" border="1" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="WhiteBlue-color">COLUMNS</th>
                                        <th class="veri-details">DETAILS</th>
                                </thead>

                                <tbody>

                                    <% if (dt != null && dt.Rows.Count > 0)                                 
                                        {%>

                                    <%
                                        List<string> columnsToDisplay = new List<string> { "caseRecId", "childLastName", "childFirstName", "childMiddleName", "birthDate", "childMRN", "admissionDate", "dischargeDate" };

                                        for (int J = 0; J < dt.Columns.Count; J++)
                                        {
                                            string columnName = dt.Columns[J].ColumnName;

                                            // skip unwanted data elements

                                            //if (string.Equals(columnName, "method", StringComparison.OrdinalIgnoreCase))
                                            if (!columnsToDisplay.Contains(columnName, StringComparer.OrdinalIgnoreCase))
                                            {
                                                continue;
                                            }

                                            // Display data elements

                                            string displayName = columnName;

                                            switch (columnName)
                                            {
                                                case "caseRecId":
                                                    displayName = "Case Rec ID";
                                                    break;

                                                case "childLastName":
                                                    displayName = "Child Last Name";
                                                    break;

                                                case "childFirstName":
                                                    displayName = "Child First Name";
                                                    break;

                                                case "childMiddleName":
                                                    displayName = "Child Middle Name";
                                                    break;

                                                case "birthDate":
                                                    displayName = "Child DOB";
                                                    break;

                                                case "childMRN":
                                                    displayName = "Child MRN";
                                                    break;

                                                case "admissionDate":
                                                    displayName = "Admission Date";
                                                    break;

                                                case "dischargeDate":
                                                    displayName = "Discharge Date";
                                                    break;
                                            }
                                    %>
                                    <tr>
                                        <td class="WhiteBlue-color"><%= displayName %></td>
                                        <td class="white-color">

                                            <% if (columnName == "caseRecId"){ %>
                                            
                                                    <% if (Convert.ToString(dt.Rows[0][columnName]) != null){ %>

                                                            <%-- Print Case Record ID --%>

                                                            <%= dt.Rows[0][columnName] %>

                                                            <%-- List of Linked Case Reports--%>

                                                            <input id="listLinkedCaseReports" type="image" src="Content/css/images/info-circle-fill.svg" title="View Linked Case Reports" onclick="return linkedCaseReports('LinkedCaseReportsList.aspx?Schema=MBDR_System&Table=CaseReportsView&keyName=caseId&caseRecId=<%= dt.Rows[0][columnName] %>')" target="_blank" />

                                                            <%--Linked Case Record Details--%>

                                                            <button id="btnLinkedCaseRecordDetails" title="Case Record Details" onclick="return linkedCaseRecordDetails('LinkedCaseRecordDetails.aspx?Schema=MBDR_System&Table=CaseRecordsView&keyName=caseId&caseRecId=<%= dt.Rows[0][columnName] %>')" target="_blank" >
                                                                <div class="link-icon">
                                                                    Case Record Details                                                                
                                                                </div>
                                                            </button>

                                                     <%}else {%>

                                                            <%--Leave Cell Blank--%>
                                                     <%}%>

                                            <%}else{%>

                                                    <%if (Convert.ToString(dt.Rows[0][columnName]) != null){ %>    
                                                            <%-- Print Values --%>

                                                            <%= dt.Rows[0][columnName] %></td>

                                                    <%}else {%>

                                                            <%--Leave Cell Blank--%>
                                                    <%}%>
                                            <%}%>
                                    </tr>

                                    <%}%>

                                    <%}
                                        else
                                        { %>
                                    <tr>
                                        <%--<td colspan="2">No data available</td>--%>
                                        <td colspan="2"></td>
                                    </tr>
                                    <%}%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <%--  CHD/CCHD Case Verification Table--%>

                <div class="row">
                    <div class="col col-lg-12 col-sm-12">
                        <div class="tableFixHead-verification" style="background: white;">
                            <asp:GridView ID="gvCCHDCaseVerification" runat="server" AutoGenerateColumns="false" ShowHeader="true" OnRowDataBound="gvCCHDCaseVerification_RowDataBound" OnRowCreated="gvCCHDCaseVerification_RowCreated"  OnRowCommand="gvCCHDCaseVerification_RowCommand" CssClass="gvCCHD">
                                <HeaderStyle CssClass="header-style" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="CHD Diagnosis Codes" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            </strong> <%# Eval("chdDiagnoses") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="CCHD Diagnosis Codes" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            </strong> <%# Eval("cchdDiagnoses") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="Prenatal CHD Indication" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            
                                            <asp:DropDownList runat="server" ID="ddlPrenatalCCHDDiagnosis" onchange="onDropDownChange();" CssClass="cursor-default" style="width: 85px;">
                                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Unknown" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="Date of Earliest CCHD Indication" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="dateCCHDIndic" runat="server" TextMode="Date" CssClass="cursor-pointerWhiteNewField" onchange="onDateChange();" >
                                            </asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Literal runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="btnSaveCCHD" CommandName="SaveCCHD" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Text="Save" ValidationGroup="save" CssClass="cursor-default" Enabled="false" />
                                              <asp:Label ID="cchdVdateNuser" Visible="false" Enabled="false" runat="server" Text='<%# Eval("verificationDate", "{0}, ") + Eval("submitterName") + (string.IsNullOrEmpty(Eval("savedInOtherReport").ToString()) ? "" : ", " + Eval("savedInOtherReport")) + "" %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="save-button-style" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <br>
                    </div>
                </div>
                <br>
                <br>

                <%--  ICD-10 DIAGNOSIS CODE VERIFICATION DETAILS FOR CASE REPORT --%>

                <div class="row">
                    <div class="col col-lg-12 col-sm-12">
                        <div class="tableFixHead-verification" style="background: white;">
                            <asp:GridView ID="gvVerificationStatus" runat="server" AutoGenerateColumns="false" DataKeyNames="diagnosis" OnRowDataBound="gvVerificationStatus_RowDataBound" OnRowCommand="gvVerificationStatus_RowCommand">
                                <Columns>

                                    <asp:TemplateField HeaderText="ICD-10 DIAGNOSIS CODE">
                                        <ItemTemplate>

                                            <span id='<%# "btnCollapse_" + Container.DataItemIndex %>' onclick='<%# "toggleDetails(\""+ Eval("diagnosis") +"\",\""+ Container.DataItemIndex + "\",this);return false;"%>' style="font-weight: 700; transform: scale(1.5); transform-origin: center; display: inline-block; float: left; margin-right: 4px; margin-left: 4px;">
                                                <asp:Label runat="server" ID="plusSign" Visible="false" Text="+"></asp:Label>
                                                </span>

                                            <asp:Label ID="diagnosis" runat="server" Text='<%# Eval("diagnosis")%>' ToolTip='<%# Eval("condition")%>'></asp:Label>

                                            <asp:HiddenField ID="diagnosisId" runat="server" Value='<%# Eval("diagnosisId") %>' />
                                            <asp:HiddenField ID="hddiagnosis" runat="server" Value='<%# Eval("diagnosis") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="DIAGNOSIS VERIFICATION STATUS <br /> (Only one status may be selected)">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlVerifyStatus" onchange= '<%# "onVerificatonDropDownChange("+ Container.DataItemIndex +");" %>' CssClass="form-control" DataValueField="verifyId" DataTextField="details" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="SelectedRow"  >
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DIAGNOSIS VERIFICATION METHOD <br /> (Multiple choice, select all that apply)">
                                        <ItemTemplate>
                                            <asp:CheckBoxList ID="idVerfiyMethod" runat="server" RepeatLayout="Flow"  onchange= '<%# "onVerificatonCheckBoxChecked("+ Container.DataItemIndex +");" %>' RepeatDirection="Horizontal" RepeatRows="2" CssClass="checbox-list" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="CheckedRow">
                                            </asp:CheckBoxList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="COMMENTS">
                                        <ItemTemplate>
                                            <div class="comment-wrapper">
                                                <asp:TextBox ID="txtComment" runat="server" style="display:none"  oninput= '<%# "onVerificationComments("+ Container.DataItemIndex +");" %>' TextMode="MultiLine" placeholder=" -- Type Here -- " CssClass="comment">
                                                </asp:TextBox>

                                                <asp:Label ID="lblComment" runat="server" style="display:none"  />
                                            </div>
                                            <asp:RequiredFieldValidator ID="reValidator" runat="server" Enabled="false" style="color:red;" ErrorMessage="Comments Required *" ControlToValidate="txtComment" ValidationGroup="save" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date/Time, UserName">
                                        <ItemTemplate>

                                            <asp:Label ID="lblverificationDate" runat="server" Text='<%# Eval("verificationDate") %>' Visible="true" /> 
                                            <br />
                                            <asp:Label ID="lblSubmitterName" runat="server" Text='  <%# Eval("submitterName") %>' Visible="true" />                                           
                                            <br />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSave" runat="server" BackColor="#808080" CssClass="cursor-default" BorderColor="#808080" Text="Save" ValidationGroup="save" CommandName="SaveRow" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" Visible="true" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                
                <br>
            </div>
        </div>
    </div>


    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
        <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>

    <div id="dynamicMsg">
        <p id="dynamicMsgs"></p>
    </div>

        <div id="dialogSubmitAlert" class="modal">
        <p id="messagdialogSubmitAlert"></p>
    </div>

        <div id="securityMsg">
</div>

    <script>

        function clearBGD() {
           
            var bodyContent = document.getElementById("mlsSecurityBody");
            var labelContent = document.getElementById("mlsSecuritylabel");

            if (bodyContent) {
                bodyContent.style.display = "none";
            }
            if (labelContent) {
                labelContent.style.display = "none";
            }
            sessionStorage.clear();
        }


        function toggleDetails(diagonsisbuttonId, uId, span) {

            /*  Taking Qcode ID dynamically */
            var rows = document.querySelectorAll(`[data-id='${diagonsisbuttonId}']`);
            /*  Taking + value dynamically and set value - (history)*/
            var span = document.getElementById("btnCollapse_" + uId + "");
            var hddiagnosis = document.getElementById("Content_gvVerificationStatus_hddiagnosis_" + uId + "").value;
            var isExpanded = span.innerText === '-';
            if (hddiagnosis == diagonsisbuttonId) {
                var count = 0;
                var maxCount = 0;
                if (parseInt(uId) == 0) {
                    count = 0;
                    maxCount = rows.length;
                } else {
                    count = uId;
                    maxCount = parseInt(rows.length) + parseInt(count);
                }

                for (var i = count; i < maxCount; i++) {
                    var plusdisable = document.getElementById("btnCollapse_" + i);
                    /* Showing history on click */
;
                    if (parseInt(i) != parseInt(uId)) {
                        for (var j = 1; j < rows.length; j++) {
                            plusdisable.style.display = 'none';
                            if (isExpanded) {
                                rows[j].style.display = 'none';
                            } else {
                                rows[j].style.display = '';
                            }
                        }
                    }
                }
                span.innerText = isExpanded ? '+' : '-';
            }
        }

        /*sending array value dynamically */

        function onVerificatonDropDownChange(gridRowid) {
          
            Verification(gridRowid);
        }

        function onVerificatonCheckBoxChecked(gridRowid) { 
       
            Verification(gridRowid);
        }

        function onVerificationComments(gridRowid) {
            Verification(gridRowid);                       
        }

        /*diagnosis verification*/
        
        function Verification(id) {

            var ddlVerification = document.getElementById("Content_gvVerificationStatus_ddlVerifyStatus_" + id + "");
            var btnSaveVerification = document.getElementById("Content_gvVerificationStatus_btnSave_" + id + "");
            var bChecked = false;
            for (var i = 0; i < 8; i++) { 
                var chkVerification = document.getElementById("Content_gvVerificationStatus_idVerfiyMethod_" + id + "_" + i + "_" + id + "");
                var validator = document.getElementById("Content_gvVerificationStatus_reValidator_" + id + "");
                var commentVerification = document.getElementById("Content_gvVerificationStatus_txtComment_" + id + "");
                if (chkVerification.checked)
                {
                    bChecked = true;
                    if (ddlVerification.value != "SELECT STATUS" && chkVerification.value != "8") {
                        validator.enabled = false;
                        validator.style.visibility = "hidden";
                        btnSaveVerification.disabled = false;
                        btnSaveVerification.setAttribute("onclick", "return ShowProgress(event)");
                        btnSaveVerification.classList.remove("cursor-default");
                        btnSaveVerification.classList.add("cursor-pointer");
                    }
                    else if (commentVerification.value == "" && ddlVerification.value == "SELECT STATUS") {
                        validator.enabled = true;
                        validator.style.visibility = "visible";
                        btnSaveVerification.disabled = true;
                        btnSaveVerification.classList.remove("cursor-pointer");
                        btnSaveVerification.classList.add("cursor-default");

                    }
                    else if (commentVerification.value == "" && ddlVerification.value != "SELECT STATUS") {
                        validator.enabled = true;
                        validator.style.visibility = "visible";
                        btnSaveVerification.disabled = true;
                        btnSaveVerification.classList.remove("cursor-pointer");
                        btnSaveVerification.classList.add("cursor-default");

                    }
                    else if (commentVerification.value != "" && ddlVerification.value == "SELECT STATUS") {
                        validator.enabled = false;
                        validator.style.visibility = "hidden";
                        btnSaveVerification.disabled = true;
                        btnSaveVerification.classList.remove("cursor-pointer");
                        btnSaveVerification.classList.add("cursor-default");
                    }
                    else if (commentVerification.value != "" && ddlVerification.value != "SELECT STATUS") {
                        validator.enabled = false;
                        validator.style.visibility = "hidden";
                        btnSaveVerification.disabled = false;
                        btnSaveVerification.classList.remove("cursor-default");
                        btnSaveVerification.classList.add("cursor-pointer");
                    }
                    else {
                        validator.enabled = false;
                        validator.style.visibility = "visible";
                        btnSaveVerification.disabled = true;
                        btnSaveVerification.classList.remove("cursor-default");
                        btnSaveVerification.classList.add("cursor-pointer");
                    }   
                    
                }
                else
                {
                    validator.enabled = false;
                    validator.style.visibility = "hidden";
                    if (!bChecked) {
                        bChecked = false;
                    }
                  
                }
            }

            if (!bChecked) {
                btnSaveVerification.disabled = true;
                btnSaveVerification.classList.remove("cursor-pointer");
                btnSaveVerification.classList.add("cursor-default");
            }
        }

        /*CCHD*/

        function onDropDownChange() {

            var ddlValue = document.getElementById("Content_gvCCHDCaseVerification_ddlPrenatalCCHDDiagnosis_0");
            var btnSaveCCHD = document.getElementById("Content_gvCCHDCaseVerification_btnSaveCCHD_0");
            if (ddlValue.value != "-1")
            {
                document.getElementById("<%=hdfCCHDDiagnosis.ClientID%>").value = document.getElementById("Content_gvCCHDCaseVerification_ddlPrenatalCCHDDiagnosis_0").value;
                btnSaveCCHD.disabled = false;
                btnSaveCCHD.setAttribute("onclick", "return ShowProgress(event)");
                btnSaveCCHD.classList.remove("cursor-default");
                btnSaveCCHD.classList.add("cursor-pointer");               
            }
            else
            {
                btnSaveCCHD.disabled = true;
                btnSaveCCHD.classList.remove("cursor-pointer");
                btnSaveCCHD.classList.add("cursor-default");
            }
        }

        function onDateChange()
        {
             document.getElementById("<%=hdfCCHDDiagnosisDate.ClientID%>").value = document.getElementById("Content_gvCCHDCaseVerification_dateCCHDIndic_0").value;
        }

        function SaveAlert(event) {

            event.preventDefault();

            $("#dialogSubmitAlert").dialog({
                width: 450,
                modal: true,
                dialogClass: "no-close",
                position: {
                    my: "center center",
                    at: "center center"
                },
                title: "Confirmation",
                open: function () {
                    var connErrorMsg = 'Please confirm the CHD Verification information you are about to submit.';
                    $(this).html(connErrorMsg);
                },
                buttons: {
                    OK: function () {
                        allowDeactive = true;
                        $(this).dialog("close");
                        ShowProgress();
                        document.getElementById("<%=hdfCCHDDiagnosis.ClientID%>").value = document.getElementById("Content_gvCCHDCaseVerification_ddlPrenatalCCHDDiagnosis_0").value;
                        document.getElementById("<%=hdfCCHDDiagnosisDate.ClientID%>").value = document.getElementById("Content_gvCCHDCaseVerification_dateCCHDIndic_0").value;
                },
                Cancel: function () {
                    $(this).dialog("close");
                    return false;
                }
            },
        });
        }

        function ComDial(message) {

            $("#dynamicMsg").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Confirmation",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

        function ShowProgress(save) {

            /*var isValid = true;*/
            var isValid = Page_ClientValidate("save");

            if (!isValid) {

                $("#btnSave").prop("disabled", true);
            }
            else {

                $("#btnSave").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",



                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },



                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        function enableDisableControls(dropdownClientId, dateInputClientId, saveButtonClientId) {
            var dropdown = document.getElementById(dropdownClientId);
            var dateInput = document.getElementById(dateInputClientId);
            var saveButton = document.getElementById(saveButtonClientId);

            dateInput.disabled = dropdown.value != 'Yes';

            saveButton.disabled = !(dropdown.value == 'Yes' && dateInput.value);
        }
                

    </script>
    <script type="text/javascript">


        function linkedCaseRecordDetails(url) {
            
            const title = "Linked Case Record Details";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }



        function linkedCaseReports(url) {
            
            const title = "List of Linked Case Reports for Case Record";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }


    </script>

    <% } %>
</asp:Content>


