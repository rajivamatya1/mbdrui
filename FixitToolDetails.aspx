﻿<%@ Page Title="FIX IT TOOL" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FixitToolDetails.aspx.cs" Inherits="DynamicGridView.FixitToolDetails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
    <link href="Content/css/MLTable.css" rel="stylesheet" />
    <style>
        .caseReports {
            display: table-cell;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">

    <%-- Hidden Fields ---%>
    <div>
        <asp:HiddenField ID="hdfCaseReportId" runat="server" />
        <asp:HiddenField ID="hdfCaseRecordId" runat="server" />
        <asp:HiddenField ID="hdfFixitId" runat="server" />
    </div>

    <%-- Header Field Start ---%>
    <div class="row" id="mlsSecurityHeader">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" />FIX IT TOOL</h1>
        </div>
    </div>
    <%-- Header Field End ---%>

    <%-- Button Field Actions Start (Create,Resolved,Match,Comments...)---%>

    <div class="row" id="mainScreen" runat="server">
        <div class="col col-lg-12 col-md-12 col-sm-12" id="mlsSecurityBody">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button Visible="false" Enabled="false" Text="Return to Listing" OnClick="btnReturnResult_Click" runat="server" ID="btnReturnResult" CssClass="btn btn-primary returnSearch" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="row row-no-padding-matching-linking">
                        <div class="row">
                            <div class="col col-lg-8 col-md-8 col-sm-8 mlsourceMargin">
                                <div class="col col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-item reporterInfo">
                                        <b>FID</b>:
                                        <asp:Literal ID="litFid" runat="server" />
                                        <b class="marginLeft10">Flagged By</b>:
                                        <asp:Literal ID="litReporter" runat="server" />
                                        <b class="marginLeft10">Date Flagged</b>:
                                        <asp:Literal ID="LitDateFlagged" runat="server" />
                                        <b class="marginLeft10">Actions</b>:
                                        <asp:Literal ID="LitActions" runat="server" />
                                    </div>
                                </div>
                                <div class="col col-lg-12 col-md-12 col-sm-12">                                       
                                    <div class="form-item reporterInfo">
                                        <b>Comment</b>:
                                     <asp:Literal ID="LitComments" runat="server" />
                                    </div>
                                </div>

                                <div class="col col-lg-12 col-md-12 col-sm-12 pt-1">
                                    <div class="admin-workarea-menu">
                                        <ul class="menu">
                                            <li class="leaf">
                                                <asp:LinkButton ID="btnRelease" title="Release ownership" Width="90px" runat="server" OnClick="btnRelease_Click" OnClientClick="return releaseAlert(event)">
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/unlock-fill.svg"  alt="Link"></image> Release
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf margin10px">
                                                <asp:LinkButton ID="btnResolved" title="Resolved" Width="90px" runat="server" OnClick="btnResolved_Click" OnClientClick="return resolveAlert(event)"> 
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/resolved.svg"  alt="Link"></image> Resolved
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf margin150px" runat="server">
                                                <asp:LinkButton ID="btnMove" title="Move" Width="90px" CssClass="enableGrey" Enabled="false" runat="server" OnClick="btnMove_Click" OnClientClick="ShowProgress(this)">
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/move.svg" alt="Link"></image> Move
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf margin10px" runat="server">
                                                <asp:LinkButton ID="btnCreate" title="Create" Width="90px" CssClass="enableGrey" Enabled="false" runat="server" OnClick="btnCreate_Click" OnClientClick="ShowProgress(this)">
                                                 <div class="link-icon">
                                                      <image src="Content/css/images/file-earmark-plus-fill.svg"  alt="Create Case Record ID"></image> Create                                           
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-4">
                                <div>
                                    <asp:Label class="comments-FixitDetails" ID="lblFixTool" runat="server">Comment for a CREATE or MOVE</asp:Label>
                                    <br>
                                    <asp:TextBox ID="txtFixTool" ToolTip="Comment for a CREATE or MOVE" placeholder="- - type optional comment - -" Style="margin-top: -12px;" runat="server" TextMode="MultiLine" class="fixTextBox"></asp:TextBox><br>
                                    <asp:RequiredFieldValidator ID="rqFieldValudator" Style="display: none; color: red;" runat="server" ErrorMessage="Required field *" ControlToValidate="txtFixTool" Enabled="false"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- <div class="admin-workarea">--%>
                    <div>
                        <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                            <div class="wrapperFixIt">

                                <div class="scroll-wrapper" id="top-scroll">
                                </div>
                                <div class="table-container-fix">


                                    <table id="fixitTable" class="fititTable" border="1" cellspacing="0">
                                        <% if (dtFixItReportDataCases != null && dtFixItReportDataCases.Rows.Count > 0)
                                            {%>
                                        <%-- Header Name Display Start--%>
                                        <thead>
                                            <tr>
                                                <th class="colorRowColLabel sticky-fixitFirstColumn"></th>
                                                <% for (int i = 0; i < dtFixItReportDataCases.Rows.Count; i++)
                                                    {
                                                        if (Convert.ToString(dtFixItReportDataCases.Rows[i][0]) == "Case Record")
                                                        {
                                                            if (i == 0)
                                                            {%><th class="colorRowRecords sticky-fixitsecondColumn"><%=dtFixItReportDataCases.Rows[i][0] %>
                                         <span class="toggleCaseReports" data-case-id="<%= Convert.ToInt32(dtFixItReportDataCases.Rows[i][41]) %>" onclick="toggleReports(this);">
                                             <img src="Content/css/images/minus-circle.svg" title="Expand/Collapse" alt="Right Icon" class="arrowsFixit" />
                                         </span>
                                     </th>
                                                <%}
                                                    else
                                                    {%><th class="colorRowRecords"><%=dtFixItReportDataCases.Rows[i][0] %>
                                         <span class="toggleCaseReports" data-case-id="<%= Convert.ToInt32(dtFixItReportDataCases.Rows[i][41]) %>" onclick="toggleReports(this);">
                                             <img src="Content/css/images/minus-circle.svg" title="Expand/Collapse" alt="Right Icon" class="arrowsFixit" />
                                         </span>
                                     </th>
                                                <%}
                                                    }
                                                    else
                                                    {%><th class="colorRowReports caseReports case-<%=dtFixItReportDataCases.Rows[i][41]%>" style="left: 0px; text-align: left; position: static;"><%=dtFixItReportDataCases.Rows[i][0] %></th>
                                                <%}
                                                    }%>
                                            </tr>
                                        </thead>
                                        <%-- Header Name Display End--%>
                                        <%-- Check Box Display Start--%>
                                        <tbody id="table-body">
                                            <tr style="background-color: #eceef1;">
                                                <td class="colorRowColLabel sticky-fixitFirstColumn"></td>

                                                <%
                                                    int j = 0;
                                                    int minCaseRecordId = int.MaxValue;

                                                    for (int i = 0; i < dtFixItReportDataCases.Rows.Count; i++)
                                                    {
                                                        if (Convert.ToString(dtFixItReportDataCases.Rows[i][0]) == "Case Record")
                                                        {
                                                            int currentCaseRecordId = Convert.ToInt32(dtFixItReportDataCases.Rows[i][41]);

                                                            if (currentCaseRecordId < minCaseRecordId)
                                                            {
                                                                minCaseRecordId = currentCaseRecordId;  // check min case record id
                                                            }
                                                        }
                                                    }

                                                    for (int i = 0; i < dtFixItReportDataCases.Rows.Count; i++)
                                                    {
                                                        string checkBoxType = Convert.ToString(dtFixItReportDataCases.Rows[i][0]);
                                                        string checkboxId = "chkId" + i;
                                                        string onClickFunction;
                                                        string checkboxValue;
                                                        string cssclass = "";
                                                        string caseRecordId = "";
                                                        string caseReportId = "";
                                                        bool isCheckedReport = false;
                                                        bool isCheckedRecord = false;
                                                        if (checkBoxType == "Case Record")
                                                        {
                                                            j = i;
                                                            onClickFunction = "handleRecordClick(this)";
                                                            checkboxValue = Convert.ToString("caserecord_" + dtFixItReportDataCases.Rows[i][41]);
                                                            cssclass = "colorColCaseRecords";
                                                            caseRecordId = Convert.ToString(dtFixItReportDataCases.Rows[i][41]);

                                                            if (!string.IsNullOrEmpty(caseRecordId) && !string.IsNullOrEmpty(hdfCaseRecordId.Value))
                                                            {
                                                                if (caseRecordId == hdfCaseRecordId.Value)
                                                                {
                                                                    isCheckedRecord = true;
                                                                    
                                                                }
                                                            }
                                                            string cellStyle = (Convert.ToInt32(caseRecordId) == minCaseRecordId) ? "classPinkCell sticky-fixitsecondColumn" : "";
                                                            %>
                                                            <td class="<%=cellStyle %>">
                                                                <input type="checkbox" id="<%=checkboxId %>" data-case-id="<%=caseRecordId %>" class="caseRecord chkHeader <%=cssclass %>" 
                                                                    name="chkname" onclick="<%= onClickFunction %>" value="<%=checkboxValue %>" <%= isCheckedRecord ? "checked" :"" %>  />
                                                                <input type="button" id="btnCheckAll_<%=dtFixItReportDataCases.Rows[j][41] %>" class="caseReport chkHeader buttonSelectAll" name="chkAllname" value="Select All" 
                                                                    onclick="checkAllAssoCaseReports('<%=dtFixItReportDataCases.Rows[j][41] %>')" />
                                                            </td>
                                                            <%
                                                                    if (isCheckedRecord)
                                                                    {
                                                                        string elementId = checkboxId;
                                                                        string script = $"handleRecordClick(document.getElementById('{elementId}'));";
                                                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "handle Record Click", script, true);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    onClickFunction = "handleReportClick(this)";
                                                                    checkboxValue = Convert.ToString("casereport_" + dtFixItReportDataCases.Rows[j][41] + "_" + dtFixItReportDataCases.Rows[i][3]);
                                                                    caseReportId = Convert.ToString(dtFixItReportDataCases.Rows[i][3]);

                                                                    if (!string.IsNullOrEmpty(caseReportId) && !string.IsNullOrEmpty(hdfCaseReportId.Value))
                                                                    {
                                                                        string[] splitRecords = hdfCaseReportId.Value.Split(',');
                                                                        foreach (var item in splitRecords)
                                                                        {
                                                                            if (caseReportId == item)
                                                                            {
                                                                                isCheckedReport = true;

                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                        %>
                                                        <td class="caseReports case-<%=dtFixItReportDataCases.Rows[i][41]%>">
                                                            <input type="checkbox" id="<%=checkboxId %>" data-case-id="<%=dtFixItReportDataCases.Rows[j][41] %>" data-case-reportid="<%=caseReportId %>"
                                                                class="caseReport chkHeader  <%=cssclass %>" name="chkname" onclick="<%= onClickFunction %>" 
                                                                value="<%=checkboxValue %>" <%= isCheckedReport ? "checked" :"" %>  /> 
                                                        </td>
                                                        <%
                                                        }
                                                    }%>
                                            </tr>
                                            <%-- Check Box Display End--%>
                                            <%-- Details Display Start--%>

                                            <% 
                                                string existingItemValue = "";
                                                string existingFirstItemValue = "";

                                                for (int i = 1; i < dtFixItReportDataCases.Columns.Count; i++)
                                                {
                                                    if (i != 41)
                                                    {%>
                                            <tr>
                                                <td class="colorRowColLabel sticky-fixitFirstColumn"><%=dtFixItReportDataCases.Columns[i].ColumnName %></td>
                                                <% 
                                                    var index = 0;

                                                    foreach (System.Data.DataRow item in dtFixItReportDataCases.Rows)
                                                    {

                                                        string currentitemValue = item[i].ToString().ToLower();

                                                        if (Convert.ToString(item[0]) == "Case Record" && index == 0)
                                                        {
                                                            existingItemValue = Convert.ToString(item[i]).ToLower();
                                                            existingFirstItemValue = Convert.ToString(item[i]).ToLower();
                                                            
                                                           if (dtFixItReportDataCases.Columns[i].ColumnName == "Historic Data")
                                                            {
                                                                if (!string.IsNullOrEmpty(Convert.ToString(item[i])))
                                                                {
                                                                   %><td class="colorColCaseRecords sticky-fixitsecondColumn case-<%=item[41]%> greenTick"></td>  <%-- Image --%> <%
                                                                }
                                                                else
                                                                { 
                                                                   %><td class="colorColCaseRecords sticky-fixitsecondColumn case-<%=item[41]%>">&nbsp;</td>  <%--blank--%><%
                                                                }
                                                            }
                                                            else
                                                            {   
                                                               %><td class="colorColCaseRecords sticky-fixitsecondColumn"><%=item[i] %></td><%                                                                                                     
                                                            }
                                                        }
                                                        else if (Convert.ToString(item[0]) == "Case Record")
                                                        {
                                                            existingItemValue = Convert.ToString(item[i]).ToLower();

                                                            if (dtFixItReportDataCases.Columns[i].ColumnName == "Historic Data")
                                                            {
                                                                if (!string.IsNullOrEmpty(Convert.ToString(item[i])))
                                                                {
                                                                    %><td class="colorColCaseRecords greenTick"></td>
                                                                    <%-- Image --%> <%
                                                                }
                                                                else
                                                                {
                                                                    %><td class="colorColCaseRecords">&nbsp;</td>
                                                                    <%--blank--%><%
                                                                     }
                                                            }
                                                            else if (existingFirstItemValue != existingItemValue && dtFixItReportDataCases.Columns[i].ColumnName != "Scrutinizer ID"
                                                                    && dtFixItReportDataCases.Columns[i].ColumnName != "Case Record ID"
                                                                    && dtFixItReportDataCases.Columns[i].ColumnName != "Case Report ID"
                                                                    && dtFixItReportDataCases.Columns[i].ColumnName != "Admission Date"
                                                                    && dtFixItReportDataCases.Columns[i].ColumnName != "Discharge Date"
                                                                    && dtFixItReportDataCases.Columns[i].ColumnName != "Historic Data")
                                                            {%>
                                                                <td class="classPurpCell"><%=item[i] %></td>                    
                                                            <%}
                                                            else
                                                            {
                                                                %>
                                                                <td class="colorColCaseRecords"><%=item[i] %></td>
                                                                <%                                                                                                      
                                                            }
                                                        }
                                                    else if (existingItemValue.Trim() != currentitemValue.Trim()
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Scrutinizer ID"
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Case Record ID"
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Case Report ID"
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Admission Date"
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Discharge Date"
                                                                && dtFixItReportDataCases.Columns[i].ColumnName != "Historic Data")
                                                    {
                                                                %><td class="caseReports case-<%=item[41]%> pinkCellSimilar"><%=item[i] %></td><%
                                                        }
                                                        else if (dtFixItReportDataCases.Columns[i].ColumnName == "Historic Data")
                                                        {
                                                            if (!string.IsNullOrEmpty(Convert.ToString(item[i])))
                                                            {
                                                               %><td class="caseReports case-<%=item[41]%> greenTick"></td>  <%-- Image --%> <%
                                                            }
                                                            else
                                                            { 
                                                               %><td class="caseReports case-<%=item[41]%>">&nbsp;</td>  <%--blank--%><%
                                                            }
                                                        }
                                                        else
                                                        {
                                                           %><td class="caseReports case-<%=item[41]%>"><%=item[i] %></td><%}  
                                                            index++;
                                                        }
                                                %>
                                            </tr>
                                            <%
                                                }
                                            }%>
                                        </tbody>
                                        <%} %>
                                    </table>
                                </div>
                                <div id="bottom-scroll" class="scroll-wrapper" style="display: none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>

    <div id="errorMsgs">
        <p id="errorMsgsForAll"></p>
    </div>

    <div id="commonMsg">
    </div>

    <div id="dialogRelease" class="modal" title="Selection Alert">
        <p id="messageRelease"></p>
    </div>

    <div id="dialogResolve" class="modal" title="Selection Alert">
        <p id="messageResolve"></p>
    </div>

    <div id="dialogMove" class="modal" title="Selection Alert">
        <p id="messageMove"></p>
    </div>

    <div id="securityMsg">
</div>

    <script>

        function clearBGD() {
            
            var headerContent = document.getElementById("mlsSecurityHeader");
            var bodyContent = document.getElementById("mlsSecurityBody");

            if (headerContent) {
                headerContent.style.display = "none";
            }
            if (bodyContent) {
                bodyContent.style.display = "none";
            }
            sessionStorage.clear();
        }

        window.onload = function () {

            var hdffixTool = document.getElementById("<%=hdfFixitId.ClientID%>");
            if (hdffixTool.value == "true") {
                var event = new Event('click'); // simulating a click event
                toggleCheckBtnFix(event, 'btnFix');
            }
        };     

        function ShowProgress(btnEle) {

            if (btnEle && $(btnEle).hasClass('enableGrey')) {
                return;
            }
            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");
            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        // Variables to store checked case records and case reports       

        function updateCheckedLists() {
           
            let checkedCaseRecord = null;
            let checkedCaseReports = [];
            // Get checked case record and case reports
            checkedCaseRecord = document.querySelector('.caseRecord:checked')?.dataset.caseId || null;
            checkedCaseReports = Array.from(document.querySelectorAll('.caseReport:checked')).map(report => report.dataset.caseReportid);

            let allAssociatedReportsSelected = false;
            let isCaseRecordChecked = false;

            document.querySelectorAll('.caseRecord').forEach(record => {
                const caseId = record.dataset.caseId;
                const associatedReports = document.querySelectorAll(`.caseReport[data-case-id="${caseId}"]`);

                const checkedRecords = document.querySelector('.caseRecord:checked')?.dataset.caseId || null;;

                // Check if all associated reports for this record are selected
                const allReportsChecked = Array.from(associatedReports).every(report => report.checked);

                if (allReportsChecked && !checkedRecords) {
                    allAssociatedReportsSelected = true;
                }
            });      // Scenario to handle - one case one report do not allow to create new, selected all reports and then create should not allow

            // Enable the appropriate button based on selections

            if (checkedCaseRecord && checkedCaseReports.length > 0 && !allAssociatedReportsSelected) {
                btnMoveEnable();
                btnCreateDisable();
                btnResolvedDisable();
            }
            else if (checkedCaseReports.length > 0 && !allAssociatedReportsSelected) {
                btnCreateEnable();
                btnMoveDisable();
                btnResolvedDisable();
            }
            else if (allAssociatedReportsSelected) {
                btnMoveDisable();
                btnCreateDisable();
                if (checkedCaseRecord || checkedCaseReports.length > 0) {
                    btnResolvedDisable();
                } else {
                    btnResolvedEnable();
                }
            }
            else {
                btnMoveDisable();
                btnCreateDisable();
                if (checkedCaseRecord || checkedCaseReports.length > 0) {
                    btnResolvedDisable();
                } else {
                    btnResolvedEnable();
                }
            }

            document.getElementById("<%=hdfCaseRecordId.ClientID%>").value = checkedCaseRecord;
            document.getElementById("<%=hdfCaseReportId.ClientID%>").value = checkedCaseReports;
        }

        function handleRecordClick(recordCheckbox) {
            
            const caseId = recordCheckbox.dataset.caseId;
            const allRecords = document.querySelectorAll('.caseRecord');

            if (recordCheckbox.checked) {
                // If this case record is checked, uncheck and disable all other case records
                let associatedReports = document.querySelectorAll(`.caseReport[data-case-id="${caseId}"]`);
                allRecords.forEach(record => {
                    if (record !== recordCheckbox) {
                        record.checked = false;
                        record.disabled = true;
                    }
                });

                // Disable associated case reports
                associatedReports.forEach(report => {
                    report.checked = false;
                    report.disabled = true;
                });
            } else {

                // Only enable other case records if all associated case reports are unchecked
                const associatedReports = Array.from(document.querySelectorAll('.caseReport'));
               
                const anyCheckedReports = Array.from(associatedReports).some(report => report.checked);

                if (!anyCheckedReports) {
                    // No associated case reports are checked, so enable all case records
                    allRecords.forEach(record => record.disabled = false);
                } else {
                    // Keep other case records disabled if any associated reports are checked

                    let checkCaseRecord = "";
                    const allReports = document.querySelectorAll(`.caseReport`);
                    allReports.forEach(report => {
                        if (report.checked) {
                            checkCaseRecord = report.dataset.caseId;
                        }
                    });
                    allRecords.forEach(record => {
                        if (record.dataset.caseId == checkCaseRecord) {
                            record.disabled = true;
                        } else {
                            record.disabled = false;
                        }
                    });
                }

                // Enable associated case reports since the case record was unchecked
                associatedReports.forEach(report => {
                    report.disabled = false
                });
            }

            // Update button states after any change
            updateCheckedLists();
        }

        function checkAllAssoCaseReports(caseId) {
            var button = document.getElementById('btnCheckAll_' + caseId +'');
            const associatedRecord = document.querySelector(`.caseRecord[data-case-id="${caseId}"]`);
            const associatedReports = document.querySelectorAll(`.caseReport[data-case-id="${caseId}"]`);
            const allRecords = document.querySelectorAll('.caseRecord');
            const anyCheckedReports = Array.from(associatedReports).some(report => report.checked);

                if (button.value == "Select All") {
                    
                    for (var j = 0; j < associatedReports.length; j++) {
                        if (associatedReports[j] != this) {
                            associatedReports[j].checked = true;
                        }
                    }
                    associatedRecord.disabled = true;                
                button.value = "Unselect All";
                } 
                else {
                   
                    for (var j = 0; j < associatedReports.length; j++) {
                        if (associatedReports[j] != this) {
                            associatedReports[j].checked = false;
                        }
                    }
                    associatedRecord.disabled = false;
                    button.value = "Select All";
            }
            // Update button states after any change
            updateCheckedLists();
        }

        function handleReportClick(reportCheckbox) {
            
            const caseId = reportCheckbox.dataset.caseId;
            const associatedRecord = document.querySelector(`.caseRecord[data-case-id="${caseId}"]`);
           
            if (reportCheckbox.checked) {
                // Disable the associated case record when any case report is selected
                associatedRecord.disabled = true;
            } else {
                // Check if any other associated reports are still checked
                reportCheckbox.checked = false;
                var button = document.getElementById('btnCheckAll_' + caseId + '');
                button.value = "Select All";
                const associatedReports = document.querySelectorAll(`.caseReport[data-case-id="${caseId}"]`);
                const anyCheckedReports = Array.from(associatedReports).some(report => report.checked);
                const allRecords = document.querySelectorAll('.caseRecord');

                if (!anyCheckedReports) {
                    // Enable the associated case record if no other reports are checked
                    const anyCheckedRecord = Array.from(allRecords).some(report => report.checked);
                    if (!anyCheckedRecord) {
                        associatedRecord.disabled = false;
                    }
                } else {
                    if (anyCheckedReports) {
                        const allCheckedRecords = document.querySelectorAll('.caseRecord:checked');
                        if (!allCheckedRecords) {
                            associatedRecord.disabled = true;
                        }

                    } else {
                        allRecords.forEach(record => {
                            if (!record.checked) {
                                record.disabled = true;
                            }

                        });
                    }
                }
            }

            // Update button states after any change
            updateCheckedLists();
        }

        function btnCreateEnable() {
            var btnCreate = document.getElementById("<%=btnCreate.ClientID %>");
            $(btnCreate).attr('href', "javascript: __doPostBack('ctl00$Content$btnCreate', '')");
            $(btnCreate).removeClass('enableGrey');
            $(btnCreate).addClass('disableGrey');
        }

        function btnCreateDisable() {
            var btnCreate = document.getElementById("<%=btnCreate.ClientID %>");
            $(btnCreate).removeAttr('href');
            $(btnCreate).removeClass('disableGrey');
            $(btnCreate).addClass('enableGrey');
        }

        function btnMoveEnable() {

            var btnMove = document.getElementById("<%=btnMove.ClientID %>");
            $(btnMove).attr('href', "javascript: __doPostBack('ctl00$Content$btnMove', '')");
            $(btnMove).removeClass('enableGrey');
            $(btnMove).addClass('disableGrey');
        }

        function btnMoveDisable() {

            var btnMove = document.getElementById("<%=btnMove.ClientID %>");
            $(btnMove).removeAttr('href');
            $(btnMove).removeClass('disableGrey');
            $(btnMove).addClass('enableGrey');
        }

        function btnResolvedEnable() {

            var btnResolved = document.getElementById("<%=btnResolved.ClientID %>");
            $(btnResolved).attr('href', "javascript: __doPostBack('ctl00$Content$btnResolved', '')");
            $(btnResolved).removeClass('enableGrey');
            $(btnResolved).addClass('disableGrey');
        }

        function btnResolvedDisable() {

            var btnResolved = document.getElementById("<%=btnResolved.ClientID %>");
            $(btnResolved).removeAttr('href');
            $(btnResolved).removeClass('disableGrey');
            $(btnResolved).addClass('enableGrey');
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",

                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },

                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        function errorMsgs(message, titleFor) {

            $("#errorMsgs").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                            ShowProgress();
                     <%= Page.ClientScript.GetPostBackEventReference(btnReturnResult, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }

            $(".ui-dialog-titlebar-close").on("click", function () {
                ShowProgress();
         <%= Page.ClientScript.GetPostBackEventReference(btnReturnResult, String.Empty)  %> 
            });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
                    ShowProgress();
         <%= Page.ClientScript.GetPostBackEventReference(btnReturnResult, String.Empty)  %> 
                }
            });
        }

        function cAlertMsgs(message, titleFor) {

            $("#commonMsg").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

        function releaseAlert(event) {
         
            event.preventDefault();

            let $btnEle = $('#Content_btnRelease');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }

            $(function () {
                $("#dialogRelease").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Release Confirmation",
                    open: function () {
                        var connErrorMsg = 'You have clicked to Release this Fixable. You will no longer OWN the Fixable and will be returned to the Fix It Tool Listing screen.';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        OK: function () {
                            allowAssign = true;
                            $(this).dialog("close");
                            ShowProgress();
                     <%= Page.ClientScript.GetPostBackEventReference(btnRelease, String.Empty)  %>  
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
            });
        }

        function resolveAlert(event) {

            event.preventDefault();

            let $btnEle = $('#Content_btnResolved');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }

            $(function () {
                $("#dialogResolve").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Resolve Confirmation",
                    open: function () {
                        var connErrorMsg = 'You have clicked to Resolve the Fixable. This Fixable will no longer be accessible in the Fix It Tool for performing corrections.';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        OK: function () {
                            allowAssign = true;
                            $(this).dialog("close");
                            ShowProgress();
                            <%= Page.ClientScript.GetPostBackEventReference(btnResolved, String.Empty)  %>  
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
            });
        }       

        function toggleReports(element) {

            const caseId = element.getAttribute('data-case-id');
            const reports = document.querySelectorAll(`.caseReports.case-${caseId}`);

            reports.forEach(row => {
                if (row.style.display === 'table-cell' || !row.style.display) {
                    row.style.display = 'none';
                } else {
                    row.style.display = 'table-cell';
                }
            });

            const rightArrow = 'Content/css/images/plus-circle.svg';
            const leftArrow = 'Content/css/images/minus-circle.svg';

            const img = element.querySelector("img");
            if (img) {
                img.src = img.src.includes(rightArrow) ? leftArrow : rightArrow;
            }

            // added for scroll bar toggle dynamically 

            const $topScroll = $('#top-scroll');
            const $bottomScroll = $('#bottom-scroll');
            const $tableContainer = $('.table-container-fix');
            const $mainTable = $('#fixitTable');

            $topScroll.html('<div style="height:1px;"></div>');
            $bottomScroll.html('<div style="height:1px;"></div>');
            const $dummyScrollBarTop = $topScroll.find('div');
            const $dummyScrollBarBottom = $bottomScroll.find('div');
            function updateScrollBarWidths() {
                const tableWidth = $mainTable.outerWidth();
                $dummyScrollBarTop.css('width', `${tableWidth}px`);
                $dummyScrollBarBottom.css('width', `${tableWidth}px`);
            }
            updateScrollBarWidths();
        }

        // Scrollbar on page load 

        $(document).ready(function () {
            const $topScroll = $('#top-scroll');
            const $bottomScroll = $('#bottom-scroll');
            const $tableContainer = $('.table-container-fix');
            const $mainTable = $('#fixitTable');


            $topScroll.html('<div style="height:1px;"></div>');
            $bottomScroll.html('<div style="height:1px;"></div>');
            const $dummyScrollBarTop = $topScroll.find('div');
            const $dummyScrollBarBottom = $bottomScroll.find('div');
            function updateScrollBarWidths() {
                const tableWidth = $mainTable.outerWidth();
                $dummyScrollBarTop.css('width', `${tableWidth}px`);
                $dummyScrollBarBottom.css('width', `${tableWidth}px`);
            }

            updateScrollBarWidths();
            $(window).on('resize', updateScrollBarWidths);

            $topScroll.on('scroll', function () {
                const scrollLeft = $topScroll.scrollLeft();
                $tableContainer.scrollLeft(scrollLeft);
                $bottomScroll.scrollLeft(scrollLeft);
            })

            $bottomScroll.on('scroll', function () {
                const scrollLeft = $bottomScroll.scrollLeft();
                $tableContainer.scrollLeft(scrollLeft);
                $topScroll.scrollLeft(scrollLeft);
            })

            $tableContainer.on('scroll', function () {
                const scrollLeft = $tableContainer.scrollLeft();
                $bottomScroll.scrollLeft(scrollLeft);
                $topScroll.scrollLeft(scrollLeft);
            })
        })

        function BDConflictDialog(type) {
          
            $(function () {
                $("#dialogMove").dialog({
                    width: "80%",
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                    my: "center center",
                    at: "center center"
                },
                    title: "Resolve Birth/Death Records Conflict",
                    open: function () {

                        var connMsg = '<iframe src="/FixItToolBirthDeath.aspx?type=' + type +'&fixItId=<%=hdfFixitId.Value%>&existingtargetValue=<%=hdfCaseRecordId.Value%>&existingreports=<%=hdfCaseReportId.Value%>&checkname=<%=hdfCheckName.Value%>&primary=<%=hdfPrimary.Value%>&sourcebirth=<%=hdfBirthSource.Value %>&targetbirth=<%=hdfBirthTarget.Value %>&birth=<%=hdfBirths.Value %>&sourcedeath=<%=hdfDeathSource.Value%>&targetdeath=<%=hdfDeathTarget.Value%>&death=<%=hdfDeath.Value%>&usercomments='+ <%=hdfComments.Value%> +'"width="100%" height="785px" style="border: none;"></iframe>';
                        $(this).find('#messageMove').html(connMsg);
                    }
                });

                $(".ui-dialog-titlebar-close").on("click", function () {
                    closeModelPopUp();
                });
            });
        }

        function closeModelPopUp() {          
            $("#dialogMove").dialog("close");
            updateCheckedLists();
        }

        function closeModelPopUpRefresh(fixItId) {
            $("#dialogMove").dialog("close");
        
            window.location.href = 'FixitToolDetails.aspx?fixItId=' + fixItId + '';
        }

    </script>
    <div>
        <asp:HiddenField ID="hdfPrimary" runat="server" />
        <asp:HiddenField ID="hdfBirths" runat="server" />
        <asp:HiddenField ID="hdfDeath" runat="server" />
        <asp:HiddenField ID="hdfBirthSource" runat="server" />
        <asp:HiddenField ID="hdfBirthTarget" runat="server" />
        <asp:HiddenField ID="hdfDeathSource" runat="server" />
        <asp:HiddenField ID="hdfDeathTarget" runat="server" />
        <asp:HiddenField ID="hdfCheckName" runat="server" />
         <asp:HiddenField ID="hdfComments" runat="server" />
    </div>

</asp:Content>
