﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DownloadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string component = !string.IsNullOrEmpty(Request.QueryString["component"]) ? Convert.ToString(Request.QueryString["component"]) : "";

        string schema = string.Empty;
        string tableName = string.Empty;

        if (component != "0" && component != "")
        {
            if (component == "userManage")
            {
                schema = "MBDR_System";
                tableName = "UsersHistory";

                GenerateUMCSV(schema, tableName);
            }
            else if (component == "caseVerify")
            {
                schema = "MBDR_System";
                tableName = "CaseVerificationAudit";

                GenerateCVCSV(schema, tableName);
            }

            /*** Future development ***/

            //else if (ddlComp.SelectedValue == "MatchLink")
            //{
            //    schema = "MBDR_System";
            //    tableName = "MatchingLinking";
            //}

            //else if (ddlComp.SelectedValue == "FixIt")
            //{
            //    schema = "MBDR_System";
            //    tableName = "FixItTools";
            //}
            //else if (ddlComp.SelectedValue == "RefTable")
            //{
            //    schema = "MBDR_System";
            //    tableName = "ReferenceTables";
            //}
        }

    }

    /********* User Management Start ***********/
    protected void GenerateUMCSV(string schema, string tableName)
    {
        List<GridViewModel> lstFilter = Common.GetFilterList();

        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        if (model != null)
        {
            includeColumns = model.IncludedColumns;
            readonlyColumns = model.ReadonlyColumns;
            dataKeyname = model.DataKeyName;
        }

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            DataTable dt = new DataTable();
            string query = "";
            string where = GetWhereClauseUM();
            string OrderBySID = "displayName, modified asc";

            query = string.Format(@"SELECT modifier 'Performed By',	modified 'Action Date/Time', status 'Status', displayName 'Display Name', oldFirstName 'Old First Name', newFirstName 'New First Name', oldLastName 'Old Last Name', newLastName 'New Last Name', oldEmailAddress 'Old Email Address', newEmailAddress 'New Email Address', oldRole 'Old Role', newRole 'New Role' FROM {0}.{1} {2} ORDER BY {3}", schema, tableName, where, OrderBySID);

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dt);

            string csvContent = ConvertToCSV(dt);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment;filename=umAudit.csv");
            Response.ContentEncoding = Encoding.UTF8;
            Response.Write(csvContent);
            Response.End();

        }
    }    
    private string GetWhereClauseUM()
    {
        string where = "where 1=1";

        string performedBy = !string.IsNullOrEmpty(Request.QueryString["performedby"]) ? Convert.ToString(Request.QueryString["performedby"]) : "";
        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayname"]) ? Convert.ToString(Request.QueryString["displayname"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

        if (performedBy != "0" && performedBy != "")
        {
            if (performedBy != "Select Performer")
            {
                where += " and modifier = '" + performedBy + "'";
            }
        }

        if (displayName != "0" && displayName != "")
        {
            if (displayName != "All Users")
            {
                where += " and displayName = '" + displayName + "'";
            }
        }

        /*** modified datetime - From Date to Date ***/

        if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // both from/to date
        {
            where += " and modified > ='" + fromDate + " 00:00:00'  and modified < ='" + toDate + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate)) // from date only 
        {
            where += " and modified > ='" + fromDate + " 00:00:00' ";
        }
        else if (string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // to date only 
        {
            where += " and modified < ='" + toDate + " 23:59:59' ";
        }

        return where;
    }

    /********* User Management End ***********/

    /********* Case Verifcation Start ***********/

    protected void GenerateCVCSV(string schema, string tableName)
    {
        List<GridViewModel> lstFilter = Common.GetFilterList();

        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        if (model != null)
        {
            includeColumns = model.IncludedColumns;
            readonlyColumns = model.ReadonlyColumns;
            dataKeyname = model.DataKeyName;
        }

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            DataTable dt = new DataTable();
            string query = "";
            string where = GetWhereClauseCV();
            string OrderBySID = "performedBy asc";

            query = string.Format(@"SELECT performedBy 'Performed By', actionDateTime 'Action Date/Time', reportId 'Report ID', actionType 'Type', prenatalCodes 'Prenatal Codes', ICD10DiagnosisCode 'ICD-10 Codes',indicationOrStatus 'Indication/Status', dateOfEarliestCCHD 'Date Of Earliest', methods 'Method',comment 'Comment' FROM {0}.{1} {2} ORDER BY {3}", schema, tableName, where, OrderBySID);

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dt);

            string csvContent = ConvertToCSV(dt);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment;filename=cvAudit.csv");
            Response.ContentEncoding = Encoding.UTF8;
            Response.Write(csvContent);
            Response.End();

        }
    }
    private string GetWhereClauseCV()
    {
        string where = "where 1=1";

        string performedBy = !string.IsNullOrEmpty(Request.QueryString["performedby"]) ? Convert.ToString(Request.QueryString["performedby"]) : "";
        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayname"]) ? Convert.ToString(Request.QueryString["displayname"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

        if (performedBy != "0" && performedBy != "")
        {
            if (performedBy != "Select Performer")
            {
                where += " and performedBy = '" + performedBy + "'";
            }
        }

        /*** modified datetime - From Date to Date ***/

        if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // both from/to date
        {
            where += " and actionDateTime > ='" + fromDate + " 00:00:00'  and actionDateTime < ='" + toDate + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate)) // from date only 
        {
            where += " and actionDateTime > ='" + fromDate + " 00:00:00' ";
        }
        else if (string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate)) // to date only 
        {
            where += " and actionDateTime < ='" + toDate + " 23:59:59' ";
        }

        return where;
    }

    /********* Case Verifcation End ***********/
    protected string ConvertToCSV(DataTable dataTable)
    {
        StringBuilder csvbuilder = new StringBuilder();

        foreach (DataColumn dc in dataTable.Columns) 
        {
            csvbuilder.Append(dc.ColumnName + ",");
        }

        csvbuilder.Length--;
        csvbuilder.AppendLine();

        foreach (DataRow dr in dataTable.Rows) 
        {
            foreach(var item in dr.ItemArray)
            {
                string value = item.ToString();

                if (value.Contains(","))
                {
                    value = "\"" + value + "\"";
                }

                csvbuilder.Append(value + ",");
            }
            csvbuilder.Length--;
            csvbuilder.AppendLine();
        }
        return csvbuilder.ToString();
    }

}