﻿<%@ Page Title="Case Verification" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="VerificationStatistics.aspx.cs" Inherits="DynamicGridView.VerificationStatistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>

    <style>
        .tableProp {
            border-collapse: collapse !important;
            border: none;
            width: 65%;
            overflow: auto;
        }

        .tableProp td, .tableProp th {
            border: none;
            font-size: medium !important;
        }

        .tblStatics {
            background-color: white !important;
            border: 1px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        VERIFICATION STATISTICS</h1>
    <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
    <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
    <asp:HiddenField runat="server" ID="hdfName" Value="" />
    <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
    <asp:HiddenField runat="server" ID="isResetClick" Value="" />

    <div class="row pt-1">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item" style="display: grid;">
                <label style="font-weight: 600 !important;">Filter by Facility</label>
                <asp:DropDownList runat="server" ID="ddlFacility" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFacility_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFacility" InitialValue="Select Facility" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Facility is Required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item">
                <label style="font-weight: 600 !important;">Date of Birth Range</label>
                <div class="form-item">
                    <div style="display: flex; align-items: baseline;">
                        <label style="font-weight: 600 !important; margin-right: -45px !important;">From</label>
                        <asp:TextBox runat="server" CssClass="form-control inputCurrentDate" ID="txtFromDate" ValidationGroup="search" onblur="ValidateFromDate(this);" TextMode="Date" />
                    </div>
                    <asp:CustomValidator runat="server" ID="CustomValidator1" ValidationGroup="search" ControlToValidate="txtFromDate" ErrorMessage="Invalid Date." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" EnableClientScript="true" onkeypress="return false;" onpaste="return false;" />
                </div>
                <div class="form-item">
                    <div style="display: flex; align-items: baseline;">
                        <label style="font-weight: 600 !important; margin-right: -45px !important;">To</label>
                        <asp:TextBox runat="server" CssClass="form-control inputCurrentDate" ID="txtToDate" ValidationGroup="search" onblur="ValidateToDate(this);" TextMode="Date" />
                    </div>
                    <asp:CustomValidator runat="server" ID="CustomValidator2" ValidationGroup="search" ControlToValidate="txtToDate" ErrorMessage="Invalid Date." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" EnableClientScript="true" onkeypress="return false;" onpaste="return false;" />
                </div>

            </div>

            <div class="form-item">
                <asp:Button Text="Search" runat="server" ID="btnSearch" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-danger" />
            </div>

            <div class="form-item">
                <asp:Button Text="Reset " runat="server" ID="btnReset" CausesValidation="false" OnClientClick="return resetValidation();" OnClick="btnReset_Click" CssClass="btn btn-danger" />
            </div>

        </div>
    </div>

    <div class="pt-2" id="searchreport" runat="server" visible="false">
        <div class="col col-lg-12 col-sm-12">
            <% if (dt != null && dt.Rows.Count > 0)
                {%>
            <table cellpadding="0" cellspacing="0" id="vfTable" class="pt-1">
                <tr>
                    <td class="cvsTextBoxNotaion" colspan="<%=Convert.ToInt32(dt.Rows.Count+2) %>">For a case report to be considered “Complete,” the Case Report must have a Diagnosis Verification Status assigned to each Q-Code and the Prenatal Congenital Heart Defect (CHD) Indication status must be documented when a CHD diagnosis is present.
                    </td>
                </tr>

                <tr>
                    <td colspan="<%=Convert.ToInt32(dt.Rows.Count+2) %>" class="keepUpperBorder">
                        <h1>
                            <asp:Label ID="lblFacility" runat="server"></asp:Label>
                            <br>
                            Date of Birth Range from <%= formattedFromDate %> to <%= formattedToDate %>
                        </h1>
                    </td>
                </tr>
                <tr style="border: 0px">
                    <td style="border: 0px">
                        <div class="custom-cv-table">
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder">
                                    <h3 class="margin20px">CASE REPORT DETAILS BY VERIFICATION STATUS</h3>
                                </div>
                            </div>
                            <div class="custom-cv-row">
                                <div class="custom-cv-cell">Complete </div>
                                <div class="custom-cv-cell">
                                    <asp:Label ID="lblRCompletePercentage" runat="server" />%

                                </div>
                                <div class="custom-cv-cell">

                                    <asp:Label ID="lblRComplete" runat="server" />
                                    of
                                        <asp:Label ID="lblRCompleteTotal" runat="server" />
                                    Case Reports
                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">Incomplete</div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblInCompletePercentage" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblInComplete" runat="server" />
                                    of
                                <asp:Label ID="lblInCompleteTotal" runat="server" />
                                    Case Reports
                                </div>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder" colspan="<%=Convert.ToInt32(dt.Rows.Count+2) %>">
                                    <h3 class="margin20px">PRENATAL CHD DIAGNOSIS BY STATUS</h3>
                                </div>
                            </div>
                            <div class="custom-cv-row">
                                <div class="custom-cv-cell">Yes</div>
                                <div class="custom-cv-cell">
                                    <asp:Label ID="lblCHDPercentageYes" runat="server" />%
                                </div>
                                <div class="custom-cv-cell">
                                    <asp:Label ID="lblCHDCompletedYes" runat="server" />
                                    of
                                        <asp:Label ID="lblCHDTotalYes" runat="server" />
                                    Diagnosis Codes

                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">
                                    No
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDPercentageNo" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDCompletedNo" runat="server" />
                                    of
                                        <asp:Label ID="lblCHDTotalNo" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">Unknown</div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDPercentageUnknown" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDCompletedUnknown" runat="server" />
                                    of
                                        <asp:Label ID="lblCHDTotalUnknown" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">Not Recorded</div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDPercentageNR" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCHDCompletedNR" runat="server" />
                                    of
                                        <asp:Label ID="lblCHDTotalNR" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder" colspan="<%=Convert.ToInt32(dt.Rows.Count+2) %>">
                                    <h3 class="margin20px">ICD-10 DIAGNOSTIC Q-CODES BY DIAGNOSIS VERIFICATION STATUS</h3>

                                </div>
                            </div>
                            <div class="custom-cv-row">
                                <div class="custom-cv-cell">Confirmed Diagnosis</div>
                                <div class="custom-cv-cell">
                                    <asp:Label ID="lblCDiagnosisPercentage" runat="server" />%
                                </div>
                                <div class="custom-cv-cell">
                                    <asp:Label ID="lblCDiagnosisCompleted" runat="server" />
                                    of
                                        <asp:Label ID="lblCDiagnosisTotal" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">Confirmed Not a Diagnosis</div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCNDiagnosisPercentage" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblCNDiagnosisCompleted" runat="server" />
                                    of
                                        <asp:Label ID="lblCNDiagnosisTotal" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">
                                <div class="custom-cv-cell removeUpperBorder">Unable to Verify Diagnosis</div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblUDiagnosisPercentage" runat="server" />%
                                </div>
                                <div class="custom-cv-cell removeUpperBorder">
                                    <asp:Label ID="lblUDiagnosisCompleted" runat="server" />
                                    of
                                        <asp:Label ID="lblUDiagnosisTotal" runat="server" />
                                    Diagnosis Codes
                                </div>
                            </div>
                        </div>
                    </td>
                    <td style="border: 0px">&nbsp;</td>

                    <td style="border: 0px">
                        <% if (dt.Rows.Count > 1)
                            {  %>
                        <div class="custom-cv-table">
                            <% if (dt.Rows.Count > 1)
                                {  %>
                            <div class="custom-cv-row removeAllBorder">
                                <div style="font-size: 14px !important;" class="custom-cv-cell removeAllBorder" colspan="<%=Convert.ToInt32(dt.Rows.Count - 1) %>"><b>User Details</b></div>
                            </div>
                            <%}%>
                            <div class="custom-cv-row">
                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell"><%= !string.IsNullOrEmpty(Convert.ToString(item[3])) ? Convert.ToString(item[3]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    } %>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {

                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[4])) ? Convert.ToString(item[4]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }

                                    } %>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[5])) ? Convert.ToString(item[5]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder">&nbsp;</div>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder">&nbsp;</div>
                            </div>
                            <div class="custom-cv-row">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell"><%=!string.IsNullOrEmpty(Convert.ToString(item[16])) ? Convert.ToString(item[16]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[17])) ? Convert.ToString(item[17]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[18])) ? Convert.ToString(item[18]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[19])) ? Convert.ToString(item[19]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder">&nbsp;</div>
                            </div>
                            <div class="custom-cv-row removeAllBorder">
                                <div class="custom-cv-cell removeAllBorder">&nbsp;</div>
                            </div>

                            <div class="custom-cv-row">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell"><%=!string.IsNullOrEmpty(Convert.ToString(item[9])) ? Convert.ToString(item[9]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[10])) ? Convert.ToString(item[10]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                            <div class="custom-cv-row removeUpperBorder">

                                <% if (dt.Rows.Count > 1)
                                    {
                                        int i = 0;
                                        foreach (System.Data.DataRow item in dt.Rows)
                                        {
                                            if (i != 0)
                                            {
                                %>
                                <div class="custom-cv-cell removeUpperBorder"><%=!string.IsNullOrEmpty(Convert.ToString(item[11])) ? Convert.ToString(item[11]) : "N/A" %></div>
                                <%}
                                            i++;
                                        }
                                    }%>
                            </div>
                        </div>
                        <%} %>
                    </td>
                </tr>


            </table>

            <%}%>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>


    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
            }
        });

        /*** will trigger search button by enter ***/

        function resetValidation() {

            document.getElementById("<%=txtFromDate.ClientID %>").value = "";
            document.getElementById("<%=txtToDate.ClientID %>").value = "";

            document.getElementById("<%=ddlFacility.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

        }

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

        function hideReport() {
            var $searchReportElement = $("#<%=searchreport.ClientID%>");

            if ($searchReportElement.length) {
                $searchReportElement.hide();
            }
        }

        function ShowProgress() {

            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ValidateToDate(input) {

            var txtFrom = document.getElementById("<%=txtFromDate.ClientID %>").value;
            var txtTo = document.getElementById("<%=txtToDate.ClientID %>").value;

            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");
            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");

            if (txtTo.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtTo === "" || txtTo === 'yyyy-mm-dd') {

                    fromDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                }
                else if (!dateRegex.test(txtTo)) {

                    toDateValidator.style.display = "block";
                    toDateValidator.innerHTML = "Invalid Date";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                }
                else {

                    var minDate = new Date("2018-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtTo);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/2018

                    if (checkDateIfisValid < minDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date must be 01/01/2018 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    if (txtTo !== null) {
                        if (txtFrom.length === 0) {

                            toDateValidator.style.display = "none";
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;

                        } else if (txtTo < txtFrom) {
                            if (!dateRegex.test(txtFrom)) {

                                fromDateValidator.style.display = "block";
                                fromDateValidator.innerHTML = "Invalid Date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                            } else {

                                fromDateValidator.style.display = "none";

                                toDateValidator.style.display = "block";
                                toDateValidator.innerHTML = "Must be before from date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                                return false;
                            }
                        }
                        else {
                            if (enteredChildDOB !== "") {
                                if (!dateRegex.test(enteredChildDOB)) {
                                    validatorChildDOB.style.display = "block";
                                    fromDateValidator.style.display = "none";       // clear the from/to validator message
                                    toDateValidator.style.display = "none";
                                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                    return false;
                                }
                            } else {
                                if (!dateRegex.test(txtFrom)) {

                                    fromDateValidator.style.display = "block";
                                    fromDateValidator.innerHTML = "Invalid Date";

                                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         }
                         else {
                             toDateValidator.style.display = "none";
                             fromDateValidator.style.display = "none";

                             $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                                    return true;
                                }
                            }
                        }
                    }
                    else {

                        if (enteredChildDOB !== "") {
                            if (!dateRegex.test(enteredChildDOB)) {
                                validatorChildDOB.style.display = "block";
                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                                return false;
                            }
                        } else {

                            toDateValidator.style.display = "none";
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                    }
                }
            } else {
                document.getElementById("<%=txtToDate.ClientID%>").value = "";
            }
        }

        function ValidateFromDate(input) {

            var txtFrom = document.getElementById("<%=txtFromDate.ClientID%>").value;
            var txtTo = document.getElementById("<%=txtToDate.ClientID %>").value;

            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");
            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");

            if (txtFrom.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtFrom === "" || txtFrom === 'yyyy-mm-dd') {

                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                }
                else if (!dateRegex.test(txtFrom)) {

                    fromDateValidator.style.display = "block";
                    fromDateValidator.innerHTML = "Invalid Date";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                }
                else {

                    var minDate = new Date("2018-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtFrom);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/2018

                    if (checkDateIfisValid < minDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date must be 01/01/2018 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }


                    var checkDateIfisValidToDate = new Date(txtTo);

                    if (checkDateIfisValidToDate > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    if (txtTo.length > 0) {

                        if (txtFrom > txtTo) {

                            toDateValidator.style.display = "none";

                            fromDateValidator.style.display = "block";
                            fromDateValidator.innerHTML = "Must be before to date";


                            $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                     return false;

                 }
                 else if (!dateRegex.test(txtTo)) {

                     toDateValidator.style.display = "block";
                     toDateValidator.innerHTML = "Invalid Date";
                     $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                 }
                 else {
                     if (enteredChildDOB !== "") {
                         if (!dateRegex.test(enteredChildDOB)) {
                             validatorChildDOB.style.display = "block";
                             fromDateValidator.style.display = "none";       // clear the from/to validator message
                             toDateValidator.style.display = "none";
                             $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                             return false;
                         }
                     }
                     else {
                         fromDateValidator.style.display = "none";
                         toDateValidator.style.display = "none";
                         $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                         return true;
                     }
                 }
             }
             else if (txtFrom <= txtTo) {

                 toDateValidator.style.display = "none";

                 fromDateValidator.style.display = "block";
                 fromDateValidator.innerHTML = "Must be before to date.";
                 $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                 return false;

             }
             else {

                 if (enteredChildDOB !== "") {
                     if (!dateRegex.test(enteredChildDOB)) {
                         validatorChildDOB.style.display = "block";
                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         return false;
                     }
                 }
                 else {
                     fromDateValidator.style.display = "none";
                     toDateValidator.style.display = "none";
                     $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                    }
                }
            } else {
                document.getElementById("<%=txtFromDate.ClientID%>").value = "";
            }
        }

    </script>

    <% } %>
</asp:Content>



