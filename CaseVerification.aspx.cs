﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class CaseVerification : System.Web.UI.Page
    {
        dynamic permissions = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();

        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "CASE_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CheckRole");
            }
            return permissions;
        }
    }
}