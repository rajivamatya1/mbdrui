﻿<%@ Page Title="Facility Report Card" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FacilityReportCard.aspx.cs" Inherits="DynamicGridView.FacilityReportCard" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <%--  <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
    <script src="Content/js/alert.js"></script>--%>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <div>
        <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
        <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
        <asp:HiddenField runat="server" ID="hdfName" Value="" />
        <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
        <asp:HiddenField runat="server" ID="isResetClick" Value="" />
    </div>
    <div>
        <h1 class="page-title">
            <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
            FACILITY REPORT CARD
             <asp:Button ID="btnFacDash" runat="server" Text="Return To Facility Dashboard" class="rightAlignment" OnClick="btnFacDash_Click" />
        </h1>
    </div>
    
    <div class="radio-group">

            <label class="radio-label">
                <asp:RadioButton runat="server" ID="rdMonth" AutoPostBack="true" OnCheckedChanged="rdMonth_CheckedChanged" Checked="true" />
                <span>Month</span>
            </label>

            <label class="radio-label">
                <asp:RadioButton runat="server" ID="rdQuarter" AutoPostBack="true" OnCheckedChanged="rdQuarter_CheckedChanged" />
                <span>Quarter</span>
            </label>

            <label class="radio-label" >
                <asp:RadioButton runat="server" ID="rdAnnual" AutoPostBack="true" OnCheckedChanged="rdAnnual_CheckedChanged" />
                <span>Annual</span>
            </label>

        </div>

    <div class="row">

        <div class="col col-lg-12 col-sm-12">
            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Submitter/Organization</label>
                <asp:DropDownList runat="server" ID="ddlOrg" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlOrg_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlOrg" InitialValue="Select Submitter/Organization" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Organization is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>
            <div class="form-item displayGrid">
                <label class="fontDDSize">Select Facility</label>
                <asp:DropDownList runat="server" ID="ddlFacility" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlFacility_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFacility" InitialValue="Select Facility" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Facility is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item">
                <label class="fontDDSize"></label>
                <asp:Button Text="View Official Report" Visible="false" runat="server" ID="btnPrint" OnClick="btnPrint_Click" CssClass="btn btn-danger printCVS" />
            </div>
        </div>

        <div class="col col-lg-12 col-sm-12 pt-1">
            <div class="form-item displayGrid">
                <label class="fontDDSize">Discharge Year</label>
                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlYear" InitialValue="Select Year" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Year is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item displayGrid" id="divMonth" runat="server" visible="true">
                <label class="fontDDSize">Discharge Month</label>
                <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control fieldRadius" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlMonth" InitialValue="Select Month" ValidationGroup="search" EnableClientScript="true" ErrorMessage="Month is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>
            <div class="form-item displayGrid" id="divFile" runat="server" visible="true">
                <label class="fontDDSize" style="width: 125px;">Select File</label>
                <asp:DropDownList runat="server" ID="ddlFile" CssClass="form-control fieldRadius">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlFile" InitialValue="Select File" ValidationGroup="search" EnableClientScript="true" ErrorMessage="File is required." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item displayGrid" id="divQuarter" runat="server" visible="false">
                <label class="fontDDSize" style="width: 125px;">Discharge Quarter</label>
                <asp:DropDownList runat="server" ID="ddlQuarter" CssClass="form-control fieldRadius">
                    <asp:ListItem Value="-1" Text="Select Quarter">Select Quarter </asp:ListItem>
                    <asp:ListItem Value="1" Text="1st Quarter (Jan.-Mar.)">1st Quarter (Jan-Mar)  </asp:ListItem>
                    <asp:ListItem Value="2" Text="2nd Quarter (Apr.-Jun.)">2nd Quarter (Apr-Jun) </asp:ListItem>
                    <asp:ListItem Value="3" Text="3rd Quarter (Jul.-Sept.)">3rd Quarter (Jul-Sept) </asp:ListItem>
                    <asp:ListItem Value="4" Text="4th Quarter (Oct.-Dec.)">4th Quarter (Oct-Dec)</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="form-item">
                <label class="fontDDSize"></label>
                <asp:Button Text="Submit" runat="server" ID="btnSearch" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-danger" />
            </div>

            <div class="form-item">
                <label class="fontDDSize"></label>
                <asp:Button Text="Reset " runat="server" ID="btnReset" CausesValidation="false" OnClientClick="return resetValidation();" OnClick="btnReset_Click" CssClass="btn btn-danger" />
            </div>
        </div>

    </div>
    <br>
    <br>
    <div class="pt-2" id="searchreport" runat="server" visible="false">
        <div class="col col-lg-12 col-sm-12">
            <% if (dtFRCStats != null && dtFRCStats.Rows.Count > 0)
                {            %>
  
            <br />
            <% if (rdMonth.Checked)
                { %>
            <table cellpadding="0" cellspacing="0" id="tableFRCDetails" border="1" class="facilityTable secondtablebackgroud">
                <tr class="secondtablefirsttr">
                    <%-- <td colspan="2"><%=ddlYear.SelectedValue %> Facility Report Card For: </td>--%>
                    <td colspan="2">Facility Report Card For: </td>
                </tr>
                <tr class="secondtablefirsttr">
                    <td colspan="2">
                        <% if (ddlOrg.SelectedIndex != 0 && ddlFacility.SelectedIndex == 1)
                            {%>

                        <%=dtFRCStats.Rows[0]["submitter"] %>
                        <%}
                            else
                            {%>
                        <%=ddlFacility.SelectedItem.Text%>
                        <%}%>
                    </td>
                </tr>
                <%-- add factility code here once available--%>

                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">Year</td>
                    <td><%=ddlYear.SelectedValue %></td>
                </tr>
                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">Month</td>
                    <td><%= System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(dtFRCStats.Rows[0]["month"])) %> </td>
                </tr>
                <tr class="secondtablefacilitytr">
                    <td class="commonwidth">File Name</td>
                    <td><%=dtFRCStats.Rows[0]["fileName"] %></td>
                </tr>
                <tr style="background-color: white">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>File Created Date</td>
                    <td><%=dtFRCStats.Rows[0]["fileCreatedDate"] %></td>
                </tr>
                <tr>
                    <td>Days Early/Late Of Submission</td>
                    <td><%=dtFRCStats.Rows[0]["daysEarlyOrLateOfSubmission"] %></td>
                </tr>
                <tr>
                    <td>Load Date</td>
                    <td><%=dtFRCStats.Rows[0]["loadDate"] %></td>
                </tr>
                <tr>
                    <td>Date Of Last File (If Delinquent)</td>
                    <td><%=dtFRCStats.Rows[0]["dateOfLastFileIfDelinquent"] %></td>
                </tr>
                <tr>
                    <td>Days Since Last File (If Delinquent)</td>
                    <td><%=dtFRCStats.Rows[0]["daysSinceLastFileIfDelinquent"] %></td>
                </tr>
                <tr>
                    <td>Reports Count</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCount"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Prior Month</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountPriorMonth"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Monthly Avg</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountMonthlyAvg"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Percent Change Vs Monthly Avg</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountPercentChangeVsMonthlyAvg"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Invalid</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountInvalid"] %></td>
                </tr>
                <tr>
                    <td>Reports Invalid Percent
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsInvalidPercent"] %></td>
                </tr>
                <tr>
                    <td>Data Errors Count
                    </td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsCount"] %></td>
                </tr>
                <tr>
                    <td>Data Errors Percent
                    </td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsPercent"] %></td>
                </tr>
                <tr>
                    <td>Reports Count Duplicates
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountDuplicates"] %></td>
                </tr>
                <tr>
                    <td>Reports Count VERA
                    </td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountVERA"] %></td>
                </tr>
                <tr>
                    <td>File Load Failure</td>
                    <td><%=dtFRCStats.Rows[0]["fileLoadFailure"] %></td>
                </tr>
                <tr>
                    <td colspan="2"><b>Comments</b>
                        <input type="button" id="chkSelectAll" onclick="SelectAll();" value="Select All" class="hideDisable"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField ID="hdauditUpdate" runat="server" />
                        <table cellpadding="0" cellspacing="0" id="tblFrcComment">

                            <%  if (dtFRCStatsComment != null)
                                {
                                    foreach (System.Data.DataRow dr in dtFRCStatsComment.Rows)
                                    {%>
                            <tr>

                                <%--adding a col for checkbox--%>                           

                                <%
                                    if (Convert.ToString(dr["auditId"]) == Convert.ToString(hdauditId.Value) && hdauditUpdate.Value == "false")
                                    {

                                %>
                                <td class="propertyFRCCommenttable">
                                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" CssClass="comment" MaxLength="4000" onkeyup="UpdateCharaterCount(this);" />
                                    <br>
                                    <asp:Label ID="lblCount" runat="server" CssClass="rightAlignment" Text="0/4000" />
                                    <asp:HiddenField ID="hdauditId" runat="server" />
                                </td>
                                <td class="propertyFRCCommenttable"><%=dr["created"] %>
                                    <asp:Button ID="btnComment" OnClick="btnComment_Click" Text="Save" CssClass="cursor-default" Enabled="false" runat="server" />
                                    <br>
                                    <%=dr["displayName"] %>
                                </td>
                                <%}
                                    else
                                    {%>
                                <td class="propertyFRCCommenttable"><%=dr["comment"] %> <%--<asp:Label ID="lbl" runat="server" Text='<%#Eval("comment") %>' />--%></td>
                                <td class="propertyFRCCommenttable"><%=dr["created"] %>
                                    <br />
                                    <%=dr["displayName"] %></td>
                                <%}%>
                            </tr>
                            <%}
                                }
                            %>
                        </table>

                    </td>
                </tr>

            </table>

            <%} %>

            <%  else if (rdQuarter.Checked)
                { %>
            <center>
           <div class="center-text">
            <table cellpadding="0" cellspacing="0" id="tableFRCQuerterDetails" border="1" class="facilityTable secondtablebackgroud">
                
                <tr class="secondtablefirsttr">
                    <td class="boldFont"><%=ddlFacility.SelectedItem.Text%></td>
                    <td colspan="4" class="boldFont"><%=ddlQuarter.SelectedItem.Text.Replace(".", "")%> <%= ddlYear.SelectedItem.Text%></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="boldFont"><%=dtFRCStats.Rows[0]["monthDisplay"] %></td>
                    <td class="boldFont"><%=dtFRCStats.Rows[1]["monthDisplay"] %></td>
                    <td class="boldFont"><%=dtFRCStats.Rows[2]["monthDisplay"] %></td>
                    <td class="boldFont"><%=dtFRCStats.Rows[3]["monthDisplay"] %></td>
                    
                </tr>
                <tr>
                    <td class="left-align">File Submission Timeliness</td>
                    <td><%=dtFRCStats.Rows[0]["fileTimeliness"] %></td>
                    <td><%=dtFRCStats.Rows[1]["fileTimeliness"] %></td>
                    <td><%=dtFRCStats.Rows[2]["fileTimeliness"] %></td>
                    <td class="greyCell">n/a</td>

                
                </tr>
                <tr>
                    <td class="left-align">File Submissions (N)</td>
                    <td><%=dtFRCStats.Rows[0]["fileSubmissions"] %></td>
                    <td><%=dtFRCStats.Rows[1]["fileSubmissions"] %></td>
                    <td><%=dtFRCStats.Rows[2]["fileSubmissions"] %></td>
                    <td><%=dtFRCStats.Rows[3]["fileSubmissions"] %></td>
                </tr>
                <tr>
                    <td class="left-align">File Load Failures (N)</td>
                    <td><%=dtFRCStats.Rows[0]["fileLoadFailures"] %></td>
                    <td><%=dtFRCStats.Rows[1]["fileLoadFailures"] %></td>
                    <td><%=dtFRCStats.Rows[2]["fileLoadFailures"] %></td>
                    <td><%=dtFRCStats.Rows[3]["fileLoadFailures"] %></td>
                </tr>
                <tr>
                    <td class="left-align">Reports (N)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCount"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsCount"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsCount"] %></td>
                    <td><%=dtFRCStats.Rows[3]["reportsCount"] %></td>
                </tr>
                <tr>

                    <td class="left-align">Reports Monthly Average (N) (12 month rolling)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountMonthlyAvg"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsCountMonthlyAvg"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsCountMonthlyAvg"] %></td>
                    <td class="greyCell">n/a</td>
                </tr>
                <tr>
                    <td class="left-align">Reports Volume Change vs Monthly Average (%)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsPercentChangeVsMonthlyAvg"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsPercentChangeVsMonthlyAvg"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsPercentChangeVsMonthlyAvg"] %></td>
                    <td class="greyCell">n/a</td>
                </tr>
                <tr>
                    <td class="left-align">Reports Invalid (N)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountInvalid"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsCountInvalid"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsCountInvalid"] %></td>
                    <td><%=dtFRCStats.Rows[3]["reportsCountInvalid"] %></td>
                </tr>
                <tr>
                    <td class="left-align">Reports Invalid (%)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsInvalidPercent"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsInvalidPercent"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsInvalidPercent"] %></td>
                    <td><%=dtFRCStats.Rows[3]["reportsInvalidPercent"] %></td>
                </tr>
                <tr>

                    <td class="left-align">Reports Duplicated (N)</td>
                    <td><%=dtFRCStats.Rows[0]["reportsCountDuplicates"] %></td>
                    <td><%=dtFRCStats.Rows[1]["reportsCountDuplicates"] %></td>
                    <td><%=dtFRCStats.Rows[2]["reportsCountDuplicates"] %></td>
                    <td><%=dtFRCStats.Rows[3]["reportsCountDuplicates"] %></td>
                </tr>
                <tr>

                    <td class="left-align">Data Errors (N)</td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsCount"] %></td>
                    <td><%=dtFRCStats.Rows[1]["dataErrorsCount"] %></td>
                    <td><%=dtFRCStats.Rows[2]["dataErrorsCount"] %></td>
                    <td><%=dtFRCStats.Rows[3]["dataErrorsCount"] %></td>
                </tr>
                <tr>

                    <td class="left-align">Data Errors (%)</td>
                    <td><%=dtFRCStats.Rows[0]["dataErrorsPercent"] %></td>
                    <td><%=dtFRCStats.Rows[1]["dataErrorsPercent"] %></td>
                    <td><%=dtFRCStats.Rows[2]["dataErrorsPercent"] %></td>
                    <td><%=dtFRCStats.Rows[3]["dataErrorsPercent"] %></td>
                </tr>
            </table></div>
            <%}
                else
                { %>
                   <table cellpadding="0" cellspacing="0" border="1" style="margin: 0 auto; text-align: center !important; width: 55% !important; margin-top: 4px;">
                           <tr class="secondtablefirsttr">
                               <td class="boldFont"><%=ddlFacility.SelectedItem.Text%></td>
                               <td colspan="5" class="boldFont"><%= ddlYear.SelectedItem.Text%></td>
                           </tr>
                            <tr>
                                <td></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[0]["quarterDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[1]["quarterDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[2]["quarterDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[3]["quarterDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[4]["quarterDisplay"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">File Submission Timeliness</td>

                                <td><%= string.IsNullOrWhiteSpace(Convert.ToString(dtFRCStats.Rows[0]["fileTimeliness"]).Trim()) ? "" : Convert.ToString(dtFRCStats.Rows[0]["fileTimeliness"]) %></td>
                                <td><%= string.IsNullOrWhiteSpace(Convert.ToString(dtFRCStats.Rows[1]["fileTimeliness"]).Trim()) ? "" : Convert.ToString(dtFRCStats.Rows[1]["fileTimeliness"]) %></td>
                                <td><%= string.IsNullOrWhiteSpace(Convert.ToString(dtFRCStats.Rows[2]["fileTimeliness"]).Trim()) ? "" : Convert.ToString(dtFRCStats.Rows[2]["fileTimeliness"]) %></td>
                                <td><%= string.IsNullOrWhiteSpace(Convert.ToString(dtFRCStats.Rows[3]["fileTimeliness"]).Trim()) ? "" : Convert.ToString(dtFRCStats.Rows[3]["fileTimeliness"]) %></td>
                                <td class="greyCell">n/a</td>

                            </tr>
                            <tr>
                                <td class="left-align">File Submissions (N)</td>
                                <td><%=dtFRCStats.Rows[0]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[1]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[2]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[3]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[4]["fileSubmissions"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">File Load Failures (N)</td>
                                <td><%=dtFRCStats.Rows[0]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[1]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[2]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[3]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[4]["fileLoadFailures"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCount"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Reports Quarterly Average (N) (1 year rolling)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountQuarterlyAvg"] %></td>
                                <td class="greyCell">n/a</td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Volume Change vs Quarterly Average (%)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsPercentChangeVsQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsPercentChangeVsQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsPercentChangeVsQuarterlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsPercentChangeVsQuarterlyAvg"] %></td>
                                <td class="greyCell">n/a</td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Invalid (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCountInvalid"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Invalid (%)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsInvalidPercent"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Reports Duplicated (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCountDuplicates"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Data Errors (N)</td>
                                <td><%=dtFRCStats.Rows[0]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[1]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[2]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[3]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[4]["dataErrorsCount"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Data Errors (%)</td>
                                <td><%=dtFRCStats.Rows[0]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[1]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[2]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[3]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[4]["dataErrorsPercent"] %></td>
                            </tr>
                        </table>
            <% } %>

            <% }
                else
                { %>
            <tr>
                <td colspan="4"></td>
            </tr>
            <%}%>
            <br>
            <br>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div id="commonMsg">
        <asp:HiddenField ID="hdCheckIds" runat="server" />
    </div>



    <script type="text/javascript">

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
                    }
                });

        /*** will trigger search button by enter ***/

        function SelectAll() {

            var inputElements = document.getElementsByClassName('chkHeader');
            var inputId = document.getElementById('chkSelectAll');
            if (inputId.value == "Select All") {
                for (var i = 0; i < inputElements.length; i++) {
                    inputElements[i].checked = true;
                }

                inputId.value = "UnSelect All";
            } else {
                for (var i = 0; i < inputElements.length; i++) {
                    inputElements[i].checked = false;
                }

                inputId.value = "Select All";
            }
            checkBoxComment();
            return false;
        }

        function chkStatusMaintain(ids) {
            const Id = ids.split(",")
            var inputElements = document.getElementsByClassName('chkHeader');
            for (var j = 0; j < Id.length; j++) {
                for (var i = 0; i < inputElements.length; i++) {
                    if (inputElements[i].value == Id[j]) {
                        inputElements[i].checked = true;
                    }
                }
            }
        }

        function checkBoxComment() {
            var hdCheck = document.getElementById("<%=hdCheckIds.ClientID%>");
            hdCheck.value = "";
            var inputElements = document.getElementsByClassName('chkHeader');
            var hiddenvalue = "";

            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    if (!hiddenvalue) {
                        hiddenvalue = inputElements[i].value;
                    } else {
                        hiddenvalue = hiddenvalue + "," + inputElements[i].value;
                    }
                }
            }
            hdCheck.value = hiddenvalue;
        }

        function UpdateCharaterCount(textBox) {
            var maxLength = 4000;
            var currentLength = maxLength - textBox.value.length;
            var charLeft = maxLength - currentLength;
            document.getElementById('<%=lblCount.ClientID %>').innerText = charLeft + '/' + maxLength;

            var btnSave = document.getElementById('<%=btnComment.ClientID%>');

            if (charLeft > 0) {
                btnSave.removeAttribute('disabled');
                btnSave.removeAttribute('class');
                btnSave.classList.add('cursor-pointer');
            }
            else {
                btnSave.setAttribute('disabled', 'true');
                btnSave.removeAttribute('class');
                btnSave.classList.add('cursor-default');
            }
        }

        function resetValidation()
        {
            document.getElementById("<%=ddlFacility.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

        }

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }


        function hideReport() {
            var $searchReportElement = $("#<%=searchreport.ClientID%>");

            if ($searchReportElement.length) {
                $searchReportElement.hide();
            }
        }

        function ShowProgress() {
            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

    </script>

    <% } %>
</asp:Content>



