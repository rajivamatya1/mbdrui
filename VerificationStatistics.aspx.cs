﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
namespace DynamicGridView
{
    public partial class VerificationStatistics : System.Web.UI.Page
    {
        protected string fromDate;
        protected string toDate;
        protected string formattedFromDate;
        protected string formattedToDate;
        protected DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }

                txtFromDate.Attributes["min"] = new DateTime(2018, 1, 1).ToString("yyyy-MM-dd");
                txtFromDate.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");

                txtToDate.Attributes["min"] = new DateTime(2018, 1, 1).ToString("yyyy-MM-dd");
                txtToDate.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");

                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";

                if (!IsPostBack)
                {
                    BindColumnsDropDown(schema, tableName);
                }
                else
                {
                    dt = (DataTable)Session["verificationDT"];
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_Page_Load");
            }
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            try
            {
                /******************** Facility DropDown ***************************/
              
                DataTable dtEntityList = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";

                    queryList = "SELECT DISTINCT admittingEntity, details FROM MBDR_System.CaseVerificationFacilities";

                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtEntityList);
                    ddlFacility.DataSource = dtEntityList;
                    ddlFacility.DataTextField = "details";
                    ddlFacility.DataValueField = "admittingEntity";
                    ddlFacility.DataBind();
                    ddlFacility.Items.Insert(0, "Select Facility");
                    ddlFacility.Items.Insert(1, "All Facilities");
                    if (!string.IsNullOrEmpty(admittingEntity))
                    {
                        ListItem selectedItem = ddlFacility.Items.FindByValue(admittingEntity);
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_BindColumnsDropDown");
            }
        }

        public void BindGrid()
        {
            try
            {
                dt = new DataTable();
                string sortExp = string.Empty;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string funName = "";
                    string schema = "MBDR_System";
                    string admittingEntity = string.Empty; 

                    string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                    if (selectedadmittingEntity != "Select Facility")
                    {
                        admittingEntity = selectedadmittingEntity;
                    }

                    if (txtFromDate.Text == "")
                    {
                        txtFromDate.Text = "2018-01-01";
                    }

                    if (txtToDate.Text == "")
                    {
                        txtToDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                    }

                    fromDate = txtFromDate.Text;
                    formattedFromDate = DateTime.Parse(fromDate).ToString("MMMM d, yyyy");

                    toDate = txtToDate.Text;
                    formattedToDate = DateTime.Parse(toDate).ToString("MMMM d, yyyy");

                    if (selectedadmittingEntity == "All Facilities")
                    {
                        funName = "getCaseVerificationStatsAll";
                        query = string.Format("SELECT * FROM {0}.{1}('{2}','{3}')", schema, funName, fromDate, toDate);
                    }
                    else
                    {
                        funName = "getCaseVerificationStatsWithUsers";
                        query = string.Format("SELECT * FROM {0}.{1}({2},'{3}','{4}') order by userid asc", schema, funName, admittingEntity, fromDate, toDate);
                    }

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    conn.Close();

                    if (dt.Rows.Count > 0)
                    {
                        Session["verificationDT"] = dt;   

                        searchreport.Visible = true;

                        if (selectedadmittingEntity == "All Facilities")
                        {
                            lblFacility.Text = "All Facilities";
                        }
                        else if (selectedadmittingEntity != "Select Facility")
                        {
                            lblFacility.Text = Convert.ToString(dt.Rows[0]["facility"]);
                        }

                        /*** ----------------- Case Report Details by verification status ----------------- ***/

                        // For Complete

                        lblRCompletePercentage.Text = Convert.ToString(dt.Rows[0]["reportsCompletePercent"]);
                        lblRComplete.Text = Convert.ToString(dt.Rows[0]["reportsComplete"]);
                        lblRCompleteTotal.Text = Convert.ToString(dt.Rows[0]["reportsTotal"]);

                        // For InComplete

                        lblInCompletePercentage.Text = Convert.ToString(dt.Rows[0]["reportsIncompletePercent"]);
                        lblInComplete.Text = Convert.ToString(dt.Rows[0]["reportsIncomplete"]);
                        lblInCompleteTotal.Text = Convert.ToString(dt.Rows[0]["reportsTotal"]);


                        /*** ----------------- ICD-10 DIAGNOSTIC Q-CODES BY DIAGNOSIS VERIFICATION STATUS ----------------- ***/

                        // Confirmed Diagnosis

                        lblCDiagnosisPercentage.Text = Convert.ToString(dt.Rows[0]["diagsConfirmPercent"]);
                        lblCDiagnosisCompleted.Text = Convert.ToString(dt.Rows[0]["diagsConfirm"]);
                        lblCDiagnosisTotal.Text = Convert.ToString(dt.Rows[0]["diagsTotal"]);

                        // Confirmed Not a Diagnosis

                        lblCNDiagnosisPercentage.Text = Convert.ToString(dt.Rows[0]["diagsNotPercent"]);
                        lblCNDiagnosisCompleted.Text = Convert.ToString(dt.Rows[0]["diagsNot"]);
                        lblCNDiagnosisTotal.Text = Convert.ToString(dt.Rows[0]["diagsTotal"]);

                        // Unable to Verify Diagnosis

                        lblUDiagnosisPercentage.Text = Convert.ToString(dt.Rows[0]["diagsUnablePercent"]);
                        lblUDiagnosisCompleted.Text = Convert.ToString(dt.Rows[0]["diagsUnable"]);
                        lblUDiagnosisTotal.Text = Convert.ToString(dt.Rows[0]["diagsTotal"]);

                        /***   ----------------- PRENATAL CHD DIAGNOSIS BY STATUS -----------------   ***/

                        //Yes
                        lblCHDPercentageYes.Text = Convert.ToString(dt.Rows[0]["chdYesPercent"]);
                        lblCHDCompletedYes.Text = Convert.ToString(dt.Rows[0]["chdYes"]);
                        lblCHDTotalYes.Text = Convert.ToString(dt.Rows[0]["chdTotal"]);

                        //No
                        lblCHDPercentageNo.Text = Convert.ToString(dt.Rows[0]["chdNoPercent"]);
                        lblCHDCompletedNo.Text = Convert.ToString(dt.Rows[0]["chdNo"]);
                        lblCHDTotalNo.Text = Convert.ToString(dt.Rows[0]["chdTotal"]);

                        // Unknown
                        lblCHDPercentageUnknown.Text = Convert.ToString(dt.Rows[0]["chdUnknownPercent"]);
                        lblCHDCompletedUnknown.Text = Convert.ToString(dt.Rows[0]["chdUnknown"]);
                        lblCHDTotalUnknown.Text = Convert.ToString(dt.Rows[0]["chdTotal"]);

                        // Not Recorded
                        lblCHDPercentageNR.Text = Convert.ToString(dt.Rows[0]["chdNotRecordedPercent"]);
                        lblCHDCompletedNR.Text = Convert.ToString(dt.Rows[0]["chdNotRecorded"]);
                        lblCHDTotalNR.Text = Convert.ToString(dt.Rows[0]["chdTotal"]);

                    }
                    else
                    {
                        searchreport.Visible = false;

                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' No results found.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_btnSearch_Click");
            }
        }

        protected void ddlFacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = "";
                txtToDate.Text = "";
                searchreport.Visible = false;
                Session["verificationDT"] = null;
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_ddlFacility_SelectedIndexChanged");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Session["verificationDT"] = null;
                ddlFacility.SelectedIndex = 0;

                Response.Redirect("VerificationStatistics.aspx?schema=MBDR_System&table=CaseVerificationStats", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationStatistics_btnReset_Click");
            }
        }

    }
}