﻿<%@ Page Title="Candidate Death Record Search" Language="C#" MasterPageFile="~/loginMaster.master" AutoEventWireup="true" CodeFile="DeathRecordsSearchView.aspx.cs" Inherits="DeathRecordsSearchView" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
            <asp:HiddenField runat="server" ID="hdfTableName" Value="" />
            <asp:HiddenField runat="server" ID="isResetClick" Value="" />
            <asp:HiddenField runat="server" ID="hdfSearchType" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="Candidate Death Records" runat="server" /></h1>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div id="dvAdvanceSearchView" runat="server">

                <div class="row">
                    <div class="form-item">
                        <label for="Content_txtchdLN">Child Last Name </label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtchdLN" ValidationGroup="search" TextMode="SingleLine" />
                        <asp:RegularExpressionValidator ControlToValidate="txtchdLN" ID="RegularExpressionValidator2" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <label for="Content_txtchdFN">Child First Name </label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtchdFN" ValidationGroup="search" TextMode="SingleLine" />
                        <asp:RegularExpressionValidator ControlToValidate="txtchdFN" ID="RegularExpressionValidator3" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <label for="Content_txtChildDOB">DOB </label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtChildDOB" TextMode="Date" ValidationGroup="search" onblur="ValidateDate(this);" EnableViewState="true" />
                        <asp:CustomValidator runat="server" ID="CustomValidator3" ValidationGroup="search" ControlToValidate="txtChildDOB" EnableClientScript="true" ErrorMessage="Invalid DOB." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" onkeypress="return false;" onpaste="return false" />
                    </div>
                    <div class="form-item">
                        <label for="Content_txtMomLN">Mother Last Name </label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtMomLN" ValidationGroup="search" TextMode="SingleLine" />
                        <asp:RegularExpressionValidator ControlToValidate="txtMomLN" ID="RegularExpressionValidator9" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <label for="Content_txtMomFN">Mother First Name</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtMomFN" ValidationGroup="search" TextMode="SingleLine" />
                        <asp:RegularExpressionValidator ControlToValidate="txtMomFN" ID="RegularExpressionValidator10" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Required 1 character." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <asp:Button Text="Search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" OnClientClick="return ShowProgress();" CssClass="btn btn-success" ValidationGroup="search" />
                    </div>
                    <div class="form-item">
                        <asp:Button Text="Reset" runat="server" ID="btnReset" OnClientClick="resetValidation()" CssClass="btn btn-danger" />
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <b>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label></b>
            </div>
        </div>
    </div>
    <div class="row-view-matching">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true" OnRowCommand="gvDynamic_RowCommand"
                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>

    <% } %>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <script>       

        function resetValidation() {

            document.getElementById("<%=txtchdLN.ClientID %>").value = "";
            document.getElementById("<%=txtchdFN.ClientID %>").value = "";
            document.getElementById("<%=txtChildDOB.ClientID %>").value = "";
            document.getElementById("<%=txtMomLN.ClientID %>").value = "";
            document.getElementById("<%=txtMomFN.ClientID %>").value = "";
            <%--document.getElementById("<%=txtadd1.ClientID %>").value = "";--%>


            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

            var gridView = document.getElementById("<%= gvDynamic.ClientID %>");
            var pageIndex = 0;
            gridView.PageIndex = pageIndex;
        }

        /* using as common dialogbox for all not only noRecFound */
        function noRecFound(message, titleFor) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

        function ValidateDate(input) {

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;

           var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

           if (enteredChildDOB.length === 10) {
               if (enteredChildDOB === "undefined" || enteredChildDOB === null || enteredChildDOB === "") {
                   enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
        }

        var currentYear = new Date().getFullYear();

        var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

        if (enteredChildDOB === "" || enteredChildDOB === 'yyyy-mm-dd') {
            validatorChildDOB.style.display = "none";
            $("#<%=btnSearch.ClientID%>").prop("disabled", false);
        }
        else if (!dateRegex.test(enteredChildDOB)) {
            validatorChildDOB.style.display = "block";
            $("#<%=btnSearch.ClientID%>").prop("disabled", true);
            return false;
        }
        else {

            var minDate = new Date("1900-01-01");
            // parse the date

            var checkDateIfisValid = new Date(enteredChildDOB);

            //check if it is validate date or not

            if (isNaN(checkDateIfisValid.getTime())) {
                validatorChildDOB.style.display = "block";
                validatorChildDOB.innerHTML = "Invalid Date";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                return false;
            }

            //check if the date is before 01/01/1900

            if (checkDateIfisValid < minDate) {
                validatorChildDOB.style.display = "block";
                validatorChildDOB.innerHTML = "Date must be 01/01/1900 or later.";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                return false;
            }

            //check if the date is after today's date

            var maxDate = new Date();

            if (checkDateIfisValid > maxDate) {
                validatorChildDOB.style.display = "block";
                validatorChildDOB.innerHTML = "Date cannot be in the future.";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                return false;
            }

            validatorChildDOB.style.display = "none";
            $("#<%=btnSearch.ClientID%>").prop("disabled", false);
        }
    }
    else {
               document.getElementById("<%=txtChildDOB.ClientID%>").value = "";
           }
       }

        function ShowProgress() {
            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnAdvanceSearch").prop("disabled", true);
            }
            else {

                $("#btnAdvanceSearch").prop("disabled", false);

                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

    </script>
</asp:Content>
