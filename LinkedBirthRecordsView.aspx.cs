﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class LinkedBirthRecord : System.Web.UI.Page
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string masterRecordNumber = !string.IsNullOrEmpty(Request.QueryString["BirthRecNo"]) ? Convert.ToString(Request.QueryString["BirthRecNo"]) : "";
            if (!IsPostBack)
            {
                var lstFilter = Common.GetFilterList();
                var model = lstFilter.FirstOrDefault(s => s.Table.StartsWith(tableName) && s.Schema == schema);

                if (model != null)
                {
                    ltrTableName.Text = "Birth Record Details";
                }

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    BindGrid(tableName, schema, masterRecordNumber);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "LinkedBirthRecord_Page_Load");
        }
    }

    public void BindGrid(string tableName, string schema, string masterRecordNumber)
    {
        try
        {
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();


            if (model != null)
            {
                string includeColumns = model.IncludedColumns;
                string readonlyColumns = model.ReadonlyColumns;
                string dataKeyname = model.DataKeyName;


                List<string> lstColumns = includeColumns.Split(',').ToList();

                DataTable dtActualRow = new DataTable();
                DataSet dsRows = new DataSet();


                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = $"SELECT * FROM {schema}.{tableName} WHERE masterRecordNumber = @masterRecordNumber";

                    using (var cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@masterRecordNumber", masterRecordNumber);
                        cmd.CommandTimeout = 0;
                        conn.Open();

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(dsRows);
                        }
                    }
                }


                dtActualRow = dsRows.Tables[0];

                DataSet dsTables = new DataSet();
                dt = new DataTable();
                dt.Columns.Add("Columns");
                dt.Columns.Add(model.Table.Split(':')[1]);

                List<ColumnList> lstRecord = new List<ColumnList>();

                List<string> lstNavigation = new List<string>();


                if (dtActualRow.Rows.Count > 0)
                {
                    foreach (var column in lstColumns)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {

                            foreach (DataColumn item in dtActualRow.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = column.Contains("->") ? column.Split('>')[1] : column;

                                DataRow dr = dt.NewRow();
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                    string displayColname = string.Empty;
                                    if (!string.IsNullOrEmpty(colName))
                                    {
                                        dr[0] = columnInfo;
                                        displayColname = columnInfo;
                                    }
                                    else
                                    {
                                        dr[0] = item.ColumnName;
                                        displayColname = item.ColumnName;
                                    }
                                    if (displayColname != "RecordFrom")
                                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]) });
                                    dr[model.Table.Split(':')[1]] = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]);
                                    for (int j = 0; j < lstNavigation.Count; j++)
                                    {
                                        try
                                        {
                                            string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(lstNavigation[j])).FirstOrDefault().ColumnMapping;
                                            if (colMapping.Contains(item.ColumnName))
                                            {
                                                string strName = Common.GetBetween(colMapping, item.ColumnName, ",");
                                                dr[lstNavigation[j]] = Convert.ToString(dsTables.Tables[lstNavigation[j]].Rows[0][strName.Split(':')[1]]);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dr[lstNavigation[j]] = "";
                                        }
                                    }
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }
            }
          
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "LinkedBirthRecord_BindGrid");
        }
    }


    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#e3f4ff");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "LinkedBirthRecord_gvDynamic_RowDataBound");
        }
    }
}