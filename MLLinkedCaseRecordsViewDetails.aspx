﻿<%@ Page Title=" Linked Case Records View Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="MLLinkedCaseRecordsViewDetails.aspx.cs" Inherits="MLLinkedCaseRecordsViewDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #primary-icon, #divGreen, #divRed, div.logo, img.bell-icon, #cVer
        {
         display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <% if (Session["username"] != null)
        { %>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Literal Text="" ID="ltrTableName" runat="server" />
            </h1>
        </div>
    </div>


    <div class="program-description">
       <%-- <div class="admin-workarea">--%>
            <div class="row row-no-padding">
                <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                    <div class="tableFixHead">
                        <asp:GridView CssClass="table table-bordered table-striped table-fixed" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                            OnRowDataBound="gvDynamic_RowDataBound" EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" ValidateRequestMode="Disabled">
                        </asp:GridView>
                    </div>
                </div>
            </div>
       <%-- </div>--%>
    </div>
    <% } %>

    <script>
        setTimeout(function () {
            window.close();
        }, 600000);
    </script>

</asp:Content>

