﻿<%@ Page Title="MBDR UI Home" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        List of Tables</h1>
    <asp:Label ID="lblUGID" runat="server" />
    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "RECORDS_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/CaseRecordsView.aspx?schema=MBDR_System&table=CaseRecordsView">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Case Records
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of all existing case records. 
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Records
                </div>
            </div>
            <div class="program-description">
                List of all existing case records. 
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Records
                </div>
            </div>
            <div class="program-description">
                List of all existing case records. 
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "REPORTS_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/CaseReportsView.aspx?schema=MBDR_System&table=CaseReportsView">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Case Reports
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of all existing case reports. 
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Reports
                </div>
            </div>
            <div class="program-description">
                List of all existing case reports. 
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Reports
                </div>
            </div>
            <div class="program-description">
                List of all existing case reports. 
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "ML_WRITE" || item.PermissionName == "ML_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/MatchingView.aspx?schema=MBDR_System&table=Matching_Linking">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Matching and Linking
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of all the unmatched new case reports. Use this screen to view possible matches to existing case reports, birth records, and/or death records and link if applicable or create a new case.
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Matching and Linking
                </div>
            </div>
            <div class="program-description">
                List of all the unmatched new case reports. Use this screen to view possible matches to existing case reports, birth records, and/or death records and link if applicable or create a new case.
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Matching and Linking
                </div>
            </div>
            <div class="program-description">
                List of all the unmatched new case reports. Use this screen to view possible matches to existing case reports, birth records, and/or death records and link if applicable or create a new case.
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "ARCHIVE_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/ArchiveCaseReports.aspx?schema=UI_MBDR_ARCHIVE&table=ArchiveDetails">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Archived Case Reports
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of all incoming case reports, before any applied registry formatting.
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Archived Case Reports
                </div>
            </div>
            <div class="program-description">
                List of all incoming case reports, before any applied registry formatting.
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Archived Case Reports
                </div>
            </div>
            <div class="program-description">
                List of all incoming case reports, before any applied registry formatting.
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "AE_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/errorLogs.aspx">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Activity and Error Logs
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of file load activity and validation errors. 
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Activity and Error Logs
                </div>
            </div>
            <div class="program-description">
                List of file load activity and validation errors. 
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Activity and Error Logs
                </div>
            </div>
            <div class="program-description">
                List of file load activity and validation errors. 
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "REF_WRITE" || item.PermissionName == "REF_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/referenceTables.aspx">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Reference Tables
                    </div>
                </div>
            </a>
            <div class="program-description">
                List of support tables used for loading and validating new case files/reports. 
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Reference Tables
                </div>
            </div>
            <div class="program-description">
                List of support tables used for loading and validating new case files/reports. 
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Reference Tables
                </div>
            </div>
            <div class="program-description">
                List of support tables used for loading and validating new case files/reports. 
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "USER_WRITE" || item.PermissionName == "USER_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/UserManagementPage.aspx?schema=MBDR_System&table=Users">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        User Management
                    </div>
                </div>
            </a>
            <div class="program-description">
                Designate User Roles and Permissions.<br>
                <br>
                <b><i>Note: The System Administrator is the only role with the ability to Create, Edit and Disable other user accounts.</i></b>
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    User Management
                </div>
            </div>
            <div class="program-description">
                Designate User Roles and Permissions.<br>
                <br>
                <b><i>Note: The System Administrator is the only role with the ability to Create, Edit and Disable other user accounts.</i></b>
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    User Management
                </div>
            </div>
            <div class="program-description">
                Designate User Roles and Permissions.<br>
                <br>
                <b><i>Note: The System Administrator is the only role with the ability to Create, Edit and Disable other user accounts.</i></b>
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">


            <%--            <a href="/CaseVerification.aspx?schema=MBDR_System&table=CaseVerificationView">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Case Verification 
                    </div>
                </div>
            </a>
            <div class="program-description">
                Perform Case Verification
            </div>--%>


            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "CASE_WRITE" || item.PermissionName == "CASE_VIEW")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/CaseVerification.aspx?schema=MBDR_System&table=getCaseVerificationList">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Case Verification 
                    </div>
                </div>
            </a>
            <div class="program-description">
                Perform Case Verification.
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Verification
                </div>
            </div>
            <div class="program-description">
                Perform Case Verification.
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Case Verification
                </div>
            </div>
            <div class="program-description">
                Perform Case Verification.
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">

            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "FIXIT_WRITE")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/FixitTool.aspx">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Fix It Tool 
                    </div>
                </div>
            </a>
            <div class="program-description">
                Review and Update Cases.
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Fix It Tool
                </div>
            </div>
            <div class="program-description">
                Review and Update Cases.
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Fix It Tool
                </div>
            </div>
            <div class="program-description">
                Review and Update Cases.
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">

            <% 
                if (Permissions != null)
                {
                    bool chkPermission = false;
                    foreach (var item in Permissions)
                    {
                        if (item.PermissionName == "FACILITY_REPORT_CARD")
                        {
                            chkPermission = true;
                            break;
                        }
                        else
                        {
                            chkPermission = false;
                        }
                    }
                    if (chkPermission)
                    {
            %>
            <a href="/FacilityDashboard.aspx">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Facility Dashboard
                    </div>
                </div>
            </a>
            <div class="program-description">
                View monthly BD submission statistics, and generate Facility Report Cards and Annual Quality Reports.
            </div>
            <%
                }
                else
                {
            %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Facility Dashboard
                </div>
            </div>
            <div class="program-description">
                View monthly BD submission statistics, and generate Facility Report Cards and Annual Quality Reports.
            </div>
            <%}%>
            <% }
                else
                { %>
            <div class="program-header disabled-program">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Facility Dashboard
                </div>
            </div>
            <div class="program-description">
                View monthly BD submission statistics, and generate Facility Report Cards and Annual Quality Reports.
            </div>
            <% } %>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
    <div class="program">

        <% 
            if (Permissions != null)
            {
                bool chkPermission = false;
                foreach (var item in Permissions)
                {
                    if (item.PermissionName == "AUDIT_VIEW_ONLY")  // change to Audit permission later
                    {
                        chkPermission = true;
                        break;
                    }
                    else
                    {
                        chkPermission = false;
                    }
                }
                if (chkPermission)
                {
        %>
        <a href="/Audit.aspx">
            <div class="program-header">
                <div class="program-icon">
                    <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                </div>
                <div class="program-name">
                    Audit
                </div>
            </div>
        </a>
        <div class="program-description">
            Perform and export audit reports.
        </div>
        <%
            }
            else
            {
        %>
        <div class="program-header disabled-program">
            <div class="program-icon">
                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
            </div>
            <div class="program-name">
                Audit
            </div>
        </div>
        <div class="program-description">
            Perform and export audit reports.
        </div>
        <%}%>
        <% }
            else
            { %>
        <div class="program-header disabled-program">
            <div class="program-icon">
                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
            </div>
            <div class="program-name">
                Audit
            </div>
        </div>
        <div class="program-description">
             Perform and export audit reports.
        </div>
        <% } %>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <%--<asp:Label ID="LabelQUERY" runat="server" Text="Label"></asp:Label>--%>
        </div>
    </div>
    <% } %>

    <div id="altarumLogin">
    </div>

    <div id="somLogin">
    </div>

</asp:Content>
