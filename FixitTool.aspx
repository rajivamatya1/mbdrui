﻿<%@ Page Title="FIX IT TOOL" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FixitTool.aspx.cs" Inherits="DynamicGridView.FixitTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">


    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
            <asp:HiddenField runat="server" ID="hdfTableName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfAdvSearchFilter" Value="" />
            <asp:HiddenField runat="server" ID="hdfSearchType" />
            <asp:HiddenField runat="server" ID="isResetClick" Value="" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" />FIX IT TOOL LISTING</h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div id="dvAdvanceSearchView" runat="server">
                <div class="row">
                    <div class="form-item">
                        <label>Filter by FID</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtFid" ValidationGroup="search" TextMode="SingleLine" />
                    </div>
                   <div class="form-item">
                        <label>Filter by Status</label>
                       <asp:DropDownList runat="server" CssClass="form-control radiusborder" ID="ddlFilterByStatus" ValidationGroup="search">                         
                           <asp:ListItem Text="Unresolved" Value="Unresolved"></asp:ListItem>
                           <asp:ListItem Text="Resolved" Value="Resolved"></asp:ListItem>
                           </asp:DropDownList>
                    </div>
                    <div class="form-item">
                        <label>Filter by Owner</label>
                        <asp:DropDownList runat="server" CssClass="form-control radiusborder" ID="ddlFilterByOwner" ValidationGroup="search" />
                    </div>
                    <div class="form-item">
                        <label>Filter by Flagged By</label>
                        <asp:DropDownList runat="server" CssClass="form-control radiusborder" ID="ddlFilterByFlagged" ValidationGroup="search">
                        </asp:DropDownList>
                    </div>
                    <div class="form-item">
                        <label>Child Last Name</label>
                        <asp:TextBox runat="server" CssClass="form-control radiusborder" ID="txtchdLN" ValidationGroup="search" TextMode="SingleLine" />
                    </div>
                   <div class="form-item">
                        <label>Child DOB</label>
                        <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtChildDOB" ValidationGroup="search" onblur="ValidateToDate(this);" EnableViewState="true" />
                        <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ValidationGroup="search" ID="CustomValidator3" ClientValidationFunction="ValidateToDate" ControlToValidate="txtChildDOB" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                    </div>
                    <div class="form-item" id="dvAdvanceSearchBtn" runat="server">
                        <asp:Button Text="Search" runat="server" ID="btnSearch" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-danger" />
                    </div>
                    <div class="form-item" id="dvbtnResetAdv" runat="server">
                        <asp:Button Text="Reset" runat="server" ID="btnResetAdv" CausesValidation="False" OnClientClick="resetValidation()" CssClass="btn btn-danger" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="page-title">
            <label><b class="yellow-highlight-fixit">Indicates Fixables in Progress</b></label>
        </div>
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" OnRowEditing="gvDynamic_RowEditing" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"                    
                    AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>


    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
                    }
                });

        /*** will trigger search button by enter ***/

        function resetValidation() {

            document.getElementById("<%=ddlFilterByFlagged.ClientID %>").value = "0";
            document.getElementById("<%=ddlFilterByOwner.ClientID %>").value = "0";
            document.getElementById("<%=ddlFilterByStatus.ClientID %>").value = "0";
            document.getElementById("<%=txtchdLN.ClientID %>").value = "";
            document.getElementById("<%=txtChildDOB.ClientID %>").value = "";
            document.getElementById("<%=txtFid.ClientID %>").value = "";

            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

            var gridView = document.getElementById("<%= gvDynamic.ClientID %>");
            var pageIndex = 0;
            gridView.PageIndex = pageIndex;
        }

        function ValidateDate(input) {

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;

            var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

            if (enteredChildDOB.length === 10) {
                if (enteredChildDOB === "undefined" || enteredChildDOB === null || enteredChildDOB === "") {
                    enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
                }
            }
        }

        function ShowProgress() {
            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",

                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },

                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

    </script>

</asp:Content>
