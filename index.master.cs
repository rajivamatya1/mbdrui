﻿using DynamicGridView.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
public partial class index : System.Web.UI.MasterPage
{
    //private Timer timer;

    private IEnumerable<dynamic> permissions;
    protected DataTable dtNotification;

    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        permissions = CheckRole() ?? Enumerable.Empty<dynamic>();

        if (!this.IsPostBack)
        {
            string env = ConfigurationManager.AppSettings["environment"];

            if (!string.IsNullOrEmpty(env))
            {
                /***** Deployment Release 17.0 *****/

                string releaseVer = "17.0";

                if (env == "dev") 
                {
                    lblVersion.Text = "Dev V " + releaseVer;
                }
                else if(env == "qa")
                {
                    lblVersion.Text = "AQA V " + releaseVer;
                }
                else if (env == "sqa")
                {
                    lblVersion.Text = "SQA V " + releaseVer;
                }
                else if (env == "prod")
                {
                    lblVersion.Text = "Prod V " + releaseVer;
                }
            }

            Session["Reset"] = true;

            CheckServerStatus();

            tmrCheckServerStatus.Enabled = true; // Start the timer
        }

        Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");

        SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");

        int totalSessionTime = (int)section.Timeout.TotalMinutes * 1000 * 60;

        int sessionTimeOutAlert = totalSessionTime - (90 * 1000); // Alert message at 13.5 minutes 

        Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", $"SessionExpireAlert({sessionTimeOutAlert});", true);

    }

    protected void CheckRealTimeNotificaton()
    {
        string userId = string.Empty;
        if (Session["userid"] != null)
        {
            userId = Convert.ToString(Session["userid"]);
        }

        if (!string.IsNullOrEmpty(userId))
        {
            dtNotification = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string where = "where createddate >= DATEADD(day, -30, GETDATE()) and userId=" + userId + "";
                string orderby = " Order by CreatedDate desc";
                string query = string.Format("SELECT * FROM {0}.{1} {2} {3}", "MBDR_System", "NotificationMessages", where, orderby);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtNotification);
            }
        }
    }

    protected IEnumerable<dynamic> Permissions { get; set; }

    public IEnumerable<dynamic> CheckRole()
    {
        List<dynamic> permissions = new List<dynamic>();

        try
        {
            string userId = string.Empty;
            if (Session["userid"] != null)
            {
                userId = Convert.ToString(Session["userid"]);
            }

            if (!string.IsNullOrEmpty(userId))
            {
                DataTable dtAccessPermission = Common.CheckAccessPermission(userId);

                Permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission =>
                            (permission.Field<string>("PermissionName") == "RECORDS_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "REPORTS_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "ML_WRITE") || //6p
                            (permission.Field<string>("PermissionName") == "ML_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "ARCHIVE_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "AE_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "REF_WRITE") || // 5p
                            (permission.Field<string>("PermissionName") == "REF_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "USER_WRITE") || // 6p
                            (permission.Field<string>("PermissionName") == "USER_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "REPORT_VIEW") || // 2p
                            (permission.Field<string>("PermissionName") == "CASE_WRITE") || // 8p
                            (permission.Field<string>("PermissionName") == "CASE_VIEW") || // 4p
                            (permission.Field<string>("PermissionName") == "FIXIT_WRITE") || // 5p
                            (permission.Field<string>("PermissionName") == "FIXIT_VIEW") || //2p
                            (permission.Field<string>("PermissionName") == "AUDIT_VIEW_ONLY") || //2p
                            (permission.Field<string>("PermissionName") == "FACILITY_REPORT_CARD")) // 2p
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    });
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "index_CheckRole");
        }

        return permissions;
    }

    protected void tmrCheckServerStatus_Tick(object sender, EventArgs e)
    {
        CheckServerStatus();
    }
    
    public void CheckServerStatus()
    {
        try
        {
            if (Convert.ToString(Session["userid"]) != null && Convert.ToInt32(Session["userid"]) != 0)
            {
                string json = "{\"cmd\":\"PING\",\"user\":" + Convert.ToInt32(Session["userid"]) + "}";
                string response = SocketConn(json);
                string details = string.Empty;

                if (response == "SocketConnectionError")
                {
                    divRed.Visible = true;
                    divGreen.Visible = false;

                    details = "Server unavailable";
                }
                else
                {
                    divRed.Visible = false;
                    divGreen.Visible = true;

                    dynamic data = JObject.Parse(response);

                    if (data.name == "SUCCESS")
                    {
                        details = data.details;
                    }
                    else
                    {
                        details = "Server unavailable";
                    }
                }
                detailsValue.Value = details;
            }
            else
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;

                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }
                Response.Redirect(miMiLogin, false);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "index_CheckServerStatus");
        }
    }

    public string SocketConn(string JsonRecord)
    {
        string responseFromServer = string.Empty;
        string socketIp = ConfigurationManager.AppSettings["ipAddress"];
        int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
        try
        {
            using (var socket = new TcpClient(socketIp, socketPort))
            {
                string json = JsonRecord;
                byte[] body = Encoding.UTF8.GetBytes(json);
                int bodyLength = Encoding.UTF8.GetByteCount(json);
                var bl = (byte)(bodyLength);
                var bh = (byte)(bodyLength >> 8);
                using (var stream = socket.GetStream())
                {
                    stream.WriteByte(bh);
                    stream.WriteByte(bl);
                    stream.Write(body, 0, bodyLength);

                    short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                    byte[] buffer = new byte[size];

                    int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                    if (sizestream == size)
                    {
                        responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                    }
                    else
                    {
                        responseFromServer = "SocketConnectionError";
                    }
                }
            }
        }
        catch (Exception)
        {
            responseFromServer = "SocketConnectionError";
        }
        return responseFromServer;
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        HttpContext.Current.Application[appGUID] = "";
        Session["userid"] = "";
        Session["username"] = "";
        Session["BrowserName"] = "";
        Session["BrowserCount"] = "";
        Session.Abandon();
        Session.Clear();
        //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", "")); // need to test
        //string miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
        //Response.Redirect(miMiLogin);

        string env = ConfigurationManager.AppSettings["environment"];
        string miMiLogin = string.Empty;

        if (!string.IsNullOrEmpty(env))
        {
            if (env == "dev" || env == "qa")
            {
                miMiLogin = "login.aspx";
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }
        }
        else
        {
            miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
        }
        Response.Redirect(miMiLogin, false);
    }
}

public static class cApp
{
    public static string GenerateSHA512String(string inputString)
    {
        SHA512 sha512 = SHA512.Create();
        byte[] bytes = Encoding.UTF8.GetBytes(inputString);
        byte[] hash = sha512.ComputeHash(bytes);
        return GetStringFromHash(hash);
    }

    public static string GetStringFromHash(byte[] hash)
    {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            result.Append(hash[i].ToString("X2"));
        }
        return result.ToString();
    }
}