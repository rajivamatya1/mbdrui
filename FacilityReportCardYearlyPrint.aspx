﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FacilityReportCardYearlyPrint.aspx.cs" Inherits="FacilityReportCardYearlyPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%--Report--%></title>
    <style>
        @media print {

            header, footer {
                display: none;
            }

            body {
                margin: 0;
                padding: 0;
            }

            .header {
                max-width: 100%;
                border: none;
                padding: 10mm;
            }

            .print-button {
                display: none;
            }

            .logo-img {
                width: 70px;
            }
        }

        .reportBanner {
            height: 80px;
            width: 100%;
        }

        .printlogo {
            height: 50px;
        }

        .left-align {
            padding-left: 10px !important;
            text-align: left !important;
        }

        .boldFont {
            font-weight: bold;
        }

        .pProperty {
            font-size: small;
            font-style: italic;
            margin-top: 4px !important;
            margin-top: 2px !important;
            font-family: 'Times New Roman';
        }

        /******** Banner property ********/

        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .header {
            padding: 0px;
            /*border-bottom: 2px solid black;*/
            width: 95%;
        }

        .logo {
            flex-grow: 1;
            text-align: center;
        }

        .logo-img {
            width: 100px;
            height: auto;
        }

        .print-button {
            position: absolute;
            right: 0;
        }

        .text-section {
            display: flex;
            justify-content: space-between;
            width: 100%;
        }

        .print-button button {
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
        }

        .content {
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
            margin-bottom: 20px;
        }

        .left, .center, .right {
            text-align: center;
        }

        .left {
            flex-basis: 25%;
            text-align: center;
        }

        .center {
            flex-basis: 50%;
            text-align: center;
        }

        .right {
            flex-basis: 25%;
            text-align: center;
        }

        p {
            margin: 0;
            font-size: 14px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="max-height: 100%" class="printFRCs">
            <table cellpadding="0" cellspacing="0" id="tableFRCDetails" border="0">
                <p style="font-size: smaller; font-weight: 400; margin-left: 25px !important">
                    <%=DateTime.Now.ToString("d") %>
                </p>
                <div class="header">
                    <div class="content">
                        <div class="logo">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Content/css/images/frcsReportLogo.svg" CssClass="reportBanner" />
                        </div>
                        <div class="print-button" id="trprint">
                            <a href="#" onclick="printpage();">
                                <img src="Content/css/images/print.png" class="printlogo" /></a>
                        </div>
                    </div>
                    <div class="text-section">
                        <div class="left">
                            <p><br/>
                                GRETCHEN WHITMER
                                <br />
                                GOVERNOR
                            </p>
                        </div>
                        <div class="center">
                            <p>
                                STATE OF MICHIGAN<br />
                                DEPARTMENT OF HEALTH AND HUMAN SERVICES<br />
                                LANSING
                            </p>
                        </div>
                        <div class="right">
                            <p><br/>
                                ELIZABETH HERTEL<br />
                                DIRECTOR
                            </p>
                        </div>
                    </div>
                </div>
                <tr>
                    <td>
                        <h2>
                            <center>Michigan Birth Defects Registry (MBDR) Quarterly Report</center>
                        </h2>
                        <p style="font-size: medium !important; font-weight: 400 !important; margin-bottom: 4px !important;">
                            This report provides data on your reporting facility’s performance measures regarding birth defects reports. This report card provides this quarter’s data quality measures in timeliness, accuracy, and completeness.
                        </p>
                        <p style="font-size: medium !important; font-weight: 400 !important;">
                            Michigan Birth Defects reporting is required monthly, between the 10th and the 15th of each month, for the previous month’s data. Monthly submissions prior to the 10th day of the month may require re-submission. Monthly submissions received after the 15th of each month is a breach of MDHHS/MBDR compliance and may result in disciplinary actions.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="1" style="margin: 0 auto; text-align: center !important; width: auto; margin-top: 4px;">
                            <tr>
                                <td class="boldFont">                                    
                                    <%= !string.IsNullOrEmpty(selectedFacilityText) ? selectedFacilityText : (!string.IsNullOrEmpty(selectedFacility) ? selectedFacility : "No data available")  %>                                    
                                </td>

                                <td colspan="5" class="boldFont"><%=selectedQuarterText%> <%= selectedYear %></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[0]["monthDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[1]["monthDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[2]["monthDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[3]["monthDisplay"] %></td>
                                <td class="boldFont"><%=dtFRCStats.Rows[4]["monthDisplay"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">File Submission Timeliness</td>
                                <td><%=dtFRCStats.Rows[0]["fileTimeliness"] %></td>
                                <td><%=dtFRCStats.Rows[1]["fileTimeliness"] %></td>
                                <td><%=dtFRCStats.Rows[2]["fileTimeliness"] %></td>
                                <td><%=dtFRCStats.Rows[3]["fileTimeliness"] %></td>
                               <td class="greyCell"></td>
                            </tr>
                            <tr>
                                <td class="left-align">File Submissions (N)</td>
                                <td><%=dtFRCStats.Rows[0]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[1]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[2]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[3]["fileSubmissions"] %></td>
                                <td><%=dtFRCStats.Rows[4]["fileSubmissions"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">File Load Failures (N)</td>
                                <td><%=dtFRCStats.Rows[0]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[1]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[2]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[3]["fileLoadFailures"] %></td>
                                <td><%=dtFRCStats.Rows[4]["fileLoadFailures"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCount"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCount"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Reports Monthly Average (N) (12 month rolling)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCountMonthlyAvg"] %></td>
                                <td class="greyCell"></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Volume Change vs Monthly Average (%)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsPercentChangeVsMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsPercentChangeVsMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsPercentChangeVsMonthlyAvg"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsPercentChangeVsMonthlyAvg"] %></td>
                                <td class="greyCell"></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Invalid (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountInvalid"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCountInvalid"] %></td>
                            </tr>
                            <tr>
                                <td class="left-align">Reports Invalid (%)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsInvalidPercent"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsInvalidPercent"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Reports Duplicated (N)</td>
                                <td><%=dtFRCStats.Rows[0]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[1]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[2]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[3]["reportsCountDuplicates"] %></td>
                                <td><%=dtFRCStats.Rows[4]["reportsCountDuplicates"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Data Errors (N)</td>
                                <td><%=dtFRCStats.Rows[0]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[1]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[2]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[3]["dataErrorsCount"] %></td>
                                <td><%=dtFRCStats.Rows[4]["dataErrorsCount"] %></td>
                            </tr>
                            <tr>

                                <td class="left-align">Data Errors (%)</td>
                                <td><%=dtFRCStats.Rows[0]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[1]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[2]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[3]["dataErrorsPercent"] %></td>
                                <td><%=dtFRCStats.Rows[4]["dataErrorsPercent"] %></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <div class="marginLeft50" style="font-size: smaller;">
                <b>If you have any questions or concerns, please email us at <a href="mailto:MDHHS-MBDR@Michigan.gov" class="blueColor" target="_blank">MDHHS-MBDR@Michigan.gov.</a></b>
            </div>
            <br />
            <div style="font-size: smaller;">
                <b class="pProperty"><i>Definitions:</i></b>
                <p class="pProperty">“File” means a Comma Separated Values (CSV) file containing one or more birth defect Reports, per the current MBDR flatfile specification, that is submitted electronically to the birth defects registry</p>
                <p class="pProperty">““Report” means a single line in a File detailing a patient admission or encounter that qualifies to be reported to the birth defects registry.  Files may contain many Reports, each as a distinct line in the File.</p>
                <p class="pProperty">“Invalid Report” is a line in a File that has one or more errors which prevent that specific report from being usable by the registry (e.g., missing or invalid data in a required field, or an invalid facility reference).  A Report may have multiple data errors but would only be counted as one Invalid Report, and there can be multiple Invalid Reports in the same File.</p>
                <p class="pProperty">“Data Error” is any instance of missing data in a required field, or invalid values (unexpected or out-of-range) in any field.  There can be multiple Data Errors in each Report (line) in a File.</p>
                <p class="pProperty">“Duplicate Report” is when a Report is submitted that contains an identical set of values as an existing Report in the registry, whether the duplicate report is in the same file or a different one.  Duplicates are not necessarily errors as it is sometimes unavoidable, but they are still tracked and prevented from being loaded to the registry.</p>
            </div>
            <br />
            <div style="font-size: xx-small;">
                <center>
                    333 S Grand Ave  ●  P.O. BOX 30691  ●  LANSING, MICHIGAN  48909
           <br />
                    www.michigan.gov/mbdr</center>
            </div>
        </div>
    </form>
    <script>
        function printpage() {
            document.getElementById("trprint").style.display = "none";
            window.print();
            document.getElementById("trprint").style.display = "block";
        }
    </script>
</body>
</html>
