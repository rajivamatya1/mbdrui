﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class ArchiveCaseReports : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            try
            {
                txtDateFrom.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateFrom.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                txtDateTo.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateTo.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                txtChildDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtChildDOB.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

                if (!IsPostBack)
                {
                    string timeout = !string.IsNullOrEmpty(Request.QueryString["timeout"]) ? Convert.ToString(Request.QueryString["timeout"]) : "";

                    if (timeout == "Yes")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "timeout", "noRecFound( 'The timeout period elapsed prior to completion of the operation or the server is not responding.');", true);
                    }

                    string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                    string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";

                    string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                    string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

                    string child_last_name = !string.IsNullOrEmpty(Request.QueryString["child_last_name"]) ? Convert.ToString(Request.QueryString["child_last_name"]) : "";
                    string child_first_name = !string.IsNullOrEmpty(Request.QueryString["child_first_name"]) ? Convert.ToString(Request.QueryString["child_first_name"]) : "";
                    string child_alias_last_name = !string.IsNullOrEmpty(Request.QueryString["child_alias_last_name"]) ? Convert.ToString(Request.QueryString["child_alias_last_name"]) : "";
                    string child_alias_first_name = !string.IsNullOrEmpty(Request.QueryString["child_alias_first_name"]) ? Convert.ToString(Request.QueryString["child_alias_first_name"]) : "";
                    string child_birthdate = !string.IsNullOrEmpty(Request.QueryString["child_birthdate"]) ? Convert.ToString(Request.QueryString["child_birthdate"]) : "";
                    string child_ssn = !string.IsNullOrEmpty(Request.QueryString["child_ssn"]) ? Convert.ToString(Request.QueryString["child_ssn"]) : "";                    
                    string mother_ssn = !string.IsNullOrEmpty(Request.QueryString["mother_ssn"]) ? Convert.ToString(Request.QueryString["mother_ssn"]) : "";
                    string mother_last_name = !string.IsNullOrEmpty(Request.QueryString["mother_last_name"]) ? Convert.ToString(Request.QueryString["mother_last_name"]) : "";
                    string mother_first_name = !string.IsNullOrEmpty(Request.QueryString["mother_first_name"]) ? Convert.ToString(Request.QueryString["mother_first_name"]) : "";
                    string address_1 = !string.IsNullOrEmpty(Request.QueryString["address_1"]) ? Convert.ToString(Request.QueryString["address_1"]) : "";
                    string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admitting_Entity"]) ? Convert.ToString(Request.QueryString["admitting_Entity"]) : "";

                    if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                    {
                        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                        List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                        string[] header = model.Table.Split(':');
                        if (header.Length > 0)
                        {
                            lblHeader.Text = "ARCHIVED CASE REPORTS";
                            lblHeader.Visible = true;
                        }
                        hdfName.Value = headerLabel;
                        lblSchema.Text = schema;
                        lblTable.Text = tableName;
                        txtchdLN.Text = child_last_name;
                        txtchdFN.Text = child_first_name;
                        txtchdALN.Text = child_alias_last_name;
                        txtchdAFN.Text = child_alias_first_name;
                        txtChildDOB.Text = child_birthdate;
                        txtChildSSN.Text = child_ssn;
                        txtMomSSN.Text = mother_ssn;                        
                        txtMomLN.Text = mother_last_name;
                        txtMomFN.Text = mother_first_name;                       

                        txtadd1.Text = address_1;
                        txtDateFrom.Text = fromDate;
                        txtDateTo.Text = toDate;
                        
                        BindColumnsDropDown(schema, tableName);
                        
                        if (!string.IsNullOrEmpty(child_last_name) || !string.IsNullOrEmpty(child_first_name) || !string.IsNullOrEmpty(child_alias_last_name) || !string.IsNullOrEmpty(child_alias_first_name) || !string.IsNullOrEmpty(child_birthdate) || !string.IsNullOrEmpty(child_ssn) || !string.IsNullOrEmpty(mother_ssn) || !string.IsNullOrEmpty(mother_last_name) || !string.IsNullOrEmpty(mother_first_name) || !string.IsNullOrEmpty(address_1) || !string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate) || !string.IsNullOrEmpty(admittingEntity))
                        {
                            BindGrid(schema, tableName);
                        }
                        else
                        {
                            BindGridEmpty(schema, tableName);
                        }
                    }                    
                }
                else
                {
                    if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                    {
                        BindGridEmpty(schema, tableName);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_Page_Load");
            }
        }

        public void BindGridEmpty(string schema, string tableName)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                SqlCommand command = new SqlCommand(sqlQuery, conn);
                command.Parameters.AddWithValue("@schema", schema);
                command.Parameters.AddWithValue("@tableName", tableName);
                command.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter daColumns = new SqlDataAdapter(command);
                conn.Close();
                daColumns.Fill(dt);

            }

            gvDynamic.Columns.Clear();
            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            gvDynamic.Columns.Add(commandField);

            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            List<string> lstColumns = includeColumns.Split(',').ToList();
            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 11)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string colName = Convert.ToString(dt.Rows[i][0]);
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (colName != "plurality" && colName != "childMRN" && colName != "birthOrder" && colName != "vitalStatus" && colName != "birthWeight" && colName != "childMedicaidNumber" && colName != "birthHospital" && colName != "county" && colName != "country" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "gender" && colName != "momMiddleName" && colName != "momSuffix" && colName != "admittingEntityCode" && colName != "patientType" && colName != "admissionSource" && colName != "admissionDate" && colName != "dischargeStatus" && colName != "dischargeDate" && colName != "icd9_diag_code" && colName != "icd10_diag_status" && colName != "icd9_proc_code" && colName != "cpt_proc_code" && colName != "icd10_proc_code" && colName != "ICD9SyndromeCode" && colName != "ICD10SyndromeCode" && colName != "cytogeneticsTesting" && colName != "ICD9CytogeneticsDiagnosisCodes" && colName != "ICD10CytogeneticsDiagnosisCodes" && colName != "labCode" && colName != "cytogeneticsLabReportNumber" && colName != "headCircumference" && colName != "deliveryLength" && colName != "infantPostnatalEcho" && colName != "ageFirstPostnatalEcho" && colName != "dateFirstPostnatalEcho" && colName != "infantAdmitted" && colName != "ageInfantAdmitted" && colName != "ageInfantDischarged" && colName != "ageInfantAdmittedICU" && colName != "dateInfantAdmittedICU" && colName != "approved" && colName != "lastUpdated")
                            {
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = Convert.ToString(dt.Rows[i][0]);
                                    field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            DataTable dtEmpty = new DataTable();
            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.DataSource = dtEmpty;
            dvFromDate.Visible = true;
            dvToDate.Visible = true;
            gvDynamic.DataBind();
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtEntityList = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admitting_Entity"]) ? Convert.ToString(Request.QueryString["admitting_Entity"]) : "";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);

                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    string queryList = "";
                    queryList = string.Format("select distinct details,entityIndex, admitting_Entity from UI_MBDR_ARCHIVE.ArchiveDetails Inner Join Reference.Entities on Reference.Entities.entityId =  UI_MBDR_ARCHIVE.ArchiveDetails.admitting_entity order by details asc");
                    cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtEntityList);
                    ddlArchiveTables.DataSource = dtEntityList;
                    ddlArchiveTables.DataTextField = "details";
                    ddlArchiveTables.DataValueField = "admitting_Entity";
                    ddlArchiveTables.DataBind();
                    ddlArchiveTables.Items.Insert(0, "Select Facility");
                    if (!string.IsNullOrEmpty(admittingEntity))
                    {
                        ListItem selectedItem = ddlArchiveTables.Items.FindByValue(admittingEntity);
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_BindColumnsDropDown");
            }
        }

        public void BindGrid(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "archiveId")
                            {
                                sortExp = "archiveId desc";
                                break;
                            }
                        }
                    }
                }

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; 
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName; 
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderByAId = sortExp;
            
                    if (string.IsNullOrEmpty(OrderByAId))
                    {
                        OrderByAId = " archiveId desc";
                    }

                    int pageIndex = 0;

                    OrderByAId = sortExp;

                    where = GetWhereClause();

                    if (!where.Contains("and"))
                    {
                        BindGridEmpty(schema, tableName);
                        if (ddlArchiveTables.SelectedValue != "Select Facility")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                        }
                    }
                    else
                    {
                        query = string.Format("select Row,archiveId,child_last_name,child_first_name,child_alias_last_name,child_alias_first_name,child_birthdate,child_ssn,mother_ssn, mother_last_name,mother_first_name,address_1,loaded_datetime from (SELECT ROW_NUMBER() OVER(ORDER BY {6} ) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4}) order by {6} ", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderByAId);
                        
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();                        
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dt);

                        gvDynamic.Columns.Clear();
                        CommandField commandField = new CommandField();
                        commandField.ButtonType = ButtonType.Link;
                        commandField.HeaderText = "Action";
                        commandField.ShowEditButton = true;
                        gvDynamic.Columns.Add(commandField);


                        List<string> lstColumns = includeColumns.Split(',').ToList();

                        foreach (var col in lstColumns)
                        {
                            if (gvDynamic.Columns.Count < 13)
                            {
                                if (!string.IsNullOrEmpty(col))
                                {
                                    foreach (DataColumn item in dt.Columns)
                                    {
                                        string colName = item.ColumnName;
                                        string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                            BoundField field = new BoundField();
                                            field.HeaderText = columnInfo;
                                            field.DataField = item.ColumnName;
                                            field.SortExpression = item.ColumnName;
                                            if (readonlyColumns.Contains(colName))
                                            {
                                                field.ReadOnly = true;
                                            }
                                            gvDynamic.Columns.Add(field);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {

                                break;
                            }
                        }

                        gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                        gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                        if(gvDynamic.VirtualItemCount < 1)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);  
                        }
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        gvDynamic.DataSource = dt;
                        gvDynamic.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];

                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        private string GetWhereClause()
        {
            string where = "where 1 = 1";

            if (!string.IsNullOrEmpty(txtchdLN.Text) && !txtchdLN.Text.Contains("*"))
            {
                where += " and child_last_name = '" + txtchdLN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdLN.Text.Contains("*"))
            {
                where += " and child_last_name like '" + txtchdLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtchdFN.Text) && !txtchdFN.Text.Contains("*"))
            {
                where += " and child_first_name = '" + txtchdFN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdFN.Text.Contains("*"))
            {
                where += " and child_first_name like '" + txtchdFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtchdALN.Text) && !txtchdALN.Text.Contains("*"))
            {
                where += " and child_alias_last_name = '" + txtchdALN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdALN.Text.Contains("*"))
            {
                where += " and child_alias_last_name like '" + txtchdALN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtchdAFN.Text) && !txtchdAFN.Text.Contains("*"))
            {
                where += " and child_alias_first_name = '" + txtchdAFN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdAFN.Text.Contains("*"))
            {
                where += " and child_alias_first_name like '" + txtchdAFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtChildDOB.Text))
            {
                where += " and child_birthdate > ='" + txtChildDOB.Text + " 00:00:00'  and child_birthdate < ='" + txtChildDOB.Text + " 23:59:59'";
            }

            if (!string.IsNullOrEmpty(txtChildSSN.Text))
            {
                where += " and child_ssn ='" + txtChildSSN.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtMomSSN.Text))
            {
                where += " and mother_ssn ='" + txtMomSSN.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtMomLN.Text) && !txtMomLN.Text.Contains("*"))
            {
                where += " and mother_last_name = '" + txtMomLN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtMomLN.Text.Contains("*"))
            {
                where += " and mother_last_name like '" + txtMomLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtMomFN.Text) && !txtMomFN.Text.Contains("*"))
            {
                where += " and mother_first_name = '" + txtMomFN.Text.Trim().Replace("'", "''") + "'";
            }
            else if (txtMomFN.Text.Contains("*"))
            {
                where += " and mother_first_name like '" + txtMomFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtadd1.Text) && !txtadd1.Text.Contains("*"))
            {
                where += " and address_1 = '" + txtadd1.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtadd1.Text.Contains("*"))
            {
                where += " and address_1 like '" + txtadd1.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // both from/to date
            {
                where += " and loaded_datetime > ='" + txtDateFrom.Text + " 00:00:00'  and loaded_datetime < ='" + txtDateTo.Text + " 23:59:59'";
            }
            else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text)) // from date only 
            {
                where += " and loaded_datetime > ='" + txtDateFrom.Text + " 00:00:00' ";
            }
            else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // to date only 
            {
                where += " and loaded_datetime < ='" + txtDateTo.Text + " 23:59:59' ";
            }

            if (ddlArchiveTables.SelectedValue != "Select Facility")
            {
                where += " and admitting_Entity ='" + ddlArchiveTables.SelectedValue + "'";
            }

            return where;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where 1=1 ";
                    int pageIndex = 0;

                    where = GetWhereClause();

                    query = string.Format("select count({0}) from {1}.{2} {3}", datakeyName, schema, tableName, where);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_GetTotalRecords");
            }            
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_PageIndexChanging");
            }
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = -1;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();

                string dataKeyname = string.Empty;
                if (model != null)
                {
                    dataKeyname = model.DataKeyName; 
                }

                Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value, false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_ViewRecord");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_RowEditing");
            }
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                    GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;

                    if (model != null)
                    {
                        readonlyColumns = model.ReadonlyColumns; 
                        dataKeyname = model.DataKeyName;
                    }
                    string query = " set ";
                    foreach (DataControlFieldCell cell in row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            string colName = ((BoundField)cell.ContainingField).DataField;
                            if (!readonlyColumns.Contains(colName) && colName != "id")
                            {
                                query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                            }
                        }
                    }
                    gvDynamic.EditIndex = -1;
                    conn.Open();
                    string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                    string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                    SqlCommand cmd = new SqlCommand(finalQuery, conn);
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_RowUpdating");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_Sorting");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_GetSortDirection");
            }
            return nextDir;
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var birthDateValue = DataBinder.Eval(e.Row.DataItem, "child_birthdate");

                    if (birthDateValue != null && !string.IsNullOrEmpty(birthDateValue.ToString()))
                    {
                        DateTime birthDate = Convert.ToDateTime(birthDateValue);
                        e.Row.Cells[5].Text = birthDate.ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        e.Row.Cells[5].Text = string.Empty;
                    }

                    var row = (DataRowView)e.Row.DataItem;
                    int index = -1;
                    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                    field.Text = "View";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.OnClientClick = "ShowProgress();";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_RowDataBound");
            }
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string where = "1";
                    string sortExpre = string.Empty;
                    if (!string.IsNullOrEmpty(txtchdLN.Text))
                    {
                        where += "&child_Last_Name=" + txtchdLN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtchdFN.Text))
                    {
                        where += "&child_First_Name=" + txtchdFN.Text.Trim() + "";
                    }
                   
                    if (!string.IsNullOrEmpty(txtchdALN.Text))
                    {
                        where += "&child_alias_last_name=" + txtchdALN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtchdAFN.Text))
                    {
                        where += "&child_alias_first_name=" + txtchdAFN.Text.Trim() + "";
                    }

                    if (!string.IsNullOrEmpty(txtChildDOB.Text))
                    {
                        where += "&child_birthdate=" + txtChildDOB.Text + "";
                    }

                    if (!string.IsNullOrEmpty(txtChildSSN.Text))
                    {
                        where += "&child_ssn=" + txtChildSSN.Text.Trim() + "";
                    }

                    if (!string.IsNullOrEmpty(txtMomSSN.Text))
                    {
                        where += "&mother_ssn=" + txtMomSSN.Text.Trim() + "";
                    }

                    if (!string.IsNullOrEmpty(txtMomLN.Text))
                    {
                        where += "&mother_last_name=" + txtMomLN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtMomFN.Text))
                    {
                        where += "&mother_first_name=" + txtMomFN.Text.Trim() + "";
                    }

                    if (!string.IsNullOrEmpty(txtadd1.Text))
                    {
                        where += "&address_1=" + txtadd1.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtDateFrom.Text))
                    {
                        where += "&fromDate=" + txtDateFrom.Text + "";
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text))
                    {
                        where += "&toDate=" + txtDateTo.Text + "";
                    }

                    if (ddlArchiveTables.SelectedValue != "Select Facility")
                    {
                        where += "&admitting_Entity=" + ddlArchiveTables.SelectedValue + "";
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        sortExpre = "archiveId desc";
                    }
                    else
                    {
                        sortExpre = Convert.ToString(ViewState["SortExpression"]);
                    }

                    string id =  Uri.EscapeDataString(Convert.ToString(e.CommandArgument.ToString()));

                    if (Convert.ToInt32(id) > 0)
                    {
                        Response.Redirect("ArchiveCaseReportsViewDetails.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + "&where=" + where + "&sortExp=" + sortExpre + "", false);
                    }
                    else
                    {
                        Response.Redirect("ArchiveCaseReportsViewDetails.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + "&where=" + where + "&sortExp=" + sortExpre + "", true);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_gvDynamic_RowCommand");
            }
        }

        protected void ddlArchiveTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string value = Convert.ToString(ddlArchiveTables.SelectedValue);
                string schema = "UI_MBDR_ARCHIVE";
                string tableName = "ArchiveDetails";
                gvDynamic.PageIndex = 0;
                BindGrid(schema, tableName);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_ddlArchiveTables_SelectedIndexChanged");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    ViewState["SortExpression"] = "archiveId desc";
                    gvDynamic.PageIndex = 0;
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_btnSearch_Click");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ddlArchiveTables.SelectedIndex = 0;

                ViewState["SortExpression"] = "archiveId desc";
                gvDynamic.PageIndex = 0;
                BindGridEmpty(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "ArchiveCaseReports_btnReset_Click");
            }
        }

        protected void txtChildDOB_TextChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "validate", "ValidateDate();", true);
            }
        }
    }
}