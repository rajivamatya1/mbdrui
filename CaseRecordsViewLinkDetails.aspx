﻿<%@ Page Title="Case Records View Linked Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="CaseRecordsViewLinkDetails.aspx.cs" Inherits="DynamicGridView.CaseRecordsViewLinkDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">


    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfCaseId" Value="" />
            <asp:HiddenField ID="hdfSchema" runat="server" />
            <asp:HiddenField ID="hdfTableName" runat="server" />
            <asp:HiddenField ID="hdfKeyName" runat="server" />
            <asp:HiddenField ID="hdfRowID" runat="server" />
            <asp:HiddenField ID="hdfSearch" runat="server" />
            <asp:HiddenField ID="hdfColumnName" runat="server" />
            <asp:HiddenField ID="hdfCaseRecId" runat="server" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="Linked Case Reports" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div>
                <li class="form-item">
                    <asp:LinkButton ID="btnCaseRecord" title="Link back to Case Record Details" runat="server" OnClick="btnCaseRecord_Click" OnClientClick="return ShowProgress();" > 
                        <div class="link-icon">
                            <image src="Content/css/images/box-arrow-up-left-white.svg"  alt="Case Record Details"></image> Return to Case Record
                        </div>
                    </asp:LinkButton>
                     <div class="form-item">
                                   <asp:Label ID="lblCaseRecordMessagedisplay" runat="server" CssClass="messagedisplay"></asp:Label>
                                </div>
                </li>
            </div>
        </div>
    </div>

    <br />
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <script type="text/javascript">

        function ValidateDate(sender, args) {
            var ddlcloumValue = document.getElementById("Content_ddlColumn").value;
            if (ddlcloumValue == "child_birthdate") {
                var dateString = document.getElementById(sender.controltovalidate).value;
                var regex = /((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$/;
                if (regex.test(dateString)) {
                    args.IsValid = true;
                }
                else {

                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        function hideReport() {
            var $searchReportElement = $("#<%=gvDynamic.ClientID%>");

             if ($searchReportElement.length) {
                 $searchReportElement.hide();
             }
         }

        function ShowProgress() {
            hideReport();

            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");
            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });
            return true;
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

    </script>
</asp:Content>
