﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FileSubmissionLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            txtDateFrom.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateFrom.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            txtDateTo.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateTo.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');
                    if (header.Length > 0)
                    {
                        lblHeader.Text = header[1];
                        lblHeader.Visible = true;
                    }
                    hdfName.Value = headerLabel;
                    lblSchema.Text = schema;
                    lblTable.Text = tableName;


                    BindColumnsDropDown(schema, tableName);

                    txtSearch.Text = search;
                    ddlColumn.SelectedValue = colName;
                    txtDateFrom.Text = fromDate;
                    txtDateTo.Text = toDate;

                    BindGrid(schema, tableName);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                {
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_Page_Load");
        }
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);

                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                }

                foreach (DataColumn item in dt.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            if (item.ColumnName != "errorDateTime" && item.ColumnName != "loadDateTime" && item.ColumnName != "claimedBy")
                            {
                                lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                            }
                        }
                    }
                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_BindColumnsDropDown");
        }
    }

    private string GetWhereClause()
    {        
        string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
        string where = "where 1=1";
        string chkDateTimeName = string.Empty;

        if (tableName == "ValidationFailures")
        {
            chkDateTimeName = "errordatetime";
        }
        else
        {
            chkDateTimeName = "loadDateTime";
        }

        if (chkDateTimeName == "loadDateTime")  // file load activity log
        {
            if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and loadDateTime > ='" + txtDateFrom.Text + " 00:00:00'  and loadDateTime < ='" + txtDateTo.Text + " 23:59:59'";
            }

            else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and loadDateTime > ='" + txtDateFrom.Text + " 00:00:00' ";
            }

            else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and loadDateTime < ='" + txtDateTo.Text + " 23:59:59' ";
            }
        }
        else //Case Reports Validation Errors
        {
            if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and errordatetime > ='" + txtDateFrom.Text + " 00:00:00'  and errordatetime < ='" + txtDateTo.Text + " 23:59:59'";
            }

            else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and errordatetime > ='" + txtDateFrom.Text + " 00:00:00' ";
            }

            else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text))
            {
                where += " and errordatetime < ='" + txtDateTo.Text + " 23:59:59' ";
            }
        }
        return where;
    }

    public void BindGrid(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            string chkDateTimeName = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Convert.ToString(item[0]) == "loadDateTime")
                        {
                            sortExp = " loadDateTime desc";
                            chkDateTimeName = "loadDateTime";
                            dvFromDate.Visible = true;
                            dvToDate.Visible = true;
                            break;
                        }

                        if (Convert.ToString(item[0]) == "errorDateTime")
                        {
                            sortExp = " errorDateTime desc";
                            chkDateTimeName = "errorDateTime";
                            dvFromDate.Visible = true;
                            dvToDate.Visible = true;
                            break;
                        }
                    }
                }
            }

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            hdfDataKeyName.Value = dataKeyname;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                int pageIndex = 0;

                if (!string.IsNullOrEmpty(sortExp))
                {
                    where = GetWhereClause();

                    if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                    {
                        string searchText = Common.ReplaceSQLChar(txtSearch.Text.Trim());
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} {8} and {2} like '{3}%') as result where Row between({6}) and ({7})",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '{3}%' ) as result where Row between({6}) and ({7})",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1} {6}) as result where Row between({4}) and ({5})", schema, tableName, sortExp, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({4}) and ({5})", schema, tableName, sortExp, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                    {
                        string searchText = Common.ReplaceSQLChar(txtSearch.Text.Trim());
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {8}) AS Row,* from {0}.{1} {7} and {2} like '{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, sortExp);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {7}) AS Row,* from {0}.{1} where {2} like '{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, sortExp);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {6}) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, sortExp);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, sortExp);
                        }
                    }
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }

            gvDynamic.Columns.Clear();
            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            commandField.ShowEditButton = true;
            gvDynamic.Columns.Add(commandField);
            List<string> lstColumns = includeColumns.Split(',').ToList();

            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 10)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        foreach (DataColumn item in dt.Columns)
                        {
                            string colName = item.ColumnName;
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                BoundField field = new BoundField();
                                field.HeaderText = columnInfo;
                                field.DataField = item.ColumnName;
                                field.SortExpression = item.ColumnName;
                                if (readonlyColumns.Contains(colName))
                                {
                                    field.ReadOnly = true;
                                }
                                gvDynamic.Columns.Add(field);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname,chkDateTimeName);
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner3", "removeProgress();", true);
            gvDynamic.DataSource = dt;
            gvDynamic.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_");
        }
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName, string chkDateTimeName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";

                where = GetWhereClause();

                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    query = string.Format("select count({0}) from {1}.{2} {5} and {3} like '{4}%' ", datakeyName, schema, tableName, Convert.ToString(ddlColumn.SelectedValue), Common.ReplaceSQLChar(txtSearch.Text.Trim()),where);
                }
                else
                {
                    query = string.Format("select count({0}) from {1}.{2} {3}", datakeyName, schema, tableName, where);
                }

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_GetTotalRecords");
        }
        return totalRecords;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                gvDynamic.PageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_btnSearch_Click");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ddlColumn.SelectedIndex = 0;
            ViewState["SortExpression"] = null;
            gvDynamic.PageIndex = 0;
            BindGrid(lblSchema.Text, lblTable.Text);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_btnReset_Click");
        }
    }

    protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                string searchText = Common.ReplaceSQLChar(txtSearch.Text.Trim());
                string columnName = ddlColumn.SelectedValue;
                string fromDate = Common.ReplaceSQLChar(txtDateFrom.Text.Trim());
                string toDate = Common.ReplaceSQLChar(txtDateTo.Text.Trim());
                string id = e.CommandArgument.ToString();

                Response.Redirect("FileLogDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + " Details" + "&searchText=" + searchText + "&colName=" + columnName + "&fromDate=" + fromDate + "&toDate=" + toDate + "", true);

                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_gvDynamic_RowCommand");
        }
    }

    protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(lblSchema.Text, lblTable.Text);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_gvDynamic_Sorting");
        }
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var row = (DataRowView)e.Row.DataItem;
                
                LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                field.Text = "View";
                field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.OnClientClick = "ShowProgress();";
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_gvDynamic_RowDataBound");
        }        
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                ViewState["PageIndex"] = gvDynamic.PageIndex;
            }
            BindGrid(lblSchema.Text, lblTable.Text);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_gvDynamic_PageIndexChanging");
        }
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC";
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_");
        }
        return nextDir;
    }

    protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDynamic.EditIndex = e.NewEditIndex;
            BindGrid(lblSchema.Text, lblTable.Text);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_gvDynamic_RowEditing");
        }
    }

    protected void btnReturnAaEL_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("/errorLogs.aspx");
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileSubmissionLog_btnReturnAaEL_Click");
        }
    }

    protected void txtChildDOB_TextChanged(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "validate", "ValidateDate();", true);
        }
    }
}