﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="DefaultErrorEditView.aspx.cs" Inherits="DefaultErrorEditView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" Runat="Server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />

            <asp:Label Text="" runat="server" Visible="false" ID="lblErrorSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblErrorTable" />
            <asp:HiddenField runat="server" ID="hdfErrorName" Value="" />
            <h1><img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <asp:Button Text="" ID="btnBack" OnClick="btnBack_Click" runat="server" />
        </div>
    </div>    
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" 
                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true" 
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging" 
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing" 
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First" 
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>  
        </div>
    </div>
</asp:Content>

