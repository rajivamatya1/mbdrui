﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


//User Management Roles

public partial class UserManagementRolesPermission : System.Web.UI.Page
{
    protected DataTable dtRoles = new DataTable();
    protected DataTable dtPermission = new DataTable();
    protected DataTable dtAccess = new DataTable();
    dynamic permissions = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();
            if (permissions.Count > 0)
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnUserRole.Visible = false;
                permissionRoleAccess.Attributes.Add("style", "opacity: 0.5; pointer-events: none;");
                btnSave.Enabled = false;
                btnSave.Visible = false;
                btnCancel.Enabled = false;
                btnCancel.Visible = false;
            }

            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string roleId = !string.IsNullOrEmpty(Request.QueryString["roleId"]) ? Convert.ToString(Request.QueryString["roleId"]) : "";
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                hdfDataKeyName.Value = "roleId";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string query = string.Format("SELECT details,roleId FROM {0}.{1} order by details asc", "Reference", "Roles");
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtRoles);

                    //string query1 = string.Format("SELECT details,permissionId FROM {0}.{1}", "Reference", "Permissions"); 
                    // Note : reverse it once details value available for Facility Dashboard
                    string query1 = string.Format(@"
                        SELECT 
                            CASE
                                WHEN details = 'Facility Report Card - Read & Write' THEN  'Facility Dashboard - Read & Write'
                                ELSE details
                            END AS details,permissionId FROM {0}.{1} order by details asc", "Reference", "Permissions");
                    cmd = new SqlCommand(query1, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtPermission);

                    string queryAccess = string.Format("SELECT roleId,permissionId,CONCAT(RoleId, '-', PermissionId) AS CombinedResult FROM {0}.{1}", "Reference", "Access");
                    cmd = new SqlCommand(queryAccess, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtAccess);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRolesPermission_Page_Load");
        }        
    }

    public void Reload()
    {
        try
        {

            permissions = CheckRole();
            if (permissions.Count > 0)
            {
                btnSave.Enabled = true;
            }
            else
            {
                btnUserRole.Visible = false;
                permissionRoleAccess.Attributes.Add("style", "opacity: 0.5; pointer-events: none;");
                btnSave.Enabled = false;
                btnSave.Visible = false;
                btnCancel.Enabled = false;
                btnCancel.Visible = false;
            }

            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string roleId = !string.IsNullOrEmpty(Request.QueryString["roleId"]) ? Convert.ToString(Request.QueryString["roleId"]) : "";
            hdfSchema.Value = schema;
            hdfTableName.Value = tableName;
            hdfDataKeyName.Value = "roleId";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = string.Format("SELECT details,roleId FROM {0}.{1}", "Reference", "Roles");
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtRoles);

                string query1 = string.Format("SELECT details,permissionId FROM {0}.{1}", "Reference", "Permissions");
                cmd = new SqlCommand(query1, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtPermission);

                string queryAccess = string.Format("SELECT roleId,permissionId,CONCAT(RoleId, '-', PermissionId) AS CombinedResult FROM {0}.{1}", "Reference", "Access");
                cmd = new SqlCommand(queryAccess, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtAccess);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRolesPermission_Reload");
        }
    }

    public dynamic CheckRole()
    {
        dynamic permissions = null;
        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

             permissions = dtAccessPermission.AsEnumerable()
                .Where(permission => permission.Field<string>("PermissionName") == "USER_WRITE")
                .Select(permission => new
                {
                    UserId = permission.Field<string>("userId"),
                    RoleId = permission.Field<string>("RoleId"),
                    RoleName = permission.Field<string>("RoleName"),
                    RoleDetails = permission.Field<string>("RoleDetails"),
                    PermissionId = permission.Field<string>("PermissionId"),
                    PermissionName = permission.Field<string>("PermissionName")
                })
                .ToList();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRolesPermission_CheckRole");
        }
        return permissions;
    }
    
    protected void btnUserManage_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementPage.aspx?schema=MBDR_System&table=Users");
    }

    public bool CheckAccess(int roleID, int permissionID)
    {
        
        // Create a SQL command to check if a matching record exists
        
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string query = "SELECT COUNT(*) FROM Reference.Access WHERE RoleID = @RoleID AND PermissionID = @PermissionID";
            SqlCommand command = new SqlCommand(query, conn);
            // Set the parameters
            command.Parameters.AddWithValue("@RoleID", roleID);
            command.Parameters.AddWithValue("@PermissionID", permissionID);
            command.CommandTimeout = 0;

            conn.Open();
            // Execute the query and get the count
            int count = (int)command.ExecuteScalar();
            conn.Close();
            // If the count is greater than 0, access is granted
            return count > 0;
        }
    }

    public DataTable GetExisitingData()
    {
        DataTable dt = new DataTable();

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
           
            string query = string.Format("SELECT * FROM {0}.{1}", "Reference", "Access");

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;

            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dt);
        }
        return dt;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        // Keep track of the checked checkboxes
        List<string> checkedIds = new List<string>();       

        // Find the checkbox control by its ID
        //CheckBox checkbox = FindControl(checkboxId) as CheckBox;
        //bool chk = checkbox.Checked;
        var items = Request.Form["chkname"];
        string[] rolepermissionId = items.Split(',');

        DataTable dtExit = GetExisitingData();
        if (dtExit != null && dtExit.Rows.Count>0)
        {
            foreach (DataRow drExist in dtExit.Rows)
            {
                int existroleID = Convert.ToInt32(drExist["roleId"]);
                int existpermissionID = Convert.ToInt32(drExist["permissionId"]);
                bool isExist = false;
                for (int i = 0; i < rolepermissionId.Length; i++)
                {
                    string[] ids = rolepermissionId[i].Split('-');
                    int roleID = Convert.ToInt32(ids[0]);
                    int permissionID = Convert.ToInt32(ids[1]);
                    if (roleID == Convert.ToInt32(drExist["roleId"]) && permissionID == Convert.ToInt32(drExist["permissionId"]))
                    {
                        isExist = true;
                        break;
                    }
                }

                if (!isExist)
                {
                    DeleteUncheckedAccess(existroleID, existpermissionID);
                }

            }
        }
        for (int i = 0; i < rolepermissionId.Length; i++)
        {
            string[] ids = rolepermissionId[i].Split('-');
            int roleID = Convert.ToInt32(ids[0]);
            int permissionID = Convert.ToInt32(ids[1]);
            SaveAccess(roleID, permissionID);
        }

        Reload();
        ClientScript.RegisterStartupScript(this.GetType(), "message", "AlertMessage('Data Saved Successfully.');", true);
    }

    private void DeleteUncheckedAccess(int roleID, int permissionID)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string deleteQuery = "DELETE FROM Reference.Access WHERE RoleID = @RoleID AND PermissionID = @PermissionID";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery, connection);

                deleteCommand.Parameters.Clear();
                deleteCommand.Parameters.AddWithValue("@RoleID", roleID);
                deleteCommand.Parameters.AddWithValue("@PermissionID", permissionID);
                deleteCommand.CommandTimeout = 0;

                connection.Open();
                deleteCommand.ExecuteNonQuery();
                connection.Close();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "UserManagementRolesPermission_DeleteUncheckedAccess");
        }
    }

    private void SaveAccess(int roleID, int permissionID)
    {
        // Check if the data already exists in the access table
        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string checkQuery = "SELECT COUNT(*) FROM Reference.Access WHERE RoleID = @RoleID AND PermissionID = @PermissionID";
            SqlCommand checkCommand = new SqlCommand(checkQuery, connection);
            checkCommand.Parameters.AddWithValue("@RoleID", roleID);
            checkCommand.Parameters.AddWithValue("@PermissionID", permissionID);
            checkCommand.CommandTimeout = 0;

            connection.Open();
            int count = (int)checkCommand.ExecuteScalar();
            connection.Close();
            if (count == 0)
            {
                // Data doesn't exist, so insert it into the access table
                string insertQuery = "INSERT INTO Reference.Access (RoleID, PermissionID) VALUES (@RoleID, @PermissionID)";
                SqlCommand insertCommand = new SqlCommand(insertQuery, connection);
                insertCommand.Parameters.AddWithValue("@RoleID", roleID);
                insertCommand.Parameters.AddWithValue("@PermissionID", permissionID);
                insertCommand.CommandTimeout = 0;

                connection.Open();
                insertCommand.ExecuteNonQuery();
                connection.Close();
            }
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        
    }

    protected void btnUserRole_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementRoles.aspx?Schema=Reference&Table=Roles");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/UserManagementRolesPermission.aspx?schema=Reference&table=Permission");
    }
}
