﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class DefaultDataViewMatching : System.Web.UI.Page
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
                string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                string searchText = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string columnName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
                string facility = !string.IsNullOrEmpty(Request.QueryString["facility"]) ? Convert.ToString(Request.QueryString["facility"]) : "";

                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                hdfSearch.Value = searchText;
                hdfColumnName.Value = columnName;
                hdfName.Value = name;
                hdfKeyName.Value = keyName;
                hdfRowID.Value = rowID;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                hdfFromDate.Value = fromDate;
                hdfToDate.Value = toDate;
                hdfChangeFacility.Value = facility;
                if (model != null)
                {
                    ltrTableName.Text = "REFERENCE TABLES  DETAILS";
                }
                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    BindColumnsDropDown(schema, tableName);
                    BindGrid(tableName, schema, keyName, rowID);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_Page_Load");
        }       
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dtDropDown = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtDropDown);
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                }

                foreach (DataColumn item in dtDropDown.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }
                    }

                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_BindColumnsDropDown");
        }
    }

    public void BindGrid(string tableName, string schema, string keyName, string rowID)
    {
        try
        {
            string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();

            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            List<ColumnList> lstRecord = new List<ColumnList>();
            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");

                //gvDynamic.Columns.Clear();
                List<string> lstColumns = includeColumns.Split(',').ToList();


                DataTable dtActualRow = new DataTable();
                DataSet dsRows = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    int row = Convert.ToInt32(rowID);
                    string query = "";
                    query = string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, rowID);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row + 1);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row - 1);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dsRows);
                }

                if (dsRows.Tables[1].Rows.Count > 0)
                {
                    btnNext.Enabled = true;
                    btnNext.CssClass = "btn btn-success";
                }
                else
                {
                    btnNext.Enabled = false;
                    btnNext.CssClass = "btn btn-disabled";
                }

                if (dsRows.Tables[2].Rows.Count > 0)
                {
                    btnPrev.Enabled = true;
                    btnPrev.CssClass = "btn btn-success";
                }
                else
                {
                    btnPrev.Enabled = false;
                    btnPrev.CssClass = "btn btn-disabled";
                }
                dtActualRow = dsRows.Tables[0];
                if (dtActualRow.Rows.Count > 0)
                {
                    //if (dtActualRow.Columns.Contains("child_first_name"))
                    //{
                    //    //ltrName.Text = string.Format("{0} {1}", dtActualRow.Rows[0]["child_first_name"], dtActualRow.Rows[0]["child_last_name"]);
                    //    ltrReportName.Text = string.Format("{0} {1}", dtActualRow.Rows[0]["child_first_name"], dtActualRow.Rows[0]["child_last_name"]);
                    //}
                    //else
                    //{
                    //    //ltrName.Text = string.Format("{0} {1}", dtActualRow.Rows[0]["baby_first_name"], dtActualRow.Rows[0]["baby_last_name"]);
                    //    ltrReportName.Text = string.Format("{0} {1}", dtActualRow.Rows[0]["baby_first_name"], dtActualRow.Rows[0]["baby_last_name"]);
                    //}
                    //ltrFacilitySouce.Text = string.Format("{0}", hdfName.Value);// dtActualRow.Rows[0]["FacilitySource"]);
                }

                string compareQuery = "";
                DataSet dsTables = new DataSet();
                dt = new DataTable();
                dt.Columns.Add("Columns");
                dt.Columns.Add(model.Table.Split(':')[1]);
                List<string> lstNavigation = new List<string>();
                if (model.ComparisonTables.Count > 0)
                {
                    if (!model.ComparisonTables.Any(s => s.TableName == ""))
                    {
                        foreach (var item in model.ComparisonTables)
                        {
                            if (!string.IsNullOrEmpty(item.TableName))
                            {
                                string[] lstTableNames = item.TableName.Split(':');
                                string key = item.FKey.Split(':')[0];
                                string key1 = item.FKey.Split(':')[1];
                                if (!string.IsNullOrEmpty(lstTableNames[1]))
                                {
                                    lstNavigation.Add(lstTableNames[1]);
                                    dt.Columns.Add(lstTableNames[1]);
                                }
                                else
                                {
                                    lstNavigation.Add(lstTableNames[0]);
                                    dt.Columns.Add(lstTableNames[0]);
                                }
                                compareQuery = compareQuery + " select * from " + item.Schema + "." + lstTableNames[0] + " where " + key + " = '" + dtActualRow.Rows[0][key1] + "'";
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(compareQuery))
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand(compareQuery, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        for (int i = 0; i < lstNavigation.Count; i++)
                        {
                            if (i == 0)
                            {
                                da.TableMappings.Add("Table", lstNavigation[i]);
                            }
                            else
                            {
                                da.TableMappings.Add("Table" + (i) + "", lstNavigation[i]);
                            }
                        }
                        da.Fill(dsTables);
                    }
                }
                foreach (var column in lstColumns)
                {
                    if (!string.IsNullOrEmpty(column))
                    {

                        foreach (DataColumn item in dtActualRow.Columns)
                        {
                            string colName = item.ColumnName;
                            string columnName = column.Contains("->") ? column.Split('>')[1] : column;

                            DataRow dr = dt.NewRow();
                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                string displayColname = string.Empty;
                                if (!string.IsNullOrEmpty(colName))
                                {
                                    dr[0] = columnInfo;
                                    displayColname = columnInfo;
                                }
                                else
                                {
                                    dr[0] = item.ColumnName;
                                    displayColname = item.ColumnName;
                                }
                                if (displayColname != "RecordFrom")
                                    lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]) });
                                dr[model.Table.Split(':')[1]] = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]);
                                for (int j = 0; j < lstNavigation.Count; j++)
                                {
                                    try
                                    {
                                        string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(lstNavigation[j])).FirstOrDefault().ColumnMapping;
                                        if (colMapping.Contains(item.ColumnName))
                                        {
                                            string strName = Common.GetBetween(colMapping, item.ColumnName, ",");
                                            dr[lstNavigation[j]] = Convert.ToString(dsTables.Tables[lstNavigation[j]].Rows[0][strName.Split(':')[1]]);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        dr[lstNavigation[j]] = "";
                                    }

                                }
                                if (!string.IsNullOrEmpty(hdfSearchText.Value))
                                {
                                    if (item.ColumnName == hdfSearchText.Value)
                                    {
                                        dt.Rows.Add(dr);
                                    }
                                }
                                else
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }
            }
            gvDynamic.DataSource = dt;
            gvDynamic.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                btnReset_Click(this, EventArgs.Empty);
            }
        }        
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hdfSearchText.Value = Convert.ToString(ddlColumn.SelectedValue);
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_btnSearch_Click");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ddlColumn.SelectedIndex = 0;

            hdfSearchText.Value = "";
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_btnReset_Click");
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            int row = Convert.ToInt32(hdfRowID.Value);
            Response.Redirect("DefaultDataViewMatching.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row - 1) + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_btnPrev_Click");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            int row = Convert.ToInt32(hdfRowID.Value);
            Response.Redirect("DefaultDataViewMatching.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row + 1) + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_btnNext_Click");
        }
    }

    protected void btnAll_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdfSchema.Value == "UI_MBDR_TOTINT")
            {
                Response.Redirect("DefaultTotintView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "&fromDate=" + hdfFromDate.Value + "&toDate=" + hdfToDate.Value + "&facility=" + hdfChangeFacility.Value + "");
            }
            else
            {
                Response.Redirect("DefaultDataView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_btnAll_Click");
        }
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#e3f4ff");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultDataViewMatching_gvDynamic_RowDataBound");
        }
    }
}