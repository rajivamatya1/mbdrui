﻿<%@ Page Title="Audit" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="Audit.aspx.cs" Inherits="DynamicGridView.Audit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
            <asp:HiddenField runat="server" ID="hdfTableName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="isResetClick" Value="" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" />Audit</h1>
        </div>
    </div>
        <div class="row">
        <div class="row">

            <div class="col col-lg-12 col-sm-12">
                <div class="form-item displayGrid">
                    <label class="fontDDSize">Component <i>(required)</i></label>
                    <asp:DropDownList runat="server" ID="ddlComp" CssClass="form-control fieldRadius" ValidationGroup="search" AutoPostBack="true" OnSelectedIndexChanged="ddlComp_SelectedIndexChanged">
                        <asp:ListItem Text="User Management" Value="userManage" />
                        <asp:ListItem Text="Case Verification " Value="caseVerify" />
                    </asp:DropDownList>
                </div>
                <div class="form-item displayGrid">
                    <label class="fontDDSize">Performed By</label>
                    <asp:DropDownList runat="server" ID="ddlPerform" CssClass="form-control fieldRadius" ValidationGroup="search" AutoPostBack="true" OnSelectedIndexChanged="ddlPerform_SelectedIndexChanged">
                    </asp:DropDownList>

                </div>
                <div class="form-item displayGrid" id="divDisplayName" runat="server">
                    <label class="fontDDSize">Display Name</label>
                    <asp:DropDownList runat="server" ID="ddlDisplay" CssClass="form-control fieldRadius" ValidationGroup="search" AutoPostBack="true" OnSelectedIndexChanged="ddlDisplay_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="row pt-1">
                <div class="col col-lg-12 col-sm-12 pt-1">
                    <div class="form-item" runat="server" id="dvFromDate">
                        <label class="fontDDSize">Action From Date</label>
                        <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateFrom" ValidationGroup="search" onblur="ValidateFromDate(this);" />
                        <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" ID="CustomValidator1" runat="server" ValidationGroup="search" ClientValidationFunction="ValidateFromDate" ControlToValidate="txtDateFrom" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                    </div>
                    <div class="form-item" runat="server" id="dvToDate">
                        <label class="fontDDSize">Action To Date</label>
                        <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateTo" ValidationGroup="search" onblur="ValidateToDate(this);" />
                        <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ID="CustomValidator2" ValidationGroup="search" ClientValidationFunction="ValidateToDate" ControlToValidate="txtDateTo" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                    </div>
                    <div class="form-item">
                        <label class="fontDDSize"></label>
                        <asp:Button Text="Audit" runat="server" ID="btnSearch" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-danger" />
                    </div>

                    <div class="form-item">
                        <label class="fontDDSize"></label>
                        <asp:Button Text="Reset " runat="server" ID="btnReset" CausesValidation="false" OnClientClick="return ShowProgress();" OnClick="btnReset_Click" CssClass="btn btn-danger" />
                    </div>

                    <div class="form-item">
                        <label class="fontDDSize"></label>
                        <asp:Button Text="Export " runat="server" ID="btnExport" CausesValidation="false" Enabled="false" OnClick="btnExport_Click" CssClass="btn btn-danger grey-color" />
                    </div>
                </div>
                <div class="pt-1" id="searchreport" runat="server" visible="true">
                    <div class="row">
                        <div class="col col-lg-12 col-sm-12 pt-1">
                            <div class="tableFixHead pt-1">

                                <% if (ddlComp.SelectedValue == "userManage")
                                    {  %>
                                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                                    AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                                    <Columns>
                                        <asp:BoundField DataField="modifier" HeaderText="Performed By" SortExpression="modifier" />
                                        <asp:BoundField DataField="modified" HeaderText="Action Date/Time" SortExpression="modified" />
                                        <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                                        <asp:BoundField DataField="displayName" HeaderText="Display Name" SortExpression="displayName" />
                                        <asp:BoundField DataField="newFirstName" HeaderText="First Name" SortExpression="newFirstName" />
                                        <asp:BoundField DataField="newLastName" HeaderText="Last Name" SortExpression="newLastName" />
                                        <asp:BoundField DataField="newEmailAddress" HeaderText="Email Address" SortExpression="newEmailAddress" />
                                        <asp:BoundField DataField="newRole" HeaderText="Role Type" SortExpression="newRole" />
                                    </Columns>
                                </asp:GridView>
                                <%}
                                    else if (ddlComp.SelectedValue == "caseVerify")
                                    {%>
                                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamicCaseVerification_Sorting" OnRowDataBound="gvDynamicCaseVerification_RowDataBound"
                                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamicCaseVerification" OnPageIndexChanging="gvDynamicCaseVerification_PageIndexChanging"
                                    AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                                    <Columns>
                                        <asp:BoundField DataField="performedBy" HeaderText="Performed By" SortExpression="performedBy" />
                                        <asp:BoundField DataField="actionDateTime" HeaderText="Action Date/Time" SortExpression="actionDateTime" />
                                        <asp:BoundField DataField="reportId" HeaderText="Report ID" SortExpression="reportId" />
                                        <asp:BoundField DataField="actionType" HeaderText="Type" SortExpression="actionType" />
                                        <asp:BoundField DataField="prenatalCodes" HeaderText="Prenatal Codes" SortExpression="prenatalCodes" />
                                        <asp:BoundField DataField="ICD10DiagnosisCode" HeaderText="ICD-10 Codes" SortExpression="ICD10DiagnosisCode" />
                                        <asp:BoundField DataField="indicationOrStatus" HeaderText="Indication/Status" SortExpression="indicationOrStatus" />
                                        <asp:BoundField DataField="dateOfEarliestCCHD" HeaderText="Date Of Earliest" SortExpression="dateOfEarliestCCHD" />
                                        <asp:BoundField DataField="methods" HeaderText="Method" SortExpression="methods" />
                                        <asp:BoundField DataField="comment" HeaderText="Comment" SortExpression="comment" />
                                    </Columns>
                                </asp:GridView>
                                <%}  %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
         <br />
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>

    <div id="commonMsg">
    </div>

    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
            }
        });

        /*** will trigger search button by enter ***/

        function ShowProgress() {
            hideReport();
            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function hideReport() {
            var $searchReportElement = $("#<%=searchreport.ClientID%>");

            if ($searchReportElement.length) {
                $searchReportElement.hide();
            }
        }

        function noRecFound(message, titleFor) {
            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    /*  var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')*/
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

        function ValidateToDate(input) {
            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID %>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");
            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");


            if (txtTo.length === 10) {
                hideReport();
                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtTo === "" || txtTo === 'yyyy-mm-dd') {

                    fromDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                 }
                 else if (!dateRegex.test(txtTo)) {

                     toDateValidator.style.display = "block";
                     toDateValidator.innerHTML = "Invalid Date";
                     $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                 }
                 else {

                     var minDate = new Date("1900-01-01");
                     // parse the date

                     var checkDateIfisValid = new Date(txtTo);

                     //check if it is validate date or not

                     if (isNaN(checkDateIfisValid.getTime())) {
                         toDateValidator.style.display = "block";
                         toDateValidator.innerHTML = "Invalid Date";
                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         return false;
                     }

                     //check if the date is before 01/01/1900

                     if (checkDateIfisValid < minDate) {
                         toDateValidator.style.display = "block";
                         toDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         return false;
                     }

                     //check if the date is after today's date

                     var maxDate = new Date();

                     if (checkDateIfisValid > maxDate) {
                         toDateValidator.style.display = "block";
                         toDateValidator.innerHTML = "Date cannot be in the future.";
                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                         return false;
                     }

                     if (txtTo !== null) {
                         if (txtFrom.length === 0) {

                             toDateValidator.style.display = "none";
                             fromDateValidator.style.display = "none";

                             $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                     return true;

                 } else if (txtTo < txtFrom) {
                     if (!dateRegex.test(txtFrom)) {

                         fromDateValidator.style.display = "block";
                         fromDateValidator.innerHTML = "Invalid Date";

                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                     } else {

                         fromDateValidator.style.display = "none";

                         toDateValidator.style.display = "block";
                         toDateValidator.innerHTML = "Must be before from date";

                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                         return false;
                     }
                 }
                 else {
                     if (!dateRegex.test(txtFrom)) {

                         fromDateValidator.style.display = "block";
                         fromDateValidator.innerHTML = "Invalid Date";

                         $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                     }
                     else {
                         toDateValidator.style.display = "none";
                         fromDateValidator.style.display = "none";

                         $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                         return true;
                     }
                 }
             }
             else {

                 toDateValidator.style.display = "none";
                 fromDateValidator.style.display = "none";

                 $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                         return true;
                         //}
                     }
                 }
             } else {
                 document.getElementById("<%=txtDateTo.ClientID%>").value = "";
            }

        }

        function ValidateFromDate(input) {
            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID%>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");
            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");

            if (txtFrom.length === 10) {
                hideReport();
                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtFrom === "" || txtFrom === 'yyyy-mm-dd') {

                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

         }
         else if (!dateRegex.test(txtFrom)) {

             fromDateValidator.style.display = "block";
             fromDateValidator.innerHTML = "Invalid Date";

             $("#<%=btnSearch.ClientID%>").prop("disabled", true);
         }
         else {


             var minDate = new Date("1900-01-01");
             // parse the date

             var checkDateIfisValid = new Date(txtFrom);

             //check if it is validate date or not

             if (isNaN(checkDateIfisValid.getTime())) {
                 fromDateValidator.style.display = "block";
                 fromDateValidator.innerHTML = "Invalid Date";
                 $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                 return false;
             }

             //check if the date is before 01/01/1900

             if (checkDateIfisValid < minDate) {
                 fromDateValidator.style.display = "block";
                 fromDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                 $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                 return false;
             }

             //check if the date is after today's date

             var maxDate = new Date();

             if (checkDateIfisValid > maxDate) {
                 fromDateValidator.style.display = "block";
                 fromDateValidator.innerHTML = "Date cannot be in the future.";
                 $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                 return false;
             }

             var checkDateIfisValidToDate = new Date(txtTo);

             if (checkDateIfisValidToDate > maxDate) {
                 toDateValidator.style.display = "block";
                 toDateValidator.innerHTML = "Date cannot be in the future.";
                 $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                 return false;
             }

             if (txtTo.length > 0) {

                 if (txtFrom > txtTo) {

                     toDateValidator.style.display = "none";

                     fromDateValidator.style.display = "block";
                     fromDateValidator.innerHTML = "Must be before to date";


                     $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                      return false;

                  }
                  else if (!dateRegex.test(txtTo)) {

                      toDateValidator.style.display = "block";
                      toDateValidator.innerHTML = "Invalid Date";
                      $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                  }
                  else {
                      fromDateValidator.style.display = "none";
                      toDateValidator.style.display = "none";
                      $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                      return true;
                  }
              }
              else if (txtFrom <= txtTo) {

                  toDateValidator.style.display = "none";

                  fromDateValidator.style.display = "block";
                  fromDateValidator.innerHTML = "Must be before to date.";
                  $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                  return false;

              }
              else {
                  fromDateValidator.style.display = "none";
                  toDateValidator.style.display = "none";
                  $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                 return true;
             }
         }
     }
     else {
                document.getElementById("<%=txtDateFrom.ClientID%>").value = "";

                if (txtTo) {

                    fromDateValidator.style.display = "none";
                    toDateValidator.style.display = "none";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                    return true;
                }
            }

        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });


        function cAlertMsgs(message, titleFor) {

            $("#commonMsg").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

    </script>
</asp:Content>
