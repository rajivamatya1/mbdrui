﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FileLogDetail.aspx.cs" Inherits="FileLogDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfRowId" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
            <asp:HiddenField runat="server" ID="hdfTable" Value="" />
            <asp:HiddenField runat="server" ID="hdfFileName" Value="" />
            <asp:HiddenField runat="server" ID="hdfSubmitterName" Value="" />

            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button Text="Return to File Load Activity Log" runat="server" ID="btnReturnAL" OnClick="btnReturnAL_Click" CssClass="btn btn-primary" OnClientClick="return ShowProgress();" />
                                     <asp:Button Text="Return to Case Reports Validation Errors" runat="server" ID="btnReturnCRVE" OnClick="btnReturnCRVE_Click" CssClass="btn btn-primary" OnClientClick="return ShowProgress();"  />
                                    <asp:Button Text="Return to Activity and Error Logs" runat="server" ID="btnReturnAaEL" OnClick="btnReturnAaEL_Click" CssClass="btn btn-primary" OnClientClick="return ShowProgress();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="admin-workarea">
                        <br>
                        <div class="col col-lg-12 col-sm-12">
                            <div class="form-item">
                                <label for="Content_txtSearch">Search by Keyword</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch" ValidationGroup="search" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSearch" ValidationGroup="search" ValidationExpression="[a-zA-Z0-9]*[^!@%~?:#$%^&*=()0']*" runat="server" ErrorMessage="Invalid Input" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-item">
                                <label for="Content_ddlColumn">In Column</label>
                                <asp:DropDownList runat="server" ID="ddlColumn" CssClass="form-control" ValidationGroup="search">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="ddlColumn"></asp:RequiredFieldValidator>
                            </div>

                            <div class="form-item">
                                <asp:Button Text="Search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-success" ValidationGroup="search" />
                            </div>
                            <div class="form-item">
                                <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click" CssClass="btn btn-danger" />
                            </div>
                        </div>
                        <div class="col col-lg-12 col-sm-12">
                            <div class="tableFixHead">
                                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting"
                                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
    <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
    <br />
    Loading ... Please wait ...
   <br />
</div>

    <script>

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ShowProgress(para) {

            var modal = $('<div />');
            modal.addClass("modalspin");
            $('body').append(modal);
            var loading = $(".loadingspin");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ "position": "center", top: top, left: left });

            return true;
        }

    </script>

</asp:Content>

