﻿
using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;


public partial class MLSideBySideView : System.Web.UI.Page
{

    protected DataTable dtAllRecords;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            if (!IsPostBack)
            {

                int scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"])) : 0;
                int archiveId = Convert.ToInt32(Convert.ToString(Request.QueryString["archiveId"]));
                string columnname = !string.IsNullOrEmpty(Request.QueryString["columnname"]) ? Convert.ToString(Convert.ToString(Request.QueryString["columnname"])) : "";
                string columnvalue = !string.IsNullOrEmpty(Request.QueryString["columnvalue"]) ? Convert.ToString(Convert.ToString(Request.QueryString["columnvalue"])) : "";
                string reportType = !string.IsNullOrEmpty(Request.QueryString["reportType"]) ? Convert.ToString(Convert.ToString(Request.QueryString["reportType"])) : "";
                hdfReportType.Value = reportType;

                string schema = "MBDR_System";

                //Records view on link click
                DataSet dsRows = new DataSet();
                DataTable dataTableML = new DataTable();
                DataTable dtCBD = new DataTable();
                DataTable dtCaseReport = new DataTable();
                DataTable dtBirthRecord = new DataTable();
                DataTable dtDeathRecord = new DataTable();
                string includeColumns = string.Empty;

                dtAllRecords = new DataTable();
                List<MLPossibleMatchChart> lstAllMatches = new List<MLPossibleMatchChart>();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string query = "";
                    string where = "where scrutinizerId=" + scrutinizerId + "";
                    query = string.Format("select top 1 * from {0}.{1} {2}", schema, "MCBDMatchinLinkingView", where);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dataTableML);
                }
                //dsRows.Tables.Add(dataTableML);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string query = "";
                    if (!string.IsNullOrEmpty(columnname) && !string.IsNullOrEmpty(columnvalue))
                    {
                        string where = "where scrutinizerId=" + scrutinizerId + " and " + columnname + "='" + columnvalue + "'";
                        query = string.Format("select * from {0}.{1} {2}", schema, "MCBDMasterRecordsView", where);
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dtCBD);
                        
                    }
                }


                DataTable mergedTable = new DataTable();

                // Clone the structure of the first DataTable
                mergedTable = dataTableML.Clone();

                // Modify the data type of the column in mergedTable to match the widest data type
                mergedTable.Columns["caserecid"].DataType = typeof(long);

                // Import the data from the first DataTable
                foreach (DataRow row in dataTableML.Rows)
                {
                    mergedTable.ImportRow(row);
                }

                // Modify the data type of the column in dtCBD to match the widest data type
                dtCBD.Columns["caserecid"].DataType = typeof(long);

                // Import the data from the second DataTable
                foreach (DataRow row in dtCBD.Rows)
                {
                    mergedTable.ImportRow(row);
                }

                if (reportType == "Case Reports")
                {
                    var queryBirthRecord = from DataRow row in mergedTable.Rows
                                           where row.Field<string>("TableName") == "Birth Record"
                                           select row;

                    var queryDeathRecord = from DataRow row in mergedTable.Rows
                                           where row.Field<string>("TableName") == "Death Record"
                                           select row;

                    // Birth Records avaialble with Reprot ID and Death ID but death and case reprot will not feact from 

                    foreach (DataRow dr in mergedTable.Rows)     //Case Report data fetech
                    {
                        if (Convert.ToString(dr["TableName"]) == "Case Report")
                        {
                            string birthId = Convert.ToString(dr["birthId"]);
                            if (!string.IsNullOrEmpty(birthId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtBirthRecord = new DataTable();
                                    string query = "";
                                    string where = "where birthId=" + birthId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideBirthRecordsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtBirthRecord);

                                }


                            }
                        }
                    }



                    if (queryBirthRecord.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                                   .Where(row => row.Field<string>("TableName") == "Birth Record")
                                   .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);
                        }
                    }

                    //dtCaseReport.Columns["caserecid"].DataType = typeof(long);
                    foreach (DataRow row in dtBirthRecord.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }


                    foreach (DataRow dr in mergedTable.Rows)     //Case Report data fetech
                    {
                        if (Convert.ToString(dr["TableName"]) == "Case Report")
                        {
                            string deathId = Convert.ToString(dr["deathId"]);
                            if (!string.IsNullOrEmpty(deathId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtDeathRecord = new DataTable();
                                    string query = "";
                                    string where = "where deathId=" + deathId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideDeathRecordsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtDeathRecord);

                                }


                            }
                        }
                    }

                    if (queryDeathRecord.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                              .Where(row => row.Field<string>("TableName") == "Death Record")
                              .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);
                        }
                    }

                    foreach (DataRow row in dtDeathRecord.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }


                }

                if (reportType == "Birth Records")
                {
                    var queryCaseReport = from DataRow row in mergedTable.Rows
                                          where row.Field<string>("TableName") == "Case Report"
                                          select row;

                    var queryDeathRecord = from DataRow row in mergedTable.Rows
                                           where row.Field<string>("TableName") == "Death Record"
                                           select row;

                    // Birth Records avaialble with Reprot ID and Death ID but death and case reprot will not feact from 


                    foreach (DataRow dr in mergedTable.Rows)     //Case Report data fetech
                    {
                        if (Convert.ToString(dr["TableName"]) == "Birth Record")
                        {
                            string caserepId = Convert.ToString(dr["ReportId"]);
                            if (!string.IsNullOrEmpty(caserepId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtCaseReport = new DataTable();
                                    string query = "";
                                    string where = "where reportId=" + caserepId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideCaseReportsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtCaseReport);

                                }

                            }
                        }
                    }
                    if (queryCaseReport.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                               .Where(row => row.Field<string>("TableName") == "Case Report")
                               .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);

                        }
                    }

                    //dtCaseReport.Columns["caserecid"].DataType = typeof(long);
                    foreach (DataRow row in dtCaseReport.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }                    


                    foreach (DataRow dr in mergedTable.Rows)     //Case Report data fetech
                    {
                        if (Convert.ToString(dr["TableName"]) == "Birth Record")
                        {
                            string deathId = Convert.ToString(dr["deathId"]);
                            if (!string.IsNullOrEmpty(deathId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtDeathRecord = new DataTable();
                                    string query = "";
                                    string where = "where deathId=" + deathId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideDeathRecordsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtDeathRecord);

                                }


                            }
                        }
                    }

                    if (queryDeathRecord.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                                 .Where(row => row.Field<string>("TableName") == "Death Record")
                                 .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);

                        }
                    }
                    //dtCaseReport.Columns["caserecid"].DataType = typeof(long);
                    foreach (DataRow row in dtDeathRecord.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }


                }

                if (reportType == "Death Records")
                {
                    var queryCaseReport = from DataRow row in mergedTable.Rows
                                          where row.Field<string>("TableName") == "Case Report"
                                          select row;

                    foreach (DataRow dr in mergedTable.Rows)
                    {
                        if (Convert.ToString(dr["TableName"]) == "Death Record")
                        {
                            string caserepId = Convert.ToString(dr["ReportId"]);
                            if (!string.IsNullOrEmpty(caserepId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtCaseReport = new DataTable();
                                    string query = "";
                                    string where = "where reportId=" + caserepId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideCaseReportsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtCaseReport);

                                }


                            }
                        }
                    }

                    if (queryCaseReport.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                                 .Where(row => row.Field<string>("TableName") == "Case Report")
                                 .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);

                        }
                    }
                    //dtCaseReport.Columns["caserecid"].DataType = typeof(long);
                    foreach (DataRow row in dtCaseReport.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }



                    var queryBirthRecord = from DataRow row in mergedTable.Rows
                                           where row.Field<string>("TableName") == "Birth Record"
                                           select row;

                    // Birth Records avaialble with Reprot ID and Death ID but death and case reprot will not feact from 
                    foreach (DataRow dr in mergedTable.Rows)     //Case Report data fetech
                    {
                        if (Convert.ToString(dr["TableName"]) == "Death Record")
                        {
                            string birthId = Convert.ToString(dr["birthId"]);
                            if (!string.IsNullOrEmpty(birthId))
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    dtBirthRecord = new DataTable();
                                    string query = "";
                                    string where = "where birthId=" + birthId + "";
                                    query = string.Format("select * from {0}.{1} {2}", schema, "SideBySideBirthRecordsView", where);
                                    SqlCommand cmd = new SqlCommand(query, conn);
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                                    conn.Close();
                                    da.Fill(dtBirthRecord);

                                }
                            }
                        }
                    }


                    if (queryBirthRecord.Any())
                    {
                        var rowsToRemove = mergedTable.AsEnumerable()
                                     .Where(row => row.Field<string>("TableName") == "Birth Record")
                                     .ToList();

                        foreach (var row in rowsToRemove)
                        {
                            mergedTable.Rows.Remove(row);

                        }
                    }
                    //dtCaseReport.Columns["caserecid"].DataType = typeof(long);
                    foreach (DataRow row in dtBirthRecord.Rows)
                    {
                        mergedTable.ImportRow(row);
                    }
                }

                dsRows.Tables.Add(mergedTable);

                lstAllMatches = ListMLPossibleMatchChart(dsRows);
                PropertyInfo[] propertie = typeof(MLPossibleMatchChart).GetProperties();
                var filter = Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith("Matching_Linking")).FirstOrDefault();
                if (filter != null)
                {
                    includeColumns = filter.IncludedColumns;

                }

                List<string> lstColumn = includeColumns.Split(',').ToList();

                for (int i = 0; i < lstAllMatches.Count + 1; i++)
                {
                    dtAllRecords.Columns.Add();
                }
                // Binding data

                for (int i = 0; i < propertie.Length; i++)
                {
                    DataRow dr = dtAllRecords.NewRow();
                    var colName = lstColumn.Find(s => s.StartsWith(propertie[i].Name));
                    string displayColname = string.Empty;

                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnNames = colName.Split(':')[1];
                        dr[0] = columnNames;
                        displayColname = columnNames;
                    }
                    else
                    {
                        displayColname = propertie[i].Name;
                        foreach (var col in lstColumn)
                        {
                            string columnNames = col.Contains("->") ? col.Split('>')[1] : col;
                            if (string.Equals(columnNames.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                            {
                                dr[0] = columnNames.Split(':')[1];
                            }
                        }
                    }

                    for (int j = 0; j < lstAllMatches.Count; j++)
                    {
                        dr[j + 1] = Convert.ToString(propertie[i].GetValue(lstAllMatches[j], null));
                    }

                    dtAllRecords.Rows.Add(dr);
                }

            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingViewDetails_Page_load");
        }
    }

    protected List<MLPossibleMatchChart> ListMLPossibleMatchChart(DataSet dsRows)
    {
        List<MLPossibleMatchChart> listMasterChart = new List<MLPossibleMatchChart>();

        string tableRecName = string.Empty;
        foreach (DataRow dr in dsRows.Tables[0].Rows)
        {
            var item = new MLPossibleMatchChart();
            item.RecordName = Convert.ToString(dr["TableName"]);
            item.CaseRecID = Convert.ToString(dr["caseRecId"]);
            item.ReportID = Convert.ToString(dr["reportId"]);
            item.MasterRecordNumber = Convert.ToString(dr["masterRecordNumber"]);
            item.DeathNumber = Convert.ToString(dr["deathNumber"]);
            item.ChildLastName = Convert.ToString(dr["childLastName"]);
            item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
            item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
            item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
            item.ChildSSN = Convert.ToString(dr["childSSN"]);
            item.MomFirstName = Convert.ToString(dr["momFirstName"]);
            item.MomLastName = Convert.ToString(dr["momLastName"]);
            item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
            item.MomSuffix = Convert.ToString(dr["momSuffix"]);
            item.Address1 = Convert.ToString(dr["address1"]);
            item.City = Convert.ToString(dr["city"]);
            item.County = Convert.ToString(dr["county"]);
            item.State = Convert.ToString(dr["state"]);
            item.Country = Convert.ToString(dr["country"]);
            item.ZipCode = Convert.ToString(dr["zipcode"]);
            item.ChildMRN = Convert.ToString(dr["childMRN"]);
            item.Gender = Convert.ToString(dr["gender"]);
            item.Plurality = Convert.ToString(dr["plurality"]);
            item.BirthOrder = Convert.ToString(dr["birthOrder"]);
            item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
            item.BirthWeight = Convert.ToString(dr["birthWeight"]);
            item.BirthDate = Convert.ToString(dr["birthDate"]);
            item.BirthHospital = Convert.ToString(dr["birthHospital"]);

            listMasterChart.Add(item);
        }


        List<MLPossibleMatchChart> listMasterChartNew = new List<MLPossibleMatchChart>();
        MLPossibleMatchChart items;

        foreach (MLPossibleMatchChart item in listMasterChart)
        {
            items = new MLPossibleMatchChart();

            if (item.RecordName == "Matching Linking")
            {

                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Matching Linking");
                if (existingRecord != null)
                {
                    listMasterChartNew.Remove(existingRecord);
                }
                items = SetItemProperties(item);
                listMasterChartNew.Add(items);
                break;
            }
            else
            {
                // Set the properties to default values and add table name
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Matching Linking");
                if (existingRecord == null)
                {
                    items.RecordName = "Matching Linking";
                    SetDefaultProperties(items);
                    listMasterChartNew.Add(items);
                }
            }
        }
        string caseRecId = string.Empty;
        foreach (MLPossibleMatchChart item in listMasterChart)
        {
            items = new MLPossibleMatchChart();

            if (item.RecordName == "Case Report")
            {
                // Set the properties from the item
                //caseRecId = item.
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Case Report");
                if (existingRecord != null)
                {
                    listMasterChartNew.Remove(existingRecord);
                }
                items = SetItemProperties(item);
                listMasterChartNew.Add(items);
                break;
            }
            else
            {
                // Set the properties to default values and add table name

                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Case Report");
                if (existingRecord == null)
                {
                    items.RecordName = "Case Report";
                    SetDefaultProperties(items);
                    listMasterChartNew.Add(items);
                }

            }

        }

        foreach (MLPossibleMatchChart item in listMasterChart)
        {
            items = new MLPossibleMatchChart();

            if (item.RecordName == "Birth Record")
            {
                // Set the properties from the item
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Birth Record");
                if (existingRecord != null)
                {
                    listMasterChartNew.Remove(existingRecord);
                }
                items = SetItemProperties(item);
                listMasterChartNew.Add(items);
                break;
            }
            else
            {
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Birth Record");
                if (existingRecord == null)
                {
                    items.RecordName = "Birth Record";
                    SetDefaultProperties(items);
                    listMasterChartNew.Add(items);
                }
            }
        }




        foreach (MLPossibleMatchChart item in listMasterChart)
        {
            items = new MLPossibleMatchChart();

            if (item.RecordName == "Death Record")
            {
                // Set the properties from the item
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Death Record");
                if (existingRecord != null)
                {
                    listMasterChartNew.Remove(existingRecord);
                }
                items = SetItemProperties(item);

                listMasterChartNew.Add(items);
                break;
            }
            else
            {
                MLPossibleMatchChart existingRecord = listMasterChartNew.Find(r => r.RecordName == "Death Record");
                if (existingRecord == null)
                {
                    items.RecordName = "Death Record";
                    SetDefaultProperties(items);
                    listMasterChartNew.Add(items);
                }
            }

        }

        return listMasterChartNew;

        // Helper methods
    }

    private void UpdateCaseReportItem(MLPossibleMatchChart caseReportItem, MLPossibleMatchChart item)
    {
        throw new NotImplementedException();
    }

    private MLPossibleMatchChart SetItemProperties(MLPossibleMatchChart item)
    {
        MLPossibleMatchChart newItem = new MLPossibleMatchChart();
        newItem.RecordName = item.RecordName;
        newItem.CaseRecID = item.CaseRecID;
        newItem.ReportID = item.ReportID;
        newItem.MasterRecordNumber = item.MasterRecordNumber;
        newItem.DeathNumber = item.DeathNumber;
        newItem.ChildLastName = item.ChildLastName;
        newItem.ChildFirstName = item.ChildFirstName;
        newItem.ChildMiddleName = item.ChildMiddleName;
        newItem.ChildSuffix = item.ChildSuffix;
        newItem.ChildSSN = item.ChildSSN;
        newItem.MomFirstName = item.MomFirstName;
        newItem.MomLastName = item.MomLastName;
        newItem.MomMiddleName = item.MomMiddleName;
        newItem.MomSuffix = item.MomSuffix;
        newItem.Address1 = item.Address1;
        newItem.City = item.City;
        newItem.County = item.County;
        newItem.State = item.State;
        newItem.Country = item.Country;
        newItem.ZipCode = item.ZipCode;
        newItem.ChildMRN = item.ChildMRN;
        newItem.Gender = item.Gender;
        newItem.Plurality = item.Plurality;
        newItem.BirthOrder = item.BirthOrder;
        newItem.VitalStatus = item.VitalStatus;
        newItem.BirthWeight = item.BirthWeight;
        newItem.BirthDate = item.BirthDate;
        newItem.BirthHospital = item.BirthHospital;
        return newItem;
    }

    // Sets the default properties of the items and adds the table name
    private void SetDefaultProperties(MLPossibleMatchChart item)
    {
        item.CaseRecID = null;
        item.ReportID = null;
        item.MasterRecordNumber = null;
        item.DeathNumber = null;
        item.ChildLastName = null;
        item.ChildFirstName = null;
        item.ChildMiddleName = null;
        item.ChildSuffix = null;
        item.ChildSSN = null;
        item.MomFirstName = null;
        item.MomLastName = null;
        item.MomMiddleName = null;
        item.MomSuffix = null;
        item.Address1 = null;
        item.City = null;
        item.County = null;
        item.State = null;
        item.Country = null;
        item.ZipCode = null;
        item.ChildMRN = null;
        item.Gender = null;
        item.Plurality = null;
        item.BirthOrder = null;
        item.VitalStatus = null;
        item.BirthWeight = null;
        item.BirthDate = null;
        item.BirthHospital = null;
    }

}