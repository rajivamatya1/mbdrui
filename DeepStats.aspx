﻿<%@ Page Title="Match Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="DeepStats.aspx.cs" Inherits="DeepStats" EnableEventValidation="false"%>


<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
   <%-- <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
    <style type="text/css">
        #primary-icon, #divGreen, #divRed, div.logo, img.bell-icon, #cVer
        {
         display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
  <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg" >
        Match Details</h1>
        <div class="row-view-matching">
            <div class="col col-lg-12 col-sm-12">                
                <div>
                    <asp:Label ID="lblMsgQuery" runat="server" Text="Label" Font-Bold="true" ></asp:Label> 
                </div><br> 
                <div>
                    <button id="btnInfo" type="button" value="Info" width="120px">
                        <img src="Content/css/images/info-circle.svg" alt="Match detail info"> Info</img>
                    </button>
                    <div id="dialog" style="display: none">
                    </div>
                </div><br><br><br>                
                <div class="tableFixHead">
                    <asp:GridView ID="gvDynamic" CssClass="table table-bordered table-striped table-fixed" OnRowDataBound="gvDynamic_RowDataBound" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled" runat="server"></asp:GridView>
                </div>
            </div>
        </div>
    <% } %>
    
    <script type="text/javascript" src="Content/js/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="Content/js/jquery/1.7.2/jquery-ui.js"></script>

     <script type="text/javascript">
         $(function () {
             var fileName = "queryInfo.pdf";
             $("#btnInfo").click(function () {
                 $("#dialog").dialog({
                     modal: true,
                     title: " Label Description",
                     width: 650,
                     height: 500,
                     buttons: {
                         Close: function () {
                             $(this).dialog('close');
                         }
                     },
                     open: function () {
                         var object = "<object data=\"{FileName}\" type=\"application/pdf\" width=\"700px\" height=\"700px\">";
                         object += "If you are unable to view file, you can download from <a href=\"{FileName}\">here</a>";
                         object += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                         object += "</object>";
                         object = object.replace(/{FileName}/g, "Content/Pdf/" + fileName);
                         $("#dialog").html(object);
                     }
                 });
             });
         });


         setTimeout(function () {
             window.close();
         }, 600000); 

     </script>
</asp:Content>  
