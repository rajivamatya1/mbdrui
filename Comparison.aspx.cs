﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class Comparison : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        if (!IsPostBack)
        {
            string comparisonTable = !string.IsNullOrEmpty(Request.QueryString["CompTable"]) ? Convert.ToString(Request.QueryString["CompTable"]) : "";
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
            string rowID = !string.IsNullOrEmpty(Request.QueryString["RecordID"]) ? Convert.ToString(Request.QueryString["RecordID"]) : "";
            string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";

            lblSchema.Text = schema;
            lblTable.Text = tableName;
            hdfName.Value = name;
            hdfRecordId.Value = rowID;
            hdfKeyName.Value = keyName;

            DataTable dt = new DataTable();
            string recordColumn = string.Format("Record ID {0}", rowID);
            dt.Columns.Add(recordColumn);
            

            btnBack.Text = string.Format("Back to {0} table", name);
            GridViewModel model = Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
            List<string> lstComparisonTables = comparisonTable.Split(',').ToList();
            List<CompareViewModel> lstCompareTable = new List<CompareViewModel>();
            if (model != null)
            {
                lstCompareTable = model.ComparisonTables.Where(s => lstComparisonTables.Contains(s.TableName)).ToList();
                string[] header = model.Table.Split(':');
                if (header.Length > 0)
                {
                    lblHeader.Text = header[1];
                    lblHeader.Visible = true;
                    dt.Columns.Add(header[1]);
                }
            }
           
            List<string> mainColumns = model.IncludedColumns.Split(',').ToList();
            List<string> lstTables = new List<string>();
            if (comparisonTable.Contains(","))
            {
                var tables = comparisonTable.Split(',');
                foreach (var item in tables)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        dt.Columns.Add(item.Split(':')[1]);
                        lstTables.Add(item);
                    }
                }
            }
            else
            {
                lstTables.Add(comparisonTable);
                dt.Columns.Add(comparisonTable.Split(':')[1]);
            }

            //sql query to get the data from both tables.
            DataSet dsRecords = new DataSet();
            DataTable dtOrigTable = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = string.Format("select * from {0}.{1} where " + keyName + "='{2}' ;", schema, tableName, rowID);
                foreach (var item in lstCompareTable)
                {
                    query += string.Format("select * from {0}.{1} where {2}='{3}' ;", item.Schema, item.TableName.Split(':')[0], item.FKey.Split(':')[0], rowID);
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                for (int i = 0; i < lstTables.Count; i++)
                {
                    da.TableMappings.Add("Table" + (i + 1) + "", lstTables[i].Split(':')[1]);
                }

                da.Fill(dtOrigTable);
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string childComparisonTables = string.Empty;
                foreach (var item in lstCompareTable)
                {
                    childComparisonTables += string.Format("select * from {0}.{1} where {2}='{3}' ;", item.Schema, item.TableName.Split(':')[0], item.FKey.Split(':')[0].Trim(), dtOrigTable.Rows[0][item.FKey.Split(':')[1].Trim()]);
                }

                SqlCommand cmd = new SqlCommand(childComparisonTables, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                for (int i = 0; i < lstTables.Count; i++)
                {
                    if (i == 0)
                    {
                        da.TableMappings.Add("Table", lstTables[i].Split(':')[1]);
                    }
                    else
                    {
                        da.TableMappings.Add("Table" + (i + 1) + "", lstTables[i].Split(':')[1]);
                    }
                }
                da.Fill(dsRecords);
            }

            //DataTable dtOrigTable = dsRecords.Tables[0];
            //foreach (DataColumn item in dtOrigTable.Columns)
            //{


            foreach (var col in mainColumns)
            {
                if (!string.IsNullOrEmpty(col))
                {
                    string[] colName = col.Split(':');
                    if (lstCompareTable.Any(s => s.ColumnMapping.IndexOf(colName[0].Trim(), StringComparison.InvariantCultureIgnoreCase) >= 0))
                    {
                        DataRow dr = dt.NewRow();
                        dr[recordColumn] = colName[1];
                        dr[lblHeader.Text] = Convert.ToString(dtOrigTable.Rows[0][colName[0].Trim()]);
                        foreach (var tblItem in lstTables)
                        {
                            string tblName = tblItem.Split(':')[1];
                            try
                            {
                                string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(tblName)).FirstOrDefault().ColumnMapping;
                                string strName = Common.GetBetween(colMapping, colName[0].Trim(), ",");
                                dr[tblItem.Split(':')[1]] = Convert.ToString(dsRecords.Tables[tblName].Rows[0][strName.Split(':')[1]]);
                            }
                            catch (Exception ex)
                            {
                                dr[tblName] = "";
                            }

                        }
                        dt.Rows.Add(dr);
                    }

                }
            }



            //}
            //}
            grvComparison.DataSource = dt;
            grvComparison.DataBind();
        }
    }

    protected void btnGoBackToRecord_Click(object sender, EventArgs e)
    {
        Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfKeyName.Value + "&rowId=" + Convert.ToString(hdfRecordId.Value) + "&name=" + hdfName.Value);
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (lblSchema.Text == "MBDR_Totint_Views")
        {
            Response.Redirect("DefaultTotintView.aspx?schema=" + lblSchema.Text + "&table=" + lblTable.Text + "&name=" + hdfName.Value);
        }
        else
        {
            Response.Redirect("DefaultDataView.aspx?schema=" + lblSchema.Text + "&table=" + lblTable.Text + "&name=" + hdfName.Value);
        }
    }
}