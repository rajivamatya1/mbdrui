﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="Comparison.aspx.cs" Inherits="Comparison" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfRecordId" Value="" />
            <asp:HiddenField runat="server" ID="hdfKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <h1><img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>

    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Button Text="" ID="btnBack" OnClick="btnBack_Click" runat="server" />
            <asp:Button Text="Back to Record" ID="btnGoBackToRecord" OnClick="btnGoBackToRecord_Click" runat="server" />
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView runat="server" CssClass="table table-bordered table-striped table-fixed" ID="grvComparison" ShowHeader="true" ShowHeaderWhenEmpty="true" EmptyDataText="No record found."></asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

