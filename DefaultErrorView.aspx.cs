﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class DefaultErrorView : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";
                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');
                    if (header.Length > 0)
                    {
                        lblHeader.Text = header[1];
                        lblHeader.Visible = true;
                    }
                    hdfName.Value = headerLabel;
                    lblSchema.Text = schema;
                    lblTable.Text = tableName;
                    BindErrorTypeDropdown(schema, tableName);
                    BindGrid(schema, tableName);
                    BindColumnsDropDown(schema, tableName);
                }
            }
        }

        public void BindErrorTypeDropdown(string schema, string tableName)
        {
            string query = string.Format("select distinct error_description from {0}.{1} order by error_description asc", schema, tableName);
            DataSet dt = new DataSet();
            List<DropDownModal> lstErrorTypes = new List<DropDownModal>();
            List<DropDownModal> lstTotintTables = new List<DropDownModal>();
            DropDownModal errorType;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }
            foreach (DataRow item in dt.Tables[0].Rows)
            {
                errorType = new DropDownModal();
                errorType.Name = Convert.ToString(item[0]);
                errorType.OrigColName = Convert.ToString(item[0]);
                lstErrorTypes.Add(errorType);
            }

            lstErrorTypes.Insert(0, new DropDownModal() { Name = "", OrigColName = "Please Select" });
            ddlErrorType.DataSource = lstErrorTypes;
            ddlErrorType.DataValueField = "Name";
            ddlErrorType.DataTextField = "OrigColName";
            ddlErrorType.DataBind();

            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            var totintTables = lstFilter.Where(s => s.Schema == schema).ToList();
            foreach (var item in totintTables)
            {
                string[] totIntTable = item.Table.Split(':');
                if (totIntTable.Length > 1)
                {
                    lstTotintTables.Add(new DropDownModal() { Name = totIntTable[1], OrigColName = string.Format("{0}.{1}", item.Schema, totIntTable[0]) });
                }
                else
                {
                    lstTotintTables.Add(new DropDownModal() { Name = totIntTable[0], OrigColName = string.Format("{0}.{1}", item.Schema, totIntTable[0]) });
                }
            }

            ddlTotintTables.DataSource = lstTotintTables;
            ddlTotintTables.DataTextField = "Name";
            ddlTotintTables.DataValueField = "OrigColName";
            ddlTotintTables.DataBind();
            ddlTotintTables.SelectedValue = string.Format("{0}.{1}", schema, tableName);
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            DataTable dt = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();

                // Error log Dropdown Code end here
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                }

                foreach (DataColumn item in dt.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }

                    }

                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        public void BindGrid(string schema, string tableName)
        {
            try
            {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                sortExp = " LoadedDateTime desc";
            }

            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                int pageIndex = 0;
                string errorType = Convert.ToString(ddlErrorType.SelectedValue).Trim();
                if (!string.IsNullOrEmpty(txtDateFrom.Text))
                {
                    where += " and LoadedDateTime >='" + txtDateFrom.Text + " 00:00:00' ";
                }
                if (!string.IsNullOrEmpty(txtDateTo.Text))
                {
                    where += " and LoadedDateTime <='" + txtDateTo.Text + " 23:59:59' ";
                }

                if (!string.IsNullOrEmpty(errorType))
                {
                    //where += " and PostalCode = '" + errorType + "'";
                    where += " and error_description = '" + errorType + "'";
                }

                if (!string.IsNullOrEmpty(sortExp))
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text);
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' {8} ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                           
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                            
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1} {6}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                            
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }

                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text);
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%' {7}) as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);

                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);

                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);

                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);

                        }
                    }
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }



            gvDynamic.Columns.Clear();
            List<string> lstColumns = includeColumns.Split(',').ToList();

            foreach (var col in lstColumns)
            {
                if (!string.IsNullOrEmpty(col))
                {
                    foreach (DataColumn item in dt.Columns)
                    {
                        string colName = item.ColumnName;
                        string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                            BoundField field = new BoundField();
                            field.HeaderText = columnInfo;
                            field.DataField = item.ColumnName;
                            field.SortExpression = item.ColumnName;
                            if (readonlyColumns.Contains(colName))
                            {
                                field.ReadOnly = true;
                            }
                            gvDynamic.Columns.Add(field);
                            break;
                        }
                    }
                }
            }

            CommandField cf = new CommandField();
            cf.ButtonType = ButtonType.Link;
            cf.DeleteText = "View";
            cf.ShowDeleteButton = true;
            gvDynamic.Columns.Add(cf);

            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
            gvDynamic.DataSource = dt;
            gvDynamic.DataBind();


            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultErrorView_BindGrid");
            }
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    where += " and " + Convert.ToString(ddlColumn.SelectedValue) + " like '%" + Common.Common.ReplaceSQLChar(txtSearch.Text) + "%'";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlErrorType.SelectedValue)))
                {
                    where += " and error_description='" + Convert.ToString(ddlErrorType.SelectedValue).Trim() + "'";
                }
                if (!string.IsNullOrEmpty(where)) {
                    where = " where " + where.Substring(5);
                }

                query = string.Format("select count({0}) from {1}.{2} {3}", datakeyName, schema, tableName,where);
                
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                ViewState["PageIndex"] = gvDynamic.PageIndex;
            }
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDynamic.EditIndex = -1;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
            string tableName = model.StagingTable.Split(':')[0];
            string displayName = model.StagingTable.Split(':')[1];
            string dataKeyname = string.Empty;
            if (model != null)
            {
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
            }

            Response.Redirect("DefaultErrorEditView.aspx?Schema=" + model.StagingSchema + "&Table=" + tableName + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + displayName + "&errorSchema=" + lblSchema.Text + "&errorTable=" + lblTable.Text + "&errorName=" + hdfName.Value);
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDynamic.EditIndex = e.NewEditIndex;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;

                if (model != null)
                {
                    readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";").ToLower();
                    dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";");
                }
                string query = " set ";
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    if (cell.ContainingField is BoundField)
                    {
                        string colName = ((BoundField)cell.ContainingField).DataField;
                        if (!readonlyColumns.Contains(colName) && colName != "id")
                        {
                            query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                        }
                    }
                }
                gvDynamic.EditIndex = -1;
                conn.Open();
                string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                SqlCommand cmd = new SqlCommand(finalQuery, conn);
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
                conn.Close();
                BindGrid(lblSchema.Text, lblTable.Text);
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC"; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
            return nextDir;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid(lblSchema.Text, lblTable.Text);
            //gvDynamic.Focus();
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            txtDateFrom.Text = string.Empty;
            txtDateTo.Text = string.Empty;
            ddlColumn.SelectedIndex = 0;
            ddlErrorType.SelectedIndex = 0;
            ViewState["SortExpression"] = null;
            BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void ddlTotintTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = Convert.ToString(ddlTotintTables.SelectedValue);

            if (!string.IsNullOrEmpty(value))
            {
                string[] tableInfo = value.Split('.');
                string schema = tableInfo[0];
                string tableName = tableInfo[1];
                string name = ddlTotintTables.SelectedItem.Text;
                Response.Redirect("DefaultErrorView.aspx?schema=" + schema + "&table=" + tableName + "&name=" + name + "");
            }
        }

        protected void ddlErrorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ddlErrorType.SelectedValue)))
                BindGrid(lblSchema.Text, lblTable.Text);
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var row = (DataRowView)e.Row.DataItem;
                LinkButton field = e.Row.Cells[e.Row.Cells.Count - 1].Controls[0] as LinkButton;
                field.Text = "View";
                field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
            }
        }
    }
}