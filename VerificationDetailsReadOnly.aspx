﻿<%@ Page Title="Case Verification" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="VerificationDetailsReadOnly.aspx.cs" Inherits="DynamicGridView.VerificationDetailsReadOnly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <%--  <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>

     <style type="text/css">
     #primary-icon, #divGreen, #divRed, div.logo, img.bell-icon, #cVer
     {
      display:none;
     }
 </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        <%--VERIFICATION Details --%>
         <asp:Literal Text="" ID="ltrTableName" runat="server" />

    </h1>
   

    <asp:HiddenField ID="hdfDataKeyName" runat="server" />
    <asp:HiddenField ID="hdfDiagnosisId" runat="server" />
    <asp:HiddenField ID="hdfStatusId" runat="server" />
    <asp:HiddenField ID="hdfUserName" runat="server" />
    

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead-verification">
                <asp:GridView ID="gvVerificationStatus" runat="server" AutoGenerateColumns="false" DataKeyNames="diagnosis" OnRowDataBound="gvVerificationStatus_RowDataBound" OnRowCommand="gvVerificationStatus_RowCommand">
                    <Columns>
                        <%--<asp:BoundField DataField="diagnosis" HeaderText="ICD-10 DIAGNOSTIC CODE" />--%>

                        <asp:TemplateField HeaderText="ICD-10 DIAGNOSIS CODE" >
                            <ItemTemplate>
                                <%# Eval("diagnosis") %>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="DIAGNOSIS VERIFICATION STATUS <br /> (Only one status may be selected)">
                            <ItemTemplate>
                                <%--<asp:DropDownList runat="server" ID="ddlVerifyStatus" CssClass="form-control" DataValueField="verifyId" DataTextField="details">
                                </asp:DropDownList>--%>
                                <asp:Label ID="ddlVerifyStatus" runat="server" CssClass="form-control" />
                             </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DIAGNOSIS VERIFICATION METHOD <br /> (Multiple choice, select all that apply)">
                            <ItemTemplate>
                                <asp:CheckBoxList ID="idVerfiyMethod" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" RepeatRows="2" CssClass="checbox-list-disabled" >
                                </asp:CheckBoxList>
                            </ItemTemplate> 
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="COMMENTS">
                            <ItemTemplate>
                                <%# Eval("comment") %> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Date/Time, UserName">
                            <ItemTemplate>
                                <%# Eval("verificationDate") %> <br />
                                <%# Eval("[user]") %> <br />
                               <%--<asp:Label ID="userName" runat="server"></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>                    
                </asp:GridView>
            </div>
        </div>
    </div>


    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
        <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>
    <script>

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')
                    var connMsg = message;
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }



        function ShowProgress() {

            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnAdvanceSearch").prop("disabled", true);
            }
            else {

                $("#btnAdvanceSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",



                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },



                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

    </script>

    <% } %>
</asp:Content>


