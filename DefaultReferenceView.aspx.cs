﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class DefaultReferenceView : System.Web.UI.Page
{
    protected DataTable dt;
    dynamic permissions = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        permissions = CheckRole();
        if (permissions.Count > 0)
        {
            btnUpdate.Enabled = true;
        }
        else
        {
            btnUpdate.Enabled = false;
        }
        if (!IsPostBack)
        {
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
            string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
            string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
            string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
            string searchText = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
            string columnName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            btnBackTo.Text = "Reference Tables";
            hdfSearch.Value = searchText;
            hdfColumnName.Value = columnName;
            hdfName.Value = name;
            hdfKeyName.Value = keyName;
            hdfRowID.Value = rowID;
            hdfSchema.Value = schema;
            hdfTableName.Value = tableName;
            if (model != null)
            {
                ltrTableName.Text = model.Table.Split(':')[1];
            }


            if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
            {
                BindColumnsDropDown(schema, tableName);
                BindGrid(tableName, schema, keyName, rowID);
            }
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    public dynamic CheckRole()
    {
        dynamic permissions = null;

        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

            if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
            {
                permissions = dtAccessPermission.AsEnumerable()
                .Where(permission => permission.Field<string>("PermissionName") == "REF_WRITE")
                .Select(permission => new
                {
                    UserId = permission.Field<string>("userId"),
                    RoleId = permission.Field<string>("RoleId"),
                    RoleName = permission.Field<string>("RoleName"),
                    RoleDetails = permission.Field<string>("RoleDetails"),
                    PermissionId = permission.Field<string>("PermissionId"),
                    PermissionName = permission.Field<string>("PermissionName")
                })
                .ToList();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DefaultReferenceView_CheckRole");
        }

        return permissions;
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        DataTable dtDropDown = new DataTable();
        List<DropDownModal> lstColumnNames = new List<DropDownModal>();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string query = "";
            query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dtDropDown);
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
            }

            foreach (DataColumn item in dtDropDown.Columns)
            {
                if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                    if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                    {
                        lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                    }
                }

            }
        }
        ddlColumn.DataSource = lstColumnNames;
        ddlColumn.DataTextField = "Name";
        ddlColumn.DataValueField = "OrigColName";
        ddlColumn.DataBind();
    }

    public void BindGrid(string tableName, string schema, string keyName, string rowID)
    {
        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
        List<GridViewModel> lstFilter = Common.GetFilterList();
        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();

        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        List<ColumnList> lstRecord = new List<ColumnList>();
        if (model != null)
        {
            includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
            readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
            dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");

            //gvDynamic.Columns.Clear();
            List<string> lstColumns = includeColumns.Split(',').ToList();


            DataTable dtActualRow = new DataTable();
            DataSet dsRows = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                
                string query = "";
                query = string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, rowID);
                int row = Convert.ToInt32(rowID);
                query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row + 1);
                query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, row - 1);

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dsRows);
            }

            if (dsRows.Tables[1].Rows.Count > 0)
            {
                btnNext.Enabled = true;
                btnNext.CssClass = "btn btn-success";
            }
            else
            {
                btnNext.Enabled = false;
                btnNext.CssClass = "btn btn-disabled";
            }

            if (dsRows.Tables[2].Rows.Count > 0)
            {
                btnPrev.Enabled = true;
                btnPrev.CssClass = "btn btn-success";
            }
            else
            {
                btnPrev.Enabled = false;
                btnPrev.CssClass = "btn btn-disabled";
            }
            dtActualRow = dsRows.Tables[0];
            if (dtActualRow.Rows.Count > 0)
            {
                //ltrFacilitySouce.Text = string.Format("{0}", hdfName.Value);// dtActualRow.Rows[0]["FacilitySource"]);
            }

            string compareQuery = "";
            DataSet dsTables = new DataSet();
            dt = new DataTable();
            
            dt.Columns.Add("Columns");
            dt.Columns.Add(model.Table.Split(':')[1]);
            dt.Columns.Add("ActualColumn");
            List<string> lstNavigation = new List<string>();
            if (model.ComparisonTables.Count > 0)
            {
                if (!model.ComparisonTables.Any(s => s.TableName == ""))
                {

                    foreach (var item in model.ComparisonTables)
                    {
                        if (!string.IsNullOrEmpty(item.TableName))
                        {
                            string[] lstTableNames = item.TableName.Split(':');
                            string key = item.FKey.Split(':')[0];
                            if (!string.IsNullOrEmpty(lstTableNames[1]))
                            {
                                lstNavigation.Add(lstTableNames[1]);
                                dt.Columns.Add(lstTableNames[1]);
                            }
                            else
                            {
                                lstNavigation.Add(lstTableNames[0]);
                                dt.Columns.Add(lstTableNames[0]);
                            }
                            compareQuery = compareQuery + " select * from " + item.Schema + "." + lstTableNames[0] + " where " + key + " = '" + rowID + "'";
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(compareQuery))
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(compareQuery, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    for (int i = 0; i < lstNavigation.Count; i++)
                    {
                        if (i == 0)
                        {
                            da.TableMappings.Add("Table", lstNavigation[i]);
                        }
                        else
                        {
                            da.TableMappings.Add("Table" + (i) + "", lstNavigation[i]);
                        }
                    }
                    da.Fill(dsTables);
                }
            }


            foreach (var column in lstColumns)
            {
                if (!string.IsNullOrEmpty(column))
                {

                    foreach (DataColumn item in dtActualRow.Columns)
                    {
                        string colName = item.ColumnName;
                        string columnName = column.Contains("->") ? column.Split('>')[1] : column;

                        DataRow dr = dt.NewRow();
                        //var colName = lstColumns.FirstOrDefault(s => s.StartsWith(item.ColumnName));

                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                            string displayColname = string.Empty;
                            if (!string.IsNullOrEmpty(colName))
                            {
                                //string columnName = colName.Split(':')[1];
                                dr[2] = colName;
                                dr[0] = columnInfo;
                                displayColname = columnInfo;
                            }
                            else
                            {
                                dr[2] = item.ColumnName;
                                dr[0] = item.ColumnName;
                                displayColname = item.ColumnName;
                            }
                            if (displayColname != "RecordFrom")
                                lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]) });
                            dr[model.Table.Split(':')[1]] = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]);
                            for (int j = 0; j < lstNavigation.Count; j++)
                            {
                                try
                                {
                                    string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(lstNavigation[j])).FirstOrDefault().ColumnMapping;
                                    string strName = Common.GetBetween(colMapping, item.ColumnName, ",");
                                    dr[lstNavigation[j]] = Convert.ToString(dsTables.Tables[lstNavigation[j]].Rows[0][strName.Split(':')[1]]);
                                }
                                catch (Exception ex)
                                {
                                    dr[lstNavigation[j]] = "";
                                }

                            }
                            if (!string.IsNullOrEmpty(hdfSearchText.Value))
                            {
                                if (item.ColumnName == hdfSearchText.Value)
                                {
                                    dt.Rows.Add(dr);
                                }
                            }
                            else
                            {
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
            }

        }
        gvDynamic.DataSource = dt;
        gvDynamic.DataBind();
        //rptChild.DataSource = lstRecord;
        //rptChild.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        hdfSearchText.Value = Convert.ToString(ddlColumn.SelectedValue);
        BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        ddlColumn.SelectedIndex = 0;
        hdfSearchText.Value = "";
        BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        int row = Convert.ToInt32(hdfRowID.Value);
        Response.Redirect("DefaultReferenceView.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row - 1) + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        int row = Convert.ToInt32(hdfRowID.Value);
        Response.Redirect("DefaultReferenceView.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row + 1) + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
    }

    protected void btnAll_Click(object sender, EventArgs e)
    {
        if (hdfSchema.Value == "MBDR_Totint_Views")
        {
            Response.Redirect("DefaultTotintView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "&fromDate=" + hdfFromDate.Value + "&toDate=" + hdfToDate.Value + "&facility=" + hdfChangeFacility.Value + "");
        }
        else
        {
            Response.Redirect("DefaultDataView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "");
        }

    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var row = (DataRowView)e.Row.DataItem;
            ((TextBox)e.Row.FindControl("txtValue")).Text = Convert.ToString(row[1]);
            e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#e3f4ff");
        }
    }

    protected void btnBackTo_Click(object sender, EventArgs e)
    {
        if (hdfSchema.Value == "MBDR_Totint_Views")
        {
            Response.Redirect("DefaultTotintView.aspx?schema=" + hdfSchema.Value + "&table=" + hdfTableName.Value + "&name=" + hdfName.Value + "&searchText=" + hdfSearch.Value + "&colName=" + hdfColumnName.Value + "&fromDate=" + hdfFromDate.Value + "&toDate=" + hdfToDate.Value + "&facility=" + hdfChangeFacility.Value + "");
        }
        else if (hdfSchema.Value == "MBDR_UI_System")
        {
            Response.Redirect("stagingTables.aspx");
        }
        else if (hdfSchema.Value == "MBDR_UI_Annual")
        {
            Response.Redirect("processingTables.aspx");
        }
        else if (hdfSchema.Value == "MBDR_UI_Archive")
        {
            Response.Redirect("archiveTables.aspx");
        }
        else if (hdfSchema.Value == "Reference")
        {
            Response.Redirect("referenceTables.aspx");
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string query = string.Format("Update {0}.{1} set", hdfSchema.Value, hdfTableName.Value);
        foreach (GridViewRow item in gvDynamic.Rows)
        {
            string colName = ((HiddenField)item.FindControl("hdfRealName")).Value;
            string value = ((TextBox)item.FindControl("txtValue")).Text;
            query = query + " " + colName + "=" + "'" + value + "',";
        }
        query = query.Substring(0, query.Length - 1) + " where " + hdfKeyName.Value + "='" + hdfRowID.Value + "'";
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
