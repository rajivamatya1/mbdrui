﻿using DynamicGridView.Common;
using System;
using System.Configuration;

public partial class Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblSiteAdminMsg.Text = "An error has occurred. Please contact the Site Admin (Altarum).";
            lblErrorMessage.Text = !string.IsNullOrEmpty(Request.QueryString["credentialsMessage"]) ? Convert.ToString(Request.QueryString["credentialsMessage"]) : "";
            lblAttemptEmail.Text = !string.IsNullOrEmpty(Request.QueryString["atpEmail"]) ? Convert.ToString(Request.QueryString["atpEmail"]) : "";

            if (lblErrorMessage.Text == "doubleLogin")
            {
                lblSiteAdminMsg.Text = string.Empty;
                lblErrorMessage.Text = string.Empty;

                string browserCount = Convert.ToString(Session["BrowserCount"]);
                if (browserCount == "2")
                {
                    Session["BrowserCount"] = 1;
                }
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "errorMsgs('You are already logged in.', 'Login Error Message');", true);
            }
            
            else if (lblErrorMessage.Text == "roleMissing")
            {
                lblSiteAdminMsg.Text = string.Empty;
                lblErrorMessage.Text = string.Empty;

                string eMsg = "Missing Role|Email Address:" + lblAttemptEmail.Text;

                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Snap(eMsg, path, "Error Page");

                lblAttemptEmail.Text = string.Empty;

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "showLoginPage('Your account has been disabled. Please contact the System Administrator for assistance.', 'Error Message');", true);
            }
            
            else if (lblErrorMessage.Text == "userNotFound")
            {
                lblSiteAdminMsg.Text = string.Empty;
                lblErrorMessage.Text = string.Empty;

                string eMsg = "User Not Found|Email Address:" + lblAttemptEmail.Text;

                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Snap(eMsg, path, "Error Page");

                lblAttemptEmail.Text = string.Empty;

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "showLoginPage('This user does not exist. Please contact the System Administrator for assistance.', 'Authentication Failed');", true);
            }
            
            else if (lblErrorMessage.Text == "wrongCredit")
            {
                lblSiteAdminMsg.Text = string.Empty;
                lblErrorMessage.Text = string.Empty;

                string eMsg = "Authentication Failed|Email Address:" + lblAttemptEmail.Text;

                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Snap(eMsg, path, "Error Page");

                lblAttemptEmail.Text = string.Empty;

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "showLoginPage('The credentials entered do not match the account. Please contact the System Administrator for assistance.', 'Authentication Failed');", true);
            }
        }
    }

    protected void btnHideLogin_Click(object sender, EventArgs e)
    {
        string env = ConfigurationManager.AppSettings["environment"];
        string miMiLogin = String.Empty;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        if (browserCount == "2")
        {
            Session["BrowserCount"] = 1;
        }
        if (!string.IsNullOrEmpty(env))
        {
            if (env == "dev" || env == "qa")
            {
                miMiLogin = "login.aspx";
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }
        }
        else
        {
            miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
        }
        Response.Redirect(miMiLogin);
    }

    protected void btnLoginPage_Click(object sender, EventArgs e)
    {
        string env = ConfigurationManager.AppSettings["environment"];
        string miMiLogin = string.Empty;

        if (!string.IsNullOrEmpty(env))
        {
            if (env == "dev" || env == "qa")
            {
                miMiLogin = "login.aspx";
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }
        }
        else
        {
            miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
        }
        Response.Redirect(miMiLogin);
    }

}