﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class MatchingLinkingCaseReportsViewLinkDetails : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            try
            {
                if (!IsPostBack)
                {
                    string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                    string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                    string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                    string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                    string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                    string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
                    string rowid = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
                    string scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["scrutinizerId"]) : "";
                    string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";
                    string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
                    hdfDataRowId.Value = rowid;
                    hdfScrutinizerId.Value = scrutinizerId;
                    hdfTableName.Value = tableName;
                    hdfSchema.Value = schema;
                    hdfCaseRecId.Value = caseRecId;
                    if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                    {
                        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                        List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                        string[] header = model.Table.Split(':');                      
                        hdfName.Value = headerLabel;
                        lblSchema.Text = schema;

                        lblTable.Text = tableName;
                        BindGrid(schema, tableName);

                        if (header.Length > 0)
                        {
                            //lblHeader.Text = "LINKED CASE REPORTS";// header[1];
                            lblHeader.Text = "Linked Case Reports for Case " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + hdfCaseRecId.Value + "</span></b>";
                            lblHeader.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_Page_Load");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        
        private string getQueryString()
        {
            string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
            string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
            string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
            string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
            string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
            string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
            string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
            string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
            string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
            string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
            string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
            string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
            string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
            string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
            string returnpage = !string.IsNullOrEmpty(Request.QueryString["returnpage"]) ? Convert.ToString(Request.QueryString["returnpage"]) : "";
            string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

            string whereSep = "1";
            whereSep += "&isSCIDBlank=" + isSCIDBlank + "";

            if (!string.IsNullOrEmpty(displayName))
            {
                whereSep += "&displayName=" + displayName + "";
            } 
            if (!string.IsNullOrEmpty(returnpage))
            {
                whereSep += "&returnpage=" + returnpage + "";
            }
            if (!string.IsNullOrEmpty(matchLevel))
            {
                whereSep += "&matchLevel=" + matchLevel + ""; ;
            }
            if (!string.IsNullOrEmpty(childLastName))
            {

                whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childFirstName))
            {

                whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childMiddleName))
            {

                whereSep += "&childMiddleName=" + childMiddleName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(birthDate))
            {

                whereSep += "&birthDate=" + birthDate + "";
            }
            if (!string.IsNullOrEmpty(childSSN))
            {

                whereSep += "&childSSN=" + childSSN.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(momSSN))
            {

                whereSep += "&momSSN=" + momSSN.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(momLastName))
            {

                whereSep += "&momLastName=" + momLastName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(momFirstName))
            {

                whereSep += "&momFirstName=" + momFirstName.Trim().Replace("'", "''") + "";
            }

            if (!string.IsNullOrEmpty(address1))
            {

                whereSep += "&address1=" + address1.Replace("'", "''").Trim() + "";
            }


            if (!string.IsNullOrEmpty(city))
            {

                whereSep += "&city=" + city.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                whereSep += "&fromDate=" + fromDate + "";
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                whereSep += "&toDate=" + toDate + "";
            }

            return whereSep;
        }

        public void BindGrid(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "assimilatedTimestamp")
                            {
                                sortExp = " assimilatedTimestamp desc";
                                break;
                            }
                        }
                    }
                }

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName+"Link") && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                    //where = " where caseRecId ='" + hdfDataRowId.Value + "'";
                    where = " where caseRecId ='" + hdfCaseRecId.Value + "'";
                    string tablenameUpdate = "";
                    tablenameUpdate = "CaseReportsView";
                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2} desc) AS Row,* from {0}.{1}{5}) as result where Row between({3}) and ({4})", schema, tablenameUpdate, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                }

                gvDynamic.Columns.Clear();

                List<string> lstColumns = includeColumns.Split(',').ToList();

                foreach (var col in lstColumns)
                {
                    if (gvDynamic.Columns.Count < 15)
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            foreach (DataColumn item in dt.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                                
                                if (colName == "reportId" || colName == "caseRecId" || colName == "masterRecordNumber" || colName == "deathNumber" || colName == "childLastName" || colName == "childFirstName" || colName == "birthDate" || colName == "childSSN" || colName == "momSSN" || colName == "momLastName" || colName == "momFirstName" || colName == "address1" || colName == "city" || colName == "state" || colName == "zipcode")
                                {
                                    if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                        BoundField field = new BoundField();
                                        field.HeaderText = columnInfo;
                                        field.DataField = item.ColumnName;
                                        field.SortExpression = item.ColumnName;
                                        if (readonlyColumns.Contains(colName))
                                        {
                                            field.ReadOnly = true;
                                        }
                                        gvDynamic.Columns.Add(field);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                gvDynamic.DataSource = dt;
                gvDynamic.DataBind();
                hdfCaseRecId.Value = Convert.ToString(dt.Rows[0][2]);
                string caseRecId = hdfCaseRecId.Value;
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_BindGrid");
            }
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
           
            int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
            try
            {
                string where = " where caseRecId ='" + hdfDataRowId.Value + "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    query = string.Format("select count({0}) from {1}.{2} {3}", datakeyName, schema, tableName, where);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                   
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_GetTotalRecords");
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_PageIndexChanging");
            }
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = -1;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_RowEditing");
            }
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                    GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;

                    if (model != null)
                    {
                        readonlyColumns = model.ReadonlyColumns;
                        dataKeyname = model.DataKeyName;
                    }
                    string query = " set ";
                    foreach (DataControlFieldCell cell in row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            string colName = ((BoundField)cell.ContainingField).DataField;
                            if (!readonlyColumns.Contains(colName) && colName != "id")
                            {
                                query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                            }
                        }
                    }
                    gvDynamic.EditIndex = -1;
                    conn.Open();
                    string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                    string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                    SqlCommand cmd = new SqlCommand(finalQuery, conn);

                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_RowUpdating");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_Sorting");
            }
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();

                string dataKeyname = string.Empty;
                if (model != null)
                {
                    dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
                }

                Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value, true);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_gvDynamic_ViewRecord");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_GetSortDirection");
            }
            return nextDir;
        }   

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e) 
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    var row = (DataRowView)e.Row.DataItem;
            //    int index = -1;
            //    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
            //    field.Text = "View";
            //    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
            //    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
            //}
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "Edit")
            //{

            //    int id = Convert.ToInt32(e.CommandArgument.ToString());
            //    Response.Redirect("CaseRecordsReportDetails.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&caseRecId=" + hdfDataRowId.Value + "&name=" + hdfName.Value + "");
            //}
        }

        protected void btnCaseRecord_Click(object sender, EventArgs e) //Linked Case Record Pg.3
        {
            try
            {
                string rendertableName = !string.IsNullOrEmpty(Request.QueryString["renderTable"]) ? Convert.ToString(Request.QueryString["renderTable"]) : "";
                string searchText = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string columnName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                string caseRecId = hdfDataRowId.Value;
                string where = getQueryString();
                if (rendertableName == "Matching_Linking" || rendertableName == "BirthRecords" || rendertableName == "DeathRecords")
                   
                {
                    Response.Redirect("MatchingViewDetails.aspx?Schema=MBDR_System&Table=Matching_Linking&refresh=false&name=scrutinizerId&scrutinizerId=" + hdfScrutinizerId.Value + "&searchText=" + searchText + "&colName=" + columnName + "&sortExp="+ sortExp + "&keyName=caseRecId&caseRecId=" + caseRecId + "&where=" + where + "", false);
                }
                else
                {
                    Response.Redirect("MatchingFullViewDetails.aspx?pagecall=fullview&Schema=MBDR_System&Table=ScrutinizerFullViewCaseReports&keyName=caseRecId&name=MBDR_System&scrutinizerId=" + hdfScrutinizerId.Value + "&caseRecId=" + caseRecId + "&sortExp=" + sortExp +  "&where=" + where + "", false);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "M&LCaseReportsViewLinkDetails_btnCaseRecord_Click");
            }
        }
    }
}