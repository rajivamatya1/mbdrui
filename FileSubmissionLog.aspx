﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FileSubmissionLog.aspx.cs" Inherits="FileSubmissionLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <%--  <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <asp:HiddenField runat="server" ID="isResetClick" Value="" />

            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button Text="Return to Activity and Error Logs" runat="server" ID="btnReturnAaEL" OnClick="btnReturnAaEL_Click" CssClass="btn btn-primary" OnClientClick="return ShowProgress();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label for="Content_txtSearch">Search by Keyword</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch" ValidationGroup="search" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSearch" ValidationGroup="search" ValidationExpression="[a-zA-Z0-9]*[^!@%~?:#$%^&*=()0']*" runat="server" ErrorMessage="Invalid Input" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
              </div>
            <div class="form-item">
                <label for="Content_ddlColumn">In Column</label>
                <asp:DropDownList runat="server" ID="ddlColumn" CssClass="form-control" ValidationGroup="search">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="ddlColumn"></asp:RequiredFieldValidator>
            </div>
            <div class="form-item" runat="server" id="dvFromDate">
                <label for="Content_txtDateFrom">From Date</label>
                <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateFrom" ValidationGroup="search" onblur="ValidateFromDate(this);" />
                <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ID="CustomValidator1" ValidationGroup="search" ClientValidationFunction="ValidateFromDate" ControlToValidate="txtDateFrom" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
            </div>
            <div class="form-item" runat="server" id="dvToDate">
                <label for="Content_txtDateTo">To Date</label>
                <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtDateTo" ValidationGroup="search" onblur="ValidateToDate(this);" />
                <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ValidationGroup="search" ID="CustomValidator2" ClientValidationFunction="ValidateToDate" ControlToValidate="txtDateTo" ErrorMessage="Select from date" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
            </div>
            <div class="form-item">
                <asp:Button Text="Search" runat="server" ID="btnSearch" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-success" ValidationGroup="search" />
            </div>
            <div class="form-item">
                <asp:Button Text="Reset" runat="server" ID="btnReset" OnClientClick="resetValidation()" CssClass="btn btn-danger" />
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled" OnRowEditing="gvDynamic_RowEditing">
                </asp:GridView>
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {
           
            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
           }
       });

       /*** will trigger search button by enter ***/



        function resetValidation() {

            document.getElementById("<%=txtSearch.ClientID %>").value = "";
            document.getElementById("<%=ddlColumn.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=txtDateFrom.ClientID %>").value = "";
            document.getElementById("<%=txtDateTo.ClientID %>").value = "";

            document.getElementById("<%=isResetClick.ClientID %>").value = "true";

            var gridView = document.getElementById("<%= gvDynamic.ClientID %>");
            var pageIndex = 0;
            gridView.PageIndex = pageIndex;
        }

        function hideReport()
        {
            var $searchReportElement = $("#<%=gvDynamic.ClientID%>");

            if ($searchReportElement.length)
            {
                $searchReportElement.hide();
            }
        }

        function ShowProgress()
        {
            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {
                $("#btnAdvanceSearch").prop("disabled", true);
            }
            else {
                $("#btnAdvanceSearch").prop("disabled", false);

                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });

                return true;

            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ValidateToDate(input) {
                      
            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID %>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");
            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");

            if (txtTo.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtTo === "" || txtTo === 'yyyy-mm-dd') {

                    fromDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                }
                else if (!dateRegex.test(txtTo)) {

                    toDateValidator.style.display = "block";
                    toDateValidator.innerHTML = "Invalid Date";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                }
                else {

                    var minDate = new Date("1900-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtTo);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/1900

                    if (checkDateIfisValid < minDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    if (txtTo !== null) {
                        if (txtFrom.length === 0)
                        {
                            <%--toDateValidator.style.display = "block";
                            toDateValidator.innerHTML = "Please select from date";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                            return false;--%>

                            toDateValidator.style.display = "none";
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;

                        }
                        else if (txtTo < txtFrom) {
                            if (!dateRegex.test(txtFrom)) {

                                fromDateValidator.style.display = "block";
                                fromDateValidator.innerHTML = "Invalid Date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                            }
                            else {
                                fromDateValidator.style.display = "none";

                                toDateValidator.style.display = "block";
                                toDateValidator.innerHTML = "Must be before from date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                                return false;
                            }
                        }
                        else {
                            if (!dateRegex.test(txtFrom)) {

                                fromDateValidator.style.display = "block";
                                fromDateValidator.innerHTML = "Invalid Date";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                            }
                            else {
                                toDateValidator.style.display = "none";
                                fromDateValidator.style.display = "none";

                                $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                                return true;
                            }
                        }
                    }
                    else {
                        toDateValidator.style.display = "none";
                        fromDateValidator.style.display = "none";

                        $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                        return true;
                    }
                }
            }
            else
            {
                document.getElementById("<%=txtDateTo.ClientID%>").value = "";
            }
        }

        function ValidateFromDate(input)
        {
            var txtFrom = document.getElementById("<%=txtDateFrom.ClientID%>").value;
            var txtTo = document.getElementById("<%=txtDateTo.ClientID %>").value;

            var toDateValidator = document.getElementById("<%=CustomValidator2.ClientID %>");
            var fromDateValidator = document.getElementById("<%=CustomValidator1.ClientID %>");

            if (txtFrom.length === 10) {

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtFrom === "" || txtFrom === 'yyyy-mm-dd') {

                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

        }
        else if (!dateRegex.test(txtFrom)) {

            fromDateValidator.style.display = "block";
            fromDateValidator.innerHTML = "Invalid Date";

            $("#<%=btnSearch.ClientID%>").prop("disabled", true);
        }
        else {


            var minDate = new Date("1900-01-01");
            // parse the date

            var checkDateIfisValid = new Date(txtFrom);

            //check if it is validate date or not

            if (isNaN(checkDateIfisValid.getTime())) {
                fromDateValidator.style.display = "block";
                fromDateValidator.innerHTML = "Invalid Date";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                return false;
            }

            //check if the date is before 01/01/1900

            if (checkDateIfisValid < minDate) {
                fromDateValidator.style.display = "block";
                fromDateValidator.innerHTML = "Date must be 01/01/1900 or later.";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                return false;
            }

            //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    var checkDateIfisValidToDate = new Date(txtTo);

                    if (checkDateIfisValidToDate > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    if (txtTo.length > 0) {

                if (txtFrom > txtTo) {

                    toDateValidator.style.display = "none";

                    fromDateValidator.style.display = "block";
                    fromDateValidator.innerHTML = "Must be before to date";


                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                    return false;

                }
                else if (!dateRegex.test(txtTo)) {

                    toDateValidator.style.display = "block";
                    toDateValidator.innerHTML = "Invalid Date";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                }
                else {
                        fromDateValidator.style.display = "none";
                        toDateValidator.style.display = "none";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                        return true;
                }
            }
            else if (txtFrom <= txtTo) {

                toDateValidator.style.display = "none";

                fromDateValidator.style.display = "block";
                fromDateValidator.innerHTML = "Must be before to date.";
                $("#<%=btnSearch.ClientID%>").prop("disabled", true);

                return false;

            }
            else {

                    fromDateValidator.style.display = "none";
                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                    return true;
            }
        }
    } else {
        document.getElementById("<%=txtDateFrom.ClientID%>").value = "";
            }
        }


        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

    </script>
</asp:Content>

