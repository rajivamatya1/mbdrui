﻿<%@ Page Title="MBDR UI Matching Full View Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="MatchingFullViewDetails.aspx.cs" Inherits="MatchingFullViewDetails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <%-- <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
    <link href="Content/css/MLTable.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
    <%if (hdfPageRedirect.Value == "first")
        {  %>
    <script>

        sessionStorage.removeItem("detailsId");
        sessionStorage.clear;
        sessionStorage.removeItem("caseDetailId");
        sessionStorage.clear;
        sessionStorage.removeItem("isCheckCheckedBox");
        sessionStorage.clear;
    </script>
    <%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label runat="server" Text="Matching and Linking - All Matches for Case Record : " /><b class="yellow-highlight" style="font-size: 22px;"><%= !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "" %></b>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <div class="program-name">
                                <h1></h1>
                                <asp:HiddenField ID="hdflinkCaseReport" runat="server" />
                                <asp:HiddenField ID="hdfScrutinizerID" runat="server" />
                                <asp:HiddenField ID="hdfArchiveID" runat="server" />
                                <asp:HiddenField ID="hdfSchema" runat="server" />
                                <asp:HiddenField ID="hdfTableName" runat="server" />
                                <asp:HiddenField ID="hdfDataKeyName" runat="server" />
                                <asp:HiddenField ID="hdfTableKeyName" runat="server" />
                                <asp:HiddenField ID="hdfRowID" runat="server" />
                                <asp:HiddenField ID="hdfNextRowID" runat="server" />
                                <asp:HiddenField ID="hdfPreviousRowID" runat="server" />
                                <asp:HiddenField ID="hdfDetailsID" runat="server" />
                                <asp:HiddenField ID="hdfCaseDetailsID" runat="server" />
                                <asp:HiddenField ID="IsCaseDetailID" runat="server" />
                                <asp:HiddenField ID="hdfMessage" runat="server" />
                                <asp:HiddenField ID="hdfFirstTable" runat="server" />
                                <asp:HiddenField ID="hdfMultipleTableName" runat="server" />
                                <asp:HiddenField ID="hdfOwner" runat="server" />
                                <asp:HiddenField ID="hdfPageRedirect" runat="server" />
                                <asp:HiddenField ID="hdfPageDetailId" runat="server" />
                                <asp:HiddenField ID="hdfIsTabClicked" runat="server" />
                                <asp:HiddenField ID="hdfIsTabClickedCR" runat="server" />
                                <asp:HiddenField ID="hdfIsTabClickedBR" runat="server" />
                                <asp:HiddenField ID="hdfIsTabClickedDR" runat="server" />
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button Text="Return to Possible Matches" OnClick="btnPossibleMatch_Click" runat="server" ID="btnPossibleMatch" CssClass="btn btn-primary" OnClientClick="mySessionClear()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="row row-no-padding-matching-linking">
                        <div class="col col-lg-12 col-md-12 col-sm-12">
                            <div class="comparison-buttons-container">
                                <div class="form-item">
                                    <strong>Compare with Source: </strong>
                                    <asp:Button Text="Case Reports" runat="server" ID="btnCaseReports" OnClick="btnCaseReports_Click" CssClass="btn btn-primary" />
                                    <asp:Button Text="Birth Records" runat="server" ID="btnBirthRecords" OnClick="btnBirthRecords_Click" CssClass="btn btn-primary" />
                                    <asp:Button Text="Death Records" runat="server" ID="btnDeathRecords" OnClick="btnDeathRecords_Click" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="admin-workarea-matching-linking">
                        <div class="row row-no-padding" style="display: none;">
                            <div class="col col-lg-12 col-md-12 col-sm-12">
                                <div class="admin-workarea-menu">
                                    <ul class="menu">
                                        <li class="leaf">
                                            <asp:LinkButton ID="btnOwn" title="Take ownership" runat="server" OnClick="btnOwn_Click" OnClientClick="ShowProgress(this)"> 
                                            <div class="link-icon">
                                                <image src="Content/css/images/file-user-icon.svg"  alt="Link"></image> Own
                                            </div>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="leaf">
                                            <asp:LinkButton ID="btnRelease" title="Release ownership" runat="server" OnClick="btnRelease_Click" OnClientClick="ShowProgress(this)"> 
                                            <div class="link-icon">
                                                <image src="Content/css/images/unlock-fill.svg"  alt="Link"></image> Release
                                            </div>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="leaf" runat="server">
                                            <asp:LinkButton ID="btnLink" title="Link to an existing case and/or birth and/or death record. The system will create a case record if one does not already exist." runat="server" OnClick="btnLink_Click" OnClientClick="ShowProgress(this)">
                                            <div class="link-icon">
                                                <image src="Content/css/images/link-solid.svg" alt="Link"></image> Link
                                            </div>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="leaf" runat="server">
                                            <asp:LinkButton ID="btnAssign" title="Create new case record" runat="server" Text="Confirm">
                                                <div class="link-icon">
                                                    <image src="Content/css/images/file-earmark-plus-fill.svg"  alt="Create Case Record ID"></image> Create New Case                                           
                                                </div>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="leaf">
                                            <asp:LinkButton ID="btnPreviousMatch" title="View previous potential matches" runat="server" Width="130px"> 
                                        <div class="link-icon">
                                            <image src="Content/css/images/backward-solid.svg"  alt="Previous Matches"></image> Previous Matches                                                   
                                        </div>
                                            </asp:LinkButton>
                                        </li>
                                        <li class="leaf">
                                            <asp:LinkButton ID="btnNextMatch" title="View next potential matches" runat="server" Width="130px"> 
                                        <div class="link-icon">
                                            <image src="Content/css/images/forward-solid.svg"  alt="Previous Matches"></image> Next Matches                                                   
                                        </div>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <%--<br />--%>

                        <div>
                            <label colspan="2"><b><%=assignedUserName%></b></label>
                            <asp:Label ID="lblSockerResponse" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="row row-no-padding">
                            <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                                <div class="wrapper">
                                    <form method="get" action="">
                                        <div class="Container Flipped">
                                            <div class="Content">

                                                <table id="mlTable" style="width: auto" border="1" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th class="sticky-unmatched" colspan="2">Candidate Case Report:
                                                <asp:Literal Text="" ID="ltrName" runat="server" />
                                                            </th>

                                                            <% 
                                                                if (dt == null)
                                                                {
                                                                    dt = ConvertToDatatable(Session["datarecords"]);
                                                                }
                                                                int counter = 2;
                                                                string value = "";
                                                                int a = 1;
                                                                if (dt.Columns.Count > 2)
                                                                {
                                                                    a = 2;
                                                                }
                                                                if (dt != null)
                                                                {
                                                                    for (int i = a; i < dt.Columns.Count; i++)
                                                                    {
                                                                        if (value != Convert.ToString(dt.Rows[0][i]))
                                                                        {
                                                                            if (i != a)
                                                                            { %>
                                                            <th class="sticky-matched" colspan="<%= counter %>">All Matched Case Reports for Case Record : <%= dt.Rows[4][2] %></th>
                                                            <%
                                                                    value = Convert.ToString(dt.Rows[0][i]);
                                                                    counter = 0;
                                                                }
                                                                if (i == dt.Columns.Count - 1)
                                                                {
                                                                    counter++;
                                                                    if (Convert.ToString(dt.Rows[0][i]) == "Case Reports")
                                                                    {
                                                            %>
                                                            <th class="sticky-matched" colspan="<%= counter %>">All Matched Case Reports for Case Record : <%= dt.Rows[4][2] %></th>

                                                            <%}
                                                                else
                                                                {
                                                            %>
                                                            <th class="sticky-matched" colspan="<%= counter %>">Matched Record</th>
                                                            <%}

                                                                    }
                                                                    else
                                                                    {
                                                                        value = Convert.ToString(dt.Rows[0][i]);
                                                                        counter++;
                                                                    }
                                                                }
                                                                else if (i == dt.Columns.Count - 1)
                                                                {
                                                                    counter++;
                                                                    if (Convert.ToString(dt.Rows[0][i]) == "Case Reports")
                                                                    {
                                                            %>
                                                            <th class="sticky-matched" colspan="<%= counter %>">All Matched Case Reports for Case Record : <%= dt.Rows[4][2] %></th>

                                                            <%}
                                                                else
                                                                {
                                                            %>
                                                            <th class="sticky-matched" colspan="<%= counter %>">Matched Record</th>
                                                            <%}

                                                                        }
                                                                        else
                                                                        {
                                                                            value = Convert.ToString(dt.Rows[0][i]);
                                                                            counter++;
                                                                        }
                                                                    }
                                                                }
                                                            %>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="table-body">
                                                        <tr>
                                                            <td class="alt">Match Details</td>

                                                            <% for (int i = 1; i <= 1; i++)
                                                                {%>
                                                            <td class="blue">
                                                                <button id="btndeepStat" title="Check the match details for the possible match" onclick="return deepstat('DeepStats.aspx?reporttype=<%=hdfTableName.Value %>&archiveId=<%=hdfArchiveID.Value%>&detailId=<%=dt.Rows[1][i]%>');" width="120px">
                                                                    <div class="link-icon">
                                                                        <image src="Content/css/images/info-circle.svg" alt="Match Details"></image>
                                                                        <strong>Match Details</strong>
                                                                    </div>
                                                                </button>
                                                            </td>
                                                            <% }%>



                                                            <% for (int i = 2; i < dt.Columns.Count; i++)
                                                                {%>
                                                            <td>&nbsp;</td>
                                                            <%} %>
                                                        </tr>

                                                        <% foreach (System.Data.DataRow item in dt.Rows)
                                                            {
                                                                if (Convert.ToString(item[0]) != "RecordFrom" && Convert.ToString(item[0]) != "IsMatchRecord" && Convert.ToString(item[0]) != "Phantom Hit" && !string.IsNullOrEmpty(Convert.ToString(item[0])))
                                                                {%>
                                                        <tr>

                                                            <% if (Convert.ToString(item[0]) == "Case Record ID" || Convert.ToString(item[0]) == "Trip")
                                                                {%>

                                                            <td class="alt"><b><%=item[0] %></b> </td>

                                                            <%}
                                                            else
                                                            { %>

                                                            <td class="alt"><%=item[0] %> </td>
                                                            <%}%>

                                                            <td class="blue">
                                                                <%
                                                                    if (Convert.ToString(item[0]) == "Trip")
                                                                    {
                                                                        int tripValue = Convert.ToInt32(item[1]);

                                                                        if (tripValue == 0)
                                                                        {%>
                                                                <span><b>1</b></span>
                                                                <%}
                                                                    else if (tripValue >= 1)
                                                                    {%>
                                                                <span class="span-red-link"><b>2</b></span>
                                                                <%}
                                                                    }
                                                                    else if (Convert.ToString(item[0]) == "Match Level")
                                                                    {
                                                                        int matchLevelValue = Convert.ToInt32(item[1]);

                                                                        if (matchLevelValue >= 0)
                                                                        {%>
                                                                <span></span>
                                                                <%}
                                                                    }
                                                                    else
                                                                    {%>
                                                                <%= item[1] %>
                                                                <%}
                                                                %>
                                                    
                                                            </td>


                                                            <% for (int i = 2; i < dt.Columns.Count; i++)
                                                                {%>

                                                           
                                                                <% if (Convert.ToString(item[0]) == "Case Record ID")
                                                                    
                                                                   {%>
                                                                  <td><b><%=item[i]%></b></td>   
                                                                    <%}
                                                                %>

                                                                <% if (i >= 2 && Convert.ToString(item[0]) == "Trip")
                                                                    {
                                                                        int tripValue = Convert.ToInt32(item[i]);

                                                                        if (tripValue == 0)
                                                                        {%>
                                                                       <td> <span><b></b></span></td>
                                                                        <%}
                                                                        else if (tripValue >= 1)
                                                                        {%>
                                                                        <td><span class="span-red-link"><b></b></span><%-- All matches Do not show the trip values in possible matches - Converting to null value--%>
                                                                        </td><%
                                                                        }
                                                                    }
                                                                %>

                                                                <% else if (Convert.ToString(item[0]) != "Case Record ID" && Convert.ToString(item[0]) != "Trip")
                                                                    {                                                                        
                                                                        string existingItemValue = Convert.ToString(item[1]).ToLower();
                                                                        string currentitemValue = item[i].ToString().ToLower();
                                                                         if (existingItemValue != currentitemValue && Convert.ToString(item[0]) != "Detail ID"
                                                                                                            && item[0].ToString() != "Scrutinizer ID" 
                                                                                                            && Convert.ToString(item[0]) != "Archive ID" 
                                                                                                            && Convert.ToString(item[0]) != "Case Report ID" 
                                                                                                            && Convert.ToString(item[0]) != "Case Record ID" 
                                                                                                            && Convert.ToString(item[0]) != "Birth Rec No." 
                                                                                                            && Convert.ToString(item[0]) != "Death Rec No." 
                                                                                                            && Convert.ToString(item[0]) != "Birth Certificate Number" 
                                                                                                             && Convert.ToString(item[0]) != "Confidence" 
                                                                                                            && Convert.ToString(item[0]) != "Match Level" 
                                                                                                            && Convert.ToString(item[0]) != "Trip"
                                                                                                            && Convert.ToString(item[0]) != "Received Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Processed Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Assimilated Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Search Context"
                                                                                                            && Convert.ToString(item[0]) != "messageState"
                                                                                                            && Convert.ToString(item[0]) != "Historic ID")
                                                                         {%>
                                                                           <td style="background: #FFC7CE !important"> <b><%=item[i] %></b> </td>
                                                                         <%}
                                                                         else
                                                                         {  %>

                                                                             <td> <%=item[i] %></td>
                                                                         <% 
                                                                         }
                                                                        }%>

                                                             <% }

                                                                if (dt.Columns.Count == 2)
                                                                {
                                                            %>
                                                            <td>&nbsp;</td>
                                                            <%} %>
                                                        </tr>
                                                        <% 
                                                                }
                                                            }
                                                        %>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="zoomPosition" style="display:none;">
        <strong>-</strong>
        <input type="range" disabled="disabled" id="zoomRange" min="50" max="200" value="160" step="10">
        <strong>+</strong>
    </div>

    <div id="dialog" class="modal" title="Selection Alert">
        <p id="message"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
                <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>

    <div id="socketConn">
        <p id="connSuccessMsg"></p>
    </div>

    <div id="dialogAssign" class="modal" title="Selection Alert">
        <p id="messageAssign"></p>
    </div>


    <% }
    %>

    <script src="Content/js/jquery/1.8.3/jquery.min.js"></script>
    <script>

        const table = document.getElementById('mlTable');
        const zoomRange = document.getElementById('zoomRange');

        zoomRange.addEventListener('input', function () {
            const zoomLevel = zoomRange.value;
            table.style.transform = `scale(${zoomLevel / 100})`;
            table.style.transformOrigin = 'top left';
        });

        function mySessionClear() {
            var someVarName = sessionStorage.removeItem("detailsId");
            sessionStorage.clear();
            var someVarName = sessionStorage.removeItem("caseDetailId");
            sessionStorage.clear();
        }
        function ShowProgress(btnEle) {

            if (btnEle && $(btnEle).hasClass('enableGrey')) {
                return;
            }
            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");
            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });
        }
        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }
        function CheckBoxFunction() {
            var inputElements = document.getElementsByClassName('chkHeader');
            for (var k = 0; inputElements[k]; ++k) {
                if (inputElements[k].checked) {
                    var checkboxes = document.querySelectorAll("input[type=checkbox]");
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].value != inputElements[k].value) {
                            for (var j = 0; j < checkboxes.length; j++) {
                                if (checkboxes[j] != inputElements[k]) {
                                    checkboxes[j].disabled = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        function myFunction() {
            var checkedValue = null;
            var isCheckCheckedBox = "false";
            var inputElements = document.getElementsByClassName('chkHeader');

            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].addEventListener("change", function () {
                    for (var j = 0; j < checkboxes.length; j++) {
                        if (checkboxes[j] != this) {
                            checkboxes[j].disabled = this.checked;
                        }
                    }
                });
            }
            for (var i = 0; inputElements[i]; ++i) {

                if (inputElements[i].checked) {
                    isCheckCheckedBox = "true";

                    var isChecked = sessionStorage.getItem("isCheckCheckedBox");

                    if (isChecked == null) {
                        sessionStorage.setItem("isCheckCheckedBox", isCheckCheckedBox);
                    }
                    var btnAssign = document.getElementById("<%=btnAssign.ClientID%>");
                    $(btnAssign).removeAttr('href');
                    $(btnAssign).removeClass('disableGrey');
                    $(btnAssign).addClass('enableGrey');
                    var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID%>");
                    $(btnPre).removeAttr('href');
                    $(btnPre).removeClass('disableGrey');
                    $(btnPre).addClass('enableGrey');

                    var btnNext = document.getElementById("<%=btnNextMatch.ClientID%>");
                    $(btnNext).removeAttr('href');
                    $(btnNext).removeClass('disableGrey');
                    $(btnNext).addClass('enableGrey');

                    var btnRelease = document.getElementById("<%=btnRelease.ClientID%>");
                    $(btnRelease).removeAttr('href');
                    $(btnRelease).removeClass('disableGrey');
                    $(btnRelease).addClass('enableGrey');

                    var btnLink = document.getElementById("<%=btnLink.ClientID%>");
                    $(btnLink).attr('href', "javascript: __doPostBack('ctl00$Content$btnLink', '')");
                    $(btnLink).removeClass('enableGrey');
                    $(btnLink).addClass('disableGrey');

                    checkedValue = inputElements[i].value;

                    if (document.getElementById("<%=hdfTableName.ClientID%>").value == "Matching_Linking") {
                        document.getElementById("<%=IsCaseDetailID.ClientID%>").value = "Case Report";
                    }
                    var caseDetailId = sessionStorage.getItem("caseDetailId");

                    if (caseDetailId == null) {
                        sessionStorage.setItem("caseDetailId", checkedValue);
                        sessionStorage.setItem("detailsId", checkedValue);
                    }
                    document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = sessionStorage.getItem("caseDetailId");
                    var someVarName = sessionStorage.getItem("detailsId");

                    if (someVarName != null) {
                        if (!someVarName.includes(checkedValue)) {
                            someVarName = someVarName + "," + checkedValue;
                        }
                    }
                    else {
                        someVarName = checkedValue;
                    }
                    sessionStorage.setItem("detailsId", someVarName);
                    document.getElementById("<%=hdfDetailsID.ClientID%>").value = sessionStorage.getItem("detailsId");
                }
                else {
                    checkedValue = inputElements[i].value;
                    someVarName = sessionStorage.getItem("caseDetailId");

                    if (isCheckCheckedBox == "false") {
                        var btnAssign = document.getElementById("<%=btnAssign.ClientID%>");
                    $(btnAssign).attr('href', "javascript: __doPostBack('ctl00$Content$btnAssign', '')");
                    $(btnAssign).removeAttr('disabled');
                    $(btnAssign).addClass('disableGrey');
                    $(btnAssign).removeClass('enableGrey');
                    var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID%>");
                    $(btnPre).attr('href', "javascript: __doPostBack('ctl00$Content$btnPreviousMatch', '')");
                    $(btnPre).removeAttr('disabled');
                    $(btnPre).addClass('disableGrey');
                    $(btnPre).removeClass('enableGrey');

                    var btnNext = document.getElementById("<%=btnNextMatch.ClientID%>");
                    $(btnNext).attr('href', "javascript: __doPostBack('ctl00$Content$btnNextMatch', '')");
                    $(btnNext).removeAttr('disabled');
                    $(btnNext).addClass('disableGrey');
                    $(btnNext).removeClass('enableGrey');

                    var btnRelease = document.getElementById("<%=btnRelease.ClientID%>");
                    $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
                    $(btnRelease).removeAttr('disabled');
                    $(btnRelease).addClass('disableGrey');
                    $(btnRelease).removeClass('enableGrey');


                    var btnLink = document.getElementById("<%=btnLink.ClientID%>");
                        $(btnLink).removeAttr('href');
                        $(btnLink).removeClass('disableGrey');
                        $(btnLink).addClass('enableGrey');
                    }

                    if (document.getElementById("<%=IsCaseDetailID.ClientID%>").value == "casereport" && (someVarName != null && !someVarName.includes(checkedValue))) {
                        var someVarName = sessionStorage.removeItem("detailsId");
                        sessionStorage.clear();
                        var someVarName = sessionStorage.removeItem("caseDetailId");
                        sessionStorage.clear();
                        document.getElementById("<%=hdfDetailsID.ClientID%>").value = null;
                    document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = null;
                    document.getElementById("<%=IsCaseDetailID.ClientID%>").value = null;
                }
                else if (someVarName != null && someVarName.includes(checkedValue)) {
                    var someVarName = sessionStorage.removeItem("detailsId");
                    sessionStorage.clear();
                    var someVarName = sessionStorage.removeItem("caseDetailId");
                    sessionStorage.clear();

                    document.getElementById("<%=hdfDetailsID.ClientID%>").value = null;
                    document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = null;
                    }
                }
            }
        }

        function isDataNull() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID%>");
            $(btnAssign).removeAttr('href');
            $(btnAssign).removeClass('disableGrey');
            $(btnAssign).addClass('enableGrey');
            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID%>");
            $(btnPre).removeAttr('href');
            $(btnPre).removeClass('disableGrey');
            $(btnPre).addClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID%>");
            $(btnNext).removeAttr('href');
            $(btnNext).removeClass('disableGrey');
            $(btnNext).addClass('enableGrey');

        }

        function myFirstClickFunction(tablename) {

            var checkedValue = null;
            var isCheckCheckedBox = "false";
            var inputElements = document.getElementsByClassName('chkHeader');
            document.getElementById("<%=hdfFirstTable.ClientID%>").value = tablename;
            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].addEventListener("change", function () {
                    for (var j = 0; j < checkboxes.length; j++) {
                        if (checkboxes[j] != this) {
                            checkboxes[j].disabled = this.checked;
                        }
                    }
                });
            }


            for (var i = 0; inputElements[i]; ++i) {

                if (inputElements[i].checked) {
                    isCheckCheckedBox = "true";

                    var isChecked = sessionStorage.getItem("isCheckCheckedBox");

                    if (isChecked == null) {
                        sessionStorage.setItem("isCheckCheckedBox", isCheckCheckedBox);
                    }

                    var btnAssign = document.getElementById("<%=btnAssign.ClientID%>");
                    $(btnAssign).removeAttr('href');
                    $(btnAssign).removeClass('disableGrey');
                    $(btnAssign).addClass('enableGrey');
                    var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID%>");
                    $(btnPre).removeAttr('href');
                    $(btnPre).removeClass('disableGrey');
                    $(btnPre).addClass('enableGrey');

                    var btnNext = document.getElementById("<%=btnNextMatch.ClientID%>");
                    $(btnNext).removeAttr('href');
                    $(btnNext).removeClass('disableGrey');
                    $(btnNext).addClass('enableGrey');

                    var btnRelease = document.getElementById("<%=btnRelease.ClientID%>");
                    $(btnRelease).removeAttr('href');
                    $(btnRelease).removeClass('disableGrey');
                    $(btnRelease).addClass('enableGrey');

                    var btnLink = document.getElementById("<%=btnLink.ClientID%>");
                    $(btnLink).attr('href', "javascript: __doPostBack('ctl00$Content$btnLink', '')");
                    $(btnLink).removeClass('enableGrey');
                    $(btnLink).addClass('disableGrey');

                    checkedValue = inputElements[i].value;

                    if (document.getElementById("<%=hdfTableName.ClientID%>").value == "Matching_Linking") {
                        document.getElementById("<%=IsCaseDetailID.ClientID%>").value = "Case Report";
                    }
                    var caseDetailId = sessionStorage.getItem("caseDetailId");

                    if (caseDetailId == null) {
                        sessionStorage.setItem("caseDetailId", checkedValue);
                        sessionStorage.setItem("detailsId", checkedValue);

                    }
                    document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = sessionStorage.getItem("caseDetailId");
                    var someVarName = sessionStorage.getItem("detailsId");

                    if (someVarName != null) {
                        if (!someVarName.includes(checkedValue)) {
                            someVarName = someVarName + "," + checkedValue;
                        }
                    }
                    else {
                        someVarName = checkedValue;
                    }
                    sessionStorage.setItem("detailsId", someVarName);
                    document.getElementById("<%=hdfDetailsID.ClientID%>").value = sessionStorage.getItem("detailsId");
                }
                else {

                    checkedValue = inputElements[i].value;
                    someVarName = sessionStorage.getItem("caseDetailId");
                    if (isCheckCheckedBox == "false") {
                        var btnAssign = document.getElementById("<%=btnAssign.ClientID%>");
                        $(btnAssign).attr('href', "javascript: __doPostBack('ctl00$Content$btnAssign', '')");
                        $(btnAssign).removeAttr('disabled');
                        $(btnAssign).addClass('disableGrey');
                        $(btnAssign).removeClass('enableGrey');
                        var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID%>");
                        $(btnPre).attr('href', "javascript: __doPostBack('ctl00$Content$btnPreviousMatch', '')");
                        $(btnPre).removeAttr('disabled');
                        $(btnPre).addClass('disableGrey');
                        $(btnPre).removeClass('enableGrey');

                        var btnNext = document.getElementById("<%=btnNextMatch.ClientID%>");
                        $(btnNext).attr('href', "javascript: __doPostBack('ctl00$Content$btnNextMatch', '')");
                        $(btnNext).removeAttr('disabled');
                        $(btnNext).addClass('disableGrey');
                        $(btnNext).removeClass('enableGrey');

                        var btnRelease = document.getElementById("<%=btnRelease.ClientID%>");
                        $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
                        $(btnRelease).removeAttr('disabled');
                        $(btnRelease).addClass('disableGrey');
                        $(btnRelease).removeClass('enableGrey');

                        var btnLink = document.getElementById("<%=btnLink.ClientID%>");
                        $(btnLink).removeAttr('href');
                        $(btnLink).removeClass('disableGrey');
                        $(btnLink).addClass('enableGrey');
                    }
                    if (document.getElementById("<%=IsCaseDetailID.ClientID%>").value == "casereport" && (someVarName != null && !someVarName.includes(checkedValue))) {
                        var someVarName = sessionStorage.removeItem("detailsId");
                        sessionStorage.clear();
                        var someVarName = sessionStorage.removeItem("caseDetailId");
                        sessionStorage.clear();
                        document.getElementById("<%=hdfDetailsID.ClientID%>").value = null;
                        document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = null;
                        document.getElementById("<%=IsCaseDetailID.ClientID%>").value = null;
                    }
                    else if (someVarName != null && someVarName.includes(checkedValue)) {
                        var someVarName = sessionStorage.removeItem("detailsId");
                        sessionStorage.clear();
                        var someVarName = sessionStorage.removeItem("caseDetailId");
                        sessionStorage.clear();
                        document.getElementById("<%=hdfDetailsID.ClientID%>").value = null;
                        document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = null;
                    }
                }
            }
        }

        function deepstat(url) {

            const title = "DeepStat";
            const w = "700";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, '_blank', title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function popupAlert(tablename) {

            if (tablename == "Matching_Linking") {
                document.getElementById("message").innerHTML = "Please check birth records and death records tab for possible matches.";
            }
            else if (tablename == "BirthRecords") {
                document.getElementById("message").innerHTML = "Please check case reports and death records tab for possible matches.";
            }
            else {
                document.getElementById("message").innerHTML = "Please check case reports and birth records tab for possible matches.";

            }

            $(function () {
                $("#dialog").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    buttons: [
                        {
                            text: "OK",
                            open: function () {
                                $(this).addClass('okcls')
                            },
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ],
                    position: {
                        my: "center center",
                        at: "center center"
                    }
                });
            });

            var isChecked = sessionStorage.getItem("isCheckCheckedBox");

            if (isChecked == null) {
                sessionStorage.setItem("isCheckCheckedBox", true);
            }
        }

        function assignAlert(event) {
            event.preventDefault();
            let $btnEle = $('#Content_btnAssign');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }

            $(function () {
                $("#dialogAssign").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Selection Confirmation",
                    open: function () {
                        var connErrorMsg = 'Are you sure you want to Assign a new case record ID ? Please make sure to check all the tabs for possible matches before Assign.';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        Yes: function () {
                            allowAssign = true;
                            $(this).dialog("close");
                            ShowProgress();
                            <%= Page.ClientScript.GetPostBackEventReference(btnAssign, String.Empty)  %>  // __doPostBack()
                        },
                        No: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
           });
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",


                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server.Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);

                },

                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        function socketConnSuccess() {

            $("#socketConn").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Successful",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/database-check.svg" width="40" height="40"/>')
                    var connMsg = ' Server is available.  ';
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#6495ED");;
        }
    </script>
</asp:Content>

