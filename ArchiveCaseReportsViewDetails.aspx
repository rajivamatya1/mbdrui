﻿<%@ Page Title="MBDR UI Matching View Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="ArchiveCaseReportsViewDetails.aspx.cs" Inherits="ArchiveCaseReportsViewDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <% if (Session["username"] != null)
        { %>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Literal Text="" ID="ltrTableName" runat="server" />
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:HiddenField ID="hdfTableName" runat="server" />
                                    <asp:HiddenField ID="hdfNextRowID" runat="server" />
                                    <asp:HiddenField ID="hdfPreviousRowID" runat="server" />
                                    <asp:HiddenField ID="hdfSchema" runat="server" />
                                    <asp:HiddenField ID="hdfKeyName" runat="server" />
                                    <asp:HiddenField ID="hdfRowID" runat="server" />
                                    <asp:HiddenField ID="hdfSearchText" runat="server" />
                                    <asp:HiddenField ID="hdfName" runat="server" />
                                    <asp:HiddenField ID="hdfSearch" runat="server" />
                                    <asp:HiddenField ID="hdfColumnName" runat="server" />
                                    <asp:HiddenField ID="hdfFromDate" runat="server" />
                                    <asp:HiddenField ID="hdfToDate" runat="server" />
                                    <asp:HiddenField ID="hdfChangeFacility" runat="server" />
                                    <asp:Button Text="Previous" runat="server" OnClick="btnPrev_Click" ID="btnPrev" CssClass="btn btn-primary" OnClientClick="return ShowProgress();"/>
                                    <asp:Button Text="Next" runat="server" OnClick="btnNext_Click" ID="btnNext" CssClass="btn btn-primary" OnClientClick="return ShowProgress();"/>                                    
                                    <asp:Button Text="Return to Search Results" OnClick="btnAID_Click" runat="server" ID="btnAID" CssClass="btn btn-primary" OnClientClick="return ShowProgress();"/>    
                                    <asp:Button Text="New Search" OnClick="btnAll_Click" runat="server" ID="btnAll" CssClass="btn btn-primary" OnClientClick="return ShowProgress();"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="admin-workarea">
                        <div class="row row-no-padding">
                            <div class="col col-lg-12 col-sm-12">
                                <div class="form-item">
                                    <label for="Content_ddlColumn">Filter by Data Element</label>
                                    <asp:DropDownList runat="server" ID="ddlColumn" CssClass="form-control" ValidationGroup="search">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="ddlColumn"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-item">
                                    <asp:Button Text="Apply" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-success" ValidationGroup="search" />
                                </div>
                                <div class="form-item">
                                    <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click" CssClass="btn btn-danger" />
                                </div>
                            </div>
                        </div>
                        <div class="row row-no-padding">
                            <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                                <div class="tableFixHead">
                                    <asp:GridView CssClass="table table-bordered table-striped table-fixed" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                                      OnRowDataBound="gvDynamic_RowDataBound"  EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" ValidateRequestMode="Disabled">
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% } %>

        <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
       <br />
    </div>

    <script>
        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ShowProgress(para) {

            var modal = $('<div />');
            modal.addClass("modalspin");
            $('body').append(modal);
            var loading = $(".loadingspin");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ "position": "center", top: top, left: left });

            return true;
        }
    </script>
</asp:Content>

