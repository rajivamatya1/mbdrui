﻿using DynamicGridView.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;

public partial class DeepStats : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            DataTable dsRows = new DataTable();
            DataTable dt = new DataTable();
            int archiveId = Convert.ToInt32(Convert.ToString(Request.QueryString["archiveId"]));
            string reportType = Convert.ToString(Request.QueryString["reportType"]);
            string detailId = Convert.ToString(Request.QueryString["detailId"]);
            string pageview = Convert.ToString(Request.QueryString["pagecall"]);
            string whereM = string.Empty;
            string query = string.Empty;
            string kind = string.Empty;
            string reportName = string.Empty;
            if (reportType.Equals("DeathRecords"))
            {
                reportName = "Death Records";
                whereM = "where archiveId=" + archiveId + " and kind=2 order by query, attemptTime desc ";
                kind = "2";
                if (pageview == "clearview")
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerClearView_DeathRecords c on m.scrutinizerId = c.scrutinizerId where m.archiveId=" + archiveId + "";
                }
                else
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerFullViewDeathRecords c on m.scrutinizerId = c.scrutinizerId where  m.archiveId=" + archiveId + "";
                }
            }

            else if (reportType.Equals("BirthRecords"))
            {
                reportName = "Birth Records";
                kind = "1";
                whereM = "where archiveId=" + archiveId + " and kind=1 order by query, attemptTime desc  ";
                if (pageview == "clearview")
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerClearView_BirthRecords c on m.scrutinizerId = c.scrutinizerId where m.archiveId=" + archiveId + "";
                }
                else
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerFullViewBirthRecords c on m.scrutinizerId = c.scrutinizerId where m.archiveId=" + archiveId + "";
                }
            }
            else
            {
                reportName = "Case Reports";
                kind = "0";
                whereM = "where archiveId=" + archiveId + " and kind=0 order by query, attemptTime desc  ";
                if (pageview == "clearview")
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerClearViewCaseReports c on m.scrutinizerId = c.scrutinizerId where m.archiveId=" + archiveId + "";
                }
                else
                {
                    query = "select top 1 m.childFirstName,m.childLastName,c.childFirstName,c.childLastName from MBDR_System.Matching_Linking m inner join MBDR_System.ScrutinizerFullViewCaseReports c on m.scrutinizerId = c.scrutinizerId where m.archiveId=" + archiveId + "";
                }
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string whereDS = "WHERE archiveId=" + archiveId +" and kind = "+ kind + "";
                string order = "ORDER BY Trips DESC";
                string queryM = string.Format("SELECT [scrutinizerId],Trips,[Statistic ID],Hits,[Match Level],Statistic FROM {0}.{1} {2} {3}", "MBDR_System", "DeepstatView", whereDS, order);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dsRows);
                gvDynamic.DataSource = dsRows;
                gvDynamic.DataBind();
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    lblMsgQuery.Text = string.Format("Comparing {0} {1} in {2}", dt.Rows[0][0], dt.Rows[0][1], reportName);
                }
                else
                {
                    lblMsgQuery.Text = "There is no data to compare.";
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "DeepStats_Page_Load");
        }
    }

   
    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = e.Row.DataItem as DataRowView;
            if (rowView != null)
            {
                int tripsValue = Convert.ToInt32(rowView["Trips"]);
                if (tripsValue == 2)
                {
                    e.Row.Cells[1].Text = $"<b><span class=\"red-italic\">{tripsValue}</span></b>";
                }
            }
        }
    }
}