﻿<%@ Page Title="User Management Page" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="UserManagementPage.aspx.cs" Inherits="UserManagementPage" EnableEventValidation="false" %>


<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
  <%--  <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>

    <div>
        <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
        <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
        <asp:HiddenField runat="server" ID="hdfTableName" Value="" />
        <asp:HiddenField ID="hidMaskedPassword" runat="server" />
        <asp:HiddenField ID="hdnCommandArgument" runat="server" />
        <asp:HiddenField ID="hdnUserID" runat="server" />
        <asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" Visible="false" />

        <asp:HiddenField runat="server" ID="hdExtFN" />
        <asp:HiddenField runat="server" ID="hdExtLN" />
        <asp:HiddenField runat="server" ID="hdExtEmail" />
        <asp:HiddenField runat="server" ID="hdExtConEmail" />
        <asp:HiddenField runat="server" ID="hdExtPass" />
        <asp:HiddenField runat="server" ID="hdExtConPass" />
        <asp:HiddenField runat="server" ID="hdExtRoles" />
        <asp:HiddenField runat="server" ID="hdEnviroment" />
    </div>

    <div id="mlsSecurityTitle">
        <div style="margin-left: 15px; float: left;">
            <h1>User Management</h1>
        </div>
        <div style="margin-right: 15px; float: right;">
            <asp:Button ID="btnNewAc" runat="server" Text="Create New Account" Enabled="false" CssClass="cursor-default" OnClick="btnNewAc_Click" />
        </div>
    </div>

    <div class="col col-lg-12 col-md-12 col-sm-12" id="createUserAccess" visible="true" runat="server">
        <div class="program" id="mlsSecurityHeader">
            <div class="program-header">
                <div class="program-name">
                    <asp:Label ID="lblHeader" runat="server"></asp:Label>
                </div>
            </div>
            <div class="program-description">
                <div class="row">
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblFirstName" for="firstName">First Name:</label>
                            <asp:TextBox ID="txtFirstName" runat="server" EnableViewState="false" placeholder="First Name" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" CssClass="form-text" ID="ReqValFirstName" ControlToValidate="txtFirstName" runat="server" ErrorMessage="Please enter first name."></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblLastName" for="lastName">Last Name:</label>
                            <asp:TextBox ID="txtLastName" runat="server" EnableViewState="false" placeholder="Last Name" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" CssClass="form-text" ID="ReqValLastName" ControlToValidate="txtLastName" runat="server" ErrorMessage="Please enter Last Name."></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblEmail" for="email">Email:</label>
                            <asp:TextBox ID="txtEmailId" runat="server" TextMode="Email" EnableViewState="false" placeholder="username@michigan.gov" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" CssClass="form-text" ID="ReqValEmail" ControlToValidate="txtEmailId" runat="server" ErrorMessage="Please enter email address."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validationEmail" runat="server" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" ValidationExpression="^\w+([-+.']\w+)*@michigan\.gov$" ControlToValidate="txtEmailId" ErrorMessage="Email format must be username@michigan.gov" />
                        </div>
                    </div>
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblConfirmEmail" for="confirmEmail">Confirm Email:</label>
                            <asp:TextBox ID="txtConfirmMail" runat="server" TextMode="Email" EnableViewState="false" placeholder="username@michigan.gov" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" CssClass="form-text" ID="ReqValConfirmEmail" ControlToValidate="txtConfirmMail" runat="server" ErrorMessage="Please re-enter email address."></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompValConfirmEmail" runat="server" ControlToValidate="txtConfirmMail" ControlToCompare="txtEmailId" ErrorMessage="Email addresses do not match." ValidationGroup="search" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                        </div>
                    </div>
                </div>
                <div class="row" id="divpassword" runat="server" visible="true">
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblUserPass" for="password">Password:</label>
                            <asp:TextBox ID="txtUserPass" runat="server" TextMode="Password" EnableViewState="false" placeholder="Enter Password" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ValidationGroup="search" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" CssClass="form-text" ID="ReqValPass" ControlToValidate="txtUserPass" runat="server" ErrorMessage="Please enter password."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegExpPass" runat="server" ControlToValidate="txtUserPass"  ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" CssClass="form-text" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_!*.~-]).{8,}$" ValidationGroup="search" ErrorMessage="Password must contain at least 8 characters, including an uppercase letter, a lower case letter, a number and a symbol."/>
                        </div>
                    </div>
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblConfirmPassword" for="confirmPassword">Confirm Password:</label>
                            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" EnableViewState="false" placeholder="Enter Password" autocomplete="off" onkeydown="return disableEnter(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator Width="100%" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" ValidationGroup="search" CssClass="form-text" ID="ReqValConfirmPass" ControlToValidate="txtConfirmPassword" runat="server" ErrorMessage="Please confirm password."></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompValConfirmPass" runat="server" ValidationGroup="search" ControlToValidate="txtConfirmPassword" ControlToCompare="txtUserPass" ErrorMessage=" Passwords do not match." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
                            <asp:RegularExpressionValidator ID="RegExpConfirmPass" runat="server" ControlToValidate="txtConfirmPassword"  ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" CssClass="form-text" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_!*.~-]).{8,}$" ValidationGroup="search" ErrorMessage="Password must contain at least 8 characters, including an uppercase letter, a lower case letter, a number and a symbol."/>
                      
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item-userManagement">
                            <label id="lblRoleType" for="roleType">Role Type:</label>
                            <asp:DropDownList ID="ddlRoleType" runat="server" AutoPostBack="false" onkeydown="return disableEnter(event);"></asp:DropDownList>
                            <asp:CustomValidator ID="cvRoleType" runat="server" ForeColor="Red" Font-Size="XX-Small" ControlToValidate="ddlRoleType" CssClass="form-text"
                                ErrorMessage="Please select a role." OnServerValidate="cvRoleType_ServerValidate"
                                ValidationGroup="search">
                            </asp:CustomValidator>
                        </div>
                    </div>
                    <div class="col col-lg-6 col-sm-6">
                        <div class="form-item">
                            <asp:Button ID="btnCreate" runat="server" Text="Create" ValidationGroup="search" OnClick="btnCreate_Click" Enabled="false" CssClass="cursor-default" />
                            <asp:Button ID="btnClear" runat="server" Text="Clear" OnClientClick="resetFields(); return false;" UseSubmitBehavior="false" CssClass="btnCommonBlue" Visible="true" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="mlsSecurityBody">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <span class="program-title-usermgmt">Existing User Accounts</span>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button ID="btnUserRole" Visible="true" runat="server" Text="User Roles" OnClick="btnUserRole_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                    <asp:Button ID="btnPermissions" Visible="true" runat="server" Text="Permissions" OnClick="btnPermissions_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-view-matching">
            <div class="row">
                <div class="col col-lg-12 col-sm-12">
                    <div class="tableFixHead">
                        <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                            OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                            EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                            OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowEditing="gvDynamic_RowEditing" OnRowCreated="gvDynamic_RowCreated"
                            OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                            PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="selectRoleType">
        <p id="roleNotSelected"></p>
    </div>

    <div id="dialogDeactive" class="modal" title="Deactive Alert">
        <p id="messagDeactive"></p>
    </div>

    <div id="diaBox">
        <p id="dynamicDiaBox"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
           <br />
    </div>

       <div id="socketConnError" class="socketConnErrorModal">
       <p id="connErrorMsg"></p>
   </div>

     <div id="securityMsg">
 </div>

    <script type="text/javascript">   

        function clearBGD() {
            
            var headerContent = document.getElementById("mlsSecurityHeader");
            var bodyContent = document.getElementById("mlsSecurityBody");
            var titleContent = document.getElementById("mlsSecurityTitle");

            if (headerContent) {
                headerContent.style.display = "none";
            }
            if (bodyContent) {
                bodyContent.style.display = "none";
            }
            if (titleContent) {
                titleContent.style.display = "none";
            }
            sessionStorage.clear();
        }

        function disableEnter(event)
        {
            if (event.key == "Enter")
            {
                return false;
            }
        }

        function resetFields() {

            document.getElementById("<%=txtFirstName.ClientID%>").value ="";
            document.getElementById("<%=txtLastName.ClientID%>").value = "";

            document.getElementById("<%=txtEmailId.ClientID%>").value ="";
            document.getElementById("<%=txtConfirmMail.ClientID%>").value = ""; 

            var hdEnv = document.getElementById("<%=hdEnviroment.ClientID%>").value;

            if (hdEnv == 'dev' || hdEnv == 'qa') {

                document.getElementById("<%=txtUserPass.ClientID%>").value = "";
                document.getElementById("<%=txtConfirmPassword.ClientID%>").value = "";

                document.getElementById("<%=ReqValPass.ClientID %>").style.display = "none";
                document.getElementById("<%=ReqValConfirmPass.ClientID %>").style.display = "none";
                document.getElementById("<%=RegExpPass.ClientID %>").style.display = "none";
                document.getElementById("<%=RegExpConfirmPass.ClientID %>").style.display = "none";
                document.getElementById("<%=CompValConfirmPass.ClientID %>").style.display = "none";
            }

            document.getElementById("<%=ddlRoleType.ClientID%>").value = "";

            document.getElementById("<%=ReqValFirstName.ClientID %>").style.display = "none";
            document.getElementById("<%=ReqValLastName.ClientID %>").style.display = "none";

            document.getElementById("<%=ReqValEmail.ClientID %>").style.display = "none";
            document.getElementById("<%=validationEmail.ClientID %>").style.display = "none";
            document.getElementById("<%=ReqValConfirmEmail.ClientID %>").style.display = "none";
            document.getElementById("<%=CompValConfirmEmail.ClientID %>").style.display = "none";
            
            document.getElementById("<%=cvRoleType.ClientID %>").style.display = "none";           


            var button = document.getElementById("<%=btnCreate.ClientID%>");

            button.setAttribute("disabled", "disabled");
            button.className = "cursor-default";

            return false;
        }

        function onTextChange()
        {
            var inputs = document.querySelectorAll("input[type=text], input[type=email], input[type=password], select");
            var button = document.getElementById("<%=btnCreate.ClientID%>");
            var hdEnv = document.getElementById("<%=hdEnviroment.ClientID%>").value;
            var enable = true;

            // if validators are used

            if (!Page_IsValid) {
                enable = false;
                return false;
            }

            // Additional Validation 

            var firstname = document.getElementById("<%=txtFirstName.ClientID%>").value;
            var lastname = document.getElementById("<%=txtLastName.ClientID%>").value;
            var email = document.getElementById("<%=txtEmailId.ClientID%>").value;
            var confirmEmail = document.getElementById("<%=txtConfirmMail.ClientID%>").value;       
            var roles = document.getElementById("<%=ddlRoleType.ClientID%>").value;
           

           

            if (button.value === "Create")
            {
                if (hdEnv == 'dev' || hdEnv == 'qa')
                {
                    var password = document.getElementById("<%=txtUserPass.ClientID%>").value;
                    var confirmPassword = document.getElementById("<%=txtConfirmPassword.ClientID%>").value;

                    if (!confirmEmail && !password && !confirmPassword) {
                        enable = false;
                    }
                }

                if (!firstname && !lastname && !email)
                {
                    enable = false;
                }

                if (password && password !== confirmPassword)
                {
                    enable = false;
                }
            }
            else if (button.value === "Update")
            {
                var ExtFirstName = document.getElementById("<%=hdExtFN.ClientID%>").value;
                var ExtLastName = document.getElementById("<%=hdExtLN.ClientID%>").value;

                var ExtEmail = document.getElementById("<%=hdExtEmail.ClientID%>").value;
                var ExtConfirmEmail = document.getElementById("<%=hdExtConEmail.ClientID%>").value;

                if (hdEnv == 'dev' || hdEnv == 'qa')
                {
                    var password = document.getElementById("<%=txtUserPass.ClientID%>").value;
                    var ExtPassword = document.getElementById("<%=hdExtPass.ClientID%>").value;
                    var ExtConfirmPassword = document.getElementById("<%=hdExtConPass.ClientID%>").value;
                }

                var ExtRoles = document.getElementById("<%=hdExtRoles.ClientID%>").value;                

                if (ExtFirstName !== firstname || ExtLastName !== lastname || ExtEmail !== email || ExtConfirmEmail !== confirmEmail || ExtRoles !== roles || ExtPassword !== password) {
                    enable = true;
                }
                else
                {
                    enable = false;
                }
            }

                        
            if (email !== confirmEmail)
            {
                enable = false;
            }

            inputs.forEach(function (input)
            {
                if (input.value.trim() === "" || input.value === "Select Role")
                {
                    enable = false;
                }
            });

            if (enable)
            {
                button.removeAttribute("disabled");
                button.className = "cursor-point";
            }
            else
            {
                button.setAttribute("disabled", "disabled");
                button.className = "cursor-default";
            }
        }

        document.addEventListener("DOMContentLoaded", function () {
            var inputs = document.querySelectorAll("input[type=text], input[type=email], input[type=password], select");
            
            inputs.forEach(function (input) {
                
                input.addEventListener("input", onTextChange);
                input.addEventListener("change", onTextChange);  //To handle select change
            });
        });

        function SelectRoleType(message) {

            $("#selectRoleType").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Confirmation",

                open: function () {
                    var connMsg = message;
                    $(this).html(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

        function deactiveAlert(button) {
          
            var disableButton = document.getElementById("DisableButton");
            if (disableButton) {
                disableButton.style.display = 'inline-block';
            }

            $(function () {
                $("#dialogDeactive").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "User Deactivation Confirmation",
                    open: function () {
                        var connErrorMsg = 'You are about to disable a user account. Do you want to proceed?(Y/N)';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        Yes: function () {
                            allowDeactive = true;
                            $(this).dialog("close");
                    
                            <%= Page.ClientScript.GetPostBackEventReference(btnHidden, hdnUserID.Value)  %>

                            ShowProgress();
                        },
                        No: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
            });
        }

        function dialogBox(message, titleFor) {

            $("#diaBox").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

        function ShowProgress() {

            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");

            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });

            return true;
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",

                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },

                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });

    </script>

    <% } %>
</asp:Content>
