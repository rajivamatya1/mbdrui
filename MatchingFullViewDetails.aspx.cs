﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Web;
using System.Data.SqlTypes;

public partial class MatchingFullViewDetails : System.Web.UI.Page
{
    protected DataTable dt;
    protected DataTable dtSession;
    protected string assignedUserName;
    public string iFrameSrc { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            if (!IsPostBack)
            {
                int scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
                hdfPageRedirect.Value = "first";
                string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
                string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
                string compareTableName = !string.IsNullOrEmpty(Request.QueryString["CompTable"]) ? Convert.ToString(Request.QueryString["CompTable"]) : "CaseReports";

                hdfMultipleTableName.Value = tableName;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                hdfDataKeyName.Value = keyName;

                if (tableName == "ScrutinizerFullViewBirthRecords")
                {
                    hdfTableKeyName.Value = "Birth Records";
                }
                else if (tableName == "ScrutinizerFullViewDeathRecords")
                {
                    hdfTableKeyName.Value = "Death Records";
                }
                else
                {
                    hdfTableKeyName.Value = "Case Reports";
                }

                DataSet dsRows = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where scrutinizerId = " + scrutinizerId + "";
                    query = string.Format("select top 1 * from {0}.{1} {2}", schema, "Matching_Linking", where);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dsRows);
                    foreach (DataRow dr in dsRows.Tables[0].Rows)
                    {
                        hdfArchiveID.Value = Convert.ToString(dr["archiveId"]);
                        break;
                    }
                }
                hdfRowID.Value = caseRecId.ToString();
                hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());

                var _list = new List<MatchingViewModel>();
                _list = ListRecords(dsRows, false);
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                var row = _list.Where(s => s.ScrutinizerID == scrutinizerId).FirstOrDefault();



                List<ColumnList> lstRecord = new List<ColumnList>();
                if (row != null)
                {
                    var filter = Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                    if (filter != null)
                    {
                        includeColumns = filter.IncludedColumns;
                        readonlyColumns = filter.ReadonlyColumns;
                        dataKeyname = filter.DataKeyName;
                    }

                    List<string> lstColumns = includeColumns.Split(',').ToList();
                    List<MatchingViewModel> lstMatches = new List<MatchingViewModel>();
                    List<MatchingViewModel> lstMatchesRecords = new List<MatchingViewModel>();

                    dt = new DataTable();
                    ltrName.Text = string.Format("{0} {1}", row.ChildFirstName, row.ChildLastName);


                    string query = string.Empty;
                    string where = " where SC.scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + " and caseRecId =" + caseRecId + "";
                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        string className = btnBirthRecords.CssClass;
                        btnBirthRecords.CssClass = className + " active";
                        btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                        btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                        hdfTableKeyName.Value = "Birth Records";
                        hdfTableName.Value = "BirthRecords";
                        hdfSchema.Value = "Reference";
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC {0} ", where);
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        string className = btnDeathRecords.CssClass;
                        btnDeathRecords.CssClass = className + " active";
                        btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
                        btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                        hdfTableKeyName.Value = "Death Records";
                        hdfTableName.Value = "DeathRecords";
                        hdfSchema.Value = "Reference";
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC {0}", where);
                    }
                    else
                    {
                        string className = btnCaseReports.CssClass;
                        btnCaseReports.CssClass = className + " active";
                        btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                        btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                        hdfTableKeyName.Value = "Case Reports";
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC {0}", where);
                    }


                    lstMatches = _list; //matching link view
                    lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);
                    lstMatches.AddRange(lstMatchesRecords);
                    PropertyInfo[] properties = typeof(MatchingViewModel).GetProperties();

                    for (int i = 0; i < lstMatches.Count + 1; i++)
                    {
                        dt.Columns.Add();
                    }
                    for (int i = 0; i < properties.Length; i++)
                    {
                        DataRow dr = dt.NewRow();
                        var colName = lstColumns.Find(s => s.StartsWith(properties[i].Name));
                        string displayColname = string.Empty;
                        if (!string.IsNullOrEmpty(colName))
                        {
                            string columnName = colName.Split(':')[1];
                            dr[0] = columnName;
                            displayColname = columnName;
                        }
                        else
                        {
                            displayColname = properties[i].Name;
                            foreach (var col in lstColumns)
                            {
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dr[0] = columnName.Split(':')[1];
                                    // break;
                                }
                                //break;
                            }
                        }

                        dr[1] = Convert.ToString(properties[i].GetValue(row, null));
                        for (int j = 0; j < lstMatches.Count; j++)
                        {
                            dr[j + 1] = Convert.ToString(properties[i].GetValue(lstMatches[j], null));
                        }
                        if (dr[0] != null)
                        {
                            dt.Rows.Add(dr);
                        }
                    }
                    DataRow dr1 = dt.NewRow();
                    dr1[0] = "IsMatchRecord";
                    for (int i = 2; i < dt.Columns.Count; i++)
                    {

                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (string.IsNullOrEmpty(hdfDetailsID.Value))
                            {
                                dr1[i] = "Check for Linking Process";
                            }
                        }
                    }
                    dt.Rows.Add(dr1);
                    Session["datarecords"] = dt;

                    //scrutinizerdatarecords pull from ScrutinizerFullView Table
                    DataTable dtSc = new DataTable();
                    Session["scrutinizerdatarecords"] = null;

                    dtSc = CheckscrutinizerIdValue();
                    Session["scrutinizerdatarecords"] = dtSc;

                    foreach (DataRow dr in dtSc.Rows)
                    {
                        if (Convert.ToString(dr["scrutinizerKind"]) == "0")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                                {
                                    if (hdfTableKeyName.Value != "Birth Records")
                                    {
                                        string className = btnBirthRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnBirthRecords.CssClass = className + " activeGreen";
                                        btnBirthRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnBirthRecords.CssClass;
                                    //if (!btnBirthRecords.CssClass.Contains("btn-disabled"))
                                    if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnBirthRecords.CssClass = className + " btn-disabled";
                                        btnBirthRecords.Enabled = false;
                                    }
                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                                {
                                    if (hdfTableKeyName.Value != "Death Records")
                                    {
                                        string className = btnDeathRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnDeathRecords.CssClass = className + " activeGreen";
                                        btnDeathRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnDeathRecords.CssClass;
                                    if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnDeathRecords.CssClass = className + " btn-disabled";
                                        btnDeathRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                        else if (Convert.ToString(dr["scrutinizerKind"]) == "1")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                                {
                                    if (hdfTableKeyName.Value != "Case Reports")
                                    {
                                        string className = btnCaseReports.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnCaseReports.CssClass = className + " activeGreen";
                                        btnCaseReports.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnCaseReports.CssClass;
                                    if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnCaseReports.CssClass = className + " btn-disabled";
                                        btnCaseReports.Enabled = false;
                                    }
                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                                {
                                    if (hdfTableKeyName.Value != "Death Records")
                                    {
                                        string className = btnDeathRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnDeathRecords.CssClass = className + " activeGreen";
                                        btnDeathRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnDeathRecords.CssClass;
                                    if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnDeathRecords.CssClass = className + " btn-disabled";
                                        btnDeathRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                        else if (Convert.ToString(dr["scrutinizerKind"]) == "2")
                        {
                            foreach (DataRow dr2 in dtSc.Rows)
                            {
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                                {
                                    if (hdfTableKeyName.Value != "Case Reports")
                                    {
                                        string className = btnCaseReports.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnCaseReports.CssClass = className + " activeGreen";
                                        btnCaseReports.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnCaseReports.CssClass;
                                    if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnCaseReports.CssClass = className + " btn-disabled";
                                        btnCaseReports.Enabled = false;
                                    }

                                }
                                if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                                {
                                    if (hdfTableKeyName.Value != "Birth Records")
                                    {
                                        string className = btnBirthRecords.CssClass;
                                        if (className.Contains("btn-disabled"))
                                        {
                                            className = className.Replace("btn-disabled", "");
                                        }
                                        btnBirthRecords.CssClass = className + " activeGreen";
                                        btnBirthRecords.Enabled = true;
                                        hdfIsTabClicked.Value = "false";
                                        break;
                                    }
                                }
                                else
                                {
                                    string className = btnBirthRecords.CssClass;
                                    if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className.Contains("active"))
                                    {
                                        btnBirthRecords.CssClass = className + " btn-disabled";
                                        btnBirthRecords.Enabled = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    dt = (DataTable)Session["datarecords"];
                }
                string isOwnerID = IsOwnerShip();
                if (!isOwnerID.Equals(Session["userid"]))
                {
                    btnOwn.Enabled = true;
                    btnRelease.Enabled = false;
                    btnLink.Enabled = false;
                    btnAssign.Enabled = false;
                    btnPreviousMatch.Enabled = false;
                    btnNextMatch.Enabled = false;

                    btnOwn.CssClass = "disableGrey";
                    btnRelease.CssClass = "enableGrey";
                    btnLink.CssClass = "enableGrey";
                    btnAssign.CssClass = "enableGrey";
                    btnPreviousMatch.CssClass = "enableGrey";
                    btnNextMatch.CssClass = "enableGrey";
                    hdfOwner.Value = "false";
                }
                else
                {
                    btnOwn.Enabled = false;
                    btnRelease.Enabled = true;
                    btnOwn.OnClientClick = null;
                    btnLink.Enabled = true;
                    btnAssign.Enabled = true;
                    btnPreviousMatch.Enabled = true;
                    btnNextMatch.Enabled = true;

                    btnOwn.CssClass = "enableGrey";
                    btnRelease.CssClass = "disableGrey";
                    btnLink.CssClass = "disableGrey";
                    btnAssign.CssClass = "disableGrey";
                    btnPreviousMatch.CssClass = "disableGrey";
                    btnNextMatch.CssClass = "disableGrey";
                    hdfOwner.Value = "true";
                }
                OwnerShip();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_Page_Load");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }
    protected DataTable ConvertToDatatable(object obj)
    {
        DataTable dtreturn = (DataTable)obj;
        return dtreturn;
    }
    protected void RefreshCommand()
    {
        try
        {
            string json = string.Empty;
            int scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            json = "{\"cmd\":\"REFRESH\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + scrutinizerId + "}";

            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                string message = ResponseMessage(Convert.ToString(data.name));
                lblSockerResponse.Text = null;
                lblSockerResponse.Text = message;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_RefreshCommand");
        }
    }
    public static string ResponseMessage(string ErrorCode)
    {
        string retMessage = string.Empty;
        switch (ErrorCode)
        {
            case "SUCCESS":
                retMessage = "SUCCESS";
                break;
            case "END_OF_WORK":
                retMessage = "There are no other matches available.";
                break;
            case "UNAVAILABLE":
                retMessage = "Server unavailable.";
                break;
            case "NO_COMMAND":
                retMessage = "No command was provided.";
                break;
            case "BAD_COMMAND":
                retMessage = "No such command available.";
                break;
            case "NO_USER":
                retMessage = "No user was provided.";
                break;
            case "UNKNOWN_USER":
                retMessage = "Provide user details.";
                break;
            case "NO_DETAILS":
                retMessage = "No details provided.";
                break;
            case "NOT_ASSIGNED":
                retMessage = "Operation not allowed. Click Own to take the ownership.";
                break;
            case "NOT_OWNER":
                retMessage = "Operation not allowed. Click Own to take the ownership.";
                break;
            case "NOT_VALID":
                retMessage = "Resources not valid.";
                break;
            case "BAD_STATE":
                retMessage = "Resource state is invalid.";
                break;
            case "NO_SCRUTINIZER":
                retMessage = "Resource does not exist.";
                break;
            case "EXCEPTION":
                retMessage = "Unhandled exception encountered.";
                break;
            case "BAD_GRAMMAR":
                retMessage = "Bad JSON syntax.";
                break;
            case "STOLEN":
                retMessage = "Ownership changed.";
                break;
            case "BAD_DETAIL":
                retMessage = "Invalid detail context.";
                break;
            case "TETRIS_FAIL":
                retMessage = "TETRIS Error. Invalid Selection ";
                break;
            case "INCOMPLETE":
                retMessage = "Work unit is incomplete or invalid.";
                break;
        }
        retMessage = $"<b><span class=\"red-italic\">{retMessage}</span></b>";
        return retMessage;
    }
    protected DataTable AddMatched(DataTable dt)
    {
        try
        {
            string caseDId = hdfCaseDetailsID.Value;
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;
            DataTable dtScruClearView = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
                string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerFullView", whereM);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtScruClearView);
            }

            foreach (DataRow dr1 in dtScruClearView.Rows)
            {
                if (caseDId == Convert.ToString(dr1["detailId"]))
                {
                    reportId = Convert.ToString(dr1["reportId"]);
                    birthId = Convert.ToString(dr1["birthId"]);
                    deathId = Convert.ToString(dr1["deathId"]);
                    break;
                }
            }

            //Match Record
            foreach (DataRow dr2 in dtScruClearView.Rows)
            {
                if ((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"])) || (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"])) || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"])))
                {
                    string matchDid = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfCaseDetailsID.Value).Contains(matchDid))
                    {
                        hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + matchDid;
                    }
                }
            }

            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (!string.IsNullOrEmpty(reportId))
                    {
                        dr[i] = "Unmatched";
                    }
                    if (!string.IsNullOrEmpty(reportId) && (dt.Rows[j].ItemArray[i].ToString() == reportId))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                }
            }
            dt.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_AddMatched");
        }
        return dt;
    }
    private List<MatchingViewModel> GetAllDetails(string schema, string tableName, int? scrutinizerId, string query)
    {
        List<GridViewModel> lstFilter = Common.GetFilterList();
        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        if (model != null)
        {
            includeColumns = model.IncludedColumns;
            readonlyColumns = model.ReadonlyColumns;
            dataKeyname = model.DataKeyName;
        }
        DataSet dsRows = new DataSet();
        var lstMatches = new List<MatchingViewModel>();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;

            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dsRows);
            string tableRecName = string.Empty;
            lstMatches = ListRecords(dsRows, true);
        }
        return lstMatches;
    }
    protected void OwnerShip()
    {
        try
        {
            Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string pplTbl = "Users";
                string ScrutinizersTbl = "Scrutinizers";
                string query = "";
                query = string.Format(@"SELECT ppl.firstName, ppl.displayName, scrut.assignedUserId FROM {0}.{1} ppl
                                        INNER JOIN {0}.{2} scrut ON ppl.userId = scrut.assignedUserId
                                        WHERE scrut.scrutinizerId = {3}", pplSchema, pplTbl, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(pplRow);
            }
            string ClaimedRow = " is currently working on this candidate.";
            string unClaimedRow = "";
            string preOwner = " is currently working on this candidate.";
            if (pplRow.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) == Convert.ToString(Session["userid"]))
                {
                    assignedUserName = (pplRow.Tables[0].Rows[0][1].ToString() + Convert.ToString(ClaimedRow));
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(pplRow.Tables[0].Rows[0][2])) && Convert.ToString(pplRow.Tables[0].Rows[0][2]) != Convert.ToString(Session["userid"]))
                {

                    assignedUserName = pplRow.Tables[0].Rows[0][1].ToString() + preOwner;
                }
                else
                {
                    assignedUserName = unClaimedRow;
                }
            }
            else
            {
                assignedUserName = unClaimedRow;
            }

            //assignedUserName = $"<b><span class=\"red-italic\">{assignedUserName}</span></b>";
            lblSockerResponse.Text = null;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_OwnerShip");
        }
    }
    protected string IsOwnerShip()
    {
        string assignedUserID = string.Empty;
        try
        {
            Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string pplTbl = "Users";
                string ScrutinizersTbl = "Scrutinizers";
                string query = "";
                query = string.Format(@"SELECT ppl.firstName, ppl.displayName,scrut.assignedUserId FROM {0}.{1} ppl
                                        INNER JOIN {0}.{2} scrut ON ppl.userId = scrut.assignedUserId
                                        WHERE scrut.scrutinizerId = {3}", pplSchema, pplTbl, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(pplRow);
            }
            if (pplRow.Tables[0].Rows.Count > 0)
            {
                assignedUserID = (pplRow.Tables[0].Rows[0][2].ToString());
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_IsOwnerShip");
        }
        return assignedUserID;
    }
    protected DataTable CheckscrutinizerIdValue()
    {
        DataTable dtSc = new DataTable();
        try
        {

            Int32 scrutinizerId = Convert.ToInt32(Convert.ToString(Request.QueryString["scrutinizerId"]));
            DataSet pplRow = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string pplSchema = "MBDR_System";
                string ScrutinizersTbl = "ScrutinizerFullView";
                string query = "";
                query = string.Format(@"SELECT * FROM {0}.{1} scrut WHERE scrut.scrutinizerId = {2}", pplSchema, ScrutinizersTbl, scrutinizerId);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtSc);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_CheckscrutinizerIdValue");
        }
        return dtSc;
    }
    protected List<MatchingViewModel> ListRecords(DataSet dsRows, bool isDetailId)
    {
        List<MatchingViewModel> lstMatches = new List<MatchingViewModel>();
        string tableName = Convert.ToString(hdfTableName.Value);
        string tableRecName = string.Empty;
        foreach (DataRow dr in dsRows.Tables[0].Rows)
        {
            var item = new MatchingViewModel();

            if (tableName == "DeathRecords")
            {
                tableRecName = "Death Records";
            }
            else if (tableName == "BirthRecords")
            {
                tableRecName = "Birth Records";
            }
            else
            {
                tableRecName = "Case Reports";
            }

            item.RecordFrom = tableRecName;
            item.ScrutinizerID = Convert.ToInt32(dr["scrutinizerId"]);
            item.ReportID = Convert.ToString(dr["reportId"]);
            item.MasterRecordNumber = Convert.ToString(dr["masterRecordNumber"]);
            item.DeathNumber = Convert.ToString(dr["deathNumber"]);
            item.CaseRecID = Convert.ToString(dr["caseRecId"]);
            item.BirthCertNumber = Convert.ToString(dr["birthCertNumber"]);
            item.Confidence = Convert.ToString(dr["confidence"]);
            item.MatchLevel = Convert.ToString(dr["matchLevel"]);
            item.Trips = Convert.ToString(dr["trips"]);
            //item.PhantomHit = Convert.ToString(dr["phantomHit"]);
            item.ChildLastName = Convert.ToString(dr["childLastName"]);
            item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
            item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
            item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
            item.AliasFirstName = Convert.ToString(dr["aliasFirstName"]);
            item.AliasLastName = Convert.ToString(dr["aliasLastName"]);
            item.AliasMiddleName = Convert.ToString(dr["aliasMiddleName"]);
            item.Address1 = Convert.ToString(dr["address1"]);
            item.Address2 = Convert.ToString(dr["address2"]);
            item.City = Convert.ToString(dr["city"]);
            item.County = Convert.ToString(dr["county"]);
            item.State = Convert.ToString(dr["state"]);
            item.Country = Convert.ToString(dr["country"]);
            item.ZipCode = Convert.ToString(dr["zipcode"]);
            item.ChildSSN = Convert.ToString(dr["childSSN"]);
            item.ChildMRN = Convert.ToString(dr["childMRN"]);
            item.Gender = Convert.ToString(dr["gender"]);
            item.Plurality = Convert.ToString(dr["plurality"]);
            item.BirthOrder = Convert.ToString(dr["birthOrder"]);
            item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
            item.BirthWeight = Convert.ToString(dr["birthWeight"]);
            item.ChildMedicaidNumber = Convert.ToString(dr["childMedicaidNumber"]);
            item.BirthDate = Convert.ToString(dr["birthDate"]);
            item.BirthHospital = Convert.ToString(dr["birthHospital"]);
            item.MomSSN = Convert.ToString(dr["momSSN"]);
            item.MomFirstName = Convert.ToString(dr["momFirstName"]);
            item.MomLastName = Convert.ToString(dr["momLastName"]);
            item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
            item.MomSuffix = Convert.ToString(dr["momSuffix"]);
            //item.AdmittingEntity = Convert.ToString(dr["admittingEntity"]);
            item.AdmittingEntityCode = Convert.ToString(dr["admittingEntityCode"]);
            item.PatientType = Convert.ToString(dr["patientType"]);
            item.AdmissionSource = Convert.ToString(dr["admissionSource"]);
            item.AdmissionDate = Convert.ToString(dr["admissionDate"]);
            item.DischargeDate = Convert.ToString(dr["dischargeDate"]);
            item.ICD9SyndromeCode = Convert.ToString(dr["ICD9SyndromeCode"]);
            item.ICD10SyndromeCode = Convert.ToString(dr["ICD10SyndromeCode"]);
            item.CytogeneticsTesting = Convert.ToString(dr["cytogeneticsTesting"]);
            item.ICD9CytogeneticsDiagnosisCodes = Convert.ToString(dr["ICD9CytogeneticsDiagnosisCodes"]);
            item.ICD10CytogeneticsDiagnosisCodes = Convert.ToString(dr["ICD10CytogeneticsDiagnosisCodes"]);
            item.LabCode = Convert.ToString(dr["labCode"]);
            item.HeadCircumference = Convert.ToString(dr["headCircumference"]);
            item.DeliveryLength = Convert.ToString(dr["deliveryLength"]);
            item.InfantPostnatalEcho = Convert.ToString(dr["infantPostnatalEcho"]);
            item.AgeFirstPostnatalEcho = Convert.ToString(dr["ageFirstPostnatalEcho"]);
            item.DateFirstPostnatalEcho = Convert.ToString(dr["dateFirstPostnatalEcho"]);
            item.InfantAdmitted = Convert.ToString(dr["infantAdmitted"]);
            item.AgeInfantAdmitted = Convert.ToString(dr["ageInfantAdmitted"]);
            item.AgeInfantDischarged = Convert.ToString(dr["ageInfantDischarged"]);
            item.AgeInfantAdmittedICU = Convert.ToString(dr["ageInfantAdmittedICU"]);
            item.DateInfantAdmittedICU = Convert.ToString(dr["dateInfantAdmittedICU"]);
            item.ReceivedTimestamp = Convert.ToString(dr["receivedTimestamp"]);
            item.ProcessedTimestamp = Convert.ToString(dr["processedTimestamp"]);
            item.AssimilatedTimestamp = Convert.ToString(dr["assimilatedTimestamp"]);
            item.SearchContext = Convert.ToString(dr["searchContext"]);
            item.MessageState = Convert.ToString(dr["messageState"]);
            item.HistoricId = Convert.ToString(dr["HistoricId"]);

            item.ArchiveID = Convert.ToString(dr["archiveId"]);
            item.IsBirthId = Convert.ToString(dr["birthId"]);
            item.IsDeathId = Convert.ToString(dr["deathId"]);

            if (isDetailId)
            {
                item.DetailID = Convert.ToString(dr["detailId"]);
            }
            lstMatches.Add(item);
        }
        return lstMatches;
    }
    protected DataTable CommonMethod(string query)    //
    {
        try
        {
            string includeColumns = "";
            string readonlyColumns = "";
            string dataKeyname = "";
            int? scrutinizerId = Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value));
            string caseRecId = Convert.ToString(hdfRowID.Value);


            DataSet dsRows = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {

                string whereM = "where scrutinizerId=" + scrutinizerId + "";
                string queryM = string.Format("select top 1 * from {0}.{1} {2}", "MBDR_System", "Matching_Linking", whereM);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dsRows);
                foreach (DataRow dr in dsRows.Tables[0].Rows)
                {
                    hdfArchiveID.Value = Convert.ToString(dr["archiveId"]);
                }
            }

            hdfScrutinizerID.Value = Uri.EscapeDataString(scrutinizerId.ToString());
            List<MatchingViewModel> lstMatches = ListRecords(dsRows, false);

            string where = " where SC.scrutinizerId=" + hdfScrutinizerID.Value + " and caseRecId= " + hdfRowID.Value + "";
            query = string.Format(query + "{0}", where);


            List<MatchingViewModel> lstMatchesRecords = GetAllDetails(hdfSchema.Value, hdfTableName.Value, scrutinizerId, query);
            lstMatches.AddRange(lstMatchesRecords);


            var row = lstMatches.Where(s => s.ScrutinizerID == scrutinizerId).FirstOrDefault();
            PropertyInfo[] properties = typeof(MatchingViewModel).GetProperties();
            dt = new DataTable();
            if (row != null)
            {
                for (int i = 0; i < lstMatches.Count + 1; i++)
                {
                    dt.Columns.Add();
                }
                for (int i = 0; i < properties.Length; i++)
                {
                    DataRow dr = dt.NewRow();
                    var filter = Common.GetFilterList().Where(s => s.Schema == hdfSchema.Value && s.Table.StartsWith(hdfTableName.Value)).FirstOrDefault();
                    if (filter != null)
                    {
                        includeColumns = filter.IncludedColumns;
                        readonlyColumns = filter.ReadonlyColumns;
                        dataKeyname = filter.DataKeyName;
                    }

                    List<string> lstColumns = includeColumns.Split(',').ToList();
                    var colName = lstColumns.FirstOrDefault(s => s.StartsWith(properties[i].Name));
                    string displayColname = string.Empty;
                    List<ColumnList> lstRecord = new List<ColumnList>();
                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnName = colName.Split(':')[1];
                        dr[0] = columnName;
                        displayColname = columnName;
                    }
                    else
                    {
                        displayColname = properties[i].Name;


                        foreach (var col in lstColumns)
                        {
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), displayColname, StringComparison.InvariantCultureIgnoreCase))
                            {
                                dr[0] = columnName.Split(':')[1];
                            }
                        }
                    }
                    if (displayColname != "RecordFrom" && displayColname != "IsMatchRecord")
                    {
                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(properties[i].GetValue(row, null)) });
                    }
                    dr[1] = Convert.ToString(properties[i].GetValue(row, null));
                    for (int j = 0; j < lstMatches.Count; j++)
                    {
                        dr[j + 1] = Convert.ToString(properties[i].GetValue(lstMatches[j], null));
                    }
                    if (dr[0] != null)
                    {
                        dt.Rows.Add(dr);
                    }
                }
                Session["datarecords"] = dt;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_CommonMethod");
        }
        return dt;
    }
    protected void btnPossibleMatch_Click(object sender, EventArgs e)
    {
        try
        {
            string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";            
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string tableName = string.Empty;
            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

            Response.Redirect("MatchingViewDetails.aspx?Schema=MBDR_System&Table=Matching_Linking&refresh=false&keyName=scrutinizerId&scrutinizerId=" + recordId + "&name=Matching%20and%20Linking&returnpage=FullView" + "&caseRecId=" + caseRecId + "&sortExp=" + sortExp + "&where=" + where + "", false);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnAll_Click");
        }
    }
    protected void btnLink_Click(object sender, EventArgs e)
    {
        try
        {
            bool clearView = false;
            bool isMatch = true;
            string query = string.Empty;
            string detailId = string.Empty;
            string reportId = string.Empty;
            string birthId = string.Empty;
            string deathId = string.Empty;
            DataTable dt = new DataTable();
            detailId = hdfDetailsID.Value;  // Request.Form["chkname"];
            DataTable dtScruClearView = new DataTable();
            if (!string.IsNullOrEmpty(detailId) && !detailId.Contains(","))
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
                    string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerFullView", whereM);
                    SqlCommand cmd = new SqlCommand(queryM, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtScruClearView);
                }
                foreach (DataRow dr1 in dtScruClearView.Rows)
                {
                    if (detailId == Convert.ToString(dr1["detailId"]))
                    {
                        reportId = Convert.ToString(dr1["reportId"]);
                        birthId = Convert.ToString(dr1["birthId"]);
                        deathId = Convert.ToString(dr1["deathId"]);
                        break;
                    }
                }
                foreach (DataRow dr2 in dtScruClearView.Rows)
                {
                    if (((!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"])) || (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"])) || (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]))) && (!string.IsNullOrEmpty(detailId) && detailId != Convert.ToString(dr2["detailId"])))
                    {
                        string matchDid = Convert.ToString(dr2["detailId"]);
                        isMatch = false;
                        ClientScript.RegisterStartupScript(this.GetType(), "key", "popupAlert('" + hdfTableName.Value + "');", true);
                        break;
                    }
                }
            }
            
            if (!string.IsNullOrEmpty(detailId) && isMatch)
            {
                string json = string.Empty;
                json = "{\"cmd\":\"SELECT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailId + "],\"clearView\":" + clearView + "}";


                string response = SocketConn(json);
                if (response == "SocketConnectionError")
                {
                    System.Threading.Thread.Sleep(5000);
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                    dt = (DataTable)Session["datarecords"];
                }
                else
                {
                    dynamic data = JObject.Parse(response);
                    query = string.Empty;
                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                    }
                    else
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
                    }

                    if (data.name == "SUCCESS")
                    {
                        dt = new DataTable();
                        dt = CommonMethod(query);

                        int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

                        Response.Redirect("MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=link&scrutinizerId=" + recordId + "", true);
                    }
                    else
                    {
                        string message = ResponseMessage(Convert.ToString(data.name));
                        json = "{\"cmd\":\"VALIDATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailId + "],\"clearView\":" + clearView + "}";
                        response = SocketConn(json);
                        if (response == "SocketConnectionError")
                        {
                            System.Threading.Thread.Sleep(5000);
                            ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                        }
                        else
                        {
                            data = JObject.Parse(response);
                            lblSockerResponse.Text = null;
                            lblSockerResponse.Text = message;
                        }
                    }
                }

            }
            else if (!string.IsNullOrEmpty(detailId) && isMatch == false)
            {
                query = string.Empty;
                if (hdfTableKeyName.Value == "Birth Records")
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                }
                else if (hdfTableKeyName.Value == "Death Records")
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                }
                else
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
                }
                dt = new DataTable();
                dt = CommonMethod(query);
                DataRow dr = dt.NewRow();
                dr[0] = "IsMatchRecord";

                for (int i = 2; i < dt.Columns.Count; i++)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (string.IsNullOrEmpty(hdfDetailsID.Value))
                        {
                            dr[i] = "Click for Linking Process";
                        }
                        else if (string.IsNullOrEmpty(reportId))
                        {
                            dr[i] = "Unmatched";
                            break;
                        }
                        if (!string.IsNullOrEmpty(reportId) && dt.Rows[j].ItemArray[i].ToString() == reportId && detailId == dt.Rows[1].ItemArray[i].ToString())
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId && detailId == dt.Rows[1].ItemArray[i].ToString())
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId && detailId == dt.Rows[1].ItemArray[i].ToString())
                        {
                            dr[i] = "Matched";
                            break;
                        }
                        else
                        {
                            dr[i] = "Unmatched";
                        }
                    }
                }
                dt.Rows.Add(dr);
                lblSockerResponse.Text = null;
                OwnerShip();
            }
            else
            {
                lblSockerResponse.Text = null;
                lblSockerResponse.Text = "Check box not selected.";
            }
            DataTable dt1 = dt;
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnLink_Click");
        }
    }
    protected void btnAssign_Click(object sender, EventArgs e)
    {
        try
        {
            bool clearView = false;
            string query = string.Empty;
            string detailids = string.Empty;
            DataTable dt = new DataTable();
            string scrutinizerId = Uri.EscapeDataString(hdfScrutinizerID.Value);
            string where = " where CV.scrutinizerId = " + scrutinizerId + "";
            query = "SELECT ROW_NUMBER() OVER(ORDER BY CV.detailId) AS Row,* from [MBDR_System].[ScrutinizerFullView] CV ";
            query = string.Format(query + "{0}", where);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    clearView = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (string.IsNullOrEmpty(dr["scrutinizerKind"].ToString()))

                        {
                            detailids = dr["detailId"].ToString();
                        }
                    }
                }
            }
            string json = string.Empty;
            json = "{\"cmd\":\"SELECT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailids + "],\"clearView\":\"" + clearView + "\"}";

            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                query = string.Empty;
                if (hdfTableKeyName.Value == "Birth Records")
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                }
                else if (hdfTableKeyName.Value == "Death Records")
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                }
                else
                {
                    query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
                }

                if (data.name == "SUCCESS")
                {
                    dt = new DataTable();
                    dt = CommonMethod(query);

                    int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

                    Response.Redirect("MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching%20and%20Linking&flag=assign&scrutinizerId=" + recordId + "", true);
                }
                else
                {
                    string message = ResponseMessage(Convert.ToString(data.name));
                    json = "{\"cmd\":\"VALIDATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"did\":[" + detailids + "],\"clearView\":" + clearView + "}";
                    response = SocketConn(json);
                    if (response == "SocketConnectionError")
                    {
                        System.Threading.Thread.Sleep(5000);
                        ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                    }
                    else
                    {
                        data = JObject.Parse(response);
                        lblSockerResponse.Text = null;
                        lblSockerResponse.Text = message;
                    }
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnAssign_Click");
        }
    }
    protected void btnOwn_Click(object sender, EventArgs e)
    {
        try
        {
            string json = "{\"cmd\":\"TAKE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + "}";
            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS" || data.name == "STOLEN")
                {
                    string query = string.Empty;
                    btnOwn.Enabled = false;
                    btnRelease.Enabled = true;
                    btnLink.Enabled = true;
                    btnAssign.Enabled = true;
                    btnPreviousMatch.Enabled = true;
                    btnNextMatch.Enabled = true;

                    btnOwn.CssClass = "enableGrey";
                    btnRelease.CssClass = "disableGrey";
                    btnLink.CssClass = "disableGrey";
                    btnAssign.CssClass = "disableGrey";
                    btnPreviousMatch.CssClass = "disableGrey";
                    btnNextMatch.CssClass = "disableGrey";

                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                    }
                    else
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
                    }
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    OwnerShip();
                    hdfOwner.Value = "true";
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnOwn_Click");
        }
    }   
    protected void btnNextMatch_Click(object sender, EventArgs e)
    {
        try
        {
            int kindValue = 0;
            string query = string.Empty;
            if (hdfTableKeyName.Value == "Birth Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                kindValue = 1;
            }
            else if (hdfTableKeyName.Value == "Death Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                kindValue = 2;
            }
            else
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
            }
            string json = "{\"cmd\":\"NEXT\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + ",\"kind\":" + kindValue + "}";
            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    lblSockerResponse.Text = "Next possible records.";
                }
                else
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    string message = ResponseMessage(Convert.ToString(data.name));
                    lblSockerResponse.Text = message;
                    if (message.Equals("There are no other matches available."))
                    {
                        btnNextMatch.Enabled = false;
                        btnNextMatch.CssClass = "enableGrey";
                    }
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnNextMatch_Click");
        }
    }
    protected void btnPreviousMatch_Click(object sender, EventArgs e)
    {
        try
        {
            int kindValue = 0;
            string query = string.Empty;

            if (hdfTableKeyName.Value == "Birth Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                kindValue = 1;
            }
            else if (hdfTableKeyName.Value == "Death Records")
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                kindValue = 2;
            }
            else
            {
                query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
            }
            string json = "{\"cmd\":\"PREVIOUS\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + ",\"kind\":" + kindValue + "}";
            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    lblSockerResponse.Text = "Previous possible records.";
                }
                else
                {
                    lblSockerResponse.Text = null;
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    string message = ResponseMessage(Convert.ToString(data.name));
                    lblSockerResponse.Text = message;
                    if (message.Equals("There are no other matches available."))
                    {
                        btnPreviousMatch.Enabled = false;
                        btnPreviousMatch.CssClass = "enableGrey";
                    }
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnPreviousMatch_Click");
        }
    }
    public string SocketConn(string JsonRecord)
    {
        string responseFromServer = string.Empty;
        string socketIp = ConfigurationManager.AppSettings["ipAddress"];
        int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
        try
        {
            using (var socket = new TcpClient(socketIp, socketPort))
            {
                string json = JsonRecord;
                byte[] body = Encoding.UTF8.GetBytes(json);
                int bodyLength = Encoding.UTF8.GetByteCount(json);
                var bl = (byte)(bodyLength);
                using (var stream = socket.GetStream())
                {
                    stream.WriteByte(0);
                    stream.WriteByte(bl);
                    stream.Write(body, 0, bodyLength);

                    stream.ReadByte();
                    byte[] buffer = new byte[stream.ReadByte()];
                    stream.Read(buffer, 0, buffer.Length);
                    responseFromServer = System.Text.Encoding.ASCII.GetString(buffer);
                }
            }
        }
        catch (Exception)
        {
            responseFromServer = "SocketConnectionError";
        }
        return responseFromServer;
    }
    protected void btnCaseReports_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedCR.Value = "true";

            if (hdfIsTabClickedBR.Value == "true")
            {
                string btnDR = btnDeathRecords.CssClass;
                if (hdfIsTabClickedDR.Value == "true")
                {
                    hdfIsTabClicked.Value = "true";
                }
                else if (!btnDR.Contains("active"))
                { hdfIsTabClicked.Value = "true"; }
            }
            else
            {
                if (hdfIsTabClickedDR.Value == "true")
                {
                    string btnBR = btnBirthRecords.CssClass;
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    { hdfIsTabClicked.Value = "true"; }
                }
            }


            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");

            string className = btnCaseReports.CssClass;

            btnCaseReports.CssClass = className + " active";
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");

            hdfTableKeyName.Value = "Case Reports";
            IsCaseDetailID.Value = "casereport";
            lblSockerResponse.Text = null;
            hdfTableName.Value = "Matching_Linking";
            hdfSchema.Value = "MBDR_System";
            hdfPageRedirect.Value = "second";
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
            OwnerShip();
            dt = new DataTable();
            dt = CommonMethod(query);

            string caseDId = hdfDetailsID.Value;
            if (caseDId.Contains(",") || string.IsNullOrEmpty(caseDId))
            {
                caseDId = hdfCaseDetailsID.Value;
                hdfDetailsID.Value = caseDId;
            }
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;
            DataTable dtScruClearView = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string whereM = "where ScrutinizerID=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
                string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerFullView", whereM);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtScruClearView);
            }

            foreach (DataRow dr1 in dtScruClearView.Rows)
            {
                if (caseDId == Convert.ToString(dr1["detailId"]))
                {
                    reportId = Convert.ToString(dr1["reportId"]);
                    birthId = Convert.ToString(dr1["birthId"]);
                    deathId = Convert.ToString(dr1["deathId"]);
                    break;
                }
            }

            //Match Record
            foreach (DataRow dr2 in dtScruClearView.Rows)
            {
                if (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                        }
                    }
                }
            }

            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (string.IsNullOrEmpty(hdfDetailsID.Value))
                    {
                        dr[i] = "Check for Linking Process";
                    }
                    else if (string.IsNullOrEmpty(reportId))
                    {
                        dr[i] = "Unmatched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(reportId) && dt.Rows[j].ItemArray[i].ToString() == reportId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId && caseDId == Convert.ToString(dt.Rows[1].ItemArray[i].ToString()))
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else
                    {
                        dr[i] = "Unmatched";
                    }
                }
            }
            DataRow dr4 = dr;

            dt.Rows.Add(dr);


            if (dtScruClearView.Rows.Count == 0)
            {
                dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
            }

            foreach (DataRow drSC in dtScruClearView.Rows)
            {
                if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            //btnBirthRecords.Attributes.Remove("onclick");
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.Enabled = false;
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                            }
                        }
                    }
                }

            }

            if (Session["datarecords"] != null)
            {
                Session["datarecords"] = null;
            }
            Session["datarecords"] = dt;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnCaseReports_Click");
        }
    }
    protected void btnBirthRecords_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedBR.Value = "true";

            if (hdfIsTabClickedDR.Value == "true")
            {
                string btnCR = btnCaseReports.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    hdfIsTabClicked.Value = "true";
                }
                else if (!btnCR.Contains("active"))
                { hdfIsTabClicked.Value = "true"; }
            }
            else
            {
                if (hdfIsTabClickedCR.Value == "true")
                {
                    string btnDR = btnDeathRecords.CssClass;
                    if (hdfIsTabClickedDR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnDR.Contains("active"))
                    { hdfIsTabClicked.Value = "true"; }
                }
            }

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");

            string className = btnBirthRecords.CssClass;
            btnBirthRecords.CssClass = className + " active";
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");

            hdfTableKeyName.Value = "Birth Records";
            IsCaseDetailID.Value = "birthrecord";
            hdfPageRedirect.Value = "second";
            lblSockerResponse.Text = null;
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
            hdfSchema.Value = "Reference";
            hdfTableName.Value = "BirthRecords";
            if (!hdfMultipleTableName.Value.Contains("BirthRecords"))
            {
                hdfMultipleTableName.Value = hdfMultipleTableName.Value + "," + hdfTableName.Value;
            }

            OwnerShip();
            dt = new DataTable();
            dt = CommonMethod(query);

            string caseDId = hdfDetailsID.Value;
            if (caseDId.Contains(",") || string.IsNullOrEmpty(caseDId))
            {
                caseDId = hdfCaseDetailsID.Value;
                hdfDetailsID.Value = caseDId;
            }
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty, birthSId = string.Empty;

            DataTable dtScruClearView = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string whereM = "where ScrutinizerID=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";
                string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerFullView", whereM);
                SqlCommand cmd = new SqlCommand(queryM, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtScruClearView);
            }

            foreach (DataRow dr1 in dtScruClearView.Rows)
            {
                if (caseDId == Convert.ToString(dr1["detailId"]))
                {
                    reportId = Convert.ToString(dr1["reportId"]);
                    birthId = Convert.ToString(dr1["birthId"]);
                    deathId = Convert.ToString(dr1["deathId"]);
                    break;
                }
            }

            //Match Record
            foreach (DataRow dr2 in dtScruClearView.Rows)
            {

                if (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    caseDId = Convert.ToString(dr2["detailId"]);

                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                        {
                            hdfDetailsID.Value = hdfCaseDetailsID.Value + "," + caseDId;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                        }
                    }

                }
                else if (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                        }
                    }
                }
            }
            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (string.IsNullOrEmpty(hdfDetailsID.Value))
                    {
                        dr[i] = "Check for Linking Process";
                    }
                    else if (string.IsNullOrEmpty(birthId))
                    {
                        dr[i] = "Unmatched";
                    }
                    else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(reportId) && dt.Rows[j].ItemArray[i].ToString() == reportId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else
                    {
                        dr[i] = "Unmatched";
                    }
                }
            }
            dt.Rows.Add(dr);

            if (dtScruClearView.Rows.Count == 0)
            {
                dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
            }

            foreach (DataRow drSC in dtScruClearView.Rows)
            {
                if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            btnBirthRecords.Attributes.Remove("onclick");
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.Enabled = false;
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                            }
                        }
                    }
                }
            }

            if (Session["datarecords"] != null)
            {
                Session["datarecords"] = null;
            }
            Session["datarecords"] = dt;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnBirthRecords_Click");
        }
    }
    protected void btnDeathRecords_Click(object sender, EventArgs e)
    {
        try
        {
            hdfIsTabClickedDR.Value = "true";

            if (hdfIsTabClickedBR.Value == "true")
            {
                string btnCR = btnCaseReports.CssClass;
                if (hdfIsTabClickedCR.Value == "true")
                {
                    hdfIsTabClicked.Value = "true";
                }
                else if (!btnCR.Contains("active"))
                { hdfIsTabClicked.Value = "true"; }
            }
            else
            {
                if (hdfIsTabClickedCR.Value == "true")
                {
                    string btnBR = btnBirthRecords.CssClass;
                    if (hdfIsTabClickedBR.Value == "true")
                    {
                        hdfIsTabClicked.Value = "true";
                    }
                    else if (!btnBR.Contains("active"))
                    { hdfIsTabClicked.Value = "true"; }
                }
            }

            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("Green", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("Green", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("Green", "");


            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("activeGreen", "");
            btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");

            string className = btnDeathRecords.CssClass;
            btnDeathRecords.CssClass = className + " active";
            btnCaseReports.CssClass = btnCaseReports.CssClass.Replace("active", "");
            btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
            hdfTableKeyName.Value = "Death Records";
            hdfTableName.Value = "DeathRecords";
            hdfPageRedirect.Value = "second";
            if (!hdfMultipleTableName.Value.Contains("DeathRecords"))
            {
                hdfMultipleTableName.Value = hdfMultipleTableName.Value + "," + hdfTableName.Value;
            }
            IsCaseDetailID.Value = "deathrecord";
            hdfSchema.Value = "Reference";
            lblSockerResponse.Text = null;
            string query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
            OwnerShip();
            dt = new DataTable();
            dt = CommonMethod(query);

            string caseDId = hdfDetailsID.Value;
            if (caseDId.Contains(",") || string.IsNullOrEmpty(caseDId))
            {
                caseDId = hdfCaseDetailsID.Value;
                hdfDetailsID.Value = caseDId;
            }
            string reportId = string.Empty, birthId = string.Empty, deathId = string.Empty, birthDId = string.Empty, deathDId = string.Empty;

            DataTable dtScruClearView = new DataTable();
            if (!string.IsNullOrEmpty(caseDId))
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string whereM = "where scrutinizerId=" + Uri.EscapeDataString(hdfScrutinizerID.Value) + "";

                    string queryM = string.Format("select * from {0}.{1} {2}", "MBDR_System", "ScrutinizerFullView", whereM);
                    SqlCommand cmd = new SqlCommand(queryM, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtScruClearView);
                }
            }

            foreach (DataRow dr1 in dtScruClearView.Rows)
            {
                if (caseDId == Convert.ToString(dr1["detailId"]))
                {
                    reportId = Convert.ToString(dr1["reportId"]);
                    birthId = Convert.ToString(dr1["birthId"]);
                    deathId = Convert.ToString(dr1["deathId"]);
                    break;
                }
            }

            //Match Record
            foreach (DataRow dr2 in dtScruClearView.Rows)
            {
                if (!string.IsNullOrEmpty(reportId) && reportId == Convert.ToString(dr2["reportId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "0")
                {
                    caseDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(caseDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != caseDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + caseDId;
                        }
                    }

                }
                else if (!string.IsNullOrEmpty(birthId) && birthId == Convert.ToString(dr2["birthId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "1")
                {
                    birthDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(birthDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != birthDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + birthDId;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(deathId) && deathId == Convert.ToString(dr2["deathId"]) && Convert.ToString(dr2["scrutinizerKind"]) == "2")
                {
                    deathDId = Convert.ToString(dr2["detailId"]);
                    if (!Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                    }
                    else if (Convert.ToString(hdfDetailsID.Value).Contains(deathDId))
                    {
                        if (Convert.ToString(hdfCaseDetailsID.Value) != deathDId)
                        {
                            hdfDetailsID.Value = hdfDetailsID.Value + "," + deathDId;
                        }
                    }
                }
                else if (string.IsNullOrEmpty(deathId))
                {
                    hdfDetailsID.Value = hdfDetailsID.Value;
                }
            }

            DataRow dr = dt.NewRow();
            dr[0] = "IsMatchRecord";

            for (int i = 2; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {

                    if (string.IsNullOrEmpty(hdfDetailsID.Value))
                    {
                        dr[i] = "Check for Linking Process";
                    }
                    else if (string.IsNullOrEmpty(deathId))
                    {
                        dr[i] = "Unmatched";
                    }
                    else if (!string.IsNullOrEmpty(reportId) && dt.Rows[j].ItemArray[i].ToString() == reportId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(birthId) && dt.Rows[j].ItemArray[i].ToString() == birthId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(deathId) && dt.Rows[j].ItemArray[i].ToString() == deathId)
                    {
                        dr[i] = "Matched";
                        break;
                    }
                    else
                    {
                        dr[i] = "Unmatched";
                    }
                }
            }
            dt.Rows.Add(dr);

            if (dtScruClearView.Rows.Count == 0)
            {
                dtScruClearView = (DataTable)Session["scrutinizerdatarecords"];
            }

            foreach (DataRow drSC in dtScruClearView.Rows)
            {
                if (Convert.ToString(drSC["scrutinizerKind"]) == "0")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            btnBirthRecords.Attributes.Remove("onclick");
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                                btnBirthRecords.Enabled = false;
                            }
                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "1")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "2")
                        {
                            if (hdfTableKeyName.Value != "Death Records")
                            {
                                string className1 = btnDeathRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnDeathRecords.CssClass = className1 + " activeGreen";
                                btnDeathRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnDeathRecords.CssClass;
                            if (!btnDeathRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnDeathRecords.CssClass = className1 + " btn-disabled";
                                btnDeathRecords.Enabled = false;
                            }
                        }
                    }
                }
                else if (Convert.ToString(drSC["scrutinizerKind"]) == "2")
                {
                    foreach (DataRow dr2 in dtScruClearView.Rows)
                    {
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "0")
                        {
                            if (hdfTableKeyName.Value != "Case Reports")
                            {
                                string className1 = btnCaseReports.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnCaseReports.CssClass = className1 + " activeGreen";
                                btnCaseReports.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnCaseReports.CssClass;
                            if (!btnCaseReports.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnCaseReports.CssClass = className1 + " btn-disabled";
                                btnCaseReports.Enabled = false;
                            }

                        }
                        if (Convert.ToString(dr2["scrutinizerKind"]) == "1")
                        {
                            if (hdfTableKeyName.Value != "Birth Records")
                            {
                                string className1 = btnBirthRecords.CssClass;
                                if (className1.Contains("btn-disabled"))
                                {
                                    className1 = className1.Replace("btn-disabled", "");
                                }
                                btnBirthRecords.CssClass = className1 + " activeGreen";
                                btnBirthRecords.Enabled = true;
                                break;
                            }
                        }
                        else
                        {
                            string className1 = btnBirthRecords.CssClass;
                            if (!btnBirthRecords.CssClass.Contains("btn-disabled") && !className1.Contains("active"))
                            {
                                btnBirthRecords.Enabled = false;
                                btnBirthRecords.CssClass = className1 + " btn-disabled";
                            }
                        }
                    }
                }
            }

            if (Session["datarecords"] != null)
            {
                Session["datarecords"] = null;
            }
            Session["datarecords"] = dt;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnDeathRecords_Click");
        }
    }   
    protected void btnServerStatus_Click(object sender, EventArgs e)
    {
        try
        {
            string json = "{\"cmd\":\"PING\"}";
            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Connection Successful", "socketConnSuccess();", true);
                }
            }
            OwnerShip();
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnServerStatus_Click");
        }
    }
    protected void btnRelease_Click(object sender, EventArgs e)
    {
        try
        {
            string json = "{\"cmd\":\"RELEASE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"sid\":" + Convert.ToInt32(Uri.EscapeDataString(hdfScrutinizerID.Value)) + "}";
            string response = SocketConn(json);
            if (response == "SocketConnectionError")
            {
                System.Threading.Thread.Sleep(5000);
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);
                if (data.name == "SUCCESS")
                {
                    string query = string.Empty;
                    btnOwn.Enabled = true;
                    btnRelease.Enabled = false;
                    btnLink.Enabled = false;
                    btnAssign.Enabled = false;
                    btnPreviousMatch.Enabled = false;
                    btnNextMatch.Enabled = false;

                    btnOwn.CssClass = "disableGrey";
                    btnRelease.CssClass = "enableGrey";
                    btnLink.CssClass = "enableGrey";
                    btnAssign.CssClass = "enableGrey";
                    btnPreviousMatch.CssClass = "enableGrey";
                    btnNextMatch.CssClass = "enableGrey";

                    if (hdfTableKeyName.Value == "Birth Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewBirthRecords SC ");
                    }
                    else if (hdfTableKeyName.Value == "Death Records")
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewDeathRecords SC ");
                    }
                    else
                    {
                        query = string.Format("SELECT ROW_NUMBER() OVER(ORDER BY SC.scrutinizerId) AS Row,* from MBDR_System.ScrutinizerFullViewCaseReports SC ");
                    }
                    dt = new DataTable();
                    DataTable dt2 = new DataTable();
                    dt2 = CommonMethod(query);
                    dt = AddMatched(dt2);
                    OwnerShip();
                    hdfOwner.Value = "true";
                }
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnRelease_Click");
        }
    }
    protected void btnLinkCaseReports_Click(object sender, EventArgs e) // this is linked Case Reports from ML Actual Matches
    {
        try
        {
            string lnkCaseReportId = hdflinkCaseReport.Value.ToString();

            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";

            string where = getQueryString();

            string sortExpre = "";

            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "scrutinizerId desc";
            }
            else
            {
                sortExpre = sortExp;
            }

            int recordId = Convert.ToInt32(Uri.EscapeDataString(Convert.ToString(hdfScrutinizerID.Value)));

            Response.Redirect("MatchingLinkingCaseReportsViewLinkDetails.aspx?Schema=MBDR_System&Table=CaseReportsView&keyName=caseRecId&name=Case Report&rowId=" + lnkCaseReportId + "&scrutinizerId=" + recordId + "&renderTable=" + hdfTableName.Value + "&where=" + where + "&sortExp=" + sortExp + "", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingFullViewDetails_btnLinkCaseReports_Click");
        }
    }
    private string getWhereClause()
    {

        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
        string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
        string scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToString(Request.QueryString["scrutinizerId"]) : "";
        string archiveId = !string.IsNullOrEmpty(Request.QueryString["archiveId"]) ? Convert.ToString(Request.QueryString["archiveId"]) : "";
        string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

        string whereSep = "where 1=1 ";

        if (!string.IsNullOrEmpty(fromDate))
        {
            if (!string.IsNullOrEmpty(toDate))
            {
                whereSep += " and loaded_datetime > ='" + fromDate + " 00:00:00'  and loaded_datetime < ='" + toDate + " 23:59:59'";
            }
            else
            {
                whereSep += " and loaded_datetime > ='" + fromDate + " 00:00:00' ";
            }
        }
        if (!string.IsNullOrEmpty(displayName))
        {
            if (displayName == "UnAssignedOwner")
            {
                whereSep += " and displayName Is null";
            }
            else
            {
                whereSep += " and displayName ='" + displayName.Replace("'", "''").Trim() + "'";
            }
        }
        if (!string.IsNullOrEmpty(matchLevel) && matchLevel != "Select Match Level")  // matchLevel = Dropdown condition i.e matchlevel=0,1,2,3
        {
            if (matchLevel == "0")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 0 ";
            }
            else if (matchLevel == "1")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 1 ";
            }
            else if (matchLevel == "2")
            {
                whereSep += " and matchlevel = 0 and scrutinizerKind = 2 ";
            }
            else
            {
                whereSep += " and matchlevel != 0 and scrutinizerKind is not null";
            }
        }

        if (!string.IsNullOrEmpty(scrutinizerId) && isSCIDBlank == "true")
        {
            whereSep += " and scrutinizerId = '" + scrutinizerId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(scrutinizerId))
        {
            whereSep += " and scrutinizerId = '" + scrutinizerId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(archiveId) && isSCIDBlank == "true")
        {
            whereSep += " and archiveId = '" + archiveId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(archiveId))
        {
            whereSep += " and archiveId = '" + archiveId.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childLastName) && !childLastName.Contains("*"))
        {
            whereSep += " and childLastName ='" + childLastName.Replace("'", "''").Trim() + "'"; ;
        }
        else if (childLastName.Contains("*"))
        {
            whereSep += " and childLastName like '" + childLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childFirstName) && !childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName ='" + childFirstName.Replace("'", "''").Trim() + "'";
        }
        else if (childFirstName.Contains("*"))
        {
            whereSep += " and childFirstName like '" + childFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(childMiddleName) && !childMiddleName.Contains("*"))
        {
            whereSep += " and childMiddleName ='" + childMiddleName.Replace("'", "''").Trim() + "'";
        }
        else if (childMiddleName.Contains("*"))
        {
            whereSep += " and childMiddleName like '" + childMiddleName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(birthDate))
        {
            whereSep += " and birthDate > ='" + birthDate + " 00:00:00'  and birthDate < ='" + birthDate + " 23:59:59'";
        }

        if (!string.IsNullOrEmpty(childSSN))
        {
            whereSep += " and childSSN ='" + childSSN.Replace("'", "''").Trim() + "'";
        }


        if (!string.IsNullOrEmpty(momLastName) && !momLastName.Contains("*"))
        {
            whereSep += " and momLastName ='" + momLastName.Replace("'", "''").Trim() + "'";
        }
        else if (momLastName.Contains("*"))
        {
            whereSep += " and momLastName like '" + momLastName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(momFirstName) && !momFirstName.Contains("*"))
        {
            whereSep += " and momFirstName ='" + momFirstName.Trim().Replace("'", "''") + "'";
        }
        else if (momFirstName.Contains("*"))
        {
            whereSep += " and momFirstName like '" + momFirstName.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(address1) && !address1.Contains("*"))
        {
            whereSep += " and address1 = '" + address1.Replace("'", "''").Trim() + "'";
        }
        else if (address1.Contains("*"))
        {
            whereSep += " and address1 like '" + address1.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(city) && !city.Contains("*"))
        {
            whereSep += " and city = '" + city.Replace("'", "''").Trim() + "'";
        }
        else if (city.Contains("*"))
        {
            whereSep += " and city like '" + city.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }
        return whereSep;
    }
    private string getQueryString()
    {
        string archiveId = !string.IsNullOrEmpty(Request.QueryString["archiveId"]) ? Convert.ToString(Request.QueryString["archiveId"]) : "";
        string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
        string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
        string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
        string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
        string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
        string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
        string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
        string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
        string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
        string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
        string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
        string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
        string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
        string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

        string whereSep = "1";
        whereSep += "&isSCIDBlank=" + isSCIDBlank + "";

        if (!string.IsNullOrEmpty(displayName))
        {
            whereSep += "&displayName=" + displayName.Replace("'", "''").Trim() + ""; ;
        }
        if (!string.IsNullOrEmpty(matchLevel))
        {
            whereSep += "&matchLevel=" + matchLevel + ""; ;
        }
        if (!string.IsNullOrEmpty(archiveId))
        {

            whereSep += "&archiveId=" + archiveId.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childLastName))
        {

            whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childFirstName))
        {

            whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(childMiddleName))
        {

            whereSep += "&childMiddleName=" + childMiddleName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(birthDate))
        {

            whereSep += "&birthDate=" + birthDate + "";
        }
        if (!string.IsNullOrEmpty(childSSN))
        {

            whereSep += "&childSSN=" + childSSN.Replace("'", "''").Trim() + "";
        }

        if (!string.IsNullOrEmpty(momLastName))
        {

            whereSep += "&momLastName=" + momLastName.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(momFirstName))
        {

            whereSep += "&momFirstName=" + momFirstName.Trim().Replace("'", "''") + "";
        }

        if (!string.IsNullOrEmpty(address1))
        {

            whereSep += "&address1=" + address1.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(city))
        {

            whereSep += "&city=" + city.Replace("'", "''").Trim() + "";
        }
        if (!string.IsNullOrEmpty(fromDate))
        {
            whereSep += "&fromDate=" + fromDate + "";
        }
        if (!string.IsNullOrEmpty(toDate))
        {
            whereSep += "&toDate=" + toDate + "";
        }

        return whereSep;
    }

}