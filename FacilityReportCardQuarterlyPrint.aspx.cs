﻿using DynamicGridView.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
public partial class FacilityReportCardQuarterlyPrint : System.Web.UI.Page
{
    protected DataTable dtFRCStats;
    protected DataTable dtFRCStatsComment;
    protected string selectedQuarter;
    protected string selectedQuarterText;
    protected string selectedYear;
    protected string selectedOrg;
    protected string selectedFacility;
    protected string selectedFile;
    protected string selectedFacilityText;
    protected string selectedAuditIds;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        if (!IsPostBack)
        {
            ReportBind();
        }
    }

    protected void ReportBind()
    {
        string schema = "MBDR_System";
        selectedOrg = !string.IsNullOrEmpty(Request.QueryString["Org"]) ? Convert.ToString(Request.QueryString["Org"]) : "";
        selectedFacility = !string.IsNullOrEmpty(Request.QueryString["Facility"]) ? Convert.ToString(Request.QueryString["Facility"]) : "";
        selectedFacilityText = !string.IsNullOrEmpty(Request.QueryString["FacilityText"]) ? Convert.ToString(Request.QueryString["FacilityText"]) : "";
        selectedQuarter = !string.IsNullOrEmpty(Request.QueryString["quarter"]) ? Convert.ToString(Request.QueryString["quarter"]) : "";
        selectedYear = !string.IsNullOrEmpty(Request.QueryString["Year"]) ? Convert.ToString(Request.QueryString["Year"]) : "";
        selectedQuarterText = !string.IsNullOrEmpty(Request.QueryString["quarterText"]) ? Convert.ToString(Request.QueryString["quarterText"]) : "";
        selectedAuditIds = !string.IsNullOrEmpty(Request.QueryString["chkIds"]) ? Convert.ToString(Request.QueryString["chkIds"]) : "";

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string query = "";

            string passParam = "";

            if (selectedOrg != "0" && selectedFacility == "All Facilities")
            {
                passParam = "(" + selectedOrg + "," + selectedYear + "," + selectedQuarter + ")";

                query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsQuarterOrg", passParam, " Order by case when month is null then 2 else 1 end, month;");
            }

            else
            {
                passParam = "(" + selectedFacility + "," + selectedYear + "," + selectedQuarter + ")";

                query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsQuarter", passParam, " Order by case when month is null then 2 else 1 end, month;");
            }
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            dtFRCStats = new DataTable();

            da.Fill(dtFRCStats);
        }
    }
}