﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

public partial class _Default : System.Web.UI.Page
{
    private IEnumerable<dynamic> permissions; // Updated declaration
      
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]); 
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);
        string env = ConfigurationManager.AppSettings["environment"];
        string miMiLogin = String.Empty;

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid,browserName,existingbrowserName,browserCount))
        {           
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin="Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        permissions = CheckRole() ?? Enumerable.Empty<dynamic>();

        if(permissions == null || !permissions.Any() || permissions.Any(p => p.RoleId == null))
        {
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Permission Error", "AltarumLogin('User role missing. Please contact your system administrator to verify the details associated with your account.', 'Permission Error');", true);
                    return;
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Permission Error", "SOMLogin('User role missing. Please contact your system administrator to verify the details associated with your account.', 'Permission Error');", true);
                    return;
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }
    }

    protected IEnumerable<dynamic> Permissions { get; set; }

    public IEnumerable<dynamic> CheckRole()
    {
        try
        {
            DataTable dtAccessPermission = Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

            Permissions = dtAccessPermission.AsEnumerable()
                .Where(permission =>
                    (permission.Field<string>("PermissionName") == "RECORDS_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "REPORTS_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "ML_WRITE") || //6p
                    (permission.Field<string>("PermissionName") == "ML_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "ARCHIVE_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "AE_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "REF_WRITE") || // 5p
                    (permission.Field<string>("PermissionName") == "REF_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "USER_WRITE") || // 6p
                    (permission.Field<string>("PermissionName") == "USER_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "REPORT_VIEW") || // 2p
                    (permission.Field<string>("PermissionName") == "CASE_WRITE") || // 8p
                    (permission.Field<string>("PermissionName") == "CASE_VIEW") || // 4p
                    (permission.Field<string>("PermissionName") == "FIXIT_WRITE") || // 5p
                    (permission.Field<string>("PermissionName") == "FIXIT_VIEW") || //2p
                    (permission.Field<string>("PermissionName") == "AUDIT_VIEW_ONLY") || //2p
                    (permission.Field<string>("PermissionName") == "FACILITY_REPORT_CARD")) // 2p
                .Select(permission => new
                {
                    UserId = permission.Field<string>("userId"),
                    RoleId = permission.Field<string>("RoleId"),
                    RoleName = permission.Field<string>("RoleName"),
                    RoleDetails = permission.Field<string>("RoleDetails"),
                    PermissionId = permission.Field<string>("PermissionId"),
                    PermissionName = permission.Field<string>("PermissionName")
                });
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "Default_CheckRole");
        }

        return Permissions;
    }
      
}
