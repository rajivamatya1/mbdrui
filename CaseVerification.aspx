﻿<%@ Page Title="Case Verification" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="CaseVerification.aspx.cs" Inherits="DynamicGridView.CaseVerification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        CASE VERIFICATION</h1>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">
            <a href="/Verification.aspx?schema=MBDR_System&table=getCaseVerificationList" onclick="ShowProgress();">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Verification
                    </div>
                </div>
            </a>
            <div class="program-description">
                Perform case verification or update previously verified cases           
            </div>
        </div>
    </div>

    <div class="col col-lg-4 col-md-6 col-sm-6">
        <div class="program">


            <a href="/VerificationStatistics.aspx?schema=MBDR_System&table=CaseVerificationStats" onclick="ShowProgress();">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        Verification Statistics
                    </div>
                </div>
            </a>
            <div class="program-description">
                Generate case verification statistics
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <script>
        function ShowProgress() {

            var modal = $('<div />');
            modal.addClass("modalspin");
            $('body').append(modal);
            var loading = $(".loadingspin");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ "position": "center", top: top, left: left });

            return true;
        }
    </script>

    <% } %>
</asp:Content>

