﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

public partial class CaseReportsReportDetails : System.Web.UI.Page  // page: CaseRecordsReportDetails.aspx
{
    protected DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }
        try
        {
            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
                string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                string searchText = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string columnName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
                string facility = !string.IsNullOrEmpty(Request.QueryString["facility"]) ? Convert.ToString(Request.QueryString["facility"]) : "";
                string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";
                string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
                
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                hdfSearch.Value = searchText;
                hdfColumnName.Value = columnName;
                hdfName.Value = name;
                hdfKeyName.Value = keyName;
                hdfRowID.Value = rowID;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;
                hdfFromDate.Value = fromDate;
                hdfToDate.Value = toDate;
                hdfChangeFacility.Value = facility;
                hdfCaseID.Value = caseId;
                hdfCaseRecId.Value = caseRecId;
                
                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    BindColumnsDropDown(schema, tableName);
                    BindGrid(tableName, schema, keyName, rowID);
                }
                if (model != null)
                {
                    ltrTableName.Text = "Linked Case Report Details for Case Report " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + rowID + "</span></b>" + " for Case " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + hdfCaseRecId.Value + "</span></b>";
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_Page_Load");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dtDropDown = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtDropDown);
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                }

                foreach (DataColumn item in dtDropDown.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }
                    }
                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_BindColumnsDropDown");
        }
    }

    public void BindGrid(string tableName, string schema, string keyName, string rowID)
    {
        try
        {
            string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            List<ColumnList> lstRecord = new List<ColumnList>();
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
                List<string> lstColumns = includeColumns.Split(',').ToList();
                DataTable dtActualRow = new DataTable();
                DataSet dsRows = new DataSet();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    DataTable dataTable = new DataTable();
                    int row = Convert.ToInt32(rowID);
                    string query = "";
                    query = string.Format("select * from {0}.{1} where caseId={3} order by reportid desc", schema, tableName, row, hdfCaseID.Value); // report ID
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dataTable);

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (hdfRowID.Value == dataTable.Rows[i].ItemArray[0].ToString())
                        {

                            if (i != 0 && !string.IsNullOrEmpty(dataTable.Rows[i - 1].ItemArray[0].ToString()))
                            {
                                hdfPreviousRowID.Value = dataTable.Rows[i - 1].ItemArray[0].ToString();

                                btnPrev.Enabled = true;
                                btnPrev.CssClass = "btn btn-success";
                            }
                            else
                            {
                                btnPrev.Enabled = false;
                                btnPrev.CssClass = "btn btn-disabled";
                            }
                            if (dataTable.Rows.Count > i + 1 && !string.IsNullOrEmpty(dataTable.Rows[i + 1].ItemArray[0].ToString()))
                            {
                                hdfNextRowID.Value = dataTable.Rows[i + 1].ItemArray[0].ToString();
                                btnNext.Enabled = true;
                                btnNext.CssClass = "btn btn-success";
                            }
                            else
                            {
                                btnNext.Enabled = false;
                                btnNext.CssClass = "btn btn-disabled";
                            }
                            break;
                        }
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    int row = Convert.ToInt32(rowID);
                    string query = "";
                    query = string.Format("select * from {0}.{1} where " + keyName + "='{2}' order by reportid desc ", schema, tableName, row);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}' order by reportid desc ", schema, tableName, row + 1);
                    query = query + string.Format("select * from {0}.{1} where " + keyName + "='{2}' order by reportid desc ", schema, tableName, row - 1);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dsRows);
                }

                dtActualRow = dsRows.Tables[0];

                hdfCaseRecId.Value = dtActualRow.Rows[0][1].ToString();
                

                string compareQuery = "";
                DataSet dsTables = new DataSet();
                dt = new DataTable();
                dt.Columns.Add("Columns");
                dt.Columns.Add(model.Table.Split(':')[1]);
                List<string> lstNavigation = new List<string>();
                if (model.ComparisonTables.Count > 0)
                {
                    if (!model.ComparisonTables.Any(s => s.TableName == ""))
                    {
                        foreach (var item in model.ComparisonTables)
                        {
                            if (!string.IsNullOrEmpty(item.TableName))
                            {
                                string[] lstTableNames = item.TableName.Split(':');
                                string key = item.FKey.Split(':')[0];
                                string key1 = item.FKey.Split(':')[1];
                                if (!string.IsNullOrEmpty(lstTableNames[1]))
                                {
                                    lstNavigation.Add(lstTableNames[1]);
                                    dt.Columns.Add(lstTableNames[1]);
                                }
                                else
                                {
                                    lstNavigation.Add(lstTableNames[0]);
                                    dt.Columns.Add(lstTableNames[0]);
                                }
                                compareQuery = compareQuery + " select * from " + item.Schema + "." + lstTableNames[0] + " where " + key + " = '" + dtActualRow.Rows[0][key1] + "' order by reportid desc";
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(compareQuery))
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand(compareQuery, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        for (int i = 0; i < lstNavigation.Count; i++)
                        {
                            if (i == 0)
                            {
                                da.TableMappings.Add("Table", lstNavigation[i]);
                            }
                            else
                            {
                                da.TableMappings.Add("Table" + (i) + "", lstNavigation[i]);
                            }
                        }
                        da.Fill(dsTables);
                    }
                }
                if (dtActualRow.Rows.Count > 0)
                {
                    foreach (var column in lstColumns)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {

                            foreach (DataColumn item in dtActualRow.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = column.Contains("->") ? column.Split('>')[1] : column;

                                DataRow dr = dt.NewRow();

                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                    string displayColname = string.Empty;
                                    if (!string.IsNullOrEmpty(colName))
                                    {
                                        dr[0] = columnInfo;
                                        displayColname = columnInfo;
                                    }
                                    else
                                    {
                                        dr[0] = item.ColumnName;
                                        displayColname = item.ColumnName;
                                    }
                                    if (displayColname != "RecordFrom")
                                        lstRecord.Add(new ColumnList() { ColumnName = displayColname, ColumnValue = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]) });
                                    dr[model.Table.Split(':')[1]] = Convert.ToString(dtActualRow.Rows[0][item.ColumnName]);
                                    for (int j = 0; j < lstNavigation.Count; j++)
                                    {
                                        try
                                        {
                                            string colMapping = model.ComparisonTables.Where(s => s.TableName.EndsWith(lstNavigation[j])).FirstOrDefault().ColumnMapping;
                                            if (colMapping.Contains(item.ColumnName))
                                            {
                                                string strName = Common.GetBetween(colMapping, item.ColumnName, ",");
                                                dr[lstNavigation[j]] = Convert.ToString(dsTables.Tables[lstNavigation[j]].Rows[0][strName.Split(':')[1]]);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dr[lstNavigation[j]] = "";
                                        }

                                    }
                                    if (!string.IsNullOrEmpty(hdfSearchText.Value))
                                    {
                                        if (item.ColumnName == hdfSearchText.Value)
                                        {
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                    else
                                    {
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }
            }
           
            string caseRecId = hdfCaseRecId.Value;
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                btnReset_Click(this, EventArgs.Empty);
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hdfSearchText.Value = Convert.ToString(ddlColumn.SelectedValue);
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnSearch_Click");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ddlColumn.SelectedIndex = 0;
            hdfSearchText.Value = "";
            BindGrid(hdfTableName.Value, hdfSchema.Value, hdfKeyName.Value, hdfRowID.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnReset_Click");
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            int row = Convert.ToInt32(hdfPreviousRowID.Value);
            Response.Redirect("CaseRecordsReportDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row) + "&caseId=" + hdfCaseID.Value + "", true);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnPrev_Click");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            int row = Convert.ToInt32(hdfNextRowID.Value);
            Response.Redirect("CaseRecordsReportDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfKeyName.Value + "&rowId=" + (row) + "&name=" + hdfName.Value + "&caseId=" + hdfCaseID.Value + "", true);
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnNext_Click");
        }
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].BackColor = ColorTranslator.FromHtml("#e3f4ff");
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_gvDynamic_RowDataBound");
        }
    }

    protected void btnCaseRecord_Click(object sender, EventArgs e) // Linked Case Record Pg.4  --->Return to CASE RECORD DETAILS
    {
        try
        {
            string whereSep = filterMethid();
            Response.Redirect("CaseRecordsViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=CaseRecordsView" + "&name=" + hdfName.Value + "&rowId=" + hdfRowID.Value + "&keyName=caseId&where=" + whereSep + "&caseId=" + hdfCaseID.Value+"",false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnCaseRecord_Click");
        }
    }

    public string filterMethid()
    {
        try
        {
            string sortExp = !string.IsNullOrEmpty(Request.QueryString["sortExp"]) ? Convert.ToString(Request.QueryString["sortExp"]) : "";
            string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
            string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
            string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
            string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
            string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
            string zipcode = !string.IsNullOrEmpty(Request.QueryString["zipcode"]) ? Convert.ToString(Request.QueryString["zipcode"]) : "";
            string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
            string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
            string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
            string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
            string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
            string birthRecId = !string.IsNullOrEmpty(Request.QueryString["masterRecordNumber"]) ? Convert.ToString(Request.QueryString["masterRecordNumber"]) : "";
            string deathRecId = !string.IsNullOrEmpty(Request.QueryString["deathNumber"]) ? Convert.ToString(Request.QueryString["deathNumber"]) : "";

            string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
            string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

            string whereSep = "1";
            string sortExpre = "";
            if (string.IsNullOrEmpty(sortExp))
            {
                sortExpre = "caseRecId desc";
            }
            else
            {
                sortExpre = sortExp;
            }
            if (!string.IsNullOrEmpty(caseRecId))
            {
                whereSep += "&caseRecId=" + caseRecId.Trim() + "";
            }
            if (!string.IsNullOrEmpty(birthRecId))
            {
                whereSep += "&masterRecordNumber=" + birthRecId.Trim() + "";
            }
            if (!string.IsNullOrEmpty(deathRecId))
            {
                whereSep += "&deathNumber=" + deathRecId.Trim() + "";
            }
            if (!string.IsNullOrEmpty(childLastName))
            {

                whereSep += "&childLastName=" + childLastName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childFirstName))
            {

                whereSep += "&childFirstName=" + childFirstName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(birthDate))
            {

                whereSep += "&birthDate=" + birthDate + "";
            }
            if (!string.IsNullOrEmpty(address1))
            {

                whereSep += "&address1=" + address1.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(city))
            {

                whereSep += "&city=" + city.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(zipcode))
            {

                whereSep += "&zipcode=" + zipcode.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(childSSN))
            {

                whereSep += "&childSSN=" + childSSN.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(momLastName))
            {

                whereSep += "&momLastName=" + momLastName.Replace("'", "''").Trim() + "";
            }
            if (!string.IsNullOrEmpty(momFirstName))
            {

                whereSep += "&momFirstName=" + momFirstName.Trim().Replace("'", "''") + "";
            }
            if (!string.IsNullOrEmpty(momSSN))
            {

                whereSep += "&momSSN=" + momSSN.Replace("'", "''").Trim() + "";
            }

            if (!string.IsNullOrEmpty(fromDate))
            {
                whereSep += "&fromDate=" + fromDate + "";
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                whereSep += "&toDate=" + toDate + "";
            }

            if (string.IsNullOrEmpty(whereSep))
            {
                return "&sortExp=" + sortExpre + "";
            }
            else
            {
                return whereSep + "&sortExp=" + sortExpre + "";
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_filterMethid");
            return null;
        }
        
    }

    protected void btnLinkedCaseReportsTB_Click(object sender, EventArgs e)  // return to view all linked case reports
    {
        try
        {
            string whereSep = filterMethid();
            Response.Redirect("CaseRecordsViewLinkDetails.aspx?Schema=" + hdfSchema.Value + "&Table=CaseRecordsView" + "&keyName=caseId" + "&rowId=" + hdfRowID.Value + "&caseId=" + hdfCaseID.Value + "&name=" + hdfName.Value + "&where=" + whereSep + "",false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "CaseRecordsReportsDetails_btnLinkedCaseReportsTB_Click");
        }
    }
}