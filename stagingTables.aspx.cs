﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

public partial class stagingTables : System.Web.UI.Page
{
    protected List<BDRTablesList> BDRTables;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        BDRTables = new List<BDRTablesList>
        {
            new BDRTablesList() { Name = "MBDR Format", DBName = "vw_staging_mbdr_format", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Beaumont Hospital", DBName = "vw_staging_beaumont", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Children's Hospital", DBName = "vw_staging_dmc_childrens", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Henry Ford Hospital", DBName = "vw_staging_henryford", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Hurley Hospital", DBName = "vw_staging_hurley", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Lakeland Hospital", DBName = "vw_staging_lakeland", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Marquette Hospital", DBName = "vw_staging_marquette", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Munson Hospital", DBName = "vw_staging_munson", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Sparrow Hospital", DBName = "vw_staging_sparrow", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "Spectrum Hospital", DBName = "vw_staging_spectrum", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "St John Hospital", DBName = "vw_staging_stjohn", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "UM Inpatient Hospital", DBName = "vw_staging_um_inpatient", SCHName = "UI_MBDR_STAGING" },
            new BDRTablesList() { Name = "UM Outpatient Hospital", DBName = "vw_staging_um_outpatient", SCHName = "UI_MBDR_STAGING" },

        };
    }

    public class BDRTablesList
    {
        public string Name { get; set; }
        public string DBName { get; set; }
        public string SCHName { get; set; }
    }
}