﻿using DynamicGridView.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MatchingView : System.Web.UI.Page
{
    protected string getMsgs;
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            txtDateFrom.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateFrom.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            txtDateTo.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateTo.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            txtDateFromDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateFromDOB.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            txtDateToDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
            txtDateToDOB.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

            Session["newSID"] = null;
            Session["paramValue"] = null;
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

            if (!IsPostBack)
            {
                string timeout = !string.IsNullOrEmpty(Request.QueryString["timeout"]) ? Convert.ToString(Request.QueryString["timeout"]) : "";

                if (timeout == "Yes")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "timeout", "noRecFound( 'The timeout period elapsed prior to completion of the operation or the server is not responding.');", true);
                }

                string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
                string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
                string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
                string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : ""; 
                string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";                
                string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
                string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
                string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
                string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
                string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";
                string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : "";
                string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

                string fromDateDOB = !string.IsNullOrEmpty(Request.QueryString["fromDateDOB"]) ? Convert.ToString(Request.QueryString["fromDateDOB"]) : "";
                string toDateDOB = !string.IsNullOrEmpty(Request.QueryString["toDateDOB"]) ? Convert.ToString(Request.QueryString["toDateDOB"]) : "";

                string flagSet = !string.IsNullOrEmpty(Request.QueryString["flag"]) ? Convert.ToString(Request.QueryString["flag"]) : "";
                string scrutinizerId = !string.IsNullOrEmpty(Request.QueryString["scrutinizerId"]) ? Convert.ToString(Request.QueryString["scrutinizerId"]) : "";
                string archiveId = !string.IsNullOrEmpty(Request.QueryString["archiveId"]) ? Convert.ToString(Request.QueryString["archiveId"]) : "";
                string isSCIDBlank = !string.IsNullOrEmpty(Request.QueryString["isSCIDBlank"]) ? Convert.ToString(Request.QueryString["isSCIDBlank"]) : "false";

                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');

                    hdfName.Value = headerLabel;
                }

                if (!string.IsNullOrEmpty(flagSet))
                {
                    if (flagSet == "assign")  // create a new case flagset.
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Successfully assigned.", "noRecFound(' Request to Create successfully submitted.', 'Assign Confirmation');", true);

                    }
                    if (flagSet == "linkerror")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Connection Error.", "noRecFound(' Your request could not be completed. Please verify your connection and then retry the request.', 'Error Message');", true);
                    }
                    if (flagSet == "link")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Successfully linked.", "noRecFound(' Request to Link successfully submitted.', 'Link Confirmation');", true);
                    }
                    if (flagSet == "endOfWork")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "End Of Work", "noRecFound(' This case report has been successfully processed.', 'End Of Work');", true);
                    }
                }
                txtAID.Text = archiveId;
                txtchdLN.Text = childLastName;
                txtchdFN.Text = childFirstName;
                txtChildMiddleName.Text = childMiddleName;
                txtadd1.Text = address1;
                txtChildSSN.Text = childSSN;
                txtcity.Text = city;
                txtMomLN.Text = momLastName;
                txtMomFN.Text = momFirstName;
                txtDateFrom.Text = fromDate;
                txtDateTo.Text = toDate;

                txtDateFromDOB.Text = fromDateDOB;
                txtDateToDOB.Text = toDateDOB;

                if (isSCIDBlank == "true") 
                { 
                    txtSID.Text = scrutinizerId;
                }
                ddlOwnership.DataValueField = displayName;
                ddlMatchLevel.SelectedValue = matchLevel;
                BindColumnsDropDown(schema, tableName);
                if (!string.IsNullOrEmpty(archiveId) || !string.IsNullOrEmpty(childLastName) || !string.IsNullOrEmpty(childFirstName) || !string.IsNullOrEmpty(childMiddleName) || !string.IsNullOrEmpty(address1) || !string.IsNullOrEmpty(birthDate) || !string.IsNullOrEmpty(childSSN) || !string.IsNullOrEmpty(momLastName) || !string.IsNullOrEmpty(momFirstName) || !string.IsNullOrEmpty(momSSN) || !string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate) || !string.IsNullOrEmpty(displayName) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(fromDateDOB) || !string.IsNullOrEmpty(toDateDOB) || !string.IsNullOrEmpty(matchLevel) || (!string.IsNullOrEmpty(scrutinizerId) && isSCIDBlank =="true"))
                {
                    BindGrid(schema, tableName);
                }
                else
                {
                    BindGridEmpty(schema, tableName);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                {
                    BindGridEmpty(schema, tableName);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_Page_Load");
        }
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            DataTable dtOwnerShip = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
            string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string queryList = "";
                queryList = string.Format("select distinct case when displayName is null then 'Unassigned Owner' else displayName End 'displayName' from  MBDR_System.Matching_Linking");
                SqlCommand cmd = new SqlCommand(queryList, conn);
                getMsgs = $"M&LSearch|BindColumnsDropDown|{queryList}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
               
                 da.Fill(dtOwnerShip);
              
                ddlOwnership.DataSource = dtOwnerShip;
                ddlOwnership.DataTextField = "displayName";
                ddlOwnership.DataValueField = "displayName";
                ddlOwnership.DataBind();
                ddlOwnership.Items.Insert(0, "Select Owner");
                if (!string.IsNullOrEmpty(displayName))
                {
                    if( displayName== "UnAssignedOwner")
                    {
                        displayName = "Unassigned Owner";
                    }
                    ListItem selectedItem = ddlOwnership.Items.FindByValue(displayName);
                    if (selectedItem != null)
                    {
                        selectedItem.Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_BindColumnsDropDown");
        }
    }

    public void BindGridEmpty(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                SqlCommand command = new SqlCommand(sqlQuery, conn);
                command.Parameters.AddWithValue("@schema", schema);
                command.Parameters.AddWithValue("@tableName", tableName);
                command.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter daColumns = new SqlDataAdapter(command);
                conn.Close();
                daColumns.Fill(dt);
            }

            gvDynamic.Columns.Clear();
            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            gvDynamic.Columns.Add(commandField);

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            List<string> lstColumns = includeColumns.Split(',').ToList();
            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 13)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string colName = Convert.ToString(dt.Rows[i][0]);
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (colName != "childSuffix" && colName != "aliasLastName" && colName != "aliasFirstName" && colName != "aliasMiddleName"
                                && colName != "plurality" && colName != "childMRN" && colName != "address2" && colName != "birthOrder" && colName != "vitalStatus" && colName != "birthWeight" && colName != "childMedicaidNumber" && colName != "birthHospital" && colName != "county" && colName != "country" && colName != "gender" && colName != "momMiddleName" && colName != "momSuffix" && colName != "admittingEntityCode" && colName != "patientType" && colName != "admissionSource" && colName != "admissionDate" && colName != "dischargeStatus" && colName != "dischargeDate" && colName != "icd9_diag_code" && colName != "icd10_diag_status" && colName != "icd9_proc_code" && colName != "cpt_proc_code" && colName != "icd10_proc_code" && colName != "ICD9SyndromeCode" && colName != "ICD10SyndromeCode" && colName != "cytogeneticsTesting" && colName != "ICD9CytogeneticsDiagnosisCodes" && colName != "ICD10CytogeneticsDiagnosisCodes" && colName != "labCode" && colName != "cytogeneticsLabReportNumber" && colName != "headCircumference" && colName != "deliveryLength" && colName != "infantPostnatalEcho" && colName != "ageFirstPostnatalEcho" && colName != "dateFirstPostnatalEcho" && colName != "infantAdmitted" && colName != "ageInfantAdmitted" && colName != "ageInfantDischarged" && colName != "ageInfantAdmittedICU" && colName != "dateInfantAdmittedICU" && colName != "approved" && colName != "lastUpdated" && colName != "scrutinizerId" && colName != "momSSN")
                            {
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = Convert.ToString(dt.Rows[i][0]);
                                    field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            DataTable dtEmpty = new DataTable();
           
            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.DataSource = dtEmpty;
            gvDynamic.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_BindGridEmpty");
        }
    }

    private string GetWhereClause()
    {
        string where = "where 1=1"; 
        string matchLevel = !string.IsNullOrEmpty(Request.QueryString["matchLevel"]) ? Convert.ToString(Request.QueryString["matchLevel"]) : ""; 
        string displayName = string.Empty;

        if (!string.IsNullOrEmpty(Convert.ToString(ddlOwnership.SelectedValue)) && ddlOwnership.SelectedValue != "Select Owner")
        {
            if (ddlOwnership.SelectedValue == "Unassigned Owner")
            {
                where += " and displayName is NULL";
            }
            else
            {
                where += " and displayName = '" + Convert.ToString(ddlOwnership.SelectedValue) + "'";
            }
        }
        else if (!string.IsNullOrEmpty(displayName))
        {
            if (displayName == "UnAssignedOwner")
            {
                where += " and displayName is NULL";
            }
            else
            {
                where += " and displayName = '" + Convert.ToString(ddlOwnership.SelectedValue) + "'";
            }
        }

        if (!string.IsNullOrEmpty(Convert.ToString(ddlMatchLevel.SelectedValue)) && ddlMatchLevel.SelectedValue != "Select Match Level")
        {
            if (ddlMatchLevel.SelectedValue == "0")   // mathclevel=0 , casereport
            {
                where += " and matchlevel = 0 and scrutinizerKind = 0 ";
            }
            else if (ddlMatchLevel.SelectedValue == "1") // mathclevel=1 , birthRecord
            {
                where += " and matchlevel = 0 and scrutinizerKind = 1 ";
            }
            else if (ddlMatchLevel.SelectedValue == "2") // mathclevel=2 , deathRecord
            {
                where += " and matchlevel = 0 and scrutinizerKind = 2 ";
            }
            else  //  Match Level > to 0
            {
                where += " and matchlevel != 0 and scrutinizerKind is not null";
            }
        }
        else if (!string.IsNullOrEmpty(matchLevel))
        {
            if (matchLevel == "0")
            {
                where += " and matchlevel = 0 and scrutinizerKind = 0 ";
            }
            else if (matchLevel == "1")
            {
                where += " and matchlevel = 0 and scrutinizerKind = 1 ";
            }
            else if (matchLevel == "2")
            {
                where += " and matchlevel = 0 and scrutinizerKind = 2 ";
            }
            else
            {
                where += " and matchlevel != 0 and scrutinizerKind is not null";
            }
        }

        if (!string.IsNullOrEmpty(txtSID.Text))
        {
            where += " and scrutinizerId = '" + txtSID.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtAID.Text))
        {
            where += " and archiveId = '" + txtAID.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtchdLN.Text) && !txtchdLN.Text.Contains("*"))
        {
            where += " and childLastName = '" + txtchdLN.Text.Replace("'", "''").Trim() + "'";
        }
        else if (txtchdLN.Text.Contains("*"))
        {
            where += " and childLastName like '" + txtchdLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtchdFN.Text) && !txtchdFN.Text.Contains("*"))
        {
            where += " and childFirstName = '" + txtchdFN.Text.Replace("'", "''").Trim() + "'";
        }
        else if (txtchdFN.Text.Contains("*"))
        {
            where += " and childFirstName like '" + txtchdFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtChildMiddleName.Text) && !txtChildMiddleName.Text.Contains("*"))
        {
            where += " and childMiddleName = '" + txtChildMiddleName.Text.Replace("'", "''").Trim() + "'";
        }
        else if (txtChildMiddleName.Text.Contains("*"))
        {
            where += " and childMiddleName like '" + txtChildMiddleName.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtadd1.Text) && !txtadd1.Text.Contains("*"))
        {
            where += " and address1 = '" + txtadd1.Text.Replace("'", "''") + "'";
        }
        else if (txtadd1.Text.Contains("*"))
        {
            where += " and address1 like '" + txtadd1.Text.Replace("'", "''").Replace("*", "%") + "'";
        }

        if (!string.IsNullOrEmpty(txtcity.Text) && !txtcity.Text.Contains("*"))
        {
            where += " and city = '" + txtcity.Text.Replace("'", "''").Trim() + "'";
        }
        else if (txtcity.Text.Contains("*"))
        {
            where += " and city like '" + txtcity.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtChildSSN.Text))
        {
            where += " and childSSN ='" + txtChildSSN.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtMomLN.Text) && !txtMomLN.Text.Contains("*"))
        {
            where += " and momLastName = '" + txtMomLN.Text.Replace("'", "''").Trim() + "'";
        }
        else if (txtMomLN.Text.Contains("*"))
        {
            where += " and momLastName like '" + txtMomLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtMomFN.Text) && !txtMomFN.Text.Contains("*"))
        {
            where += " and momFirstName = '" + txtMomFN.Text.Trim().Replace("'", "''") + "'";
        }
        else if (txtMomFN.Text.Contains("*"))
        {
            where += " and momFirstName like '" + txtMomFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
        }

        /*** Loaded datetime - From Date to Date ***/

        if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // both from/to date
        {
            where += " and loaded_datetime > ='" + txtDateFrom.Text + " 00:00:00'  and loaded_datetime < ='" + txtDateTo.Text + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text)) // from date only 
        {
            where += " and loaded_datetime > ='" + txtDateFrom.Text + " 00:00:00' ";
        }
        else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // to date only 
        {
            where += " and loaded_datetime < ='" + txtDateTo.Text + " 23:59:59' ";
        }

        /*** DOB - From Date To Date ***/

        if (string.IsNullOrEmpty(txtDateToDOB.Text) && !string.IsNullOrEmpty(txtDateFromDOB.Text)) // only selection from date
        {
            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
            where += " and birthDate > ='" + txtDateFromDOB.Text + " 00:00:00'  and birthDate < ='" + currentDate + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(txtDateToDOB.Text) && string.IsNullOrEmpty(txtDateFromDOB.Text)) // only selection TO date
        {
            string defaultFromDate = "1/1/1900";
            where += " and birthDate > ='" + defaultFromDate + " 00:00:00'  and birthDate < ='" + txtDateToDOB.Text + " 23:59:59'";
        }
        else if (!string.IsNullOrEmpty(txtDateFromDOB.Text) && !string.IsNullOrEmpty(txtDateToDOB.Text))
        { 
            where += " and birthDate > ='" + txtDateFromDOB.Text + " 00:00:00'  and birthDate < ='" + txtDateToDOB.Text + " 23:59:59'";
        }        

        return where;
    }

    public void BindGrid(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Uri.EscapeDataString(Convert.ToString(item[0])) == "scrutinizerId")
                        {
                            sortExp = " scrutinizerId desc";
                            break;
                        }
                    }
                }
            }

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            hdfDataKeyName.Value = dataKeyname;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                string OrderBySID = sortExp;
                if (string.IsNullOrEmpty(OrderBySID))
                {
                    OrderBySID = "scrutinizerId desc";
                }
                int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                where = GetWhereClause();
                if (!where.Contains("and"))
                {
                    BindGridEmpty(schema, tableName);
                    if ((ddlOwnership.SelectedValue != "Select Owner") || (ddlMatchLevel.SelectedValue != "Select Match Level"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                    }                       
                }
                else
                {
                    query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY scrutinizerId ORDER BY similarSIDs desc) AS CountOfRows,* FROM {0}.{1} {5}) AS RESULT WHERE CountOfRows = 1) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ORDER BY {6}", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    getMsgs = $"M&LSearch|BindGrid|{query}";
                    ErrorLog(getMsgs);
                    cmd.CommandTimeout = 0;
                    conn.Open();                 
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);

                    gvDynamic.Columns.Clear();
                    CommandField commandField = new CommandField();
                    commandField.ButtonType = ButtonType.Link;
                    commandField.HeaderText = "Action";
                    commandField.ShowEditButton = true;
                    gvDynamic.Columns.Add(commandField);

                    List<string> lstColumns = includeColumns.Split(',').ToList();

                    foreach (var col in lstColumns)
                    {
                        if (gvDynamic.Columns.Count < 13)
                        {
                            if (!string.IsNullOrEmpty(col))
                            {
                                foreach (DataColumn item in dt.Columns)
                                {
                                    string colName = item.ColumnName;
                                    string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                    if (colName != "scrutinizerId" && colName != "archiveId" && colName != "caseRecId" && colName != "masterRecordNumber" && colName != "birthCertNumber" && colName != "deathNumber" && colName != "admittingEntityCode" && colName != "admissionSource" && colName != "aliasLastName" && colName != "aliasFirstName" && colName != "aliasMiddleName" && colName != "plurality" && colName != "birthOrder" && colName != "vitalStatus" && colName != "birthWeight" && colName != "childMedicaidNumber" && colName != "birthHospital" && colName != "admittingEntity" && colName != "reportId" && colName != "birthId" && colName != "deathId" && colName != "confidence" && colName != "matchLevel" && colName != "childSuffix" && colName != "phantomHit" & colName != "momMiddleName" & colName != "momSuffix" && colName != "momSSN")
                                    {
                                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                            BoundField field = new BoundField();
                                            field.HeaderText = columnInfo;
                                            field.DataField = item.ColumnName;
                                            field.SortExpression = item.ColumnName;
                                            if (readonlyColumns.Contains(colName))
                                            {
                                                field.ReadOnly = true;
                                            }
                                            gvDynamic.Columns.Add(field);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                    gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                    if (gvDynamic.VirtualItemCount < 1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                    }
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                    gvDynamic.DataSource = dt;
                    gvDynamic.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                btnReset_Click(this, EventArgs.Empty);
            }
        }
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                int pageIndex = 0;
                string where = GetWhereClause();
                query = string.Format("SELECT COUNT(DISTINCT {0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);
                SqlCommand cmd = new SqlCommand(query, conn);
                getMsgs = $"M&LSearch|GetTotalRecords|{query}";
                ErrorLog(getMsgs);
                cmd.CommandTimeout = 0;
                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
           
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_GetTotalRecords");
        }
        return totalRecords;
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;           
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_PageIndexChanging");
        }
    }

    protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvDynamic.EditIndex = -1;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_RowCancelingEdit");
        }
    }

    protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            List<GridViewModel> lstFilter = Common.GetFilterList();
            Response.Redirect("MatchingViewDetails.aspx?entityId=1", true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_ViewRecord");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //try
        //{
        //    gvDynamic.EditIndex = e.NewEditIndex;
        //    BindGrid(hdfSchema.Value, hdfTableName.Value);
        //}
        //catch (Exception ex)
        //{
        //    string path = ConfigurationManager.AppSettings["uiLogPath"];
        //    Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_RowEditing");
        //}
    }

    protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_RowUpdating");
        }
    }

    protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_Sorting");
        }
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC";
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_GetSortDirection");
        }
        return nextDir;
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var birthDateValue = DataBinder.Eval(e.Row.DataItem, "birthDate");

                if (birthDateValue != null && !string.IsNullOrEmpty(birthDateValue.ToString()))
                {
                    DateTime birthDate = Convert.ToDateTime(birthDateValue);
                    e.Row.Cells[5].Text = birthDate.ToString("MM/dd/yyyy");
                }
                else
                {
                    e.Row.Cells[5].Text = string.Empty;
                }

                var row = (DataRowView)e.Row.DataItem;
                int index = -1;
                LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                field.Text = "View";
                field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.OnClientClick = "ShowProgress();";
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_RowDataBound");
        }
    }

    protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                string sortExpre = string.Empty;
                string displayName = string.Empty;

                string where = "1";


                if (!string.IsNullOrEmpty(txtAID.Text))
                {
                    where += "&archiveId=" + txtAID.Text.Trim() + "";
                }

                if (!string.IsNullOrEmpty(txtchdLN.Text))
                {
                    where += "&childLastName=" + txtchdLN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtchdFN.Text))
                {
                    where += "&childFirstName=" + txtchdFN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtChildMiddleName.Text))
                {
                    where += "&childMiddleName=" + txtChildMiddleName.Text.Trim() + "";
                }
                //if (!string.IsNullOrEmpty(txtChildDOB.Text))
                //{
                //    where += "&birthDate=" + txtChildDOB.Text + "";
                //}
                if (!string.IsNullOrEmpty(txtadd1.Text))
                {
                    where += "&address1=" + txtadd1.Text + "";
                }
                if (!string.IsNullOrEmpty(txtcity.Text))
                {
                    where += "&city=" + txtcity.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtChildSSN.Text))
                {
                    where += "&childSSN=" + txtChildSSN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtMomLN.Text))
                {
                    where += "&momLastName=" + txtMomLN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtMomFN.Text))
                {
                    where += "&momFirstName=" + txtMomFN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtDateFrom.Text))
                {
                    where += "&fromDate=" + txtDateFrom.Text + "";
                }
                if (!string.IsNullOrEmpty(txtDateTo.Text))
                {
                    where += "&toDate=" + txtDateTo.Text + "";
                }

                if (!string.IsNullOrEmpty(txtDateFromDOB.Text))
                {
                    where += "&fromDateDOB=" + txtDateFromDOB.Text + "";
                }
                if (!string.IsNullOrEmpty(txtDateToDOB.Text))
                {
                    where += "&toDateDOB=" + txtDateToDOB.Text + "";
                }

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExpre = "scrutinizerId desc";
                }
                else
                {
                    sortExpre = Convert.ToString(ViewState["SortExpression"]);
                }


                if (!string.IsNullOrEmpty(ddlOwnership.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ddlOwnership.SelectedValue)) && ddlOwnership.SelectedValue != "Select Owner")
                    {
                        if (ddlOwnership.SelectedValue == "Unassigned Owner")
                        {
                            where += "&displayName=" + "UnAssignedOwner" + "";
                        }
                        else
                        {
                            where += "&displayName=" + "" + Convert.ToString(ddlOwnership.SelectedValue) + "";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ddlMatchLevel.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ddlMatchLevel.SelectedValue)) && ddlMatchLevel.SelectedValue != "Select Match Level")
                    {
                        where += "&matchLevel=" + "" + Convert.ToString(ddlMatchLevel.SelectedValue) + "";
                    }
                }

                int id = Convert.ToInt32(e.CommandArgument.ToString());

                string isSCIDBlank = !string.IsNullOrEmpty(txtSID.Text) ? "true" : "false";
                where += "&isSCIDBlank="+isSCIDBlank;

                string encodedSID = Uri.EscapeDataString(id.ToString());

                if (Convert.ToInt32(encodedSID) > 0)
                {
                    Response.Redirect("MatchingViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&scrutinizerId=" + encodedSID + "&name=" + hdfName.Value + "&where=" + where + "&sortExp=" + sortExpre + "&refresh=true", false);
                }
                else
                {
                    Response.Redirect("MatchingViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&scrutinizerId=" + encodedSID + "&name=" + hdfName.Value + "&where=" + where + "&sortExp=" + sortExpre + "&refresh=true", true);
                }
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_gvDynamic_RowCommand");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                gvDynamic.PageIndex = 0;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_btnSearch_Click");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["SortExpression"] = "scrutinizerId desc";

            ddlOwnership.SelectedIndex = 0;
            ddlMatchLevel.SelectedIndex = 0;
            gvDynamic.PageIndex = 0;

            Response.Redirect("MatchingView.aspx?schema=MBDR_System&table=Matching_Linking&name=Matching+and+Linking", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_btnReset_Click");
        }
    }

    protected void ddlOwnership_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string value = Convert.ToString(ddlOwnership.SelectedValue);
            string schema = "MBDR_System";
            string tableName = "Matching_Linking";

            ViewState["SortExpression"] = " scrutinizerId desc";
            ViewState["ddlOwnership"] = " value";
            BindGrid(schema, tableName);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "MatchingView_ddlOwnership_SelectedIndexChanged");
        }
    }

    protected void ddlMatchLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string value = Convert.ToString(ddlMatchLevel.SelectedValue);

            string schema = "MBDR_System";
            string tableName = "Matching_Linking";
            ViewState["SortExpression"] = " scrutinizerId desc";
            BindGrid(schema, tableName);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void txtChildDOB_TextChanged(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "validate", "ValidateDate();", true);
        }
    }

    public static void ErrorLog(string values)
    {
        bool shot = true;
        if (shot)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.Snap(values, path, "MatchingView.aspx");
        }
    }

}