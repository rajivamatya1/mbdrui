﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
namespace DynamicGridView
{   
    public partial class FacilityDashboard : System.Web.UI.Page
    {
        protected DataTable dtFRCDashBoard;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }

                if (!IsPostBack)
                {
                    BindColumnsDropDown();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_Page_Load");
            }
        }

        protected void btnFRC_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("FacilityReportCard.aspx?schema=MBDR_System&table=FRCLists", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_btnFRC_Click");
            }
        }

        protected void btnAQR_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("AnnualQualityReport.aspx?schema=MBDR_System&table=AQRLists", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_btnAQR_Click");
            }
        }

        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Clear Screen", "clearBGD();", true);

                string schema = "MBDR_System";
                string tableName = "DashboardSubmittersFacilities";
                string where = string.Empty;

                if (ddlOrg.SelectedIndex != 0)
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string selectedOrg = Convert.ToString(ddlOrg.SelectedValue);

                        if (selectedOrg != "Select Submitter/Organization")
                        {
                            where = " where submitterId = " + selectedOrg + " order by facility asc" + "";
                        }

                        string queryList = string.Format("SELECT distinct facility,entityId FROM {0}.{1} {2}", schema, tableName, where);
                        DataTable dtFacility = new DataTable();
                        SqlCommand cmd = new SqlCommand(queryList, conn);
                        cmd.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dtFacility);
                        ddlFacility.DataSource = dtFacility;
                        ddlFacility.DataTextField = "facility";
                        ddlFacility.DataValueField = "entityId";
                        ddlFacility.DataBind();
                        ddlFacility.Items.Insert(0, "Select Facility");
                        ddlFacility.Items.Insert(1, "All Facilities");

                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_ddlOrg_SelectedIndexChanged");
            }
        }

        protected void ddlFacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Clear Screen", "clearBGD();", true);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
           ClientScript.RegisterStartupScript(this.GetType(), "Clear Screen", "clearBGD();", true);
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Clear Screen", "clearBGD();", true);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                searchreport.Visible = false;
                BindDashBoard();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_btnSubmit_Click");
            }

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ddlOrg.SelectedIndex = 0;
                ddlFacility.SelectedIndex = 0;
                ddlYear.SelectedIndex = 0;
                ddlMonth.SelectedIndex = 0;

                searchreport.Visible = false;
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_btnReset_Click");
            }
        }
        
        public void BindColumnsDropDown()
        {
            try
            {
                string schema = "MBDR_System";
                string tableName = "DashboardSubmittersFacilities";
                /******************** Facility DropDown ***************************/

                DataTable dtOrganization = new DataTable();
                DataTable dtFacility = new DataTable();
                DataTable dtYear = new DataTable();
                DataTable dtMonth = new DataTable();

                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";
                    string orgOrder = " ORDER BY submitter ASC";
                    string facOrder = " ORDER BY facility ASC";
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter da = new SqlDataAdapter();
                   
                    //Organazation 
                    queryList = string.Format("SELECT distinct submitter,submitterId FROM {0}.{1}{2}", schema, tableName, orgOrder);
                    dtOrganization = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtOrganization);
                    ddlOrg.DataSource = dtOrganization;
                    ddlOrg.DataTextField = "submitter";
                    ddlOrg.DataValueField = "submitterId";
                    ddlOrg.DataBind();
                    ddlOrg.Items.Insert(0, "Select Submitter/Organization");

                    //Facility 
                    queryList = string.Format("SELECT distinct facility,entityId FROM {0}.{1}{2}", schema, tableName, facOrder);
                    dtFacility = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtFacility);
                    ddlFacility.DataSource = dtFacility;
                    ddlFacility.DataTextField = "facility";
                    ddlFacility.DataValueField = "entityId";
                    ddlFacility.DataBind();
                    ddlFacility.Items.Insert(0, "Select Facility");
                    ddlFacility.Items.Insert(1, "All Facilities");

                    //Year 
                    dtYear = new DataTable();
                    dtYear.Columns.Add("year");
                    var currentYear = DateTime.Now.Year;  
                    var startYear = 2023;
                    for (int i = 0; i <= (currentYear - startYear); i++)
                    {
                        if (i == 0)
                        {
                            dtYear.Rows.Add(startYear);
                        }
                        else
                        {
                            dtYear.Rows.Add(startYear + i);
                        }
                    }
                    ddlYear.DataSource = dtYear;
                    ddlYear.DataTextField = "year";
                    ddlYear.DataValueField = "year";
                    ddlYear.DataBind();
                    ddlYear.Items.Insert(0, "Select Year");

                    //Month
                    dtMonth = new DataTable();
                    dtMonth.Columns.Add("month");
                    for (int i = 1; i <=12; i ++)
                    {
                        dtMonth.Rows.Add(i);
                    }
                    ddlMonth.DataSource = dtMonth;
                    ddlMonth.DataTextField = "month";
                    ddlMonth.DataValueField = "month";
                    ddlMonth.DataBind();
                    ddlMonth.Items.Insert(0, "Select Month");
                    ddlMonth.Items.Insert(1, "Full Year");
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_BindColumnsDropDown");
            }
        }

        public void BindDashBoard()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = string.Empty;
                    string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "MBDR_System";
                    string passParam = string.Empty;
                    string checkMonth = string.Empty;

                    if (ddlMonth.SelectedValue == "Full Year")
                    {
                        checkMonth = "NULL";
                    }
                    else
                    {
                        checkMonth = ddlMonth.SelectedValue;
                    }

                    if (ddlOrg.SelectedIndex != 0 && ddlFacility.SelectedIndex == 1)
                    {
                        /* Organization-level Function MBDR_System.getFacilityDashboardStatsOrg(@submitterId, @year, @month) */

                        passParam = "(" + ddlOrg.SelectedValue + "," + ddlYear.SelectedValue + "," + checkMonth + ")";
                        query = string.Format("SELECT * FROM {0}.{1} {2} ", schema, "getFacilityDashboardStatsOrg", passParam);                        
                    }
                    else
                    {
                        /* Facility-level Function MBDR_System.getFacilityDashboardStats(@entityId, @year, @month) */

                        passParam = "(" + ddlFacility.SelectedValue + "," + ddlYear.SelectedValue + "," + checkMonth + ")";
                        query = string.Format("SELECT * FROM {0}.{1} {2} ", schema, "getFacilityDashboardStats", passParam);                       
                    }

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();

                    dtFRCDashBoard = new DataTable();

                    da.Fill(dtFRCDashBoard);

                    if (dtFRCDashBoard.Rows.Count == 0 || dtFRCDashBoard == null)
                    {
                        Session["ErrorOccurred"] = true;

                        ClientScript.RegisterStartupScript(this.GetType(), "Empty Result", "cAlertMsgs('Your request did not return any results. Please change your selection and try again.', 'No Result');", true);
                        return;
                    }
                    searchreport.Visible = true;                   
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityDashboard_BindDashBoard");
            }
        }

    }
}