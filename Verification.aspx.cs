﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Metadata;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace DynamicGridView
{
    public partial class Verification : System.Web.UI.Page
    {
        dynamic permissions = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                txtAdminDate.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtAdminDate.Attributes["min"] = new DateTime(2018, 1, 1).ToString("yyyy-MM-dd");

                txtDisDate.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDisDate.Attributes["min"] = new DateTime(2018, 1, 1).ToString("yyyy-MM-dd");

                txtChildDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtChildDOB.Attributes["min"] = new DateTime(2018, 1, 1).ToString("yyyy-MM-dd");

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }

                permissions = CheckRole();

                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

                if (!IsPostBack)
                {

                    string timeout = !string.IsNullOrEmpty(Request.QueryString["timeout"]) ? Convert.ToString(Request.QueryString["timeout"]) : "";

                    if (timeout == "Yes")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "timeout", "noRecFound( 'The timeout period elapsed prior to completion of the operation or the server is not responding.');", true);
                    }

                    string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";
                    string childMiddleName = !string.IsNullOrEmpty(Request.QueryString["childMiddleName"]) ? Convert.ToString(Request.QueryString["childMiddleName"]) : "";
                    string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
                    string childMRN = !string.IsNullOrEmpty(Request.QueryString["childMRN"]) ? Convert.ToString(Request.QueryString["childMRN"]) : "";
                    string admissionDate = !string.IsNullOrEmpty(Request.QueryString["admissionDate"]) ? Convert.ToString(Request.QueryString["admissionDate"]) : "";
                    string dischargeDate = !string.IsNullOrEmpty(Request.QueryString["dischargeDate"]) ? Convert.ToString(Request.QueryString["dischargeDate"]) : "";
                    string chkDiagCode = !string.IsNullOrEmpty(Request.QueryString["chkDiagCodes"]) ? Convert.ToString(Request.QueryString["chkDiagCodes"]) : "";

                    string reportStatus = !string.IsNullOrEmpty(Request.QueryString["reportStatus"]) ? Convert.ToString(Request.QueryString["reportStatus"]) : "";
                    string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                    if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                    {
                        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                        List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                        string[] header = model.Table.Split(':');
                        hdfName.Value = headerLabel;
                        lblSchema.Text = schema;
                        lblTable.Text = tableName;

                        txtChildDOB.Text = birthDate;
                        txtChildMRN.Text = childMRN;
                        txtAdminDate.Text = admissionDate;
                        txtDisDate.Text = dischargeDate;

                        ddlVeriStatus.SelectedValue = reportStatus;
                        if (!string.IsNullOrEmpty(admittingEntity))
                        {
                            ddlFacility.SelectedValue = admittingEntity;
                        }

                        if (!string.IsNullOrEmpty(chkDiagCode))
                        {
                            chkDiagCodes.Checked = true;
                        }

                        BindColumnsDropDown(schema, tableName);

                        if (!string.IsNullOrEmpty(reportStatus) || !string.IsNullOrEmpty(admittingEntity) || !string.IsNullOrEmpty(birthDate) || !string.IsNullOrEmpty(childMRN) || !string.IsNullOrEmpty(admissionDate) || !string.IsNullOrEmpty(dischargeDate) || !string.IsNullOrEmpty(reportStatus) || !string.IsNullOrEmpty(admittingEntity))
                        {                            
                            admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                            string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);


                            BindGrid(schema, tableName, admittingEntity);
                        }
                        else
                        {
                            BindGridEmpty(schema, tableName);
                        }

                    }
                    else
                    {
                        BindGridEmpty(schema, tableName);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                    {
                        BindGridEmpty(schema, tableName);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_Page_Load");
            }
        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "CASE_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_CheckRole");
            }
            return permissions;
        }

        public void BindGridEmpty(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    daColumns.Fill(dt);

                }

                gvDynamic.Columns.Clear();
                CommandField commandField = new CommandField();
                commandField.ButtonType = ButtonType.Link;
                commandField.HeaderText = "Action";
                gvDynamic.Columns.Add(commandField);

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                List<string> lstColumns = includeColumns.Split(',').ToList();
                foreach (var col in lstColumns)
                {
                    if (gvDynamic.Columns.Count < 9)
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string colName = Convert.ToString(dt.Rows[i][0]);
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                if (colName != "caseRecId" && colName != "facilityCode")
                                {
                                    if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                        BoundField field = new BoundField();
                                        field.HeaderText = columnInfo;
                                        field.DataField = Convert.ToString(dt.Rows[i][0]);
                                        field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                        if (readonlyColumns.Contains(colName))
                                        {
                                            field.ReadOnly = true;
                                        }
                                        gvDynamic.Columns.Add(field);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                DataTable dtEmpty = new DataTable();
                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                gvDynamic.DataSource = dtEmpty;
                gvDynamic.DataBind();

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_BindGridEmpty");
            }
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            try
            {
                string reportStatus = !string.IsNullOrEmpty(Request.QueryString["reportStatus"]) ? Convert.ToString(Request.QueryString["reportStatus"]) : "";

                reportStatus = ddlVeriStatus.SelectedValue;

                reportStatus = "Incomplete";

                if (reportStatus != "Incomplete")
                {
                    reportStatus = "Complete";
                }

                /******************** Facility DropDown ***************************/

                DataTable dt = new DataTable();
                DataTable dtEntityList = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";

                    queryList = "SELECT DISTINCT admittingEntity, details FROM MBDR_System.CaseVerificationFacilities";

                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtEntityList);
                    ddlFacility.DataSource = dtEntityList;
                    ddlFacility.DataTextField = "details";
                    ddlFacility.DataValueField = "admittingEntity";
                    ddlFacility.DataBind();
                    ddlFacility.Items.Insert(0, "Select Facility");
                    if (!string.IsNullOrEmpty(admittingEntity))
                    {
                        ListItem selectedItem = ddlFacility.Items.FindByValue(admittingEntity);
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_BindColumnsDropDown");
            }
        }

        public void BindGrid(string schema, string tableName, string entityId)
        {
            try
            {
                DataTable dt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "childLastName")
                            {
                                sortExp = "childLastName asc, childMRN asc, admissionDate asc";
                                break;
                            }
                        }
                    }
                }

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();

                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";                    
                    string OrderByCRec = sortExp;


                    if (string.IsNullOrEmpty(entityId))
                    {
                        string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                        entityId = selectedadmittingEntity;
                    }

                    if (string.IsNullOrEmpty(OrderByCRec))
                    {
                        OrderByCRec = "childLastName asc, childMRN asc, admissionDate asc";
                    }
                    int pageIndex = 0;

                    where = GetWhereClause();

                    if (!where.Contains("and"))
                    {
                        BindGridEmpty(schema, tableName);
                        if (ddlFacility.SelectedValue != "Select Facility")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                        }
                    }
                    else
                    {
                        query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {6} ) AS Row,* from {0}.{1}({7}){5}) as result where Row between ({3}) and ({4}) ", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderByCRec, entityId);

                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dt);

                        gvDynamic.Columns.Clear();
                        CommandField commandField = new CommandField();
                        commandField.ButtonType = ButtonType.Link;
                        commandField.HeaderText = "Action";
                        commandField.ShowEditButton = true;
                        gvDynamic.Columns.Add(commandField);

                        List<string> lstColumns = includeColumns.Split(',').ToList();

                        foreach (var col in lstColumns)
                        {
                            if (gvDynamic.Columns.Count < 9)
                            {
                                if (!string.IsNullOrEmpty(col))
                                {
                                    foreach (DataColumn item in dt.Columns)
                                    {
                                        string colName = item.ColumnName;
                                        string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                        if (colName != "caseRecId" && colName != "facilityCode")
                                        {
                                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                                BoundField field = new BoundField();
                                                field.HeaderText = columnInfo;
                                                field.DataField = item.ColumnName;
                                                field.SortExpression = item.ColumnName;
                                                if (readonlyColumns.Contains(colName))
                                                {
                                                    field.ReadOnly = true;
                                                }
                                                gvDynamic.Columns.Add(field);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                        gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                        if (gvDynamic.VirtualItemCount < 1)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner1", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No Results Found.');", true);
                        }
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner3", "removeProgress();", true);
                        gvDynamic.DataSource = dt;
                        gvDynamic.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        private string GetWhereClause()
        {
            string where = " where 1 = 1";

            try
            {
                //string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";
                string reportStatus = !string.IsNullOrEmpty(Request.QueryString["reportStatus"]) ? Convert.ToString(Request.QueryString["reportStatus"]) : "";

                //string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                if (chkDiagCodes.Checked)
                {
                    where += " and isCHD = 'Yes'";
                } 
                
                //if (admittingEntity != "Select Facility")
                //{
                //    where += " and admittingEntity = " + selectedadmittingEntity + "";
                //}

                reportStatus = Convert.ToString(ddlVeriStatus.SelectedValue);
                if (reportStatus != "")
                {
                    where += " and reportStatus = '" + reportStatus + "'";
                }


                if (!string.IsNullOrEmpty(txtChildMRN.Text))
                {
                    where += " and childMRN = '" + txtChildMRN.Text.Replace("'", "''").Trim() + "'";
                }

                if (!string.IsNullOrEmpty(txtChildDOB.Text))
                {
                    where += " and birthDate > = '" + txtChildDOB.Text + " 00:00:00'  and birthDate < = '" + txtChildDOB.Text + " 23:59:59'";
                }

                if (!string.IsNullOrEmpty(txtAdminDate.Text) && string.IsNullOrEmpty(txtDisDate.Text))
                {
                    where += " and admissionDate > = '" + txtAdminDate.Text + " 00:00:00'  and admissionDate < = '" + txtAdminDate.Text + " 23:59:59'";
                }

                if (!string.IsNullOrEmpty(txtDisDate.Text) && string.IsNullOrEmpty(txtAdminDate.Text))
                {
                    where += " and dischargeDate > = '" + txtDisDate.Text + " 00:00:00'  and dischargeDate < = '" + txtDisDate.Text + " 23:59:59'";
                }

                if (!string.IsNullOrEmpty(txtAdminDate.Text) && !string.IsNullOrEmpty(txtDisDate.Text))
                {
                    where += " and admissionDate > = '" + txtAdminDate.Text + " 00:00:00'  and dischargeDate < = '" + txtDisDate.Text + " 23:59:59'";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_GetWhereClause");
            }

            return where;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where 1=1 ";
                    int pageIndex = 0;

                    where = GetWhereClause();

                    string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                    string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                    string entityId = string.Empty;

                    if (admittingEntity != "Select Facility")
                    {
                        entityId = selectedadmittingEntity;
                    }

                    query = string.Format("select count(*) from {1}.{2}({4}) {3} ", datakeyName, schema, tableName, where, entityId);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_GetTotalRecords");
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                gvDynamic.PageIndex = e.NewPageIndex;
                BindGrid(lblSchema.Text, lblTable.Text, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_PageIndexChanging");
            }
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                gvDynamic.EditIndex = -1;
                BindGrid(lblSchema.Text, lblTable.Text, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();

                string dataKeyname = string.Empty;
                if (model != null)
                {
                    dataKeyname = model.DataKeyName;
                }
                Response.Redirect("VerificationDetails.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value, true);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_ViewRecord");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(lblSchema.Text, lblTable.Text, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_RowEditing");
            }
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                    GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;

                    if (model != null)
                    {
                        readonlyColumns = model.ReadonlyColumns;
                        dataKeyname = model.DataKeyName;
                    }
                    string query = " set ";
                    foreach (DataControlFieldCell cell in row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            string colName = ((BoundField)cell.ContainingField).DataField;
                            if (!readonlyColumns.Contains(colName) && colName != "id")
                            {
                                query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                            }
                        }
                    }
                    gvDynamic.EditIndex = -1;
                    conn.Open();
                    string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                    string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                    SqlCommand cmd = new SqlCommand(finalQuery, conn);
                    cmd.CommandTimeout = 0;

                    cmd.ExecuteNonQuery();
                    conn.Close();

                    string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                    string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                    string entityId = string.Empty;


                    BindGrid(lblSchema.Text, lblTable.Text, entityId);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_RowUpdating");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(lblSchema.Text, lblTable.Text, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_Sorting");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_GetSortDirection");
            }
            return nextDir;
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var birthDateValue = DataBinder.Eval(e.Row.DataItem, "birthDate");

                    if (birthDateValue != null && !string.IsNullOrEmpty(birthDateValue.ToString()))
                    {
                        DateTime birthDate = Convert.ToDateTime(birthDateValue);
                        e.Row.Cells[4].Text = birthDate.ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        e.Row.Cells[4].Text = string.Empty;
                    }

                    var row = (DataRowView)e.Row.DataItem;
                    int index = -1;
                    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                    field.Text = "View";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.OnClientClick = "ShowProgress();";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_RowDataBound");
            }
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string where = "1";

                    if (!string.IsNullOrEmpty(txtChildMRN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&childMRN=" + txtChildMRN.Text.Trim() + "";
                    }


                    if (!string.IsNullOrEmpty(txtChildDOB.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&birthDate=" + txtChildDOB.Text + "";
                    }

                    if (!string.IsNullOrEmpty(txtAdminDate.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&admissionDate=" + txtAdminDate.Text + "";
                    }


                    if (!string.IsNullOrEmpty(txtDisDate.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&dischargeDate=" + txtDisDate.Text + "";
                    }

                    if (chkDiagCodes.Checked)
                    {
                        where += "&chkDiagCodes=yes";
                    }

                    if (ddlFacility.SelectedValue != "Select Facility")
                    {
                        where += "&admittingEntity=" + ddlFacility.SelectedValue + "";
                    }


                    if (ddlVeriStatus.SelectedValue != "")
                    {
                        where += "&reportStatus=" + ddlVeriStatus.SelectedValue + "";
                    }

                    string sortExpre = string.Empty;

                    if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        sortExpre = "childLastName asc, childMRN asc, admissionDate asc";
                    }
                    else
                    {
                        sortExpre = Convert.ToString(ViewState["SortExpression"]);
                    }

                    int id = Convert.ToInt32(e.CommandArgument.ToString());

                    Response.Redirect("VerificationDetails.aspx?reportId=" + id + "&where=" + where + "&sortExp=" + sortExpre + "", true);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_gvDynamic_RowCommand");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {                
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                if (admittingEntity != "Select Facility")
                {
                    entityId = selectedadmittingEntity;
                }

                string schema = "MBDR_System";
                string tableName = "getCaseVerificationList";
                gvDynamic.PageIndex = 0;
                BindGrid(schema, tableName, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_btnSearch_Click");
            }
        }

        protected void ddlVeriStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                if (admittingEntity != "Select Facility")
                {
                    entityId = selectedadmittingEntity;
                }

                string value = Convert.ToString(ddlVeriStatus.SelectedValue);
                string schema = "MBDR_System";
                string tableName = "getCaseVerificationList";
                gvDynamic.PageIndex = 0;
                BindGrid(schema, tableName, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_ddlVeriStatus_SelectedIndexChanged");
            }
        }

        protected void ddlFacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                string entityId = string.Empty;

                if (admittingEntity != "Select Facility")
                {
                    entityId = selectedadmittingEntity;
                }

                string value = Convert.ToString(ddlFacility.SelectedValue);
                string schema = "MBDR_System";
                string tableName = "getCaseVerificationList";
                gvDynamic.PageIndex = 0;
                BindGrid(schema, tableName, entityId);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Verification_ddlFacility_SelectedIndexChanged");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SortExpression"] = "childLastName asc, childMRN asc, admissionDate asc";

                ddlFacility.SelectedIndex = 0;
                ddlVeriStatus.SelectedIndex = 0;
                gvDynamic.PageIndex = 0;

                Response.Redirect("Verification.aspx?schema=MBDR_System&table=getCaseVerificationList", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "MatchingView_btnReset_Click");
            }
        }

        protected void chkDiagCodes_CheckedChanged(object sender, EventArgs e)
        {
            string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

            string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

            string entityId = string.Empty;

            if (admittingEntity != "Select Facility")
            {
                entityId = selectedadmittingEntity;
            }

            string value = Convert.ToString(ddlFacility.SelectedValue);
            string schema = "MBDR_System";
            string tableName = "getCaseVerificationList";

            if (value != "Select Facility")
            {
                BindGrid(schema, tableName, entityId);
            }

        }

    }
}