﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class Audit : System.Web.UI.Page
    {
        dynamic permissions = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();

            try
            {
                txtDateFrom.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateFrom.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                txtDateTo.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateTo.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                if (!IsPostBack)
                {
                    BindUMDropDown();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_Page_Load");
            }
        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "AUDIT_VIEW_ONLY")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_CheckRole");
            }
            return permissions;
        }

        /************ Action Buttons Start ****************/

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string schema = string.Empty;
                string tableName = string.Empty;

                if (ddlComp.SelectedValue != "0" && ddlComp.SelectedValue != "")
                {
                    ViewState["SortExpression"] = string.Empty;

                    if (ddlComp.SelectedValue == "userManage")
                    {
                        gvDynamic.PageIndex = 0;
                        gvDynamic.DataSource = null;
                        gvDynamic.DataBind();

                        schema = "MBDR_System";
                        tableName = "UsersHistory";
                        hdfSchema.Value = schema;
                        hdfTableName.Value = tableName;

                        BindGridUserManagement(hdfSchema.Value, hdfTableName.Value);
                    }
                    else if (ddlComp.SelectedValue == "caseVerify")
                    {
                        gvDynamicCaseVerification.PageIndex = 0;
                        gvDynamicCaseVerification.DataSource = null;
                        gvDynamicCaseVerification.DataBind();

                        schema = "MBDR_System";
                        tableName = "CaseVerificationAudit";
                        hdfSchema.Value = schema;
                        hdfTableName.Value = tableName;

                        BindGridCaseVerification(hdfSchema.Value, hdfTableName.Value);
                    }

                    /*** Future development ***/

                    //else if (ddlComp.SelectedValue == "MatchLink")
                    //{
                    //    schema = "MBDR_System";
                    //    tableName = "MatchingLinking";
                    //}
                    //else if (ddlComp.SelectedValue == "FixIt")
                    //{
                    //    schema = "MBDR_System";
                    //    tableName = "FixItTools";
                    //}
                    //else if (ddlComp.SelectedValue == "RefTable")
                    //{
                    //    schema = "MBDR_System";
                    //    tableName = "ReferenceTables";
                    //}
                }



                if (Convert.ToBoolean(ViewState["NoResult"]) == false)
                {
                    btnExport.Enabled = true;
                    btnExport.CssClass = "btn btn-danger blue-color";
                }
                else
                {
                    searchreport.Visible = false;
                    btnExport.Enabled = false;
                    btnExport.CssClass = "btn btn-danger grey-color";
                    ViewState["NoResult"] = false;
                }

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_btnSearch_Click");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                searchreport.Visible = false;
                Response.Redirect("Audit.aspx", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_btnReset_Click");
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string Component = ddlComp.SelectedValue;
                string PerformedBy = ddlPerform.SelectedValue;
                string displayName = ddlDisplay.SelectedValue;
                string fromDate = txtDateFrom.Text.Trim();
                string toDate = txtDateTo.Text.Trim();

                Response.Redirect("DownloadFile.aspx?component=" + Component + "&performedby=" + PerformedBy + "&displayname=" + displayName + "&fromDate=" + fromDate + "&toDate=" + toDate + "", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_btnExport_Click");
            }
        }

        /************ Action Buttons End ****************/

        /************ Common Methods Start ****************/              

        protected void clearScreen()
        {
            searchreport.Visible = false;
            btnExport.Enabled = false;
            btnExport.CssClass = "btn btn-danger grey-color";
        }

        private string GetWhereClause()
        {
            string where = string.Empty;

            if (ddlComp.SelectedValue == "userManage")
            {
                if (ddlPerform.SelectedValue != "0" && ddlPerform.SelectedValue != "")
                {
                    if (ddlPerform.SelectedValue != "Select Performer")
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where += " and modifier = '" + ddlPerform.SelectedValue + "'";
                        }
                        else
                        {
                            ViewState["AllUsers"] = false;
                            where += " where modifier = '" + ddlPerform.SelectedValue + "'";
                        }
                    }
                }

                if (ddlDisplay.SelectedValue != "0" && ddlDisplay.SelectedValue != "")
                {
                    if (ddlDisplay.SelectedValue != "All Users")
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where += " and displayName = '" + ddlDisplay.SelectedValue + "'";
                        }
                        else
                        {
                            ViewState["AllUsers"] = false;
                            where += " where displayName = '" + ddlDisplay.SelectedValue + "'";
                        }
                    }
                    else if (ddlDisplay.SelectedValue == "All Users")
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            ViewState["AllUsers"] = false;
                        }
                        else
                        {
                            ViewState["AllUsers"] = true;
                        }
                    }
                }

                /*** modified datetime - From Date to Date ***/

                if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // both from/to date
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and modified > ='" + txtDateFrom.Text + " 00:00:00'  and modified < ='" + txtDateTo.Text + " 23:59:59'";
                    }
                    else
                    {
                        ViewState["AllUsers"] = false;
                        where += " where modified > ='" + txtDateFrom.Text + " 00:00:00'  and modified < ='" + txtDateTo.Text + " 23:59:59'";
                    }
                }
                else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text)) // from date only 
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and modified > ='" + txtDateFrom.Text + " 00:00:00' ";
                    }
                    else
                    {
                        ViewState["AllUsers"] = false;
                        where += " where modified > ='" + txtDateFrom.Text + " 00:00:00' ";
                    }
                }
                else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // to date only 
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and modified < ='" + txtDateTo.Text + " 23:59:59' ";
                    }
                    else
                    {
                        ViewState["AllUsers"] = false;
                        where += " where modified < ='" + txtDateTo.Text + " 23:59:59' ";
                    }
                }
            
            }

            else if (ddlComp.SelectedValue == "caseVerify")
            {
                if (ddlPerform.SelectedValue != "0" && ddlPerform.SelectedValue != "")
                {
                    if (ddlPerform.SelectedValue != "Select Performer")
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where += " and performedBy = '" + ddlPerform.SelectedValue + "'";
                        }
                        else
                        {
                            where += " where performedBy = '" + ddlPerform.SelectedValue + "'";
                        }
                    }
                }

                /*** modified datetime - From Date to Date ***/

                if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // both from/to date
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and actionDateTime > ='" + txtDateFrom.Text + " 00:00:00'  and actionDateTime < ='" + txtDateTo.Text + " 23:59:59'";
                    }
                    else
                    {
                        where += " where actionDateTime > ='" + txtDateFrom.Text + " 00:00:00'  and actionDateTime < ='" + txtDateTo.Text + " 23:59:59'";
                    }
                }
                else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text)) // from date only 
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and actionDateTime > ='" + txtDateFrom.Text + " 00:00:00' ";
                    }
                    else
                    {
                        where += " where actionDateTime > ='" + txtDateFrom.Text + " 00:00:00' ";
                    }
                }
                else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // to date only 
                {
                    if (!string.IsNullOrEmpty(where))
                    {
                        where += " and actionDateTime < ='" + txtDateTo.Text + " 23:59:59' ";
                    }
                    else
                    {
                        where += " where actionDateTime < ='" + txtDateTo.Text + " 23:59:59' ";
                    }
                }
            }
            
            return where;
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_GetSortDirection");
            }
            return nextDir;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = GetWhereClause();
                    query = string.Format("SELECT COUNT({0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);
                    SqlCommand cmdAudit = new SqlCommand(query, conn);
                    cmdAudit.CommandTimeout = 0;
                    DataTable dtAudit = new DataTable();
                    conn.Open();
                    SqlDataAdapter daAudit = new SqlDataAdapter(cmdAudit);
                    conn.Close();
                    daAudit.Fill(dtAudit);
                    if (dtAudit.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dtAudit.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_GetTotalRecords");
            }
            return totalRecords;
        }

        protected void ddlComp_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPerform.Items.Clear();
            txtDateFrom.Text = string.Empty;
            txtDateTo.Text = string.Empty;
            searchreport.Visible = false;   // Clean Screen

            if (ddlComp.SelectedValue == "caseVerify")
            {
                divDisplayName.Visible = false;
                BindCVDropDown();
            }
            else
            {
                divDisplayName.Visible = true;
                BindUMDropDown();
            }
        }

        /************ Common Methods End ****************/


        /************ User Management Audit Start ****************/

        public void BindUMDropDown()
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtModifer = new DataTable();
                DataTable dtDisplayName = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string queryList = "";
                    queryList = string.Format("select distinct modifier from  MBDR_System.UsersHistory order by modifier asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daM = new SqlDataAdapter(cmd);
                    conn.Close();
                    daM.Fill(dtModifer);
                    ddlPerform.DataSource = dtModifer;
                    ddlPerform.DataTextField = "modifier";
                    ddlPerform.DataValueField = "modifier";
                    ddlPerform.DataBind();
                    ddlPerform.Items.Insert(0, "Select Performer");

                    string queryDisplayName = "";
                    queryDisplayName = string.Format("select distinct displayName from  MBDR_System.UsersHistory order by displayName asc");
                    SqlCommand cmdDN = new SqlCommand(queryDisplayName, conn);
                    cmdDN.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daDN = new SqlDataAdapter(cmdDN);
                    conn.Close();
                    daDN.Fill(dtDisplayName);
                    ddlDisplay.DataSource = dtDisplayName;
                    ddlDisplay.DataTextField = "displayName";
                    ddlDisplay.DataValueField = "displayName";
                    ddlDisplay.DataBind();
                    ddlDisplay.Items.Insert(0, "All Users");
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_BindUMDropDown");
            }
        }

        public void BindGridUserManagement(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                searchreport.Visible = true;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;

                string sortExp = string.Empty;

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();

                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }

                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderBySID = sortExp;

                    if (string.IsNullOrEmpty(OrderBySID))
                    {
                        OrderBySID = "displayName, modified asc";
                    }

                    int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;

                    where = GetWhereClause();


                    if (Convert.ToBoolean(ViewState["AllUsers"]) == true)
                    {
                        ViewState["AllUsers"] = false;

                        query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY userId ORDER BY userId DESC) AS CountOfRows,* FROM {0}.{1}) AS RESULT) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);
                    }
                    else
                    {
                        query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY userId ORDER BY userId DESC) AS CountOfRows,* FROM {0}.{1} {5}) AS RESULT) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);
                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);

                    gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };

                    gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);

                    if (gvDynamic.VirtualItemCount < 1)
                    {
                        ViewState["NoResult"] = true;

                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                    }

                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                    gvDynamic.DataSource = dt;
                    gvDynamic.DataBind();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_BindGridUserManagement");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }
                
        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;
                ViewState["PageIndex"] = gvDynamic.PageIndex;
                BindGridUserManagement(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_gvDynamic_PageIndexChanging");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;                
                BindGridUserManagement(hdfSchema.Value, hdfTableName.Value);               
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_gvDynamic_Sorting");
            }
        }
        
        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string oldFirstName = DataBinder.Eval(e.Row.DataItem, "oldFirstName").ToString();
                    string newFirstName = DataBinder.Eval(e.Row.DataItem, "newFirstName").ToString();

                    if (!oldFirstName.Equals(newFirstName, StringComparison.OrdinalIgnoreCase))
                    {
                        e.Row.Cells[4].BackColor = System.Drawing.Color.Pink;
                    }

                    string oldLastName = DataBinder.Eval(e.Row.DataItem, "oldLastName").ToString();
                    string newLastName = DataBinder.Eval(e.Row.DataItem, "newLastName").ToString();

                    if (!oldLastName.Equals(newLastName, StringComparison.OrdinalIgnoreCase))
                    {
                        e.Row.Cells[5].BackColor = System.Drawing.Color.Pink;
                    }

                    string oldEmailAddress = DataBinder.Eval(e.Row.DataItem, "oldEmailAddress").ToString();
                    string newEmailAddress = DataBinder.Eval(e.Row.DataItem, "newEmailAddress").ToString();

                    if (!oldEmailAddress.Equals(newEmailAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        e.Row.Cells[6].BackColor = System.Drawing.Color.Pink;
                    }

                    string oldRole = DataBinder.Eval(e.Row.DataItem, "oldRole").ToString();
                    string newRole = DataBinder.Eval(e.Row.DataItem, "newRole").ToString();

                    if (!oldRole.Equals(newRole, StringComparison.OrdinalIgnoreCase))
                    {
                        e.Row.Cells[7].BackColor = System.Drawing.Color.Pink;
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_gvDynamic_RowDataBound");
            }
        }

        /************ User Management Audit End ****************/

        /********************* Case Verifcation Audit Start ****************************/

        public void BindCVDropDown()
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtModifer = new DataTable();
                DataTable dtDisplayName = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string displayName = !string.IsNullOrEmpty(Request.QueryString["displayName"]) ? Convert.ToString(Request.QueryString["displayName"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string queryList = "";
                    queryList = string.Format("select distinct performedBy from  MBDR_System.CaseVerificationAudit order by performedBy asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daM = new SqlDataAdapter(cmd);
                    conn.Close();
                    daM.Fill(dtModifer);
                    ddlPerform.DataSource = dtModifer;
                    ddlPerform.DataTextField = "performedBy";
                    ddlPerform.DataValueField = "performedBy";
                    ddlPerform.DataBind();
                    ddlPerform.Items.Insert(0, "Select Performer");
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_BindCVDropDown");
            }
        }
        
        public void BindGridCaseVerification(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                searchreport.Visible = true;
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;

                string sortExp = string.Empty;

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();

                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }

                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderBySID = sortExp;

                    if (string.IsNullOrEmpty(OrderBySID))
                    {
                        OrderBySID = "performedBy asc";
                    }

                    int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;

                    where = GetWhereClause();

                    query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY reportid ORDER BY reportid DESC) AS CountOfRows,* FROM {0}.{1} {5}) AS RESULT) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ", schema, tableName, dataKeyname, ((gvDynamicCaseVerification.PageIndex * gvDynamicCaseVerification.PageSize) + 1), (gvDynamicCaseVerification.PageIndex + 1) * gvDynamicCaseVerification.PageSize, where, OrderBySID);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);

                    gvDynamicCaseVerification.DataKeyNames = new string[] { dataKeyname.Trim() };

                    gvDynamicCaseVerification.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);

                    if (gvDynamicCaseVerification.VirtualItemCount < 1)
                    {
                        ViewState["NoResult"] = true;

                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                    }

                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                    gvDynamicCaseVerification.DataSource = dt;
                    gvDynamicCaseVerification.DataBind();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        protected void gvDynamicCaseVerification_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamicCaseVerification.PageIndex = e.NewPageIndex;
                ViewState["PageIndex"] = gvDynamicCaseVerification.PageIndex;
                BindGridCaseVerification(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_gvDynamicCaseVerification_PageIndexChanging");
            }
        }

        protected void gvDynamicCaseVerification_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvDynamicCaseVerification_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGridCaseVerification(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "Audit_gvDynamicCaseVerification_Sorting");
            }
        }

        /********************* Case Verifcation Audit End ****************************/

        protected void ddlPerform_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchreport.Visible = false;
        }

        protected void ddlDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchreport.Visible = false;
        }

    }
}