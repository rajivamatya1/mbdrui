﻿
    function cAlertMsgs(message, titleFor) {

        $("#commonMsg").dialog({

            width: 450,
            modal: true,
            dialogClass: "no-close",

            open: function () {
                var connMsg = message;
                $(this).append(connMsg);
            },

            buttons: [
                {
                    text: "Ok",
                    open: function () {
                        $(this).addClass('okcls')
                    },
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ],
            position: {
                my: "center center",
                at: "center center"
            },
            closeOnEscape: false
        }).prev(".ui-dialog-titlebar").css("background", "#00607F");

        if (titleFor) {
            $(".ui-dialog-title").text(titleFor);
        }
    }
    function sViolation(message, titleFor) {

        $("#securityMsg").dialog({

            width: 450,
            modal: true,
            dialogClass: "no-close",

            open: function () {
                var connMsg = message;
                $(this).append(connMsg);
            },

            buttons: [
                {
                    text: "Ok",
                    open: function () {
                        $(this).addClass('okcls')
                    },
                    click: function () {
                        window.location.href = "/Default.aspx";
                    }
                }
            ],
            position: {
                my: "center center",
                at: "center center"
            },
            closeOnEscape: false
        }).prev(".ui-dialog-titlebar").css("background", "#00607F");

        if (titleFor) {
            $(".ui-dialog-title").text(titleFor);
        }

        $(".ui-dialog-titlebar-close").on("click", function () {
            window.location.href = "/Default.aspx";
        });

        $(document).on("keydown", function (e) {
            if (e.which === 27) {
                window.location.href = "/Default.aspx";
            }
        });
    }
    function AltarumLogin(message, titleFor) {
      
        $("#altarumLogin").dialog({

            width: 450,
            modal: true,
            dialogClass: "no-close",

            open: function () {
                var connMsg = message;
                $(this).append(connMsg);
            },

            buttons: [
                {
                    text: "Ok",
                    open: function () {
                        $(this).addClass('okcls')
                    },
                    click: function () {
                        window.location.href = "/Login.aspx";
                    }
                }
            ],
            position: {
                my: "center center",
                at: "center center"
            },
            closeOnEscape: false
        }).prev(".ui-dialog-titlebar").css("background", "#00607F");

        if (titleFor) {
            $(".ui-dialog-title").text(titleFor);
        }

        $(".ui-dialog-titlebar-close").on("click", function () {
            window.location.href = "/Login.aspx";
        });

        $(document).on("keydown", function (e) {
            if (e.which === 27) {
                window.location.href = "/Login.aspx";
            }
        });
    }
    function SOMLogin(message, titleFor) {
       
        $("#somLogin").dialog({

            width: 450,
            modal: true,
            dialogClass: "no-close",

            open: function () {
                var connMsg = message;
                $(this).append(connMsg);
            },

            buttons: [
                {
                    text: "Ok",
                    open: function () {
                        $(this).addClass('okcls')
                    },
                    click: function () {
                        window.location.href = "https://miloginworkerqa.michigan.gov";
                    }
                }
            ],
            position: {
                my: "center center",
                at: "center center"
            },
            closeOnEscape: false
        }).prev(".ui-dialog-titlebar").css("background", "#00607F");

        if (titleFor) {
            $(".ui-dialog-title").text(titleFor);
        }

        $(".ui-dialog-titlebar-close").on("click", function () {
            window.location.href = "https://miloginworkerqa.michigan.gov";
        });

        $(document).on("keydown", function (e) {
            if (e.which === 27) {
                window.location.href = "https://miloginworkerqa.michigan.gov";
            }
        });
    }