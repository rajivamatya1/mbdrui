// JavaScript Document
$(document).ready(function () {
    $("#primary-icon").on("click", function () {
        $("#primary-icon").toggleClass("active");
        $("#primary").toggleClass("active");
    });
    $("#primary").on("click", function () {
        $("#primary-icon").removeClass("active");
        $("#primary").removeClass("active");
    });
    $("main").on("click", function () {
        $("#primary-icon").removeClass("active");
        $("#primary").removeClass("active");
    });
    $("#btnLogout").on("blur", function () {
        $("#primary-icon").removeClass("active");
        $("#primary").removeClass("active");
    });
    $('legend').click(function () {
        $('fieldset').toggleClass('active');
    });
    $('.button-left').click(function () {
        $('.sidebar').toggleClass('fliph');
    });
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $("#primary-icon").removeClass("active");
            $("#primary").removeClass("active");
        }
    });
    $("#lnkErrorId").on("click", function () {
        $("#lnkErrorId").removeClass("active");
        $("#lnkErrorId").toggleClass('active');
    });
});

function SessionExpireAlert(sessionTimeOutAlert) {
    
    var totalTimeOut = 900;
    var notifyTimeOut = sessionTimeOutAlert; // 13 mins and 30 seconds 
    var alertTimeOut = totalTimeOut; // 13 mins and 30 seconds 


    // Display the initial countdown

    document.getElementById("secondsIdle").innerHTML = alertTimeOut;
    document.getElementById("seconds").innerHTML = alertTimeOut;

    //Show Popup immediately
    setTimeout(function () {
        $find("mpeTimeout").show();
        alertTimeOut = 90; //Reset countdown to 90 seconds for the popup countdown
    }, notifyTimeOut); //time to pop up 13 mins and 30 seconds 

    // start countdown timer
    var countdownInterval = setInterval(function () {              
        
        alertTimeOut--;

        document.getElementById("seconds").innerHTML = alertTimeOut;
        document.getElementById("secondsIdle").innerHTML = alertTimeOut;
        

        if (alertTimeOut <= 0)
        {   
            clearInterval(countdownInterval); // stop countdown
            logoutUser();
        }
    }, 1000);
   

    // function to log out the user

    function logoutUser() {
        
        var env = document.getElementById("envValue").value;
        var loginpage = document.getElementById("miMiLogin").value;
        
        if (env === "dev" || env === "qa") {
            window.location.href = "login.aspx";
        }
        else {
         
            window.location.href = loginpage;  // keep it -Page Redirect to MiLogin -            
            window.closeChildWindow();
        }
    }

    // Attach event listener to "yes" button to log out immediately

    document.getElementById("btnYes").onclick = function () {
      
        clearInterval(countdownInterval);
    };

    document.getElementById("btnNo").onclick = function () {
       
        clearInterval(countdownInterval);
    };
}

function closeChildWindow() {

    if (window.opener && !window.opener.closed) {
        window.close();
    }
}

function ResetSession() {
   
    //Redirect to refresh Session.
    window.location = window.location.href;
}


document.addEventListener("DOMContentLoaded", function () {
   
    document.getElementById("btnYes").addEventListener("click", function () {
        ResetSession();
    });

    document.getElementById("btnNo").addEventListener("click", function () {
        var env = document.getElementById("envValue").value;
        var loginpage = document.getElementById("miMiLogin").value;

        if (env === "dev" || env === "qa") {
            window.location.href = "login.aspx";
        }
        else {
            window.location.href = loginpage;
            window.closeChildWindow();
        }
    });

});