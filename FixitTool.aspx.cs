﻿using DynamicGridView.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class FixitTool : System.Web.UI.Page
    {
        dynamic permissions = null;

        bool flagFixer = false;
        bool flagUnresloved = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            permissions = CheckRole();

            try
            {
                txtChildDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtChildDOB.Attributes["min"] = new DateTime(1, 1, 1).ToString("yyyy-MM-dd");                

                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

                if (!IsPostBack)
                {
                    if(Convert.ToBoolean(Session["ConFail"]) == true)
                    {
                        Session["ConFail"] = false;
                        ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
                    }

                    BindColumn();

                    if (!flagFixer)
                    {
                        flagUnresloved = false;
                        NoneGreyOutSearchFields();
                        BindColumn();
                        BindGridEmpty();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                    {
                        BindGridEmpty();
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_Page_Load");
            }
        }

        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "ML_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_CheckRole");
            }
            return permissions;
        }

        protected void BindColumn()
        {
            try
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

                // USer Info
                DataTable dtOwner = new DataTable();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"Select distinct fixerId, displayName from MBDR_System.FixItFixerNames order by displayName asc";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    daColumns.Fill(dtOwner);

                    ddlFilterByOwner.DataSource = dtOwner;
                    ddlFilterByOwner.DataTextField = "displayName";
                    ddlFilterByOwner.DataValueField = "fixerId";
                    ddlFilterByOwner.DataBind();
                    ddlFilterByOwner.Items.Insert(0, "Select Owner");

                    for (int i = ddlFilterByOwner.Items.Count - 1; i >= 0; i--)
                    {
                        ListItem item = ddlFilterByOwner.Items[i];

                        if (item.Value == "46")
                        {
                            ddlFilterByOwner.Items.Remove(item);
                        }                        
                    }

                    string currentUser = Convert.ToString(Session["userid"]);

                    ListItem currentUserItem = ddlFilterByOwner.Items.FindByValue(currentUser);

                    if (flagUnresloved && currentUserItem != null && currentUserItem.Value == currentUser)
                    {
                        ddlFilterByOwner.Items.Clear();

                        ddlFilterByOwner.Items.Add(currentUserItem);
                        ddlFilterByOwner.SelectedValue = currentUser;

                        GreyOutSearchFields();

                        flagFixer = true;
                    }
                    else
                    {
                        ddlFilterByOwner.SelectedIndex = 0;
                    }
                }

                DataTable dtFlagged = new DataTable();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"Select distinct displayName, userId from MBDR_System.FixItFlaggerNames order by displayName asc";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    daColumns.Fill(dtFlagged);

                    ddlFilterByFlagged.DataSource = dtFlagged;
                    ddlFilterByFlagged.DataTextField = "displayName";
                    ddlFilterByFlagged.DataValueField = "userId";
                    ddlFilterByFlagged.DataBind();
                    ddlFilterByFlagged.Items.Insert(0, "Select Flagged By");
                }

                if(flagFixer) // if user owns show list when page load.
                {
                    BindGrid(hdfSchema.Value, hdfTableName.Value);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_BindColumn");
            }
        }

        public void GreyOutSearchFields()
        {
            ddlFilterByStatus.Enabled = false;
            ddlFilterByOwner.Enabled = false;

            ddlFilterByFlagged.Enabled = false;
            txtchdLN.Enabled = false;
            txtFid.Enabled = false;
            txtChildDOB.Enabled = false;
            btnSearch.Enabled = false;
            btnResetAdv.Enabled = false;

            ddlFilterByFlagged.CssClass = "enableGreyFitIt";
            txtchdLN.CssClass = "enableGreyFitIt";
            txtFid.CssClass = "enableGreyFid";
            txtChildDOB.CssClass = "enableGreyDate";
            btnSearch.CssClass = "cursor-default";
            btnResetAdv.CssClass = "cursor-default";
        }

        public void NoneGreyOutSearchFields()
        {
            ddlFilterByStatus.Enabled = true;
            ddlFilterByOwner.Enabled = true;

            ddlFilterByFlagged.Enabled = true;
            txtchdLN.Enabled = true;
            txtFid.Enabled = true;
            txtChildDOB.Enabled = true;
            btnSearch.Enabled = true;
            btnResetAdv.Enabled = true;

            ddlFilterByFlagged.CssClass = "form-control radiusborder";
            txtchdLN.CssClass = "form-control radiusborder";
            txtFid.CssClass = "form-control";
            txtChildDOB.CssClass = "cursor-pointerWhiteNewField customDateField";
            btnSearch.CssClass = "cursor-pointer";
            btnResetAdv.CssClass = "cursor-pointer";
        }

        private string GetWhereClause()
        {
            string where = "where 1=1";

            if (!string.IsNullOrEmpty(txtFid.Text))
            {
                where += " and fixItId = '" + txtFid.Text.Replace("'", "''").Trim() + "'";
            }

            if (ddlFilterByStatus.SelectedValue != "0" && ddlFilterByStatus.SelectedValue != "")
            {
                if (ddlFilterByStatus.SelectedValue == "Resolved")
                {
                    where += " and resolved is not null";
                }
                else
                {
                    where += " and resolved is null";
                }
            }

            if (ddlFilterByOwner.SelectedValue != "0" && ddlFilterByOwner.SelectedValue != "Select Owner")
            {
                if (flagFixer)
                {
                    string currentFixerUser = Convert.ToString(Session["DisplayName"]);

                    where += " and fixer = '" + currentFixerUser + "'";
                }
                else
                {
                    where += " and fixer = '" + ddlFilterByOwner.SelectedItem + "'";
                }
            }

            if (ddlFilterByFlagged.SelectedValue != null && ddlFilterByFlagged.SelectedValue != "" && ddlFilterByFlagged.SelectedValue != "0" && ddlFilterByFlagged.SelectedValue != "Select Flagged By")
            {
                where += " and reporter = '" + ddlFilterByFlagged.SelectedItem + "'";
            }

            if (!string.IsNullOrEmpty(txtchdLN.Text) && !txtchdLN.Text.Contains("*"))
            {
                where += " and childLastName = '" + txtchdLN.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtChildDOB.Text) && !txtChildDOB.Text.Contains("*"))
            {
                where += " and convert(date,birthDate,101) ='" + txtChildDOB.Text + "'";
            }

            return where;
        }

        public void BindGridEmpty()
        {
            try
            {
                DataTable dt = new DataTable();

                string schema = "MBDR_System";
                string tableName = "FixItListing";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    daColumns.Fill(dt);

                }

                gvDynamic.Columns.Clear();
                CommandField commandField = new CommandField();
                commandField.ButtonType = ButtonType.Link;
                commandField.HeaderText = "Action";
                gvDynamic.Columns.Add(commandField);

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                List<string> lstColumns = includeColumns.Split(',').ToList();
                foreach (var col in lstColumns)
                {
                    if (gvDynamic.Columns.Count < 12)
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string colName = Convert.ToString(dt.Rows[i][0]);
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                                if (colName != "reporterId" && colName != "fixerId")
                                {
                                    if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                        BoundField field = new BoundField();
                                        field.HeaderText = columnInfo;
                                        field.DataField = Convert.ToString(dt.Rows[i][0]);
                                        field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                        if (readonlyColumns.Contains(colName))
                                        {
                                            field.ReadOnly = true;
                                        }
                                        gvDynamic.Columns.Add(field);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                DataTable dtEmpty = new DataTable();
                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                gvDynamic.DataSource = dtEmpty;
                gvDynamic.DataBind();
            }

            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_BindGridEmpty");
            }
        }

        public void BindGrid(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();

                schema = "MBDR_System";
                tableName = "FixItListing";

                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;

                string sortExp = string.Empty;

                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Uri.EscapeDataString(Convert.ToString(item[0])) == "fixItId")
                            {
                                sortExp = " cascades desc";
                                break;
                            }
                        }
                    }
                }
                
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderBySID = sortExp;
                    if (string.IsNullOrEmpty(OrderBySID))
                    {
                        OrderBySID = "cascades desc";
                    }
                    int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                    where = GetWhereClause();
                    if (!where.Contains("and"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                    }
                    else
                    {
                        query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY fixItId ORDER BY fixItId DESC) AS CountOfRows,* FROM {0}.{1} {5}) AS RESULT) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ORDER BY {6}", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);

                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dt);

                        gvDynamic.Columns.Clear();
                        CommandField commandField = new CommandField();
                        commandField.ButtonType = ButtonType.Link;
                        commandField.HeaderText = "Action";
                        commandField.ShowEditButton = true;
                        gvDynamic.Columns.Add(commandField);

                        List<string> lstColumns = includeColumns.Split(',').ToList();

                        foreach (var col in lstColumns)
                        {
                            if (gvDynamic.Columns.Count < 12)
                            {
                                if (!string.IsNullOrEmpty(col))
                                {
                                    foreach (DataColumn item in dt.Columns)
                                    {
                                        string colName = item.ColumnName;
                                        string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                        if (colName != "reporterId" && colName != "fixerId")
                                        {
                                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                                BoundField field = new BoundField();
                                                field.HeaderText = columnInfo;
                                                field.DataField = item.ColumnName;
                                                field.SortExpression = item.ColumnName;
                                                if (readonlyColumns.Contains(colName))
                                                {
                                                    field.ReadOnly = true;
                                                }
                                                gvDynamic.Columns.Add(field);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };

                        gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);

                        if (gvDynamic.VirtualItemCount < 1)
                        {
                            if (flagFixer)
                            {
                                flagFixer = false;
                            }

                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                        }

                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        gvDynamic.DataSource = dt;
                        gvDynamic.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitToolSearchView_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = GetWhereClause();
                    query = string.Format("SELECT COUNT(DISTINCT {0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_GetTotalRecords");
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;
                ViewState["PageIndex"] = gvDynamic.PageIndex;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_PageIndexChanging");
            }
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = -1;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_Sorting");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_GetSortDirection");
            }
            return nextDir;
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var birthDateValue = DataBinder.Eval(e.Row.DataItem, "birthDate");
                    
                    if (birthDateValue != null && !string.IsNullOrEmpty(birthDateValue.ToString()))
                    {
                        DateTime birthDate = Convert.ToDateTime(birthDateValue);
                        e.Row.Cells[5].Text = birthDate.ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        e.Row.Cells[5].Text = string.Empty;
                    }

                    /*   ToolTip for comments */

                    int commentIndex = 10;

                    string fullComment = DataBinder.Eval(e.Row.DataItem, "comment").ToString();

                    if (fullComment.Length > 25)
                    {
                        e.Row.Cells[commentIndex].Text = fullComment.Substring(0, 25) + " ...";
                    }
                    else
                    {
                        e.Row.Cells[commentIndex].Text = fullComment;
                    }

                    e.Row.Cells[commentIndex].ToolTip = fullComment;

                    /*  Fix details Nagaviator for case */


                    var resolvedCase = DataBinder.Eval(e.Row.DataItem, "resolved");

                    var fixerId = DataBinder.Eval(e.Row.DataItem, "fixerId");

                    string currentUser = Convert.ToString(Session["userid"]);

                    string isItCurrentUser = Convert.ToString(fixerId);

                    if (!string.IsNullOrEmpty(isItCurrentUser) && currentUser == isItCurrentUser)
                    {
                        Session["userExist"] = true;
                    }
                    else
                    {
                        Session["userExist"] = false;
                    }

                    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                    field.Text = "Fix";
                    field.Enabled = false;
                    field.Visible = false;

                    int inFlightValue = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "inFlight"));

                    if (inFlightValue > 0) 
                    {
                        e.Row.Cells[0].CssClass = "classYellowCell";
                    }

                    //field.Text = "Fix";

                    if (resolvedCase != null && !string.IsNullOrEmpty(resolvedCase.ToString()))   // disable fix url if resolved is null or empty
                    {
                        field.Visible = false;
                    }
                    else
                    {
                        if (fixerId != null && !string.IsNullOrEmpty(fixerId.ToString()))
                        {
                            if (!string.IsNullOrEmpty(isItCurrentUser) && currentUser != isItCurrentUser)
                            {
                                field.Visible = false;
                            }
                            else
                            {
                                field.Enabled = true;
                                field.Visible = true;
                                field.ToolTip = string.Format("Fix Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                                field.OnClientClick = "ShowProgress();";
                            }
                        } // conditions for fixer id not null 
                        else
                        {
                            if (!string.IsNullOrEmpty(isItCurrentUser) && currentUser == isItCurrentUser)
                            {
                                field.Visible = false;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(isItCurrentUser))
                                {
                                    field.Visible = false;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isItCurrentUser) && currentUser == isItCurrentUser)
                                    {
                                        field.Visible = true;
                                    }
                                    else if (!string.IsNullOrEmpty(isItCurrentUser) && currentUser != isItCurrentUser)
                                    {
                                        field.Visible = false;
                                    }
                                    else if (string.IsNullOrEmpty(isItCurrentUser) && string.IsNullOrEmpty(resolvedCase.ToString()))
                                    {
                                        if (Convert.ToBoolean(Session["userExist"]))
                                        {
                                            field.Visible = false;
                                        }
                                        else
                                        {
                                            field.Enabled = true;
                                            field.Visible = true;
                                            field.ToolTip = string.Format("Fix Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                                            field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                                            field.OnClientClick = "ShowProgress();";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_RowDataBound");
            }
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string columnName = string.Empty;
                    string where = "1";

                    string resolved = Convert.ToString(ddlFilterByStatus.SelectedValue);

                    if (resolved != "")
                    {
                        where += "&resolved=" + resolved + "";
                    }

                    string fixer = Convert.ToString(ddlFilterByOwner.SelectedValue);

                    if (fixer != "Select Owner")
                    {
                        where += "&fixer=" + fixer + "";
                    } 

                    string reporter = Convert.ToString(ddlFilterByFlagged.SelectedValue);

                    if (reporter != "Select Flagged By")
                    {
                        where += "&reporter=" + reporter + "";
                    }

                    if (!string.IsNullOrEmpty(txtchdLN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&childFirstName=" + txtchdLN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtChildDOB.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&birthDate=" + txtChildDOB.Text + "";
                    }
                   
                    string sortExpre = string.Empty;
                    
                    if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        sortExpre = "cascades desc";
                    }
                    else
                    {
                        sortExpre = Convert.ToString(ViewState["SortExpression"]);
                    }

                    int getfixItId = Convert.ToInt32(e.CommandArgument.ToString());

                    Response.Redirect("FixitToolDetails.aspx?Schema=MBDR_System&Table=FixItListing&fixItId=" + getfixItId + "&where=" + where + "&sortExp=" + sortExpre + "", false);

                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_RowCommand");
            }
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_gvDynamic_RowEditing");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    //ViewState["SortExpression"] = "cascades desc";
                    gvDynamic.PageIndex = 0;
                    BindGrid(hdfSchema.Value, hdfTableName.Value);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_btnSearch_Click");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["SortExpression"] = "fixItId desc";
                gvDynamic.PageIndex = 0;

                BindGridEmpty();
                
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FixitTool_btnReset_Click");
            }
        }
    }
}