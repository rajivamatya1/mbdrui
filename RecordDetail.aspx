﻿<%@ Page Title="MBDR Default Record View" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="RecordDetail.aspx.cs" Inherits="DynamicGridView.RecordDetail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfRecordId" Value="" />
            <asp:HiddenField runat="server" ID="hdfKeyName" Value="" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <h1><img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <asp:Button Text="" ID="btnBack" OnClick="btnBack_Click" runat="server" />
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-9 col-sm-12">
            <div class="program">
                <div class="program-header">
                    <div class="program-icon">
                        <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                    </div>
                    <div class="program-name">
                        <asp:Label Text="" ID="lblPanelHeading" runat="server" />
                    </div>
                </div>
                <div class="program-description">
                    <div class="row">
                        <!--<div class="col col-lg-12 col-sm-12">
                            <asp:Label Text="Source" ID="Label1" runat="server" Font-Bold="true" /><br />
                            <asp:Label Text="" ID="lblTableName" runat="server" Font-Bold="true" /><br />
                            <br />
                        </div>-->
                        <asp:Repeater runat="server" ID="rptCategory" OnItemDataBound="rptCategory_ItemDataBound">
                            <ItemTemplate>
                                <div class="col col-lg-4 col-md-6 col-sm-6">
                                    <asp:Repeater runat="server" ID="rptChild">
                                        <ItemTemplate>
                                            <asp:Label Text='<%#Eval("ColumnName") %>' ID="lblColumnName" runat="server" Font-Bold="true" />
                                            <br />
                                            <asp:Label Text='<%#Eval("ColumnValue") %>' ID="lblColumnValue" runat="server" /><br />
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-lg-3 col-sm-12">
            <div class="program" runat="server" id="pnlNavigation">
                <div class="program-header">
                    <div class="program-name">
                        Navigation
                    </div>
                </div>
                <div class="program-description">
                    <asp:Label Text="Compare this record with:" ID="Label2" runat="server" Font-Bold="true" /><br />
                    <asp:Repeater runat="server" ID="rptNavigation" OnItemCommand="rptNavigation_ItemCommand" OnItemDataBound="rptNavigation_ItemDataBound">
                        <ItemTemplate>
                            <asp:Button Text="" CommandName="Compare" CommandArgument="" ID="btnCompare" CssClass="btn btn-xs btn-primary" runat="server" /> <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
