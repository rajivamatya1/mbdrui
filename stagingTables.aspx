﻿<%@ Page Title="MBDR Staging Tables" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="stagingTables.aspx.cs" Inherits="stagingTables" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <h1 class="page-title"><img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> Staging Tables</h1>
        <% foreach (BDRTablesList Names in BDRTables) { %>
            <div class="col col-lg-3 col-md-6 col-sm-6">
                <div class="program">
                    <a href="/DefaultDataView.aspx?schema=<%= Names.SCHName %>&table=<%= Names.DBName %>&name=<%= Names.Name %>">
                        <div class="program-header">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <div class="program-name">
                                <%= Names.Name %>
                            </div>
                        </div>
                    </a>
                </div>	
            </div>
        <% } %>
    </div>
</asp:Content>
