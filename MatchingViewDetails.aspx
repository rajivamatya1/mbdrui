﻿<%@ Page Title="MBDR UI Matching View Details" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="MatchingViewDetails.aspx.cs" Inherits="MatchingViewDetails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <%--   <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
    <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>--%>
    <link href="Content/css/MLTable.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <div class="row" id="mlsSecurityHeader">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="Matching and Linking - Possible Source Matches" runat="server" />
            </h1>
        </div>
    </div>
    <div class="row" id="mlsSecurityBody">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-6">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:HiddenField ID="hdfSecurityCheck" runat="server" />

                                    <asp:HiddenField ID="hdflinkCaseReport" runat="server" />
                                    <asp:HiddenField ID="hdfFullViewDetails" runat="server" />
                                    <asp:HiddenField ID="hdfScrutinizerID" runat="server" />
                                    <asp:HiddenField ID="hdfArchiveID" runat="server" />
                                    <asp:HiddenField ID="hdfLastName" runat="server" />
                                    <asp:HiddenField ID="hdfFirstName" runat="server" />
                                    <asp:HiddenField ID="hdfDOB" runat="server" />
                                    <asp:HiddenField ID="hdfMomLastName" runat="server" />
                                    <asp:HiddenField ID="hdfMomFirstName" runat="server" />
                                    <asp:HiddenField ID="hdfAddress1" runat="server" />
                                    <asp:HiddenField ID="hdfSchema" runat="server" />
                                    <asp:HiddenField ID="hdfTableName" runat="server" />
                                    <asp:HiddenField ID="hdfDataKeyName" runat="server" />
                                    <asp:HiddenField ID="hdfTableKeyName" runat="server" />
                                    <asp:HiddenField ID="hdfRowID" runat="server" />
                                    <asp:HiddenField ID="hdfNextRowID" runat="server" />
                                    <asp:HiddenField ID="hdfPreviousRowID" runat="server" />
                                    <asp:HiddenField ID="hdfDetailsID" runat="server" />
                                    <asp:HiddenField ID="hdfCaseDetailsID" runat="server" />
                                    <asp:HiddenField ID="IsCaseDetailID" runat="server" />
                                    <asp:HiddenField ID="hdfMessage" runat="server" />
                                    <asp:HiddenField ID="hdfFirstTable" runat="server" />
                                    <asp:HiddenField ID="hdfMultipleTableName" runat="server" />
                                    <asp:HiddenField ID="hdfOwner" runat="server" />
                                    <asp:HiddenField ID="hdfMatchDetailId" runat="server" />
                                    <asp:HiddenField ID="hdfFromDate" runat="server" />
                                    <asp:HiddenField ID="hdfToDate" runat="server" />
                                    <asp:HiddenField ID="hdfFirstTableClickID" runat="server" />

                                    <asp:HiddenField ID="hdfRecordFrom" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportID" runat="server" />
                                    <asp:HiddenField ID="hdfBirthReportID" runat="server" />
                                    <asp:HiddenField ID="hdfDeathReportID" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportBirthRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordBirthRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordBirthRecNo" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportDeathRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordDeathRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordDeathRecNo" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportCaseRecordId" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordCaseRecordId" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordCaseRecordID" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportRecordForm" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordRecordForm" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordRecordForm" runat="server" />


                                    <asp:HiddenField ID="hdfJSCaseReportId" runat="server" />
                                    <asp:HiddenField ID="hdfJSCase_BirthRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfJSCase_DeathRecNo" runat="server" />

                                    <asp:HiddenField ID="hdfJSBirthRecordId" runat="server" />
                                    <asp:HiddenField ID="hdfJSBirth_BirthRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfJSBirth_DeathRecNo" runat="server" />

                                    <asp:HiddenField ID="hdfJSDeathRecordId" runat="server" />
                                    <asp:HiddenField ID="hdfJSDeath_BirthRecNo" runat="server" />
                                    <asp:HiddenField ID="hdfJSDeath_DeathRecNo" runat="server" />

                                    <asp:HiddenField ID="hdfIsTabClicked" runat="server" />
                                    <asp:HiddenField ID="hdfIsTabClickedCR" runat="server" />
                                    <asp:HiddenField ID="hdfIsTabClickedBR" runat="server" />
                                    <asp:HiddenField ID="hdfIsTabClickedDR" runat="server" />
                                    <asp:HiddenField ID="hdfIsTabClickedSM" runat="server" />
                                    <asp:HiddenField ID="hdfisCheckedCR" runat="server" />
                                    <asp:HiddenField ID="hdfisCheckedBR" runat="server" />
                                    <asp:HiddenField ID="hdfisCheckedDR" runat="server" />
                                    <asp:HiddenField ID="hdfFirstCheckedCRDid" runat="server" />
                                    <asp:HiddenField ID="hdfFirstCheckedBRDid" runat="server" />
                                    <asp:HiddenField ID="hdfFirstCheckedDRDid" runat="server" />
                                    <asp:HiddenField ID="hdfisFirstCheckedDetailsID" runat="server" />

                                    <asp:HiddenField ID="hdfCaseReportDidOnly" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordDidOnly" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordDidOnly" runat="server" />


                                    <asp:HiddenField ID="hdfCaseReportDidMatch" runat="server" />
                                    <asp:HiddenField ID="hdfBirthRecordDidMatch" runat="server" />
                                    <asp:HiddenField ID="hdfDeathRecordDidMatch" runat="server" />

                                    <%-- if Case Rec ID is avaialble and bithid/deathid  null (assigned value for linking process if there is indiviusal deathID/birthID from existing Source contains Case Rec ID)--%>

                                    <asp:HiddenField ID="hdfDIDCheckedValue" runat="server" />

                                    <asp:HiddenField ID="hdfDeathIDCurrentPageValue" runat="server" />
                                    <asp:HiddenField ID="hdfBirthIDCurrentPageValue" runat="server" />
                                    <asp:HiddenField ID="hdfCaseRecIDCurrentPageValue" runat="server" />
                                    <asp:HiddenField ID="hdftieit" runat="server" />

                                    <asp:HiddenField ID="hdfchkCaseRecId" runat="server" />
                                    <asp:HiddenField ID="hdfchkBirtRecId" runat="server" />
                                    <asp:HiddenField ID="hdfchkDeathRecId" runat="server" />

                                    <asp:HiddenField ID="hdfSimilarCaseRecordID" runat="server" />
                                    <asp:HiddenField ID="hdfSimilarIDs" runat="server" />
                                    <asp:HiddenField ID="hdfSimilarCheckUnCheckSIDs" runat="server" />
                                    <asp:HiddenField ID="hdffixTool" runat="server" />



                                    <asp:Button Text="Prev Candidate" runat="server" OnClick="btnPrev_Click" Visible="false" ID="btnPrev" CssClass="btn btn-primary" OnClientClick="mySessionClear()" />
                                    <asp:Button Text="Next Candidate" runat="server" OnClick="btnNext_Click" Visible="false" ID="btnNext" CssClass="btn btn-primary" OnClientClick="mySessionClear()" />
                                    <asp:Button Text="Return to Search Results" OnClick="btnAdvanceResult_Click" runat="server" ID="btnAdvanceResult" CssClass="btn btn-primary" OnClientClick="return ShowProgress();" />
                                    <asp:Button Text="New Search" OnClick="btnAll_Click" runat="server" ID="btnAll" CssClass="btn btn-primary" OnClientClick="return ShowProgress(); mySessionClear()"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="row row-no-padding-matching-linking">
                        <div class="row">
                            <div class="col col-lg-8 col-md-8 col-sm-8 mlsourceMargin">

                                <div class="col col-lg-12 col-md-12 col-sm-12">
                                    <div class="comparison-buttons-container">
                                        <div class="form-item-ml">
                                            <strong>Compare with Source: </strong>
                                            <asp:Button Text="Case Reports" runat="server" ID="btnCaseReports" OnClick="btnCaseReports_Click" CssClass="btn btn-primary" />
                                            <asp:Button Text="Birth Records" runat="server" ID="btnBirthRecords" OnClick="btnBirthRecords_Click" CssClass="btn btn-primary" />
                                            <asp:Button Text="Death Records" runat="server" ID="btnDeathRecords" OnClick="btnDeathRecords_Click" CssClass="btn btn-primary" />
                                            <asp:Button Text="Similar" runat="server" ID="btnSimilar" OnClick="btnSimilar_Click" CssClass="btn btn-primary" Visible="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col col-lg-12 col-md-12 col-sm-12">
                                    <div class="admin-workarea-menu">
                                        <ul class="menu">
                                            <li class="leaf">
                                                <asp:LinkButton ID="btnOwn" title="Take ownership" runat="server" OnClick="btnOwn_Click" OnClientClick="ShowProgress(this)"> 
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/file-user-icon.svg"  alt="Link"></image> Own
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf">
                                                <asp:LinkButton ID="btnRelease" title="Release ownership" runat="server" OnClick="btnRelease_Click" OnClientClick="return releaseAlert(event)">
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/unlock-fill.svg"  alt="Link"></image> Release
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf" runat="server">
                                                <asp:LinkButton ID="btnLink" title="Link to an existing case and/or birth and/or death record. The system will create a case record if one does not already exist." runat="server" OnClick="btnLink_Click" OnClientClick="return linkAlert(event)">
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/link-solid.svg" alt="Link"></image> Link
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf" runat="server">
                                                <asp:LinkButton ID="btnAssign" title="Create new case record" runat="server" Text="Confirm" OnClick="btnAssign_Click" OnClientClick="return assignAlert(event)">
                                                 <div class="link-icon">
                                                      <image src="Content/css/images/file-earmark-plus-fill.svg"  alt="Create Case Record ID"></image> Create New Case                                           
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf">
                                                <asp:LinkButton ID="btnPreviousMatch" BackColor="DarkGray" Enabled="false" title="Temporarily Disable" runat="server" OnClick="btnPreviousMatch_Click" Width="130px" OnClientClick="return disableLinkButton();"> 
                                                     <div class="link-icon">
                                                         <image src="Content/css/images/backward-solid.svg"  alt="Previous Matches"></image> Prev Match Level                                                   
                                                     </div>
                                                </asp:LinkButton>
                                            </li>
                                            <li class="leaf">
                                                 <asp:LinkButton ID="btnNextMatch" BackColor="DarkGray" Enabled="false" title="Temporarily Disable" runat="server" OnClick="btnNextMatch_Click" Width="130px" OnClientClick="return disableLinkButton();">
                                                 <div class="link-icon">
                                                     <image src="Content/css/images/forward-solid.svg"  alt="Previous Matches"></image> Next Match Level                                                   
                                                 </div>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <label colspan="2" class="labelMarginTop"><b><%=assignedUserName %></b></label>
                                        <asp:Label ID="lblSockerResponse" runat="server"></asp:Label>
                                    </div>
                                </div>

                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-4">
                                <div>
                                    
                                    <button id="btnFix" class="checkbox-btnFix" onclick="toggleCheckBtnFix(event, this)">
                                        <span class="checkbox-icon-btnFix"></span>Flag for Fix it Tool Review                                         
                                    </button>
                                    <asp:Label ID="lblFixTool" style="display: none; margin-top: 2px; font-weight:400;" runat="server">Comments:</asp:Label>
                                    <br>
                                    <asp:TextBox ID="txtFixTool" style="display: none; margin-top: -12px;" runat="server" TextMode="MultiLine" class="fixTextBox"></asp:TextBox><br>
                                    <asp:RequiredFieldValidator ID="rqFieldValudator" style="display: none; color:red;" runat="server" ErrorMessage="Required field *" ControlToValidate="txtFixTool" Enabled="false"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="admin-workarea-matching-linking">
                        <br>

                        <div class="row row-no-padding">
                            <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">

                                <% if (hdfTableKeyName.Value != "Similar Records")
                                    { %>
                                <div class="wrapper">
                                    <div class="Container Flipped">
                                        <div class="Content">
                                            <table id="mlTable" style="width: auto" border="1" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th class="sticky-unmatched" colspan="2">Candidate Case Report:
                                                                <asp:Literal Text="" ID="ltrName" runat="server" />
                                                        </th>

                                                        <% 
                                                            int counter = 2;
                                                            string value = "";
                                                            int a = 1;

                                                            if (dt != null)
                                                            {
                                                                if (dt.Columns.Count > 2)
                                                                {
                                                                    a = 2;
                                                                }
                                                                for (int i = a; i < dt.Columns.Count; i++)
                                                                {
                                                                    if (value != Convert.ToString(dt.Rows[0][i]))
                                                                    {
                                                                        if (i != a)
                                                                        { %>

                                                        <th class="sticky-matched" colspan="<%= counter %>">Possible Matches</th>
                                                        <%
                                                                value = Convert.ToString(dt.Rows[0][i]);
                                                                counter = 0;
                                                            }
                                                            if (i == dt.Columns.Count - 1)
                                                            {
                                                                counter++;%>

                                                        <th class="sticky-matched" colspan="<%= counter %>">Possible Matches</th>
                                                        <%}
                                                                else
                                                                {
                                                                    value = Convert.ToString(dt.Rows[0][i]);
                                                                    counter++;
                                                                }
                                                            }
                                                            else if (i == dt.Columns.Count - 1)
                                                            {
                                                                counter++;%>

                                                        <th class="sticky-matched" colspan="<%= counter %>">Possible Matches</th>
                                                        <%}
                                                                    else
                                                                    {
                                                                        value = Convert.ToString(dt.Rows[0][i]);
                                                                        counter++;
                                                                    }
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </thead>
                                                <tbody id="table-body">
                                                    <tr class="alt">
                                                        <% if (dt != null && dt.Rows.Count != 0)
                                                            { %>
                                                        <td class="alt">&nbsp;</td>
                                                        <%
                                                            }
                                                            else
                                                            {%>
                                                        <td>The record is not available. Please go to next record.</td>

                                                        <%} %>

                                                        <td class="blue">
                                                            <label for="chkMainTable" class="showNone">Select Case Report</label>


                                                            <%  
                                                                bool isFirstDeathChecked = false;
                                                                bool isFirstBirthChecked = false;
                                                                if (dt != null)
                                                                {
                                                                    for (int i = 2; i < dt.Columns.Count; i++)
                                                                    { %>
                                                        <td>
                                                            <%-- dt values if need to change exist 74 and 73 for this page--%>
                                                            <%  
                                                                if ((Convert.ToString(dt.Rows[0][i]) == "Birth Records") && Convert.ToString(dt.Rows[74][i]) == "Matched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "BirthRecords")
                                                                    {%>
                                                                    <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                                    <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && Convert.ToString(hdfFirstTable.Value) == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (Convert.ToString(hdfFirstTable.Value) == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Birth Records") && Convert.ToString(dt.Rows[74][i]) == "Unmatched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "BirthRecords")
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && !string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)) && (Convert.ToString(dt.Rows[4][i]) == hdfBirthRecordCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfCaseReportCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfDeathRecordCaseRecordID.Value))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && !string.IsNullOrEmpty(Convert.ToString(hdfDeathRecordCaseRecordID.Value)) && (Convert.ToString(dt.Rows[4][i]) == hdfBirthRecordCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfCaseReportCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfDeathRecordCaseRecordID.Value))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfBirthIDCurrentPageValue.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                {
                                                                    // multiple birth records col
                                                                    hdfDIDCheckedValue.Value = "true";
                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Success", "checkboxDisabled(" + Convert.ToString(dt.Rows[1][i]) + ");", true);
                                                            %>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" checked="checked" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}   // condition when multiple columns with mix match awol and nonpareil (scenarios - birthID/DeathID mis match)
                                                                else if (!string.IsNullOrEmpty(hdfBirthIDCurrentPageValue.Value) && hdfBirthIDCurrentPageValue.Value != "Nonpareil" && hdfDIDCheckedValue.Value == "true")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfBirthIDCurrentPageValue.Value) && hdfCaseReportDidMatch.Value.Contains(Convert.ToString(dt.Rows[1][i])) && hdfDIDCheckedValue.Value != "true")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && !string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else  // for true cond value="disabled"
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="disabled" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Birth Records") && Convert.ToString(dt.Rows[74][i]) == "Nonpareil")
                                                                {

                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "BirthRecords")
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                // if checked id exist in hdfDetailsID.Value 
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])) && hdfFirstTable.Value == "DeathRecords")
                                                                {
                                                                    isFirstBirthChecked = true;
                                                            %>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" checked="checked" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%
                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Success", "checkboxDisabled(" + Convert.ToString(dt.Rows[1][i]) + ");", true);
                                                                }
                                                                //RemovedControl first 
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && (!isFirstBirthChecked) && hdfFirstTable.Value == "DeathRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i])) && hdfFirstTable.Value == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" checked="checked" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}

                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Birth Records") && Convert.ToString(dt.Rows[74][i]) == "tieit")
                                                                {
                                                                    if (hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" disabled="disabled" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />

                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Death Records") && Convert.ToString(dt.Rows[74][i]) == "Matched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "DeathRecords")
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && Convert.ToString(hdfFirstTable.Value) == "DeathRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (Convert.ToString(hdfFirstTable.Value) == "DeathRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}

                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Death Records") && Convert.ToString(dt.Rows[74][i]) == "Unmatched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "DeathRecords")
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(hdfBirthRecordCaseRecordId.Value)) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && (Convert.ToString(dt.Rows[4][i]) == (Convert.ToString(hdfDeathRecordCaseRecordID.Value)) && (Convert.ToString(dt.Rows[4][i]) == (Convert.ToString(hdfCaseReportCaseRecordId.Value)) && ((Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfBirthRecordCaseRecordId.Value))))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(hdfCaseReportCaseRecordId.Value)) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && (Convert.ToString(dt.Rows[4][i]) == (Convert.ToString(hdfDeathRecordCaseRecordID.Value)) && (Convert.ToString(dt.Rows[4][i]) == (Convert.ToString(hdfCaseReportCaseRecordId.Value)) && ((Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfBirthRecordCaseRecordId.Value))))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDeathIDCurrentPageValue.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                {
                                                                    // multiple death records col
                                                                    hdfDIDCheckedValue.Value = "true";
                                                            %>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" checked="checked" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDeathIDCurrentPageValue.Value) && hdfCaseReportDidMatch.Value.Contains(Convert.ToString(dt.Rows[1][i])) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value) && hdfDeathRecordDidOnly.Value != Convert.ToString(dt.Rows[1][i]))
                                                                {
                                                            %>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDeathIDCurrentPageValue.Value) && hdfCaseReportDidMatch.Value.Contains(Convert.ToString(dt.Rows[1][i])) && hdfDIDCheckedValue.Value != "true")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && !string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="disabled" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Death Records") && Convert.ToString(dt.Rows[74][i]) == "Nonpareil")
                                                                {

                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && hdfFirstTable.Value == "DeathRecords")
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                // if checked id exist in hdfDetailsID.Value 
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])) && hdfFirstTable.Value == "BirthRecords")
                                                                {
                                                                    isFirstDeathChecked = true;
                                                            %>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" checked="checked" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%   //Add code here 
                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Success", "checkboxDisabled(" + Convert.ToString(dt.Rows[1][i]) + ");", true);
                                                                }
                                                                //RemovedControl first 
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (!isFirstDeathChecked) && hdfFirstTable.Value == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfDetailsID.Value == Convert.ToString(dt.Rows[1][i])) && hdfFirstTable.Value == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" checked="checked" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Death Records") && Convert.ToString(dt.Rows[74][i]) == "tieit")
                                                                {
                                                                    if (hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Case Reports") && Convert.ToString(dt.Rows[74][i]) == "Matched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && Convert.ToString(hdfFirstTable.Value) == "Matching_Linking")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (Convert.ToString(hdfFirstTable.Value) == "Matching_Linking")
                                                                { %>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <% }
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Case Reports") && Convert.ToString(dt.Rows[74][i]) == "Unmatched")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!hdfDetailsID.Value.Contains(",") && Convert.ToString(hdfDetailsID.Value) == Convert.ToString(dt.Rows[1][i]))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(hdfBirthRecordCaseRecordId.Value)) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && (Convert.ToString(dt.Rows[4][i]) == hdfCaseReportCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfBirthRecordCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfDeathRecordCaseRecordID.Value))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(Convert.ToString(hdfDeathRecordCaseRecordID.Value)) && !string.IsNullOrEmpty(Convert.ToString(dt.Rows[4][i])) && (Convert.ToString(dt.Rows[4][i]) == hdfCaseReportCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfBirthRecordCaseRecordId.Value) && (Convert.ToString(dt.Rows[4][i]) == Convert.ToString(hdfDeathRecordCaseRecordID.Value))))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" disabled="disabled" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfCaseRecIDCurrentPageValue.Value) && hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" checked="checked" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfCaseReportDidOnly.Value) && string.IsNullOrEmpty(hdfBirthRecordDidOnly.Value) && string.IsNullOrEmpty(hdfDeathRecordDidOnly.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfCaseRecIDCurrentPageValue.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" disabled="disabled" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="disabled" />
                                                            <%}
                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Case Reports") && Convert.ToString(dt.Rows[74][i]) == "Nonpareil")
                                                                {
                                                                    if (!string.IsNullOrEmpty(hdfisFirstCheckedDetailsID.Value) && Convert.ToString(hdfisFirstCheckedDetailsID.Value) == Convert.ToString(dt.Rows[1][i]) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfDetailsID.Value))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (hdfisCheckedCR.Value == "true")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && hdfFirstTable.Value == "BirthRecords")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfFirstTable.Value == "DeathRecords"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else if (!string.IsNullOrEmpty(hdfDetailsID.Value) && (hdfFirstTable.Value == "CaseReports" || hdfFirstTable.Value == "Matching_Linking"))
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" checked="checked" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}

                                                                }
                                                                else if ((Convert.ToString(dt.Rows[0][i]) == "Case Reports") && Convert.ToString(dt.Rows[74][i]) == "tieit")
                                                                {
                                                                    if (hdfDetailsID.Value.Contains(Convert.ToString(dt.Rows[1][i])))
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                }
                                                                else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>
                                                            <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" onclick="myFunction()" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}
                                                                else
                                                                {
                                                                    {%>
                                                            <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="myFirstClickFunction('<%=hdfTableName.Value %>')" value="<%=dt.Rows[1][i] %>" title="<%=dt.Rows[74][i] %>" />
                                                            <%}%>


                                                            <%}%>
                                                        </td>
                                                        <% }
                                                            if (dt.Columns.Count == 2)
                                                            {
                                                                string message = "";

                                                                if (hdfTableName.Value == "DeathRecords")
                                                                {
                                                                    message = "Unfortunately, the system couldn't find the data you requested.";

                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Data error", "isDataNull();", true);
                                                                }
                                                                else if (hdfTableName.Value == "BirthRecords")
                                                                {
                                                                    message = "Unfortunately, the system couldn't find the data you requested.";
                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Data error", "isDataNull();", true);
                                                                }
                                                                else
                                                                {
                                                                    message = "Unfortunately, the system couldn't find the data you requested.";
                                                                    ClientScript.RegisterStartupScript(this.GetType(), "Data error", "isDataNull();", true);
                                                                }
                                                        %>
                                                        <td>
                                                            <label><b><%=message %></b></label>
                                                        </td>
                                                        <%}
                                                        %>
                                                    </tr>
                                                    <tr>
                                                        <%-- below 1st grey column data--%>

                                                        <td class="alt">Details </td>

                                                        <%--below blue column data--%>

                                                        <% for (int i = 1; i <= 1; i++)
                                                            {
                                                        %>
                                                        <td class="blue">

                                                            <button id="btndeepstat" title="Check the match details for the possible match" onclick="return deepstat('deepstats.aspx?pagecall=clearview&reporttype=<%=hdfTableName.Value %>&archiveid=<%=Uri.EscapeDataString(hdfArchiveID.Value)%>')" target="_blank" width="120px">
                                                                <div class="link-icon">
                                                                    <image src="content/css/images/info-circle.svg" alt="Match details"></image>
                                                                    <strong>Match Details</strong>
                                                                </div>
                                                            </button>

                                                        </td>

                                                        <% }
                                                        %>
                                                        <%--Display Logic for items --%>

                                                        <% for (int i = 2; i < dt.Columns.Count; i++)
                                                            {

                                                                hdfMatchDetailId.Value = Convert.ToString(dt.Rows[1][i]);

                                                                foreach (System.Data.DataRow item in dt.Rows)
                                                                {

                                                                    if (Convert.ToString(item[0]) == "Case Record ID")
                                                                    { %>
                                                                    <td>
                                                                    <% if (hdfTableName.Value == "BirthRecords")
                                                                        {  %>
                                                                    <div>&nbsp;</div>
                                                                    <%}  // phantom hit condtion - which is hidden and row 3
                                                                        else if (!string.IsNullOrEmpty(Convert.ToString(item[i])) && (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[73][i])) && Convert.ToString(dt.Rows[73][i]) != "0"))
                                                                        {  %>
                                                                    <button id="btnFullView" title="View all matched case reports for the case record" onclick="ShowProgress(this); RedirectFullView('<%=item[i]%>');" target="_blank" width="120px">
                                                                        <div class="link-icon">
                                                                            <image src="Content/css/images/info-circle.svg" alt="View All Matches"></image>
                                                                            <strong>All Matches</strong>
                                                                        </div>
                                                                    </button>
                                                                    <asp:Button ID="btnFullViewDetails" runat="server" OnClick="btnFullViewDetails_Click" Text="Full View Details" Visible="false" />
                                                                    <%}
                                                                    else
                                                                    { %>
                                                                <button id="btnFullView1" title="Case Record ID is Null or Phantom Hit is false" width="120px" disabled="disabled">
                                                                    <div class="link-icon">
                                                                        <image src="Content/css/images/info-circle.svg" alt="View All Matches"></image>
                                                                        <strong>All Matches</strong>
                                                                    </div>
                                                                </button>
                                                            <%} %>
                                                        </td>
                                                        <%           break;
                                                                    }
                                                                }
                                                            }
                                                            if (dt.Columns.Count == 2)
                                                            {
                                                        %>
                                                        <td>&nbsp;</td>
                                                        <%} %>
                                                    </tr>

                                                    <% foreach (System.Data.DataRow item in dt.Rows)
                                                        {
                                                            if (Convert.ToString(item[0]) != "RecordFrom" && Convert.ToString(item[0]) != "IsMatchRecord" && Convert.ToString(item[0]) != "Phantom Hit" && !string.IsNullOrEmpty(Convert.ToString(item[0])))
                                                            {%>
                                                    <tr>

                                                        <% if (Convert.ToString(item[0]) == "Case Record ID" || Convert.ToString(item[0]) == "Trip")
                                                            {%>

                                                                <td class="alt"><b><%=item[0] %></b> </td>

                                                            <%}
                                                            else
                                                            { %>

                                                                 <td class="alt"><%=item[0] %> </td>
                                                        <%}%>

                                                        <td class="blue">

                                                            <%
                                                                if (Convert.ToString(item[0]) == "Trip")
                                                                {
                                                                    int tripValue = Convert.ToInt32(item[1]);

                                                                    if (tripValue == 0)
                                                                    {%>
                                                                    <span><b>1</b></span>
                                                                    <%}
                                                                    else if (tripValue >= 1)
                                                                    {%>
                                                                    <span class="span-red-link"><b>2</b></span>
                                                                    <%}
                                                                }
                                                                else if (Convert.ToString(item[0]) == "Match Level")
                                                                {
                                                                    int matchLevelValue = Convert.ToInt32(item[1]);

                                                                    if (matchLevelValue >= 0)
                                                                    {%>
                                                                    <span></span>
                                                                    <%}
                                                                }
                                                                else
                                                                {%>
                                                                <%= item[1] %>
                                                                <%}
                                                                %>

                                                        </td>
                                                        <% for (int i = 2; i < dt.Columns.Count; i++)
                                                            {  
                                                                string itemValue = item[i].ToString();
                                                                string scrutinizerId = hdfScrutinizerID.Value;
                                                           
                                                                if (Convert.ToString(item[0]) == "Case Record ID")
                                                                {%><td> <%

                                                                    string returnPage = !string.IsNullOrEmpty(Request.QueryString["returnpage"]) ? Convert.ToString(Request.QueryString["returnpage"]) : "";
                                                                    string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";


                                                                    string hyperlinkedCaseRecID = !string.IsNullOrEmpty(Request.QueryString["hyperlinkedCaseRecID"]) ? Convert.ToString(Request.QueryString["hyperlinkedCaseRecID"]) : "";


                                                                    if (!string.IsNullOrEmpty(returnPage) && returnPage == "FullView" && Convert.ToString(item[i]) == caseRecId)
                                                                    {
                                                                    %>
                                                                        <span class="span-red-link"><b><%=item[i]%></b></span>                                                              

                                                                     <%
                                                                    }
                                                                    else
                                                                    {%>

                                                                    <b><%=item[i]%></b>                                                              

                                                                <%}
                                                                if (!string.IsNullOrEmpty(Convert.ToString(item[i])))
                                                                { %>
                                                                    <asp:Button ID="btnLinkCaseReports" runat="server" OnClick="btnLinkCaseReports_Click" Visible="false" CssClass="linkhidden" />
                                                                   
                                                                    <input type="image" src="Content/css/images/info-circle-fill.svg" title="View Linked Case Reports" onclick="return linkedCaseRecordDetails('MatchingLinkingCaseReportsViewLinkDetails.aspx?Schema=MBDR_System&Table=CaseReportsView&keyName=caseId&caseRecId=<%=item[i] %>')" target="_blank" />

                                                                    <%--Case Reports Details--%>
                                                                    <input type="hidden" id="hdfcaseRecId_<%=Convert.ToString(dt.Rows[1][i]) %>" name="hdfcaseRecId" value="<%=Convert.ToString(dt.Rows[1][i]) + "_" + Convert.ToString(item[i])%>" />
                                                                    <button id="btnLinkedCaseRecordDetails" title="Case Record Details" onclick="return linkedCaseRecordDetails('MLLinkedCaseRecordsViewDetails.aspx?Schema=MBDR_System&Table=CaseRecordsView&keyName=caseId&caseRecId=<%=item[i] %>')" target="_blank" width="120px">
                                                                        <div class="link-icon">
                                                                            Case Record Details                                                                
                                                                        </div>
                                                                    </button>
                                                                 <%}

                                                                else
                                                                { %>
                                                               <input type="hidden" id="hdfcaseRecId" name="hdfcaseRecId" value="" />

                                                                <input type="image" src="Content/css/images/info-circle-fill.svg" title="View Linked Case Reports" disabled="disabled" hidden="hidden" value="Linked Case Reports" />
                                                                  
                                                               <% }
                                                                %> </td> <%
                                                                }
                                                                  

                                                                if (Convert.ToString(item[0]) == "Case Report ID" && i >= 2)
                                                                {
                                                                %>
                                                              <td>  <input type="hidden" id="hdfCaseReportID_<%=Convert.ToString(dt.Rows[1][i]) %>" class="chkReportHiddenId" name="hidden" value="<%=Convert.ToString(item[i]) + "_" + Convert.ToString(dt.Rows[1][i]) %>" />
                                                                <b><u><a href="#" onclick="SideBySide('MLSideBySideView.aspx?scrutinizerId=<%=scrutinizerId %>&columnname=reportId&columnvalue=<%=item[i] %>&reportType=<%=hdfTableKeyName.Value %>'); return false;"><%=itemValue %></a></u></b>
                                                               </td> <%}

                                                                if (Convert.ToString(item[0]) == "Birth Rec No." && i >= 2)
                                                                {
                                                                %>
                                                              <td>  <input type="hidden" id="hdfBirthReportID_<%=Convert.ToString(dt.Rows[1][i]) %>" class="chkBirthHiddenId" name="hidden" value="<%=Convert.ToString(item[i]) + "_" + Convert.ToString(dt.Rows[1][i]) %>" />
                                                                <b><u><a href="#" onclick="SideBySide('MLSideBySideView.aspx?scrutinizerId=<%=scrutinizerId %>&columnname=masterRecordNumber&columnvalue=<%=item[i] %>&reportType=<%=hdfTableKeyName.Value %>'); return false;"><%=itemValue %></a></u></b>
                                                               </td> <%}

                                                                if (Convert.ToString(item[0]) == "Death Rec No." && i >= 2)
                                                                {
                                                                %>
                                                             <td>   <input type="hidden" id="hdfDeathReportID_<%=Convert.ToString(dt.Rows[1][i]) %>" name="hidden" class="chkDeathHiddenId" value="<%=Convert.ToString(item[i]) + "_" + Convert.ToString(dt.Rows[1][i]) %>" />
                                                                <b><u><a href="#" onclick="SideBySide('MLSideBySideView.aspx?scrutinizerId=<%=scrutinizerId %>&columnname=deathNumber&columnvalue=<%=item[i] %>&reportType=<%=hdfTableKeyName.Value %>'); return false;"><%=itemValue %></a></u></b>
                                                              </td>  <%}

                                                                if (Convert.ToString(item[0]) == "Trip" && i >= 2)
                                                                {
                                                                    int tripValue = Convert.ToInt32(item[i]);

                                                                    if (tripValue == 0)
                                                                    {%>
                                                                  <td> <span><b></b></span></td> 
                                                                    <%}
                                                                else if (tripValue >= 1)
                                                                {%>
                                                            <td><span class="span-red-link"><b></b></span><%--Do not show the trip values in possible matches - Converting to null value--%>
                                                            </td><%}
                                                                }

                                                                else if (Convert.ToString(item[0]) != "Case Report ID" && Convert.ToString(item[0]) != "Case Record ID" && Convert.ToString(item[0]) != "Birth Rec No." && Convert.ToString(item[0]) != "Death Rec No." && Convert.ToString(item[0]) != "Trip")
                                                                {
                                                                    string existingItemValue = Convert.ToString(item[1]).ToLower();
                                                                    string currentitemValue = item[i].ToString().ToLower();
                                                                    if (existingItemValue != currentitemValue && Convert.ToString(item[0]) != "Detail ID"
                                                                                                            && item[0].ToString() != "Scrutinizer ID" 
                                                                                                            && Convert.ToString(item[0]) != "Archive ID" 
                                                                                                            && Convert.ToString(item[0]) != "Case Report ID" 
                                                                                                            && Convert.ToString(item[0]) != "Case Record ID" 
                                                                                                            && Convert.ToString(item[0]) != "Birth Rec No." 
                                                                                                            && Convert.ToString(item[0]) != "Death Rec No." 
                                                                                                            && Convert.ToString(item[0]) != "Birth Certificate Number" 
                                                                                                             && Convert.ToString(item[0]) != "Confidence" 
                                                                                                            && Convert.ToString(item[0]) != "Match Level" 
                                                                                                            && Convert.ToString(item[0]) != "Trip" 
                                                                                                            && Convert.ToString(item[0]) != "Received Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Processed Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Assimilated Timestamp"
                                                                                                            && Convert.ToString(item[0]) != "Search Context"
                                                                                                            && Convert.ToString(item[0]) != "messageState"
                                                                                                            && Convert.ToString(item[0]) != "Historic ID")
                                                                    {%>
                                                                      <td class="pinkCellSimilar"> 
                                                                      <b><%=item[i] %></b> </td>
                                                                    <%}
                                                                    else
                                                                    {  %>

                                                                   <td> <%=item[i] %></td>
                                                                    <% }
                                                                    }

                                                            %>    

                                                        <% }

                                                            if (dt.Columns.Count == 2)
                                                            {
                                                        %>
                                                        <td>&nbsp;</td>
                                                        <%} %>
                                                    </tr>
                                                    <% }

                                                            }
                                                        }
                                                    %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <%}
                                    else
                                    { %>
                                <div class="Container Flipped">
                                    <div class="Content">
                                        <table style="width: 100%" border="1" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="sticky-unmatched" colspan="2">Primary Candidate :
                                                        <asp:Literal Text="" ID="ltrSimilarName" runat="server" />
                                                    </th>
                                                    <% 
                                                        int counter = 2;
                                                        string value = "";
                                                        int a = 1;

                                                        if (dt != null)
                                                        {
                                                            if (dt.Columns.Count > 2)
                                                            {
                                                                a = 2;
                                                            }
                                                            for (int i = a; i < dt.Columns.Count; i++)
                                                            {
                                                                if (value != Convert.ToString(dt.Rows[0][i]))
                                                                {
                                                                    if (i != a)
                                                                    { %>
                                                    <th class="sticky-matched" style="left: 0px;TEXT-ALIGN: left;position: static;" colspan="<%= counter %>">Similar Candidates</th>
                                                    <%
                                                            value = Convert.ToString(dt.Rows[0][i]);
                                                            counter = 0;
                                                        }
                                                        if (i == dt.Columns.Count - 1)
                                                        {
                                                            counter++;%>
                                                    <th class="sticky-matched" style="left: 0px;TEXT-ALIGN: left;position: static;" colspan="<%= counter %>">Similar Candidates</th>
                                                    <%}
                                                            else
                                                            {
                                                                value = Convert.ToString(dt.Rows[0][i]);
                                                                counter++;
                                                            }
                                                        }
                                                        else if (i == dt.Columns.Count - 1)
                                                        {
                                                            counter++;%>

                                                    <th class="sticky-matched" style="left: 0px;TEXT-ALIGN: left;position: static;" colspan="<%= counter %>">Similar Candidates</th>
                                                    <%}
                                                                else
                                                                {
                                                                    value = Convert.ToString(dt.Rows[0][i]);
                                                                    counter++;
                                                                }
                                                            }
                                                        }


                                                    %>
                                                </tr>
                                            </thead>
                                            <tbody id="table-body">
                                                <tr class="alt">
                                                    <td class="alt">&nbsp;
                                                    </td>
                                                    <td class="blue" style="display: flex;">
                                                        <div style="margin: 0px 0px 0px 105px;">


                                                            <% if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                                {%>

                                                            <input type="button" id="chkId1" disabled="disabled" class="chkHeader selectAllSimilar" name="chkname" value="Select All" />
                                                            <%}
                                                                else if (string.IsNullOrEmpty(hdfSimilarCheckUnCheckSIDs.Value))
                                                                { %>

                                                            <input type="button" id="chkId1" value="Select All" class="chkHeader selectAllSimilar" name="chkname" onclick="CheckAllSIdsFunction()" />

                                                            <%}

                                                                else
                                                                {%>

                                                            <input type="button" id="chkId1" class="chkHeader selectAllSimilar" value="Unselect All" name="chkname" onclick="CheckAllSIdsFunction()" />

                                                            <%}%>
                                                        </div>
                                                    </td>
                                                    <%  if (dt != null)
                                                        {
                                                            for (int i = 2; i < dt.Columns.Count; i++)
                                                            {
                                                                if (!string.IsNullOrEmpty(hdfSimilarCheckUnCheckSIDs.Value) && hdfSimilarCheckUnCheckSIDs.Value.Contains(Convert.ToString(dt.Rows[2][i])))
                                                                {
                                                    %>
                                                    <td>
                                                        <input type="checkbox" id="chkId<%=i%>" checked="checked" class="chkHeader" name="chkname" onclick="CheckUncheckIndivisualSIdsFunction('<%=dt.Rows[2][i] %>')" value="<%=dt.Rows[2][i] %>" title="<%=dt.Rows[2][i] %>" />

                                                    </td>
                                                    <%
                                                        }
                                                        else if (!string.IsNullOrEmpty(hdfOwner.Value) && Convert.ToString(hdfOwner.Value) == "false")
                                                        {%>
                                                    <td>
                                                        <input type="checkbox" id="chkId<%=i%>" disabled="disabled" class="chkHeader" name="chkname" value="<%=dt.Rows[2][i] %>" title="<%=dt.Rows[2][i] %>" />
                                                    </td>
                                                    <%}
                                                        else
                                                        {
                                                    %>
                                                    <td>
                                                        <input type="checkbox" id="chkId<%=i%>" class="chkHeader" name="chkname" onclick="CheckUncheckIndivisualSIdsFunction('<%=dt.Rows[2][i] %>')" value="<%=dt.Rows[2][i] %>" title="<%=dt.Rows[2][i] %>" />

                                                    </td>
                                                    <% }
                                                        }
                                                    %>
                                                </tr>

                                                <% foreach (System.Data.DataRow item in dt.Rows)
                                                    {
                                                        if (Convert.ToString(item[0]) != "RecordFrom" && Convert.ToString(item[0]) != "IsMatchRecord" && Convert.ToString(item[0]) != "Phantom Hit" && !string.IsNullOrEmpty(Convert.ToString(item[0])))
                                                        {%>
                                                <tr>

                                                    <% if (Convert.ToString(item[0]) == "Case Record ID" || Convert.ToString(item[0]) == "Trip")
                                                        {%>

                                                    <td class="alt"><b><%=item[0] %></b> </td>

                                                    <%}
                                                        else
                                                        { %>

                                                    <td class="alt"><%=item[0] %> </td>
                                                    <%}%>

                                                    <td class="blue">
                                                        <%= item[1] %>
                                                              
                                                    </td>

                                                            <% for (int i = 2; i < dt.Columns.Count; i++)
                                                        
                                                                {%>

                                                                    <%  string existingItemValue = Convert.ToString(item[1]).ToLower();
                                                                        string currentitemValue = item[i].ToString().ToLower();
                                                                        string scrutinizerId = hdfScrutinizerID.Value;

                                                                        if (existingItemValue != currentitemValue && Convert.ToString(item[0]) != "Detail ID"
                                                                            && item[0].ToString() != "Scrutinizer ID"
                                                                            && Convert.ToString(item[0]) != "Archive ID"
                                                                            && Convert.ToString(item[0]) != "Case Report ID"
                                                                            && Convert.ToString(item[0]) != "Case Record ID"
                                                                            && Convert.ToString(item[0]) != "Birth Rec No."
                                                                            && Convert.ToString(item[0]) != "Death Rec No."
                                                                            && Convert.ToString(item[0]) != "Birth Certificate Number"
                                                                            && Convert.ToString(item[0]) != "Confidence"
                                                                            && Convert.ToString(item[0]) != "Match Level"
                                                                            && Convert.ToString(item[0]) != "Trip"
                                                                            && Convert.ToString(item[0]) != "Received Timestamp"
                                                                            && Convert.ToString(item[0]) != "Processed Timestamp"
                                                                            && Convert.ToString(item[0]) != "Assimilated Timestamp"
                                                                            && Convert.ToString(item[0]) != "Search Context"
                                                                            && Convert.ToString(item[0]) != "messageState"
                                                                            && Convert.ToString(item[0]) != "Historic ID"
                                                                            )
                                                                        {%><td class="pinkCellSimilar"><b><%=item[i]%></b>  </td><%}
                                                                        else
                                                                        {%> <td><%=item[i]%></td><%} 
                                                                    %>
                                                              <%}

                                                                if (dt.Columns.Count == 2)
                                                                {%><td>&nbsp;</td><%} 
                                                           %>
                                                </tr>
                                                <%}

                                                        }
                                                    }
                                                %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <%}  %>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="errorMsgs">
        <p id="errorMsgsForAll"></p>
    </div>
    <div class="zoomPosition" style="display:none;">
        <strong>-</strong>
        <input type="range" disabled="disabled" id="zoomRange" min="50" max="200" value="160" step="10">
        <strong>+</strong>
    </div>

    <div id="dialog" class="modal" title="Selection Alert">
        <p id="message"></p>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="socketConnError" class="socketConnErrorModal">
        <p id="connErrorMsg"></p>
    </div>

    <div id="socketConn">
        <p id="connSuccessMsg"></p>
    </div>

    <div id="dialogAssign" class="modal" title="Selection Alert">
        <p id="messageAssign"></p>
    </div>

    <div id="dialogRelease" class="modal" title="Selection Alert">
        <p id="messageRelease"></p>
    </div>

    <div id="dialogLink" class="modal" title="Link Alert">
        <p id="messageLinkShow"></p>
        <p id="messageLink"></p>
    </div>

    <div id="dialogHyperLink" class="modal">
        <p id="messageHyperLink"></p>
    </div>

    <div id="successDiagBox">
        <p id="successDiagBoxMsg"></p>
    </div>

    <div id="endOfWOrk">
        <p id="endOfWOrkMsg"></p>
    </div>

    <div id="dtNull">
        <p id="dtNullMessage"></p>
    </div>

    <div id="commonMsg">
    </div>

        <div id="securityMsg">
        </div>

    <% }%>

    <script>

        function clearBGD() {

            var headerContent = document.getElementById("mlsSecurityHeader");
            var bodyContent = document.getElementById("mlsSecurityBody");

            if (headerContent) {
                headerContent.style.display = "none";
            }
            if (bodyContent) {
                bodyContent.style.display = "none";
            }
            sessionStorage.clear();
        }

        function disableLinkButton() {

            return false;
        }

        window.addEventListener('scroll', function () {

            var scrollLeft = window.screenX || document.documentElement.scrollLeft;
            document.querySelectorAll('.sticky-matched').forEach(function (element) {
                element.style.left = -scrollLeft + 'px';
            });
        });

        function hyerLinkedCaseRecId(hyperlinkedCaseRecID) {
            var value = hyperlinkedCaseRecID;

            $(function () {
                var connErrorMsg = 'Matching and Linking - Possible Source Matches :: ';
                var dialogContent = connErrorMsg + "<b>" + value + "</b>";

                $("#dialogHyperLink").html(dialogContent);

                $("#dialogHyperLink").dialog({
                    width: "90%",
                    height: 600,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Matching and linking - Side by Side View Candidate",
                    open: function () {
                        // Capture the table value
                        var tableValue = $("#mcbdTable")[0].outerHTML;

                        // Print the table value within the modal
                        $("#messageHyperLink").html(tableValue);
                    },
                    buttons: [
                        {
                            text: "Done",
                            open: function () {
                                $(this).addClass('okcls');
                            },
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ],
                });
            });
        }

        const table = document.getElementById('mlTable');
        const zoomRange = document.getElementById('zoomRange');

        zoomRange.addEventListener('input', function () {
            const zoomLevel = zoomRange.value;
            table.style.transform = `scale(${zoomLevel / 100})`;
            table.style.transformOrigin = 'top left';
        });

        function Redirect(id) {
            document.getElementById("<%=hdflinkCaseReport.ClientID %>").value = id;
            <%= Page.ClientScript.GetPostBackEventReference(btnLinkCaseReports,string.Empty)  %>
        };

        function RedirectFullView(id) {
            document.getElementById("<%=hdfFullViewDetails.ClientID %>").value = id;
            <%= Page.ClientScript.GetPostBackEventReference(btnFullViewDetails,string.Empty)  %>
        };

        function mySessionClear() {
            var someVarName = sessionStorage.removeItem("detailsId");
            sessionStorage.clear();
            var someVarName = sessionStorage.removeItem("caseDetailId");
            sessionStorage.clear();
        }

        function ShowProgress(btnEle) {

            if (btnEle && $(btnEle).hasClass('enableGrey')) {
                return;
            }
            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");
            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function CheckBoxFunction() {

            var inputElements = document.getElementsByClassName('chkHeader');
            for (var k = 0; inputElements[k]; ++k) {
                if (inputElements[k].checked) {
                    var checkboxes = document.querySelectorAll("input[type=checkbox]");
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].value != inputElements[k].value) {
                            for (var j = 0; j < checkboxes.length; j++) {
                                if (checkboxes[j] != inputElements[k]) {
                                    checkboxes[j].disabled = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        function isDataNull() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).removeAttr('href');
            $(btnAssign).removeClass('disableGrey');
            $(btnAssign).addClass('enableGrey');

            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID %>");
            $(btnPre).removeAttr('href');
            $(btnPre).removeClass('disableGrey');
            $(btnPre).addClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID %>");
            $(btnNext).removeAttr('href');
            $(btnNext).removeClass('disableGrey');
            $(btnNext).addClass('enableGrey');
        }

        function greyOutRelPreNext() {
            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID %>");
            $(btnPre).removeAttr('href');
            $(btnPre).removeClass('disableGrey');
            $(btnPre).addClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID %>");
            $(btnNext).removeAttr('href');
            $(btnNext).removeClass('disableGrey');
            $(btnNext).addClass('enableGrey');

            var btnRelease = document.getElementById("<%=btnRelease.ClientID %>");
            $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
            $(btnRelease).removeAttr('disabled');
            $(btnRelease).addClass('disableGrey');
            $(btnRelease).removeClass('enableGrey');

        }

        // only link enable rest will be grey out
        function btnLinkEnable() {

            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).removeClass('disableGrey');
            $(btnAssign).addClass('enableGrey');

            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID %>");
            $(btnPre).removeAttr('href');
            $(btnPre).removeClass('disableGrey');
            $(btnPre).addClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID %>");
            $(btnNext).removeAttr('href');
            $(btnNext).removeClass('disableGrey');
            $(btnNext).addClass('enableGrey');

            var btnRelease = document.getElementById("<%=btnRelease.ClientID %>");
            $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
            $(btnRelease).removeAttr('disabled');
            $(btnRelease).addClass('disableGrey');
            $(btnRelease).removeClass('enableGrey');

            var btnLink = document.getElementById("<%=btnLink.ClientID %>");
            $(btnLink).attr('href', "javascript: __doPostBack('ctl00$Content$btnLink', '')");
            $(btnLink).removeClass('enableGrey');
            $(btnLink).addClass('disableGrey');
        }

        //only create enable rest will be grey out
        function btnCreateEnable() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).attr('href', "javascript: __doPostBack('ctl00$Content$btnAssign', '')");
            $(btnAssign).removeClass('enableGrey');
            $(btnAssign).addClass('disableGrey');

            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID %>");
            $(btnPre).removeAttr('href');
            $(btnPre).removeClass('disableGrey');
            $(btnPre).addClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID %>");
            $(btnNext).removeAttr('href');
            $(btnNext).removeClass('disableGrey');
            $(btnNext).addClass('enableGrey');

            var btnRelease = document.getElementById("<%=btnRelease.ClientID %>");
            $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
            $(btnRelease).removeAttr('disabled');
            $(btnRelease).addClass('disableGrey');
            $(btnRelease).removeClass('enableGrey');

            var btnLink = document.getElementById("<%=btnLink.ClientID %>");
            $(btnLink).removeClass('disableGrey');
            $(btnLink).addClass('enableGrey');
        }

        // link enable between Link/Create - create will be grey out
        function btnLinkEnableBtw() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).removeClass('disableGrey');
            $(btnAssign).addClass('enableGrey');

            var btnLink = document.getElementById("<%=btnLink.ClientID %>");
            $(btnLink).attr('href', "javascript: __doPostBack('ctl00$Content$btnLink', '')");
            $(btnLink).removeClass('enableGrey');
            $(btnLink).addClass('disableGrey');
        }

        // Create enable between Link/Create - Link will be grey out
        function btnCreateEnableBtw() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).attr('href', "javascript: __doPostBack('ctl00$Content$btnAssign', '')");
            $(btnAssign).removeClass('enableGrey');
            $(btnAssign).addClass('disableGrey');

            var btnLink = document.getElementById("<%=btnLink.ClientID %>");
            $(btnLink).removeClass('disableGrey');
            $(btnLink).addClass('enableGrey');
        }

        // default greyout and non greyout buttons
        function btnDefaults() {
            var btnAssign = document.getElementById("<%=btnAssign.ClientID %>");
            $(btnAssign).attr('href', "javascript: __doPostBack('ctl00$Content$btnAssign', '')");
            $(btnAssign).removeAttr('disabled');
            $(btnAssign).addClass('disableGrey');
            $(btnAssign).removeClass('enableGrey');
            var btnPre = document.getElementById("<%=btnPreviousMatch.ClientID %>");
            $(btnPre).attr('href', "javascript: __doPostBack('ctl00$Content$btnPreviousMatch', '')");
            $(btnPre).removeAttr('disabled');
            $(btnPre).addClass('disableGrey');
            $(btnPre).removeClass('enableGrey');

            var btnNext = document.getElementById("<%=btnNextMatch.ClientID %>");
            $(btnNext).attr('href', "javascript: __doPostBack('ctl00$Content$btnNextMatch', '')");
            $(btnNext).removeAttr('disabled');
            $(btnNext).addClass('disableGrey');
            $(btnNext).removeClass('enableGrey');

            var btnRelease = document.getElementById("<%=btnRelease.ClientID %>");
            $(btnRelease).attr('href', "javascript: __doPostBack('ctl00$Content$btnRelease', '')");
            $(btnRelease).removeAttr('disabled');
            $(btnRelease).addClass('disableGrey');
            $(btnRelease).removeClass('enableGrey');

            var btnLink = document.getElementById("<%=btnLink.ClientID %>");
            $(btnLink).removeAttr('href');
            $(btnLink).removeClass('disableGrey');
            $(btnLink).addClass('enableGrey');
        }

        /*For similar SIDS**/

        function checkedSIDs(checkedValue) {

            var collectSIDS = sessionStorage.getItem("SIds");
            if (!collectSIDS) {
                collectSIDS = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
            }

            if (collectSIDS) {
                if (!collectSIDS.includes(checkedValue)) {
                    collectSIDS = collectSIDS + "," + checkedValue;
                }
            }
            else {
                collectSIDS = checkedValue;
            }
            sessionStorage.setItem("SIds", collectSIDS);
            document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value = collectSIDS;
        }

        function UncheckedSIDs(UncheckedValue) {

            var collectSIDS = sessionStorage.getItem("SIds");
            if (collectSIDS) {
                var values = collectSIDS.split(",");
                var newValues = null;
                for (var j = 0; j < values.length; j++) {
                    if (values[j]) {
                        if (values[j] !== UncheckedValue) {
                            if (newValues) {
                                newValues = newValues + "," + values[j];
                            }
                            else {
                                newValues = values[j];
                            }
                        }
                    }
                }

                if (newValues == null) {
                    sessionStorage.setItem("SIds", "");
                    document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value = null;
                }
                else {
                    collectSIDS = sessionStorage.removeItem("SIds");
                    sessionStorage.clear();
                    collectSIDS = newValues;
                    sessionStorage.setItem("SIds", collectSIDS);
                    document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value = collectSIDS;
                }
            }
            else {
                document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value = null;
            }
        }

        /*check uncheck all SIDs from primarySID blue col*/
        function CheckAllSIdsFunction() {

            var button = document.getElementById("chkId1");
            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            
            if (button.value == "Select All") {
                
                for (var j = 0; j < checkboxes.length; j++) {
                    if (checkboxes[j] != this) {
                        checkboxes[j].checked = true;
                        checkedSIDs(checkboxes[j].value);
                        button.value = "Unselect All";
                    }
                }
            } else {
                
                for (var j = 0; j < checkboxes.length; j++) {
                    if (checkboxes[j] != this) {
                        checkboxes[j].checked = false;
                        UncheckedSIDs(checkboxes[j].value);
                        button.value = "Select All";
                    }
                }
            }
            let checkedDIDfromCR;
            if ("<%=hdfisCheckedBR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    }
                }
            }
            else if ("<%=hdfisCheckedDR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    }
                }
            }
            else if ("<%=hdfisCheckedCR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    }
                }
            }
            else {
                checkedDIDfromCR = null;
            }

            var checkIds = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
            var dids = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
            if (checkedDIDfromCR) {
                btnLinkEnable();
            } else if (checkIds || dids) {
                btnCreateEnable();
            }
            else {
                btnDefaults();
            }
        }

        /*check/uncheck indiviusal from similarSIDs*/
        function CheckUncheckIndivisualSIdsFunction(checkedvalue) {

            var button = document.getElementById("chkId1");
            button.value = "Select All"; // if indiviusal unselect change label
            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            var indChecked = false;

            for (var j = 0; j < checkboxes.length; j++) {
                if (checkboxes[j].value == checkedvalue) {
                    if (checkboxes[j].checked) {
                        checkedSIDs(checkboxes[j].value);
                        indChecked = true;
                    }
                    else {
                        UncheckedSIDs(checkboxes[j].value);
                        indChecked = false;
                    }
                }
            }

            if (indChecked) {

                button.value = "Unselect All";
            }
            else {
                button.value = "Select All";
            }

            let checkedDIDfromCR;
            if ("<%=hdfisCheckedBR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    }
                }
            }
            else if ("<%=hdfisCheckedDR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    }
                }
            }
            else if ("<%=hdfisCheckedCR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        }
                    }
                }
            }
            else {
                checkedDIDfromCR = null;
            }
            var checkIds = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
            var dids = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
            if (checkedDIDfromCR) {
                btnLinkEnable();
            } else if (checkIds || dids) {
                btnCreateEnable();
            }
            else {
                btnDefaults();
            }
        }

        /*For similar SIDS**/

        window.onload = function () {
            
            var hdffixTool = document.getElementById("<%=hdffixTool.ClientID%>");
            if (hdffixTool.value == "true") {
                var event = new Event('click'); // simulating a click event
                toggleCheckBtnFix(event, 'btnFix');
            }

            var sidOwner = "<%= isOwnerID%>";
            var sessionUserId = "<%= Session["userid"]%>";
            var btnFix = document.getElementById("btnFix");

            if (sidOwner == sessionUserId)
            {
                btnFix.disabled = false;
                btnFix.classList.remove('btnFix-cursor-default');
            }
            else
            {
                btnFix.disabled = true;
                btnFix.classList.add('btnFix-cursor-default');
            }

            var isChecked = sessionStorage.getItem("isCheckCheckedBox");
            var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
            var awolBirthVar = document.getElementById("<%=hdfBirthIDCurrentPageValue.ClientID %>").value;
            var awolDeathVar = document.getElementById("<%=hdfDeathIDCurrentPageValue.ClientID %>").value;
            var checksimilarIds = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
            var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;
            var checkedDIDfromCR = document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value;
            //First CheckBox checked... 
            var firstCheckeddetailsID = document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value;
            if (firstCheckeddetailsID) {
                document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = firstCheckeddetailsID;
            }

            if ("<%=hdfisCheckedBR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    }
                }

            }
            else if ("<%=hdfisCheckedDR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedCR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    }
                }
            }
            else if ("<%=hdfisCheckedCR.Value %>" == "true") {
                if (!checkedDIDfromCR) {
                    checkedDIDfromCR = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    if (!checkedDIDfromCR && "<%=hdfisCheckedBR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                            checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        }
                    } else if (!checkedDIDfromCR && "<%=hdfisCheckedDR.Value %>" == "true") {
                        checkedDIDfromCR = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    }
                }
            }

            if (hdfDetailId) {

                //if match records like birth/Death 

                var someVarName = sessionStorage.getItem("detailsId");
                if (hdfDetailId.includes(',') && someVarName && !someVarName.includes(',')) {
                    sessionStorage.setItem("detailsId", hdfDetailId);
                }
                sessionStorage.setItem("isCheckCheckedBox", "true");

            } else {
                sessionStorage.setItem("isCheckCheckedBox", "false");
            }

            isChecked = sessionStorage.getItem("isCheckCheckedBox");
            if (isChecked == "null" || isChecked == null) {
                isChecked = "false";
            }

            if (isChecked === "true") {

                greyOutRelPreNext();

                // handle checkbox flagged for Create a New Case or Link
                var checkedValue = null;
                var hdfFirstTable = document.getElementById("<%=hdfFirstTable.ClientID %>").value;
                if (hdfFirstTable === "BirthRecords") {
                    var inputElements = document.getElementsByClassName('chkHeader');
                    for (var i = 0; inputElements[i]; ++i) {

                        if (inputElements[i].checked) {
                            checkedValue = inputElements[i].value;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
                        }
                    }

                    var BirthCaseRdcId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;

                    if (BirthCaseRdcId == null || BirthCaseRdcId == "") {
                        if (checkedDIDfromCR) {

                            btnLinkEnableBtw();

                        } else if (!checkedDIDfromCR) {

                            btnCreateEnableBtw();

                        }
                        else if (hdfDetailId.includes(checkedValue)) {

                            btnLinkEnableBtw();
                        }
                        else {

                            btnCreateEnableBtw();
                        }
                    }

                    else {

                        for (var i = 0; inputElements[i]; ++i) {

                            if (checkedDIDfromCR != null && checkedDIDfromCR != "") {

                                btnLinkEnableBtw();

                            } else {

                                btnCreateEnableBtw();
                            }
                        }
                    }
                }
                else if (hdfFirstTable === "DeathRecords") {
                    var inputElements = document.getElementsByClassName('chkHeader');

                    var DeathCaseRdcId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                    if (DeathCaseRdcId == null || DeathCaseRdcId == "") {
                        if (checkedDIDfromCR != null && checkedDIDfromCR != "") {

                            btnLinkEnableBtw();

                        } else {

                            btnCreateEnableBtw();
                        }
                    }
                    else {

                        for (var i = 0; inputElements[i]; ++i) {
                            if (inputElements[i].checked) {
                                var checkedValue = inputElements[i].value;
                                document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
                            }
                            if (checkedDIDfromCR != null && checkedDIDfromCR != "") {

                                btnLinkEnableBtw();

                            } else {

                                btnCreateEnableBtw();
                            }
                        }
                    }
                }
                else {
                    var CaseReportCaseRecordId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    var inputElements = document.getElementsByClassName('chkHeader');
                    for (var i = 0; inputElements[i]; ++i) {

                        if (inputElements[i].checked) {
                            var checkedValue = inputElements[i].value;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
                        }
                    }
                    if (CaseReportCaseRecordId == "" || CaseReportCaseRecordId == null) {

                        btnCreateEnableBtw();
                    }
                    else {

                        for (var i = 0; inputElements[i]; ++i) {

                            if (checkedDIDfromCR != null && checkedDIDfromCR != "") {

                                btnLinkEnableBtw();

                            } else {

                                btnCreateEnableBtw();
                            }
                        }
                    }
                }
            }
            else if (awolDeathVar == "awol" || awolBirthVar == "awol" && hdfDetailId) {

                btnLinkEnable();

                checkedValue = inputElements[i].value;

                var someVarName = sessionStorage.getItem("detailsId");
                if (someVarName == null) {
                    someVarName = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                }
                if (someVarName && (someVarName !== null || someVarName !== "null")) {
                    if (!someVarName.includes(checkedValue)) {
                        someVarName = someVarName + "," + checkedValue;
                    }
                    else {
                        someVarName = checkedValue;
                    }

                }
                else {
                    someVarName = checkedValue;
                }
                sessionStorage.setItem("detailsId", someVarName);
                document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
                document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
            }
            else if (tieitValue == "tieit" && hdfDetailId) {

                btnLinkEnable();

            }
            else if (checksimilarIds) {
                btnCreateEnable();
            }
            else {
                var btnLink = document.getElementById("<%=btnLink.ClientID %>");
                $(btnLink).removeAttr('href');
                $(btnLink).removeClass('disableGrey');
                $(btnLink).addClass('enableGrey');
            }
        };

        function checkboxDisabled(dids) {

            var inputElements = document.getElementsByClassName('chkHeader');

            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            for (var j = 0; j < checkboxes.length; j++) {

                if (checkboxes[j].defaultValue != dids) {
                    checkboxes[j].disabled = true;
                }
            }

        }

        function checkedValueAddinSession(checkedValue) {

            var someVarName = sessionStorage.getItem("detailsId");
            if (someVarName == null || someVarName == "null" || document.getElementById("<%=hdfDetailsID.ClientID %>").value.includes(',')) {
                someVarName = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
            }

            if (someVarName !== null || someVarName !== "null") {
                if (!someVarName.includes(checkedValue)) {
                    someVarName = someVarName + "," + checkedValue;
                }
            }
            else {
                someVarName = checkedValue;
            }
            sessionStorage.setItem("detailsId", someVarName);


            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
        }

        function UncheckedValueRemoveinSession(UncheckedValue) {
            var someVarName = sessionStorage.getItem("detailsId");

            if (someVarName && (someVarName !== null || someVarName !== "null")) {
                var values = someVarName.split(",");

                var newValues = null;

                for (var j = 0; j < values.length; j++) {
                    if (values[j] !== "null") {
                        if (values[j] !== null && values[j] !== UncheckedValue) {

                            if (newValues) {
                                newValues = newValues + "," + values[j];
                            }
                            else {
                                newValues = values[j];
                            }
                        }
                    }
                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records" && values[j] && values[j] === UncheckedValue) {
                        document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value = null;
                    } else if ("<%=hdfTableKeyName.Value %>" == "Death Records" && values[j] && values[j] === UncheckedValue) {
                        document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value = null;
                    } else if (values[j] && values[j] === UncheckedValue) {
                        document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value = null;
                    }
                }
                if (newValues == null) {

                    sessionStorage.setItem("detailsId", "");
                    document.getElementById("<%=hdfDetailsID.ClientID %>").value = null;
                }
                else {
                    someVarName = sessionStorage.removeItem("detailsId");
                    sessionStorage.clear();
                    someVarName = newValues;
                    sessionStorage.setItem("detailsId", someVarName);
                    document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
                    document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = someVarName;
                }

            }
            else {
                document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
                document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = someVarName;
                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = someVarName;
                sessionStorage.setItem("caseDetailId", someVarName);
                sessionStorage.setItem("detailsId", someVarName);
            }
        }

        function myFunction() {  // after altering page
           
            var checkedValue = null;
            var UncheckedValue = null;
            var isCheckCheckedBox = "false";
            var inputElements = document.getElementsByClassName('chkHeader');
            var firstChkId = document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value;
            var didAdd = document.getElementById("<%=hdfCaseReportDidMatch.ClientID %>").value;
            var checkedDid = null;
            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].addEventListener("change", function () {
                    for (var j = 0; j < checkboxes.length; j++) {
                        if (checkboxes[j] != this) {

                            if (checkboxes[j].title !== "disabled") {
                                if (!checkboxes[j].disabled) {
                                    checkboxes[j].disabled = this.checked;
                                } else if (checkboxes[j].title !== "disabled") {
                                    checkboxes[j].disabled = this.checked;
                                }
                            }
                        }
                    }
                });
            }

            for (var i = 0; inputElements[i]; ++i) {

                var hdfFirstTable = document.getElementById("<%=hdfFirstTable.ClientID %>").value;
                // if the first value is assign null, then assign the first table value.(Link model Pop-up)
                if (!hdfFirstTable) {
                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfFirstTable.ClientID %>").value = "BirthRecords";
                        hdfFirstTable = "BirthRecords";
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfFirstTable.ClientID %>").value = "DeathRecords";
                        hdfFirstTable = "DeathRecords";
                    }
                    else {
                        document.getElementById("<%=hdfFirstTable.ClientID %>").value = "Matching_Linking";
                        hdfFirstTable = "Matching_Linking";
                    }
                }

                // condition to handle check or unchecked in the same source tab. 
                if ("<%=hdfTableKeyName.Value %>" == "Birth Records" && hdfFirstTable.includes('BirthRecords')) {
                    document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = null;
                }
                else if ("<%=hdfTableKeyName.Value %>" == "Death Records" && hdfFirstTable.includes('DeathRecords')) {
                    document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = null;
                }
                else if ("<%=hdfTableKeyName.Value %>" == "Case Reports" && hdfFirstTable.includes('Matching_Linking')) {
                    document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = null;
                }

                if (inputElements[i].checked) {
                    isCheckCheckedBox = "true";
                    var checkedDIDfromCR = null;
                    var isChecked = null;

                    //First CheckBox checked... 
                    var firstCheckeddetailsID = document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value;
                    if (!firstCheckeddetailsID) {
                        document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = inputElements[i].value;
                    } else {
                        document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = firstCheckeddetailsID;
                    }

                    checkedDid = inputElements[i].value;
                    isChecked = sessionStorage.getItem("isCheckCheckedBox");
                    if (isChecked == "null" || isChecked == null || isChecked == "false") {
                        sessionStorage.setItem("isCheckCheckedBox", isCheckCheckedBox);
                    }

                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfisCheckedBR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfFirstCheckedBRDid.ClientID %>").value = inputElements[i].value;
                        checkedDIDfromCR = document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value;
                        document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value = inputElements[i].value;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfisCheckedDR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfFirstCheckedDRDid.ClientID %>").value = inputElements[i].value;
                        checkedDIDfromCR = document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value;
                        document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value = inputElements[i].value;
                    }
                    else {
                        document.getElementById("<%=hdfisCheckedCR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value = inputElements[i].value;
                        checkedDIDfromCR = document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value;
                        document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value = inputElements[i].value;

                    }

                    // Case Rec ID of corresponding DID Values
                    var inputElementsCaseId = document.getElementsByName('hdfcaseRecId');
                    var fetchCaseRecIdValue = "";
                    for (var k = 0; inputElementsCaseId[k]; ++k) {

                        var value = inputElementsCaseId[k].value;
                        const myArray = value.split("_");
                        let DIds = myArray[0];
                        let caseRecID = myArray[1];
                        if (DIds === inputElements[i].value) {
                            fetchCaseRecIdValue = caseRecID;
                            break;
                        }
                    }

                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value = fetchCaseRecIdValue;

                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value = fetchCaseRecIdValue;

                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                        document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value = fetchCaseRecIdValue;
                    }

                    checkedValue = inputElements[i].value;

                    if (hdfFirstTable === "BirthRecords") {

                        var BirthCaseRdcId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        var awolDeathVar = document.getElementById("<%=hdfDeathIDCurrentPageValue.ClientID %>").value;
                        var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;

                        if (!tieitValue) {
                            var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                            if (hdfDetailId) {
                                tieitValue = "tieit";
                            } else {
                                tieitValue = null;
                            }
                        }
                        if (BirthCaseRdcId == null || BirthCaseRdcId == "") {
                            if (tieitValue == "tieit" && (typeof checkedDIDfromCR === "string" && checkedDIDfromCR.trim().length !== 0)) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value || document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }
                        }
                        else if (awolDeathVar == "awol") {
                            btnLinkEnable();
                        }
                        else {
                            btnLinkEnable();
                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");
                            if (caseDetailId == null) {
                                sessionStorage.setItem("caseDetailId", checkedValue);
                                sessionStorage.setItem("detailsId", checkedValue);
                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");
                        }
                        checkedValueAddinSession(checkedValue);

                    }
                    else if (hdfFirstTable === "DeathRecords") {
                        var awolBirthVar = document.getElementById("<%=hdfBirthIDCurrentPageValue.ClientID %>").value;
                        var DeathCaseRdcId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;
                        if (DeathCaseRdcId == null || DeathCaseRdcId == "") {
                            if ((tieitValue == "tieit" && (typeof checkedDIDfromCR === "string" && checkedDIDfromCR.trim().length !== 0))) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value || document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();

                            }
                        }
                        else if (awolBirthVar == "awol") {

                            btnLinkEnable();
                        }
                        else {

                            btnLinkEnable();
                            checkedValue = inputElements[i].value;
                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");
                            if (caseDetailId == null) {
                                sessionStorage.setItem("caseDetailId", checkedValue);
                                sessionStorage.setItem("detailsId", checkedValue);
                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");

                        }
                        checkedValueAddinSession(checkedValue);
                    }
                    else {
                        var CaseReportCaseRecordId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        var awolBirthVar = document.getElementById("<%=hdfBirthIDCurrentPageValue.ClientID %>").value;
                        var awolDeathVar = document.getElementById("<%=hdfDeathIDCurrentPageValue.ClientID %>").value;
                        if (CaseReportCaseRecordId == "" || CaseReportCaseRecordId == null) {

                            if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value || document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            } else {
                                btnCreateEnable();
                            }
                        }
                        else if (awolDeathVar == "awol" || awolBirthVar == "awol") {

                            btnLinkEnable();
                        }
                        else {
                            btnLinkEnable();
                            checkedValue = inputElements[i].value;
                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");
                            if (!caseDetailId) { // change
                                sessionStorage.setItem("caseDetailId", checkedValue);
                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");
                        }
                        checkedValueAddinSession(checkedValue);
                    }

                    break;
                }
                else {
                    checkedValue = null;
                    checkedDid = null;
                    someVarName = sessionStorage.getItem("caseDetailId");

                    var isAllCheck = false;
                    sessionStorage.setItem("isCheckCheckedBox", "false");
                    for (var s = 0; inputElements[s]; ++s) {
                        if (inputElements[s].checked) {
                            isAllCheck = true;
                            break;
                        }
                    }

                    if (isAllCheck) {
                        sessionStorage.setItem("isCheckCheckedBox", "true");
                    }
                    else {


                    }
                    var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;

                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfisCheckedBR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedBRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSBirth_BirthRecNo.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSBirth_DeathRecNo.ClientID %>").value = null;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfisCheckedDR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedDRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSDeath_BirthRecNo.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSDeath_DeathRecNo.ClientID %>").value = null;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                        document.getElementById("<%=hdfisCheckedCR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSCase_BirthRecNo.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSCase_DeathRecNo.ClientID %>").value = null;
                    }

                    // myFunction
                    var UncheckedValue = inputElements[i].value;
                    if (isCheckCheckedBox == "false") {

                        if (hdfDetailId) {
                            sessionStorage.setItem("isCheckCheckedBox", "true");

                        } else {
                            sessionStorage.setItem("isCheckCheckedBox", "false");
                        }
                        // Remove DID if ceckbox unchecked ....
                        // just unflagged DID

                        UncheckedValueRemoveinSession(UncheckedValue);


                    }
                    else {

                        sessionStorage.setItem("detailsId", "");
                        document.getElementById("<%=hdfDetailsID.ClientID %>").value = "";
                    }

                    //latetst value pickup basis of check/Unchecked and the current table did values
                    var hdfisCheckedBR = document.getElementById("<%=hdfisCheckedBR.ClientID %>").value;
                    var hdfisCheckedDR = document.getElementById("<%=hdfisCheckedDR.ClientID %>").value;
                    var hdfisCheckedCR = document.getElementById("<%=hdfisCheckedCR.ClientID %>").value;
                    hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                    var checkSimilarIds = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
                    var inputElementsCaseId = document.getElementsByName('hdfcaseRecId');
                    var fetchCaseRecIdValue = "";
                    for (var k = 0; inputElementsCaseId[k]; ++k) {

                        var value = inputElementsCaseId[k].value;
                        const myArray = value.split("_");
                        let DIds = myArray[0];

                        if (DIds === inputElements[i].value) {
                            fetchCaseRecIdValue = null;
                            break;
                        }
                    }
                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value = null;
                    } else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value = null;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                        document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value = null;
                    }

                    if (hdfisCheckedBR == "true") {
                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            if (!document.getElementById("<%=hdfFirstTable.ClientID%>").value) {
                                document.getElementById("<%=hdfFirstTable.ClientID%>").value = "BirthRecords";
                                document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").value = hdfDetailId;
                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = hdfDetailId;
                        }
                    }
                    else if (hdfisCheckedDR == "true") {

                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            if (!document.getElementById("<%=hdfFirstTable.ClientID%>").value) {
                                document.getElementById("<%=hdfFirstTable.ClientID%>").value = "DeathRecords";
                                document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").value = hdfDetailId;
                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = hdfDetailId;
                        }
                    }
                    else if (hdfisCheckedCR == "true") {

                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            document.getElementById("<%=hdfFirstTable.ClientID%>").value = "Matching_Linking";
                            document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").value = hdfDetailId;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").value = hdfDetailId;
                        }
                    }

                    /*codes below options for create or link*/

                    if (hdfFirstTable === "BirthRecords") {

                        var BirthCaseRdcId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (BirthCaseRdcId == null || BirthCaseRdcId == "") {

                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }
                        }
                        else {

                            btnLinkEnable();
                        }

                    }
                    else if (hdfFirstTable === "DeathRecords") {
                        var DeathCaseRdcId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        if (DeathCaseRdcId == null || DeathCaseRdcId == "") {
                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {

                                btnCreateEnable();
                            }
                        }
                        else {

                            btnLinkEnable();
                        }
                    }
                    else {
                        var CaseReportCaseRecordId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;

                        if (CaseReportCaseRecordId == "" || CaseReportCaseRecordId == null) {

                            if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {

                                btnCreateEnable();
                            }
                        }
                        else {

                            btnLinkEnable();
                        }
                    }
                }
            }

            /*codes below to take specific Report ID, Birth Rec No, Death Rec No*/
            RequiredFourData(checkedValue);

        };

        function RequiredFourData(checkedValue) {

            if ("<%=hdfTableKeyName.Value %>" === "Case Reports") {

                var inputElements = document.getElementsByClassName('chkReportHiddenId');

                for (var i = 0; inputElements[i]; ++i) {
                    var value = inputElements[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];  // all required id's reportID,birthCertificate,deathNumber
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSCaseReportId.ClientID %>');
                        innerId.value = Ids;
                        break;
                    }
                }

                /*check birth ID under the Case Source */

                var inputElementsbirth = document.getElementsByClassName('chkBirthHiddenId');

                for (var i = 0; inputElementsbirth[i]; ++i) {
                    var value = inputElementsbirth[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSCase_BirthRecNo.ClientID %>');
                        innerId.value = Ids;
                        break;
                    }
                    else {
                        document.getElementById('<%=hdfJSCase_BirthRecNo.ClientID %>').value = null;
                    }
                }

                /*check death ID under the Case Source as  */
                var inputElementsdeath = document.getElementsByClassName('chkDeathHiddenId');

                for (var i = 0; inputElementsdeath[i]; ++i) {
                    var value = inputElementsdeath[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSCase_DeathRecNo.ClientID %>');
                        innerId.value = Ids;
                        break;
                    }
                    else {
                        document.getElementById('<%=hdfJSCase_DeathRecNo.ClientID %>').value = null;  // assign null after uncheck death rec no
                    }
                }
            }

            else if ("<%=hdfTableKeyName.Value %>" === "Birth Records") {

                var inputElements = document.getElementsByClassName('chkBirthHiddenId');
                for (var i = 0; inputElements[i]; ++i) {
                    var value = inputElements[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSBirthRecordId.ClientID %>');
                        innerId.value = Ids;
                    }

                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSBirth_BirthRecNo.ClientID %>');
                        innerId.value = Ids;
                        break;
                    } else {
                        document.getElementById('<%=hdfJSBirth_BirthRecNo.ClientID %>').value = null;  // assign null after uncheck birth rec no
                    }
                }

                //Death value pickup
                var inputElementsdeath = document.getElementsByClassName('chkDeathHiddenId');

                for (var i = 0; inputElementsdeath[i]; ++i) {
                    var value = inputElementsdeath[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSBirth_DeathRecNo.ClientID %>');
                        innerId.value = Ids;
                        break;
                    }
                    else {
                        document.getElementById('<%=hdfJSBirth_DeathRecNo.ClientID %>').value = null;  // assign null after uncheck death rec no
                    }

                }
            }

            else if ("<%=hdfTableKeyName.Value %>" === "Death Records") {

                var inputElements = document.getElementsByClassName('chkDeathHiddenId');

                for (var i = 0; inputElements[i]; ++i) {
                    var value = inputElements[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSDeathRecordId.ClientID %>');
                        innerId.value = Ids;
                        var innerDeathId = document.getElementById('<%=hdfJSDeath_DeathRecNo.ClientID %>');   //qq
                        innerDeathId.value = Ids;
                        break;
                    }
                    else {
                        document.getElementById('<%=hdfJSDeath_DeathRecNo.ClientID %>').value = null;  // assign null after uncheck death rec no
                    }

                }

                /*check birth ID*/

                var inputElementsbirth = document.getElementsByClassName('chkBirthHiddenId');

                for (var i = 0; inputElementsbirth[i]; ++i) {
                    var value = inputElementsbirth[i].value;
                    const myArray = value.split("_");
                    let Ids = myArray[0];
                    let detailsId = myArray[1];
                    if (checkedValue === detailsId) {
                        var innerId = document.getElementById('<%=hdfJSDeath_BirthRecNo.ClientID %>');
                        innerId.value = Ids;
                        break;
                    }
                    else {
                        document.getElementById('<%=hdfJSDeath_BirthRecNo.ClientID %>').value = null;  // assign null after uncheck death rec no
                    }
                }
            }
        }

        function myFirstClickFunction(tablename)
        {
            var checkedValue = null;
            var isCheckCheckedBox = "false";
            var inputElements = document.getElementsByClassName('chkHeader');
            document.getElementById("<%=hdfFirstTable.ClientID %>").value = tablename;
            var firstChkId = document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value;
            var checkboxes = document.querySelectorAll("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].addEventListener("change", function () {
                    for (var j = 0; j < checkboxes.length; j++) {

                        if (checkboxes[j] != this) {

                            if (checkboxes[j].title !== "disabled") {
                                if (!checkboxes[j].disabled) {
                                    checkboxes[j].disabled = this.checked;
                                } else if (checkboxes[j].title !== "disabled") {
                                    checkboxes[j].disabled = this.checked;
                                }
                            }

                        }
                    }
                });
            }

            for (var i = 0; inputElements[i]; ++i) {

                if (inputElements[i].checked) {
                    //Checked DId's

                    isCheckCheckedBox = "true";
                    var isChecked = null;
                    isChecked = sessionStorage.getItem("isCheckCheckedBox");
                    //First CheckBox checked... 

                    var firstCheckeddetailsID = document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value;
                    if (firstCheckeddetailsID == "" || firstCheckeddetailsID == null) {
                        document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = inputElements[i].value;
                    }

                    if (isChecked == "null" || isChecked == null || isChecked == "false") {
                        sessionStorage.setItem("isCheckCheckedBox", isCheckCheckedBox);
                    }

                    var hdfFirstTable = document.getElementById("<%=hdfFirstTable.ClientID %>").value;
                    if (!hdfFirstTable) {

                        if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                            document.getElementById("<%=hdfFirstTable.ClientID %>").value = "BirthRecords";
                            hdfFirstTable = "BirthRecords";

                        }
                        else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                            document.getElementById("<%=hdfFirstTable.ClientID %>").value = "DeathRecords";
                            hdfFirstTable = "DeathRecords";
                        }
                        else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                            document.getElementById("<%=hdfFirstTable.ClientID %>").value = "Matching_Linking";
                            hdfFirstTable = "Matching_Linking";
                        }
                    }
                    // Case Rec ID of corresponding DID Values

                    var inputElementsCaseId = document.getElementsByName('hdfcaseRecId');

                    var fetchCaseRecIdValue = "";
                    for (var k = 0; inputElementsCaseId[k]; ++k) {

                        var value = inputElementsCaseId[k].value;
                        const myArray = value.split("_");
                        let DIds = myArray[0];
                        let caseRecID = myArray[1];
                        if (DIds === inputElements[i].value) {
                            fetchCaseRecIdValue = caseRecID;

                            break;
                        }
                    }

                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {
                        document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfisCheckedBR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value = inputElements[i].value;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfisCheckedDR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value = inputElements[i].value;
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                        document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value = fetchCaseRecIdValue;
                        document.getElementById("<%=hdfisCheckedCR.ClientID %>").value = "true";
                        document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value = inputElements[i].value;
                    }


                    if (hdfFirstTable === "BirthRecords") {

                        var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;
                        document.getElementById("<%=hdfFirstCheckedBRDid.ClientID %>").value = inputElements[i].value;

                        var activeCurrentCaseRecId = null;
                        var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                        if (!hdfDetailId) {
                            hdfDetailId = sessionStorage.getItem("detailsId");
                        }
                        if ("<%=hdfisCheckedBR.Value %>" == "true") {

                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                            }
                        } else if ("<%=hdfisCheckedDR.Value %>" == "true") {
                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                            }
                        }
                        else if ("<%=hdfisCheckedCR.Value %>" == "true") {
                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                            }
                        }


                        //check any did checked and respective caseRecid
                        if (!activeCurrentCaseRecId && hdfDetailId && hdfDetailId.includes(document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value)) {
                            activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;

                            if (!activeCurrentCaseRecId && hdfDetailId && hdfDetailId.includes(document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value)) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;

                                if (!activeCurrentCaseRecId && hdfDetailId && hdfDetailId.includes(document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value)) {
                                    activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                                }
                            }
                        }

                        if (!tieitValue) {

                            if (hdfDetailId && activeCurrentCaseRecId) {
                                tieitValue = "tieit";
                            } else {
                                tieitValue = null;
                            }
                        }

                        checkedValue = inputElements[i].value;
                        var someVarName = sessionStorage.getItem("detailsId");
                        if (someVarName) {
                            if (!someVarName.includes(checkedValue)) {
                                someVarName = someVarName + "," + checkedValue;
                            }
                            else {
                                someVarName = checkedValue;
                            }
                        }
                        else {
                            someVarName = checkedValue;
                        }
                        sessionStorage.setItem("detailsId", someVarName);
                        document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;  // sessionStorage.getItem("detailsId");
                        document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;


                        var BirthCaseRdcId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;


                        if (BirthCaseRdcId == null || BirthCaseRdcId == "") {

                            var hdfCaseRecId = document.getElementById("hdfcaseRecId");
                            if (hdfCaseRecId != null) {
                                var oTable = document.getElementById("hdfcaseRecId").value;


                                var values = oTable.split("_");
                                var did = values[0];
                                var caseRecId = values[1];
                                if (inputElements[i].value == did) {
                                    BirthCaseRdcId = caseRecId;
                                }
                                else {
                                    BirthCaseRdcId = null;
                                }
                            } else {
                                BirthCaseRdcId = null;
                            }
                        }


                        if (tieitValue) {
                            btnLinkEnable();
                        }
                        else if (!activeCurrentCaseRecId && hdfDetailId) {
                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }
                        }
                        else if (activeCurrentCaseRecId && hdfDetailId) {
                            btnCreateEnable();
                        }

                        else if (BirthCaseRdcId == null || BirthCaseRdcId == "") {
                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }

                            checkedValue = inputElements[i].value;
                            var someVarName = sessionStorage.getItem("detailsId");

                            if (someVarName) {
                                if (!someVarName.includes(checkedValue)) {
                                    someVarName = someVarName + "," + checkedValue;
                                }
                                else {
                                    someVarName = checkedValue;
                                }
                            }
                            else {
                                someVarName = checkedValue;
                            }

                            sessionStorage.setItem("detailsId", someVarName);
                            sessionStorage.setItem("caseDetailId", checkedValue);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;   //sessionStorage.getItem("detailsId");
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
                        }
                        else {

                            btnLinkEnable();

                            checkedValue = inputElements[i].value;

                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");

                            if (caseDetailId == null) {
                                sessionStorage.setItem("caseDetailId", checkedValue);
                                sessionStorage.setItem("detailsId", checkedValue);

                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");
                            var someVarName = sessionStorage.getItem("detailsId");

                            if (someVarName) {
                                if (!someVarName.includes(checkedValue)) {
                                    someVarName = someVarName + "," + checkedValue;
                                }
                                else {
                                    someVarName = checkedValue;
                                }
                            }
                            else {
                                someVarName = checkedValue;
                            }
                            sessionStorage.setItem("detailsId", someVarName);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;  // sessionStorage.getItem("detailsId");
                        }

                    }
                    else if (hdfFirstTable === "DeathRecords") {

                        document.getElementById("<%=hdfFirstCheckedDRDid.ClientID %>").value = inputElements[i].value;
                        var DeathCaseRdcId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;
                        var activeCurrentCaseRecId = null;
                        var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                        if (!hdfDetailId) {
                            hdfDetailId = sessionStorage.getItem("detailsId");
                        }
                        if ("<%=hdfisCheckedBR.Value %>" == "true") {
                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                            }
                        } else if ("<%=hdfisCheckedDR.Value %>" == "true") {
                            document.getElementById("<%=hdfisCheckedDR.ClientID %>").value = "false";
                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                            }
                        }
                        else if ("<%=hdfisCheckedCR.Value %>" == "true") {
                            document.getElementById("<%=hdfisCheckedCR.ClientID %>").value = "false";
                            if (hdfDetailId && !hdfDetailId.includes(",")) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                            }
                        }

                        //check any did checked and respective caseRecid
                        if (!activeCurrentCaseRecId && hdfDetailId) {
                            activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                            if (!activeCurrentCaseRecId) {
                                activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                                if (!activeCurrentCaseRecId) {
                                    activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                                }
                            }
                        }

                        if (!tieitValue) {

                            if (hdfDetailId && activeCurrentCaseRecId) {
                                tieitValue = "tieit";
                            } else {
                                tieitValue = null;
                            }
                        }

                        if (DeathCaseRdcId == null || DeathCaseRdcId == "") {
                            var hdfCaseRecId = document.getElementById("hdfcaseRecId");
                            if (hdfCaseRecId != null) {
                                var oTable = document.getElementById("hdfcaseRecId").value;
                                var values = oTable.split("_");
                                var did = values[0];
                                var caseRecId = values[1];
                                if (inputElements[i].value == did) {
                                    DeathCaseRdcId = caseRecId;
                                }
                                else {
                                    DeathCaseRdcId = null;
                                }
                            } else {
                                DeathCaseRdcId = null;
                            }

                        }

                        checkedValue = inputElements[i].value;
                        var someVarName = sessionStorage.getItem("detailsId");
                        if (someVarName) {
                            if (!someVarName.includes(checkedValue)) {
                                someVarName = someVarName + "," + checkedValue;
                            }
                            else {
                                someVarName = checkedValue;
                            }
                        }
                        else {
                            someVarName = checkedValue;
                        }
                        sessionStorage.setItem("detailsId", someVarName);
                        document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;  // sessionStorage.getItem("detailsId");
                        document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;

                        if (tieitValue) {

                            btnLinkEnable();

                        }
                        else if (!activeCurrentCaseRecId && hdfDetailId) {

                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }
                        }
                        else if (activeCurrentCaseRecId && hdfDetailId) {

                            btnCreateEnable();
                        }
                        else if (DeathCaseRdcId == null || DeathCaseRdcId == "") {

                            if (document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }

                        }
                        else {

                            btnLinkEnable();

                            checkedValue = inputElements[i].value;

                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");

                            if (caseDetailId == null) {
                                sessionStorage.setItem("caseDetailId", checkedValue);
                                sessionStorage.setItem("detailsId", checkedValue);

                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");
                            var someVarName = sessionStorage.getItem("detailsId");

                            if (someVarName) {
                                if (!someVarName.includes(checkedValue)) {
                                    someVarName = someVarName + "," + checkedValue;
                                }
                                else {
                                    someVarName = checkedValue;
                                }
                            }
                            else {
                                someVarName = checkedValue;
                            }
                            sessionStorage.setItem("detailsId", someVarName);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;  // sessionStorage.getItem("detailsId");
                        }
                    }
                    else {
                        document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value = inputElements[i].value;
                        var CaseReportCaseRecordId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;


                        if (CaseReportCaseRecordId == null || CaseReportCaseRecordId == "") {

                            var hdfCaseRecId = document.getElementById("hdfcaseRecId");
                            if (hdfCaseRecId != null) {
                                var oTable = document.getElementById("hdfcaseRecId").value;
                                var values = oTable.split("_");
                                var did = values[0];
                                var caseRecId = values[1];
                                if (inputElements[i].value == did) {
                                    CaseReportCaseRecordId = caseRecId;
                                }
                                else {
                                    CaseReportCaseRecordId = null;
                                }
                            } else {
                                CaseReportCaseRecordId = null;
                            }

                        }

                        if (CaseReportCaseRecordId == "" || CaseReportCaseRecordId == null) {


                            if (document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else if (document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value) {
                                btnLinkEnable();
                            }
                            else {
                                btnCreateEnable();
                            }


                            checkedValue = inputElements[i].value;
                            var someVarName = sessionStorage.getItem("detailsId");
                            if (someVarName) {
                                if (!someVarName.includes(checkedValue)) {
                                    someVarName = someVarName + "," + checkedValue;
                                }
                                else {
                                    someVarName = checkedValue;
                                }
                            }
                            else {
                                someVarName = checkedValue;
                            }
                            sessionStorage.setItem("detailsId", someVarName);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;   // sessionStorage.getItem("detailsId");
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = checkedValue;
                        }
                        else {

                            btnLinkEnable();

                            checkedValue = inputElements[i].value;

                            if (document.getElementById("<%=hdfTableName.ClientID %>").value == "Matching_Linking") {
                                document.getElementById("<%=IsCaseDetailID.ClientID %>").value = "Case Report";
                            }
                            var caseDetailId = sessionStorage.getItem("caseDetailId");

                            if (caseDetailId == null) {
                                sessionStorage.setItem("caseDetailId", checkedValue);

                            }
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = sessionStorage.getItem("caseDetailId");
                            var someVarName = sessionStorage.getItem("detailsId");

                            if (someVarName) {
                                if (!someVarName.includes(checkedValue)) {
                                    someVarName = someVarName + "," + checkedValue;
                                }
                                else {
                                    someVarName = checkedValue;
                                }
                            }
                            else {
                                someVarName = checkedValue;
                            }
                            sessionStorage.setItem("detailsId", someVarName);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;  //sessionStorage.getItem("detailsId");
                        }

                    }
                    break;

                }
                else {

                    var tieitValue = document.getElementById("<%=hdftieit.ClientID %>").value;
                    var activeCurrentCaseRecId = null;
                    document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID %>").value = null;
                    document.getElementById("<%=hdfFirstTable.ClientID %>").value = null;
                    document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = null;
                    checkedValue = null;
                    var isAllCheck = false;
                    for (var s = 0; inputElements[s]; ++s) {
                        if (inputElements[s].checked) {
                            isAllCheck = true;
                            break;
                        }
                    }

                    if (isAllCheck) {
                        sessionStorage.setItem("isCheckCheckedBox", "true");
                    }
                    else {

                        var inputElementsCaseId = document.getElementsByName('hdfcaseRecId');
                        var fetchCaseRecIdValue = "";
                        for (var k = 0; inputElementsCaseId[k]; ++k) {

                            var value = inputElementsCaseId[k].value;
                            const myArray = value.split("_");
                            let DIds = myArray[0];

                            if (DIds === inputElements[i].value) {
                                fetchCaseRecIdValue = null;
                                break;
                            }
                        }
                    }

                    if ("<%=hdfTableKeyName.Value %>" == "Birth Records") {

                        document.getElementById("<%=hdfisCheckedBR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedBRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkBirtRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value = null;
                        var birthId = document.getElementById("<%=hdfJSBirth_BirthRecNo.ClientID %>");
                        birthId.value = "";


                        var deathid = document.getElementById("<%=hdfJSBirth_DeathRecNo.ClientID %>");
                        deathid.value = "";

                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Death Records") {
                        document.getElementById("<%=hdfisCheckedDR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedDRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkDeathRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSDeath_BirthRecNo.ClientID %>").value = "";
                        document.getElementById("<%=hdfJSDeath_DeathRecNo.ClientID %>").value = "";
                    }
                    else if ("<%=hdfTableKeyName.Value %>" == "Case Reports") {
                        document.getElementById("<%=hdfisCheckedCR.ClientID %>").value = "false";
                        document.getElementById("<%=hdfFirstCheckedCRDid.ClientID %>").value = null;
                        document.getElementById("<%=hdfchkCaseRecId.ClientID %>").value = null;
                        document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value = null;
                        document.getElementById("<%=hdfJSCase_BirthRecNo.ClientID %>").value = "";
                        document.getElementById("<%=hdfJSCase_DeathRecNo.ClientID %>").value = "";
                    }
                    // just unflagged DID
                    var uncheckedValue = inputElements[i].value;

                    var someVarName = sessionStorage.getItem("detailsId");

                    if (someVarName) {
                        var values = someVarName.split(",");

                        var newValues = null;

                        for (var j = 0; j < values.length; j++) {
                            if (values[j] !== "null") {
                                if (values[j] && values[j] !== uncheckedValue) {
                                    if (newValues) {
                                        newValues = newValues + "," + values[j];
                                    }
                                    else {
                                        newValues = values[j];
                                    }
                                }
                            }

                            // set null when checkbox unchecked. 
                            if ("<%=hdfTableKeyName.Value %>" === "Birth Records" && values[j] && values[j] === uncheckedValue) {
                                document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value = null;
                            }
                            else if ("<%=hdfTableKeyName.Value %>" === "Death Records" && values[j] && values[j] === uncheckedValue) {
                                document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value = null;
                            }
                            else if (values[j] && values[j] === uncheckedValue) {
                                document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value = null;
                            }

                        }

                        // checked new value from source
                        if (newValues) {

                            someVarName = sessionStorage.removeItem("detailsId");
                            sessionStorage.clear();
                            someVarName = newValues;
                            sessionStorage.setItem("detailsId", someVarName);
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = someVarName;
                        }
                        else {

                            sessionStorage.setItem("detailsId", "");
                            document.getElementById("<%=hdfDetailsID.ClientID %>").value = "";
                            document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = "";
                        }
                    }
                    else {

                        sessionStorage.setItem("detailsId", "");
                        document.getElementById("<%=hdfDetailsID.ClientID %>").value = "";
                    }
                    var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                    var activeCurrentCaseRecId = null;

                    //latetst value pickup basis of check/Unchecked
                    var hdfisCheckedBR = document.getElementById("<%=hdfisCheckedBR.ClientID %>").value;
                    var hdfisCheckedDR = document.getElementById("<%=hdfisCheckedDR.ClientID %>").value;
                    var hdfisCheckedCR = document.getElementById("<%=hdfisCheckedCR.ClientID %>").value;

                    if (hdfisCheckedBR == "true") {
                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            document.getElementById("<%=hdfFirstTable.ClientID%>").Value = "BirthRecords";
                            document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").Value = hdfDetailId;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").Value = hdfDetailId;
                            activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        }
                    } else if (hdfisCheckedDR == "true") {

                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            document.getElementById("<%=hdfFirstTable.ClientID%>").Value = "DeathRecords";
                            document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").Value = hdfDetailId;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").Value = hdfDetailId;
                            activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        }
                    }
                    else if (hdfisCheckedCR == "true") {

                        if (hdfDetailId && !hdfDetailId.includes(",")) {
                            document.getElementById("<%=hdfFirstTable.ClientID%>").Value = "Matching_Linking";
                            document.getElementById("<%=hdfisFirstCheckedDetailsID.ClientID%>").Value = hdfDetailId;
                            document.getElementById("<%=hdfCaseDetailsID.ClientID%>").Value = hdfDetailId;
                            activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    }

                    if (!activeCurrentCaseRecId && document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfBirthRecordDidOnly.ClientID %>").value)) {
                        activeCurrentCaseRecId = document.getElementById("<%=hdfBirthRecordCaseRecordId.ClientID %>").value;
                        if (!activeCurrentCaseRecId && document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value)) {
                            activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        }
                        if (!activeCurrentCaseRecId && document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value)) {
                            activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }

                    }
                    else if (!activeCurrentCaseRecId && document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfDeathRecordDidOnly.ClientID %>").value)) {
                        activeCurrentCaseRecId = document.getElementById("<%=hdfDeathRecordCaseRecordID.ClientID %>").value;
                        if (!activeCurrentCaseRecId && document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value)) {
                            activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                        }
                    }
                    else if (!activeCurrentCaseRecId && document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value && hdfDetailId.includes(document.getElementById("<%=hdfCaseReportDidOnly.ClientID %>").value)) {
                        activeCurrentCaseRecId = document.getElementById("<%=hdfCaseReportCaseRecordId.ClientID %>").value;
                    }

                    if (!tieitValue) {

                        if (hdfDetailId && activeCurrentCaseRecId) {
                            tieitValue = "tieit";
                        } else {
                            tieitValue = null;
                        }
                    }
                    var checkSimilarIds = document.getElementById("<%=hdfSimilarCheckUnCheckSIDs.ClientID %>").value;
                    if (isCheckCheckedBox == "false") {
                        var hdfDetailId = document.getElementById("<%=hdfDetailsID.ClientID %>").value;
                        if (hdfDetailId) {
                            sessionStorage.setItem("isCheckCheckedBox", "true");

                        } else {
                            sessionStorage.setItem("isCheckCheckedBox", "false");
                        }

                        if (tieitValue && activeCurrentCaseRecId) {
                            btnLinkEnable();
                        }
                        else if (activeCurrentCaseRecId && hdfDetailId) {
                            btnLinkEnable();
                        }
                        else if (!activeCurrentCaseRecId && hdfDetailId) {
                            btnCreateEnable();
                        } else if (checkSimilarIds) {
                            btnCreateEnable();
                        }
                        else {
                            btnDefaults();
                        }

                    }
                    else {
                        document.getElementById("<%=hdfDetailsID.ClientID %>").value = someVarName;
                        document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value = someVarName;
                        document.getElementById("<%=IsCaseDetailID.ClientID %>").value = someVarName;
                        sessionStorage.setItem("caseDetailId", someVarName);
                        sessionStorage.setItem("detailsId", someVarName);
                    }
                }
            }

            //Assign Value for side by side view link

            var caseDetailId = null;

            caseDetailId = sessionStorage.getItem("caseDetailId"); //Checkbox checked get value


            RequiredFourData(checkedValue);
            if (caseDetailId) {
                sessionStorage.setItem("caseDetailId", "");
            }
        }

        function deepstat(url) {
            const title = "DeepStat";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function SearchAgainstBirth(url) {
            const title = "DeepStat";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function SearchAgainstDeath(url) {
            const title = "DeepStat";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function SideBySide(url) {
            const title = "SideBySide";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function linkedCaseRecordDetails(url) {
            const title = "Linked Case Record Details";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function popupAlert(tablename) {

            if (tablename == "Matching_Linking") {
                document.getElementById("message").innerHTML = "Please review all available source data before proceeding.";
            }
            else if (tablename == "BirthRecords") {
                document.getElementById("message").innerHTML = "Please review all available source data before proceeding.";
            }
            else {
                document.getElementById("message").innerHTML = "Please review all available source data before proceeding.";

            }

            $(function () {
                $("#dialog").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    buttons: [
                        {
                            text: "OK",
                            open: function () {
                                $(this).addClass('okcls')
                            },
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ],
                    position: {
                        my: "center center",
                        at: "center center"
                    }
                });
            });
            var isChecked = sessionStorage.getItem("isCheckCheckedBox");

            if (isChecked == null) {
                sessionStorage.setItem("isCheckCheckedBox", true);
            }
        }       

        function socketConnError() {

            $("#socketConnError").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Error",

                open: function () {

                    var imageConnError = $('<img src="Content/css/images/database-exclamation.svg" width="40" height="40"/>')
                    var connErrorMsg = ' Encountered an error while trying to connect to the server. Please check your network connection or server configuration and try again.  ';
                    $(this).append(imageConnError, connErrorMsg);
                },

                buttons: [
                    {
                        text: "DISMISS",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],

                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "darkred");;
        }

        function socketConnSuccess() {

            $("#socketConn").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Connection Successful",

                open: function () {
                    var imageConn = $('<img src="Content/css/images/database-check.svg" width="40" height="40"/>')
                    var connMsg = ' Server is available.  ';
                    $(this).append(imageConn, connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#6495ED");;
        }

        /*Required fix it checkbox is flagged without comment */

        function chkValidation() {
        
            var itemChecked = document.getElementById("<%=hdffixTool.ClientID %>").Value;
            var itemtxt = document.getElementById("<%=txtFixTool.ClientID%>");
            if (itemChecked == "true") {
                if (!itemtxt.value) {
                    return false;
                }
            }
            return true;
        }

        function assignAlert(event) {

            event.preventDefault();

            let $btnEle = $('#Content_btnAssign');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }
            
            if (!chkValidation()) {
                return;
            }

            var isChecked = sessionStorage.getItem("isCheckCheckedBox");
            sessionStorage.setItem("isCheckCheckedBox", isChecked);
            var istabchekced = '<%=hdfIsTabClicked.Value %>';
             if (istabchekced === "true") {
                 $(function () {
                     $("#dialogAssign").dialog({
                         width: 450,
                         modal: true,
                         dialogClass: "no-close",
                         position: {
                             my: "center center",
                             at: "center center"
                         },
                         title: "Selection Confirmation",
                         open: function () {
                             var connErrorMsg = 'You are about to create a new Case Record for the Candidate Case Report. This will include any Similar Candidates selected. If the Candidate was flagged for review, it will appear in the Fix It Tool once the Create has been successfully completed. Before proceeding, Please ensure all possible source matches have been reviewed. This step is crucial to ensuring accurate MBDR data. Do you wish to Proceed? (Yes/No)';
                             $(this).html(connErrorMsg);
                         },
                         buttons: {
                             Yes: function () {
                                 allowAssign = true;
                                 $(this).dialog("close");
                                 ShowProgress();
                     <%= Page.ClientScript.GetPostBackEventReference(btnAssign, String.Empty)  %>  // __doPostBack()
                     },
                     No: function () {
                         $(this).dialog("close");
                         return false;
                     }
                 },
             });
         });
     }
     else {
                 popupAlert('<%=hdfTableName.Value %>');
             }
         }

        function linkAlert(event) {

            event.preventDefault();

            let $btnEle = $('#Content_btnLink');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }
            if (!chkValidation()) {
                return;
            }
            var tablename = document.getElementById("<%=hdfFirstTable.ClientID %>").value;
            if (!tablename) {
                tablename = document.getElementById("<%=hdfFirstTable.ClientID %>").Value;
            }

            var istabchekced = '<%=hdfIsTabClicked.Value %>';

            if (istabchekced === "true") {

                $(function () {
                    $("#dialogLink").dialog({
                        width: "80%",
                        modal: true,
                        dialogClass: "no-close",
                        position: {
                            my: "center center",
                            at: "center center"
                        },
                        title: "Link Confirmation",
                        open: function () {
                            var Msg = 'You are about to link the Candidate Case Report to the Case, Birth, and/or Death record(s), directly or indirectly, selected. This will include any Similar Candidates selected. If the Candidate was flagged for review, it will appear in the Fix It Tool once the Link has been successfully completed. Before proceeding, please ensure all available source matches have been reviewed. This step is crucial to ensuring accurate MBDR data. Do you wish to Proceed? (Yes/No)';
                            var highlightedMsg = '<span class="highlight">' + Msg + '</span>';

                            $(this).find('#messageLinkShow').html(highlightedMsg).css('font-size', 'larger').append('<br><br>');


                            $(this).dialog("option", "buttons", {
                                "Yes": {
                                    text: "Yes",
                                    class: "yes-button",
                                    click: function () {
                                        allowLink = true;
                                        $(this).dialog("close");
                                        ShowProgress();
                                            <%= Page.ClientScript.GetPostBackEventReference(btnLink, String.Empty)  %> 
                                    }
                                },
                                "No": {
                                    text: "No",
                                    class: "no-button",
                                    click: function () {
                                        $(this).dialog("close");
                                        return false;
                                    }
                                }
                            });

                            var caseDetailId = sessionStorage.getItem("caseDetailId"); //Checkbox checked get value

                            if (caseDetailId == null || caseDetailId == "") {
                                caseDetailId = document.getElementById("<%=hdfCaseDetailsID.ClientID %>").value;
                            }

                            if ("<%=hdfTableKeyName.Value %>" === "Case Reports") {
                                var inputElements = document.getElementsByClassName('chkReportHiddenId');
                                var fetchValue = "";
                                for (var i = 0; inputElements[i]; ++i) {

                                    var value = inputElements[i].value;
                                    const myArray = value.split("_");
                                    let Ids = myArray[0];  // all required id's reportID,birthCertificate,deathNumber
                                    let detailsId = myArray[1];
                                    if (caseDetailId === detailsId) {
                                        var innerId = document.getElementById('<%=hdfJSCaseReportId.ClientID %>');
                                        innerId.value = Ids;
                                        fetchValue = Ids;
                                        break;
                                    }
                                }
                                var connErrorMsg = "";
                                var dids = document.getElementById("<%=hdfDetailsID.ClientID%>").value;
                                if (!fetchValue) {

                                    if (tablename == "BirthRecords") {
                                        var CRBirthRecordKeyName = document.getElementById('<%=hdfJSBirthRecordId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=masterRecordNumber&columnvalue=' + CRBirthRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                    }
                                    else if (tablename == "DeathRecords") {
                                        var CRDeathRecordKeyName = document.getElementById('<%=hdfJSDeathRecordId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                    }
                                    else {
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids + '&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';

                                    }
                                }
                                else {
                                    var connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=reportId&columnvalue=' + fetchValue + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                }
                                $(this).find('#messageLink').html(connErrorMsg);
                            }

                            else if ("<%=hdfTableKeyName.Value %>" === "Birth Records") {
                                var inputElements = document.getElementsByClassName('chkBirthHiddenId');
                                var fetchValue = "";
                                for (var i = 0; inputElements[i]; ++i) {

                                    var value = inputElements[i].value;
                                    const myArray = value.split("_");
                                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                                    let detailsId = myArray[1];

                                    if (caseDetailId === detailsId) {
                                        var innerId = document.getElementById('<%=hdfJSBirthRecordId.ClientID %>');
                                        innerId.value = Ids;
                                        fetchValue = Ids;
                                        break;
                                    }
                                }
                                var connErrorMsg = "";
                                var dids = document.getElementById("<%=hdfDetailsID.ClientID%>").value;
                                if (!fetchValue) {
                                    if (tablename == "Matching_Linking") {
                                        var CRCaseReportKeyName = document.getElementById('<%=hdfJSCaseReportId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=reportId&columnvalue=' + CRCaseReportKeyName + '&reportType=' + tablename + '" width="100% " height="450px"></iframe>';
                                    }
                                    else if (tablename == "DeathRecords") {
                                        var CRDeathRecordKeyName = document.getElementById('<%=hdfJSDeathRecordId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                    }
                                    else {
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids + '&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';

                                    }
                                }
                                else {
                                    connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=masterRecordNumber&columnvalue=' + fetchValue + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                }

                                $(this).find('#messageLink').html(connErrorMsg);
                            }

                            else if ("<%=hdfTableKeyName.Value %>" === "Death Records") {
                                var inputElements = document.getElementsByClassName('chkDeathHiddenId');
                                var fetchValue = "";
                                for (var i = 0; inputElements[i]; ++i) {

                                    var value = inputElements[i].value;
                                    const myArray = value.split("_");
                                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                                    let detailsId = myArray[1];
                                    if (caseDetailId === detailsId) {
                                        var innerId = document.getElementById('<%=hdfJSDeathRecordId.ClientID %>');
                                        innerId.value = Ids;
                                        fetchValue = Ids;
                                        break;
                                    }
                                }
                                var connErrorMsg = "";
                                var dids = document.getElementById("<%=hdfDetailsID.ClientID%>").value;
                                if (!fetchValue) {
                                    if (tablename == "Matching_Linking") {
                                        var CRCaseReportKeyName = document.getElementById('<%=hdfJSCaseReportId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=reportId&columnvalue=' + CRCaseReportKeyName + '&reportType=' + tablename + '" width="100% " height="450px"></iframe>';
                                    }
                                    else if (tablename == "BirthRecords") {
                                        var CRBirthRecordKeyName = document.getElementById('<%=hdfJSBirthRecordId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=masterRecordNumber&columnvalue=' + CRBirthRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                    }
                                    else {
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids + '&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';

                                    }
                                }
                                else {
                                    connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + fetchValue + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                }
                                $(this).find('#messageLink').html(connErrorMsg);
                            }

                            else if ("<%=hdfTableKeyName.Value %>" === "Similar Records") {
                                var inputElements = document.getElementsByClassName('chkDeathHiddenId');
                                var fetchValue = "";
                                for (var i = 0; inputElements[i]; ++i) {

                                    var value = inputElements[i].value;
                                    const myArray = value.split("_");
                                    let Ids = myArray[0];  // all required id's reportID,BirthId,Dea
                                    let detailsId = myArray[1];
                                    if (caseDetailId === detailsId) {
                                        var innerId = document.getElementById('<%=hdfJSDeathRecordId.ClientID %>');
                                        innerId.value = Ids;
                                        fetchValue = Ids;
                                        break;
                                    }
                                }
                                var connErrorMsg = "";
                                var dids = document.getElementById("<%=hdfDetailsID.ClientID%>").value;
                                if (!fetchValue) {
                                    if (tablename == "Matching_Linking") {
                                        var CRCaseReportKeyName = document.getElementById('<%=hdfJSCaseReportId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=reportId&columnvalue=' + CRCaseReportKeyName + '&reportType=' + tablename + '" width="100% " height="450px"></iframe>';
                                    }
                                    else if (tablename == "BirthRecords") {
                                        var CRBirthRecordKeyName = document.getElementById('<%=hdfJSBirthRecordId.ClientID %>').value;
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids +'&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=masterRecordNumber&columnvalue=' + CRBirthRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                    }
                                    else {
                                        connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids + '&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + CRDeathRecordKeyName + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';

                                    }
                                }
                                else {
                                    connErrorMsg = '<iframe src="/MLLinkModelView.aspx?dids=' + dids + '&scrutinizerId=<%=hdfScrutinizerID.Value %>&columnname=deathNumber&columnvalue=' + fetchValue + '&reportType=' + tablename + '" width="100%" height="450px"></iframe>';
                                }
                                $(this).find('#messageLink').html(connErrorMsg);
                            }
                        }
                    });
                });
            }
            else {
                popupAlert('<%=hdfTableName.Value %>');
            }
        }

        function successDiagBox() {

            $("#successDiagBox").dialog({

                width: 500,
                modal: true,
                dialogClass: "no-close",
                title: "Success",

                open: function () {
                    var newSid = "<%=hdfScrutinizerID.Value %>";
                    var connMsg = 'The content of this scrutiny has been updated and has the new SID : ' + newSid;
                    $(this).html(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls');
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#005F85");;
        }

        function endOfWOrk() {

            $("#endOfWOrk").dialog({

                width: 500,
                modal: true,
                dialogClass: "no-close",
                title: "End Of Work",

                open: function () {
                    var connMsg = ' This case report has been successfully processed.';
                    $(this).html(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls');
                        },
                        click: function () {
                            $(this).dialog("close");
                            ShowProgress();
                            <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#005F85");

            $(".ui-dialog-titlebar-close").on("click", function () {
                ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
            });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
                    ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                }
            });
        }

        function dtNullError() {

            $("#dtNull").dialog({
                width: 500,
                modal: true,
                dialogClass: "no-close",
                title: "Datatable Error Message",

                open: function () {
                    var connMsg = 'Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.';
                    $(this).html(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls');
                        },
                        click: function () {
                            $(this).dialog("close");
                            ShowProgress();
                            <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#005F85");

            $(".ui-dialog-titlebar-close").on("click", function () {
                ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
            });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
                    ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                }
            });
        }

        function errorMsgs(message, titleFor) {
            $("#errorMsgs").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },
                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                            ShowProgress();
                            <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }

            $(".ui-dialog-titlebar-close").on("click", function () {
                ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
            });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
                    ShowProgress();
                <%= Page.ClientScript.GetPostBackEventReference(btnAdvanceResult, String.Empty)  %> 
                }
            });
        }

        function cAlertMsgs(message, titleFor) {
            $("#commonMsg").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",

                open: function () {
                    var connMsg = message;
                    $(this).append(connMsg);
                },
                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }
        }

        function releaseAlert(event) {

            event.preventDefault();

            let $btnEle = $('#Content_btnRelease');
            if ($btnEle.hasClass('enableGrey')) {
                return;
            }

            $(function () {
                $("#dialogRelease").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Release Confirmation",
                    open: function () {
                        var connErrorMsg = 'You have clicked to Release this Candidate. You will no longer OWN the Candidate and will be returned to the Matching and Linking Search Screen.';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        OK: function () {
                            allowAssign = true;
                            $(this).dialog("close");
                            ShowProgress();
                    <%= Page.ClientScript.GetPostBackEventReference(btnRelease, String.Empty)  %>  // __doPostBack()
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
            });
        }

        function toggleCheckBtnFix(event, element)
        {
            event.preventDefault();

            var lblFixTool = document.getElementById("<%=lblFixTool.ClientID %>");
            var txtFixTool = document.getElementById("<%=txtFixTool.ClientID %>");
            var hdffixTool = document.getElementById("<%=hdffixTool.ClientID %>");
            var rqFieldValudator = document.getElementById("<%=rqFieldValudator.ClientID %>");


           
            var element = document.getElementById("btnFix");
            var isVisible = element.classList.contains('checked');
            var Visible = element.classList.toggle('checked');

            if (!Visible) {
                txtFixTool.value = "";
            }

            lblFixTool.style.display = !Visible ? "none" : "block";
            txtFixTool.style.display = !Visible ? "none" : "block";
            rqFieldValudator.style.display = !Visible ? "none" : "block";

            document.getElementById("<%=hdffixTool.ClientID %>").Value = !Visible ? "false" : "true";
            hdffixTool.value = !Visible ? "false" : "true";

            txtFixTool.disabled = isVisible;

            ValidatorEnable(rqFieldValudator, !isVisible);
        }

    </script>
</asp:Content>

