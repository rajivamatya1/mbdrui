﻿<%@ Page Title=" Candidate Death Record Details " enableEventValidation="false" Language="C#" MasterPageFile="~/loginMaster.master" AutoEventWireup="true" CodeFile="DeathRecordsSearchViewDetails.aspx.cs" Inherits="DeathRecordsSearchViewDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="Server">
    <% if (Session["username"] != null)
        { %>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Literal Text="" ID="ltrTableName" runat="server" />
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:HiddenField ID="hdfTableName" runat="server" />
                                    <asp:HiddenField ID="hdfSchema" runat="server" />
                                    <asp:HiddenField ID="hdfKeyName" runat="server" />
                                    <asp:HiddenField ID="hdfRowID" runat="server" />
                                    <asp:HiddenField ID="hdfCaseReportId" runat="server" />
                                     <asp:HiddenField ID="hdfNextRowID" runat="server" />
                                     <asp:HiddenField ID="hdfPreviousRowID" runat="server" />
                                    <asp:HiddenField ID="hdfSearchText" runat="server" />
                                    <asp:HiddenField ID="hdfName" runat="server" />
                                    <asp:HiddenField ID="hdfSearch" runat="server" />
                                    <asp:HiddenField ID="hdfColumnName" runat="server" />
                                    <asp:HiddenField ID="hdfFromDate" runat="server" />
                                    <asp:HiddenField ID="hdfToDate" runat="server" />
                                    <asp:HiddenField ID="hdfChangeFacility" runat="server" />   
                                    <asp:HiddenField ID="hdfadmittingEntity" runat="server" />   
                                    <asp:HiddenField ID="hdfCaseRecId" runat="server" />   
                                    <asp:Button Text="Previous" runat="server" OnClick="btnPrev_Click" ID="btnPrev" CssClass="btn btn-primary" />
                                    <asp:Button Text="Next" runat="server" OnClick="btnNext_Click" ID="btnNext" CssClass="btn btn-primary" />                                    
                                    <asp:Button Text="Return to Search Results" OnClick="btnAdvanceResult_Click" runat="server" ID="btnAdvanceResult" CssClass="btn btn-primary" />                   
                                   <%-- <asp:Button Text="New Search" OnClick="btnAdvancedAll_Click" runat="server" ID="btnAdvancedAll" CssClass="btn btn-primary" /> --%>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program-description">
                    <div class="admin-workarea">                      
                   
                        <div class="row row-no-padding">
                            <div class="col col-lg-12 col-md-12 col-sm-12 admin-area-table-container">
                                <div class="tableFixHead">
                                    <table class="table table-bordered table-striped table-fixed" visible="true" rules="all" id="gvDynamic" style="border-collapse: collapse;" border="1" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th scope="col">Columns</th>
                                                <th scope="col">Death Record</th>
                                        </thead>
                                        <tbody>
                                            <% if (dt != null)
                                                {
                                                    for (int i = 0; i < dt.Rows.Count; i++)
                                                    {
                                            %>
                                            <tr>
                                                <% if (Convert.ToString(dt.Rows[i][0]) == "Case Record ID")
                                                    { %>
                                                <td><%=dt.Rows[i][0] %>  </td>

                                                <td style="background-color:#E3F4FF;">
                                                    <%=dt.Rows[i][1] %>
                                                    <button id="btnLinkedCaseRecordDetails" title="Case Record Details" onclick="return linkedCaseRecordDetails('MLLinkedCaseRecordsViewDetails.aspx?Schema=MBDR_System&Table=CaseRecordsView&keyName=caseId&caseRecId=<%=dt.Rows[i][1] %>')" target="_blank" width="120px">
                                                        <div class="link-icon">
                                                            Case Record Details                                                                
                                                        </div>
                                                    </button>
                                                </td>
                                                <%}
                                                    else
                                                    {
                                                %>
                                                <td><%=dt.Rows[i][0] %>  </td>

                                                <td style="background-color:#E3F4FF;"><%=dt.Rows[i][1] %>  </td>
                                                <%
                                                    }
                                                %>
                                            </tr>

                                            <%
                                                    }
                                                }

                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% } %>
    <script>

        function linkedCaseRecordDetails(url) {
            const title = " Case Record Details";
            const w = "600";
            const h = "300";
            var propWin = `directories=no,titlebar=no,scrollbars=no,toolbar=no,location=no,status=Yes,menubar=no,resizable=no `;
            const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
            const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

            const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            const systemZoom = width / window.screen.availWidth;
            const left = (width - w) / 2 / systemZoom + dualScreenLeft
            const top = (height - h) / 2 / systemZoom + dualScreenTop

            const newWindow = window.open(url, title, propWin, `width=${w / systemZoom},height=${h / systemZoom},top=${top},left=${left}`)

            if (window.focus) newWindow.focus();
            return false;
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

    </script>
</asp:Content>

