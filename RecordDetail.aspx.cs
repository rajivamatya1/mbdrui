﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class RecordDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
                string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";

                hdfName.Value = name;
                

                btnBack.Text = string.Format("Back to {0} table", name);
                hdfRecordId.Value = rowID;
                lblTableName.Text = tableName;
                lblSchema.Text = schema;
                hdfKeyName.Value = keyName;
                lblPanelHeading.Text = string.Format("Viewing Record ID {0}", rowID);

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    lblTable.Text = tableName;
                    BindRepeater(schema, tableName, keyName, rowID);
                }
            }
        }

        public void BindRepeater(string schema, string tableName, string keyName, string value)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                query = string.Format("select * from {0}.{1} where " + keyName + "='{2}'", schema, tableName, value);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }

            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            
            GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            
            

            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|","");
                string[] header = model.Table.Split(':');
                if (header.Length > 0)
                {
                    lblHeader.Text = header[1];
                    lblHeader.Visible = true;
                }
            }

            List<string> lstColumns = includeColumns.Split(',').ToList();
            List<string> lstCategories = includeColumns.Split('|').ToList();
            List<CategoryModel> lstModel = new List<CategoryModel>();
            List<ColumnList> lstCategoryColumns;
            ColumnList colList;
            CategoryModel cat;

            if (model.ComparisonTables.Count > 0)
            {
                if (!model.ComparisonTables.Any(s => s.TableName == ""))
                {
                    pnlNavigation.Visible = true;
                    List<NavigationModel> lstNavigation = new List<NavigationModel>();
                    foreach (var item in model.ComparisonTables)
                    {
                        if (!string.IsNullOrEmpty(item.TableName))
                        {
                            string[] lstTableNames = item.TableName.Split(':');
                            if (!string.IsNullOrEmpty(lstTableNames[1]))
                            {
                                lstNavigation.Add(new NavigationModel() { DisplayName = lstTableNames[1], ActualName = lstTableNames[0] });
                            }
                            else
                            {
                                lstNavigation.Add(new NavigationModel() { DisplayName = lstTableNames[0], ActualName = lstTableNames[0] });
                            }
                        }
                    }
                    pnlNavigation.Visible = true;
                    rptNavigation.DataSource = lstNavigation;
                    rptNavigation.DataBind();
                }
                else
                {
                    pnlNavigation.Visible = false;
                }
            }
            else {
                pnlNavigation.Visible = false;
            }
            
            foreach (var item in includeColumns.Split(','))
            {
                if (!string.IsNullOrEmpty(item))
                {
                    cat = new CategoryModel();

                    lstCategoryColumns = new List<ColumnList>();

                    foreach (DataColumn dtCol in dt.Columns)
                    {
                        string colName = dtCol.ColumnName;

                        if (string.Equals(item.Split(':')[0].Trim(), colName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            colList = new ColumnList();
                            string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                            colList.ColumnName = columnInfo;
                            colList.ColumnValue = Convert.ToString(dt.Rows[0][colName]);
                            lstCategoryColumns.Add(colList);
                            break;
                        }
                    }

                    cat.lstColumns = lstCategoryColumns;
                    lstModel.Add(cat);
                }
            }
            rptCategory.DataSource = lstModel;
            rptCategory.DataBind();
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RepeaterItem item = e.Item;
                CategoryModel categoryModel = (CategoryModel)e.Item.DataItem;
                ((Repeater)item.FindControl("rptChild")).DataSource = categoryModel.lstColumns;
                ((Repeater)item.FindControl("rptChild")).DataBind();

            }
        }

        protected void rptNavigation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            NavigationModel model = (NavigationModel)e.Item.DataItem;
            ((Button)item.FindControl("btnCompare")).Text = string.Format("{0} table", model.DisplayName);
            ((Button)item.FindControl("btnCompare")).CommandArgument = string.Format("{0}:{1}", model.ActualName,model.DisplayName);
        }

        protected void rptNavigation_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Compare")
            {
                string tableName = e.CommandArgument.ToString();
                Response.Redirect("Comparison.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTableName.Text + "&CompTable=" + tableName + "&keyName=" + hdfKeyName.Value + "&RecordID=" + hdfRecordId.Value + "&name=" + hdfName.Value+"");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (lblSchema.Text == "MBDR_Totint_Views")
            {
                Response.Redirect("DefaultTotintView.aspx?schema=" + lblSchema.Text + "&table=" + lblTable.Text + "&name=" + hdfName.Value);
            }
            else 
            {
                Response.Redirect("DefaultDataView.aspx?schema=" + lblSchema.Text + "&table=" + lblTable.Text + "&name=" + hdfName.Value);
            }
        }
    }
}