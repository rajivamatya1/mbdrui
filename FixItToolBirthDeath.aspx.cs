﻿using DynamicGridView.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

public partial class FixItToolBirthDeath : System.Web.UI.Page
{
    protected DataTable dtCaseReport;

    protected DataTable dtFixITBirthData;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string checkname = !string.IsNullOrEmpty(Request.QueryString["checkname"]) ? Convert.ToString(Request.QueryString["checkname"]) : "";

            if (!Page.IsPostBack)
            {
                string type = string.Empty;   // for button state 

                if (!string.IsNullOrEmpty(checkname) && (checkname == "CHECK_BOTH" || checkname == "CHECK_BIRTH"))
                {
                    type = "birth";
                }
                else
                {
                    type = "death";
                }

                BindData(type);

                ClientScript.RegisterStartupScript(this.GetType(), "handledisable Proceed Click", "loadtimeproceeddisable();", true);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_Page_Load");
        }
    }

    protected void BindData(string type)
    {
        try
        {
            string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
            string checkname = !string.IsNullOrEmpty(Request.QueryString["checkname"]) ? Convert.ToString(Request.QueryString["checkname"]) : "";
            string primary = !string.IsNullOrEmpty(Request.QueryString["primary"]) ? Convert.ToString(Request.QueryString["primary"]) : "";
            string sourcebirth = !string.IsNullOrEmpty(Request.QueryString["sourcebirth"]) ? Convert.ToString(Request.QueryString["sourcebirth"]) : "";
            string targetbirth = !string.IsNullOrEmpty(Request.QueryString["targetbirth"]) ? Convert.ToString(Request.QueryString["targetbirth"]) : "";
            string potentialbirth = !string.IsNullOrEmpty(Request.QueryString["birth"]) ? Convert.ToString(Request.QueryString["birth"]) : "";
            string sourcedeath = !string.IsNullOrEmpty(Request.QueryString["sourcedeath"]) ? Convert.ToString(Request.QueryString["sourcedeath"]) : "";
            string targetdeath = !string.IsNullOrEmpty(Request.QueryString["targetdeath"]) ? Convert.ToString(Request.QueryString["targetdeath"]) : "";
            string potentialdeath = !string.IsNullOrEmpty(Request.QueryString["death"]) ? Convert.ToString(Request.QueryString["death"]) : "";
            string fixitDetailsComment = !string.IsNullOrEmpty(Request.QueryString["usercomments"]) ? Convert.ToString(Request.QueryString["usercomments"]) : "";

            string typeBID = "birthId";
            string typeDID = "deathId";
            string ids = string.Empty;
            string tablename = string.Empty;
            hdType.Value = type;

            if (checkname == "CHECK_BOTH")
            {
                if (type == "birth")
                {
                    btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("activeGreen", "");
                    string className = btnBirthRecords.CssClass;
                    btnBirthRecords.CssClass = className + " active";

                    btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                    className = btnDeathRecords.CssClass;
                    btnDeathRecords.CssClass = className + " activeGreen";
                }
                else
                {
                    btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                    string className = btnBirthRecords.CssClass;
                    btnBirthRecords.CssClass = className + " activeGreen";

                    btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("activeGreen", "");
                    className = btnDeathRecords.CssClass;
                    btnDeathRecords.CssClass = className + " active";
                }
            }

            else if (checkname == "CHECK_BIRTH")
            {
                string className = btnBirthRecords.CssClass;
                btnBirthRecords.CssClass = className + " active";
                btnDeathRecords.CssClass = btnDeathRecords.CssClass.Replace("active", "");
                string classdeathName = btnDeathRecords.CssClass;
                btnDeathRecords.CssClass = classdeathName + " btn-disabled";
            }

            else
            {
                string className = btnDeathRecords.CssClass;
                btnDeathRecords.CssClass = className + " active";
                btnBirthRecords.CssClass = btnBirthRecords.CssClass.Replace("active", "");
                string classbirthName = btnBirthRecords.CssClass;
                btnBirthRecords.CssClass = classbirthName + " btn-disabled";
            }

            ids = Regex.Replace(ids, ",{2,}", ",");

            DataTable dtFixItTargets = new DataTable();

            List<FixItBDConflictModel> fixItCaseDataViewModels = new List<FixItBDConflictModel>();

            FixItBDConflictModel fixItReportDataViewModels = new FixItBDConflictModel();

            //3. Get List of 

            string strQueryCaseID = "select * from MBDR_System.FixItReportData where fixItId = " + fixItId + " and reportId = " + primary;

            DataTable dtCaseReportData = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(strQueryCaseID, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dtCaseReportData);

                // 4. Get CaseID + Records from FixItCaseData
                foreach (DataRow dr in dtCaseReportData.Rows)
                {
                    fixItReportDataViewModels = ListRecords(dr, "Most Recent Report");
                    fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                    string strQuerySource = "";
                    DataTable dtFixItCaseReportData = new DataTable();
                    SqlCommand cmdReport = new SqlCommand();
                    SqlDataAdapter daReport = new SqlDataAdapter();

                    // Source birth/death

                    if (type == "birth" && !string.IsNullOrEmpty(sourcebirth))
                    {
                        dtFixItCaseReportData = new DataTable();
                        tablename = "Source Birth";

                        if (sourcebirth != "0")
                        {
                            strQuerySource = "select * from MBDR_System.FixItBirthData where " + typeBID + " = " + sourcebirth;

                            cmdReport = new SqlCommand(strQuerySource, conn);
                            cmdReport.CommandTimeout = 0;

                            conn.Open();
                            daReport = new SqlDataAdapter(cmdReport);
                            conn.Close();
                            daReport.Fill(dtFixItCaseReportData);

                            // 5. Get Records from FixItCaseReportData

                            if (dtFixItCaseReportData.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                                {
                                    fixItReportDataViewModels = new FixItBDConflictModel();
                                    fixItReportDataViewModels = ListRecords(dataRow, tablename);
                                    fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                                }
                            }
                            else
                            {
                                fixItReportDataViewModels = new FixItBDConflictModel();
                                fixItReportDataViewModels = ListBlankRecords(tablename);
                                fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                            }
                        }
                        else
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListBlankRecords(tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }

                    else if (!string.IsNullOrEmpty(sourcedeath))
                    {
                        dtFixItCaseReportData = new DataTable();
                        tablename = "Source Death";

                        if (sourcedeath != "0")
                        {
                            strQuerySource = "select * from MBDR_System.FixItDeathData where " + typeDID + " = " + sourcedeath;

                            cmdReport = new SqlCommand(strQuerySource, conn);
                            cmdReport.CommandTimeout = 0;

                            conn.Open();
                            daReport = new SqlDataAdapter(cmdReport);
                            conn.Close();
                            daReport.Fill(dtFixItCaseReportData);

                            // 5. Get Records from FixItCaseReportData
                            if (dtFixItCaseReportData.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                                {
                                    fixItReportDataViewModels = new FixItBDConflictModel();
                                    fixItReportDataViewModels = ListRecords(dataRow, tablename);
                                    fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                                }
                            }
                            else
                            {
                                fixItReportDataViewModels = new FixItBDConflictModel();
                                fixItReportDataViewModels = ListBlankRecords(tablename);
                                fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                            }
                        }
                        else
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListBlankRecords(tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }

                    // Target birth/death

                    if (type == "birth" && !string.IsNullOrEmpty(targetbirth))
                    {
                        dtFixItCaseReportData = new DataTable();
                        tablename = "Target Birth";

                        if (targetbirth != "0")
                        {
                            strQuerySource = "select * from MBDR_System.FixItBirthData where " + typeBID + " = " + targetbirth;

                            cmdReport = new SqlCommand(strQuerySource, conn);
                            cmdReport.CommandTimeout = 0;

                            conn.Open();
                            daReport = new SqlDataAdapter(cmdReport);
                            conn.Close();
                            daReport.Fill(dtFixItCaseReportData);

                            // 5. Get Records from FixItCaseReportData
                            if (dtFixItCaseReportData.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                                {
                                    fixItReportDataViewModels = new FixItBDConflictModel();
                                    fixItReportDataViewModels = ListRecords(dataRow, tablename);
                                    fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                                }
                            }
                            else
                            {
                                fixItReportDataViewModels = new FixItBDConflictModel();
                                fixItReportDataViewModels = ListBlankRecords(tablename);
                                fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                            }
                        }
                        else
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListBlankRecords(tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }

                    else if (!string.IsNullOrEmpty(targetdeath))
                    {
                        dtFixItCaseReportData = new DataTable();
                        tablename = "Target Death";

                        if (targetdeath != "0")
                        {
                            strQuerySource = "select * from MBDR_System.FixItDeathData where " + typeDID + " = " + targetdeath;

                            cmdReport = new SqlCommand(strQuerySource, conn);
                            cmdReport.CommandTimeout = 0;

                            conn.Open();
                            daReport = new SqlDataAdapter(cmdReport);
                            conn.Close();
                            daReport.Fill(dtFixItCaseReportData);

                            // 5. Get Records from FixItCaseReportData

                            if (dtFixItCaseReportData.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                                {
                                    fixItReportDataViewModels = new FixItBDConflictModel();
                                    fixItReportDataViewModels = ListRecords(dataRow, tablename);
                                    fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                                }
                            }
                            else
                            {
                                fixItReportDataViewModels = new FixItBDConflictModel();
                                fixItReportDataViewModels = ListBlankRecords(tablename);
                                fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                            }
                        }
                        else
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListBlankRecords(tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }

                    // Potential Birth /Death

                    if (type == "birth" && !string.IsNullOrEmpty(potentialbirth))
                    {
                        strQuerySource = "select * from MBDR_System.FixItBirthData where " + typeBID + " in (" + potentialbirth + ") ";
                        tablename = "Potential Birth";
                        dtFixItCaseReportData = new DataTable();
                        cmdReport = new SqlCommand(strQuerySource, conn);
                        cmdReport.CommandTimeout = 0;

                        conn.Open();
                        daReport = new SqlDataAdapter(cmdReport);
                        conn.Close();
                        daReport.Fill(dtFixItCaseReportData);

                        // 5. Get Records from FixItCaseReportData
                        foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListRecords(dataRow, tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }

                    else if (type == "death" && !string.IsNullOrEmpty(potentialdeath))  // Only for Death
                    {
                        strQuerySource = "select * from MBDR_System.FixItDeathData where " + typeDID + " in (" + potentialdeath + ") ";
                        tablename = "Potential Death";
                        dtFixItCaseReportData = new DataTable();
                        cmdReport = new SqlCommand(strQuerySource, conn);
                        cmdReport.CommandTimeout = 0;

                        conn.Open();
                        daReport = new SqlDataAdapter(cmdReport);
                        conn.Close();
                        daReport.Fill(dtFixItCaseReportData);

                        // 5. Get Records from FixItCaseReportData
                        foreach (DataRow dataRow in dtFixItCaseReportData.Rows)
                        {
                            fixItReportDataViewModels = new FixItBDConflictModel();
                            fixItReportDataViewModels = ListRecords(dataRow, tablename);
                            fixItCaseDataViewModels.Add(fixItReportDataViewModels);
                        }
                    }
                }
            }

            // Data fill 

            //2. Get List of column 

            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;

            string schema = "MBDR_System";
            string tableName = "FixItBirthData";

            PropertyInfo[] properties = typeof(FixItBDConflictModel).GetProperties();

            var filter = Common.GetFilterList().Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();

            if (filter != null)
            {
                includeColumns = filter.IncludedColumns;
                readonlyColumns = filter.ReadonlyColumns;
                dataKeyname = filter.DataKeyName;
            }

            List<string> lstColumns = includeColumns.Split(',').ToList();

            dtFixITBirthData = new DataTable();

            foreach (PropertyInfo property in properties)
            {
                var colName = lstColumns.FirstOrDefault(s => s.StartsWith(property.Name, StringComparison.OrdinalIgnoreCase));
                var displayColname = "";
                if (!string.IsNullOrEmpty(colName))
                {
                    string columnName = colName.Split(':')[1];

                    displayColname = columnName;
                }
                else
                {
                    string colDName = property.Name;

                    foreach (var col in lstColumns)
                    {
                        string columnName = col.Split(':')[0];

                        if (colDName.Contains(columnName))
                        {
                            displayColname = col.Split(':')[1];
                            break;
                        }
                    }
                }

                dtFixITBirthData.Columns.Add(displayColname);
            }

            foreach (FixItBDConflictModel item in fixItCaseDataViewModels)
            {
                DataRow dr = dtFixITBirthData.NewRow();

                foreach (PropertyInfo property in properties)
                {

                    var colName = lstColumns.FirstOrDefault(s => s.StartsWith(property.Name, StringComparison.OrdinalIgnoreCase));
                    var displayColname = "";
                    if (!string.IsNullOrEmpty(colName))
                    {
                        string columnName = colName.Split(':')[1];

                        displayColname = columnName;
                    }
                    else
                    {
                        string colDName = property.Name;

                        foreach (var col in lstColumns)
                        {
                            string columnName = col.Split(':')[0];

                            if (colDName.Contains(columnName))
                            {
                                displayColname = col.Split(':')[1];
                                break;
                            }
                        }
                    }

                    dr[displayColname] = Convert.ToString(property.GetValue(item, null));
                }

                dtFixITBirthData.Rows.Add(dr);
            }

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_BindGridData");
        }

    }

    protected FixItBDConflictModel ListRecords(DataRow dr, string tableName)
    {
        FixItBDConflictModel item = new FixItBDConflictModel();

        item.CaseRecordTable = tableName;
        item.CaseId = Convert.ToString(dr["caseId"]);

        try
        {
            if (tableName.Contains("Birth"))
            {
                item.CaseRecId = Convert.ToString(dr["caseRecId"]);
                item.ReportId = "";
                item.BirthCertNumber = Convert.ToString(dr["birthCertNumber"]);
                item.DeathNumber = Convert.ToString(dr["deathNumber"]);
                item.ChildLastName = Convert.ToString(dr["childLastName"]);
                item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
                item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
                item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
                item.ChildSSN = Convert.ToString(dr["childSSN"]);

                string rawDOB = Convert.ToString(dr["childBirth"]);

                if (!string.IsNullOrWhiteSpace(rawDOB) && rawDOB.Length == 8 && rawDOB.All(char.IsDigit))
                {
                    item.BirthDate = DateTime.ParseExact(Convert.ToString(dr["childBirth"]), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
                }
                else
                {
                    item.BirthDate = string.Empty;
                }

                item.MomLastName = Convert.ToString(dr["motherLastName"]);
                item.MomFirstName = Convert.ToString(dr["motherFirstName"]);
                item.MomMiddleName = Convert.ToString(dr["motherMiddleName"]);
                //item.MomSuffix = Convert.ToString(dr["motherSuffix"]);
                item.Address1 = Convert.ToString(dr["cfMotherAddress1"]);
                item.City = Convert.ToString(dr["motherCity"]);
                item.County = Convert.ToString(dr["birthCounty"]);
                item.State = Convert.ToString(dr["birthState"]);
                //item.Country = Convert.ToString(dr["country"]);
                item.ZipCode = Convert.ToString(dr["zipcode"]);
                item.ChildMRN = Convert.ToString(dr["childMRN"]);
                item.Gender = Convert.ToString(dr["sex"]);
                item.Plurality = Convert.ToString(dr["birthPlurality"]);
                item.BirthOrder = Convert.ToString(dr["birthOrder"]);
                // item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
                item.BirthWeight = Convert.ToString(dr["childBirthWeight"]);
                item.BirthHospital = Convert.ToString(dr["entityName"]);
                item.BirthDeathId = Convert.ToString(dr["birthId"]);
            }
            else if (tableName.Contains("Death"))
            {
                // death
                item.CaseRecId = Convert.ToString(dr["caseRecId"]);
                item.ReportId = "";
                item.BirthCertNumber = "";
                item.DeathNumber = Convert.ToString(dr["deathNumber"]);
                item.ChildLastName = Convert.ToString(dr["decedentLastName"]);
                item.ChildFirstName = Convert.ToString(dr["decedentFirstName"]);
                item.ChildMiddleName = Convert.ToString(dr["decedentMiddleName"]);
                item.ChildSuffix = Convert.ToString(dr["decedentSuffix"]);
                item.ChildSSN = Convert.ToString(dr["decedentSSN"]);

                string rawDOB = Convert.ToString(dr["decedentBirth"]);

                if (!string.IsNullOrWhiteSpace(rawDOB) && rawDOB.Length == 8 && rawDOB.All(char.IsDigit))
                {
                    item.BirthDate = DateTime.ParseExact(Convert.ToString(dr["decedentBirth"]), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
                }
                else
                {
                    item.BirthDate = string.Empty;
                }

                item.MomLastName = Convert.ToString(dr["motherLastName"]);
                item.MomFirstName = Convert.ToString(dr["motherFirstName"]);
                item.MomMiddleName = Convert.ToString(dr["motherMiddleName"]);
                //item.MomSuffix = Convert.ToString(dr["motherSuffix"]);
                item.Address1 = "";
                item.City = "";
                item.County = "";
                item.State = "";
                //item.Country = Convert.ToString(dr["country"]);
                item.ZipCode = "";
                item.ChildMRN = "";
                item.Gender = "";
                item.Plurality = "";
                item.BirthOrder = "";
                // item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
                item.BirthWeight = "";
                item.BirthHospital = Convert.ToString(dr["entityName"]);
                item.BirthDeathId = Convert.ToString(dr["deathId"]);
            }
            else
            {
                item.CaseRecId = Convert.ToString(dr["caseRecId"]);
                item.ReportId = Convert.ToString(dr["reportId"]);
                item.BirthCertNumber = Convert.ToString(dr["birthCertNumber"]);
                item.DeathNumber = Convert.ToString(dr["deathNumber"]);
                item.ChildLastName = Convert.ToString(dr["childLastName"]);
                item.ChildFirstName = Convert.ToString(dr["childFirstName"]);
                item.ChildMiddleName = Convert.ToString(dr["childMiddleName"]);
                item.ChildSuffix = Convert.ToString(dr["childSuffix"]);
                item.ChildSSN = Convert.ToString(dr["childSSN"]);

                string rawDOB = Convert.ToString(dr["birthDate"]);

                if (!string.IsNullOrWhiteSpace(rawDOB))
                {
                    item.BirthDate = Convert.ToDateTime(dr["birthDate"]).Date.ToString("MM/dd/yyyy");
                }
                else
                {
                    item.BirthDate = string.Empty;
                }


                item.MomLastName = Convert.ToString(dr["momLastName"]);
                item.MomFirstName = Convert.ToString(dr["momFirstName"]);
                item.MomMiddleName = Convert.ToString(dr["momMiddleName"]);
                //item.MomSuffix = Convert.ToString(dr["motherSuffix"]);
                item.Address1 = Convert.ToString(dr["address1"]);
                item.City = Convert.ToString(dr["city"]);
                item.County = Convert.ToString(dr["county"]);
                item.State = Convert.ToString(dr["state"]);
                item.Country = Convert.ToString(dr["country"]);
                item.ZipCode = Convert.ToString(dr["zipcode"]);
                item.ChildMRN = Convert.ToString(dr["childMRN"]);
                item.Gender = Convert.ToString(dr["gender"]);
                item.Plurality = Convert.ToString(dr["plurality"]);
                item.BirthOrder = Convert.ToString(dr["birthOrder"]);
                item.VitalStatus = Convert.ToString(dr["vitalStatus"]);
                item.BirthWeight = Convert.ToString(dr["birthWeight"]);
                item.BirthHospital = Convert.ToString(dr["entityName"]);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_ListRecords");
        }

        return item;
    }

    protected FixItBDConflictModel ListBlankRecords(string tableName)
    {
        FixItBDConflictModel item = new FixItBDConflictModel();

        item.CaseRecordTable = tableName;
        item.CaseId = "";

        try
        {
            if (tableName.Contains("Birth"))
            {
                item.CaseRecId = "";
                item.ReportId = "";
                item.BirthCertNumber = "";
                item.DeathNumber = "";
                item.ChildLastName = "";
                item.ChildFirstName = "";
                item.ChildMiddleName = "";
                item.ChildSuffix = "";
                item.ChildSSN = "";
                item.MomLastName = "";
                item.MomFirstName = "";
                item.MomMiddleName = "";
                item.MomSuffix = "";
                item.BirthDate = "";
                item.Address1 = "";
                item.City = "";
                item.County = "";
                item.State = "";
                item.Country = "";
                item.ZipCode = "";
                item.ChildMRN = "";
                item.Gender = "";
                item.Plurality = "";
                item.BirthOrder = "";
                item.VitalStatus = "";
                item.BirthWeight = "";
                item.BirthHospital = "";
                item.BirthDeathId = "";
            }
            else if (tableName.Contains("Death"))
            {
                // death
                item.CaseRecId = "";
                item.ReportId = "";
                item.BirthCertNumber = "";
                item.DeathNumber = "";
                item.ChildLastName = "";
                item.ChildFirstName = "";
                item.ChildMiddleName = "";
                item.ChildSuffix = "";
                item.ChildSSN = "";
                item.MomLastName = "";
                item.MomFirstName = "";
                item.MomMiddleName = "";
                item.MomSuffix = "";
                item.BirthDate = "";
                item.Address1 = "";
                item.City = "";
                item.County = "";
                item.State = "";
                item.Country = "";
                item.ZipCode = "";
                item.ChildMRN = "";
                item.Gender = "";
                item.Plurality = "";
                item.BirthOrder = "";
                item.VitalStatus = "";
                item.BirthWeight = "";
                item.BirthHospital = "";
                item.BirthDeathId = "";
            }
            else
            {
                item.CaseRecId = "";
                item.ReportId = "";
                item.BirthCertNumber = "";
                item.DeathNumber = "";
                item.ChildLastName = "";
                item.ChildFirstName = "";
                item.ChildMiddleName = "";
                item.ChildSuffix = "";
                item.ChildSSN = "";
                item.MomLastName = "";
                item.MomFirstName = "";
                item.MomMiddleName = "";
                item.MomSuffix = "";
                item.BirthDate = "";
                item.Address1 = "";
                item.City = "";
                item.County = "";
                item.State = "";
                item.Country = "";
                item.ZipCode = "";
                item.ChildMRN = "";
                item.Gender = "";
                item.Plurality = "";
                item.BirthOrder = "";
                item.VitalStatus = "";
                item.BirthWeight = "";
                item.BirthHospital = "";
                item.BirthDeathId = "";
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_ListBlankRecords");
        }

        return item;
    }

    protected void btnBirthRecords_Click(object sender, EventArgs e)
    {
        try
        {
            BindData("birth");
            string elementId = "chkProceedBirth";
            string script = $"handleProceedCheck(document.getElementById('{elementId}'),'birth');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "handle Proceed Click", script, true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_btnBirthRecords_Click");
        }
    }

    protected void btnDeathRecords_Click(object sender, EventArgs e)
    {
        try
        {
            BindData("death");

            string elementId = "chkProceedDeath";
            string script = $"handleProceedCheck(document.getElementById('{elementId}'),'death');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "handle Proceed Click", script, true);

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_btnDeathRecords_Click");
        }
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {
        try
        {
            string json = string.Empty;
            string fixItId = !string.IsNullOrEmpty(Request.QueryString["fixItId"]) ? Convert.ToString(Request.QueryString["fixItId"]) : "";
            string btnType = !string.IsNullOrEmpty(Request.QueryString["type"]) ? Convert.ToString(Request.QueryString["type"]) : "";
            string existingtargetValue = !string.IsNullOrEmpty(Request.QueryString["existingtargetValue"]) ? Convert.ToString(Request.QueryString["existingtargetValue"]) : "";
            string existingreports = !string.IsNullOrEmpty(Request.QueryString["existingreports"]) ? Convert.ToString(Request.QueryString["existingreports"]) : "";
            string fixitDetailsComment = !string.IsNullOrEmpty(Request.QueryString["usercomments"]) ? Convert.ToString(Request.QueryString["usercomments"]) : "";

            int birthId = string.IsNullOrEmpty(hdfBirthId.Value) ? 0 : Convert.ToInt32(hdfBirthId.Value);
            int deathId = string.IsNullOrEmpty(hdfDeathId.Value) ? 0 : Convert.ToInt32(hdfDeathId.Value);

            if (!string.IsNullOrEmpty(btnType) && btnType == "move")
            {
                json = "{\"cmd\":\"FMOVE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + fixItId + ",\"target\":" + existingtargetValue + ",\"reports\":[" + existingreports + "],\"comment\":\"" + fixitDetailsComment + "\",\"birth\":" + birthId + ",\"death\":" + deathId + "}";
            }
            else
            {
                json = "{\"cmd\":\"FCREATE\",\"user\":" + Convert.ToInt32(Session["userid"]) + ",\"fixIt\":" + fixItId + ",\"reports\":[" + existingreports + "],\"comment\":\"" + fixitDetailsComment + "\",\"birth\":" + birthId + ",\"death\":" + deathId + "}";
            }
            string response = SocketConn(json);

            string getMsgs = $"FID Birth/Death Conflict |btnProceed_Click|response:{response}";
            ErrorLog(getMsgs);


            if (response == "SocketConnectionError")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Connection error", "socketConnError();", true);
            }
            else
            {
                dynamic data = JObject.Parse(response);

                if (data.name != null && data.name == "SECURITY" && Convert.ToString(data.status) == "22")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Security Violation", "sViolationDC('Your request has encountered an issue. If issues persists, please report to Altarum Support. Click OK to proceed.', 'Security Violation');", true);

                    return;
                }
                else if (data.name != "SUCCESS")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "btnMove_Click", "errorMsgs('Your request encountered an error and cannot be processed. Please report this message to Altarum Support. Click OK to return to the search screen.', 'Error Message');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "close and refresh model popup", "closewindowandRedirect();", true);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FixtitToolBirthDeath_btnProceed_Click");
        }
    }

    public string SocketConn(string JsonRecord)
    {
        string responseFromServer = string.Empty;
        string socketIp = ConfigurationManager.AppSettings["ipAddress"];
        int socketPort = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"]);
        try
        {
            using (var socket = new TcpClient(socketIp, socketPort))
            {
                string json = JsonRecord;
                byte[] body = Encoding.UTF8.GetBytes(json);
                int bodyLength = Encoding.UTF8.GetByteCount(json);
                var bl = (byte)(bodyLength);
                var bh = (byte)(bodyLength >> 8);
                using (var stream = socket.GetStream())
                {
                    stream.WriteByte(bh);
                    stream.WriteByte(bl);
                    stream.Write(body, 0, bodyLength);

                    short size = (short)((stream.ReadByte() << 8) + stream.ReadByte());

                    byte[] buffer = new byte[size];

                    int sizestream = stream.Read(buffer, 0, buffer.Length); // SIZE SHOULD BE EQUAL TO STREAM.READ 

                    if (sizestream == size)
                    {
                        responseFromServer = Encoding.ASCII.GetString(buffer);
                    }
                    else
                    {
                        responseFromServer = "SocketConnectionError";
                    }
                }
            }
        }
        catch (Exception)
        {
            responseFromServer = "SocketConnectionError";
        }
        return responseFromServer;
    }

    public static void ErrorLog(string values)
    {
        bool shot = true;
        if (shot)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.Snap(values, path, "Login.aspx");
        }
    }

}