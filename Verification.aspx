﻿<%@ Page Title="Case Verification" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="Verification.aspx.cs" Inherits="DynamicGridView.Verification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <%--  <script src="Content/js/jquery/3.6.1/jquery.min.js"></script>
    <script src="Content/js/jquery/ui/1.13.2/jquery-ui.min.js"></script>
    <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">
        <img class="prefix-icon" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
        VERIFICATION</h1>
    <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
    <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
    <asp:HiddenField runat="server" ID="hdfName" Value="" />
    <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
    <asp:HiddenField runat="server" ID="isResetClick" Value="" />

    <div class="row pt-1">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label style="margin-right: 50px; font-weight: 600;">FILTER BY: (required)</label>
            </div>
        </div>
    </div>

    <div class="row pt-1">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item pt-1">
                <label for="Content_ddlCaseReports" style="margin-right: 15px;">Verification Status</label>
                <asp:DropDownList runat="server" ID="ddlVeriStatus" CssClass="form-control">
                    <asp:ListItem Text="Incomplete" Value="Incomplete"></asp:ListItem>
                    <asp:ListItem Text="Complete" Value="Complete"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqVerificationStatus" runat="server" ControlToValidate="ddlVeriStatus" InitialValue="Select Verification Status" ValidationGroup="search" ErrorMessage="Please select an option from verifcation status." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
            </div>

            <div class="form-item pt-1">
                <label for="Content_ddlCaseReports">Facility</label>
                <asp:DropDownList runat="server" ID="ddlFacility" CssClass="form-control"></asp:DropDownList> <br />
                <asp:RequiredFieldValidator 
                    ID="RequiredFieldValidator1" 
                    runat="server" 
                    ControlToValidate="ddlFacility" 
                    InitialValue="Select Facility" 
                    ValidationGroup="search" 
                    ErrorMessage="Please select an option from facility." 
                    Display="Dynamic" 
                    ForeColor="Red" 
                    Font-Size="XX-Small" 
                    Font-Italic="true" />
            </div>

            <div class="form-item checkboxCCHD pt-1">
                <asp:CheckBox ID="chkDiagCodes" OnCheckedChanged="chkDiagCodes_CheckedChanged" runat="server" AutoPostBack="true" />
                <label class="checkboxCCHDLabel">Include Reports with CHD/CCHD Diagnosis Codes to Verify</label>
            </div>
        </div>
    </div>
    <div class="row pt-1">
        <div class="col col-lg-12 col-sm-12">
            <div class="row">
                <div class="form-item">
                    <label style="margin-right: 50px; font-weight: 600;">SEARCH BY:</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-1">
        <div class="col col-lg-12 col-sm-12">
            <div class="row">
                <div class="form-item">
                    <label>Child DOB </label>
                    <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" TextMode="Date" ID="txtChildDOB" ValidationGroup="search" onblur="ValidateDate(this);" EnableViewState="true" />
                    <asp:CustomValidator runat="server" ID="CustomValidator3" ValidationGroup="search" ControlToValidate="txtChildDOB" EnableClientScript="true" ErrorMessage="Invalid DOB." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" onkeypress="return false;" onpaste="return false;" />
                </div>
                <div class="form-item">
                    <label>Child MRN </label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtChildMRN" ValidationGroup="search" TextMode="SingleLine" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtChildMRN" ValidationGroup="search" ValidationExpression="^[\s\S]{1,}$" runat="server" ErrorMessage="Invalid MRN. MRN must be digits." ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                </div>

                <div class="form-item">
                    <label for="Content_txtDateFrom">Admission Date</label>
                    <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" ID="txtAdminDate" TextMode="Date" ValidationGroup="search" onblur="ValidateFromDate(this);" EnableViewState="true" />
                    <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ID="CustValAdmiDate" ValidationGroup="search" ControlToValidate="txtAdminDate" ErrorMessage="Invalid Date." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
                </div>
                <div class="form-item">
                    <label>Discharge Date</label>
                    <asp:TextBox runat="server" CssClass="cursor-pointerWhiteNewField customDateField" ID="txtDisDate" TextMode="Date" ValidationGroup="search" onblur="ValidateToDate(this);" EnableViewState="true" />
                    <asp:CustomValidator EnableClientScript="true" onkeypress="return false;" onpaste="return false;" runat="server" ID="CustValDisDate" ValidationGroup="search" ControlToValidate="txtDisDate" ErrorMessage="Invalid Date." Display="Dynamic" ForeColor="Red" Font-Size="XX-Small" Font-Italic="true" />
                </div>

                <div class="form-item">
                    <asp:Button Text="Search" runat="server" ID="btnSearch" ValidationGroup="search" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-danger" />
                </div>
                <div class="form-item">
                    <asp:Button Text="Reset " runat="server" ID="btnReset" CausesValidation="false" OnClientClick="return resetValidation();" OnClick="btnReset_Click" CssClass="btn btn-danger" />
                </div>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="Please enter search criteria." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>


    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="noRec">
        <p id="connSuccessMsg"></p>
    </div>


    <script>

        $('.row').on('keydown', 'input, select, textarea', function (event) {

            if (event.key == "Enter" || event.keyCode == 13) {
                event.preventDefault();
                document.getElementById("<%=btnSearch.ClientID%>").click();
                    }
                });

        /*** will trigger search button by enter ***/

        function resetValidation() {

            document.getElementById("<%=txtChildDOB.ClientID %>").value = "";
            document.getElementById("<%=txtChildMRN.ClientID %>").value = "";
            document.getElementById("<%=txtAdminDate.ClientID %>").value = "";
            document.getElementById("<%=txtDisDate.ClientID %>").value = "";

            document.getElementById("<%=ddlFacility.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=ddlVeriStatus.ClientID %>").selectedIndex = 0;
            document.getElementById("<%=isResetClick.ClientID %>").value = "true";
            var gridView = document.getElementById("<%= gvDynamic.ClientID %>");
            var pageIndex = 0;
            gridView.PageIndex = pageIndex;

        }

        function noRecFound(message) {

            $("#noRec").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    /*var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')*/
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }        

        function hideReport()
        {
            var $searchReportElement = $("#<%=gvDynamic.ClientID%>");

            if ($searchReportElement.length)
            {
                $searchReportElement.hide();
            }
        }

        function ShowProgress()
        {
            hideReport();

            var isValid = Page_ClientValidate("search");

            if (!isValid) {

                $("#btnSearch").prop("disabled", true);
            }
            else {

                $("#btnSearch").prop("disabled", false);

                var modal = $('<div />');
                modal.addClass("modalspin");
                $('body').append(modal);
                var loading = $(".loadingspin");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }

        function ValidateToDate(input)
        {
            var txtTo = document.getElementById("<%=txtDisDate.ClientID %>").value;
            var toDateValidator = document.getElementById("<%=CustValDisDate.ClientID %>");

            if (txtTo.length === 10) {

                if (txtTo === "undefined" || txtTo === null || txtTo === "") {
                    txtTo = document.getElementById("<%=txtDisDate.ClientID%>").value;
                }

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtTo === "" || txtTo === 'yyyy-mm-dd') {
                    toDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);
                }
                else if (!dateRegex.test(txtTo)) {
                    toDateValidator.style.display = "block";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                    return false;
                }
                else {

                    var minDate = new Date("2018-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtTo);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/2018

                    if (checkDateIfisValid < minDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date must be 01/01/2018 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        toDateValidator.style.display = "block";
                        toDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    <%--toDateValidator.style.display = "none";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", false); --%>

                    var emptytxtFrom = document.getElementById("<%=txtAdminDate.ClientID %>").value;

                    if (emptytxtFrom && emptytxtFrom.length === 10)
                    {
                        var response = ValidateFromDate();

                        if (response)
                        {
                            toDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else {
                            return false;
                        }
                    }   

                    var emptyDOB = document.getElementById("<%=txtChildDOB.ClientID %>").value;

                    if (emptyDOB && emptyDOB.length === 10) {

                        var response = ValidateDate();

                        if (response)
                        {
                            toDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                             return true;
                         }
                        else
                        {
                             return false;
                         }
                    }

                    if (!emptytxtFrom && !emptyDOB)
                    {
                        toDateValidator.style.display = "none";

                        $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                        return true;
                     }
                }
            } else {
                document.getElementById("<%=txtDisDate.ClientID%>").value = "";
            }
        }

        function ValidateFromDate(input) {

            var txtFrom = document.getElementById("<%=txtAdminDate.ClientID%>").value;
            var fromDateValidator = document.getElementById("<%=CustValAdmiDate.ClientID %>");

            if (txtFrom.length === 10) {

                if (txtFrom === "undefined" || txtFrom === null || txtFrom === "") {
                    txtFrom = document.getElementById("<%=txtAdminDate.ClientID%>").value;
                }

                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (txtFrom === "" || txtFrom === 'yyyy-mm-dd') {
                    fromDateValidator.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);
                }
                else if (!dateRegex.test(txtFrom)) {
                    fromDateValidator.style.display = "block";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                    return false;
                }
                else {

                    var minDate = new Date("2018-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(txtFrom);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/2018

                    if (checkDateIfisValid < minDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date must be 01/01/2018 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        fromDateValidator.style.display = "block";
                        fromDateValidator.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                  <%--  fromDateValidator.style.display = "none";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);--%>


                    var emptytxtTo = document.getElementById("<%=txtDisDate.ClientID %>").value;

                    if (emptytxtTo && emptytxtTo.length === 10)
                    {
                        var response = false;

                        response = ValidateToDate();

                        if (response)
                        {
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                    var emptyDOB = document.getElementById("<%=txtChildDOB.ClientID %>").value;

                    if (emptyDOB && emptyDOB.length === 10) {

                        var response = false;

                        response = ValidateDate();

                        if (response) {
                            fromDateValidator.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else {
                            return false;
}
                    }

                    if (!emptytxtTo && !emptyDOB)
                    {
                        fromDateValidator.style.display = "none";

                        $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                         return true;
                     }
                }
            } else {
                document.getElementById("<%=txtAdminDate.ClientID%>").value = "";
            }
        }

        function ValidateDate(input) {

            var enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;

            var validatorChildDOB = document.getElementById("<%=CustomValidator3.ClientID%>");

            if (enteredChildDOB.length === 10) {

                if (enteredChildDOB === "undefined" || enteredChildDOB === null || enteredChildDOB === "") {
                    enteredChildDOB = document.getElementById("<%=txtChildDOB.ClientID%>").value;
                }


                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (enteredChildDOB === "" || enteredChildDOB === 'yyyy-mm-dd') {
                    validatorChildDOB.style.display = "none";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);
                }
                else if (!dateRegex.test(enteredChildDOB)) {
                    validatorChildDOB.style.display = "block";
                    $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                    return false;
                }
                else {

                    var minDate = new Date("2018-01-01");
                    // parse the date

                    var checkDateIfisValid = new Date(enteredChildDOB);

                    //check if it is validate date or not

                    if (isNaN(checkDateIfisValid.getTime())) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Invalid Date";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is before 01/01/2018

                    if (checkDateIfisValid < minDate) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Date must be 01/01/2018 or later.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                    //check if the date is after today's date

                    var maxDate = new Date();

                    if (checkDateIfisValid > maxDate) {
                        validatorChildDOB.style.display = "block";
                        validatorChildDOB.innerHTML = "Date cannot be in the future.";
                        $("#<%=btnSearch.ClientID%>").prop("disabled", true);
                        return false;
                    }

                  <%--  validatorChildDOB.style.display = "none";

                    $("#<%=btnSearch.ClientID%>").prop("disabled", false);--%>

                    var emptytxtFrom = document.getElementById("<%=txtAdminDate.ClientID %>").value;

                    if (emptytxtFrom && emptytxtFrom.length === 10)
                    {
                        var response = false;

                        response = ValidateFromDate();

                        if (response)
                        {
                            validatorChildDOB.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                    var emptytxtTo = document.getElementById("<%=txtDisDate.ClientID %>").value;

                    if (emptytxtTo && emptytxtTo.length === 10) {                       

                        var response = false;

                        response = ValidateToDate();

                        if (response)
                        {
                            validatorChildDOB.style.display = "none";

                            $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }

                    if (!emptytxtTo && !emptytxtFrom)
                    {
                        validatorChildDOB.style.display = "none";

                        $("#<%=btnSearch.ClientID%>").prop("disabled", false);

                        return true;
                    }
                }
            } else {
                document.getElementById("<%=txtChildDOB.ClientID%>").value = "";
            }
        }

        // Attach spnner to pagination buttons

        document.addEventListener('DOMContentLoaded', function () {

            var gridView = document.getElementById('<%= gvDynamic.ClientID %>');

            if (gridView) {
                gridView.addEventListener('click', function (e) {

                    var target = e.target;

                    if (target && target.tagName === 'A') {
                        ShowProgress();
                    }
                });
            }
        });


    </script>

    <% } %>
</asp:Content>

