﻿<%@ Page Title="" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="FileInfoDataView.aspx.cs" Inherits="FileInfoDataView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" Runat="Server">
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg"> 
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" />

            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label for="Content_txtSearch">Search by Keyword</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch" ValidationGroup="search" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSearch" ValidationGroup="search" ValidationExpression="[a-zA-Z0-9]*[^!@%~?:#$%^&*=()0']*" runat="server" ErrorMessage="Invalid Input" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator> 
            </div>
            <div class="form-item">
                <label for="Content_ddlColumn">In Column</label>
                <asp:DropDownList runat="server" ID="ddlColumn" CssClass="form-control" ValidationGroup="search">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="ddlColumn"></asp:RequiredFieldValidator>
            </div>
            <div class="form-item">
                <label for="Content_txtDateFrom">Date From</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDateFrom" ValidationGroup="search" TextMode="Date" />
            </div>
            <div class="form-item">
                <label for="Content_txtDateTo">Date To</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDateTo" ValidationGroup="search" TextMode="Date" />
            </div>
            <div class="form-item">
                <asp:Button Text="Search" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-success" ValidationGroup="search" />
            </div>
            <div class="form-item">
                <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click" CssClass="btn btn-danger"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label for="Content_ddlTotintTables">Filter by Facility</label>
                <asp:DropDownList runat="server" ID="ddlTotintTables" AutoPostBack="true" OnSelectedIndexChanged="ddlTotintTables_SelectedIndexChanged" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="form-item">
                <label for="Content_ddlErrorType">Filter by Error Type</label>
                <asp:DropDownList runat="server" ID="ddlErrorType" AutoPostBack="true" OnSelectedIndexChanged="ddlErrorType_SelectedIndexChanged" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting"
                    AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true" 
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging" 
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing" 
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="10" PagerSettings-FirstPageText="First" OnRowDataBound="gvDynamic_RowDataBound" 
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>  
        </div>
    </div>
</asp:Content>

