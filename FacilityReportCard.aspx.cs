﻿using DynamicGridView.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
namespace DynamicGridView
{   
    public partial class FacilityReportCard : System.Web.UI.Page
    {
        protected DataTable dtFRCStats;
        protected DataTable dtFRCStatsComment;
        protected string selectedMonth;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }
               

                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";

                if (!IsPostBack)
                {
                    BindColumnsDropDown(schema, tableName);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_Page_Load");
            }
        }
               
        public void BindColumnsDropDown(string schema, string tableName)
        {
            try
            {
                /******************** Facility DropDown ***************************/

                DataTable dtOrganization = new DataTable();
                DataTable dtFacility = new DataTable();
                DataTable dtYear = new DataTable();
                DataTable dtMonth = new DataTable();

                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";
                    string orgOrder = " ORDER BY submitter ASC";
                    string facOrder = " ORDER BY facility ASC";
                    string yearOrder = " ORDER BY year ASC";
                    string monthOrder = " ORDER BY month ASC";
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter da = new SqlDataAdapter();
                    //Organazation 

                    queryList = string.Format("SELECT distinct submitter,submitterId FROM {0}.{1}{2}", schema, tableName, orgOrder);
                    dtOrganization = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtOrganization);
                    ddlOrg.DataSource = dtOrganization;
                    ddlOrg.DataTextField = "submitter";
                    ddlOrg.DataValueField = "submitterId";
                    ddlOrg.DataBind();
                    ddlOrg.Items.Insert(0, "Select Submitter/Organization");


                    //Facility 

                    queryList = string.Format("SELECT distinct facility,entityId FROM {0}.{1}{2}", schema,tableName, facOrder);
                    dtFacility = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtFacility);
                    ddlFacility.DataSource = dtFacility;
                    ddlFacility.DataTextField = "facility";
                    ddlFacility.DataValueField = "entityId";
                    ddlFacility.DataBind();
                    ddlFacility.Items.Insert(0, "Select Facility");
                  
                    //Year 
                    queryList = string.Format("SELECT distinct year FROM {0}.{1}{2}", schema, tableName, yearOrder);
                    dtYear = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtYear);

                    foreach (DataRow dr in dtYear.Rows)
                    {
                        if (dr.IsNull("year"))
                        {
                            dr.Delete();
                            break;
                        }
                    }
                    ddlYear.DataSource = dtYear;
                    ddlYear.DataTextField = "year";
                    ddlYear.DataValueField = "year";
                    ddlYear.DataBind();
                    ddlYear.Items.Insert(0, "Select Year");
                   

                    //Month 
                    queryList = string.Format("SELECT distinct month FROM {0}.{1}{2}", schema, tableName, monthOrder);
                    dtMonth = new DataTable();
                    cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtMonth);
                    foreach (DataRow dr in dtMonth.Rows)
                    {
                        if (dr.IsNull("month"))
                        {
                            dr.Delete();
                            break;
                        }
                    }
                    ddlMonth.DataSource = dtMonth;
                    ddlMonth.DataTextField = "month";
                    ddlMonth.DataValueField = "month";
                    ddlMonth.DataBind();
                    ddlMonth.Items.Insert(0, "Select Month");
                    
                    ddlFile.DataSource = null;
                    ddlFile.DataBind();
                    ddlFile.Items.Insert(0, "Select File");
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_BindColumnsDropDown");
            }
        }
        
        public DataTable GetReportDetails()
        {
            DataTable dtgetInfo = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string facilitiy = ddlFacility.SelectedValue;
                    string query = string.Empty;

                    if (!string.IsNullOrEmpty(facilitiy) && facilitiy == "All Facilities")
                    {
                        query = "select auditId,comment,created,loadId,submitterId,organizationName,entityId,facilityName,userid,displayName from [MBDR_System].[FRCReports] where loadId='" + ddlFile.SelectedValue + "' and submitterId ='" + ddlOrg.SelectedValue + "' and entityId is null order by created desc";
                    }
                    else
                    {
                        query = "select auditId,comment,created,loadId,submitterId,organizationName,entityId,facilityName,userid,displayName from [MBDR_System].[FRCReports] where loadId='" + ddlFile.SelectedValue + "' and entityId ='" + ddlFacility.SelectedValue + "' and submitterId ='" + ddlOrg.SelectedValue + "' and entityId is not null order by created desc";
                    }

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    
                    da.Fill(dtgetInfo);                    
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_GetReportDetails");
            }
            return dtgetInfo;
        }

        protected void InsertUpdate()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();                    
                    string entityId = "null";
                    string facilityName = null;
                    
                    if (ddlFacility.SelectedValue != "All Facilities")
                    {
                        entityId = ddlFacility.SelectedValue;
                        facilityName = ddlFacility.SelectedItem.ToString();
                    }

                    string inserQuery = string.Empty;
                   
                    txtComment.Attributes["placeholder"] = " -- Type Here -- ";

                    inserQuery = "insert into [MBDR_System].[FRCAudit] (loadId,entityId,userId,created,comment) values ('" + ddlFile.SelectedValue + "'," + entityId + ",'" + Session["userid"] + "','" + DateTime.Now + "',null)";

                    cmd = new SqlCommand(inserQuery, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    int rowsAffected = cmd.ExecuteNonQuery();
                    conn.Close();
                    
                    dtFRCStatsComment = GetReportDetails();

                    hdauditUpdate.Value = "false";
                    hdauditId.Value = Convert.ToString(dtFRCStatsComment.Rows[0]["auditId"]);                    
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_InsertUpdate");
            }
        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            try
            {
                // Approach below allow the single or double quotes while in the comments 

                string comment = "" + txtComment.Text + "";

                string updatequery = "UPDATE [MBDR_System].[FRCAudit] SET comment = @comment WHERE auditId = @auditId";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    //string updatequery = "update [MBDR_System].[FRCAudit] set comment='" + comment + "' where auditId=" + hdauditId.Value + "";
                    using (SqlCommand cmd = new SqlCommand(updatequery, conn))
                    {
                        cmd.Parameters.AddWithValue("@comment", comment);
                        cmd.Parameters.AddWithValue("@auditId", hdauditId.Value);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }

                BindGrid();

                hdauditUpdate.Value = "true";
                dtFRCStatsComment = GetReportDetails();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_btnComment_Click");
            }
        }

        public void BindGrid()
        {
            try
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";

                    string passParam = "";

                    if (ddlOrg.SelectedIndex != 0 && ddlFacility.SelectedIndex == 1)
                    {
                        /* Organization-level Function MBDR_System.getFRCStatsOrg(@submitterId int, @loadId int, @year int, @month int) */

                        if (rdMonth.Checked)
                        {
                            passParam = "(" + ddlOrg.SelectedValue + "," + ddlFile.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlMonth.SelectedValue + ")";
                            query = string.Format("SELECT * FROM {0}.{1} {2} ", schema, "getFRCStatsOrg", passParam);
                        }

                        /* Organization-level Function MBDR_System.getFRCStatsQuarterOrg(@submitterId int, @year int, @qtr int) */

                        else if (rdQuarter.Checked) 
                        {
                            passParam = "(" + ddlOrg.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlQuarter.SelectedValue + ")";

                            query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsQuarterOrg", passParam, " Order by case when month is null then 2 else 1 end, month;");
                        } 
                        else
                        {
                            // MBDR_System.getFRCStatsAnnualOrg (@submitterId int, @year int) 
                            // select* from MBDR_System.getFRCStatsAnnualOrg(18, 2024)

                            passParam = "(" + ddlOrg.SelectedValue + "," + ddlYear.SelectedValue + ")";

                            query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsAnnualOrg", passParam, " Order by case when quarter is null then 2 else 1 end, quarter;");
                        }                    
                    }
                    else
                    {
                        /* Facility-level Function MBDR_System.getFRCStatsOrg( @entityId int, @loadId int, @year int, @month int) */

                        if (rdMonth.Checked)
                        {
                            passParam = "(" + ddlFacility.SelectedValue + "," + ddlFile.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlMonth.SelectedValue + ")";

                            query = string.Format("SELECT * FROM {0}.{1} {2} ", schema, "getFRCStats", passParam);
                        }

                        /* Facility-level Function MBDR_System.getFRCStatsQuarter(@entityId int, @year int, @qtr int) */

                        else if (rdQuarter.Checked) 
                        {
                            passParam = "(" + ddlFacility.SelectedValue + "," + ddlYear.SelectedValue + "," + ddlQuarter.SelectedValue + ")";

                            query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsQuarter", passParam, " Order by case when month is null then 2 else 1 end, month;");
                        }
                        else
                        {
                            //MBDR_System.getFRCStatsAnnual(@entityId int, @year int)

                            passParam = "(" + ddlFacility.SelectedValue + "," + ddlYear.SelectedValue + ")";

                            query = string.Format("SELECT * FROM {0}.{1} {2} {3}", schema, "getFRCStatsAnnual", passParam, " Order by case when quarter is null then 2 else 1 end, quarter;");
                        }
                    }

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    
                    dtFRCStats = new DataTable();
                    
                    da.Fill(dtFRCStats);

                    if (dtFRCStats.Rows.Count == 0 || dtFRCStats == null )
                    {
                        Session["ErrorOccurred"] = true;
                                           
                        ClientScript.RegisterStartupScript(this.GetType(), "Empty Result", "cAlertMsgs('Your request did not return any results. Please change your selection and try again.', 'No Result');", true);
                        return;
                    }

                    searchreport.Visible = true;
                    if (rdQuarter.Checked)
                    {
                        btnPrint.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        private string GetWhereClause()
        {
            string where = " where 1 = 1";

            try
            {
                string admittingEntity = !string.IsNullOrEmpty(Request.QueryString["admittingEntity"]) ? Convert.ToString(Request.QueryString["admittingEntity"]) : "";
                string reportStatus = !string.IsNullOrEmpty(Request.QueryString["reportStatus"]) ? Convert.ToString(Request.QueryString["reportStatus"]) : "";

                string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);
          
                if (admittingEntity != "Select Facility")
                {
                    where += " and entityId =" + selectedadmittingEntity + "";
                }

                string selectedyear = Convert.ToString(ddlYear.SelectedValue);

                if (selectedyear != "Select Facility")
                {
                    where += " and year =" + selectedyear + "";
                }

                string selectedmonth = Convert.ToString(ddlMonth.SelectedValue);

                if (selectedmonth != "Select Month")
                {
                    where += " and month =" + selectedmonth + "";
                }

                string selectedLoadId = Convert.ToString(ddlFile.SelectedValue);

                if (selectedLoadId != "Select File")
                {
                    where += " and loadId =" + selectedLoadId + "";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_GetWhereClause");
            }

            return where;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            { 
                BindGrid();

                if(Convert.ToBoolean(Session["ErrorOccurred"]) != true)
                {
                    if (rdMonth.Checked)
                    {
                        InsertUpdate();
                    }
                }

                Session["ErrorOccurred"] = false;

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_btnSearch_Click");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ddlOrg.SelectedIndex = 0;
                ddlFacility.SelectedIndex = 0;
                ddlFile.SelectedIndex = 0;
                ddlYear.SelectedIndex = 0;
                ddlMonth.SelectedIndex = 0;

                Response.Redirect("FacilityReportCard.aspx?schema=MBDR_System&table=FRCLists", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_btnReset_Click");
            }
        }
       
        private void dropdownFileUpdate()
        {
            try
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";

                if ( ddlOrg.SelectedIndex != 0 && ddlFacility.SelectedIndex != 0 && ddlYear.SelectedIndex != 0 && ddlMonth.SelectedIndex != 0)
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string where = "where 1=1";

                        string selectedadmittingEntity = Convert.ToString(ddlFacility.SelectedValue);

                        if (selectedadmittingEntity != "Select Facility" && selectedadmittingEntity != "All Facilities")
                        {
                            where += " and entityId = " + selectedadmittingEntity + "";
                        }

                        string selectedOrgEntity = Convert.ToString(ddlOrg.SelectedValue);

                        if (selectedOrgEntity != "Select Submitter/Organization" && selectedadmittingEntity == "All Facilities")
                        {
                            where += " and submitterId = " + selectedOrgEntity + "";
                        }                  

                        string selectedyear = Convert.ToString(ddlYear.SelectedValue);

                        if (selectedyear != "Select Facility")
                        {
                            where += " and year = " + selectedyear + "";
                        }

                        string selectedmonth = Convert.ToString(ddlMonth.SelectedValue);

                        if (selectedmonth != "Select Month")
                        {
                            where += " and month = " + selectedmonth + "";
                        }

                        string queryList = string.Format("SELECT DISTINCT filename,loadId FROM {0}.{1} {2}", schema, tableName, where);
                        DataTable dtEntityList = new DataTable();
                        SqlCommand cmd = new SqlCommand(queryList, conn);
                        cmd.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();

                        da.Fill(dtEntityList);
                        ddlFile.DataSource = dtEntityList;
                        ddlFile.DataTextField = "fileName";
                        ddlFile.DataValueField = "loadId";

                        ddlFile.DataBind();

                        if (dtEntityList.Rows.Count == 0)
                        {
                            ddlFile.Items.Insert(0, "No file found");
                            btnSearch.Enabled = false;
                            btnSearch.CssClass = "cursor-default";
                        }
                        else
                        {
                            ddlFile.Items.Insert(0, "Select File");
                            btnSearch.Enabled = true;
                            btnSearch.CssClass = "cursor-pointer";
                        }
                    }
                }
                else 
                {
                    btnPrint.Visible = false;
                }
            }
            catch (Exception ex)
            {

                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_dropdownFileUpdate"); 
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropdownFileUpdate();
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropdownFileUpdate();
        }

        protected void ddlFacility_SelectedIndexChanged(object sender, EventArgs e)
        {
            dropdownFileUpdate();
        }

        protected void ddlOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["schema"]) ? Convert.ToString(Request.QueryString["schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["table"]) ? Convert.ToString(Request.QueryString["table"]) : "";

                if (ddlOrg.SelectedIndex !=0)
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string where = "where 1=1 ";
                        string selectedOrg = Convert.ToString(ddlOrg.SelectedValue);

                        if (selectedOrg != "Select Submitter/Organization")
                        {
                            where += " and submitterId =" + selectedOrg + "";
                        }

                        string queryList = string.Format("SELECT distinct facility,entityId FROM {0}.{1} {2}", schema, tableName, where);
                        DataTable dtFacility = new DataTable();
                        SqlCommand cmd = new SqlCommand(queryList, conn);
                        cmd.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dtFacility);
                        ddlFacility.DataSource = dtFacility;
                        ddlFacility.DataTextField = "facility";
                        ddlFacility.DataValueField = "entityId";
                        ddlFacility.DataBind();
                        ddlFacility.Items.Insert(0, "Select Facility");
                        ddlFacility.Items.Insert(1, "All Facilities");

                    }
                }                
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_ddlOrg_SelectedIndexChanged");
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string Org = string.Empty;
            string Faciltity = string.Empty;
            string Year = ddlYear.SelectedValue; 
            string Month = ddlMonth.SelectedValue; 
            string Facility = ddlFacility.SelectedValue;
            string File = ddlFile.SelectedValue;
            string url = string.Empty;
            string FacilityText = string.Empty;
            if (ddlOrg.SelectedIndex != 0 && ddlFacility.SelectedIndex == 1)
            {
                Org = ddlOrg.SelectedValue;
            }
            else
            {
               Facility = ddlFacility.SelectedValue;
                FacilityText = ddlFacility.SelectedItem.Text;
            }

            if (rdMonth.Checked)
            {
                url = "FacilityReportCardPrint.aspx?Org=" + Org + "&Facility=" + Facility + "&Year=" + Year + "&Month=" + Month + "&File=" + File + "&FacilityText=" + FacilityText + "&chkIds=" + hdCheckIds.Value + "";

                dtFRCStatsComment = GetReportDetails();
                hdauditUpdate.Value = "false";
                hdauditId.Value = Convert.ToString(dtFRCStatsComment.Rows[0]["auditId"]);
            }
            else if(rdQuarter.Checked) 
            {
                string quarterTextValue = ddlQuarter.SelectedItem.Text.Replace(".", string.Empty); // removing dot from text

                url = "FacilityReportCardQuarterlyPrint.aspx?Facility=" + Facility + "&Year=" + Year + "&quarter=" + ddlQuarter.SelectedValue + "&quarterText=" + quarterTextValue + "&FacilityText=" + FacilityText + "&chkIds=" + hdCheckIds.Value + "&Org=" + ddlOrg.SelectedValue + "";
            } 
            else
            {
                //string quarterTextValue = ddl.SelectedItem.Text.Replace(".", string.Empty); // removing dot from text

                url = "FacilityReportCardYearlyPrint.aspx?Facility=" + Facility + "&Year=" + Year + "&FacilityText=" + FacilityText + "&chkIds=" + hdCheckIds.Value + "&Org=" + ddlOrg.SelectedValue + "";
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Print Result", "window.open(\""+ url + "\",'_blank','width=800,height=600, toolbar=no,menubar=no,scrollbars=no,status=no');chkStatusMaintain('"+ hdCheckIds.Value +"');", true);
            
            BindGrid();            
           
        }

        protected void rdMonth_CheckedChanged(object sender, EventArgs e)
        {
            divFile.Visible= true;
            divMonth.Visible= true;
            divQuarter.Visible = false;
            rdAnnual.Checked = false;
            rdQuarter.Checked = false;

            ddlOrg.SelectedIndex = 0;
            ddlFacility.SelectedIndex = 0;

            ddlFile.Items.Clear();
            ddlFile.DataSource = null;
            ddlFile.DataBind();
            ddlFile.Items.Insert(0, "Select File");

            ddlYear.SelectedIndex = 0;
            ddlMonth.SelectedIndex = 0;
            btnPrint.Visible = false;
        }
        
        protected void QuerterlyChecked()
        {
            divFile.Visible = false;
            divMonth.Visible = false;
            divQuarter.Visible = true;
            rdAnnual.Checked = false;
            rdMonth.Checked = false;
        }

        protected void rdQuarter_CheckedChanged(object sender, EventArgs e)
        {
            QuerterlyChecked();

            ddlOrg.SelectedIndex = 0;
            ddlFacility.SelectedIndex = 0;
            ddlFile.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            ddlMonth.SelectedIndex = 0;
            ddlQuarter.SelectedIndex = 0;
        }

        protected void rdAnnual_CheckedChanged(object sender, EventArgs e)
        {
            divFile.Visible = false;
            divMonth.Visible = false;
            divQuarter.Visible = false;
            rdMonth.Checked = false;
            rdQuarter.Checked = false;

            ddlOrg.SelectedIndex = 0;
            ddlFacility.SelectedIndex = 0;
            ddlFile.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            ddlMonth.SelectedIndex = 0;
            btnPrint.Visible=false;
        }

        protected void btnFacDash_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("FacilityDashboard.aspx", false);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "FacilityReportCard_btnFacDash_Click");
            }
        }
               
    }
}