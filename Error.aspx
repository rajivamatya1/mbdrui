﻿<%@ Page Title="MBDR UI Error" Language="C#" MasterPageFile="~/loginMaster.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <div class="pt-1">

    </div>
    <center>
        <div class="pt-1">
            <asp:Label ID="lblSiteAdminMsg" runat="server" ></asp:Label>
        </div>
       <%-- An error has occurred. Please contact the Site Admin (Altarum). --%>
        <div>
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
            <asp:Label ID="lblAttemptEmail" runat="server"></asp:Label>
        </div>
        <asp:Button ID="btnHideLogin" runat="server" OnClick="btnHideLogin_Click" Visible="false" />
        <asp:Button ID="btnLoginPage" runat="server" OnClick="btnLoginPage_Click" Visible="false" />
    </center>


    <div id="errorMsgs">
        <p id="errorMsgsForAll"></p>
    </div>

     <div id="loginPage">
     <p id="loginPageAll"></p>
 </div>

    <script type="text/javascript">
 <%--       function errorLogOut(message, titleFor) {
          
            var userResponse = confirm(message);
            if (userResponse) {
                 <%= Page.ClientScript.GetPostBackEventReference(btnHideLogin, "OK")  %> 

            }
            else {
                 <%= Page.ClientScript.GetPostBackEventReference(btnHideLogin, "Cancel")  %> 
            }
        }--%>


        function errorMsgs(message, titleFor) {

            $("#errorMsgs").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    /* var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')*/
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                     <%= Page.ClientScript.GetPostBackEventReference(btnHideLogin, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#8B0000");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }

            $(".ui-dialog-titlebar-close").on("click", function () {
         <%= Page.ClientScript.GetPostBackEventReference(btnHideLogin, String.Empty)  %> 
      });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
         <%= Page.ClientScript.GetPostBackEventReference(btnHideLogin, String.Empty)  %> 
         }
     });
        }

        function showLoginPage(message, titleFor) {

            $("#loginPage").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Search Result",

                open: function () {
                    /* var imageConn = $('<img src="Content/css/images/record.svg" width="10" height="10"/>')*/
                    var connMsg = message;
                    $(this).append(connMsg);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                <%= Page.ClientScript.GetPostBackEventReference(btnLoginPage, String.Empty)  %> 
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                },
                closeOnEscape: false
            }).prev(".ui-dialog-titlebar").css("background", "#8B0000");

            if (titleFor) {
                $(".ui-dialog-title").text(titleFor);
            }

            $(".ui-dialog-titlebar-close").on("click", function () {
    <%= Page.ClientScript.GetPostBackEventReference(btnLoginPage, String.Empty)  %> 
               });

            $(document).on("keydown", function (e) {
                if (e.which === 27) {
    <%= Page.ClientScript.GetPostBackEventReference(btnLoginPage, String.Empty)  %> 
           }
       });
        }

    </script>
</asp:Content>
