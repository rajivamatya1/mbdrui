﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public partial class DefaultErrorEditView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        if (!IsPostBack)
        {
            string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
            string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";

            string errorSchema = !string.IsNullOrEmpty(Request.QueryString["errorSchema"]) ? Convert.ToString(Request.QueryString["errorSchema"]) : "";
            string errorTableName = !string.IsNullOrEmpty(Request.QueryString["errorTable"]) ? Convert.ToString(Request.QueryString["errorTable"]) : "";
            string errorHeader = !string.IsNullOrEmpty(Request.QueryString["errorName"]) ? Convert.ToString(Request.QueryString["errorName"]) : "";

            lblErrorSchema.Text = errorSchema;
            lblErrorTable.Text = errorTableName;
            hdfErrorName.Value = errorHeader;
            btnBack.Text = string.Format("Back to {0} table", errorHeader);

            if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
            {
                string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string[] header = model.Table.Split(':');
                if (header.Length > 0)
                {
                    lblHeader.Text = header[1];
                    lblHeader.Visible = true;
                }
                hdfName.Value = headerLabel;
                lblSchema.Text = schema;
                lblTable.Text = tableName;
                BindGrid(schema, tableName);
                if (gvDynamic.Rows.Count > 0)
                {
                    gvDynamic.EditIndex = 0;
                    BindGrid(lblSchema.Text, lblTable.Text);
                }

            }
        }
    }

    public void BindGrid(string schema, string tableName)
    {
        DataTable dt = new DataTable();

        List<GridViewModel> lstFilter = Common.GetFilterList();
        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
        string includeColumns = string.Empty;
        string readonlyColumns = string.Empty;
        string dataKeyname = string.Empty;
        if (model != null)
        {
            includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
            readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
            dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
        }

        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string recordId = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
            string query = "";
            query = string.Format("select * from {0}.{1} where {2} = '{3}'", schema, tableName, dataKeyname, recordId);

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Close();
            da.Fill(dt);
        }



        gvDynamic.Columns.Clear();
        List<string> lstColumns = includeColumns.Split(',').ToList();

        foreach (var col in lstColumns)
        {
            if (!string.IsNullOrEmpty(col))
            {
                foreach (DataColumn item in dt.Columns)
                {
                    string colName = item.ColumnName;
                    string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                    if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                        BoundField field = new BoundField();
                        field.HeaderText = columnInfo;
                        field.DataField = item.ColumnName;
                        field.SortExpression = item.ColumnName;
                        if (readonlyColumns.Contains(colName))
                        {
                            field.ReadOnly = true;
                        }
                        gvDynamic.Columns.Add(field);
                        break;
                    }
                }
            }
        }

        if (schema != "UI_MBDR_ARCHIVE")
        {
            CommandField edit = new CommandField();
            edit.ButtonType = ButtonType.Link;
            edit.EditText = "Edit";
            edit.UpdateText = "Update";
            edit.CancelText = "Process";
            edit.ShowEditButton = true;
            gvDynamic.Columns.Add(edit);
        }


        gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
        gvDynamic.DataSource = dt;
        gvDynamic.DataBind();
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDynamic.PageIndex = e.NewPageIndex;
        {
            ViewState["PageIndex"] = gvDynamic.PageIndex;
        }
        BindGrid(lblSchema.Text, lblTable.Text);
    }

    protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
            GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;

            if (model != null)
            {
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";").ToLower();
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";");
            }
            string query = " set ";
            foreach (DataControlFieldCell cell in row.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    string colName = ((BoundField)cell.ContainingField).DataField;
                    if (!readonlyColumns.Contains(colName) && colName != "CompanyMasterID")
                    {
                        query = query + " [" + ((BoundField)cell.ContainingField).DataField + "]=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                    }
                }
            }
            gvDynamic.EditIndex = -1;
            conn.Open();
            string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

            string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
            SqlCommand cmd = new SqlCommand(finalQuery, conn);
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid(lblSchema.Text, lblTable.Text);
            if (gvDynamic.Rows.Count > 0)
            {
                gvDynamic.EditIndex = 0;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
        }
    }

    protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
    {
        List<GridViewModel> lstFilter = Common.GetFilterList();
        GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
        string tableName = model.StagingTable.Split(':')[0];
        string dataKeyname = string.Empty;
        if (model != null)
        {
            dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
        }

        Response.Redirect("RecordDetail.aspx?Schema=" + model.StagingSchema + "&Table=" + tableName + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value);
    }

    protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDynamic.EditIndex = e.NewEditIndex;
        BindGrid(lblSchema.Text, lblTable.Text);
    }

    protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
            GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;

            if (model != null)
            {
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";").ToLower();
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";");
            }
            string query = " set ";
            foreach (DataControlFieldCell cell in row.Cells)
            {
                if (cell.ContainingField is BoundField)
                {
                    string colName = ((BoundField)cell.ContainingField).DataField;
                    if (!readonlyColumns.Contains(colName) && colName != "CompanyMasterID")
                    {
                        query = query + " [" + ((BoundField)cell.ContainingField).DataField + "]=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                    }
                }
            }
            gvDynamic.EditIndex = -1;
            conn.Open();
            string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

            string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
            SqlCommand cmd = new SqlCommand(finalQuery, conn);
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid(lblSchema.Text, lblTable.Text);
            if (gvDynamic.Rows.Count > 0)
            {
                gvDynamic.EditIndex = 0;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
        }
    }

    protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
        ViewState["SortExpression"] = exp;
        BindGrid(lblSchema.Text, lblTable.Text);
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC"; // Default next sort expression behaviour.
        if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
        {   // Exists... DESC.
            nextDir = "DESC";
            ViewState["sort"] = null;
        }
        else
        {   // Doesn't exists, set ViewState.
            ViewState["sort"] = column;
        }
        return nextDir;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(lblSchema.Text, lblTable.Text);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ViewState["SortExpression"] = null;
        BindGrid(lblSchema.Text, lblTable.Text);
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("DefaultErrorView.aspx?schema=" + lblErrorSchema.Text + "&table=" + lblErrorTable.Text + "&name=" + hdfErrorName.Value + "");
    }
}