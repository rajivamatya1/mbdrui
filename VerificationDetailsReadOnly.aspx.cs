﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class VerificationDetailsReadOnly : System.Web.UI.Page
    {
        dynamic permissions = null;
        protected DataTable dt;
        protected string assignedUserName;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string browserName = Request.Browser.Browser;
                string browserCount = Convert.ToString(Session["BrowserCount"]);
                string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
                string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
                string sessionGuid = Convert.ToString(Session["GuId"]);
                string existingbrowserName = Convert.ToString(Session["BrowserName"]);

                if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
                {
                    string env = ConfigurationManager.AppSettings["environment"];
                    string miMiLogin = String.Empty;
                    if (!string.IsNullOrEmpty(env))
                    {
                        if (env == "dev" || env == "qa")
                        {
                            miMiLogin = "login.aspx";
                        }
                        else
                        {
                            miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                        }
                    }
                    else
                    {
                        miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                    }

                    Response.Redirect(miMiLogin);
                }
                permissions = CheckRole();


                string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";
                string diagnosis = !string.IsNullOrEmpty(Request.QueryString["ICD10Diagnosis"]) ? Convert.ToString(Request.QueryString["ICD10Diagnosis"]) : "";

                if (!IsPostBack)
                {

                    ltrTableName.Text = "DIAGNOSIS VERIFICATION LOG DETAILS FOR CASE REPORT  " + "<b><span class=\"yellow-highlight\" style=\"font-size:22px;\">" + reportId + "</span></b>";

                    //BindCaseReportData("MBDR_System", "CaseVerificationDetails", reportId);

                    BindVerificationStatus("MBDR_System", "CaseVerificationDetails", reportId, diagnosis);
                }


            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetailsReadOnly_Page_Load");
            }
        }

        protected void BindCaseReportData(string schema, string tableName, string reportId)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "where reportId = '" + reportId + "'";
                dt = new DataTable();

                query = string.Format("select * from {0}.{1} {2} ", schema, tableName, where);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }
        }
        public dynamic CheckRole()
        {
            dynamic permissions = null;

            try
            {
                DataTable dtAccessPermission = Common.Common.CheckAccessPermission(Convert.ToString(Session["userid"]));

                if (dtAccessPermission != null && dtAccessPermission.Rows.Count > 0)
                {
                    permissions = dtAccessPermission.AsEnumerable()
                    .Where(permission => permission.Field<string>("PermissionName") == "CASE_WRITE")
                    .Select(permission => new
                    {
                        UserId = permission.Field<string>("userId"),
                        RoleId = permission.Field<string>("RoleId"),
                        RoleName = permission.Field<string>("RoleName"),
                        RoleDetails = permission.Field<string>("RoleDetails"),
                        PermissionId = permission.Field<string>("PermissionId"),
                        PermissionName = permission.Field<string>("PermissionName")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "MatchingViewDetails_CheckRole");
            }
            return permissions;
        }


        private string GetWhereClause()
        {
            string where = " where 1 = 1";

            try
            {
                string reportId = !string.IsNullOrEmpty(Request.QueryString["reportId"]) ? Convert.ToString(Request.QueryString["reportId"]) : "";
                string diagnosis = !string.IsNullOrEmpty(Request.QueryString["ICD10Diagnosis"]) ? Convert.ToString(Request.QueryString["ICD10Diagnosis"]) : "";

                if (!string.IsNullOrEmpty(reportId))
                {
                    string isPrimary = " and isPrimary = 1";
                    where += " and reportId ='" + reportId + "'" + isPrimary;
                }
                if (!string.IsNullOrEmpty(diagnosis))
                {
                    where += " and diagnosis ='" + diagnosis + "'";
                }

            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetailsReadOnly_GetWhereClause");
            }

            return where;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where 1=1 ";
                    int pageIndex = 0;

                    where = GetWhereClause();

                    query = string.Format("select count({0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetailsReadOnly_GetTotalRecords");
            }
            return totalRecords;
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetailsReadOnly_GetSortDirection");
            }
            return nextDir;
        }



        /****************************************/
        /*  VerificationDetails */
        /****************************************/


        public void BindVerificationStatus(string schema, string tableNameVeri, string reportId, string diagnosis)
        {
            try
            {
                DataTable vdt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableNameVeri);
                        command.CommandTimeout = 0;

                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "reportId")
                            {
                                sortExp = "reportId desc";
                                break;
                            }
                        }
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderByCRec = sortExp;
                    OrderByCRec = "verificationDate desc";
                    int pageIndex = 0;

                    where = GetWhereClause();

                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {6} ) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4}) ", schema, tableNameVeri, "reportId", ((gvVerificationStatus.PageIndex * gvVerificationStatus.PageSize) + 1), (gvVerificationStatus.PageIndex + 1) * gvVerificationStatus.PageSize, where, OrderByCRec);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(vdt);

                    gvVerificationStatus.DataSource = vdt;
                    gvVerificationStatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "VerificationDetailsReadOnly_BindVerificationStatus");
            }
        }

        protected void gvVerificationStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {                
                /******************** Verification Status DropDown ***************************/

                DataTable dtStatus = new DataTable();
                DataTable dtStatusList = new DataTable();
                DataTable dtMethodList = new DataTable();
                List<DropDownModal> lstStatusColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstStatusTables = new List<DropDownModal>();
                string statusId = !string.IsNullOrEmpty(Request.QueryString["statusId"]) ? Convert.ToString(Request.QueryString["statusId"]) : "";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";
                    queryList = string.Format("select distinct vs.details, vs.statusId from [MBDR_System].[VerificationStatuses] vs order by details asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtStatusList);

                }

                /******************** Verification Methods choices ***************************/

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    string queryList = "";
                    queryList = string.Format("Select * from [MBDR_System].[VerificationMethods] order by methodId asc");
                    SqlCommand cmd = new SqlCommand(queryList, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dtMethodList);

                }

                /******************** User Name  ***************************/

                DataSet pplRow = new DataSet();

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string pplSchema = "MBDR_System";
                    string pplTbl = "Users";
                    int userName = Convert.ToInt32(Session["userid"]);
                    string query = "";
                    query = string.Format(@"SELECT ppl.firstName FROM {0}.{1} ppl WHERE userId = {2}", pplSchema, pplTbl, userName);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(pplRow);
                    if (pplRow.Tables[0].Rows.Count > 0)
                    {
                        assignedUserName = pplRow.Tables[0].Rows[0][0].ToString();
                        hdfUserName.Value = assignedUserName;
                    }
                }


                /******************** DataControlRowType ***************************/

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   /*********** case verification status *************/

                    string verificationStatus = DataBinder.Eval(e.Row.DataItem, "diagnosisStatus").ToString();
                    Label ddlVerifyStatus = (Label)e.Row.FindControl("ddlVerifyStatus");
                    ddlVerifyStatus.Text = verificationStatus;
                    ddlVerifyStatus.Enabled = false;

                    /*********** case verification method *************/

                    CheckBoxList cblList = (CheckBoxList)e.Row.FindControl("idVerfiyMethod");
                    cblList.DataSource = dtMethodList;
                    cblList.DataTextField = "details";
                    cblList.DataValueField = "methodId";
                    cblList.DataBind();

                    string methodIds = DataBinder.Eval(e.Row.DataItem, "methodIds").ToString();

                    string[] methodArray = methodIds.Split(',');

                    CheckBoxList cblFindList = (CheckBoxList)e.Row.FindControl("idVerfiyMethod");

                  
                    foreach (var checkmethodId in methodIds)
                    {
                        for (int i = 0; i < cblFindList.Items.Count; i++)
                        {
                            if (Convert.ToString(cblFindList.Items[i].Value) == Convert.ToString(checkmethodId))
                            {
                                cblFindList.Items[i].Selected = true;
                                cblFindList.Items[i].Enabled = false;
                            }
                            else
                            {
                                cblFindList.Items[i].Enabled = false;
                            }

                        }
                    }

                    /*********** case verification user name  *************/

                    //Label userLabel = (Label)e.Row.FindControl("userName");
                    //userLabel.Text = hdfUserName.Value;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "BindVerificationStatus_gvVerificationStatus_RowDataBound");
            }
        }

        protected void gvVerificationStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}