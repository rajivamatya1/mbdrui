﻿<%@ Page Title="User Roles and Permissions" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="UserManagementRolesPermission.aspx.cs" Inherits="UserManagementRolesPermission" EnableEventValidation="false" %>


<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="Server">
    <%--<link href="Content/css/jquery-ui.css/jquery-ui.css" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <% if (Session["username"] != null)
        { %>
    <h1 class="page-title">User Permissions</h1>
    <!DOCTYPE html>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12">
            <div class="program workarea">
                <div class="program-header">
                     <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
        <asp:HiddenField runat="server" ID="hdfSchema" Value="" />
        <asp:HiddenField runat="server" ID="hdfTableName" Value="" />

                    <div class="row row-no-padding">
                        <div class="col col-lg-4 col-md-4 col-sm-4">
                            <div class="program-icon">
                                <img alt="Grid Icon" src="Content/css/images/grid-icon.svg">
                            </div>
                            <span class="program-title-usermgmt">Existing User Roles and Associated Permissions </span>
                        </div>
                        <div class="col col-lg-8 col-md-6 col-sm-8">
                            <div class="action-buttons-container">
                                <div class="form-item">
                                    <asp:Button ID="btnUserManage" Visible="true" runat="server" Text="User Management" OnClick="btnUserManage_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                    <asp:Button ID="btnUserRole" Visible="true" runat="server" Text="User Role" OnClick="btnUserRole_Click" CssClass="userMgmtNav-button" OnClientClick="return ShowProgress();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-view-matching" id="permissionRoleAccess" visible="true" runat="server">
            <div class="col col-lg-12 col-sm-12">
                <div class="tableFixHead">

                    <% if ((dtPermission != null && dtPermission.Rows.Count > 0) && (dtRoles != null && dtRoles.Rows.Count > 0))
                        {
                    %>
                    <table>
                        <thead>
                            <tr>
                                <th>Permission</th>
                                <% for (int i = 0; i < dtRoles.Rows.Count; i++)
                                    {
                                %>
                                <th><%=dtRoles.Rows[i][0] %> </th>
                                <% } %>
                            </tr>
                        </thead>
                        <tbody>
                            <% for (int i = 0; i < dtPermission.Rows.Count; i++)
                                {
                            %>
                            <tr style="text-align:center;">
                                <% if (Convert.ToString(dtPermission.Rows[i][0]) == "USER_WRITE")
                                    {
                                %>
                                <td style="text-align:left;"><%=dtPermission.Rows[i][0] %>  </td>
                                <% for (int j = 0; j < dtRoles.Rows.Count; j++)
                                    { %>
                                <td>
                                    <% if (CheckAccess(Convert.ToInt32(dtRoles.Rows[j][1]), Convert.ToInt32(dtPermission.Rows[i][1])))
                                        { %>
                                        <input type="checkbox" id="Check_<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" class="chkHeader" name="chkname_hidden" disabled="disabled" checked="checked" value="<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" />
                                        <input type="hidden" name="chkname" value="<%= dtRoles.Rows[j][1] %>-<%= dtPermission.Rows[i][1] %>" /> 
                                    <% }
                                        else
                                        { %>
                                        <input type="checkbox" id="Check_<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" class="chkHeader" name="chkname" disabled="disabled" value="<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" />
                                    <% } %>
                                </td>
                                <%  }
                                    }
                                    else
                                    {  %>

                                <td style="text-align:left;"><%=dtPermission.Rows[i][0] %>  </td>

                                <% for (int j = 0; j < dtRoles.Rows.Count; j++)
                                    {
                                %>
                                <td>
                                    <% if (CheckAccess(Convert.ToInt32(dtRoles.Rows[j][1]), Convert.ToInt32(dtPermission.Rows[i][1])))
                                        { %>
                                        <input type="checkbox" id="Check_<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" class="chkHeader" name="chkname" checked="checked" value="<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" />
                                    <% }
                                        else
                                        { %>
                                         <input type="checkbox" id="Check_<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" class="chkHeader" name="chkname" value="<%=dtRoles.Rows[j][1] %>-<%=dtPermission.Rows[i][1] %>" />
                                    <% } %>
                                </td>

                                <% }
                                    }%>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                    <% } %>
                </div>
                <div class="button-container-saveCancel">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" OnClientClick="return assignSave(event)"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </div>

    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <div id="dialogSave" class="modal" title="Save">
        <p id="messageSave"></p>
    </div>

    <div id="successMessage">
    </div>

    <script>
        function assignSave(event) {
            event.preventDefault();

            $(function () {
                $("#dialogSave").dialog({
                    width: 450,
                    modal: true,
                    dialogClass: "no-close",
                    position: {
                        my: "center center",
                        at: "center center"
                    },
                    title: "Save Changes Confirmation",
                    open: function () {
                        var connErrorMsg = 'You are about to modify permissions for the roles. Are you sure you want to proceed?(Y/N)';
                        $(this).html(connErrorMsg);
                    },
                    buttons: {
                        Yes: function () {
                            allowSave = true;
                            $(this).dialog("close");
                            ShowProgress();
                                 <%= Page.ClientScript.GetPostBackEventReference(btnSave, String.Empty)  %>  
                        },
                        No: function () {
                            $(this).dialog("close");
                            return false;
                        }
                    },
                });
            });
        }



        function ShowProgress(btnEle) {

            var modal = $('<div />');

            modal.addClass("modalspin");

            $('body').append(modal);

            var loading = $(".loadingspin");
            loading.show();

            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

            loading.css({ "position": "center", top: top, left: left });
        }

        function removeProgress() {
            var modal = $('div.modalspin');
            modal.removeClass("modalspin");
            var loading = $(".loadingspin");
            loading.hide();
        }



        function AlertMessage(message) {

            $("#successMessage").dialog({

                width: 450,
                modal: true,
                dialogClass: "no-close",
                title: "Confirmation",

                open: function () {         
                    $(this).append(message);
                },

                buttons: [
                    {
                        text: "Ok",
                        open: function () {
                            $(this).addClass('okcls')
                        },
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ],
                position: {
                    my: "center center",
                    at: "center center"
                }
            }).prev(".ui-dialog-titlebar").css("background", "#00607F");;
        }

    </script>
    <% } %>
</asp:Content>
