﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

public partial class FileLogDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            if (!IsPostBack)
            {
                string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string keyName = !string.IsNullOrEmpty(Request.QueryString["keyName"]) ? Convert.ToString(Request.QueryString["keyName"]) : "";
                string rowID = !string.IsNullOrEmpty(Request.QueryString["rowId"]) ? Convert.ToString(Request.QueryString["rowId"]) : "";
                string name = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";


                hdfRowId.Value = rowID;
               
                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();

                    hdfName.Value = headerLabel;
                    lblSchema.Text = schema;
                    lblTable.Text = tableName;

                    string comparisonTable = tableName;
                    string comparisonSchema = schema;
                    if (model.ComparisonTables.Count > 0)
                    {
                        comparisonSchema = model.ComparisonTables.FirstOrDefault().Schema;
                        comparisonTable = model.ComparisonTables.FirstOrDefault().TableName.Split(':')[0];

                        string[] header = model.ComparisonTables.FirstOrDefault().TableName.Split(':');
                        if (header.Length > 0)
                        {
                            lblHeader.Text = header[1];
                            lblHeader.Visible = true;
                        }
                    }
                    else
                    {
                        string[] header = model.Table.Split(':');
                        if (header.Length > 0)
                        {
                            lblHeader.Text = header[1];
                            lblHeader.Visible = true;
                        }
                    }

                    if (tableName == "FileLoadInfo")
                    {
                        btnReturnAL.Visible = true;
                        btnReturnCRVE.Visible = false;
                        btnReturnAaEL.Visible = true;
                    }
                    else
                    {
                        btnReturnAL.Visible = false;
                        btnReturnCRVE.Visible = true;
                        btnReturnAaEL.Visible = true;
                    }

                    hdfSchema.Value = comparisonSchema;
                    hdfTable.Value = comparisonTable;

                    BindColumnsDropDown(comparisonSchema, comparisonTable);

                    // Temp comment this 

                    //txtSearch.Text = search;

                    //if (!string.IsNullOrEmpty(colName))
                    //{
                    //    ddlColumn.SelectedValue = colName;
                    //}

                    BindGrid(comparisonSchema, comparisonTable);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_Page_Load");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    public void BindColumnsDropDown(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            List<DropDownModal> lstColumnNames = new List<DropDownModal>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";

                query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                
                da.Fill(dt);
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns; 
                    readonlyColumns = model.ReadonlyColumns; 
                }

                foreach (DataColumn item in dt.Columns)
                {
                    if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        string columnInfo = Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                        if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo))
                        {
                            lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                        }

                    }

                }
            }
            ddlColumn.DataSource = lstColumnNames;
            ddlColumn.DataTextField = "Name";
            ddlColumn.DataValueField = "OrigColName";
            ddlColumn.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_");
        }
    }

    public void BindGrid(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;

                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Convert.ToString(item[0]) == "loaded_datetime")
                        {
                            sortExp = " loaded_datetime desc";
                            break;
                        }
                    }
                }
            }

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                dataKeyname = model.DataKeyName; //Common.Common.GetBetween(filterString, "=", ";").Replace("|", "");
            }
            hdfDataKeyName.Value = dataKeyname;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";

                if (tableName == "FileErrorDetails")
                {
                    where = " And errorId in (select errorId from [MBDR_System].[FileErrors] where loadId=" + hdfRowId.Value + ")";
                }
                else
                {
                    where = " And filename = '" + hdfRowId.Value + "'";
                }
                int pageIndex = 0;

                if (!string.IsNullOrEmpty(sortExp))
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        string searchText = Common.ReplaceSQLChar(txtSearch.Text);
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' {8} ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' ) as result where Row between({6}) and ({7}) order by {4}",
                            schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1} {6}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {3}) AS Row,* from {0}.{1}) as result where Row between({4}) and ({5}) order by {2}", schema, tableName, sortExp, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txtSearch.Text))
                    {
                        string searchText = Common.ReplaceSQLChar(txtSearch.Text);
                        pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                        if (!string.IsNullOrEmpty(where))
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%' {7}) as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(where))
                        {
                            where = " where " + where.Substring(5);
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                        }
                        else
                        {
                            query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                        }
                    }
                }
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
            }

            gvDynamic.Columns.Clear();
      
            List<string> lstColumns = includeColumns.Split(',').ToList();

            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 10)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        foreach (DataColumn item in dt.Columns)
                        {
                            string colName = item.ColumnName;
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                BoundField field = new BoundField();
                                field.HeaderText = columnInfo;
                                field.DataField = item.ColumnName;
                                field.SortExpression = item.ColumnName;
                                if (readonlyColumns.Contains(colName))
                                {
                                    field.ReadOnly = true;
                                }
                                gvDynamic.Columns.Add(field);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
            gvDynamic.DataSource = dt;
            gvDynamic.DataBind();

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_BindGrid");
        }
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";

                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    query = string.Format("select count({0}) from {1}.{2} where {3} like '%{4}%'", datakeyName, schema, tableName, Convert.ToString(ddlColumn.SelectedValue), Common.ReplaceSQLChar(txtSearch.Text));
                }
                else
                {
                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, datakeyName, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                }

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;

                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_");
        }
        return totalRecords;
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                ViewState["PageIndex"] = gvDynamic.PageIndex;
            }
            BindGrid(hdfSchema.Value, hdfTable.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_gvDynamic_PageIndexChanging");
        }
    }

    protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(hdfSchema.Value, hdfTable.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_gvDynamic_Sorting");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid(hdfSchema.Value, hdfTable.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_btnSearch_Click");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            txtSearch.Text = string.Empty;
            ddlColumn.SelectedIndex = 0;
            ViewState["SortExpression"] = null;
            if (hdfTable.Value.Contains("FileErrorDetails"))
            {
                BindGrid(hdfSchema.Value, hdfTable.Value);
            }
            else
            {
                BindGrid(lblSchema.Text, hdfTable.Value);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_btnReset_Click");
        }
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC"; // Default next sort expression behaviour.
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {   // Exists... DESC.
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetail_GetSortDirection");
        }
        return nextDir;
    }

    protected void btnReturnAL_Click(object sender, EventArgs e)
    {
        try
        {
            string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
            string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
            string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
            string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

            Response.Redirect("FileSubmissionLog.aspx?schema=MBDR_System&table=FileLoadInfo&name=File%20Submissions%20Log&searchText=" + search + "&colName=" + colName + "&fromDate=" + fromDate + "&toDate=" + toDate + "", false);

            //Response.Redirect("/FileSubmissionLog.aspx?schema=MBDR_System&table=FileLoadInfo&name=File%20Submissions%20Log");
           
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetails_btnReturnAL_Click");
        }
    }

    protected void btnReturnCRVE_Click(object sender, EventArgs e)
    {
        try
        {
            string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
            string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
            string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
            string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

            Response.Redirect("FileSubmissionLog.aspx?schema=MBDR_System&table=ValidationFailures&name=Case%20Reports%20Submission%20Errors&searchText=" + search + "&colName=" + colName + "&fromDate=" + fromDate + "&toDate=" + toDate + "", false);

           // Response.Redirect("/FileSubmissionLog.aspx?schema=MBDR_System&table=ValidationFailures&name=Case%20Reports%20Submission%20Errors");

            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetails_btnReturnCRVE_Click");
        }
    }

    protected void btnReturnAaEL_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("/errorLogs.aspx");
            Context.ApplicationInstance.CompleteRequest();
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "FileLogDetails_btnReturnAaEL_Click");
        }
    }

}