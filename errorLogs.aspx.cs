﻿using System;
using System.Collections.Generic;
using DynamicGridView.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

public partial class errorLogs : System.Web.UI.Page
{
    protected List<BDRTablesList> BDRTables;

    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            BDRTables = new List<BDRTablesList>
        {
            new BDRTablesList() { Name = "File Load Activity Log", DBName = "FileLoadInfo", SCHName = "MBDR_System", Desc = "List of all the files submitted by facilities. After the system attempts to load the files, the success or failure results are logged in this table." },
            new BDRTablesList() { Name = "Case Reports Validation Errors", DBName = "ValidationFailures", SCHName = "MBDR_System", Desc = "List of all the records with missing essential information. Available for reference only." },
        };

        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "errorLogs_Page_Load");
        }
    }

    public class BDRTablesList
    {
        public string Name { get; set; }
        public string DBName { get; set; }
        public string SCHName { get; set; }
        public string Desc { get; internal set; }
    }
}