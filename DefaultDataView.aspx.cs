﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class DefaultDataView : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            try
            {
                if (!IsPostBack)
                {
                    string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                    string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                    string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                    string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                    string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                    string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";
                    if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                    {
                        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                        List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                        string[] header = model.Table.Split(':');
                        if (header.Length > 0)
                        {
                            lblHeader.Text = header[1];
                            lblHeader.Visible = true;
                        }
                        hdfName.Value = headerLabel;
                        lblSchema.Text = schema;
                        lblTable.Text = tableName;
                        BindColumnsDropDown(schema, tableName);
                        txtSearch.Text = null;
                        if (!string.IsNullOrEmpty(colName))
                            ddlColumn.SelectedValue = colName;
                        txtDateFrom.Text = fromDate;
                        txtDateTo.Text = toDate;
                        BindGrid(schema, tableName);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_");
            }
        }

        public void BindColumnsDropDown(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtEntityList = new DataTable();
                List<DropDownModal> lstColumnNames = new List<DropDownModal>();
                List<DropDownModal> lstArchiveEntityTables = new List<DropDownModal>();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";

                    query = string.Format("select top 1 * from {0}.{1}", schema, tableName);
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    
                    da.Fill(dt);
                    
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();

                    GridViewModel model = lstFilter.Where(s => s.Schema == schema && s.Table.StartsWith(tableName)).FirstOrDefault();
                    string includeColumns = string.Empty;
                    string readonlyColumns = string.Empty;
                    if (model != null)
                    {
                        includeColumns = model.IncludedColumns; //Common.Common.GetBetween(filterString, "Include", "and");
                        readonlyColumns = model.ReadonlyColumns; //Common.Common.GetBetween(filterString, "only", ";");
                    }

                    foreach (DataColumn item in dt.Columns)
                    {
                        if (includeColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0 || readonlyColumns.IndexOf(item.ColumnName, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            string columnInfo = Common.Common.GetBetween(includeColumns, item.ColumnName + ":", ",");
                            if (!string.IsNullOrEmpty(columnInfo) && !string.IsNullOrWhiteSpace(columnInfo) && columnInfo != "City" && columnInfo != "County"
                                && columnInfo != "State" && columnInfo != "Country" && columnInfo != "Zip/Postal Code" && columnInfo != "Child SSN" && columnInfo != "Child MRN"
                                && columnInfo != "Sex" && columnInfo != "Plurality" && columnInfo != "Vital Status" && columnInfo != "Birth Weight"
                                && columnInfo != "Child Medicaid Number" && columnInfo != "Birth Date" && columnInfo != "Birth Hospital" && columnInfo != "MomSSN"
                                && columnInfo != "Mom Last Name" && columnInfo != "Mom First Name" && columnInfo != "Mom Middle Name" && columnInfo != "Mom Suffix"
                                && columnInfo != "Admitting/Reporting Entity" && columnInfo != "Admitting Entity Code" && columnInfo != "Admission Source"
                                && columnInfo != "Discharge Status" && columnInfo != "Discharge Date" && columnInfo != "ICD9 Syndrome Code"
                                && columnInfo != "ICD10 Syndrome Code" && columnInfo != "Cytogenetics Testing" && columnInfo != "ICD9 Cytogenetics Diagnosis Code"
                                && columnInfo != "ICD10 Cytogenetics Diagnosis Codes" && columnInfo != "Lab Code" && columnInfo != "Cytogenetics Lab Report Number"
                                && columnInfo != "Head Circumference" && columnInfo != "Delivery Length" && columnInfo != "Infant Postnatal Echo"
                                && columnInfo != "Age First Postnatal Echo" && columnInfo != "Date First Postnatal Echo" && columnInfo != "Infant Admitted to Hospital"
                                && columnInfo != "Age Infant Admitted To Hospital" && columnInfo != "Age Infant Discharge From Hospital"
                                && columnInfo != "Age Infant Admitted to ICU" && columnInfo != "Date Infant Admitted To ICU" && columnInfo != "Birth Order"
                                && columnInfo != "Mother SSN" && columnInfo != "Admission Date" && columnInfo != "Patient Type" && columnInfo != "CPT Procedure Codes"
                                && columnInfo != "ICD10 Diagnosis Codes Status" && columnInfo != "ICD10 Procedure Codes" && columnInfo != "ICD9 Diagnosis Codes"
                                && columnInfo != "ICD9 Procedure Codes" && columnInfo != "Loaded DateTime" && columnInfo != "Disease Group" && columnInfo != "Disease Subgroup" && columnInfo != "Version" && columnInfo != "BPA Description" && columnInfo != "Label" && columnInfo != "Early On Group" && columnInfo != "CSHCS Eligibility" && columnInfo != "Version" && columnInfo != "Start Month" && columnInfo != "Start Day" && columnInfo != "Start Year" && columnInfo != "End Month" && columnInfo != "End Day" && columnInfo != "End Year" && columnInfo != "Code Group" && columnInfo != "MDHHS Code" && columnInfo != "Address Name" && columnInfo != "Address City" && columnInfo != "Unique Name" && columnInfo != "Unique County Zip" && columnInfo != "County Zipcode Name" && columnInfo != "Unique Name To Zipcode" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease" && columnInfo != "Disease")
                            {
                                lstColumnNames.Add(new DropDownModal { Name = columnInfo, OrigColName = item.ColumnName });
                            }
                        }
                    }
                }
                ddlColumn.DataSource = lstColumnNames;
                ddlColumn.DataTextField = "Name";
                ddlColumn.DataValueField = "OrigColName";
                ddlColumn.DataBind();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_BindColumnsDropDown");
            }
        }

        public void BindGrid(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "loaded_datetime")
                            {
                                sortExp = " loaded_datetime desc";
                                dvFromDate.Visible = true;
                                dvToDate.Visible = true;
                                break;
                            }
                        }
                    }
                }

                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    int pageIndex = 0;
                    if (!string.IsNullOrEmpty(txtDateFrom.Text))
                    {
                        where += " and loaded_datetime >='" + txtDateFrom.Text + " 00:00:00' ";
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text))
                    {
                        where += " and loaded_datetime <='" + txtDateTo.Text + " 23:59:59' ";
                    }

                    if (!string.IsNullOrEmpty(sortExp))
                    {
                        if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                        {

                            string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text.Trim());
                            pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                            if (!string.IsNullOrEmpty(where))
                            {
                                if (ddlColumn.SelectedValue == "child_birthdate" || ddlColumn.SelectedValue == "LastUpdated" || ddlColumn.SelectedValue == "discharge_date" || ddlColumn.SelectedValue == "date_first_postnatal_echo" || ddlColumn.SelectedValue == "age_infant_admitted_icu")
                                {
                                    where += " and " + ddlColumn.SelectedValue + " >= '" + txtSearch.Text.Trim() + " 00:00:00'  and " + ddlColumn.SelectedValue + " <='" + txtSearch.Text.Trim() + " 23:59:59' ";
                                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2}  {8} ) as result where Row between({6}) and ({7}) order by {4}",
                                  schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                                }
                                else
                                {
                                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' {8} ) as result where Row between({6}) and ({7}) order by {4}",
                                    schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                                }
                            }
                            else
                            {
                                if (ddlColumn.SelectedValue == "child_birthdate" || ddlColumn.SelectedValue == "LastUpdated" || ddlColumn.SelectedValue == "discharge_date" || ddlColumn.SelectedValue == "date_first_postnatal_echo" || ddlColumn.SelectedValue == "age_infant_admitted_icu")
                                {
                                    where += ddlColumn.SelectedValue + " >='" + txtSearch.Text.Trim() + " 00:00:00'  and " + ddlColumn.SelectedValue + " <='" + txtSearch.Text.Trim() + " 23:59:59' ";
                                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where  {8} ) as result where Row between({6}) and ({7}) order by {4}",
                                  schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                                }
                                else
                                {
                                    query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {2} like '%{3}%' ) as result where Row between({6}) and ({7}) order by {4}",
                                schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                        {
                            string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text.Trim());
                            pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                            if (!string.IsNullOrEmpty(where))
                            {
                                query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%' {7}) as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                            }
                            else if (ddlColumn.SelectedValue == "lastModified" || ddlColumn.SelectedValue == "LastUpdated" || ddlColumn.SelectedValue == "ModifiedDate" || ddlColumn.SelectedValue == "lastContact")
                            {
                                where += ddlColumn.SelectedValue + " >= '" + txtSearch.Text.Trim() + " 00:00:00'  and " + ddlColumn.SelectedValue + " <='" + txtSearch.Text.Trim() + " 23:59:59' ";
                                query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {5}) AS Row,* from {0}.{1} where {8}) as result where Row between({6}) and ({7}) order by {2}",
                              schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, sortExp, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize, where);
                            }
                            else
                            {
                                query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {4}) AS Row,* from {0}.{1} where {2} like '%{3}%') as result where Row between({5}) and ({6})", schema, tableName, Convert.ToString(ddlColumn.SelectedValue), searchText, dataKeyname, pageIndex * gvDynamic.PageSize, (pageIndex + 1) * gvDynamic.PageSize);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(where))
                            {
                                where = " where " + where.Substring(5);
                                query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1} {5}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where);
                            }
                            else
                            {
                                query = string.Format("select * from (SELECT ROW_NUMBER() OVER(ORDER BY {2}) AS Row,* from {0}.{1}) as result where Row between({3}) and ({4})", schema, tableName, dataKeyname, gvDynamic.PageIndex * gvDynamic.PageSize, (gvDynamic.PageIndex + 1) * gvDynamic.PageSize);
                            }
                        }
                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                }

                gvDynamic.Columns.Clear();
                CommandField commandField = new CommandField();
                commandField.ButtonType = ButtonType.Link;
                commandField.HeaderText = "Action";
                commandField.ShowEditButton = true;
                gvDynamic.Columns.Add(commandField);

                List<string> lstColumns = includeColumns.Split(',').ToList();

                foreach (var col in lstColumns)
                {
                    if (gvDynamic.Columns.Count < 13)
                    {
                        if (!string.IsNullOrEmpty(col))
                        {
                            foreach (DataColumn item in dt.Columns)
                            {
                                string colName = item.ColumnName;
                                string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = item.ColumnName;
                                    field.SortExpression = item.ColumnName;
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);

                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                gvDynamic.DataSource = dt;
                gvDynamic.DataBind();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    btnReset_Click(this, EventArgs.Empty);
                }
            }
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";

                    if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                    {
                        query = string.Format("select count({0}) from {1}.{2} where {3} like '%{4}%'", datakeyName, schema, tableName, Convert.ToString(ddlColumn.SelectedValue), Common.Common.ReplaceSQLChar(txtSearch.Text.Trim()));
                    }
                    else
                    {
                        query = string.Format("select count({0}) from {1}.{2}", datakeyName, schema, tableName);
                    }

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_");
            }           
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                {
                    ViewState["PageIndex"] = gvDynamic.PageIndex;
                }
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_");
            }
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = -1;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();

                string dataKeyname = string.Empty;
                if (model != null)
                {
                    dataKeyname = model.DataKeyName;
                }

                Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_ViewRecord");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_RowEditing");
            }
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                    GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;

                    if (model != null)
                    {
                        readonlyColumns = model.ReadonlyColumns;
                        dataKeyname = model.DataKeyName;
                    }
                    string query = " set ";
                    foreach (DataControlFieldCell cell in row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            string colName = ((BoundField)cell.ContainingField).DataField;
                            if (!readonlyColumns.Contains(colName) && colName != "id")
                            {
                                query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                            }
                        }
                    }
                    gvDynamic.EditIndex = -1;
                    conn.Open();
                    string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);

                    string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                    SqlCommand cmd = new SqlCommand(finalQuery, conn);
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_RowUpdating");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_Sorting");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_GetSortDirection");
            }       
            return nextDir;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_btnSearch_Click");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtSearch.Text = string.Empty;
                txtDateFrom.Text = string.Empty;
                txtDateTo.Text = string.Empty;
                ddlColumn.SelectedIndex = 0;
                ViewState["SortExpression"] = null;
                gvDynamic.PageIndex = 0;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_btnReset_Click");
            }
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var row = (DataRowView)e.Row.DataItem;
                    int index = -1;
                    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                    field.Text = "View";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.OnClientClick = "ShowProgress();";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_RowDataBound");
            }            
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string searchText = Common.Common.ReplaceSQLChar(txtSearch.Text.Trim());
                    string columnName = ddlColumn.SelectedValue;
                    if (lblSchema.Text == "Reference")
                    {
                        string id = e.CommandArgument.ToString();
                        Response.Redirect("DefaultReferenceView.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + "&searchText=" + searchText + "&colName=" + columnName + "&fromDate=" + txtDateFrom.Text + "&toDate=" + txtDateTo.Text + "");
                    }
                    else
                    {
                        if (lblTable.Text == "ValidationFailures")
                        {
                            lblTable.Text = "ValidationFailureDetails";
                        }
                        int id = Convert.ToInt32(e.CommandArgument.ToString());
                        Response.Redirect("DefaultDataViewMatching.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + id + "&name=" + hdfName.Value + "&searchText=" + searchText + "&colName=" + columnName + "&fromDate=" + txtDateFrom.Text + "&toDate=" + txtDateTo.Text + "");
                    }
                }
            }
            catch (ThreadAbortException)
            {
                // Ignore the ThreadAbortException
                // This exception is expected when using Response.Redirect and can be safely ignored.
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "DefaultDataView_gvDynamic_RowCommand");
            }
        }
    }
}