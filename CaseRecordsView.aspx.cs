﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DynamicGridView
{
    public partial class CaseRecordsView : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string browserName = Request.Browser.Browser;
            string browserCount = Convert.ToString(Session["BrowserCount"]);
            string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
            string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
            string sessionGuid = Convert.ToString(Session["GuId"]);
            string existingbrowserName = Convert.ToString(Session["BrowserName"]);

            if (!Common.Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
            {
                string env = ConfigurationManager.AppSettings["environment"];
                string miMiLogin = String.Empty;
                if (!string.IsNullOrEmpty(env))
                {
                    if (env == "dev" || env == "qa")
                    {
                        miMiLogin = "login.aspx";
                    }
                    else
                    {
                        miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                    }
                }
                else
                {
                    miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
                }

                Response.Redirect(miMiLogin);
            }

            try
            {
                txtDateFrom.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateFrom.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                txtDateTo.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtDateTo.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                txtChildDOB.Attributes["max"] = DateTime.Today.ToString("yyyy-MM-dd");
                txtChildDOB.Attributes["min"] = new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");

                if (!IsPostBack)
                {
                    string timeout = !string.IsNullOrEmpty(Request.QueryString["timeout"]) ? Convert.ToString(Request.QueryString["timeout"]) : "";

                    if (timeout == "Yes")
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "timeout", "noRecFound( 'The timeout period elapsed prior to completion of the operation or the server is not responding.');", true);
                    }

                    string schema = !string.IsNullOrEmpty(Request.QueryString["Schema"]) ? Convert.ToString(Request.QueryString["Schema"]) : "";
                    string tableName = !string.IsNullOrEmpty(Request.QueryString["Table"]) ? Convert.ToString(Request.QueryString["Table"]) : "";
                    string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                    string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                    string searchFrom = !string.IsNullOrEmpty(Request.QueryString["searchFrom"]) ? Convert.ToString(Request.QueryString["searchFrom"]) : "";
                    string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
                    string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
                    string birthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
                    string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
                    string city = !string.IsNullOrEmpty(Request.QueryString["city"]) ? Convert.ToString(Request.QueryString["city"]) : "";
                    string zipcode = !string.IsNullOrEmpty(Request.QueryString["zipcode"]) ? Convert.ToString(Request.QueryString["zipcode"]) : "";
                    string childSSN = !string.IsNullOrEmpty(Request.QueryString["childSSN"]) ? Convert.ToString(Request.QueryString["childSSN"]) : "";
                    string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
                    string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";
                    string momSSN = !string.IsNullOrEmpty(Request.QueryString["momSSN"]) ? Convert.ToString(Request.QueryString["momSSN"]) : "";
                    string caseRecId = !string.IsNullOrEmpty(Request.QueryString["caseRecId"]) ? Convert.ToString(Request.QueryString["caseRecId"]) : "";
                    string birthRecId = !string.IsNullOrEmpty(Request.QueryString["masterRecordNumber"]) ? Convert.ToString(Request.QueryString["masterRecordNumber"]) : "";
                    string deathRecId = !string.IsNullOrEmpty(Request.QueryString["deathNumber"]) ? Convert.ToString(Request.QueryString["deathNumber"]) : "";
                    string caseId = !string.IsNullOrEmpty(Request.QueryString["caseId"]) ? Convert.ToString(Request.QueryString["caseId"]) : "";
                    string fromDate = !string.IsNullOrEmpty(Request.QueryString["fromDate"]) ? Convert.ToString(Request.QueryString["fromDate"]) : "";
                    string toDate = !string.IsNullOrEmpty(Request.QueryString["toDate"]) ? Convert.ToString(Request.QueryString["toDate"]) : "";

                    if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                    {
                        string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                        List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                        GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                        string[] header = model.Table.Split(':');
                        hdfSearchType.Value = "basicSearch";
                        hdfName.Value = headerLabel;
                        lblSchema.Text = schema;
                        lblTable.Text = tableName;                       
                        lblHeader.Visible = true;
                        lblHeader.Text = " Case Records ";
                        
                        txtchdLN.Text = childLastName;
                        txtchdFN.Text = childFirstName;
                        txtadd1.Text = address1;
                        txtChildDOB.Text = birthDate;
                        txtChildSSN.Text = childSSN;
                        txtcity.Text = city;
                        txtzip.Text = zipcode;
                        txtMomLN.Text = momLastName;
                        txtMomFN.Text = momFirstName;
                        txtMomSSN.Text = momSSN;
                        txtCRecId.Text = caseRecId;
                        txtBirthRecId.Text = birthRecId;
                        txtDeathRecId.Text = deathRecId;
                        txtDateFrom.Text = fromDate;
                        txtDateTo.Text = toDate;

                        if (!string.IsNullOrEmpty(childLastName) || !string.IsNullOrEmpty(childFirstName) || !string.IsNullOrEmpty(address1) || !string.IsNullOrEmpty(birthRecId) || !string.IsNullOrEmpty(birthDate) || !string.IsNullOrEmpty(childSSN) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(zipcode) || !string.IsNullOrEmpty(momLastName) || !string.IsNullOrEmpty(momFirstName) || !string.IsNullOrEmpty(momSSN) || !string.IsNullOrEmpty(caseRecId) || !string.IsNullOrEmpty(deathRecId) || !string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate))
                        {
                            BindGrid(lblSchema.Text, lblTable.Text);
                        }
                        else
                        {
                            BindGridEmpty(lblSchema.Text, lblTable.Text);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                    {
                        BindGridEmpty(lblSchema.Text, lblTable.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_Page_Load");
            }
        }

        public void BindGridEmpty(string schema, string tableName)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                SqlCommand command = new SqlCommand(sqlQuery, conn);
                command.Parameters.AddWithValue("@schema", schema);
                command.Parameters.AddWithValue("@tableName", tableName);
                command.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter daColumns = new SqlDataAdapter(command);
                conn.Close();
                daColumns.Fill(dt);
                
            }

            gvDynamic.Columns.Clear();
            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            gvDynamic.Columns.Add(commandField);

            List<GridViewModel> lstFilter = Common.Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns; 
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName; 
            }
            List<string> lstColumns = includeColumns.Split(',').ToList();
            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 15)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        for(int i=0; i < dt.Rows.Count; i++)
                        {
                            string colName = Convert.ToString(dt.Rows[i][0]);
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (colName != "childSuffix" && colName != "aliasLastName" && colName != "aliasFirstName" && colName != "aliasMiddleName"
                                && colName != "plurality" && colName != "childMRN" && colName != "address2" && colName != "birthOrder" && colName != "vitalStatus" && colName != "birthWeight" && colName != "childMedicaidNumber" && colName != "birthHospital" && colName != "county" && colName != "country" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "gender" && colName != "momMiddleName" && colName != "momSuffix" && colName != "admittingEntityCode" && colName != "patientType" && colName != "admissionSource" && colName != "admissionDate" && colName != "dischargeStatus" && colName != "dischargeDate" && colName != "icd9_diag_code" && colName != "icd10_diag_status" && colName != "icd9_proc_code" && colName != "cpt_proc_code" && colName != "icd10_proc_code" && colName != "ICD9SyndromeCode" && colName != "ICD10SyndromeCode" && colName != "cytogeneticsTesting" && colName != "ICD9CytogeneticsDiagnosisCodes" && colName != "ICD10CytogeneticsDiagnosisCodes" && colName != "labCode" && colName != "cytogeneticsLabReportNumber" && colName != "headCircumference" && colName != "deliveryLength" && colName != "infantPostnatalEcho" && colName != "ageFirstPostnatalEcho" && colName != "dateFirstPostnatalEcho" && colName != "infantAdmitted" && colName != "ageInfantAdmitted" && colName != "ageInfantDischarged" && colName != "ageInfantAdmittedICU" && colName != "dateInfantAdmittedICU" && colName != "approved" && colName != "lastUpdated")
                            {
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = Convert.ToString(dt.Rows[i][0]);
                                    field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            DataTable dtEmpty = new DataTable();
            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.DataSource = dtEmpty;
            gvDynamic.DataBind();
        }

        public void BindGrid(string schema, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                string sortExp = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExp = Convert.ToString(ViewState["SortExpression"]);
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                        SqlCommand command = new SqlCommand(sqlQuery, conn);
                        command.Parameters.AddWithValue("@schema", schema);
                        command.Parameters.AddWithValue("@tableName", tableName);
                        command.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter daColumns = new SqlDataAdapter(command);
                        conn.Close();
                        DataTable dtColumns = new DataTable();
                        daColumns.Fill(dtColumns);
                        foreach (DataRow item in dtColumns.Rows)
                        {
                            if (Convert.ToString(item[0]) == "caseRecId")
                            {
                                sortExp = "caseRecId desc";
                                break;
                            }
                        }
                    }
                }
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                string includeColumns = string.Empty;
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    includeColumns = model.IncludedColumns;
                    readonlyColumns = model.ReadonlyColumns; 
                    dataKeyname = model.DataKeyName;
                }
                hdfDataKeyName.Value = dataKeyname;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "";
                    string OrderByCRec = sortExp;
                    if (string.IsNullOrEmpty(OrderByCRec))
                    {
                        OrderByCRec = "caseRecId desc";
                    }
                    int pageIndex = 0;
                    hdfSearchType.Value = "advancedSearchCRec";
                    OrderByCRec = sortExp;

                    where = GetWhereClause();

                    if (!where.Contains("and"))
                    {
                        BindGridEmpty(schema, tableName);
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                    }
                    else
                    {
                        query = string.Format("SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {6}) AS Row,* FROM {0}.{1} {5}) AS RESULT WHERE ROW between({3}) and ({4})", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderByCRec);

                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.CommandTimeout = 0;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        conn.Close();
                        da.Fill(dt);
                        gvDynamic.Columns.Clear();
                        CommandField commandField = new CommandField();
                        commandField.ButtonType = ButtonType.Link;
                        commandField.HeaderText = "Action";
                        commandField.ShowEditButton = true;
                        gvDynamic.Columns.Add(commandField);

                        List<string> lstColumns = includeColumns.Split(',').ToList();
                        foreach (var col in lstColumns)
                        {
                            if (gvDynamic.Columns.Count < 15)
                            {
                                if (!string.IsNullOrEmpty(col))
                                {
                                    foreach (DataColumn item in dt.Columns)
                                    {
                                        string colName = item.ColumnName;
                                        string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                                        if (colName != "childSuffix" && colName != "aliasLastName" && colName != "aliasFirstName" && colName != "aliasMiddleName"
                                            && colName != "plurality" && colName != "childMRN" && colName != "address2" && colName != "birthOrder" && colName != "vitalStatus" && colName != "birthWeight" && colName != "childMedicaidNumber" && colName != "birthHospital" && colName != "county" && colName != "country" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "childMiddleName" && colName != "gender" && colName != "momMiddleName" && colName != "momSuffix" && colName != "admittingEntityCode" && colName != "patientType" && colName != "admissionSource" && colName != "admissionDate" && colName != "dischargeStatus" && colName != "dischargeDate" && colName != "icd9_diag_code" && colName != "icd10_diag_status" && colName != "icd9_proc_code" && colName != "cpt_proc_code" && colName != "icd10_proc_code" && colName != "ICD9SyndromeCode" && colName != "ICD10SyndromeCode" && colName != "cytogeneticsTesting" && colName != "ICD9CytogeneticsDiagnosisCodes" && colName != "ICD10CytogeneticsDiagnosisCodes" && colName != "labCode" && colName != "cytogeneticsLabReportNumber" && colName != "headCircumference" && colName != "deliveryLength" && colName != "infantPostnatalEcho" && colName != "ageFirstPostnatalEcho" && colName != "dateFirstPostnatalEcho" && colName != "infantAdmitted" && colName != "ageInfantAdmitted" && colName != "ageInfantDischarged" && colName != "ageInfantAdmittedICU" && colName != "dateInfantAdmittedICU" && colName != "approved" && colName != "admittingEntity" && colName != "lastUpdated")
                                        {
                                            if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                string columnInfo = Common.Common.GetBetween(includeColumns, colName + ":", ",");
                                                BoundField field = new BoundField();
                                                field.HeaderText = columnInfo;
                                                field.DataField = item.ColumnName;
                                                field.SortExpression = item.ColumnName;
                                                if (readonlyColumns.Contains(colName))
                                                {
                                                    field.ReadOnly = true;
                                                }
                                                gvDynamic.Columns.Add(field);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                        gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                        if (gvDynamic.VirtualItemCount < 1)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                            ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                        }
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);

                        gvDynamic.DataSource = dt;
                        gvDynamic.DataBind();                        
                    }
                }                
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_BindGrid");

                if (ex.Message.Contains("Execution Timeout Expired"))
                {
                    Response.Redirect("/CaseRecordsView.aspx?schema=MBDR_System&table=CaseRecordsView&timeout=Yes", false);
                }
            }            
        }

        private string GetWhereClause()
        {
            string where= "where 1 = 1";

            if (!string.IsNullOrEmpty(txtCRecId.Text))
            {
                where += " and caseRecId = '" + txtCRecId.Text.Replace("'", "''").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtBirthRecId.Text))
            {
                where += " and masterRecordNumber = '" + txtBirthRecId.Text.Replace("'", "''").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtDeathRecId.Text))
            {
                where += " and deathNumber = '" + txtDeathRecId.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtchdLN.Text) && !txtchdLN.Text.Contains("*"))
            {
                where += " and childLastName = '" + txtchdLN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdLN.Text.Contains("*"))
            {
                where += " and childLastName like '" + txtchdLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtchdFN.Text) && !txtchdFN.Text.Contains("*"))
            {
                where += " and childFirstName = '" + txtchdFN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtchdFN.Text.Contains("*"))
            {
                where += " and childFirstName like '" + txtchdFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtChildDOB.Text))
            {
                where += " and birthDate > ='" + txtChildDOB.Text + " 00:00:00'  and birthDate < ='" + txtChildDOB.Text + " 23:59:59'";
            }

            if (!string.IsNullOrEmpty(txtadd1.Text) && !txtadd1.Text.Contains("*"))
            {
                where += " and address1 = '" + txtadd1.Text.Replace("'", "''") + "'";
            }
            else if(txtadd1.Text.Contains("*"))
            {
                where += " and address1 like '" + txtadd1.Text.Replace("'", "''").Replace("*", "%") + "'";
            }

            if (!string.IsNullOrEmpty(txtcity.Text) && !txtcity.Text.Contains("*"))
            {
                where += " and city = '" + txtcity.Text.Replace("'", "''").Trim() + "'";
            }
            else if(txtcity.Text.Contains("*"))
            {
                where += " and city like '" + txtcity.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtzip.Text))
            {
                where += " and zipcode ='" + txtzip.Text.Replace("'", "''").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtChildSSN.Text))
            {
                where += " and childSSN ='" + txtChildSSN.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtMomLN.Text) && !txtMomLN.Text.Contains("*"))
            {
                where += " and momLastName = '" + txtMomLN.Text.Replace("'", "''").Trim() + "'";
            }
            else if (txtMomLN.Text.Contains("*"))
            {
                where += " and momLastName like '" + txtMomLN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtMomFN.Text) && !txtMomFN.Text.Contains("*"))
            {
                where += " and momFirstName = '" + txtMomFN.Text.Trim().Replace("'", "''") + "'";
            }
            else if (txtMomFN.Text.Contains("*"))
            {
                where += " and momFirstName like '" + txtMomFN.Text.Replace("'", "''").Replace("*", "%").Trim() + "'";
            }
            if (!string.IsNullOrEmpty(txtMomSSN.Text))
            {
                where += " and momSSN ='" + txtMomSSN.Text.Replace("'", "''").Trim() + "'";
            }

            if (!string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // both from/to date
            {
                where += " and createdTimestamp > ='" + txtDateFrom.Text + " 00:00:00'  and createdTimestamp < ='" + txtDateTo.Text + " 23:59:59'";
            }
            else if (!string.IsNullOrEmpty(txtDateFrom.Text) && string.IsNullOrEmpty(txtDateTo.Text)) // from date only 
            {
                where += " and createdTimestamp > ='" + txtDateFrom.Text + " 00:00:00' ";
            }
            else if (string.IsNullOrEmpty(txtDateFrom.Text) && !string.IsNullOrEmpty(txtDateTo.Text)) // to date only 
            {
                where += " and createdTimestamp < ='" + txtDateTo.Text + " 23:59:59' ";
            }
            return where;
        }

        public int GetTotalRecords(string tableName, string schema, string datakeyName)
        {
            int totalRecords = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string query = "";
                    string where = "where 1=1 ";
                    int pageIndex = 0;

                    where = GetWhereClause();                    

                    query = string.Format("select count({0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);
                    
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    DataTable dt = new DataTable();
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    
                    conn.Close();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_GetTotalRecords");
            }
            return totalRecords;
        }

        protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = e.NewPageIndex;               
                ViewState["PageIndex"] = gvDynamic.PageIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_PageIndexChanging");
            }            
        }

        protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = -1;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_RowCancelingEdit");
            }
        }

        protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                string dataKeyname = string.Empty;
                if (model != null)
                {
                    dataKeyname = model.DataKeyName;
                }
                Response.Redirect("RecordDetail.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value, true);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_ViewRecord");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                gvDynamic.EditIndex = e.NewEditIndex;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_RowEditing");
            }
        }

        protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                    GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                    List<GridViewModel> lstFilter = Common.Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Schema == lblSchema.Text && s.Table.StartsWith(lblTable.Text)).FirstOrDefault();
                    string readonlyColumns = string.Empty;
                    string dataKeyname = string.Empty;

                    if (model != null)
                    {
                        readonlyColumns = model.ReadonlyColumns;
                        dataKeyname = model.DataKeyName;
                    }
                    string query = " set ";
                    foreach (DataControlFieldCell cell in row.Cells)
                    {
                        if (cell.ContainingField is BoundField)
                        {
                            string colName = ((BoundField)cell.ContainingField).DataField;
                            if (!readonlyColumns.Contains(colName) && colName != "id")
                            {
                                query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                            }
                        }
                    }
                    gvDynamic.EditIndex = -1;
                    conn.Open();
                    string tableName = string.Format("{0}.{1}", lblSchema.Text, lblTable.Text);
                    string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                    SqlCommand cmd = new SqlCommand(finalQuery, conn);
                    cmd.CommandTimeout = 0;
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_RowUpdating");
            }
        }

        protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                ViewState["SortExpression"] = exp;
                BindGrid(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_Sorting");
            }
        }

        protected string GetSortDirection(string column)
        {
            string nextDir = "ASC";
            try
            {
                if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
                {
                    nextDir = "DESC";
                    ViewState["sort"] = null;
                }
                else
                {
                    ViewState["sort"] = column;
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_GetSortDirection");
            }
            return nextDir;
        }

        protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var birthDateValue = DataBinder.Eval(e.Row.DataItem, "birthDate");

                    if (birthDateValue != null && !string.IsNullOrEmpty(birthDateValue.ToString()))
                    {
                        DateTime birthDate = Convert.ToDateTime(birthDateValue);
                        e.Row.Cells[6].Text = birthDate.ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        e.Row.Cells[6].Text = string.Empty;
                    }

                    var purgeId = DataBinder.Eval(e.Row.DataItem, "purgeId");
                    var caseRecId = DataBinder.Eval(e.Row.DataItem, "caseRecId").ToString();
                    
                    if (purgeId != DBNull.Value && !string.IsNullOrEmpty(purgeId.ToString()))
                    {
                        /* Checking if Case Record Id is Archieved or not */
                        e.Row.Cells[1].Text = caseRecId + " *";
                    }

                    var row = (DataRowView)e.Row.DataItem;
                    int index = -1;

                    LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                    field.Text = "View";
                    field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                    field.OnClientClick = "ShowProgress();";
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_RowDataBound");
            }
        }

        protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Edit")
                {
                    string columnName = string.Empty;
                    string where = "1";
                    string searchType = string.Empty;

                    searchType = Convert.ToString(hdfSearchType.Value);
                    if (!string.IsNullOrEmpty(txtCRecId.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&caseRecId=" + txtCRecId.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtBirthRecId.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&masterRecordNumber=" + txtBirthRecId.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtDeathRecId.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&deathNumber=" + txtDeathRecId.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtchdLN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&childLastName=" + txtchdLN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtchdFN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&childFirstName=" + txtchdFN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtChildDOB.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&birthDate=" + txtChildDOB.Text + "";
                    }
                    if (!string.IsNullOrEmpty(txtadd1.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&address1=" + txtadd1.Text + "";
                    }
                    if (!string.IsNullOrEmpty(txtcity.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&city=" + txtcity.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtzip.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += " &zipcode=" + txtzip.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtChildSSN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&childSSN=" + txtChildSSN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtMomLN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&momLastName=" + txtMomLN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtMomFN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&momFirstName=" + txtMomFN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtMomSSN.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&momSSN=" + txtMomSSN.Text.Trim() + "";
                    }
                    if (!string.IsNullOrEmpty(txtDateFrom.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&fromDate=" + txtDateFrom.Text + "";
                    }
                    if (!string.IsNullOrEmpty(txtDateTo.Text))
                    {
                        if (where == "1")
                        {
                            where = "0";
                        }
                        where += "&toDate=" + txtDateTo.Text + "";
                    }

                    string sortExpre = string.Empty;
                    if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                    {
                        sortExpre = "caseRecId desc";
                    }
                    else
                    {
                        sortExpre = Convert.ToString(ViewState["SortExpression"]);
                    }

                    int id = Convert.ToInt32(e.CommandArgument.ToString());

                    Response.Redirect("CaseRecordsViewDetails.aspx?Schema=" + lblSchema.Text + "&Table=" + lblTable.Text + "&keyName=" + hdfDataKeyName.Value + "&caseId=" + id + "&name=" + hdfName.Value + "&where=" + where + "&sortExp=" + sortExpre + "", false);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_gvDynamic_RowCommand");
            }
        }
       
        protected void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    lblHeader.Text = " Case Records ";
                    hdfSearchType.Value = "advancedSearchRec";
                    gvDynamic.PageIndex = 0;
                    BindGrid(lblSchema.Text, lblTable.Text);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
                }
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_btnAdvanceSearch_Click");
            }
            ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
        }

        protected void btnResetAdv_Click(object sender, EventArgs e)
        {
            try
            {
                gvDynamic.PageIndex = 0;
                BindGridEmpty(lblSchema.Text, lblTable.Text);
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.Common.ErrorLogCapture(ex, path, "CaseRecordsView_btnResetAdv_Click");
            }
        }

        protected void txtChildDOB_TextChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "validate", "ValidateDate();", true);
            }
        }

    }            
}