﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Web;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using DynamicGridView.Common;
using System.IdentityModel;
using System.Runtime.CompilerServices;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {            
            lblErrorMessage.Visible = false;
            string Error = Request.QueryString["error"];
            //authorization code after successful authorization
            string Code = Request.QueryString["code"];

            string getMessage = string.Empty;
            string path = string.Empty;

            if (Error != null)
            {

            }
            else if (Code != null)
            {
                //User valid from APIs(MiLogin)
                System.Diagnostics.Debug.WriteLine("Code");
                string UserId = Request.QueryString["state"];
                //Get AccessToken
                string AccessToken = string.Empty;
                try
                {
                    string RefreshToken = ExchangeMIAuthorizationCode(Code, out AccessToken);  // get access token + code 
                }
                catch (Exception ex)
                {
                    path = ConfigurationManager.AppSettings["uiLogPath"];
                    Common.ErrorLogCapture(ex, path, "Login_PageLoad");
                }
                //Session["RefreshToken"] = RefreshToken;
                //Session["AccessToken"] = AccessToken;
                try
                {
                    string getMsgs = string.Empty;

                    string checkAccessTok = string.Empty;
                    string checkRefreshTok = string.Empty;

                    if (!string.IsNullOrEmpty(AccessToken))
                    {
                        checkAccessTok = "True";
                    }
                    else
                    {
                        checkAccessTok = "False";
                    }

                    if (!string.IsNullOrEmpty(AccessToken))
                    {
                        checkRefreshTok = "True";
                    }
                    else
                    {
                        checkRefreshTok = "False";
                    }

                    MILoginInfo miInfo = FetchUserDetailId(AccessToken);

                    if (miInfo !=null && !string.IsNullOrEmpty(miInfo.mail))
                    {
                        string useremail = miInfo.mail;
                        string userRole = string.Empty;

                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            string query = "SELECT * FROM MBDR_System.Users WHERE isHidden IS NULL AND emailAddress='" + useremail + "'"; 
                            SqlCommand sqlCmd = new SqlCommand(query, con);
                            sqlCmd.CommandTimeout = 0;

                            con.Open();
                            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                            con.Close();
                            System.Data.DataTable dt = new System.Data.DataTable();
                            da.Fill(dt);

                            string emessage = string.Empty;
                           
                            if (dt != null && dt.Rows.Count > 0 && !string.IsNullOrEmpty(dt.Rows[0]["roleId"].ToString()))
                            {
                                userRole = Convert.ToString(dt.Rows[0]["roleId"]);

                                string newGuid = Guid.NewGuid().ToString();
                                Session["GuId"] = newGuid;

                                Session["username"] = useremail;
                                Session["userid"] = Convert.ToString(dt.Rows[0]["userId"]);
                                Session["DisplayName"] = Convert.ToString(dt.Rows[0]["displayName"]);

                                /****** update current login time **********/

                                string updateQuery = "UPDATE MBDR_System.Users SET lastLogin = @lastLogin WHERE userId = @userId";

                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                                {
                                    SqlCommand cmd = new SqlCommand(updateQuery, conn);
                                    cmd.Parameters.AddWithValue("@lastLogin", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@userId", Convert.ToString(dt.Rows[0]["userId"]));
                                    cmd.CommandTimeout = 0;

                                    conn.Open();
                                    int rowsAffected = cmd.ExecuteNonQuery();
                                    conn.Close();
                                }

                                HttpContext.Current.Application.Lock();

                                string appGUID = "appGUID_" + Convert.ToString(dt.Rows[0]["userId"]) + "";
                                HttpContext.Current.Application[appGUID] = newGuid;

                                HttpContext.Current.Application.UnLock();

                                string browserName = Request.Browser.Browser;

                                if (string.IsNullOrEmpty(Convert.ToString(Session["BrowserName"])))
                                {
                                    Session["BrowserName"] = browserName;
                                    Session["BrowserCount"] = "1";
                                }
                                else
                                {
                                    Session["BrowserCount"] = "2";
                                }

                                getMsgs = $"Login|Page_Load|User ID:{Session["userid"]}|User:{miInfo.mail}|Roll ID:{userRole}";
                                ErrorLog(getMsgs);

                                Response.Redirect("Default.aspx", false);
                            }
                            
                            else
                            {
                                if (string.IsNullOrEmpty(dt.Rows[0]["roleId"].ToString()))
                                {
                                    emessage = "Error.aspx?credentialsMessage=roleMissing&atpEmail=" + useremail;
                                    Response.Redirect(emessage, false);
                                }
                                else
                                {
                                    emessage = "Error.aspx?credentialsMessage=userNotFound&atpEmail=" + useremail;
                                    Response.Redirect(emessage, false);
                                }
                            }
                        }
                    }
                    else
                    {
                        getMsgs = $"Login|Page_Load|Authentication Failed|Email:{miInfo.mail}|Refresh Token:{checkAccessTok}|Token Recevied:{checkAccessTok}";
                        ErrorLog(getMsgs);

                        Response.Redirect("Error.aspx?credentialsMessage=Authentication failed (MILoginInfo --> Access Token or Email).", false);
                    }

                    getMsgs = $"Login|Page_Load|Check HandShake|Refresh Token:{checkAccessTok}|Access Token:{checkAccessTok}|User:{miInfo.mail}";
                    ErrorLog(getMsgs);
                }
                catch (Exception ex)
                {
                    path = ConfigurationManager.AppSettings["uiLogPath"];
                    Common.ErrorLogCapture(ex, path, "Login_PageLoad");
                }
            }
            else
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                string RedirectUrl = ConfigurationManager.AppSettings["miRedirectUrl"];
                string MatchUrl = ConfigurationManager.AppSettings["miMatchUrl"];
                if (url.ToLower() == MatchUrl)
                {
                    string ClientId = ConfigurationManager.AppSettings["miClientID"];
                    string Url = ConfigurationManager.AppSettings["miAuthorizeUrl"];
                    StringBuilder UrlBuilder = new StringBuilder(Url);
                    UrlBuilder.Append("response_type=" + "code");
                    UrlBuilder.Append("&client_id=" + ClientId);
                    UrlBuilder.Append("&redirect_uri=" + RedirectUrl);
                    HttpContext.Current.Response.Redirect(UrlBuilder.ToString(), false);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "login_Page_Load");
        }
    }
    
    protected void btnMiLogin_Click(object sender, EventArgs e)
    {
        try
        {
            string ClientId = ConfigurationManager.AppSettings["miClientID"];
            //get this value by opening your web app in browser.
            string RedirectUrl = ConfigurationManager.AppSettings["miRedirectUrl"];
            string Url = ConfigurationManager.AppSettings["miAuthorizeUrl"];
            StringBuilder UrlBuilder = new StringBuilder(Url);

            UrlBuilder.Append("response_type=" + "code");
            UrlBuilder.Append("&client_id=" + ClientId);
            UrlBuilder.Append("&redirect_uri=" + RedirectUrl);
            HttpContext.Current.Response.Redirect(UrlBuilder.ToString(), false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "login_btnMiLogin_Click");
        }
    }

    private string ExchangeMIAuthorizationCode(string code, out string accessToken)
    {
        accessToken = string.Empty;
        string ClientSecret = ConfigurationManager.AppSettings["miClientSecret"];
        string ClientId = ConfigurationManager.AppSettings["miClientID"];

        //get this value by opening your web app in browser.
        string RedirectUrl = ConfigurationManager.AppSettings["miRedirectUrl"];

        var Content = "grant_type=authorization_code" +
            "&client_id=" + ClientId +
            "&client_secret=" + ClientSecret +
            "&code=" + code +
            "&redirect_uri=" + RedirectUrl;

        var request = WebRequest.Create(Convert.ToString(ConfigurationManager.AppSettings["miTokenUrl"]));
        request.Method = "POST";
        byte[] byteArray = Encoding.UTF8.GetBytes(Content);
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = byteArray.Length;
        
        using (Stream dataStream = request.GetRequestStream())
        {
            try
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            catch (Exception ex)
            {
                string path = ConfigurationManager.AppSettings["uiLogPath"];
                Common.ErrorLogCapture(ex, path, "Login_ExchangeMIAuthorizationCode");
            }
        }
        try
        {
            var Response = (HttpWebResponse)request.GetResponse();
            Stream responseDataStream = Response.GetResponseStream();
            StreamReader reader = new StreamReader(responseDataStream);
            string ResponseData = reader.ReadToEnd();
            reader.Close();
            responseDataStream.Close();
            Response.Close();
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                var ReturnedToken = JsonConvert.DeserializeObject<Token>(ResponseData);

                if (ReturnedToken.refresh_token != null)
                {
                    accessToken = ReturnedToken.access_token;
                    return ReturnedToken.refresh_token;
                }
                else
                {
                    accessToken = ReturnedToken.access_token;
                    return null;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "Login_ExchangeMIAuthorizationCode");
        }
        return string.Empty;
    }

    private MILoginInfo FetchUserDetailId(string accessToken)
    {
        MILoginInfo result = new MILoginInfo();
        try
        {
            var EmailRequest = string.Format("{0}", ConfigurationManager.AppSettings["miUserInfoUrl"]);
            var request = WebRequest.Create(EmailRequest);
            request.Method = "POST";
            request.Headers.Add("Authorization", "Bearer " + accessToken);
            request.ContentType = "application/x-www-form-urlencoded";

            var Response = (HttpWebResponse)request.GetResponse();
            var DataStream = Response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            var Reader = new StreamReader(DataStream);
            // Read the content.
            var JsonString = Reader.ReadToEnd();
            // Cleanup the streams and the response
            // 
            Reader.Close();
            DataStream.Close();
            Response.Close();
            result = JsonConvert.DeserializeObject<MILoginInfo>(JsonString);
           
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "Login_FetchUserDetailId");
            
        }
        return result;

    }

    protected void MBDRpass(object sender, EventArgs e)
    {
        try
        {
            string getMsgs = string.Empty;
            string emessage = string.Empty;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string userQuery = "SELECT * FROM MBDR_System.Users WHERE isHidden IS NULL AND  emailAddress='" + txtUserName.Text + "'";

                SqlCommand UserCmd = new SqlCommand(userQuery, con);
                UserCmd.CommandTimeout = 0;

                con.Open();
                SqlDataAdapter daUser = new SqlDataAdapter(UserCmd);
                con.Close();

                System.Data.DataTable dtUser = new System.Data.DataTable();
                daUser.Fill(dtUser);

                if (dtUser != null && dtUser.Rows.Count > 0)
                {
                    getMsgs = $"Login|MBDRpass|1|Check dt|Email:{txtUserName.Text}|Count:{dtUser.Rows.Count}";
                    ErrorLog(getMsgs);

                    if (!string.IsNullOrEmpty(Convert.ToString(dtUser.Rows[0]["roleId"])))
                    {
                        string query = "SELECT * FROM MBDR_System.Users WHERE isHidden IS NULL AND  emailAddress='" + txtUserName.Text + "' and HashedPassword=HASHBYTES('SHA2_512', '" + txtPassword.Text + "')";

                        SqlCommand sqlCmd = new SqlCommand(query, con);
                        sqlCmd.CommandTimeout = 0;

                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        con.Close();

                        System.Data.DataTable dt = new System.Data.DataTable();
                        da.Fill(dt);

                        getMsgs = $"Login|MBDRpass|2|Data Table True|Email:{txtUserName.Text}|Count:{dtUser.Rows.Count}";
                        ErrorLog(getMsgs);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            getMsgs = $"Login|MBDRpass|3|Data Table True|Email:{txtUserName.Text}|Role:{dtUser.Rows[0]["roleId"]}";
                            ErrorLog(getMsgs);

                            string newGuid = Guid.NewGuid().ToString();
                            Session["GuId"] = newGuid;

                            Session["username"] = txtUserName.Text.Trim();
                            Session["userid"] = Convert.ToString(dt.Rows[0]["userId"]);
                            Session["DisplayName"] = Convert.ToString(dt.Rows[0]["displayName"]);

                            /****** update current login time **********/

                            string updateQuery = "UPDATE MBDR_System.Users SET lastLogin = @lastLogin WHERE userId = @userId";

                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                            {
                                SqlCommand cmd = new SqlCommand(updateQuery, conn);
                                cmd.Parameters.AddWithValue("@lastLogin", DateTime.Now);
                                cmd.Parameters.AddWithValue("@userId", Convert.ToString(dt.Rows[0]["userId"]));
                                cmd.CommandTimeout = 0;

                                conn.Open();
                                int rowsAffected = cmd.ExecuteNonQuery();
                                conn.Close();
                            }

                            HttpContext.Current.Application.Lock();

                            string appGUID = "appGUID_" + Convert.ToString(dt.Rows[0]["userId"]) + "";
                            HttpContext.Current.Application[appGUID] = newGuid;

                            HttpContext.Current.Application.UnLock();

                            string browserName = Request.Browser.Browser;

                            if (string.IsNullOrEmpty(Convert.ToString(Session["BrowserName"])))
                            {
                                Session["BrowserName"] = browserName;
                                Session["BrowserCount"] = "1";
                            }
                            else
                            {
                                Session["BrowserCount"] = "2";
                            }

                            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["roleId"])))  // does not require as previously steps it does check the role. 
                            {
                                Response.Redirect("Default.aspx", false);
                            }
                            else 
                            {
                                emessage = "Error.aspx?credentialsMessage=roleMissing&atpEmail=" + txtUserName.Text;
                                Response.Redirect(emessage, false);
                            }
                        }
                        else
                        {
                            emessage = "Error.aspx?credentialsMessage=wrongCredit&atpEmail=" + txtUserName.Text;
                            Response.Redirect(emessage, false);
                        }
                    }
                    else
                    {
                        emessage = "Error.aspx?credentialsMessage=roleMissing&atpEmail=" + txtUserName.Text;
                        Response.Redirect(emessage, false);
                    }
                }
                else
                {
                    getMsgs = $"Login|MBDRpass|4|Error Email|Email:{txtUserName.Text}|Role:{dtUser.Rows.Count}";
                    ErrorLog(getMsgs);

                    emessage = "Error.aspx?credentialsMessage=userNotFound&atpEmail=" + txtUserName.Text;
                    Response.Redirect(emessage, false);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "login_MBDRpass");
        }
    }

    private void LogOutUser(string userEmail)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string queryUpdate = "Update MBDR_System.Users set User_SessionGUID = null WHERE emailAddress='" + userEmail + "'";
                SqlCommand sqlCmdUpdate = new SqlCommand(queryUpdate, conn);
                sqlCmdUpdate.CommandTimeout = 0;
                conn.Open();
                sqlCmdUpdate.ExecuteNonQuery();
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "login_LogOutUser");
        }
    }

    public static void ErrorLog(string values)
    {
        bool shot = true;
        if (shot)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.Snap(values, path, "Login.aspx");
        }
    }
}