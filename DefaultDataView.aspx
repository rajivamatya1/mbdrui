﻿<%@ Page Title="MBDR Default Data View" Language="C#" MasterPageFile="~/index.master" AutoEventWireup="true" CodeFile="DefaultDataView.aspx.cs" Inherits="DynamicGridView.DefaultDataView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">


    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <asp:Label Text="" runat="server" Visible="false" ID="lblSchema" />
            <asp:Label Text="" runat="server" Visible="false" ID="lblTable" />
            <asp:HiddenField runat="server" ID="hdfName" Value="" />
            <asp:HiddenField runat="server" ID="hdfDataKeyName" Value="" />
            <h1>
                <img class="header" alt="Grid Icon" src="Content/css/images/grid-icon-333.svg">
                <asp:Label Text="" runat="server" Visible="false" ID="lblHeader" /></h1>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="form-item">
                <label for="Content_txtSearch">Search by Keyword</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch" ValidationGroup="search" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSearch" ValidationGroup="search" ValidationExpression="[a-zA-Z0-9]*[^!@%~?:#$%^&*=()']*" runat="server" ErrorMessage="Invalid Input" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true"></asp:RegularExpressionValidator>
                <asp:CustomValidator runat="server" ClientValidationFunction="ValidateDate" ControlToValidate="txtSearch" ErrorMessage="Invalid Date. Must be in format MM/DD/YYYY: 01/23/2021" ForeColor="Red" Font-Size="XX-Small" Display="dynamic" Font-Italic="true" />
            </div>
            <div class="form-item">
                <label for="Content_ddlColumn">In Column</label>
                <asp:DropDownList runat="server" ID="ddlColumn" CssClass="form-control" ValidationGroup="search">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="ddlColumn"></asp:RequiredFieldValidator>
            </div>
            <div class="form-item" runat="server" id="dvFromDate" visible="false">
                <label for="Content_txtDateFrom">Date From</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDateFrom" ValidationGroup="search" TextMode="Date" />
            </div>
            <div class="form-item" runat="server" id="dvToDate" visible="false">
                <label for="Content_txtDateTo">Date To</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDateTo" ValidationGroup="search" TextMode="Date" />
            </div>
            <div class="form-item">
                <asp:Button Text="Search" runat="server" ID="btnSearch" OnClientClick="return ShowProgress();" OnClick="btnSearch_Click" CssClass="btn btn-success" ValidationGroup="search" />
            </div>
            <div class="form-item">
                <asp:Button Text="Reset" runat="server" ID="btnReset" OnClick="btnReset_Click" CssClass="btn btn-danger" />
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col col-lg-12 col-sm-12">
            <div class="tableFixHead">
                <asp:GridView CssClass="table table-bordered table-striped table-fixed" AllowSorting="true" OnSorting="gvDynamic_Sorting" OnRowDataBound="gvDynamic_RowDataBound"
                    OnRowCommand="gvDynamic_RowCommand" AllowCustomPaging="true" AutoGenerateColumns="false" runat="server" Visible="true" ShowHeaderWhenEmpty="true"
                    EmptyDataText="No record found." ShowHeader="true" ID="gvDynamic" OnPageIndexChanging="gvDynamic_PageIndexChanging"
                    OnRowCancelingEdit="gvDynamic_RowCancelingEdit" OnRowDeleting="gvDynamic_ViewRecord" OnRowEditing="gvDynamic_RowEditing"
                    OnRowUpdating="gvDynamic_RowUpdating" AllowPaging="true" PageSize="500" PagerSettings-FirstPageText="First"
                    PagerSettings-LastPageText="Last" PagerSettings-Mode="NumericFirstLast" PagerStyle-CssClass="gridview" ValidateRequestMode="Disabled">
                </asp:GridView>
            </div>
        </div>
    </div>


    <div class="loadingspin" align="center">
        <img src="Content/css/images/loading-waiting.gif" alt="Loading Page" width="120" height="120" /><br />
        <br />
        Loading ... Please wait ...
            <br />
    </div>

    <script>

        function ValidateDate(sender, args) {
            var ddlcloumValue = document.getElementById("Content_ddlColumn").value;
            if ((ddlcloumValue === "lastContact") || (ddlcloumValue === "LastUpdated") || (ddlcloumValue === "lastModified") || (ddlcloumValue === "ModifiedDate")) {
                var dateString = document.getElementById(sender.controltovalidate).value;
                var regex = /((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$/;
                if (regex.test(dateString))
                {
                   args.IsValid = true;
                }
                else {
                   
                    args.IsValid = false;
                }
            } else {
                args.IsValid = true;
            }
        }

        function ShowProgress() {
            var isValid = Page_ClientValidate("search");

            if (!isValid) {
                //jquery to disable search button
                $("#btnAdvanceSearch").prop("disabled", true);
            }
            else {

                $("#btnAdvanceSearch").prop("disabled", false);
                var modal = $('<div />');

                modal.addClass("modalspin");

                $('body').append(modal);

                var loading = $(".loadingspin");
                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ "position": "center", top: top, left: left });

                return true;
            }
        }

       function removeProgress() {
           var modal = $('div.modalspin');
           modal.removeClass("modalspin");
           var loading = $(".loadingspin");
           loading.hide();
       }


    </script>
</asp:Content>
