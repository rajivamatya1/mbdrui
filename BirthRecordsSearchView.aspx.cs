﻿using DynamicGridView.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

public partial class BirthRecordsSearchView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string browserName = Request.Browser.Browser;
        string browserCount = Convert.ToString(Session["BrowserCount"]);
        string appGUID = "appGUID_" + Convert.ToString(Session["userid"]) + "";
        string httpContextappGUID = Convert.ToString(HttpContext.Current.Application[appGUID]);
        string sessionGuid = Convert.ToString(Session["GuId"]);
        string existingbrowserName = Convert.ToString(Session["BrowserName"]);

        if (!Common.LoginUserSessionCheck(httpContextappGUID, sessionGuid, browserName, existingbrowserName, browserCount))
        {
            string env = ConfigurationManager.AppSettings["environment"];
            string miMiLogin = String.Empty;
            if (!string.IsNullOrEmpty(env))
            {
                if (env == "dev" || env == "qa")
                {
                    miMiLogin = "login.aspx";
                }
                else
                {
                    miMiLogin = "Error.aspx?credentialsMessage=doubleLogin";
                }
            }
            else
            {
                miMiLogin = ConfigurationManager.AppSettings["miMiLogin"];
            }

            Response.Redirect(miMiLogin);
        }

        try
        {
            if (!IsPostBack)
            {
                string schema = "MBDR_System";
                string tableName = "LinkedBirthRecordsView";
                string search = !string.IsNullOrEmpty(Request.QueryString["searchText"]) ? Convert.ToString(Request.QueryString["searchText"]) : "";
                string colName = !string.IsNullOrEmpty(Request.QueryString["colName"]) ? Convert.ToString(Request.QueryString["colName"]) : "";
                string searchFrom = !string.IsNullOrEmpty(Request.QueryString["searchFrom"]) ? Convert.ToString(Request.QueryString["searchFrom"]) : "";
                string childLastName = !string.IsNullOrEmpty(Request.QueryString["childLastName"]) ? Convert.ToString(Request.QueryString["childLastName"]) : "";
                string childFirstName = !string.IsNullOrEmpty(Request.QueryString["childFirstName"]) ? Convert.ToString(Request.QueryString["childFirstName"]) : "";
                string rawbirthDate = !string.IsNullOrEmpty(Request.QueryString["birthDate"]) ? Convert.ToString(Request.QueryString["birthDate"]) : "";
                string birthDate;
                if (DateTime.TryParse(rawbirthDate, out DateTime date))
                {
                    birthDate = date.ToString("MM/dd/yyyy");
                }
                else
                {
                    birthDate = rawbirthDate;
                }
                string momLastName = !string.IsNullOrEmpty(Request.QueryString["momLastName"]) ? Convert.ToString(Request.QueryString["momLastName"]) : "";
                string momFirstName = !string.IsNullOrEmpty(Request.QueryString["momFirstName"]) ? Convert.ToString(Request.QueryString["momFirstName"]) : "";

                string address1 = !string.IsNullOrEmpty(Request.QueryString["address1"]) ? Convert.ToString(Request.QueryString["address1"]) : "";
                hdfSchema.Value = schema;
                hdfTableName.Value = tableName;

                if (!string.IsNullOrEmpty(schema) && !string.IsNullOrEmpty(tableName))
                {
                    string headerLabel = !string.IsNullOrEmpty(Request.QueryString["name"]) ? Convert.ToString(Request.QueryString["name"]) : "";
                    List<GridViewModel> lstFilter = Common.GetFilterList();
                    GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
                    string[] header = model.Table.Split(':');

                    hdfName.Value = headerLabel;
                }
                txtchdLN.Text = childLastName;
                txtchdFN.Text = childFirstName;
                
                txtChildDOB.Text = birthDate;
                txtMomLN.Text = momLastName;
                txtMomFN.Text = momFirstName;
             
                if (!string.IsNullOrEmpty(childLastName) || !string.IsNullOrEmpty(childFirstName) || !string.IsNullOrEmpty(address1) || !string.IsNullOrEmpty(birthDate) || !string.IsNullOrEmpty(momLastName) || !string.IsNullOrEmpty(momFirstName)) 
                {
                    BindGrid(schema, tableName);
                }
                else
                {
                    BindGridEmpty(schema, tableName);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(isResetClick.Value) && isResetClick.Value == "true")
                {
                    BindGridEmpty(hdfSchema.Value, hdfTableName.Value);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_Page_Load");
        }
    }

    public void BindGridEmpty(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                SqlCommand command = new SqlCommand(sqlQuery, conn);
                command.Parameters.AddWithValue("@schema", schema);
                command.Parameters.AddWithValue("@tableName", tableName);
                command.CommandTimeout = 0;
                conn.Open();
                SqlDataAdapter daColumns = new SqlDataAdapter(command);
                conn.Close();
                daColumns.Fill(dt);
            }

            gvDynamic.Columns.Clear();
            CommandField commandField = new CommandField();
            commandField.ButtonType = ButtonType.Link;
            commandField.HeaderText = "Action";
            gvDynamic.Columns.Add(commandField);

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            List<string> lstColumns = includeColumns.Split(',').ToList();
            foreach (var col in lstColumns)
            {
                if (gvDynamic.Columns.Count < 6)
                {
                    if (!string.IsNullOrEmpty(col))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string colName = Convert.ToString(dt.Rows[i][0]);
                            string columnName = col.Contains("->") ? col.Split('>')[1] : col;
                            if (colName != "birthId" && colName != "address1" && colName != "masterRecordNumber" && colName != "deathNumber" && colName != "childMiddleName" && colName != "childSuffix" && colName != "childSSN" && colName != "momMiddleName" & colName != "momSuffix" && colName != "birthHospital")
                            {
                                if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                    BoundField field = new BoundField();
                                    field.HeaderText = columnInfo;
                                    field.DataField = Convert.ToString(dt.Rows[i][0]);
                                    field.SortExpression = Convert.ToString(dt.Rows[i][0]);
                                    if (readonlyColumns.Contains(colName))
                                    {
                                        field.ReadOnly = true;
                                    }
                                    gvDynamic.Columns.Add(field);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            DataTable dtEmpty = new DataTable();
           
            gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
            gvDynamic.DataSource = dtEmpty;
            gvDynamic.DataBind();
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_BindGridEmpty");
        }
    }

    private string GetWhereClause()
    {
        string where = "where 1=1";   

        if (!string.IsNullOrEmpty(txtchdLN.Text) && !txtchdLN.Text.Contains("*"))
        {
            where += " and childLastName = '" + txtchdLN.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtchdFN.Text) && !txtchdFN.Text.Contains("*"))
        {
            where += " and childFirstName = '" + txtchdFN.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtChildDOB.Text))
        {
            where += " and birthDate ='" + txtChildDOB.Text + "'";
        }
              

        if (!string.IsNullOrEmpty(txtMomLN.Text) && !txtMomLN.Text.Contains("*"))
        {
            where += " and momLastName = '" + txtMomLN.Text.Replace("'", "''").Trim() + "'";
        }

        if (!string.IsNullOrEmpty(txtMomFN.Text) && !txtMomFN.Text.Contains("*"))
        {
            where += " and momFirstName = '" + txtMomFN.Text.Trim().Replace("'", "''") + "'";
        }

        return where;
    }

    public void BindGrid(string schema, string tableName)
    {
        try
        {
            DataTable dt = new DataTable();
            string sortExp = string.Empty;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                sortExp = Convert.ToString(ViewState["SortExpression"]);
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    string sqlQuery = @"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @schema and TABLE_NAME = @tableName ORDER BY ORDINAL_POSITION";
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    command.Parameters.AddWithValue("@schema", schema);
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter daColumns = new SqlDataAdapter(command);
                    conn.Close();
                    DataTable dtColumns = new DataTable();
                    daColumns.Fill(dtColumns);
                    foreach (DataRow item in dtColumns.Rows)
                    {
                        if (Uri.EscapeDataString(Convert.ToString(item[0])) == "birthId")
                        {
                            sortExp = " birthId desc";
                            break;
                        }
                    }
                }
            }

            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Table.StartsWith(tableName) && s.Schema == schema).FirstOrDefault();
            string includeColumns = string.Empty;
            string readonlyColumns = string.Empty;
            string dataKeyname = string.Empty;
            if (model != null)
            {
                includeColumns = model.IncludedColumns;
                readonlyColumns = model.ReadonlyColumns;
                dataKeyname = model.DataKeyName;
            }
            hdfDataKeyName.Value = dataKeyname;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                string where = "";
                string OrderBySID = sortExp;
                if (string.IsNullOrEmpty(OrderBySID))
                {
                    OrderBySID = "birthId desc";
                }
                int pageIndex = ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 0;
                where = GetWhereClause();
                if (!where.Contains("and"))
                {
                    BindGridEmpty(schema, tableName);
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                    ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound(' Please enter search criteria.');", true);
                }
                else
                {
                    query = string.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {6}) AS ROW,* FROM (SELECT ROW_NUMBER() OVER (PARTITION BY birthId ORDER BY birthId DESC) AS CountOfRows,* FROM {0}.{1} {5}) AS RESULT) AS FullResult WHERE ROW BETWEEN ({3}) and ({4}) ORDER BY {6}", schema, tableName, dataKeyname, ((gvDynamic.PageIndex * gvDynamic.PageSize) + 1), (gvDynamic.PageIndex + 1) * gvDynamic.PageSize, where, OrderBySID);

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Close();
                    da.Fill(dt);

                    gvDynamic.Columns.Clear();
                    CommandField commandField = new CommandField();
                    commandField.ButtonType = ButtonType.Link;
                    commandField.HeaderText = "Action";
                    commandField.ShowEditButton = true;
                    gvDynamic.Columns.Add(commandField);

                    List<string> lstColumns = includeColumns.Split(',').ToList();

                    foreach (var col in lstColumns)
                    {
                        if (gvDynamic.Columns.Count < 6)
                        {
                            if (!string.IsNullOrEmpty(col))
                            {
                                foreach (DataColumn item in dt.Columns)
                                {
                                    string colName = item.ColumnName;
                                    string columnName = col.Contains("->") ? col.Split('>')[1] : col;

                                    if (colName != "birthId" && colName != "address1" && colName != "masterRecordNumber" && colName != "deathNumber" && colName != "childMiddleName" && colName != "childSuffix" && colName != "childSSN" && colName != "momMiddleName" & colName != "momSuffix" && colName != "birthHospital")
                                    {
                                        if (string.Equals(columnName.Split(':')[0].Trim().Replace("|", ""), colName, StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            string columnInfo = Common.GetBetween(includeColumns, colName + ":", ",");
                                            BoundField field = new BoundField();
                                            field.HeaderText = columnInfo;
                                            field.DataField = item.ColumnName;
                                            field.SortExpression = item.ColumnName;
                                            if (readonlyColumns.Contains(colName))
                                            {
                                                field.ReadOnly = true;
                                            }
                                            gvDynamic.Columns.Add(field);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    gvDynamic.DataKeyNames = new string[] { dataKeyname.Trim() };
                    gvDynamic.VirtualItemCount = GetTotalRecords(tableName, schema, dataKeyname);
                    if (gvDynamic.VirtualItemCount < 1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                        ClientScript.RegisterStartupScript(this.GetType(), "No record found.", "noRecFound( ' No record found.');", true);
                    }
                    ClientScript.RegisterStartupScript(this.GetType(), "disable spinner2", "removeProgress();", true);
                    gvDynamic.DataSource = dt;
                    gvDynamic.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_BindGrid");

            if (ex.Message.Contains("Execution Timeout Expired"))
            {
                btnReset_Click(this, EventArgs.Empty);
            }
        }
    }

    public int GetTotalRecords(string tableName, string schema, string datakeyName)
    {
        int totalRecords = 0;
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string query = "";
                int pageIndex = 0; 
                string where = GetWhereClause();
                query = string.Format("SELECT COUNT(DISTINCT {0}) from {1}.{2} {3} ", datakeyName, schema, tableName, where);
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandTimeout = 0;
                DataTable dt = new DataTable();
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    totalRecords = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_GetTotalRecords");
        }
        return totalRecords;
    }

    protected void gvDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvDynamic.PageIndex = e.NewPageIndex;
            ViewState["PageIndex"] = gvDynamic.PageIndex;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_PageIndexChanging");
        }
    }

    protected void gvDynamic_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvDynamic.EditIndex = -1;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_RowCancelingEdit");
        }
    }

    protected void gvDynamic_ViewRecord(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            List<GridViewModel> lstFilter = Common.GetFilterList();
            GridViewModel model = lstFilter.Where(s => s.Schema == hdfSchema.Value && s.Table.StartsWith(hdfTableName.Value)).FirstOrDefault();
            string dataKeyname = string.Empty;
            if (model != null)
            {
                dataKeyname = model.DataKeyName;
            }
            Response.Redirect("RecordDetail.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + dataKeyname + "&rowId=" + Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value) + "&name=" + hdfName.Value, true);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_ViewRecord");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void gvDynamic_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvDynamic.EditIndex = e.NewEditIndex;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_RowEditing");
        }
    }

    protected void gvDynamic_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                string id = Convert.ToString(gvDynamic.DataKeys[e.RowIndex].Value);
                GridViewRow row = (GridViewRow)gvDynamic.Rows[e.RowIndex];
                List<GridViewModel> lstFilter = Common.GetFilterList();
                GridViewModel model = lstFilter.Where(s => s.Schema == hdfSchema.Value && s.Table.StartsWith(hdfTableName.Value)).FirstOrDefault();
                string readonlyColumns = string.Empty;
                string dataKeyname = string.Empty;

                if (model != null)
                {
                    readonlyColumns = model.ReadonlyColumns;
                    dataKeyname = model.DataKeyName;
                }
                string query = " set ";
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    if (cell.ContainingField is BoundField)
                    {
                        string colName = ((BoundField)cell.ContainingField).DataField;
                        if (!readonlyColumns.Contains(colName) && colName != "id")
                        {
                            query = query + " " + ((BoundField)cell.ContainingField).DataField + "=" + string.Format("'{0}',", ((TextBox)cell.Controls[0]).Text);
                        }
                    }
                }
                gvDynamic.EditIndex = -1;
                conn.Open();
                string tableName = string.Format("{0}.{1}", hdfSchema.Value, hdfTableName.Value);
                string finalQuery = "update " + tableName + " " + query.Substring(0, query.Length - 1) + " where " + dataKeyname + "='" + id + "'";
                SqlCommand cmd = new SqlCommand(finalQuery, conn);
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
                conn.Close();
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_RowUpdating");
        }
    }

    protected void gvDynamic_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string exp = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            ViewState["SortExpression"] = exp;
            BindGrid(hdfSchema.Value, hdfTableName.Value);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_Sorting");
        }
    }

    protected string GetSortDirection(string column)
    {
        string nextDir = "ASC";
        try
        {
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == column)
            {
                nextDir = "DESC";
                ViewState["sort"] = null;
            }
            else
            {
                ViewState["sort"] = column;
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_GetSortDirection");
        }
        return nextDir;
    }

    protected void gvDynamic_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var row = (DataRowView)e.Row.DataItem;
                int index = -1;
                LinkButton field = e.Row.Cells[0].Controls[0] as LinkButton;
                field.Text = "View";
                field.ToolTip = string.Format("View Row No {0}", gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.CommandArgument = Convert.ToString(gvDynamic.DataKeys[e.Row.RowIndex].Value);
                field.OnClientClick = "ShowProgress();";
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_RowDataBound");
        }
    }

    protected void gvDynamic_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit")
            {
                string sortExpre = string.Empty;
                string displayName = string.Empty;

                string where = "1";


                if (!string.IsNullOrEmpty(txtchdLN.Text))
                {
                    where += "&childLastName=" + txtchdLN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtchdFN.Text))
                {
                    where += "&childFirstName=" + txtchdFN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtChildDOB.Text))
                {
                    where += "&birthDate=" + txtChildDOB.Text + "";
                }
              
                if (!string.IsNullOrEmpty(txtMomLN.Text))
                {
                    where += "&momLastName=" + txtMomLN.Text.Trim() + "";
                }
                if (!string.IsNullOrEmpty(txtMomFN.Text))
                {
                    where += "&momFirstName=" + txtMomFN.Text.Trim() + "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    sortExpre = "birthId desc";
                }
                else
                {
                    sortExpre = Convert.ToString(ViewState["SortExpression"]);
                }


                int getBirthID = Convert.ToInt32(e.CommandArgument.ToString());

                Response.Redirect("BirthRecordsSearchViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&keyName=" + hdfDataKeyName.Value + "&rowId=" + getBirthID + "&where=" + where + "&sortExp=" + sortExpre + "&refresh=true", false);

                //string encodedSID = Uri.EscapeDataString(id.ToString());

                //if (Convert.ToInt32(encodedSID) > 0)
                //{
                //    Response.Redirect("BirthRecordsSearchViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value +  "&where=" + where + "&sortExp=" + sortExpre + "&refresh=true", false);
                //}
                //else
                //{
                //    Response.Redirect("BirthRecordsSearchViewDetails.aspx?Schema=" + hdfSchema.Value + "&Table=" + hdfTableName.Value + "&where=" + where + "&sortExp=" + sortExpre + "&refresh=true", true);
                //}
                Context.ApplicationInstance.CompleteRequest();
            }
        }
        catch (ThreadAbortException)
        {
            // Ignore the ThreadAbortException
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_gvDynamic_RowCommand");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                gvDynamic.PageIndex = 0;
                BindGrid(hdfSchema.Value, hdfTableName.Value);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
            }
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_btnSearch_Click");
        }
        ClientScript.RegisterStartupScript(this.GetType(), "disable spinner", "removeProgress();", true);
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["SortExpression"] = "birthId desc";
            gvDynamic.PageIndex = 0;

            Response.Redirect("BirthRecordsSearchView?schema=MBDR_System&table=LinkedBirthRecordsView", false);
        }
        catch (Exception ex)
        {
            string path = ConfigurationManager.AppSettings["uiLogPath"];
            Common.ErrorLogCapture(ex, path, "BirthRecordsSearchView_btnReset_Click");
        }
    }

    protected void txtChildDOB_TextChanged(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "validate", "ValidateDate();", true);
        }
    }
}